<br>
<br>
		《FK 应用程序框架 1.96》预览版本介绍<br>
<br>
			作者：许宗森<br>
<br>
<br>
《FK 应用程序框架》基本上已经可以使用。此版本已经具备部分向导，RAD 功能。<br>
解压 fk_1.96.zip 文件，或者是 git 克隆文件， 建立根文件夹名称叫 fk<br>
<br>
<br>
1.97版本<br>
添加 ProjectDLL 向导。<br>
添加 简单类向导。里面包含 fk::LThread，fk::LObject,fk::LComponent,fk::LWindow,fk::LWindowMuch,fk::LEdit等类。<br>
添加 Dialog向导。<br>
添加 PropertyPage向导。<br>
添加 WindowPanel向导。<br>
添加非时时更新控件变量到 CPP 文件到。<br>
添加 .vxml 文件关系，双击打.vxml文件。<br>
添加使用命令行打开.vxml文件。<br>
添加向导自动管理项目 fk_local.xml 映射功能。<br>
添加支持XML节点值，中文读写，节点不支持。<br>
修复一些小功能，添加一些小功能。<br>
<br>
<br>
其中PropertyPage和WindowPanel向导，向导自动添加rgs文件到 .rc 文件中，添加接口类到 IDL 文件中，添加 IDR_XXXXXX 到 resource.h文件中。<br>
但是需要手动添加三行代码到以下三个函数中， DllGetClassObject DllRegisterServer DllUnregisterServer。<br>
这三行代码在向导生成类的 .h 文件中，稍后在更新这个自动添加的功能。<br>
<br>
下一个版本添加，自动生成事件。<br>
<br>
<br>
RAD功能：<br>
------------<br>
RAD 主要功能已经完成，控件的创建，拖动，属性编辑。<br>
1.已经支持大部分控件.部分控件稍后渐渐添加，非常容易，添加也很快。<br>
mask="0x00001003"是已经支持的控件，  mask="0x00001001" 是未支持的控件。<br>
<br>
<item mask="0x00001003" fk::LButton<br>
<item mask="0x00001003" fk::LCheckBox<br>
<item mask="0x00001003" fk::LRadioButton<br>
<item mask="0x00001003" fk::LEdit<br>
<item mask="0x00001003" fk::LComboBox<br>
<item mask="0x00001003" fk::LListBox<br>
<item mask="0x00001003" fk::LGroupBox<br>
<item mask="0x00001003" fk::LStatic<br>
<item mask="0x00001001" fk::LStatic<br>
<item mask="0x00001001" fk::LHorizontal<br>
<item mask="0x00001001" fk::LVertical<br>
<item mask="0x00001001" fk::LSlider<br>
<item mask="0x00001001" fk::LSpin<br>
<item mask="0x00001003" fk::LProgressBarCtrl<br>
<item mask="0x00001001" fk::LHotKey<br>
<item mask="0x00001003" fk::LSysListView32<br>
<item mask="0x00001003" fk::LTreeViewCtrl<br>
<item mask="0x00001003" fk::LTabCtrl<br>
<item mask="0x00001001" fk::LAnimation<br>
<item mask="0x00001001" fk::LRichEdit<br>
<item mask="0x00001003" fk::LTrackBarCtrl<br>
<item mask="0x00001003" fk::LDateTimePickerCtrl<br>
<item mask="0x00001003" fk::LMonthCalendarCtrl<br>
<item mask="0x00001001" fk::LIPAddress<br>
<item mask="0x00001001" fk::LExtendedComboBox<br>
<br>
<br>
RAD 问题：
1. RAD 建立 Group 控件，选择时有些问题，无法接受鼠标键。然后使用其它方法进行选定，
	只有弹出一个对话框后，Group 控件才能选定，而且不能拖动，只能拉大拉小. 这是个很讨厌的问题。
2. 选择多个控件后，同时改变窗口坐标属性，会出现坐标错乱，一个控件不会。

RAD 未添加的功能：<br>
1.控件改变的历史记录。<br>
2.事件。<br>
<br>
<br>

开发过程位置:<br>
1.在 《FK应用程序框架》文件夹中会有一个 fk_local.xml 文件，这个文件是用来映射 *.vxml、 配置文件、主题文件位置。<br>
2. .pxml .uxml 文件也映射到源代码 res 文件夹中。<br>
<br>
例如： <br>
d:\bin\project.exe 映射为源代码文件夹： E:\code\Project\res 文件夹. 使用绝对路径。<br>
d:\bin\project.dll 映射为源代码文件夹： E:\code\ProjectDll\res 文件夹. 使用绝对路径。<br>
<br>
<br>
* 开发过程文件位置 FK 向导自动完成，不需要手动管理。在创建项目时，向导自动添加映射功能。<br>
<br>
<br>
*设置 FK <br>
<br>
1.打开 FKView.exe。<br>
2.单击菜单 "工具\安装/卸载VS向导"，弹出对话框。<br>
<br>
3.设置FK根路径。<br>
4.设置FK模板路径。<br>
5.单击 "应用" 按钮。<br>
<br>
6.单击 “Install Wizard Visual Studio” 按钮。FKView 把向导安装到已经存在的 Visual Studio中。<br>
7.单击 “OK”按钮。<br>
8.打开 Visual Studio 2005, 之 Visual Studio 2013。<br>
