

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:33 2022
 */
/* Compiler settings for .\ArtFramework.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ArtFramework_i_h__
#define __ArtFramework_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IWindowPanel_FWD_DEFINED__
#define __IWindowPanel_FWD_DEFINED__
typedef interface IWindowPanel IWindowPanel;
#endif 	/* __IWindowPanel_FWD_DEFINED__ */


#ifndef __IGenericPanelNotify_FWD_DEFINED__
#define __IGenericPanelNotify_FWD_DEFINED__
typedef interface IGenericPanelNotify IGenericPanelNotify;
#endif 	/* __IGenericPanelNotify_FWD_DEFINED__ */


#ifndef __IMdiContainerNotify_FWD_DEFINED__
#define __IMdiContainerNotify_FWD_DEFINED__
typedef interface IMdiContainerNotify IMdiContainerNotify;
#endif 	/* __IMdiContainerNotify_FWD_DEFINED__ */


#ifndef __ISafProc_FWD_DEFINED__
#define __ISafProc_FWD_DEFINED__
typedef interface ISafProc ISafProc;
#endif 	/* __ISafProc_FWD_DEFINED__ */


#ifndef __IGenericPanelItem_FWD_DEFINED__
#define __IGenericPanelItem_FWD_DEFINED__
typedef interface IGenericPanelItem IGenericPanelItem;
#endif 	/* __IGenericPanelItem_FWD_DEFINED__ */


#ifndef __IGenericPanel_FWD_DEFINED__
#define __IGenericPanel_FWD_DEFINED__
typedef interface IGenericPanel IGenericPanel;
#endif 	/* __IGenericPanel_FWD_DEFINED__ */


#ifndef __IGenericPanels_FWD_DEFINED__
#define __IGenericPanels_FWD_DEFINED__
typedef interface IGenericPanels IGenericPanels;
#endif 	/* __IGenericPanels_FWD_DEFINED__ */


#ifndef __ITaskManager_FWD_DEFINED__
#define __ITaskManager_FWD_DEFINED__
typedef interface ITaskManager ITaskManager;
#endif 	/* __ITaskManager_FWD_DEFINED__ */


#ifndef __ISplitterWnd_FWD_DEFINED__
#define __ISplitterWnd_FWD_DEFINED__
typedef interface ISplitterWnd ISplitterWnd;
#endif 	/* __ISplitterWnd_FWD_DEFINED__ */


#ifndef __ISplitterCell_FWD_DEFINED__
#define __ISplitterCell_FWD_DEFINED__
typedef interface ISplitterCell ISplitterCell;
#endif 	/* __ISplitterCell_FWD_DEFINED__ */


#ifndef __ITabPageItem_FWD_DEFINED__
#define __ITabPageItem_FWD_DEFINED__
typedef interface ITabPageItem ITabPageItem;
#endif 	/* __ITabPageItem_FWD_DEFINED__ */


#ifndef __IStatusBar_FWD_DEFINED__
#define __IStatusBar_FWD_DEFINED__
typedef interface IStatusBar IStatusBar;
#endif 	/* __IStatusBar_FWD_DEFINED__ */


#ifndef __IMainFrame_FWD_DEFINED__
#define __IMainFrame_FWD_DEFINED__
typedef interface IMainFrame IMainFrame;
#endif 	/* __IMainFrame_FWD_DEFINED__ */


#ifndef __IMainFrames_FWD_DEFINED__
#define __IMainFrames_FWD_DEFINED__
typedef interface IMainFrames IMainFrames;
#endif 	/* __IMainFrames_FWD_DEFINED__ */


#ifndef __IMdiContainer_FWD_DEFINED__
#define __IMdiContainer_FWD_DEFINED__
typedef interface IMdiContainer IMdiContainer;
#endif 	/* __IMdiContainer_FWD_DEFINED__ */


#ifndef __IAddIn_FWD_DEFINED__
#define __IAddIn_FWD_DEFINED__
typedef interface IAddIn IAddIn;
#endif 	/* __IAddIn_FWD_DEFINED__ */


#ifndef __IAddIns_FWD_DEFINED__
#define __IAddIns_FWD_DEFINED__
typedef interface IAddIns IAddIns;
#endif 	/* __IAddIns_FWD_DEFINED__ */


#ifndef __IStatusBarItem_FWD_DEFINED__
#define __IStatusBarItem_FWD_DEFINED__
typedef interface IStatusBarItem IStatusBarItem;
#endif 	/* __IStatusBarItem_FWD_DEFINED__ */


#ifndef __IComFramework_FWD_DEFINED__
#define __IComFramework_FWD_DEFINED__
typedef interface IComFramework IComFramework;
#endif 	/* __IComFramework_FWD_DEFINED__ */


#ifndef __INavigationBar_FWD_DEFINED__
#define __INavigationBar_FWD_DEFINED__
typedef interface INavigationBar INavigationBar;
#endif 	/* __INavigationBar_FWD_DEFINED__ */


#ifndef __IExtensibility_FWD_DEFINED__
#define __IExtensibility_FWD_DEFINED__
typedef interface IExtensibility IExtensibility;
#endif 	/* __IExtensibility_FWD_DEFINED__ */


#ifndef ___IMainFrameEvents_FWD_DEFINED__
#define ___IMainFrameEvents_FWD_DEFINED__
typedef interface _IMainFrameEvents _IMainFrameEvents;
#endif 	/* ___IMainFrameEvents_FWD_DEFINED__ */


#ifndef __MainFrame_FWD_DEFINED__
#define __MainFrame_FWD_DEFINED__

#ifdef __cplusplus
typedef class MainFrame MainFrame;
#else
typedef struct MainFrame MainFrame;
#endif /* __cplusplus */

#endif 	/* __MainFrame_FWD_DEFINED__ */


#ifndef ___IMainFramesEvents_FWD_DEFINED__
#define ___IMainFramesEvents_FWD_DEFINED__
typedef interface _IMainFramesEvents _IMainFramesEvents;
#endif 	/* ___IMainFramesEvents_FWD_DEFINED__ */


#ifndef __MainFrames_FWD_DEFINED__
#define __MainFrames_FWD_DEFINED__

#ifdef __cplusplus
typedef class MainFrames MainFrames;
#else
typedef struct MainFrames MainFrames;
#endif /* __cplusplus */

#endif 	/* __MainFrames_FWD_DEFINED__ */


#ifndef ___IMdiContainerEvents_FWD_DEFINED__
#define ___IMdiContainerEvents_FWD_DEFINED__
typedef interface _IMdiContainerEvents _IMdiContainerEvents;
#endif 	/* ___IMdiContainerEvents_FWD_DEFINED__ */


#ifndef __MdiContainer_FWD_DEFINED__
#define __MdiContainer_FWD_DEFINED__

#ifdef __cplusplus
typedef class MdiContainer MdiContainer;
#else
typedef struct MdiContainer MdiContainer;
#endif /* __cplusplus */

#endif 	/* __MdiContainer_FWD_DEFINED__ */


#ifndef ___IAddInsEvents_FWD_DEFINED__
#define ___IAddInsEvents_FWD_DEFINED__
typedef interface _IAddInsEvents _IAddInsEvents;
#endif 	/* ___IAddInsEvents_FWD_DEFINED__ */


#ifndef __AddIns_FWD_DEFINED__
#define __AddIns_FWD_DEFINED__

#ifdef __cplusplus
typedef class AddIns AddIns;
#else
typedef struct AddIns AddIns;
#endif /* __cplusplus */

#endif 	/* __AddIns_FWD_DEFINED__ */


#ifndef ___IStatusBarEvents_FWD_DEFINED__
#define ___IStatusBarEvents_FWD_DEFINED__
typedef interface _IStatusBarEvents _IStatusBarEvents;
#endif 	/* ___IStatusBarEvents_FWD_DEFINED__ */


#ifndef __StatusBar_FWD_DEFINED__
#define __StatusBar_FWD_DEFINED__

#ifdef __cplusplus
typedef class StatusBar StatusBar;
#else
typedef struct StatusBar StatusBar;
#endif /* __cplusplus */

#endif 	/* __StatusBar_FWD_DEFINED__ */


#ifndef __Extensibility_FWD_DEFINED__
#define __Extensibility_FWD_DEFINED__

#ifdef __cplusplus
typedef class Extensibility Extensibility;
#else
typedef struct Extensibility Extensibility;
#endif /* __cplusplus */

#endif 	/* __Extensibility_FWD_DEFINED__ */


#ifndef ___IGenericPanelEvents_FWD_DEFINED__
#define ___IGenericPanelEvents_FWD_DEFINED__
typedef interface _IGenericPanelEvents _IGenericPanelEvents;
#endif 	/* ___IGenericPanelEvents_FWD_DEFINED__ */


#ifndef __GenericPanel_FWD_DEFINED__
#define __GenericPanel_FWD_DEFINED__

#ifdef __cplusplus
typedef class GenericPanel GenericPanel;
#else
typedef struct GenericPanel GenericPanel;
#endif /* __cplusplus */

#endif 	/* __GenericPanel_FWD_DEFINED__ */


#ifndef ___ITaskManagerEvents_FWD_DEFINED__
#define ___ITaskManagerEvents_FWD_DEFINED__
typedef interface _ITaskManagerEvents _ITaskManagerEvents;
#endif 	/* ___ITaskManagerEvents_FWD_DEFINED__ */


#ifndef __TaskManager_FWD_DEFINED__
#define __TaskManager_FWD_DEFINED__

#ifdef __cplusplus
typedef class TaskManager TaskManager;
#else
typedef struct TaskManager TaskManager;
#endif /* __cplusplus */

#endif 	/* __TaskManager_FWD_DEFINED__ */


#ifndef ___ISplitterWndEvents_FWD_DEFINED__
#define ___ISplitterWndEvents_FWD_DEFINED__
typedef interface _ISplitterWndEvents _ISplitterWndEvents;
#endif 	/* ___ISplitterWndEvents_FWD_DEFINED__ */


#ifndef __SplitterWnd_FWD_DEFINED__
#define __SplitterWnd_FWD_DEFINED__

#ifdef __cplusplus
typedef class SplitterWnd SplitterWnd;
#else
typedef struct SplitterWnd SplitterWnd;
#endif /* __cplusplus */

#endif 	/* __SplitterWnd_FWD_DEFINED__ */


#ifndef ___IGenericPanelsEvents_FWD_DEFINED__
#define ___IGenericPanelsEvents_FWD_DEFINED__
typedef interface _IGenericPanelsEvents _IGenericPanelsEvents;
#endif 	/* ___IGenericPanelsEvents_FWD_DEFINED__ */


#ifndef __GenericPanels_FWD_DEFINED__
#define __GenericPanels_FWD_DEFINED__

#ifdef __cplusplus
typedef class GenericPanels GenericPanels;
#else
typedef struct GenericPanels GenericPanels;
#endif /* __cplusplus */

#endif 	/* __GenericPanels_FWD_DEFINED__ */


#ifndef ___IComFrameworkEvents_FWD_DEFINED__
#define ___IComFrameworkEvents_FWD_DEFINED__
typedef interface _IComFrameworkEvents _IComFrameworkEvents;
#endif 	/* ___IComFrameworkEvents_FWD_DEFINED__ */


#ifndef __ComFramework_FWD_DEFINED__
#define __ComFramework_FWD_DEFINED__

#ifdef __cplusplus
typedef class ComFramework ComFramework;
#else
typedef struct ComFramework ComFramework;
#endif /* __cplusplus */

#endif 	/* __ComFramework_FWD_DEFINED__ */


#ifndef __LanguagePropPage_FWD_DEFINED__
#define __LanguagePropPage_FWD_DEFINED__

#ifdef __cplusplus
typedef class LanguagePropPage LanguagePropPage;
#else
typedef struct LanguagePropPage LanguagePropPage;
#endif /* __cplusplus */

#endif 	/* __LanguagePropPage_FWD_DEFINED__ */


#ifndef ___ICommandGroupEvents_FWD_DEFINED__
#define ___ICommandGroupEvents_FWD_DEFINED__
typedef interface _ICommandGroupEvents _ICommandGroupEvents;
#endif 	/* ___ICommandGroupEvents_FWD_DEFINED__ */


#ifndef ___INavigationBarEvents_FWD_DEFINED__
#define ___INavigationBarEvents_FWD_DEFINED__
typedef interface _INavigationBarEvents _INavigationBarEvents;
#endif 	/* ___INavigationBarEvents_FWD_DEFINED__ */


#ifndef __NavigationBar_FWD_DEFINED__
#define __NavigationBar_FWD_DEFINED__

#ifdef __cplusplus
typedef class NavigationBar NavigationBar;
#else
typedef struct NavigationBar NavigationBar;
#endif /* __cplusplus */

#endif 	/* __NavigationBar_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_ArtFramework_0000 */
/* [local] */ 

#include "windows.h"
#include "ole2.h"

















#define safMDIContainerPopupMenuKind L"safMDIContainerPopupMenu"
#define IDCMD_NAVIGATIONBAR_ONREFURBISH	10000
enum enumPartsInfo {
enumPartsInfoProgID=0x00000001,
enumPartsInfo_IID=0x00000002,
};
typedef /* [helpstring][uuid] */  DECLSPEC_UUID("CCF845FF-6EE0-4ebe-955D-9BB9BAD4404E") struct StatusBarItemInfo
    {
    UINT nID;
    UINT nWidth;
    } 	StatusBarItemInfo;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("CFAB8FF8-BCDA-40C3-B72C-8F7C0C3A7C55") 
enum enumMainFrameType
    {	umMainFrameTypeNull	= 0,
	umMainFrameTypeSDI	= 1,
	umMainFrameTypeMDI	= 2,
	umMainFrameTypeSDISplitter	= 3,
	umMainFrameTypeMDISplitter	= 4
    } 	enumMainFrameType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("a8e8d4de-1c64-487f-8186-e0efccc16b16") 
enum enumLoadLanguageType
    {	umLoadLanguageTypeArtFrame	= 2,
	umLoadLanguageTypeSoftware	= 3
    } 	enumLoadLanguageType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("89A25810-1543-4658-9336-028374AA07D2") 
enum enumConnectMode
    {	umConnectModeCommandLine	= 1,
	umConnectModeStartup	= 2,
	umConnectModeAfterStartup	= 3,
	umConnectModeUISetup	= 4,
	umConnectModeShowMainFrame	= 5
    } 	enumConnectMode;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("93A54B1D-CD0F-4793-B826-45BAA0DC1A00") 
enum enumDisconnectMode
    {	umDisconnectModeUninstallUI	= 1,
	umDisconnectModeUserClosed	= 2,
	umDisconnectModeCloseAddIn	= 3
    } 	enumDisconnectMode;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("841249C0-EFE2-11DB-B6CB-005056C00008") 
enum enumWindowPlace
    {	umWindowPlaceNone	= 0,
	umWindowPlaceLeft	= 0x1,
	umWindowPlaceRight	= 0x2,
	umWindowPlaceTop	= 0x3,
	umWindowPlaceBotton	= 0x4
    } 	enumWindowPlace;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("5491D487-42F4-4efb-B42D-5CC3F8F4CE44") 
enum enumGenericPanelStyle
    {	umGenericPanelStyleTab	= 0x1,
	umGenericPanelStylePage	= 0x2,
	umGenericPanelSytleFloating	= 0x4,
	umGenericPanelSytleFloatingState	= 0x6
    } 	enumGenericPanelStyle;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("C818B87D-D0B9-4fa7-ADE6-096A9C52C418") 
enum enumCellType
    {	umCellTypeLeft	= 0x1,
	umCellTypeRight	= 0x2
    } 	enumCellType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("35E31841-3371-43c7-8655-9A54FC96D5B7") 
enum enumRemoveCell
    {	umRemoveCellChildFloating	= 0x1,
	umRemoveCellChildDelete	= 0x2
    } 	enumRemoveCell;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("7557F8AD-A361-46ef-932B-4042CA6F9FF4") 
enum enumSplitterType
    {	umSplitterTypeVertical	= 0x1,
	umSplitterTypeHorizontal	= 0x2
    } 	enumSplitterType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("DF240C5D-DC25-4aa0-A48C-E826F5F47634") 
enum enumResizePos
    {	umResizePosCentre	= 0x1,
	umResizePosLeftFixed	= 0x2,
	umResizePosRightFixed	= 0x3,
	umResizePosLeftAuto	= 0x4,
	umResizePosRightAuto	= 0x5
    } 	enumResizePos;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("845EC77B-0F9D-4695-901E-926FC7F5E3D6") 
enum enumGenericPanelInfo
    {	umGenericPanelInfoTitle	= 0x1,
	umGenericPanelInfoDocString	= 0x2,
	umGenericPanelInfoHelpFile	= 0x3,
	umGenericPanelInfoHIcon	= 0x4
    } 	enumGenericPanelInfo;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("92A14458-65F9-452a-AC34-5EB6AA259E48") struct PARTSINFO
    {
    ULONG dwSize;
    ULONG dwMask;
    wchar_t *pszProgID;
    wchar_t *pszName;
    IID *piidKey;
    VARIANT_BOOL vbShow;
    } 	PARTSINFO;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("48565DA0-080B-483f-A0BC-9826B0AED276") struct MAINFRAMEINFO
    {
    DWORD dwSize;
    enumMainFrameType mftMainFrameType;
    wchar_t *bsName;
    wchar_t *bsTitle;
    LONG *hIcon;
    HMENU hMenu;
    IID iidKey;
    GUID cmdGuid;
    } 	MAINFRAMEINFO;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("B6DF67A5-7B42-4607-B6E8-2D2CC78B7DD7") struct GENERICPANELINFO
    {
    ULONG cb;
    ULONG uMask;
    wchar_t *lpszTitle;
    wchar_t *lpszDocString;
    wchar_t *lpszHelpFile;
    DWORD dwHelpContext;
    HICON hIcon;
    } 	GENERICPANELINFO;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("33F7A6CB-16F6-11DC-95AF-0011D8C3ECBE") 
enum enumCloseAction
    {	umCloseActionNone	= 0x1,
	umCloseActionHide	= 0x2,
	umCloseActionFree	= 0x3,
	umCloseActionMinimize	= 0x4
    } 	enumCloseAction;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("861988E0-7CD1-4f36-82D4-3D0212FDE618") 
enum enumMdiToolBar
    {	umMdiToolBarMenu	= 0x1,
	umMdiToolBarClose	= 0x2,
	umMdiToolBarCloseAll	= 0x8,
	umMdiToolBarWindows	= 0x10
    } 	enumMdiToolBar;



extern RPC_IF_HANDLE __MIDL_itf_ArtFramework_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_ArtFramework_0000_v0_0_s_ifspec;

#ifndef __IWindowPanel_INTERFACE_DEFINED__
#define __IWindowPanel_INTERFACE_DEFINED__

/* interface IWindowPanel */
/* [object][helpstring][uuid] */ 


EXTERN_C const IID IID_IWindowPanel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AC56C1CD-6B29-4369-9994-FD4C01E7F451")
    IWindowPanel : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWnd( 
            LONG_PTR *hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryCloseWindowPanel( 
            IGenericPanel *pParentPanel,
            enumCloseAction *ca,
            long *lpvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseWindowPanel( 
            IGenericPanel *pParentPanel,
            long *lpvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateWnd( 
            IGenericPanel *pParentPanel,
            int iLeft,
            int iTop,
            int iWidth,
            int iHeight,
            LONG_PTR hWndParent) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateWndXml( 
            IGenericPanel *pParentPanel,
            int iLeft,
            int iTop,
            int iWidth,
            int iHeight,
            LONG_PTR hWndParent,
            IUnknown *pXmlFile,
            LONG *hXmlNode) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnGenericPanelActiveWindowPanel( 
            IGenericPanel *pParentGenericPanel,
            IGenericPanelItem *pGenericPanelItem,
            UINT nType,
            VARIANT_BOOL *bActive,
            long *lpvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnMdiActiveWindowPanel( 
            IMdiContainer *pMdiContainer,
            IGenericPanel *pParentGenericPanel,
            ITabPageItem *pTabPageItem,
            UINT nType,
            VARIANT_BOOL bActive,
            VARIANT_BOOL *bOutActive,
            long *lpvObj) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IWindowPanelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IWindowPanel * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IWindowPanel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IWindowPanel * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWnd )( 
            IWindowPanel * This,
            LONG_PTR *hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryCloseWindowPanel )( 
            IWindowPanel * This,
            IGenericPanel *pParentPanel,
            enumCloseAction *ca,
            long *lpvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseWindowPanel )( 
            IWindowPanel * This,
            IGenericPanel *pParentPanel,
            long *lpvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateWnd )( 
            IWindowPanel * This,
            IGenericPanel *pParentPanel,
            int iLeft,
            int iTop,
            int iWidth,
            int iHeight,
            LONG_PTR hWndParent);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateWndXml )( 
            IWindowPanel * This,
            IGenericPanel *pParentPanel,
            int iLeft,
            int iTop,
            int iWidth,
            int iHeight,
            LONG_PTR hWndParent,
            IUnknown *pXmlFile,
            LONG *hXmlNode);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnGenericPanelActiveWindowPanel )( 
            IWindowPanel * This,
            IGenericPanel *pParentGenericPanel,
            IGenericPanelItem *pGenericPanelItem,
            UINT nType,
            VARIANT_BOOL *bActive,
            long *lpvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnMdiActiveWindowPanel )( 
            IWindowPanel * This,
            IMdiContainer *pMdiContainer,
            IGenericPanel *pParentGenericPanel,
            ITabPageItem *pTabPageItem,
            UINT nType,
            VARIANT_BOOL bActive,
            VARIANT_BOOL *bOutActive,
            long *lpvObj);
        
        END_INTERFACE
    } IWindowPanelVtbl;

    interface IWindowPanel
    {
        CONST_VTBL struct IWindowPanelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IWindowPanel_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IWindowPanel_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IWindowPanel_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IWindowPanel_GetWnd(This,hWnd)	\
    (This)->lpVtbl -> GetWnd(This,hWnd)

#define IWindowPanel_QueryCloseWindowPanel(This,pParentPanel,ca,lpvObj)	\
    (This)->lpVtbl -> QueryCloseWindowPanel(This,pParentPanel,ca,lpvObj)

#define IWindowPanel_CloseWindowPanel(This,pParentPanel,lpvObj)	\
    (This)->lpVtbl -> CloseWindowPanel(This,pParentPanel,lpvObj)

#define IWindowPanel_CreateWnd(This,pParentPanel,iLeft,iTop,iWidth,iHeight,hWndParent)	\
    (This)->lpVtbl -> CreateWnd(This,pParentPanel,iLeft,iTop,iWidth,iHeight,hWndParent)

#define IWindowPanel_CreateWndXml(This,pParentPanel,iLeft,iTop,iWidth,iHeight,hWndParent,pXmlFile,hXmlNode)	\
    (This)->lpVtbl -> CreateWndXml(This,pParentPanel,iLeft,iTop,iWidth,iHeight,hWndParent,pXmlFile,hXmlNode)

#define IWindowPanel_OnGenericPanelActiveWindowPanel(This,pParentGenericPanel,pGenericPanelItem,nType,bActive,lpvObj)	\
    (This)->lpVtbl -> OnGenericPanelActiveWindowPanel(This,pParentGenericPanel,pGenericPanelItem,nType,bActive,lpvObj)

#define IWindowPanel_OnMdiActiveWindowPanel(This,pMdiContainer,pParentGenericPanel,pTabPageItem,nType,bActive,bOutActive,lpvObj)	\
    (This)->lpVtbl -> OnMdiActiveWindowPanel(This,pMdiContainer,pParentGenericPanel,pTabPageItem,nType,bActive,bOutActive,lpvObj)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_GetWnd_Proxy( 
    IWindowPanel * This,
    LONG_PTR *hWnd);


void __RPC_STUB IWindowPanel_GetWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_QueryCloseWindowPanel_Proxy( 
    IWindowPanel * This,
    IGenericPanel *pParentPanel,
    enumCloseAction *ca,
    long *lpvObj);


void __RPC_STUB IWindowPanel_QueryCloseWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_CloseWindowPanel_Proxy( 
    IWindowPanel * This,
    IGenericPanel *pParentPanel,
    long *lpvObj);


void __RPC_STUB IWindowPanel_CloseWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_CreateWnd_Proxy( 
    IWindowPanel * This,
    IGenericPanel *pParentPanel,
    int iLeft,
    int iTop,
    int iWidth,
    int iHeight,
    LONG_PTR hWndParent);


void __RPC_STUB IWindowPanel_CreateWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_CreateWndXml_Proxy( 
    IWindowPanel * This,
    IGenericPanel *pParentPanel,
    int iLeft,
    int iTop,
    int iWidth,
    int iHeight,
    LONG_PTR hWndParent,
    IUnknown *pXmlFile,
    LONG *hXmlNode);


void __RPC_STUB IWindowPanel_CreateWndXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_OnGenericPanelActiveWindowPanel_Proxy( 
    IWindowPanel * This,
    IGenericPanel *pParentGenericPanel,
    IGenericPanelItem *pGenericPanelItem,
    UINT nType,
    VARIANT_BOOL *bActive,
    long *lpvObj);


void __RPC_STUB IWindowPanel_OnGenericPanelActiveWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IWindowPanel_OnMdiActiveWindowPanel_Proxy( 
    IWindowPanel * This,
    IMdiContainer *pMdiContainer,
    IGenericPanel *pParentGenericPanel,
    ITabPageItem *pTabPageItem,
    UINT nType,
    VARIANT_BOOL bActive,
    VARIANT_BOOL *bOutActive,
    long *lpvObj);


void __RPC_STUB IWindowPanel_OnMdiActiveWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IWindowPanel_INTERFACE_DEFINED__ */


#ifndef __IGenericPanelNotify_INTERFACE_DEFINED__
#define __IGenericPanelNotify_INTERFACE_DEFINED__

/* interface IGenericPanelNotify */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGenericPanelNotify;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("704FF4E0-B731-49d8-B5A5-B85A55B8096E")
    IGenericPanelNotify : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnGenericPanelDestroy( 
            IGenericPanel *pGenericPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnGenericPanelActive( 
            IGenericPanel *pGenericPanel,
            VARIANT_BOOL bActive) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnGenericPanelGetInfo( 
            GENERICPANELINFO *pGenericPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnGenericPanelGetRect( 
            LONG_PTR *lpRect,
            BSTR *bstrXmlPath) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGenericPanelNotifyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGenericPanelNotify * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGenericPanelNotify * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGenericPanelNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnGenericPanelDestroy )( 
            IGenericPanelNotify * This,
            IGenericPanel *pGenericPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnGenericPanelActive )( 
            IGenericPanelNotify * This,
            IGenericPanel *pGenericPanel,
            VARIANT_BOOL bActive);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnGenericPanelGetInfo )( 
            IGenericPanelNotify * This,
            GENERICPANELINFO *pGenericPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnGenericPanelGetRect )( 
            IGenericPanelNotify * This,
            LONG_PTR *lpRect,
            BSTR *bstrXmlPath);
        
        END_INTERFACE
    } IGenericPanelNotifyVtbl;

    interface IGenericPanelNotify
    {
        CONST_VTBL struct IGenericPanelNotifyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGenericPanelNotify_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGenericPanelNotify_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGenericPanelNotify_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGenericPanelNotify_OnGenericPanelDestroy(This,pGenericPanel)	\
    (This)->lpVtbl -> OnGenericPanelDestroy(This,pGenericPanel)

#define IGenericPanelNotify_OnGenericPanelActive(This,pGenericPanel,bActive)	\
    (This)->lpVtbl -> OnGenericPanelActive(This,pGenericPanel,bActive)

#define IGenericPanelNotify_OnGenericPanelGetInfo(This,pGenericPanel)	\
    (This)->lpVtbl -> OnGenericPanelGetInfo(This,pGenericPanel)

#define IGenericPanelNotify_OnGenericPanelGetRect(This,lpRect,bstrXmlPath)	\
    (This)->lpVtbl -> OnGenericPanelGetRect(This,lpRect,bstrXmlPath)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanelNotify_OnGenericPanelDestroy_Proxy( 
    IGenericPanelNotify * This,
    IGenericPanel *pGenericPanel);


void __RPC_STUB IGenericPanelNotify_OnGenericPanelDestroy_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanelNotify_OnGenericPanelActive_Proxy( 
    IGenericPanelNotify * This,
    IGenericPanel *pGenericPanel,
    VARIANT_BOOL bActive);


void __RPC_STUB IGenericPanelNotify_OnGenericPanelActive_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanelNotify_OnGenericPanelGetInfo_Proxy( 
    IGenericPanelNotify * This,
    GENERICPANELINFO *pGenericPanel);


void __RPC_STUB IGenericPanelNotify_OnGenericPanelGetInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanelNotify_OnGenericPanelGetRect_Proxy( 
    IGenericPanelNotify * This,
    LONG_PTR *lpRect,
    BSTR *bstrXmlPath);


void __RPC_STUB IGenericPanelNotify_OnGenericPanelGetRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGenericPanelNotify_INTERFACE_DEFINED__ */


#ifndef __IMdiContainerNotify_INTERFACE_DEFINED__
#define __IMdiContainerNotify_INTERFACE_DEFINED__

/* interface IMdiContainerNotify */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMdiContainerNotify;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8A977895-34F0-477A-BF37-8FDD8A9A60BE")
    IMdiContainerNotify : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ActiveItem( 
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem,
            /* [out] */ VARIANT_BOOL *bActive) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryCloseItem( 
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem,
            /* [out] */ VARIANT_BOOL *bClose) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMdiContainerNotifyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMdiContainerNotify * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMdiContainerNotify * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMdiContainerNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IMdiContainerNotify * This,
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ActiveItem )( 
            IMdiContainerNotify * This,
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem,
            /* [out] */ VARIANT_BOOL *bActive);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryCloseItem )( 
            IMdiContainerNotify * This,
            /* [in] */ IMdiContainer *pMdiContainer,
            /* [in] */ ITabPageItem *pTabPageItem,
            /* [out] */ VARIANT_BOOL *bClose);
        
        END_INTERFACE
    } IMdiContainerNotifyVtbl;

    interface IMdiContainerNotify
    {
        CONST_VTBL struct IMdiContainerNotifyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMdiContainerNotify_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMdiContainerNotify_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMdiContainerNotify_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMdiContainerNotify_InsertItem(This,pMdiContainer,pTabPageItem)	\
    (This)->lpVtbl -> InsertItem(This,pMdiContainer,pTabPageItem)

#define IMdiContainerNotify_ActiveItem(This,pMdiContainer,pTabPageItem,bActive)	\
    (This)->lpVtbl -> ActiveItem(This,pMdiContainer,pTabPageItem,bActive)

#define IMdiContainerNotify_QueryCloseItem(This,pMdiContainer,pTabPageItem,bClose)	\
    (This)->lpVtbl -> QueryCloseItem(This,pMdiContainer,pTabPageItem,bClose)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainerNotify_InsertItem_Proxy( 
    IMdiContainerNotify * This,
    /* [in] */ IMdiContainer *pMdiContainer,
    /* [in] */ ITabPageItem *pTabPageItem);


void __RPC_STUB IMdiContainerNotify_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainerNotify_ActiveItem_Proxy( 
    IMdiContainerNotify * This,
    /* [in] */ IMdiContainer *pMdiContainer,
    /* [in] */ ITabPageItem *pTabPageItem,
    /* [out] */ VARIANT_BOOL *bActive);


void __RPC_STUB IMdiContainerNotify_ActiveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainerNotify_QueryCloseItem_Proxy( 
    IMdiContainerNotify * This,
    /* [in] */ IMdiContainer *pMdiContainer,
    /* [in] */ ITabPageItem *pTabPageItem,
    /* [out] */ VARIANT_BOOL *bClose);


void __RPC_STUB IMdiContainerNotify_QueryCloseItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMdiContainerNotify_INTERFACE_DEFINED__ */


#ifndef __ISafProc_INTERFACE_DEFINED__
#define __ISafProc_INTERFACE_DEFINED__

/* interface ISafProc */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISafProc;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B1334678-37AB-4271-B9F9-EBF66C8E2FC1")
    ISafProc : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SafProc( 
            int nInfo,
            UINT_PTR wParam,
            LONG_PTR lParam) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISafProcVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISafProc * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISafProc * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISafProc * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SafProc )( 
            ISafProc * This,
            int nInfo,
            UINT_PTR wParam,
            LONG_PTR lParam);
        
        END_INTERFACE
    } ISafProcVtbl;

    interface ISafProc
    {
        CONST_VTBL struct ISafProcVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISafProc_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISafProc_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISafProc_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISafProc_SafProc(This,nInfo,wParam,lParam)	\
    (This)->lpVtbl -> SafProc(This,nInfo,wParam,lParam)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ISafProc_SafProc_Proxy( 
    ISafProc * This,
    int nInfo,
    UINT_PTR wParam,
    LONG_PTR lParam);


void __RPC_STUB ISafProc_SafProc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISafProc_INTERFACE_DEFINED__ */


#ifndef __IGenericPanelItem_INTERFACE_DEFINED__
#define __IGenericPanelItem_INTERFACE_DEFINED__

/* interface IGenericPanelItem */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGenericPanelItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E48D8E25-122D-469C-A5E0-EB17FBB6B09E")
    IGenericPanelItem : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Title( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Icon( 
            /* [retval][out] */ HICON *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Icon( 
            /* [in] */ HICON newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_GenericPanel( 
            /* [retval][out] */ IGenericPanel **ppGenericPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Visible( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Visible( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Bitmap( 
            /* [retval][out] */ HBITMAP *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Bitmap( 
            /* [in] */ HBITMAP newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ParentGenericPanel( 
            /* [retval][out] */ IGenericPanel **ppParentPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Active( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Object( 
            /* [retval][out] */ LONG_PTR **pObject) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Object( 
            /* [in] */ LONG_PTR *pObject) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_WindowPanel( 
            /* [retval][out] */ IWindowPanel **ppWindowPanel) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGenericPanelItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGenericPanelItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGenericPanelItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGenericPanelItem * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGenericPanelItem * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGenericPanelItem * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGenericPanelItem * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGenericPanelItem * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IGenericPanelItem * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Title )( 
            IGenericPanelItem * This,
            /* [in] */ BSTR newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Icon )( 
            IGenericPanelItem * This,
            /* [retval][out] */ HICON *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Icon )( 
            IGenericPanelItem * This,
            /* [in] */ HICON newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GenericPanel )( 
            IGenericPanelItem * This,
            /* [retval][out] */ IGenericPanel **ppGenericPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IGenericPanelItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IGenericPanelItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bitmap )( 
            IGenericPanelItem * This,
            /* [retval][out] */ HBITMAP *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Bitmap )( 
            IGenericPanelItem * This,
            /* [in] */ HBITMAP newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ParentGenericPanel )( 
            IGenericPanelItem * This,
            /* [retval][out] */ IGenericPanel **ppParentPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Active )( 
            IGenericPanelItem * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Object )( 
            IGenericPanelItem * This,
            /* [retval][out] */ LONG_PTR **pObject);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Object )( 
            IGenericPanelItem * This,
            /* [in] */ LONG_PTR *pObject);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_WindowPanel )( 
            IGenericPanelItem * This,
            /* [retval][out] */ IWindowPanel **ppWindowPanel);
        
        END_INTERFACE
    } IGenericPanelItemVtbl;

    interface IGenericPanelItem
    {
        CONST_VTBL struct IGenericPanelItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGenericPanelItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGenericPanelItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGenericPanelItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGenericPanelItem_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IGenericPanelItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IGenericPanelItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IGenericPanelItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IGenericPanelItem_get_Title(This,pVal)	\
    (This)->lpVtbl -> get_Title(This,pVal)

#define IGenericPanelItem_put_Title(This,newVal)	\
    (This)->lpVtbl -> put_Title(This,newVal)

#define IGenericPanelItem_get_Icon(This,pVal)	\
    (This)->lpVtbl -> get_Icon(This,pVal)

#define IGenericPanelItem_put_Icon(This,newVal)	\
    (This)->lpVtbl -> put_Icon(This,newVal)

#define IGenericPanelItem_get_GenericPanel(This,ppGenericPanel)	\
    (This)->lpVtbl -> get_GenericPanel(This,ppGenericPanel)

#define IGenericPanelItem_get_Visible(This,pVal)	\
    (This)->lpVtbl -> get_Visible(This,pVal)

#define IGenericPanelItem_put_Visible(This,newVal)	\
    (This)->lpVtbl -> put_Visible(This,newVal)

#define IGenericPanelItem_get_Bitmap(This,pVal)	\
    (This)->lpVtbl -> get_Bitmap(This,pVal)

#define IGenericPanelItem_put_Bitmap(This,newVal)	\
    (This)->lpVtbl -> put_Bitmap(This,newVal)

#define IGenericPanelItem_get_ParentGenericPanel(This,ppParentPanel)	\
    (This)->lpVtbl -> get_ParentGenericPanel(This,ppParentPanel)

#define IGenericPanelItem_Active(This)	\
    (This)->lpVtbl -> Active(This)

#define IGenericPanelItem_get_Object(This,pObject)	\
    (This)->lpVtbl -> get_Object(This,pObject)

#define IGenericPanelItem_put_Object(This,pObject)	\
    (This)->lpVtbl -> put_Object(This,pObject)

#define IGenericPanelItem_get_WindowPanel(This,ppWindowPanel)	\
    (This)->lpVtbl -> get_WindowPanel(This,ppWindowPanel)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_Title_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IGenericPanelItem_get_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_put_Title_Proxy( 
    IGenericPanelItem * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IGenericPanelItem_put_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_Icon_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ HICON *pVal);


void __RPC_STUB IGenericPanelItem_get_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_put_Icon_Proxy( 
    IGenericPanelItem * This,
    /* [in] */ HICON newVal);


void __RPC_STUB IGenericPanelItem_put_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_GenericPanel_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ IGenericPanel **ppGenericPanel);


void __RPC_STUB IGenericPanelItem_get_GenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_Visible_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IGenericPanelItem_get_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_put_Visible_Proxy( 
    IGenericPanelItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IGenericPanelItem_put_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_Bitmap_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ HBITMAP *pVal);


void __RPC_STUB IGenericPanelItem_get_Bitmap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_put_Bitmap_Proxy( 
    IGenericPanelItem * This,
    /* [in] */ HBITMAP newVal);


void __RPC_STUB IGenericPanelItem_put_Bitmap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_ParentGenericPanel_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ IGenericPanel **ppParentPanel);


void __RPC_STUB IGenericPanelItem_get_ParentGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_Active_Proxy( 
    IGenericPanelItem * This);


void __RPC_STUB IGenericPanelItem_Active_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_Object_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ LONG_PTR **pObject);


void __RPC_STUB IGenericPanelItem_get_Object_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_put_Object_Proxy( 
    IGenericPanelItem * This,
    /* [in] */ LONG_PTR *pObject);


void __RPC_STUB IGenericPanelItem_put_Object_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanelItem_get_WindowPanel_Proxy( 
    IGenericPanelItem * This,
    /* [retval][out] */ IWindowPanel **ppWindowPanel);


void __RPC_STUB IGenericPanelItem_get_WindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGenericPanelItem_INTERFACE_DEFINED__ */


#ifndef __IGenericPanel_INTERFACE_DEFINED__
#define __IGenericPanel_INTERFACE_DEFINED__

/* interface IGenericPanel */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGenericPanel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BFFDBFD6-96AF-4278-8180-6272D89D96CE")
    IGenericPanel : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBars( 
            /* [retval][out] */ IUnknown **ppCommandBars) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Title( 
            /* [retval][out] */ BSTR *pbstrTitleName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Title( 
            /* [in] */ BSTR bstrTitleName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateGenericPanel( 
            LONG_PTR hWndParent,
            BSTR bstrTitle,
            LONG x,
            LONG y,
            LONG nWidth,
            LONG nHeight,
            VARIANT_BOOL bFloating,
            enumGenericPanelStyle genericPanelStyle) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowWindow( 
            /* [retval][out] */ VARIANT_BOOL *bpShow) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowWindow( 
            /* [in] */ VARIANT_BOOL bShow) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateCommandBars( 
            BSTR strTittle,
            long umCommandBarsType,
            /* [retval][out] */ long **pCommandBars) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Floating( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Floating( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Transparence( 
            /* [retval][out] */ VARIANT_BOOL *bpTransparence) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Transparence( 
            /* [in] */ VARIANT_BOOL bTransparence) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetWindowPos( 
            int X,
            int Y,
            int cx,
            int cy,
            UINT uFlags) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWindowPos( 
            int *x,
            int *y,
            int *x1,
            int *y1) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateLayout( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nIndex,
            BSTR bstrTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [out] */ IGenericPanel **ppgp,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive,
            enumGenericPanelStyle genericPanelStyle,
            long *pvObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItemComControl( 
            int nIndex,
            BSTR strName,
            BSTR lpszComControlName,
            IStream *pStream,
            IUnknown **ppUnkContainer,
            IUnknown **ppUnkControl,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyGenericPanel( 
            /* [retval][out] */ IGenericPanelNotify **pNotifyGenericPanel) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_NotifyGenericPanel( 
            /* [in] */ IGenericPanelNotify *pNotifyGenericPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_TabCtrlPlace( 
            /* [retval][out] */ enumWindowPlace *winPlace) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_TabCtrlPlace( 
            /* [in] */ enumWindowPlace winPlace) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pbsName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR bsName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowCaption( 
            /* [retval][out] */ VARIANT_BOOL *pbShow) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowCaption( 
            VARIANT_BOOL bShow) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActivePage( 
            VARIANT *vtItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetNotify( 
            ULONG **ppNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItemPage( 
            int nIndex,
            BSTR bstrTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [out] */ IGenericPanel **ppgp,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive,
            enumGenericPanelStyle genericPanelStyle,
            VARIANT_BOOL bFixed,
            VARIANT_BOOL bRoll,
            long *pvObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LoadXml( 
            long *pModule,
            long *hXmlConfig,
            unsigned char *pwsXmlPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pVal) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IGenericPanelItem **ppPanelItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSplitterWnd( 
            ISplitterWnd **ppSplitterWnd) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_SplitterWnd( 
            /* [retval][out] */ ISplitterWnd **ppSplitterWnd) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_OkFocus( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_OkFocus( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetActiveItem( 
            IGenericPanelItem **pGenericPanelItem,
            IGenericPanel **ppGenericPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ItemObject( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ LONG_PTR **ppObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ VARIANT varItem) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_RootSplitterWnd( 
            ISplitterWnd **ppSplitterWnd) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_RootWindowMuch( 
            ULONG **ppWindowMuch) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_SplitterCellThis( 
            ISplitterCell **ppSplitterCell) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGenericPanelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGenericPanel * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGenericPanel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGenericPanel * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGenericPanel * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGenericPanel * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGenericPanel * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGenericPanel * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBars )( 
            IGenericPanel * This,
            /* [retval][out] */ IUnknown **ppCommandBars);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Title )( 
            IGenericPanel * This,
            /* [retval][out] */ BSTR *pbstrTitleName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Title )( 
            IGenericPanel * This,
            /* [in] */ BSTR bstrTitleName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateGenericPanel )( 
            IGenericPanel * This,
            LONG_PTR hWndParent,
            BSTR bstrTitle,
            LONG x,
            LONG y,
            LONG nWidth,
            LONG nHeight,
            VARIANT_BOOL bFloating,
            enumGenericPanelStyle genericPanelStyle);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowWindow )( 
            IGenericPanel * This,
            /* [retval][out] */ VARIANT_BOOL *bpShow);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowWindow )( 
            IGenericPanel * This,
            /* [in] */ VARIANT_BOOL bShow);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateCommandBars )( 
            IGenericPanel * This,
            BSTR strTittle,
            long umCommandBarsType,
            /* [retval][out] */ long **pCommandBars);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Floating )( 
            IGenericPanel * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Floating )( 
            IGenericPanel * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Transparence )( 
            IGenericPanel * This,
            /* [retval][out] */ VARIANT_BOOL *bpTransparence);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Transparence )( 
            IGenericPanel * This,
            /* [in] */ VARIANT_BOOL bTransparence);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetWindowPos )( 
            IGenericPanel * This,
            int X,
            int Y,
            int cx,
            int cy,
            UINT uFlags);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWindowPos )( 
            IGenericPanel * This,
            int *x,
            int *y,
            int *x1,
            int *y1);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateLayout )( 
            IGenericPanel * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IGenericPanel * This,
            int nIndex,
            BSTR bstrTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [out] */ IGenericPanel **ppgp,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive,
            enumGenericPanelStyle genericPanelStyle,
            long *pvObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItemComControl )( 
            IGenericPanel * This,
            int nIndex,
            BSTR strName,
            BSTR lpszComControlName,
            IStream *pStream,
            IUnknown **ppUnkContainer,
            IUnknown **ppUnkControl,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyGenericPanel )( 
            IGenericPanel * This,
            /* [retval][out] */ IGenericPanelNotify **pNotifyGenericPanel);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyGenericPanel )( 
            IGenericPanel * This,
            /* [in] */ IGenericPanelNotify *pNotifyGenericPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TabCtrlPlace )( 
            IGenericPanel * This,
            /* [retval][out] */ enumWindowPlace *winPlace);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TabCtrlPlace )( 
            IGenericPanel * This,
            /* [in] */ enumWindowPlace winPlace);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IGenericPanel * This,
            /* [retval][out] */ BSTR *pbsName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            IGenericPanel * This,
            /* [in] */ BSTR bsName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowCaption )( 
            IGenericPanel * This,
            /* [retval][out] */ VARIANT_BOOL *pbShow);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowCaption )( 
            IGenericPanel * This,
            VARIANT_BOOL bShow);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActivePage )( 
            IGenericPanel * This,
            VARIANT *vtItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetNotify )( 
            IGenericPanel * This,
            ULONG **ppNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItemPage )( 
            IGenericPanel * This,
            int nIndex,
            BSTR bstrTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [out] */ IGenericPanel **ppgp,
            /* [out] */ IGenericPanelItem **pPageItem,
            VARIANT_BOOL bActive,
            enumGenericPanelStyle genericPanelStyle,
            VARIANT_BOOL bFixed,
            VARIANT_BOOL bRoll,
            long *pvObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LoadXml )( 
            IGenericPanel * This,
            long *pModule,
            long *hXmlConfig,
            unsigned char *pwsXmlPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IGenericPanel * This,
            /* [retval][out] */ long *pVal);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            IGenericPanel * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IGenericPanelItem **ppPanelItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSplitterWnd )( 
            IGenericPanel * This,
            ISplitterWnd **ppSplitterWnd);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SplitterWnd )( 
            IGenericPanel * This,
            /* [retval][out] */ ISplitterWnd **ppSplitterWnd);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OkFocus )( 
            IGenericPanel * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OkFocus )( 
            IGenericPanel * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetActiveItem )( 
            IGenericPanel * This,
            IGenericPanelItem **pGenericPanelItem,
            IGenericPanel **ppGenericPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ItemObject )( 
            IGenericPanel * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ LONG_PTR **ppObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            IGenericPanel * This,
            /* [in] */ VARIANT varItem);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RootSplitterWnd )( 
            IGenericPanel * This,
            ISplitterWnd **ppSplitterWnd);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RootWindowMuch )( 
            IGenericPanel * This,
            ULONG **ppWindowMuch);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SplitterCellThis )( 
            IGenericPanel * This,
            ISplitterCell **ppSplitterCell);
        
        END_INTERFACE
    } IGenericPanelVtbl;

    interface IGenericPanel
    {
        CONST_VTBL struct IGenericPanelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGenericPanel_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGenericPanel_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGenericPanel_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGenericPanel_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IGenericPanel_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IGenericPanel_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IGenericPanel_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IGenericPanel_get_CommandBars(This,ppCommandBars)	\
    (This)->lpVtbl -> get_CommandBars(This,ppCommandBars)

#define IGenericPanel_get_Title(This,pbstrTitleName)	\
    (This)->lpVtbl -> get_Title(This,pbstrTitleName)

#define IGenericPanel_put_Title(This,bstrTitleName)	\
    (This)->lpVtbl -> put_Title(This,bstrTitleName)

#define IGenericPanel_CreateGenericPanel(This,hWndParent,bstrTitle,x,y,nWidth,nHeight,bFloating,genericPanelStyle)	\
    (This)->lpVtbl -> CreateGenericPanel(This,hWndParent,bstrTitle,x,y,nWidth,nHeight,bFloating,genericPanelStyle)

#define IGenericPanel_get_ShowWindow(This,bpShow)	\
    (This)->lpVtbl -> get_ShowWindow(This,bpShow)

#define IGenericPanel_put_ShowWindow(This,bShow)	\
    (This)->lpVtbl -> put_ShowWindow(This,bShow)

#define IGenericPanel_CreateCommandBars(This,strTittle,umCommandBarsType,pCommandBars)	\
    (This)->lpVtbl -> CreateCommandBars(This,strTittle,umCommandBarsType,pCommandBars)

#define IGenericPanel_get_Floating(This,pVal)	\
    (This)->lpVtbl -> get_Floating(This,pVal)

#define IGenericPanel_put_Floating(This,newVal)	\
    (This)->lpVtbl -> put_Floating(This,newVal)

#define IGenericPanel_get_Transparence(This,bpTransparence)	\
    (This)->lpVtbl -> get_Transparence(This,bpTransparence)

#define IGenericPanel_put_Transparence(This,bTransparence)	\
    (This)->lpVtbl -> put_Transparence(This,bTransparence)

#define IGenericPanel_SetWindowPos(This,X,Y,cx,cy,uFlags)	\
    (This)->lpVtbl -> SetWindowPos(This,X,Y,cx,cy,uFlags)

#define IGenericPanel_GetWindowPos(This,x,y,x1,y1)	\
    (This)->lpVtbl -> GetWindowPos(This,x,y,x1,y1)

#define IGenericPanel_UpdateLayout(This)	\
    (This)->lpVtbl -> UpdateLayout(This)

#define IGenericPanel_InsertItem(This,nIndex,bstrTitle,hIcon,pWindowPanel,ppgp,pPageItem,bActive,genericPanelStyle,pvObject)	\
    (This)->lpVtbl -> InsertItem(This,nIndex,bstrTitle,hIcon,pWindowPanel,ppgp,pPageItem,bActive,genericPanelStyle,pvObject)

#define IGenericPanel_InsertItemComControl(This,nIndex,strName,lpszComControlName,pStream,ppUnkContainer,ppUnkControl,pPageItem,bActive)	\
    (This)->lpVtbl -> InsertItemComControl(This,nIndex,strName,lpszComControlName,pStream,ppUnkContainer,ppUnkControl,pPageItem,bActive)

#define IGenericPanel_get_NotifyGenericPanel(This,pNotifyGenericPanel)	\
    (This)->lpVtbl -> get_NotifyGenericPanel(This,pNotifyGenericPanel)

#define IGenericPanel_put_NotifyGenericPanel(This,pNotifyGenericPanel)	\
    (This)->lpVtbl -> put_NotifyGenericPanel(This,pNotifyGenericPanel)

#define IGenericPanel_get_TabCtrlPlace(This,winPlace)	\
    (This)->lpVtbl -> get_TabCtrlPlace(This,winPlace)

#define IGenericPanel_put_TabCtrlPlace(This,winPlace)	\
    (This)->lpVtbl -> put_TabCtrlPlace(This,winPlace)

#define IGenericPanel_get_Name(This,pbsName)	\
    (This)->lpVtbl -> get_Name(This,pbsName)

#define IGenericPanel_put_Name(This,bsName)	\
    (This)->lpVtbl -> put_Name(This,bsName)

#define IGenericPanel_get_ShowCaption(This,pbShow)	\
    (This)->lpVtbl -> get_ShowCaption(This,pbShow)

#define IGenericPanel_put_ShowCaption(This,bShow)	\
    (This)->lpVtbl -> put_ShowCaption(This,bShow)

#define IGenericPanel_SetActivePage(This,vtItem)	\
    (This)->lpVtbl -> SetActivePage(This,vtItem)

#define IGenericPanel_GetNotify(This,ppNotify)	\
    (This)->lpVtbl -> GetNotify(This,ppNotify)

#define IGenericPanel_InsertItemPage(This,nIndex,bstrTitle,hIcon,pWindowPanel,ppgp,pPageItem,bActive,genericPanelStyle,bFixed,bRoll,pvObject)	\
    (This)->lpVtbl -> InsertItemPage(This,nIndex,bstrTitle,hIcon,pWindowPanel,ppgp,pPageItem,bActive,genericPanelStyle,bFixed,bRoll,pvObject)

#define IGenericPanel_LoadXml(This,pModule,hXmlConfig,pwsXmlPath)	\
    (This)->lpVtbl -> LoadXml(This,pModule,hXmlConfig,pwsXmlPath)

#define IGenericPanel_get_Count(This,pVal)	\
    (This)->lpVtbl -> get_Count(This,pVal)

#define IGenericPanel_get_Item(This,varItem,ppPanelItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,ppPanelItem)

#define IGenericPanel_CreateSplitterWnd(This,ppSplitterWnd)	\
    (This)->lpVtbl -> CreateSplitterWnd(This,ppSplitterWnd)

#define IGenericPanel_get_SplitterWnd(This,ppSplitterWnd)	\
    (This)->lpVtbl -> get_SplitterWnd(This,ppSplitterWnd)

#define IGenericPanel_get_OkFocus(This,pVal)	\
    (This)->lpVtbl -> get_OkFocus(This,pVal)

#define IGenericPanel_put_OkFocus(This,newVal)	\
    (This)->lpVtbl -> put_OkFocus(This,newVal)

#define IGenericPanel_GetActiveItem(This,pGenericPanelItem,ppGenericPanel)	\
    (This)->lpVtbl -> GetActiveItem(This,pGenericPanelItem,ppGenericPanel)

#define IGenericPanel_get_ItemObject(This,varItem,ppObject)	\
    (This)->lpVtbl -> get_ItemObject(This,varItem,ppObject)

#define IGenericPanel_DeleteItem(This,varItem)	\
    (This)->lpVtbl -> DeleteItem(This,varItem)

#define IGenericPanel_get_RootSplitterWnd(This,ppSplitterWnd)	\
    (This)->lpVtbl -> get_RootSplitterWnd(This,ppSplitterWnd)

#define IGenericPanel_get_RootWindowMuch(This,ppWindowMuch)	\
    (This)->lpVtbl -> get_RootWindowMuch(This,ppWindowMuch)

#define IGenericPanel_get_SplitterCellThis(This,ppSplitterCell)	\
    (This)->lpVtbl -> get_SplitterCellThis(This,ppSplitterCell)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_CommandBars_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ IUnknown **ppCommandBars);


void __RPC_STUB IGenericPanel_get_CommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Title_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ BSTR *pbstrTitleName);


void __RPC_STUB IGenericPanel_get_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_Title_Proxy( 
    IGenericPanel * This,
    /* [in] */ BSTR bstrTitleName);


void __RPC_STUB IGenericPanel_put_Title_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_CreateGenericPanel_Proxy( 
    IGenericPanel * This,
    LONG_PTR hWndParent,
    BSTR bstrTitle,
    LONG x,
    LONG y,
    LONG nWidth,
    LONG nHeight,
    VARIANT_BOOL bFloating,
    enumGenericPanelStyle genericPanelStyle);


void __RPC_STUB IGenericPanel_CreateGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_ShowWindow_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ VARIANT_BOOL *bpShow);


void __RPC_STUB IGenericPanel_get_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_ShowWindow_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT_BOOL bShow);


void __RPC_STUB IGenericPanel_put_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_CreateCommandBars_Proxy( 
    IGenericPanel * This,
    BSTR strTittle,
    long umCommandBarsType,
    /* [retval][out] */ long **pCommandBars);


void __RPC_STUB IGenericPanel_CreateCommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Floating_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IGenericPanel_get_Floating_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_Floating_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IGenericPanel_put_Floating_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Transparence_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ VARIANT_BOOL *bpTransparence);


void __RPC_STUB IGenericPanel_get_Transparence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_Transparence_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT_BOOL bTransparence);


void __RPC_STUB IGenericPanel_put_Transparence_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_SetWindowPos_Proxy( 
    IGenericPanel * This,
    int X,
    int Y,
    int cx,
    int cy,
    UINT uFlags);


void __RPC_STUB IGenericPanel_SetWindowPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_GetWindowPos_Proxy( 
    IGenericPanel * This,
    int *x,
    int *y,
    int *x1,
    int *y1);


void __RPC_STUB IGenericPanel_GetWindowPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_UpdateLayout_Proxy( 
    IGenericPanel * This);


void __RPC_STUB IGenericPanel_UpdateLayout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_InsertItem_Proxy( 
    IGenericPanel * This,
    int nIndex,
    BSTR bstrTitle,
    HICON hIcon,
    /* [in] */ IWindowPanel *pWindowPanel,
    /* [out] */ IGenericPanel **ppgp,
    /* [out] */ IGenericPanelItem **pPageItem,
    VARIANT_BOOL bActive,
    enumGenericPanelStyle genericPanelStyle,
    long *pvObject);


void __RPC_STUB IGenericPanel_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_InsertItemComControl_Proxy( 
    IGenericPanel * This,
    int nIndex,
    BSTR strName,
    BSTR lpszComControlName,
    IStream *pStream,
    IUnknown **ppUnkContainer,
    IUnknown **ppUnkControl,
    /* [out] */ IGenericPanelItem **pPageItem,
    VARIANT_BOOL bActive);


void __RPC_STUB IGenericPanel_InsertItemComControl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_NotifyGenericPanel_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ IGenericPanelNotify **pNotifyGenericPanel);


void __RPC_STUB IGenericPanel_get_NotifyGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_NotifyGenericPanel_Proxy( 
    IGenericPanel * This,
    /* [in] */ IGenericPanelNotify *pNotifyGenericPanel);


void __RPC_STUB IGenericPanel_put_NotifyGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_TabCtrlPlace_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ enumWindowPlace *winPlace);


void __RPC_STUB IGenericPanel_get_TabCtrlPlace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_TabCtrlPlace_Proxy( 
    IGenericPanel * This,
    /* [in] */ enumWindowPlace winPlace);


void __RPC_STUB IGenericPanel_put_TabCtrlPlace_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Name_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ BSTR *pbsName);


void __RPC_STUB IGenericPanel_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_Name_Proxy( 
    IGenericPanel * This,
    /* [in] */ BSTR bsName);


void __RPC_STUB IGenericPanel_put_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_ShowCaption_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ VARIANT_BOOL *pbShow);


void __RPC_STUB IGenericPanel_get_ShowCaption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_ShowCaption_Proxy( 
    IGenericPanel * This,
    VARIANT_BOOL bShow);


void __RPC_STUB IGenericPanel_put_ShowCaption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_SetActivePage_Proxy( 
    IGenericPanel * This,
    VARIANT *vtItem);


void __RPC_STUB IGenericPanel_SetActivePage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_GetNotify_Proxy( 
    IGenericPanel * This,
    ULONG **ppNotify);


void __RPC_STUB IGenericPanel_GetNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_InsertItemPage_Proxy( 
    IGenericPanel * This,
    int nIndex,
    BSTR bstrTitle,
    HICON hIcon,
    /* [in] */ IWindowPanel *pWindowPanel,
    /* [out] */ IGenericPanel **ppgp,
    /* [out] */ IGenericPanelItem **pPageItem,
    VARIANT_BOOL bActive,
    enumGenericPanelStyle genericPanelStyle,
    VARIANT_BOOL bFixed,
    VARIANT_BOOL bRoll,
    long *pvObject);


void __RPC_STUB IGenericPanel_InsertItemPage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_LoadXml_Proxy( 
    IGenericPanel * This,
    long *pModule,
    long *hXmlConfig,
    unsigned char *pwsXmlPath);


void __RPC_STUB IGenericPanel_LoadXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Count_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ long *pVal);


void __RPC_STUB IGenericPanel_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_Item_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ IGenericPanelItem **ppPanelItem);


void __RPC_STUB IGenericPanel_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_CreateSplitterWnd_Proxy( 
    IGenericPanel * This,
    ISplitterWnd **ppSplitterWnd);


void __RPC_STUB IGenericPanel_CreateSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_SplitterWnd_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ ISplitterWnd **ppSplitterWnd);


void __RPC_STUB IGenericPanel_get_SplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_OkFocus_Proxy( 
    IGenericPanel * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IGenericPanel_get_OkFocus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IGenericPanel_put_OkFocus_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IGenericPanel_put_OkFocus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_GetActiveItem_Proxy( 
    IGenericPanel * This,
    IGenericPanelItem **pGenericPanelItem,
    IGenericPanel **ppGenericPanel);


void __RPC_STUB IGenericPanel_GetActiveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_ItemObject_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ LONG_PTR **ppObject);


void __RPC_STUB IGenericPanel_get_ItemObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanel_DeleteItem_Proxy( 
    IGenericPanel * This,
    /* [in] */ VARIANT varItem);


void __RPC_STUB IGenericPanel_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_RootSplitterWnd_Proxy( 
    IGenericPanel * This,
    ISplitterWnd **ppSplitterWnd);


void __RPC_STUB IGenericPanel_get_RootSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_RootWindowMuch_Proxy( 
    IGenericPanel * This,
    ULONG **ppWindowMuch);


void __RPC_STUB IGenericPanel_get_RootWindowMuch_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IGenericPanel_get_SplitterCellThis_Proxy( 
    IGenericPanel * This,
    ISplitterCell **ppSplitterCell);


void __RPC_STUB IGenericPanel_get_SplitterCellThis_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGenericPanel_INTERFACE_DEFINED__ */


#ifndef __IGenericPanels_INTERFACE_DEFINED__
#define __IGenericPanels_INTERFACE_DEFINED__

/* interface IGenericPanels */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IGenericPanels;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9F9FF873-FCBE-435D-A453-DF65310B8FD9")
    IGenericPanels : public IDispatch
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IGenericPanel **pCommandItem) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_ActiveGenericPanel( 
            /* [retval][out] */ IGenericPanel **ppGenericPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            BSTR bsTitle) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindItem( 
            BSTR bsTitle,
            IGenericPanel **pgp) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActiveGenericPanel( 
            BSTR bsTitle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IGenericPanelsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IGenericPanels * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IGenericPanels * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IGenericPanels * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IGenericPanels * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IGenericPanels * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IGenericPanels * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IGenericPanels * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IGenericPanels * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            IGenericPanels * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IGenericPanel **pCommandItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IGenericPanels * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_ActiveGenericPanel )( 
            IGenericPanels * This,
            /* [retval][out] */ IGenericPanel **ppGenericPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            IGenericPanels * This,
            BSTR bsTitle);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindItem )( 
            IGenericPanels * This,
            BSTR bsTitle,
            IGenericPanel **pgp);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActiveGenericPanel )( 
            IGenericPanels * This,
            BSTR bsTitle);
        
        END_INTERFACE
    } IGenericPanelsVtbl;

    interface IGenericPanels
    {
        CONST_VTBL struct IGenericPanelsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IGenericPanels_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IGenericPanels_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IGenericPanels_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IGenericPanels_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IGenericPanels_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IGenericPanels_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IGenericPanels_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IGenericPanels_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define IGenericPanels_get_Item(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,pCommandItem)

#define IGenericPanels_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define IGenericPanels_get_ActiveGenericPanel(This,ppGenericPanel)	\
    (This)->lpVtbl -> get_ActiveGenericPanel(This,ppGenericPanel)

#define IGenericPanels_DeleteItem(This,bsTitle)	\
    (This)->lpVtbl -> DeleteItem(This,bsTitle)

#define IGenericPanels_FindItem(This,bsTitle,pgp)	\
    (This)->lpVtbl -> FindItem(This,bsTitle,pgp)

#define IGenericPanels_SetActiveGenericPanel(This,bsTitle)	\
    (This)->lpVtbl -> SetActiveGenericPanel(This,bsTitle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [propget][id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_get_Count_Proxy( 
    IGenericPanels * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB IGenericPanels_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_get_Item_Proxy( 
    IGenericPanels * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ IGenericPanel **pCommandItem);


void __RPC_STUB IGenericPanels_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_get__NewEnum_Proxy( 
    IGenericPanels * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB IGenericPanels_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_get_ActiveGenericPanel_Proxy( 
    IGenericPanels * This,
    /* [retval][out] */ IGenericPanel **ppGenericPanel);


void __RPC_STUB IGenericPanels_get_ActiveGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_DeleteItem_Proxy( 
    IGenericPanels * This,
    BSTR bsTitle);


void __RPC_STUB IGenericPanels_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_FindItem_Proxy( 
    IGenericPanels * This,
    BSTR bsTitle,
    IGenericPanel **pgp);


void __RPC_STUB IGenericPanels_FindItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IGenericPanels_SetActiveGenericPanel_Proxy( 
    IGenericPanels * This,
    BSTR bsTitle);


void __RPC_STUB IGenericPanels_SetActiveGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IGenericPanels_INTERFACE_DEFINED__ */


#ifndef __ITaskManager_INTERFACE_DEFINED__
#define __ITaskManager_INTERFACE_DEFINED__

/* interface ITaskManager */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITaskManager;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7BD32780-8360-4374-B381-19DC8FF6F17C")
    ITaskManager : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateTaskItem( 
            BSTR ProgID,
            BSTR bstrTiTle,
            /* [retval][out] */ IDispatch **pGenericPanelItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ VARIANT *varItem) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_GenericPanel( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateTaskManager( 
            LONG_PTR hWndCtrl,
            BSTR bstrCaption) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowWindow( 
            /* [retval][out] */ VARIANT_BOOL *pbShow) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowWindow( 
            /* [in] */ VARIANT_BOOL bShow) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITaskManagerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITaskManager * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITaskManager * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITaskManager * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITaskManager * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITaskManager * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITaskManager * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITaskManager * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateTaskItem )( 
            ITaskManager * This,
            BSTR ProgID,
            BSTR bstrTiTle,
            /* [retval][out] */ IDispatch **pGenericPanelItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            ITaskManager * This,
            /* [in] */ VARIANT *varItem);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GenericPanel )( 
            ITaskManager * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateTaskManager )( 
            ITaskManager * This,
            LONG_PTR hWndCtrl,
            BSTR bstrCaption);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowWindow )( 
            ITaskManager * This,
            /* [retval][out] */ VARIANT_BOOL *pbShow);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowWindow )( 
            ITaskManager * This,
            /* [in] */ VARIANT_BOOL bShow);
        
        END_INTERFACE
    } ITaskManagerVtbl;

    interface ITaskManager
    {
        CONST_VTBL struct ITaskManagerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITaskManager_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITaskManager_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITaskManager_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITaskManager_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITaskManager_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITaskManager_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITaskManager_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITaskManager_CreateTaskItem(This,ProgID,bstrTiTle,pGenericPanelItem)	\
    (This)->lpVtbl -> CreateTaskItem(This,ProgID,bstrTiTle,pGenericPanelItem)

#define ITaskManager_DeleteItem(This,varItem)	\
    (This)->lpVtbl -> DeleteItem(This,varItem)

#define ITaskManager_get_GenericPanel(This,pVal)	\
    (This)->lpVtbl -> get_GenericPanel(This,pVal)

#define ITaskManager_CreateTaskManager(This,hWndCtrl,bstrCaption)	\
    (This)->lpVtbl -> CreateTaskManager(This,hWndCtrl,bstrCaption)

#define ITaskManager_get_ShowWindow(This,pbShow)	\
    (This)->lpVtbl -> get_ShowWindow(This,pbShow)

#define ITaskManager_put_ShowWindow(This,bShow)	\
    (This)->lpVtbl -> put_ShowWindow(This,bShow)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ITaskManager_CreateTaskItem_Proxy( 
    ITaskManager * This,
    BSTR ProgID,
    BSTR bstrTiTle,
    /* [retval][out] */ IDispatch **pGenericPanelItem);


void __RPC_STUB ITaskManager_CreateTaskItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITaskManager_DeleteItem_Proxy( 
    ITaskManager * This,
    /* [in] */ VARIANT *varItem);


void __RPC_STUB ITaskManager_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITaskManager_get_GenericPanel_Proxy( 
    ITaskManager * This,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB ITaskManager_get_GenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITaskManager_CreateTaskManager_Proxy( 
    ITaskManager * This,
    LONG_PTR hWndCtrl,
    BSTR bstrCaption);


void __RPC_STUB ITaskManager_CreateTaskManager_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITaskManager_get_ShowWindow_Proxy( 
    ITaskManager * This,
    /* [retval][out] */ VARIANT_BOOL *pbShow);


void __RPC_STUB ITaskManager_get_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITaskManager_put_ShowWindow_Proxy( 
    ITaskManager * This,
    /* [in] */ VARIANT_BOOL bShow);


void __RPC_STUB ITaskManager_put_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITaskManager_INTERFACE_DEFINED__ */


#ifndef __ISplitterWnd_INTERFACE_DEFINED__
#define __ISplitterWnd_INTERFACE_DEFINED__

/* interface ISplitterWnd */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISplitterWnd;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("562966C8-1EE1-4825-BE2B-087FF07B13BE")
    ISplitterWnd : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSplitterWnd( 
            ULONG *pWindowMuch,
            /* [in] */ LONG_PTR hWnd,
            /* [in] */ long nLeft,
            /* [in] */ long __MIDL_0011,
            /* [in] */ long nRight,
            /* [in] */ long nButtom) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MoveWnd( 
            /* [in] */ long nLeft,
            /* [in] */ long nTop,
            /* [in] */ long nRight,
            /* [in] */ long nButtom) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateLayout( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSplitterItem( 
            enumSplitterType swt,
            enumResizePos fmrp,
            enumCellType umCellType,
            /* [in] */ long nWidth,
            /* [out] */ ISplitterCell **ppLeft,
            DWORD dwLeftPanelStyle,
            /* [out] */ IGenericPanel **ppLeftPanel,
            /* [out] */ ISplitterCell **ppRight,
            DWORD dwRightPanelStyle,
            /* [out] */ IGenericPanel **ppRightGenericPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActiveCell( 
            /* [in] */ VARIANT varActiveCell) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveCell( 
            BSTR bsName,
            enumRemoveCell fmrc) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWnd( 
            ULONG *phWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LoadSplitterWnd( 
            IUnknown *pModule,
            ULONG *hXmlConfig,
            wchar_t *pszXmlPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseCell( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_SplitterWidth( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_SplitterWidth( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowDragging( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowDragging( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommands( 
            /* [retval][out] */ long **ppNotifyCommands) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetActiveCell( 
            /* [out] */ ISplitterCell **ppActiveCell,
            /* [out] */ IGenericPanel **ppGenericPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_LeftCell( 
            ISplitterCell **ppLeftCell) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_RightCell( 
            ISplitterCell **ppRightCell) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ISplitterCell **ppSplitterCell) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowWindow( 
            /* [retval][out] */ VARIANT_BOOL *bpShow) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowWindow( 
            /* [in] */ VARIANT_BOOL bShow) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_LSplitterWnd( 
            ULONG *pSplitterWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWindowMuch( 
            ULONG **pWindowMuch) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISplitterWndVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISplitterWnd * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISplitterWnd * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISplitterWnd * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISplitterWnd * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISplitterWnd * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISplitterWnd * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISplitterWnd * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSplitterWnd )( 
            ISplitterWnd * This,
            ULONG *pWindowMuch,
            /* [in] */ LONG_PTR hWnd,
            /* [in] */ long nLeft,
            /* [in] */ long __MIDL_0011,
            /* [in] */ long nRight,
            /* [in] */ long nButtom);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MoveWnd )( 
            ISplitterWnd * This,
            /* [in] */ long nLeft,
            /* [in] */ long nTop,
            /* [in] */ long nRight,
            /* [in] */ long nButtom);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateLayout )( 
            ISplitterWnd * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSplitterItem )( 
            ISplitterWnd * This,
            enumSplitterType swt,
            enumResizePos fmrp,
            enumCellType umCellType,
            /* [in] */ long nWidth,
            /* [out] */ ISplitterCell **ppLeft,
            DWORD dwLeftPanelStyle,
            /* [out] */ IGenericPanel **ppLeftPanel,
            /* [out] */ ISplitterCell **ppRight,
            DWORD dwRightPanelStyle,
            /* [out] */ IGenericPanel **ppRightGenericPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActiveCell )( 
            ISplitterWnd * This,
            /* [in] */ VARIANT varActiveCell);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveCell )( 
            ISplitterWnd * This,
            BSTR bsName,
            enumRemoveCell fmrc);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWnd )( 
            ISplitterWnd * This,
            ULONG *phWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LoadSplitterWnd )( 
            ISplitterWnd * This,
            IUnknown *pModule,
            ULONG *hXmlConfig,
            wchar_t *pszXmlPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseCell )( 
            ISplitterWnd * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SplitterWidth )( 
            ISplitterWnd * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_SplitterWidth )( 
            ISplitterWnd * This,
            /* [in] */ LONG newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowDragging )( 
            ISplitterWnd * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowDragging )( 
            ISplitterWnd * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommands )( 
            ISplitterWnd * This,
            /* [retval][out] */ long **ppNotifyCommands);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetActiveCell )( 
            ISplitterWnd * This,
            /* [out] */ ISplitterCell **ppActiveCell,
            /* [out] */ IGenericPanel **ppGenericPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LeftCell )( 
            ISplitterWnd * This,
            ISplitterCell **ppLeftCell);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RightCell )( 
            ISplitterWnd * This,
            ISplitterCell **ppRightCell);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ISplitterWnd * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            ISplitterWnd * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ISplitterCell **ppSplitterCell);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            ISplitterWnd * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowWindow )( 
            ISplitterWnd * This,
            /* [retval][out] */ VARIANT_BOOL *bpShow);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowWindow )( 
            ISplitterWnd * This,
            /* [in] */ VARIANT_BOOL bShow);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LSplitterWnd )( 
            ISplitterWnd * This,
            ULONG *pSplitterWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWindowMuch )( 
            ISplitterWnd * This,
            ULONG **pWindowMuch);
        
        END_INTERFACE
    } ISplitterWndVtbl;

    interface ISplitterWnd
    {
        CONST_VTBL struct ISplitterWndVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISplitterWnd_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISplitterWnd_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISplitterWnd_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISplitterWnd_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISplitterWnd_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISplitterWnd_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISplitterWnd_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISplitterWnd_CreateSplitterWnd(This,pWindowMuch,hWnd,nLeft,__MIDL_0011,nRight,nButtom)	\
    (This)->lpVtbl -> CreateSplitterWnd(This,pWindowMuch,hWnd,nLeft,__MIDL_0011,nRight,nButtom)

#define ISplitterWnd_MoveWnd(This,nLeft,nTop,nRight,nButtom)	\
    (This)->lpVtbl -> MoveWnd(This,nLeft,nTop,nRight,nButtom)

#define ISplitterWnd_UpdateLayout(This)	\
    (This)->lpVtbl -> UpdateLayout(This)

#define ISplitterWnd_CreateSplitterItem(This,swt,fmrp,umCellType,nWidth,ppLeft,dwLeftPanelStyle,ppLeftPanel,ppRight,dwRightPanelStyle,ppRightGenericPanel)	\
    (This)->lpVtbl -> CreateSplitterItem(This,swt,fmrp,umCellType,nWidth,ppLeft,dwLeftPanelStyle,ppLeftPanel,ppRight,dwRightPanelStyle,ppRightGenericPanel)

#define ISplitterWnd_SetActiveCell(This,varActiveCell)	\
    (This)->lpVtbl -> SetActiveCell(This,varActiveCell)

#define ISplitterWnd_RemoveCell(This,bsName,fmrc)	\
    (This)->lpVtbl -> RemoveCell(This,bsName,fmrc)

#define ISplitterWnd_GetWnd(This,phWnd)	\
    (This)->lpVtbl -> GetWnd(This,phWnd)

#define ISplitterWnd_LoadSplitterWnd(This,pModule,hXmlConfig,pszXmlPath)	\
    (This)->lpVtbl -> LoadSplitterWnd(This,pModule,hXmlConfig,pszXmlPath)

#define ISplitterWnd_CloseCell(This)	\
    (This)->lpVtbl -> CloseCell(This)

#define ISplitterWnd_get_SplitterWidth(This,pVal)	\
    (This)->lpVtbl -> get_SplitterWidth(This,pVal)

#define ISplitterWnd_put_SplitterWidth(This,newVal)	\
    (This)->lpVtbl -> put_SplitterWidth(This,newVal)

#define ISplitterWnd_get_ShowDragging(This,pVal)	\
    (This)->lpVtbl -> get_ShowDragging(This,pVal)

#define ISplitterWnd_put_ShowDragging(This,newVal)	\
    (This)->lpVtbl -> put_ShowDragging(This,newVal)

#define ISplitterWnd_get_NotifyCommands(This,ppNotifyCommands)	\
    (This)->lpVtbl -> get_NotifyCommands(This,ppNotifyCommands)

#define ISplitterWnd_GetActiveCell(This,ppActiveCell,ppGenericPanel)	\
    (This)->lpVtbl -> GetActiveCell(This,ppActiveCell,ppGenericPanel)

#define ISplitterWnd_get_LeftCell(This,ppLeftCell)	\
    (This)->lpVtbl -> get_LeftCell(This,ppLeftCell)

#define ISplitterWnd_get_RightCell(This,ppRightCell)	\
    (This)->lpVtbl -> get_RightCell(This,ppRightCell)

#define ISplitterWnd_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define ISplitterWnd_get_Item(This,varItem,ppSplitterCell)	\
    (This)->lpVtbl -> get_Item(This,varItem,ppSplitterCell)

#define ISplitterWnd_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define ISplitterWnd_get_ShowWindow(This,bpShow)	\
    (This)->lpVtbl -> get_ShowWindow(This,bpShow)

#define ISplitterWnd_put_ShowWindow(This,bShow)	\
    (This)->lpVtbl -> put_ShowWindow(This,bShow)

#define ISplitterWnd_put_LSplitterWnd(This,pSplitterWnd)	\
    (This)->lpVtbl -> put_LSplitterWnd(This,pSplitterWnd)

#define ISplitterWnd_GetWindowMuch(This,pWindowMuch)	\
    (This)->lpVtbl -> GetWindowMuch(This,pWindowMuch)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_CreateSplitterWnd_Proxy( 
    ISplitterWnd * This,
    ULONG *pWindowMuch,
    /* [in] */ LONG_PTR hWnd,
    /* [in] */ long nLeft,
    /* [in] */ long __MIDL_0011,
    /* [in] */ long nRight,
    /* [in] */ long nButtom);


void __RPC_STUB ISplitterWnd_CreateSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_MoveWnd_Proxy( 
    ISplitterWnd * This,
    /* [in] */ long nLeft,
    /* [in] */ long nTop,
    /* [in] */ long nRight,
    /* [in] */ long nButtom);


void __RPC_STUB ISplitterWnd_MoveWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_UpdateLayout_Proxy( 
    ISplitterWnd * This);


void __RPC_STUB ISplitterWnd_UpdateLayout_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_CreateSplitterItem_Proxy( 
    ISplitterWnd * This,
    enumSplitterType swt,
    enumResizePos fmrp,
    enumCellType umCellType,
    /* [in] */ long nWidth,
    /* [out] */ ISplitterCell **ppLeft,
    DWORD dwLeftPanelStyle,
    /* [out] */ IGenericPanel **ppLeftPanel,
    /* [out] */ ISplitterCell **ppRight,
    DWORD dwRightPanelStyle,
    /* [out] */ IGenericPanel **ppRightGenericPanel);


void __RPC_STUB ISplitterWnd_CreateSplitterItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_SetActiveCell_Proxy( 
    ISplitterWnd * This,
    /* [in] */ VARIANT varActiveCell);


void __RPC_STUB ISplitterWnd_SetActiveCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_RemoveCell_Proxy( 
    ISplitterWnd * This,
    BSTR bsName,
    enumRemoveCell fmrc);


void __RPC_STUB ISplitterWnd_RemoveCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_GetWnd_Proxy( 
    ISplitterWnd * This,
    ULONG *phWnd);


void __RPC_STUB ISplitterWnd_GetWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_LoadSplitterWnd_Proxy( 
    ISplitterWnd * This,
    IUnknown *pModule,
    ULONG *hXmlConfig,
    wchar_t *pszXmlPath);


void __RPC_STUB ISplitterWnd_LoadSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_CloseCell_Proxy( 
    ISplitterWnd * This);


void __RPC_STUB ISplitterWnd_CloseCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_SplitterWidth_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB ISplitterWnd_get_SplitterWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_put_SplitterWidth_Proxy( 
    ISplitterWnd * This,
    /* [in] */ LONG newVal);


void __RPC_STUB ISplitterWnd_put_SplitterWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_ShowDragging_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ISplitterWnd_get_ShowDragging_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_put_ShowDragging_Proxy( 
    ISplitterWnd * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ISplitterWnd_put_ShowDragging_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_NotifyCommands_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ long **ppNotifyCommands);


void __RPC_STUB ISplitterWnd_get_NotifyCommands_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_GetActiveCell_Proxy( 
    ISplitterWnd * This,
    /* [out] */ ISplitterCell **ppActiveCell,
    /* [out] */ IGenericPanel **ppGenericPanel);


void __RPC_STUB ISplitterWnd_GetActiveCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_LeftCell_Proxy( 
    ISplitterWnd * This,
    ISplitterCell **ppLeftCell);


void __RPC_STUB ISplitterWnd_get_LeftCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_RightCell_Proxy( 
    ISplitterWnd * This,
    ISplitterCell **ppRightCell);


void __RPC_STUB ISplitterWnd_get_RightCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_Count_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB ISplitterWnd_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_Item_Proxy( 
    ISplitterWnd * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ ISplitterCell **ppSplitterCell);


void __RPC_STUB ISplitterWnd_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get__NewEnum_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB ISplitterWnd_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_get_ShowWindow_Proxy( 
    ISplitterWnd * This,
    /* [retval][out] */ VARIANT_BOOL *bpShow);


void __RPC_STUB ISplitterWnd_get_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_put_ShowWindow_Proxy( 
    ISplitterWnd * This,
    /* [in] */ VARIANT_BOOL bShow);


void __RPC_STUB ISplitterWnd_put_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_put_LSplitterWnd_Proxy( 
    ISplitterWnd * This,
    ULONG *pSplitterWnd);


void __RPC_STUB ISplitterWnd_put_LSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterWnd_GetWindowMuch_Proxy( 
    ISplitterWnd * This,
    ULONG **pWindowMuch);


void __RPC_STUB ISplitterWnd_GetWindowMuch_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISplitterWnd_INTERFACE_DEFINED__ */


#ifndef __ISplitterCell_INTERFACE_DEFINED__
#define __ISplitterCell_INTERFACE_DEFINED__

/* interface ISplitterCell */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISplitterCell;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3C32CE26-80BC-485B-A1D7-AAA638924151")
    ISplitterCell : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertSplitterCell( 
            enumCellType umCellTypeIns,
            enumSplitterType swt,
            enumResizePos fmrp,
            enumCellType cellType,
            /* [in] */ long nWidth,
            /* [out] */ ISplitterCell **ppCellLeft,
            DWORD dwLeftPanel,
            /* [out] */ IGenericPanel **ppgpLeft,
            /* [out] */ ISplitterCell **ppCellRight,
            DWORD dwRightPanel,
            /* [out] */ IGenericPanel **ppgpRight) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR newName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowTitle( 
            /* [retval][out] */ VARIANT_BOOL *pbShowTitle) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowTitle( 
            /* [in] */ VARIANT_BOOL bShowTitle) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Pos( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Pos( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ResizePos( 
            /* [retval][out] */ enumResizePos *pRePos) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ResizePos( 
            /* [in] */ enumResizePos RePos) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_GenericPanel( 
            /* [retval][out] */ IGenericPanel **pGenericPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_LeftCell( 
            ISplitterCell **ppLeftCell) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_RightCell( 
            ISplitterCell **ppRightCell) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_SplitterWnd( 
            ISplitterWnd **ppSplitterWnd) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISplitterCellVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISplitterCell * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISplitterCell * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISplitterCell * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISplitterCell * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISplitterCell * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISplitterCell * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISplitterCell * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertSplitterCell )( 
            ISplitterCell * This,
            enumCellType umCellTypeIns,
            enumSplitterType swt,
            enumResizePos fmrp,
            enumCellType cellType,
            /* [in] */ long nWidth,
            /* [out] */ ISplitterCell **ppCellLeft,
            DWORD dwLeftPanel,
            /* [out] */ IGenericPanel **ppgpLeft,
            /* [out] */ ISplitterCell **ppCellRight,
            DWORD dwRightPanel,
            /* [out] */ IGenericPanel **ppgpRight);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            ISplitterCell * This,
            /* [retval][out] */ BSTR *pName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            ISplitterCell * This,
            /* [in] */ BSTR newName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowTitle )( 
            ISplitterCell * This,
            /* [retval][out] */ VARIANT_BOOL *pbShowTitle);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowTitle )( 
            ISplitterCell * This,
            /* [in] */ VARIANT_BOOL bShowTitle);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Pos )( 
            ISplitterCell * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Pos )( 
            ISplitterCell * This,
            /* [in] */ LONG newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ResizePos )( 
            ISplitterCell * This,
            /* [retval][out] */ enumResizePos *pRePos);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ResizePos )( 
            ISplitterCell * This,
            /* [in] */ enumResizePos RePos);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GenericPanel )( 
            ISplitterCell * This,
            /* [retval][out] */ IGenericPanel **pGenericPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LeftCell )( 
            ISplitterCell * This,
            ISplitterCell **ppLeftCell);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_RightCell )( 
            ISplitterCell * This,
            ISplitterCell **ppRightCell);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SplitterWnd )( 
            ISplitterCell * This,
            ISplitterWnd **ppSplitterWnd);
        
        END_INTERFACE
    } ISplitterCellVtbl;

    interface ISplitterCell
    {
        CONST_VTBL struct ISplitterCellVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISplitterCell_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISplitterCell_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISplitterCell_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISplitterCell_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISplitterCell_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISplitterCell_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISplitterCell_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISplitterCell_InsertSplitterCell(This,umCellTypeIns,swt,fmrp,cellType,nWidth,ppCellLeft,dwLeftPanel,ppgpLeft,ppCellRight,dwRightPanel,ppgpRight)	\
    (This)->lpVtbl -> InsertSplitterCell(This,umCellTypeIns,swt,fmrp,cellType,nWidth,ppCellLeft,dwLeftPanel,ppgpLeft,ppCellRight,dwRightPanel,ppgpRight)

#define ISplitterCell_get_Name(This,pName)	\
    (This)->lpVtbl -> get_Name(This,pName)

#define ISplitterCell_put_Name(This,newName)	\
    (This)->lpVtbl -> put_Name(This,newName)

#define ISplitterCell_get_ShowTitle(This,pbShowTitle)	\
    (This)->lpVtbl -> get_ShowTitle(This,pbShowTitle)

#define ISplitterCell_put_ShowTitle(This,bShowTitle)	\
    (This)->lpVtbl -> put_ShowTitle(This,bShowTitle)

#define ISplitterCell_get_Pos(This,pVal)	\
    (This)->lpVtbl -> get_Pos(This,pVal)

#define ISplitterCell_put_Pos(This,newVal)	\
    (This)->lpVtbl -> put_Pos(This,newVal)

#define ISplitterCell_get_ResizePos(This,pRePos)	\
    (This)->lpVtbl -> get_ResizePos(This,pRePos)

#define ISplitterCell_put_ResizePos(This,RePos)	\
    (This)->lpVtbl -> put_ResizePos(This,RePos)

#define ISplitterCell_get_GenericPanel(This,pGenericPanel)	\
    (This)->lpVtbl -> get_GenericPanel(This,pGenericPanel)

#define ISplitterCell_get_LeftCell(This,ppLeftCell)	\
    (This)->lpVtbl -> get_LeftCell(This,ppLeftCell)

#define ISplitterCell_get_RightCell(This,ppRightCell)	\
    (This)->lpVtbl -> get_RightCell(This,ppRightCell)

#define ISplitterCell_get_SplitterWnd(This,ppSplitterWnd)	\
    (This)->lpVtbl -> get_SplitterWnd(This,ppSplitterWnd)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ISplitterCell_InsertSplitterCell_Proxy( 
    ISplitterCell * This,
    enumCellType umCellTypeIns,
    enumSplitterType swt,
    enumResizePos fmrp,
    enumCellType cellType,
    /* [in] */ long nWidth,
    /* [out] */ ISplitterCell **ppCellLeft,
    DWORD dwLeftPanel,
    /* [out] */ IGenericPanel **ppgpLeft,
    /* [out] */ ISplitterCell **ppCellRight,
    DWORD dwRightPanel,
    /* [out] */ IGenericPanel **ppgpRight);


void __RPC_STUB ISplitterCell_InsertSplitterCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_Name_Proxy( 
    ISplitterCell * This,
    /* [retval][out] */ BSTR *pName);


void __RPC_STUB ISplitterCell_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterCell_put_Name_Proxy( 
    ISplitterCell * This,
    /* [in] */ BSTR newName);


void __RPC_STUB ISplitterCell_put_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_ShowTitle_Proxy( 
    ISplitterCell * This,
    /* [retval][out] */ VARIANT_BOOL *pbShowTitle);


void __RPC_STUB ISplitterCell_get_ShowTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterCell_put_ShowTitle_Proxy( 
    ISplitterCell * This,
    /* [in] */ VARIANT_BOOL bShowTitle);


void __RPC_STUB ISplitterCell_put_ShowTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_Pos_Proxy( 
    ISplitterCell * This,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB ISplitterCell_get_Pos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterCell_put_Pos_Proxy( 
    ISplitterCell * This,
    /* [in] */ LONG newVal);


void __RPC_STUB ISplitterCell_put_Pos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_ResizePos_Proxy( 
    ISplitterCell * This,
    /* [retval][out] */ enumResizePos *pRePos);


void __RPC_STUB ISplitterCell_get_ResizePos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ISplitterCell_put_ResizePos_Proxy( 
    ISplitterCell * This,
    /* [in] */ enumResizePos RePos);


void __RPC_STUB ISplitterCell_put_ResizePos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_GenericPanel_Proxy( 
    ISplitterCell * This,
    /* [retval][out] */ IGenericPanel **pGenericPanel);


void __RPC_STUB ISplitterCell_get_GenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_LeftCell_Proxy( 
    ISplitterCell * This,
    ISplitterCell **ppLeftCell);


void __RPC_STUB ISplitterCell_get_LeftCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_RightCell_Proxy( 
    ISplitterCell * This,
    ISplitterCell **ppRightCell);


void __RPC_STUB ISplitterCell_get_RightCell_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ISplitterCell_get_SplitterWnd_Proxy( 
    ISplitterCell * This,
    ISplitterWnd **ppSplitterWnd);


void __RPC_STUB ISplitterCell_get_SplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISplitterCell_INTERFACE_DEFINED__ */


#ifndef __ITabPageItem_INTERFACE_DEFINED__
#define __ITabPageItem_INTERFACE_DEFINED__

/* interface ITabPageItem */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITabPageItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E3802010-BC2A-4DA3-BF1E-8995364515D5")
    ITabPageItem : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Caption( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Caption( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Visible( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Visible( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_IconIndex( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_IconIndex( 
            /* [in] */ int newVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActive( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITabPageItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITabPageItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITabPageItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITabPageItem * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITabPageItem * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITabPageItem * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITabPageItem * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITabPageItem * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Caption )( 
            ITabPageItem * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Caption )( 
            ITabPageItem * This,
            /* [in] */ BSTR newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            ITabPageItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            ITabPageItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IconIndex )( 
            ITabPageItem * This,
            /* [retval][out] */ int *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IconIndex )( 
            ITabPageItem * This,
            /* [in] */ int newVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActive )( 
            ITabPageItem * This);
        
        END_INTERFACE
    } ITabPageItemVtbl;

    interface ITabPageItem
    {
        CONST_VTBL struct ITabPageItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITabPageItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITabPageItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITabPageItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITabPageItem_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITabPageItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITabPageItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITabPageItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITabPageItem_get_Caption(This,pVal)	\
    (This)->lpVtbl -> get_Caption(This,pVal)

#define ITabPageItem_put_Caption(This,newVal)	\
    (This)->lpVtbl -> put_Caption(This,newVal)

#define ITabPageItem_get_Visible(This,pVal)	\
    (This)->lpVtbl -> get_Visible(This,pVal)

#define ITabPageItem_put_Visible(This,newVal)	\
    (This)->lpVtbl -> put_Visible(This,newVal)

#define ITabPageItem_get_IconIndex(This,pVal)	\
    (This)->lpVtbl -> get_IconIndex(This,pVal)

#define ITabPageItem_put_IconIndex(This,newVal)	\
    (This)->lpVtbl -> put_IconIndex(This,newVal)

#define ITabPageItem_SetActive(This)	\
    (This)->lpVtbl -> SetActive(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITabPageItem_get_Caption_Proxy( 
    ITabPageItem * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITabPageItem_get_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITabPageItem_put_Caption_Proxy( 
    ITabPageItem * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITabPageItem_put_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITabPageItem_get_Visible_Proxy( 
    ITabPageItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ITabPageItem_get_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITabPageItem_put_Visible_Proxy( 
    ITabPageItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ITabPageItem_put_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITabPageItem_get_IconIndex_Proxy( 
    ITabPageItem * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB ITabPageItem_get_IconIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITabPageItem_put_IconIndex_Proxy( 
    ITabPageItem * This,
    /* [in] */ int newVal);


void __RPC_STUB ITabPageItem_put_IconIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITabPageItem_SetActive_Proxy( 
    ITabPageItem * This);


void __RPC_STUB ITabPageItem_SetActive_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITabPageItem_INTERFACE_DEFINED__ */


#ifndef __IStatusBar_INTERFACE_DEFINED__
#define __IStatusBar_INTERFACE_DEFINED__

/* interface IStatusBar */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IStatusBar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("370F01AE-E337-4F3A-95E7-3598FB4DE0E2")
    IStatusBar : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *nCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IStatusBarItem **ppStatusBarItem) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetPanes( 
            /* [in] */ StatusBarItemInfo *nArray,
            int nCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_LockAero( 
            /* [retval][out] */ VARIANT_BOOL *pLockAero) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_LockAero( 
            /* [in] */ VARIANT_BOOL newLockAero) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateProgressNotify( 
            VARIANT vtItem,
            LONG_PTR **pProgressNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteProgressNotify( 
            LONG_PTR **pProgressNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCaption( 
            int iPane,
            wchar_t *pszCaption) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetCaption( 
            int iPane,
            wchar_t *szCaption) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetEnable( 
            int iPane,
            VARIANT_BOOL *pbEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetEnable( 
            int iPane,
            VARIANT_BOOL bEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetIcon( 
            int iPane,
            HICON *phIcon) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetIcon( 
            int iPane,
            HICON hIcon) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWidth( 
            int iPane,
            int *pWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetWidth( 
            int iPane,
            int iWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetNotify( 
            ULONG **ppNotify) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStatusBarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStatusBar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStatusBar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStatusBar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStatusBar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStatusBar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStatusBar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStatusBar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IStatusBar * This,
            /* [retval][out] */ long *nCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            IStatusBar * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ IStatusBarItem **ppStatusBarItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IStatusBar * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetPanes )( 
            IStatusBar * This,
            /* [in] */ StatusBarItemInfo *nArray,
            int nCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LockAero )( 
            IStatusBar * This,
            /* [retval][out] */ VARIANT_BOOL *pLockAero);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_LockAero )( 
            IStatusBar * This,
            /* [in] */ VARIANT_BOOL newLockAero);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateProgressNotify )( 
            IStatusBar * This,
            VARIANT vtItem,
            LONG_PTR **pProgressNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteProgressNotify )( 
            IStatusBar * This,
            LONG_PTR **pProgressNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCaption )( 
            IStatusBar * This,
            int iPane,
            wchar_t *pszCaption);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetCaption )( 
            IStatusBar * This,
            int iPane,
            wchar_t *szCaption);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetEnable )( 
            IStatusBar * This,
            int iPane,
            VARIANT_BOOL *pbEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetEnable )( 
            IStatusBar * This,
            int iPane,
            VARIANT_BOOL bEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetIcon )( 
            IStatusBar * This,
            int iPane,
            HICON *phIcon);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetIcon )( 
            IStatusBar * This,
            int iPane,
            HICON hIcon);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWidth )( 
            IStatusBar * This,
            int iPane,
            int *pWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetWidth )( 
            IStatusBar * This,
            int iPane,
            int iWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetNotify )( 
            IStatusBar * This,
            ULONG **ppNotify);
        
        END_INTERFACE
    } IStatusBarVtbl;

    interface IStatusBar
    {
        CONST_VTBL struct IStatusBarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStatusBar_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IStatusBar_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IStatusBar_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IStatusBar_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IStatusBar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IStatusBar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IStatusBar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IStatusBar_get_Count(This,nCount)	\
    (This)->lpVtbl -> get_Count(This,nCount)

#define IStatusBar_get_Item(This,varItem,ppStatusBarItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,ppStatusBarItem)

#define IStatusBar_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define IStatusBar_SetPanes(This,nArray,nCount)	\
    (This)->lpVtbl -> SetPanes(This,nArray,nCount)

#define IStatusBar_get_LockAero(This,pLockAero)	\
    (This)->lpVtbl -> get_LockAero(This,pLockAero)

#define IStatusBar_put_LockAero(This,newLockAero)	\
    (This)->lpVtbl -> put_LockAero(This,newLockAero)

#define IStatusBar_CreateProgressNotify(This,vtItem,pProgressNotify)	\
    (This)->lpVtbl -> CreateProgressNotify(This,vtItem,pProgressNotify)

#define IStatusBar_DeleteProgressNotify(This,pProgressNotify)	\
    (This)->lpVtbl -> DeleteProgressNotify(This,pProgressNotify)

#define IStatusBar_GetCaption(This,iPane,pszCaption)	\
    (This)->lpVtbl -> GetCaption(This,iPane,pszCaption)

#define IStatusBar_SetCaption(This,iPane,szCaption)	\
    (This)->lpVtbl -> SetCaption(This,iPane,szCaption)

#define IStatusBar_GetEnable(This,iPane,pbEnable)	\
    (This)->lpVtbl -> GetEnable(This,iPane,pbEnable)

#define IStatusBar_SetEnable(This,iPane,bEnable)	\
    (This)->lpVtbl -> SetEnable(This,iPane,bEnable)

#define IStatusBar_GetIcon(This,iPane,phIcon)	\
    (This)->lpVtbl -> GetIcon(This,iPane,phIcon)

#define IStatusBar_SetIcon(This,iPane,hIcon)	\
    (This)->lpVtbl -> SetIcon(This,iPane,hIcon)

#define IStatusBar_GetWidth(This,iPane,pWidth)	\
    (This)->lpVtbl -> GetWidth(This,iPane,pWidth)

#define IStatusBar_SetWidth(This,iPane,iWidth)	\
    (This)->lpVtbl -> SetWidth(This,iPane,iWidth)

#define IStatusBar_GetNotify(This,ppNotify)	\
    (This)->lpVtbl -> GetNotify(This,ppNotify)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBar_get_Count_Proxy( 
    IStatusBar * This,
    /* [retval][out] */ long *nCount);


void __RPC_STUB IStatusBar_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBar_get_Item_Proxy( 
    IStatusBar * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ IStatusBarItem **ppStatusBarItem);


void __RPC_STUB IStatusBar_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IStatusBar_get__NewEnum_Proxy( 
    IStatusBar * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB IStatusBar_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_SetPanes_Proxy( 
    IStatusBar * This,
    /* [in] */ StatusBarItemInfo *nArray,
    int nCount);


void __RPC_STUB IStatusBar_SetPanes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBar_get_LockAero_Proxy( 
    IStatusBar * This,
    /* [retval][out] */ VARIANT_BOOL *pLockAero);


void __RPC_STUB IStatusBar_get_LockAero_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IStatusBar_put_LockAero_Proxy( 
    IStatusBar * This,
    /* [in] */ VARIANT_BOOL newLockAero);


void __RPC_STUB IStatusBar_put_LockAero_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_CreateProgressNotify_Proxy( 
    IStatusBar * This,
    VARIANT vtItem,
    LONG_PTR **pProgressNotify);


void __RPC_STUB IStatusBar_CreateProgressNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_DeleteProgressNotify_Proxy( 
    IStatusBar * This,
    LONG_PTR **pProgressNotify);


void __RPC_STUB IStatusBar_DeleteProgressNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_GetCaption_Proxy( 
    IStatusBar * This,
    int iPane,
    wchar_t *pszCaption);


void __RPC_STUB IStatusBar_GetCaption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_SetCaption_Proxy( 
    IStatusBar * This,
    int iPane,
    wchar_t *szCaption);


void __RPC_STUB IStatusBar_SetCaption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_GetEnable_Proxy( 
    IStatusBar * This,
    int iPane,
    VARIANT_BOOL *pbEnable);


void __RPC_STUB IStatusBar_GetEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_SetEnable_Proxy( 
    IStatusBar * This,
    int iPane,
    VARIANT_BOOL bEnable);


void __RPC_STUB IStatusBar_SetEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_GetIcon_Proxy( 
    IStatusBar * This,
    int iPane,
    HICON *phIcon);


void __RPC_STUB IStatusBar_GetIcon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_SetIcon_Proxy( 
    IStatusBar * This,
    int iPane,
    HICON hIcon);


void __RPC_STUB IStatusBar_SetIcon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_GetWidth_Proxy( 
    IStatusBar * This,
    int iPane,
    int *pWidth);


void __RPC_STUB IStatusBar_GetWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_SetWidth_Proxy( 
    IStatusBar * This,
    int iPane,
    int iWidth);


void __RPC_STUB IStatusBar_SetWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IStatusBar_GetNotify_Proxy( 
    IStatusBar * This,
    ULONG **ppNotify);


void __RPC_STUB IStatusBar_GetNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IStatusBar_INTERFACE_DEFINED__ */


#ifndef __IMainFrame_INTERFACE_DEFINED__
#define __IMainFrame_INTERFACE_DEFINED__

/* interface IMainFrame */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMainFrame;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C219367C-9CD9-4FA0-9DC0-7187E37F8A4A")
    IMainFrame : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetWorkRect( 
            /* [retval][out] */ long *pRect) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowWindow( 
            int nCmdShow) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoWindows( void) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Icon( 
            /* [in] */ LONG *Icon) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Icon( 
            /* [retval][out] */ LONG **hIcon) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBars( 
            /* [retval][out] */ IUnknown **ppCommandBars) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_StatusBar( 
            /* [retval][out] */ IStatusBar **pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MainFrameType( 
            /* [retval][out] */ enumMainFrameType *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MdiContainer( 
            /* [retval][out] */ IMdiContainer **pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_SplitterWnd( 
            /* [retval][out] */ ISplitterWnd **pSplitterWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateLayout2( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommand( 
            /* [retval][out] */ IUnknown **pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InstallNotifyCommand( 
            int emInstallEvent_,
            /* [in] */ IUnknown *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Close( void) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR strName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *strName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetAccelerators( 
            LONG_PTR hInstRes,
            UINT nResID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetHWND( 
            LONG *hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetMenu( 
            /* [in] */ LONG_PTR hMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMenu( 
            /* [in] */ LONG_PTR *hMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetPopupMenu( 
            UINT uiBodsyIndex,
            long umCommand,
            /* [retval][out] */ IUnknown **ppPopupMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateGuiState( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseWorkAdd( 
            ULONG *pWorkItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseWorkRemove( 
            ULONG *pWorkItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateMainFrame( 
            MAINFRAMEINFO *pMainFrameInfo) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateMainFrameXml( 
            long *pmodule,
            const wchar_t *psVxmlFileName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Caption( 
            /* [in] */ BSTR bstrCaption) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Caption( 
            /* [retval][out] */ BSTR *bstrCaption) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ModifyMdiToolBar( 
            ULONG ulRemove,
            ULONG ulAdd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoCommand( 
            /* [in] */ ULONG ulcmd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetMinValue( 
            int iWidth,
            int iHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowWindowValue( 
            int nCmdShow,
            int iWidth,
            int iHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetSDIWindowPanel( 
            BSTR bsName,
            BSTR bsTitle,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [in] */ IUnknown *pNotifyCommand,
            /* [in] */ long *pvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetXmlSDIWindowPanel( 
            /* [in] */ long *pmodule,
            /* [in] */ GUID *pPanelGuid,
            /* [retval][out] */ IWindowPanel **pWindowPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSDIWindowPanel( 
            /* [retval][out] */ IWindowPanel **pWindowPanel) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMainFrameVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMainFrame * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMainFrame * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMainFrame * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetWorkRect )( 
            IMainFrame * This,
            /* [retval][out] */ long *pRect);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowWindow )( 
            IMainFrame * This,
            int nCmdShow);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoWindows )( 
            IMainFrame * This);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Icon )( 
            IMainFrame * This,
            /* [in] */ LONG *Icon);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Icon )( 
            IMainFrame * This,
            /* [retval][out] */ LONG **hIcon);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBars )( 
            IMainFrame * This,
            /* [retval][out] */ IUnknown **ppCommandBars);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_StatusBar )( 
            IMainFrame * This,
            /* [retval][out] */ IStatusBar **pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MainFrameType )( 
            IMainFrame * This,
            /* [retval][out] */ enumMainFrameType *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MdiContainer )( 
            IMainFrame * This,
            /* [retval][out] */ IMdiContainer **pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_SplitterWnd )( 
            IMainFrame * This,
            /* [retval][out] */ ISplitterWnd **pSplitterWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateLayout2 )( 
            IMainFrame * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            IMainFrame * This,
            /* [retval][out] */ IUnknown **pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InstallNotifyCommand )( 
            IMainFrame * This,
            int emInstallEvent_,
            /* [in] */ IUnknown *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Close )( 
            IMainFrame * This);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            IMainFrame * This,
            /* [in] */ BSTR strName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IMainFrame * This,
            /* [retval][out] */ BSTR *strName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetAccelerators )( 
            IMainFrame * This,
            LONG_PTR hInstRes,
            UINT nResID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetHWND )( 
            IMainFrame * This,
            LONG *hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetMenu )( 
            IMainFrame * This,
            /* [in] */ LONG_PTR hMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMenu )( 
            IMainFrame * This,
            /* [in] */ LONG_PTR *hMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetPopupMenu )( 
            IMainFrame * This,
            UINT uiBodsyIndex,
            long umCommand,
            /* [retval][out] */ IUnknown **ppPopupMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateGuiState )( 
            IMainFrame * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseWorkAdd )( 
            IMainFrame * This,
            ULONG *pWorkItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseWorkRemove )( 
            IMainFrame * This,
            ULONG *pWorkItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateMainFrame )( 
            IMainFrame * This,
            MAINFRAMEINFO *pMainFrameInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateMainFrameXml )( 
            IMainFrame * This,
            long *pmodule,
            const wchar_t *psVxmlFileName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Caption )( 
            IMainFrame * This,
            /* [in] */ BSTR bstrCaption);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Caption )( 
            IMainFrame * This,
            /* [retval][out] */ BSTR *bstrCaption);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ModifyMdiToolBar )( 
            IMainFrame * This,
            ULONG ulRemove,
            ULONG ulAdd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoCommand )( 
            IMainFrame * This,
            /* [in] */ ULONG ulcmd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetMinValue )( 
            IMainFrame * This,
            int iWidth,
            int iHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowWindowValue )( 
            IMainFrame * This,
            int nCmdShow,
            int iWidth,
            int iHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetSDIWindowPanel )( 
            IMainFrame * This,
            BSTR bsName,
            BSTR bsTitle,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [in] */ IUnknown *pNotifyCommand,
            /* [in] */ long *pvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetXmlSDIWindowPanel )( 
            IMainFrame * This,
            /* [in] */ long *pmodule,
            /* [in] */ GUID *pPanelGuid,
            /* [retval][out] */ IWindowPanel **pWindowPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSDIWindowPanel )( 
            IMainFrame * This,
            /* [retval][out] */ IWindowPanel **pWindowPanel);
        
        END_INTERFACE
    } IMainFrameVtbl;

    interface IMainFrame
    {
        CONST_VTBL struct IMainFrameVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMainFrame_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMainFrame_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMainFrame_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMainFrame_GetWorkRect(This,pRect)	\
    (This)->lpVtbl -> GetWorkRect(This,pRect)

#define IMainFrame_ShowWindow(This,nCmdShow)	\
    (This)->lpVtbl -> ShowWindow(This,nCmdShow)

#define IMainFrame_DoWindows(This)	\
    (This)->lpVtbl -> DoWindows(This)

#define IMainFrame_put_Icon(This,Icon)	\
    (This)->lpVtbl -> put_Icon(This,Icon)

#define IMainFrame_get_Icon(This,hIcon)	\
    (This)->lpVtbl -> get_Icon(This,hIcon)

#define IMainFrame_get_CommandBars(This,ppCommandBars)	\
    (This)->lpVtbl -> get_CommandBars(This,ppCommandBars)

#define IMainFrame_get_StatusBar(This,pVal)	\
    (This)->lpVtbl -> get_StatusBar(This,pVal)

#define IMainFrame_get_MainFrameType(This,pVal)	\
    (This)->lpVtbl -> get_MainFrameType(This,pVal)

#define IMainFrame_get_MdiContainer(This,pVal)	\
    (This)->lpVtbl -> get_MdiContainer(This,pVal)

#define IMainFrame_get_SplitterWnd(This,pSplitterWnd)	\
    (This)->lpVtbl -> get_SplitterWnd(This,pSplitterWnd)

#define IMainFrame_UpdateLayout2(This)	\
    (This)->lpVtbl -> UpdateLayout2(This)

#define IMainFrame_get_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,pNotifyCommand)

#define IMainFrame_InstallNotifyCommand(This,emInstallEvent_,pNotifyCommand)	\
    (This)->lpVtbl -> InstallNotifyCommand(This,emInstallEvent_,pNotifyCommand)

#define IMainFrame_Close(This)	\
    (This)->lpVtbl -> Close(This)

#define IMainFrame_put_Name(This,strName)	\
    (This)->lpVtbl -> put_Name(This,strName)

#define IMainFrame_get_Name(This,strName)	\
    (This)->lpVtbl -> get_Name(This,strName)

#define IMainFrame_SetAccelerators(This,hInstRes,nResID)	\
    (This)->lpVtbl -> SetAccelerators(This,hInstRes,nResID)

#define IMainFrame_GetHWND(This,hWnd)	\
    (This)->lpVtbl -> GetHWND(This,hWnd)

#define IMainFrame_SetMenu(This,hMenu)	\
    (This)->lpVtbl -> SetMenu(This,hMenu)

#define IMainFrame_GetMenu(This,hMenu)	\
    (This)->lpVtbl -> GetMenu(This,hMenu)

#define IMainFrame_GetPopupMenu(This,uiBodsyIndex,umCommand,ppPopupMenu)	\
    (This)->lpVtbl -> GetPopupMenu(This,uiBodsyIndex,umCommand,ppPopupMenu)

#define IMainFrame_UpdateGuiState(This)	\
    (This)->lpVtbl -> UpdateGuiState(This)

#define IMainFrame_CloseWorkAdd(This,pWorkItem)	\
    (This)->lpVtbl -> CloseWorkAdd(This,pWorkItem)

#define IMainFrame_CloseWorkRemove(This,pWorkItem)	\
    (This)->lpVtbl -> CloseWorkRemove(This,pWorkItem)

#define IMainFrame_CreateMainFrame(This,pMainFrameInfo)	\
    (This)->lpVtbl -> CreateMainFrame(This,pMainFrameInfo)

#define IMainFrame_CreateMainFrameXml(This,pmodule,psVxmlFileName)	\
    (This)->lpVtbl -> CreateMainFrameXml(This,pmodule,psVxmlFileName)

#define IMainFrame_put_Caption(This,bstrCaption)	\
    (This)->lpVtbl -> put_Caption(This,bstrCaption)

#define IMainFrame_get_Caption(This,bstrCaption)	\
    (This)->lpVtbl -> get_Caption(This,bstrCaption)

#define IMainFrame_ModifyMdiToolBar(This,ulRemove,ulAdd)	\
    (This)->lpVtbl -> ModifyMdiToolBar(This,ulRemove,ulAdd)

#define IMainFrame_DoCommand(This,ulcmd)	\
    (This)->lpVtbl -> DoCommand(This,ulcmd)

#define IMainFrame_SetMinValue(This,iWidth,iHeight)	\
    (This)->lpVtbl -> SetMinValue(This,iWidth,iHeight)

#define IMainFrame_ShowWindowValue(This,nCmdShow,iWidth,iHeight)	\
    (This)->lpVtbl -> ShowWindowValue(This,nCmdShow,iWidth,iHeight)

#define IMainFrame_SetSDIWindowPanel(This,bsName,bsTitle,pWindowPanel,pNotifyCommand,pvObj)	\
    (This)->lpVtbl -> SetSDIWindowPanel(This,bsName,bsTitle,pWindowPanel,pNotifyCommand,pvObj)

#define IMainFrame_SetXmlSDIWindowPanel(This,pmodule,pPanelGuid,pWindowPanel)	\
    (This)->lpVtbl -> SetXmlSDIWindowPanel(This,pmodule,pPanelGuid,pWindowPanel)

#define IMainFrame_GetSDIWindowPanel(This,pWindowPanel)	\
    (This)->lpVtbl -> GetSDIWindowPanel(This,pWindowPanel)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_GetWorkRect_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ long *pRect);


void __RPC_STUB IMainFrame_GetWorkRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_ShowWindow_Proxy( 
    IMainFrame * This,
    int nCmdShow);


void __RPC_STUB IMainFrame_ShowWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_DoWindows_Proxy( 
    IMainFrame * This);


void __RPC_STUB IMainFrame_DoWindows_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMainFrame_put_Icon_Proxy( 
    IMainFrame * This,
    /* [in] */ LONG *Icon);


void __RPC_STUB IMainFrame_put_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_Icon_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ LONG **hIcon);


void __RPC_STUB IMainFrame_get_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_CommandBars_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ IUnknown **ppCommandBars);


void __RPC_STUB IMainFrame_get_CommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_StatusBar_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ IStatusBar **pVal);


void __RPC_STUB IMainFrame_get_StatusBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_MainFrameType_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ enumMainFrameType *pVal);


void __RPC_STUB IMainFrame_get_MainFrameType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_MdiContainer_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ IMdiContainer **pVal);


void __RPC_STUB IMainFrame_get_MdiContainer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_SplitterWnd_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ ISplitterWnd **pSplitterWnd);


void __RPC_STUB IMainFrame_get_SplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_UpdateLayout2_Proxy( 
    IMainFrame * This);


void __RPC_STUB IMainFrame_UpdateLayout2_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_NotifyCommand_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ IUnknown **pNotifyCommand);


void __RPC_STUB IMainFrame_get_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_InstallNotifyCommand_Proxy( 
    IMainFrame * This,
    int emInstallEvent_,
    /* [in] */ IUnknown *pNotifyCommand);


void __RPC_STUB IMainFrame_InstallNotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_Close_Proxy( 
    IMainFrame * This);


void __RPC_STUB IMainFrame_Close_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMainFrame_put_Name_Proxy( 
    IMainFrame * This,
    /* [in] */ BSTR strName);


void __RPC_STUB IMainFrame_put_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_Name_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ BSTR *strName);


void __RPC_STUB IMainFrame_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_SetAccelerators_Proxy( 
    IMainFrame * This,
    LONG_PTR hInstRes,
    UINT nResID);


void __RPC_STUB IMainFrame_SetAccelerators_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_GetHWND_Proxy( 
    IMainFrame * This,
    LONG *hWnd);


void __RPC_STUB IMainFrame_GetHWND_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_SetMenu_Proxy( 
    IMainFrame * This,
    /* [in] */ LONG_PTR hMenu);


void __RPC_STUB IMainFrame_SetMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_GetMenu_Proxy( 
    IMainFrame * This,
    /* [in] */ LONG_PTR *hMenu);


void __RPC_STUB IMainFrame_GetMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_GetPopupMenu_Proxy( 
    IMainFrame * This,
    UINT uiBodsyIndex,
    long umCommand,
    /* [retval][out] */ IUnknown **ppPopupMenu);


void __RPC_STUB IMainFrame_GetPopupMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_UpdateGuiState_Proxy( 
    IMainFrame * This);


void __RPC_STUB IMainFrame_UpdateGuiState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_CloseWorkAdd_Proxy( 
    IMainFrame * This,
    ULONG *pWorkItem);


void __RPC_STUB IMainFrame_CloseWorkAdd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_CloseWorkRemove_Proxy( 
    IMainFrame * This,
    ULONG *pWorkItem);


void __RPC_STUB IMainFrame_CloseWorkRemove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_CreateMainFrame_Proxy( 
    IMainFrame * This,
    MAINFRAMEINFO *pMainFrameInfo);


void __RPC_STUB IMainFrame_CreateMainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_CreateMainFrameXml_Proxy( 
    IMainFrame * This,
    long *pmodule,
    const wchar_t *psVxmlFileName);


void __RPC_STUB IMainFrame_CreateMainFrameXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMainFrame_put_Caption_Proxy( 
    IMainFrame * This,
    /* [in] */ BSTR bstrCaption);


void __RPC_STUB IMainFrame_put_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrame_get_Caption_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ BSTR *bstrCaption);


void __RPC_STUB IMainFrame_get_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_ModifyMdiToolBar_Proxy( 
    IMainFrame * This,
    ULONG ulRemove,
    ULONG ulAdd);


void __RPC_STUB IMainFrame_ModifyMdiToolBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_DoCommand_Proxy( 
    IMainFrame * This,
    /* [in] */ ULONG ulcmd);


void __RPC_STUB IMainFrame_DoCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_SetMinValue_Proxy( 
    IMainFrame * This,
    int iWidth,
    int iHeight);


void __RPC_STUB IMainFrame_SetMinValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_ShowWindowValue_Proxy( 
    IMainFrame * This,
    int nCmdShow,
    int iWidth,
    int iHeight);


void __RPC_STUB IMainFrame_ShowWindowValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_SetSDIWindowPanel_Proxy( 
    IMainFrame * This,
    BSTR bsName,
    BSTR bsTitle,
    /* [in] */ IWindowPanel *pWindowPanel,
    /* [in] */ IUnknown *pNotifyCommand,
    /* [in] */ long *pvObj);


void __RPC_STUB IMainFrame_SetSDIWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_SetXmlSDIWindowPanel_Proxy( 
    IMainFrame * This,
    /* [in] */ long *pmodule,
    /* [in] */ GUID *pPanelGuid,
    /* [retval][out] */ IWindowPanel **pWindowPanel);


void __RPC_STUB IMainFrame_SetXmlSDIWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrame_GetSDIWindowPanel_Proxy( 
    IMainFrame * This,
    /* [retval][out] */ IWindowPanel **pWindowPanel);


void __RPC_STUB IMainFrame_GetSDIWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMainFrame_INTERFACE_DEFINED__ */


#ifndef __IMainFrames_INTERFACE_DEFINED__
#define __IMainFrames_INTERFACE_DEFINED__

/* interface IMainFrames */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMainFrames;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9425F50D-1B60-4300-937F-DF20CA6D26FA")
    IMainFrames : public IUnknown
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *nCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            /* [in] */ BSTR bsName,
            /* [retval][out] */ IMainFrame **ppMainFrame) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseAll( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Close( 
            BSTR bsName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetActiveMainFrame( 
            IMainFrame **pActvieMainFrame) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActiveMainFrame( 
            VARIANT varItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateGuiState( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMainFramesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMainFrames * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMainFrames * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMainFrames * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IMainFrames * This,
            /* [retval][out] */ long *nCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            IMainFrames * This,
            /* [in] */ BSTR bsName,
            /* [retval][out] */ IMainFrame **ppMainFrame);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IMainFrames * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseAll )( 
            IMainFrames * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Close )( 
            IMainFrames * This,
            BSTR bsName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetActiveMainFrame )( 
            IMainFrames * This,
            IMainFrame **pActvieMainFrame);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActiveMainFrame )( 
            IMainFrames * This,
            VARIANT varItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateGuiState )( 
            IMainFrames * This);
        
        END_INTERFACE
    } IMainFramesVtbl;

    interface IMainFrames
    {
        CONST_VTBL struct IMainFramesVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMainFrames_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMainFrames_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMainFrames_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMainFrames_get_Count(This,nCount)	\
    (This)->lpVtbl -> get_Count(This,nCount)

#define IMainFrames_GetItem(This,bsName,ppMainFrame)	\
    (This)->lpVtbl -> GetItem(This,bsName,ppMainFrame)

#define IMainFrames_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define IMainFrames_CloseAll(This)	\
    (This)->lpVtbl -> CloseAll(This)

#define IMainFrames_Close(This,bsName)	\
    (This)->lpVtbl -> Close(This,bsName)

#define IMainFrames_GetActiveMainFrame(This,pActvieMainFrame)	\
    (This)->lpVtbl -> GetActiveMainFrame(This,pActvieMainFrame)

#define IMainFrames_SetActiveMainFrame(This,varItem)	\
    (This)->lpVtbl -> SetActiveMainFrame(This,varItem)

#define IMainFrames_UpdateGuiState(This)	\
    (This)->lpVtbl -> UpdateGuiState(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMainFrames_get_Count_Proxy( 
    IMainFrames * This,
    /* [retval][out] */ long *nCount);


void __RPC_STUB IMainFrames_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_GetItem_Proxy( 
    IMainFrames * This,
    /* [in] */ BSTR bsName,
    /* [retval][out] */ IMainFrame **ppMainFrame);


void __RPC_STUB IMainFrames_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMainFrames_get__NewEnum_Proxy( 
    IMainFrames * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB IMainFrames_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_CloseAll_Proxy( 
    IMainFrames * This);


void __RPC_STUB IMainFrames_CloseAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_Close_Proxy( 
    IMainFrames * This,
    BSTR bsName);


void __RPC_STUB IMainFrames_Close_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_GetActiveMainFrame_Proxy( 
    IMainFrames * This,
    IMainFrame **pActvieMainFrame);


void __RPC_STUB IMainFrames_GetActiveMainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_SetActiveMainFrame_Proxy( 
    IMainFrames * This,
    VARIANT varItem);


void __RPC_STUB IMainFrames_SetActiveMainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMainFrames_UpdateGuiState_Proxy( 
    IMainFrames * This);


void __RPC_STUB IMainFrames_UpdateGuiState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMainFrames_INTERFACE_DEFINED__ */


#ifndef __IMdiContainer_INTERFACE_DEFINED__
#define __IMdiContainer_INTERFACE_DEFINED__

/* interface IMdiContainer */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMdiContainer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("87759CEF-E688-486E-8866-AC305E70C314")
    IMdiContainer : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *nCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            /* [in] */ VARIANT *pvarItem,
            IWindowPanel **ppPageItem,
            /* [retval][out] */ long **pvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTabPageItem( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ITabPageItem **ppTabPageItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MoveItem( 
            VARIANT varOldIndex,
            VARIANT varNewIndex) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseActiveItem( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseAll( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateItemXml( 
            long *pBuildUI,
            const wchar_t *psXmlPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nIndex,
            BSTR bsName,
            BSTR bsTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [in] */ long *pNotifyCommand,
            /* [in] */ long *pvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetActiveItem( 
            IWindowPanel **ppPageItem,
            long **pvObj) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetActiveTabPageItem( 
            /* [retval][out] */ ITabPageItem **ppTabPageItem) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MainFrame( 
            /* [retval][out] */ IMainFrame **ppMainFrame) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddMdiContainerNotify( 
            IMdiContainerNotify *pMdiContainerNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveMdiContainerNotify( 
            IMdiContainerNotify *pMdiContainerNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseItemWindowPanel( 
            /* [in] */ IUnknown *pWindowPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseItem( 
            /* [in] */ long iitem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActiveWindowPanel( 
            /* [in] */ IUnknown *pWindowPanel) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetActiveItem( 
            /* [in] */ long iitem) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMdiContainerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMdiContainer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMdiContainer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMdiContainer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMdiContainer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMdiContainer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMdiContainer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMdiContainer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IMdiContainer * This,
            /* [retval][out] */ long *nCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            IMdiContainer * This,
            /* [in] */ VARIANT *pvarItem,
            IWindowPanel **ppPageItem,
            /* [retval][out] */ long **pvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTabPageItem )( 
            IMdiContainer * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ITabPageItem **ppTabPageItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MoveItem )( 
            IMdiContainer * This,
            VARIANT varOldIndex,
            VARIANT varNewIndex);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseActiveItem )( 
            IMdiContainer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseAll )( 
            IMdiContainer * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateItemXml )( 
            IMdiContainer * This,
            long *pBuildUI,
            const wchar_t *psXmlPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IMdiContainer * This,
            int nIndex,
            BSTR bsName,
            BSTR bsTitle,
            HICON hIcon,
            /* [in] */ IWindowPanel *pWindowPanel,
            /* [in] */ long *pNotifyCommand,
            /* [in] */ long *pvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetActiveItem )( 
            IMdiContainer * This,
            IWindowPanel **ppPageItem,
            long **pvObj);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetActiveTabPageItem )( 
            IMdiContainer * This,
            /* [retval][out] */ ITabPageItem **ppTabPageItem);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MainFrame )( 
            IMdiContainer * This,
            /* [retval][out] */ IMainFrame **ppMainFrame);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddMdiContainerNotify )( 
            IMdiContainer * This,
            IMdiContainerNotify *pMdiContainerNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveMdiContainerNotify )( 
            IMdiContainer * This,
            IMdiContainerNotify *pMdiContainerNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseItemWindowPanel )( 
            IMdiContainer * This,
            /* [in] */ IUnknown *pWindowPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseItem )( 
            IMdiContainer * This,
            /* [in] */ long iitem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActiveWindowPanel )( 
            IMdiContainer * This,
            /* [in] */ IUnknown *pWindowPanel);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetActiveItem )( 
            IMdiContainer * This,
            /* [in] */ long iitem);
        
        END_INTERFACE
    } IMdiContainerVtbl;

    interface IMdiContainer
    {
        CONST_VTBL struct IMdiContainerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMdiContainer_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMdiContainer_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMdiContainer_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMdiContainer_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMdiContainer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMdiContainer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMdiContainer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMdiContainer_get_Count(This,nCount)	\
    (This)->lpVtbl -> get_Count(This,nCount)

#define IMdiContainer_GetItem(This,pvarItem,ppPageItem,pvObj)	\
    (This)->lpVtbl -> GetItem(This,pvarItem,ppPageItem,pvObj)

#define IMdiContainer_GetTabPageItem(This,varItem,ppTabPageItem)	\
    (This)->lpVtbl -> GetTabPageItem(This,varItem,ppTabPageItem)

#define IMdiContainer_MoveItem(This,varOldIndex,varNewIndex)	\
    (This)->lpVtbl -> MoveItem(This,varOldIndex,varNewIndex)

#define IMdiContainer_CloseActiveItem(This)	\
    (This)->lpVtbl -> CloseActiveItem(This)

#define IMdiContainer_CloseAll(This)	\
    (This)->lpVtbl -> CloseAll(This)

#define IMdiContainer_CreateItemXml(This,pBuildUI,psXmlPath)	\
    (This)->lpVtbl -> CreateItemXml(This,pBuildUI,psXmlPath)

#define IMdiContainer_InsertItem(This,nIndex,bsName,bsTitle,hIcon,pWindowPanel,pNotifyCommand,pvObj)	\
    (This)->lpVtbl -> InsertItem(This,nIndex,bsName,bsTitle,hIcon,pWindowPanel,pNotifyCommand,pvObj)

#define IMdiContainer_GetActiveItem(This,ppPageItem,pvObj)	\
    (This)->lpVtbl -> GetActiveItem(This,ppPageItem,pvObj)

#define IMdiContainer_GetActiveTabPageItem(This,ppTabPageItem)	\
    (This)->lpVtbl -> GetActiveTabPageItem(This,ppTabPageItem)

#define IMdiContainer_get_MainFrame(This,ppMainFrame)	\
    (This)->lpVtbl -> get_MainFrame(This,ppMainFrame)

#define IMdiContainer_AddMdiContainerNotify(This,pMdiContainerNotify)	\
    (This)->lpVtbl -> AddMdiContainerNotify(This,pMdiContainerNotify)

#define IMdiContainer_RemoveMdiContainerNotify(This,pMdiContainerNotify)	\
    (This)->lpVtbl -> RemoveMdiContainerNotify(This,pMdiContainerNotify)

#define IMdiContainer_CloseItemWindowPanel(This,pWindowPanel)	\
    (This)->lpVtbl -> CloseItemWindowPanel(This,pWindowPanel)

#define IMdiContainer_CloseItem(This,iitem)	\
    (This)->lpVtbl -> CloseItem(This,iitem)

#define IMdiContainer_SetActiveWindowPanel(This,pWindowPanel)	\
    (This)->lpVtbl -> SetActiveWindowPanel(This,pWindowPanel)

#define IMdiContainer_SetActiveItem(This,iitem)	\
    (This)->lpVtbl -> SetActiveItem(This,iitem)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMdiContainer_get_Count_Proxy( 
    IMdiContainer * This,
    /* [retval][out] */ long *nCount);


void __RPC_STUB IMdiContainer_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_GetItem_Proxy( 
    IMdiContainer * This,
    /* [in] */ VARIANT *pvarItem,
    IWindowPanel **ppPageItem,
    /* [retval][out] */ long **pvObj);


void __RPC_STUB IMdiContainer_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_GetTabPageItem_Proxy( 
    IMdiContainer * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ ITabPageItem **ppTabPageItem);


void __RPC_STUB IMdiContainer_GetTabPageItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_MoveItem_Proxy( 
    IMdiContainer * This,
    VARIANT varOldIndex,
    VARIANT varNewIndex);


void __RPC_STUB IMdiContainer_MoveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_CloseActiveItem_Proxy( 
    IMdiContainer * This);


void __RPC_STUB IMdiContainer_CloseActiveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_CloseAll_Proxy( 
    IMdiContainer * This);


void __RPC_STUB IMdiContainer_CloseAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_CreateItemXml_Proxy( 
    IMdiContainer * This,
    long *pBuildUI,
    const wchar_t *psXmlPath);


void __RPC_STUB IMdiContainer_CreateItemXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_InsertItem_Proxy( 
    IMdiContainer * This,
    int nIndex,
    BSTR bsName,
    BSTR bsTitle,
    HICON hIcon,
    /* [in] */ IWindowPanel *pWindowPanel,
    /* [in] */ long *pNotifyCommand,
    /* [in] */ long *pvObj);


void __RPC_STUB IMdiContainer_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_GetActiveItem_Proxy( 
    IMdiContainer * This,
    IWindowPanel **ppPageItem,
    long **pvObj);


void __RPC_STUB IMdiContainer_GetActiveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_GetActiveTabPageItem_Proxy( 
    IMdiContainer * This,
    /* [retval][out] */ ITabPageItem **ppTabPageItem);


void __RPC_STUB IMdiContainer_GetActiveTabPageItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMdiContainer_get_MainFrame_Proxy( 
    IMdiContainer * This,
    /* [retval][out] */ IMainFrame **ppMainFrame);


void __RPC_STUB IMdiContainer_get_MainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_AddMdiContainerNotify_Proxy( 
    IMdiContainer * This,
    IMdiContainerNotify *pMdiContainerNotify);


void __RPC_STUB IMdiContainer_AddMdiContainerNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_RemoveMdiContainerNotify_Proxy( 
    IMdiContainer * This,
    IMdiContainerNotify *pMdiContainerNotify);


void __RPC_STUB IMdiContainer_RemoveMdiContainerNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_CloseItemWindowPanel_Proxy( 
    IMdiContainer * This,
    /* [in] */ IUnknown *pWindowPanel);


void __RPC_STUB IMdiContainer_CloseItemWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_CloseItem_Proxy( 
    IMdiContainer * This,
    /* [in] */ long iitem);


void __RPC_STUB IMdiContainer_CloseItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_SetActiveWindowPanel_Proxy( 
    IMdiContainer * This,
    /* [in] */ IUnknown *pWindowPanel);


void __RPC_STUB IMdiContainer_SetActiveWindowPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMdiContainer_SetActiveItem_Proxy( 
    IMdiContainer * This,
    /* [in] */ long iitem);


void __RPC_STUB IMdiContainer_SetActiveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMdiContainer_INTERFACE_DEFINED__ */


#ifndef __IAddIn_INTERFACE_DEFINED__
#define __IAddIn_INTERFACE_DEFINED__

/* interface IAddIn */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAddIn;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C1B28E02-7D69-4DA6-B8AB-A1661676D1DF")
    IAddIn : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SatelliteDLLPath( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Description( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Connected( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ProgID( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Extensibility( 
            /* [retval][out] */ IExtensibility **ppExtensibility) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAddInVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAddIn * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAddIn * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAddIn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SatelliteDLLPath )( 
            IAddIn * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IAddIn * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Description )( 
            IAddIn * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Connected )( 
            IAddIn * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgID )( 
            IAddIn * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Extensibility )( 
            IAddIn * This,
            /* [retval][out] */ IExtensibility **ppExtensibility);
        
        END_INTERFACE
    } IAddInVtbl;

    interface IAddIn
    {
        CONST_VTBL struct IAddInVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAddIn_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAddIn_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAddIn_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAddIn_SatelliteDLLPath(This)	\
    (This)->lpVtbl -> SatelliteDLLPath(This)

#define IAddIn_get_Name(This,pVal)	\
    (This)->lpVtbl -> get_Name(This,pVal)

#define IAddIn_get_Description(This,pVal)	\
    (This)->lpVtbl -> get_Description(This,pVal)

#define IAddIn_get_Connected(This,pVal)	\
    (This)->lpVtbl -> get_Connected(This,pVal)

#define IAddIn_get_ProgID(This,pVal)	\
    (This)->lpVtbl -> get_ProgID(This,pVal)

#define IAddIn_get_Extensibility(This,ppExtensibility)	\
    (This)->lpVtbl -> get_Extensibility(This,ppExtensibility)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IAddIn_SatelliteDLLPath_Proxy( 
    IAddIn * This);


void __RPC_STUB IAddIn_SatelliteDLLPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIn_get_Name_Proxy( 
    IAddIn * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAddIn_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIn_get_Description_Proxy( 
    IAddIn * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAddIn_get_Description_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIn_get_Connected_Proxy( 
    IAddIn * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IAddIn_get_Connected_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIn_get_ProgID_Proxy( 
    IAddIn * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IAddIn_get_ProgID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIn_get_Extensibility_Proxy( 
    IAddIn * This,
    /* [retval][out] */ IExtensibility **ppExtensibility);


void __RPC_STUB IAddIn_get_Extensibility_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAddIn_INTERFACE_DEFINED__ */


#ifndef __IAddIns_INTERFACE_DEFINED__
#define __IAddIns_INTERFACE_DEFINED__

/* interface IAddIns */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IAddIns;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E7068706-BB8A-4479-BC7D-7FF3EF9ECC16")
    IAddIns : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Add( 
            PARTSINFO *pPartsInfo) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Update( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_UserDispatch( 
            /* [retval][out] */ IDispatch **pUserDispatch) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_UserDispatch( 
            /* [in] */ IDispatch *pUserDispatch) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddInCreateProc( 
            PARTSINFO *pPartsInfo,
            IAddIn **ppAddIn,
            /* [out] */ IExtensibility **ppExtensibility) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddInCloseProc( 
            PARTSINFO *pPartsInfo) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCount( 
            /* [retval][out] */ long *nCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            /* [in] */ PARTSINFO *pPartsInfo,
            /* [out] */ IAddIn **ppAddIn,
            /* [out] */ IExtensibility **ppExtensibility) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveAddIn( 
            /* [in] */ PARTSINFO *pPartsInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAddInsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAddIns * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAddIns * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAddIns * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IAddIns * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IAddIns * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IAddIns * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IAddIns * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IAddIns * This,
            PARTSINFO *pPartsInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IAddIns * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserDispatch )( 
            IAddIns * This,
            /* [retval][out] */ IDispatch **pUserDispatch);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserDispatch )( 
            IAddIns * This,
            /* [in] */ IDispatch *pUserDispatch);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddInCreateProc )( 
            IAddIns * This,
            PARTSINFO *pPartsInfo,
            IAddIn **ppAddIn,
            /* [out] */ IExtensibility **ppExtensibility);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddInCloseProc )( 
            IAddIns * This,
            PARTSINFO *pPartsInfo);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IAddIns * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCount )( 
            IAddIns * This,
            /* [retval][out] */ long *nCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            IAddIns * This,
            /* [in] */ PARTSINFO *pPartsInfo,
            /* [out] */ IAddIn **ppAddIn,
            /* [out] */ IExtensibility **ppExtensibility);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAddIn )( 
            IAddIns * This,
            /* [in] */ PARTSINFO *pPartsInfo);
        
        END_INTERFACE
    } IAddInsVtbl;

    interface IAddIns
    {
        CONST_VTBL struct IAddInsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAddIns_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAddIns_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAddIns_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAddIns_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IAddIns_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IAddIns_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IAddIns_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IAddIns_Add(This,pPartsInfo)	\
    (This)->lpVtbl -> Add(This,pPartsInfo)

#define IAddIns_Update(This)	\
    (This)->lpVtbl -> Update(This)

#define IAddIns_get_UserDispatch(This,pUserDispatch)	\
    (This)->lpVtbl -> get_UserDispatch(This,pUserDispatch)

#define IAddIns_put_UserDispatch(This,pUserDispatch)	\
    (This)->lpVtbl -> put_UserDispatch(This,pUserDispatch)

#define IAddIns_AddInCreateProc(This,pPartsInfo,ppAddIn,ppExtensibility)	\
    (This)->lpVtbl -> AddInCreateProc(This,pPartsInfo,ppAddIn,ppExtensibility)

#define IAddIns_AddInCloseProc(This,pPartsInfo)	\
    (This)->lpVtbl -> AddInCloseProc(This,pPartsInfo)

#define IAddIns_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define IAddIns_GetCount(This,nCount)	\
    (This)->lpVtbl -> GetCount(This,nCount)

#define IAddIns_GetItem(This,pPartsInfo,ppAddIn,ppExtensibility)	\
    (This)->lpVtbl -> GetItem(This,pPartsInfo,ppAddIn,ppExtensibility)

#define IAddIns_RemoveAddIn(This,pPartsInfo)	\
    (This)->lpVtbl -> RemoveAddIn(This,pPartsInfo)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_Add_Proxy( 
    IAddIns * This,
    PARTSINFO *pPartsInfo);


void __RPC_STUB IAddIns_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_Update_Proxy( 
    IAddIns * This);


void __RPC_STUB IAddIns_Update_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IAddIns_get_UserDispatch_Proxy( 
    IAddIns * This,
    /* [retval][out] */ IDispatch **pUserDispatch);


void __RPC_STUB IAddIns_get_UserDispatch_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IAddIns_put_UserDispatch_Proxy( 
    IAddIns * This,
    /* [in] */ IDispatch *pUserDispatch);


void __RPC_STUB IAddIns_put_UserDispatch_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_AddInCreateProc_Proxy( 
    IAddIns * This,
    PARTSINFO *pPartsInfo,
    IAddIn **ppAddIn,
    /* [out] */ IExtensibility **ppExtensibility);


void __RPC_STUB IAddIns_AddInCreateProc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_AddInCloseProc_Proxy( 
    IAddIns * This,
    PARTSINFO *pPartsInfo);


void __RPC_STUB IAddIns_AddInCloseProc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IAddIns_get__NewEnum_Proxy( 
    IAddIns * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB IAddIns_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_GetCount_Proxy( 
    IAddIns * This,
    /* [retval][out] */ long *nCount);


void __RPC_STUB IAddIns_GetCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_GetItem_Proxy( 
    IAddIns * This,
    /* [in] */ PARTSINFO *pPartsInfo,
    /* [out] */ IAddIn **ppAddIn,
    /* [out] */ IExtensibility **ppExtensibility);


void __RPC_STUB IAddIns_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IAddIns_RemoveAddIn_Proxy( 
    IAddIns * This,
    /* [in] */ PARTSINFO *pPartsInfo);


void __RPC_STUB IAddIns_RemoveAddIn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAddIns_INTERFACE_DEFINED__ */


#ifndef __IStatusBarItem_INTERFACE_DEFINED__
#define __IStatusBarItem_INTERFACE_DEFINED__

/* interface IStatusBarItem */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IStatusBarItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("194A568D-B4BD-47D0-9D6F-3D9A97B0D8A2")
    IStatusBarItem : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Caption( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Caption( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Enable( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Enable( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Icon( 
            /* [retval][out] */ HICON *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Icon( 
            /* [in] */ HICON newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ int *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ int newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IStatusBarItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IStatusBarItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IStatusBarItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IStatusBarItem * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IStatusBarItem * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IStatusBarItem * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IStatusBarItem * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IStatusBarItem * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Caption )( 
            IStatusBarItem * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Caption )( 
            IStatusBarItem * This,
            /* [in] */ BSTR newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Enable )( 
            IStatusBarItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Enable )( 
            IStatusBarItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Icon )( 
            IStatusBarItem * This,
            /* [retval][out] */ HICON *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Icon )( 
            IStatusBarItem * This,
            /* [in] */ HICON newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            IStatusBarItem * This,
            /* [retval][out] */ int *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            IStatusBarItem * This,
            /* [in] */ int newVal);
        
        END_INTERFACE
    } IStatusBarItemVtbl;

    interface IStatusBarItem
    {
        CONST_VTBL struct IStatusBarItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IStatusBarItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IStatusBarItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IStatusBarItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IStatusBarItem_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IStatusBarItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IStatusBarItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IStatusBarItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IStatusBarItem_get_Caption(This,pVal)	\
    (This)->lpVtbl -> get_Caption(This,pVal)

#define IStatusBarItem_put_Caption(This,newVal)	\
    (This)->lpVtbl -> put_Caption(This,newVal)

#define IStatusBarItem_get_Enable(This,pVal)	\
    (This)->lpVtbl -> get_Enable(This,pVal)

#define IStatusBarItem_put_Enable(This,newVal)	\
    (This)->lpVtbl -> put_Enable(This,newVal)

#define IStatusBarItem_get_Icon(This,pVal)	\
    (This)->lpVtbl -> get_Icon(This,pVal)

#define IStatusBarItem_put_Icon(This,newVal)	\
    (This)->lpVtbl -> put_Icon(This,newVal)

#define IStatusBarItem_get_Width(This,pVal)	\
    (This)->lpVtbl -> get_Width(This,pVal)

#define IStatusBarItem_put_Width(This,newVal)	\
    (This)->lpVtbl -> put_Width(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_get_Caption_Proxy( 
    IStatusBarItem * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IStatusBarItem_get_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_put_Caption_Proxy( 
    IStatusBarItem * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IStatusBarItem_put_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_get_Enable_Proxy( 
    IStatusBarItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IStatusBarItem_get_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_put_Enable_Proxy( 
    IStatusBarItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB IStatusBarItem_put_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_get_Icon_Proxy( 
    IStatusBarItem * This,
    /* [retval][out] */ HICON *pVal);


void __RPC_STUB IStatusBarItem_get_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_put_Icon_Proxy( 
    IStatusBarItem * This,
    /* [in] */ HICON newVal);


void __RPC_STUB IStatusBarItem_put_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_get_Width_Proxy( 
    IStatusBarItem * This,
    /* [retval][out] */ int *pVal);


void __RPC_STUB IStatusBarItem_get_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IStatusBarItem_put_Width_Proxy( 
    IStatusBarItem * This,
    /* [in] */ int newVal);


void __RPC_STUB IStatusBarItem_put_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IStatusBarItem_INTERFACE_DEFINED__ */


#ifndef __IComFramework_INTERFACE_DEFINED__
#define __IComFramework_INTERFACE_DEFINED__

/* interface IComFramework */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IComFramework;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("15BF1DF9-CEF0-401C-A8F6-BAF88BD3C0B3")
    IComFramework : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_GenericPanels( 
            IGenericPanels **ppGenericPanels) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_AddIns( 
            /* [retval][out] */ IDispatch **ppAddIns) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_IsMainFrame( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetMainFrame( 
            BSTR bsFrameName,
            IMainFrame **ppMainFrame) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationName( 
            /* [retval][out] */ BSTR *pbstrName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ApplicationName( 
            /* [in] */ BSTR pbstrName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_FrameworkPath( 
            /* [retval][out] */ BSTR *pbstrPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateTaskPanel( 
            BSTR bstrTitle,
            /* [retval][out] */ IDispatch **pTaskPanel) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_TaskManager( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ToolGenericPanel( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreatePopupMenuBar( 
            IDispatch **ppmb,
            HMENU hMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoLanguageDialg( 
            LONG_PTR hWndParent) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ApplicationPath( 
            /* [retval][out] */ BSTR *pbstrPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Update( 
            /* [in] */ BSTR pUpgFullName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ThemesPath( 
            /* [retval][out] */ BSTR *pstrPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_UserObject( 
            /* [in] */ BSTR btrKey,
            /* [retval][out] */ ULONG *pParam) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_UserObject( 
            /* [in] */ BSTR btrKey,
            /* [in] */ ULONG lParam) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MainFrames( 
            /* [retval][out] */ IMainFrames **ppmfs) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateMainFrameObject( 
            /* [retval][out] */ IMainFrame **ppMainFrame) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Run( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBarsList( 
            long **ppCommandBarsList) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSplitterWnd( 
            ISplitterWnd **ppSplitterWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SafProc( 
            int nType,
            IUnknown *pSafProc,
            VARIANT_BOOL bAdd) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Theme( 
            long **ppTheme) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Quit( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InitComFramework( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UninitComFramework( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IComFrameworkVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IComFramework * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IComFramework * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IComFramework * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IComFramework * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IComFramework * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IComFramework * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IComFramework * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_GenericPanels )( 
            IComFramework * This,
            IGenericPanels **ppGenericPanels);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_AddIns )( 
            IComFramework * This,
            /* [retval][out] */ IDispatch **ppAddIns);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IsMainFrame )( 
            IComFramework * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetMainFrame )( 
            IComFramework * This,
            BSTR bsFrameName,
            IMainFrame **ppMainFrame);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationName )( 
            IComFramework * This,
            /* [retval][out] */ BSTR *pbstrName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ApplicationName )( 
            IComFramework * This,
            /* [in] */ BSTR pbstrName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FrameworkPath )( 
            IComFramework * This,
            /* [retval][out] */ BSTR *pbstrPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateTaskPanel )( 
            IComFramework * This,
            BSTR bstrTitle,
            /* [retval][out] */ IDispatch **pTaskPanel);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TaskManager )( 
            IComFramework * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ToolGenericPanel )( 
            IComFramework * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreatePopupMenuBar )( 
            IComFramework * This,
            IDispatch **ppmb,
            HMENU hMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoLanguageDialg )( 
            IComFramework * This,
            LONG_PTR hWndParent);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ApplicationPath )( 
            IComFramework * This,
            /* [retval][out] */ BSTR *pbstrPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Update )( 
            IComFramework * This,
            /* [in] */ BSTR pUpgFullName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ThemesPath )( 
            IComFramework * This,
            /* [retval][out] */ BSTR *pstrPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UserObject )( 
            IComFramework * This,
            /* [in] */ BSTR btrKey,
            /* [retval][out] */ ULONG *pParam);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_UserObject )( 
            IComFramework * This,
            /* [in] */ BSTR btrKey,
            /* [in] */ ULONG lParam);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MainFrames )( 
            IComFramework * This,
            /* [retval][out] */ IMainFrames **ppmfs);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateMainFrameObject )( 
            IComFramework * This,
            /* [retval][out] */ IMainFrame **ppMainFrame);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Run )( 
            IComFramework * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBarsList )( 
            IComFramework * This,
            long **ppCommandBarsList);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSplitterWnd )( 
            IComFramework * This,
            ISplitterWnd **ppSplitterWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SafProc )( 
            IComFramework * This,
            int nType,
            IUnknown *pSafProc,
            VARIANT_BOOL bAdd);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Theme )( 
            IComFramework * This,
            long **ppTheme);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Quit )( 
            IComFramework * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InitComFramework )( 
            IComFramework * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UninitComFramework )( 
            IComFramework * This);
        
        END_INTERFACE
    } IComFrameworkVtbl;

    interface IComFramework
    {
        CONST_VTBL struct IComFrameworkVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IComFramework_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IComFramework_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IComFramework_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IComFramework_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IComFramework_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IComFramework_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IComFramework_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IComFramework_get_GenericPanels(This,ppGenericPanels)	\
    (This)->lpVtbl -> get_GenericPanels(This,ppGenericPanels)

#define IComFramework_get_AddIns(This,ppAddIns)	\
    (This)->lpVtbl -> get_AddIns(This,ppAddIns)

#define IComFramework_get_IsMainFrame(This,pVal)	\
    (This)->lpVtbl -> get_IsMainFrame(This,pVal)

#define IComFramework_GetMainFrame(This,bsFrameName,ppMainFrame)	\
    (This)->lpVtbl -> GetMainFrame(This,bsFrameName,ppMainFrame)

#define IComFramework_get_ApplicationName(This,pbstrName)	\
    (This)->lpVtbl -> get_ApplicationName(This,pbstrName)

#define IComFramework_put_ApplicationName(This,pbstrName)	\
    (This)->lpVtbl -> put_ApplicationName(This,pbstrName)

#define IComFramework_get_FrameworkPath(This,pbstrPath)	\
    (This)->lpVtbl -> get_FrameworkPath(This,pbstrPath)

#define IComFramework_CreateTaskPanel(This,bstrTitle,pTaskPanel)	\
    (This)->lpVtbl -> CreateTaskPanel(This,bstrTitle,pTaskPanel)

#define IComFramework_get_TaskManager(This,pVal)	\
    (This)->lpVtbl -> get_TaskManager(This,pVal)

#define IComFramework_get_ToolGenericPanel(This,pVal)	\
    (This)->lpVtbl -> get_ToolGenericPanel(This,pVal)

#define IComFramework_CreatePopupMenuBar(This,ppmb,hMenu)	\
    (This)->lpVtbl -> CreatePopupMenuBar(This,ppmb,hMenu)

#define IComFramework_DoLanguageDialg(This,hWndParent)	\
    (This)->lpVtbl -> DoLanguageDialg(This,hWndParent)

#define IComFramework_get_ApplicationPath(This,pbstrPath)	\
    (This)->lpVtbl -> get_ApplicationPath(This,pbstrPath)

#define IComFramework_Update(This,pUpgFullName)	\
    (This)->lpVtbl -> Update(This,pUpgFullName)

#define IComFramework_get_ThemesPath(This,pstrPath)	\
    (This)->lpVtbl -> get_ThemesPath(This,pstrPath)

#define IComFramework_get_UserObject(This,btrKey,pParam)	\
    (This)->lpVtbl -> get_UserObject(This,btrKey,pParam)

#define IComFramework_put_UserObject(This,btrKey,lParam)	\
    (This)->lpVtbl -> put_UserObject(This,btrKey,lParam)

#define IComFramework_get_MainFrames(This,ppmfs)	\
    (This)->lpVtbl -> get_MainFrames(This,ppmfs)

#define IComFramework_CreateMainFrameObject(This,ppMainFrame)	\
    (This)->lpVtbl -> CreateMainFrameObject(This,ppMainFrame)

#define IComFramework_Run(This)	\
    (This)->lpVtbl -> Run(This)

#define IComFramework_get_CommandBarsList(This,ppCommandBarsList)	\
    (This)->lpVtbl -> get_CommandBarsList(This,ppCommandBarsList)

#define IComFramework_CreateSplitterWnd(This,ppSplitterWnd)	\
    (This)->lpVtbl -> CreateSplitterWnd(This,ppSplitterWnd)

#define IComFramework_SafProc(This,nType,pSafProc,bAdd)	\
    (This)->lpVtbl -> SafProc(This,nType,pSafProc,bAdd)

#define IComFramework_get_Theme(This,ppTheme)	\
    (This)->lpVtbl -> get_Theme(This,ppTheme)

#define IComFramework_Quit(This)	\
    (This)->lpVtbl -> Quit(This)

#define IComFramework_InitComFramework(This)	\
    (This)->lpVtbl -> InitComFramework(This)

#define IComFramework_UninitComFramework(This)	\
    (This)->lpVtbl -> UninitComFramework(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_GenericPanels_Proxy( 
    IComFramework * This,
    IGenericPanels **ppGenericPanels);


void __RPC_STUB IComFramework_get_GenericPanels_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_AddIns_Proxy( 
    IComFramework * This,
    /* [retval][out] */ IDispatch **ppAddIns);


void __RPC_STUB IComFramework_get_AddIns_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_IsMainFrame_Proxy( 
    IComFramework * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB IComFramework_get_IsMainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_GetMainFrame_Proxy( 
    IComFramework * This,
    BSTR bsFrameName,
    IMainFrame **ppMainFrame);


void __RPC_STUB IComFramework_GetMainFrame_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_ApplicationName_Proxy( 
    IComFramework * This,
    /* [retval][out] */ BSTR *pbstrName);


void __RPC_STUB IComFramework_get_ApplicationName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IComFramework_put_ApplicationName_Proxy( 
    IComFramework * This,
    /* [in] */ BSTR pbstrName);


void __RPC_STUB IComFramework_put_ApplicationName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_FrameworkPath_Proxy( 
    IComFramework * This,
    /* [retval][out] */ BSTR *pbstrPath);


void __RPC_STUB IComFramework_get_FrameworkPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_CreateTaskPanel_Proxy( 
    IComFramework * This,
    BSTR bstrTitle,
    /* [retval][out] */ IDispatch **pTaskPanel);


void __RPC_STUB IComFramework_CreateTaskPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_TaskManager_Proxy( 
    IComFramework * This,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB IComFramework_get_TaskManager_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_ToolGenericPanel_Proxy( 
    IComFramework * This,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB IComFramework_get_ToolGenericPanel_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_CreatePopupMenuBar_Proxy( 
    IComFramework * This,
    IDispatch **ppmb,
    HMENU hMenu);


void __RPC_STUB IComFramework_CreatePopupMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_DoLanguageDialg_Proxy( 
    IComFramework * This,
    LONG_PTR hWndParent);


void __RPC_STUB IComFramework_DoLanguageDialg_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_ApplicationPath_Proxy( 
    IComFramework * This,
    /* [retval][out] */ BSTR *pbstrPath);


void __RPC_STUB IComFramework_get_ApplicationPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_Update_Proxy( 
    IComFramework * This,
    /* [in] */ BSTR pUpgFullName);


void __RPC_STUB IComFramework_Update_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_ThemesPath_Proxy( 
    IComFramework * This,
    /* [retval][out] */ BSTR *pstrPath);


void __RPC_STUB IComFramework_get_ThemesPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_UserObject_Proxy( 
    IComFramework * This,
    /* [in] */ BSTR btrKey,
    /* [retval][out] */ ULONG *pParam);


void __RPC_STUB IComFramework_get_UserObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IComFramework_put_UserObject_Proxy( 
    IComFramework * This,
    /* [in] */ BSTR btrKey,
    /* [in] */ ULONG lParam);


void __RPC_STUB IComFramework_put_UserObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_MainFrames_Proxy( 
    IComFramework * This,
    /* [retval][out] */ IMainFrames **ppmfs);


void __RPC_STUB IComFramework_get_MainFrames_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_CreateMainFrameObject_Proxy( 
    IComFramework * This,
    /* [retval][out] */ IMainFrame **ppMainFrame);


void __RPC_STUB IComFramework_CreateMainFrameObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_Run_Proxy( 
    IComFramework * This);


void __RPC_STUB IComFramework_Run_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_CommandBarsList_Proxy( 
    IComFramework * This,
    long **ppCommandBarsList);


void __RPC_STUB IComFramework_get_CommandBarsList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_CreateSplitterWnd_Proxy( 
    IComFramework * This,
    ISplitterWnd **ppSplitterWnd);


void __RPC_STUB IComFramework_CreateSplitterWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_SafProc_Proxy( 
    IComFramework * This,
    int nType,
    IUnknown *pSafProc,
    VARIANT_BOOL bAdd);


void __RPC_STUB IComFramework_SafProc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IComFramework_get_Theme_Proxy( 
    IComFramework * This,
    long **ppTheme);


void __RPC_STUB IComFramework_get_Theme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_Quit_Proxy( 
    IComFramework * This);


void __RPC_STUB IComFramework_Quit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_InitComFramework_Proxy( 
    IComFramework * This);


void __RPC_STUB IComFramework_InitComFramework_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IComFramework_UninitComFramework_Proxy( 
    IComFramework * This);


void __RPC_STUB IComFramework_UninitComFramework_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IComFramework_INTERFACE_DEFINED__ */


#ifndef __INavigationBar_INTERFACE_DEFINED__
#define __INavigationBar_INTERFACE_DEFINED__

/* interface INavigationBar */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INavigationBar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DEB9549A-1D60-4BF9-B4FB-E8C7492AF283")
    INavigationBar : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateNavigationBar( 
            LONG_PTR hwndParent,
            int nLeft,
            int nTop,
            int nWidth,
            int nBottom) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InitEditControl( 
            IUnknown *punkACL) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SubclassNavigationBar( 
            LONG_PTR hwndParent) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INavigationBarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INavigationBar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INavigationBar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INavigationBar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INavigationBar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INavigationBar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INavigationBar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INavigationBar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateNavigationBar )( 
            INavigationBar * This,
            LONG_PTR hwndParent,
            int nLeft,
            int nTop,
            int nWidth,
            int nBottom);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InitEditControl )( 
            INavigationBar * This,
            IUnknown *punkACL);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SubclassNavigationBar )( 
            INavigationBar * This,
            LONG_PTR hwndParent);
        
        END_INTERFACE
    } INavigationBarVtbl;

    interface INavigationBar
    {
        CONST_VTBL struct INavigationBarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INavigationBar_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define INavigationBar_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define INavigationBar_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define INavigationBar_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define INavigationBar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define INavigationBar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define INavigationBar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define INavigationBar_CreateNavigationBar(This,hwndParent,nLeft,nTop,nWidth,nBottom)	\
    (This)->lpVtbl -> CreateNavigationBar(This,hwndParent,nLeft,nTop,nWidth,nBottom)

#define INavigationBar_InitEditControl(This,punkACL)	\
    (This)->lpVtbl -> InitEditControl(This,punkACL)

#define INavigationBar_SubclassNavigationBar(This,hwndParent)	\
    (This)->lpVtbl -> SubclassNavigationBar(This,hwndParent)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INavigationBar_CreateNavigationBar_Proxy( 
    INavigationBar * This,
    LONG_PTR hwndParent,
    int nLeft,
    int nTop,
    int nWidth,
    int nBottom);


void __RPC_STUB INavigationBar_CreateNavigationBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INavigationBar_InitEditControl_Proxy( 
    INavigationBar * This,
    IUnknown *punkACL);


void __RPC_STUB INavigationBar_InitEditControl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INavigationBar_SubclassNavigationBar_Proxy( 
    INavigationBar * This,
    LONG_PTR hwndParent);


void __RPC_STUB INavigationBar_SubclassNavigationBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INavigationBar_INTERFACE_DEFINED__ */


#ifndef __IExtensibility_INTERFACE_DEFINED__
#define __IExtensibility_INTERFACE_DEFINED__

/* interface IExtensibility */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IExtensibility;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AC690A2D-0F36-44E8-BA24-61F656BC2EB2")
    IExtensibility : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnAddInsUpdate( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnBeginShutdown( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnConnection( 
            IComFramework *pComFramework,
            enumConnectMode ConnectMode2,
            IAddIn *pAddIn,
            LONG *pUserDispatch) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnStartupComplete( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OnDisconnection( 
            enumDisconnectMode disconnectMode,
            IMainFrame *pMainFrame,
            IAddIn *pAddIn) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IExtensibilityVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IExtensibility * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IExtensibility * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IExtensibility * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IExtensibility * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IExtensibility * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IExtensibility * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IExtensibility * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnAddInsUpdate )( 
            IExtensibility * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnBeginShutdown )( 
            IExtensibility * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnConnection )( 
            IExtensibility * This,
            IComFramework *pComFramework,
            enumConnectMode ConnectMode2,
            IAddIn *pAddIn,
            LONG *pUserDispatch);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnStartupComplete )( 
            IExtensibility * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OnDisconnection )( 
            IExtensibility * This,
            enumDisconnectMode disconnectMode,
            IMainFrame *pMainFrame,
            IAddIn *pAddIn);
        
        END_INTERFACE
    } IExtensibilityVtbl;

    interface IExtensibility
    {
        CONST_VTBL struct IExtensibilityVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IExtensibility_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IExtensibility_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IExtensibility_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IExtensibility_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IExtensibility_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IExtensibility_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IExtensibility_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IExtensibility_OnAddInsUpdate(This)	\
    (This)->lpVtbl -> OnAddInsUpdate(This)

#define IExtensibility_OnBeginShutdown(This)	\
    (This)->lpVtbl -> OnBeginShutdown(This)

#define IExtensibility_OnConnection(This,pComFramework,ConnectMode2,pAddIn,pUserDispatch)	\
    (This)->lpVtbl -> OnConnection(This,pComFramework,ConnectMode2,pAddIn,pUserDispatch)

#define IExtensibility_OnStartupComplete(This)	\
    (This)->lpVtbl -> OnStartupComplete(This)

#define IExtensibility_OnDisconnection(This,disconnectMode,pMainFrame,pAddIn)	\
    (This)->lpVtbl -> OnDisconnection(This,disconnectMode,pMainFrame,pAddIn)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IExtensibility_OnAddInsUpdate_Proxy( 
    IExtensibility * This);


void __RPC_STUB IExtensibility_OnAddInsUpdate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IExtensibility_OnBeginShutdown_Proxy( 
    IExtensibility * This);


void __RPC_STUB IExtensibility_OnBeginShutdown_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IExtensibility_OnConnection_Proxy( 
    IExtensibility * This,
    IComFramework *pComFramework,
    enumConnectMode ConnectMode2,
    IAddIn *pAddIn,
    LONG *pUserDispatch);


void __RPC_STUB IExtensibility_OnConnection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IExtensibility_OnStartupComplete_Proxy( 
    IExtensibility * This);


void __RPC_STUB IExtensibility_OnStartupComplete_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IExtensibility_OnDisconnection_Proxy( 
    IExtensibility * This,
    enumDisconnectMode disconnectMode,
    IMainFrame *pMainFrame,
    IAddIn *pAddIn);


void __RPC_STUB IExtensibility_OnDisconnection_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IExtensibility_INTERFACE_DEFINED__ */



#ifndef __ArtFrameworkLib_LIBRARY_DEFINED__
#define __ArtFrameworkLib_LIBRARY_DEFINED__

/* library ArtFrameworkLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ArtFrameworkLib;

#ifndef ___IMainFrameEvents_DISPINTERFACE_DEFINED__
#define ___IMainFrameEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IMainFrameEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IMainFrameEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C621E617-FB20-407D-BBA6-2831354AF83A")
    _IMainFrameEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IMainFrameEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IMainFrameEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IMainFrameEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IMainFrameEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IMainFrameEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IMainFrameEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IMainFrameEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IMainFrameEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IMainFrameEventsVtbl;

    interface _IMainFrameEvents
    {
        CONST_VTBL struct _IMainFrameEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IMainFrameEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IMainFrameEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IMainFrameEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IMainFrameEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IMainFrameEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IMainFrameEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IMainFrameEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IMainFrameEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_MainFrame;

#ifdef __cplusplus

class DECLSPEC_UUID("15041957-5E54-4B68-BF1E-CB100F927B52")
MainFrame;
#endif

#ifndef ___IMainFramesEvents_DISPINTERFACE_DEFINED__
#define ___IMainFramesEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IMainFramesEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IMainFramesEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C623C283-1F9F-4884-88EA-DBE95E0F47D2")
    _IMainFramesEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IMainFramesEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IMainFramesEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IMainFramesEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IMainFramesEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IMainFramesEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IMainFramesEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IMainFramesEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IMainFramesEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IMainFramesEventsVtbl;

    interface _IMainFramesEvents
    {
        CONST_VTBL struct _IMainFramesEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IMainFramesEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IMainFramesEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IMainFramesEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IMainFramesEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IMainFramesEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IMainFramesEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IMainFramesEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IMainFramesEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_MainFrames;

#ifdef __cplusplus

class DECLSPEC_UUID("916E05E8-3639-4F29-9F2F-2DCCBDFB02D4")
MainFrames;
#endif

#ifndef ___IMdiContainerEvents_DISPINTERFACE_DEFINED__
#define ___IMdiContainerEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IMdiContainerEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IMdiContainerEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("3431CD02-BF63-4FEF-9330-1299AD5092DC")
    _IMdiContainerEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IMdiContainerEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IMdiContainerEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IMdiContainerEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IMdiContainerEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IMdiContainerEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IMdiContainerEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IMdiContainerEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IMdiContainerEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IMdiContainerEventsVtbl;

    interface _IMdiContainerEvents
    {
        CONST_VTBL struct _IMdiContainerEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IMdiContainerEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IMdiContainerEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IMdiContainerEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IMdiContainerEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IMdiContainerEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IMdiContainerEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IMdiContainerEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IMdiContainerEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_MdiContainer;

#ifdef __cplusplus

class DECLSPEC_UUID("DB1A794A-C776-4E25-9335-957865D691D0")
MdiContainer;
#endif

#ifndef ___IAddInsEvents_DISPINTERFACE_DEFINED__
#define ___IAddInsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IAddInsEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IAddInsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("99285823-F438-4048-AC79-FEFDCAC5D577")
    _IAddInsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IAddInsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IAddInsEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IAddInsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IAddInsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IAddInsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IAddInsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IAddInsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IAddInsEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IAddInsEventsVtbl;

    interface _IAddInsEvents
    {
        CONST_VTBL struct _IAddInsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IAddInsEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IAddInsEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IAddInsEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IAddInsEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IAddInsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IAddInsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IAddInsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IAddInsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_AddIns;

#ifdef __cplusplus

class DECLSPEC_UUID("3F028CDA-4847-4F26-87BE-F2811FE7DFB3")
AddIns;
#endif

#ifndef ___IStatusBarEvents_DISPINTERFACE_DEFINED__
#define ___IStatusBarEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IStatusBarEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IStatusBarEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("F4C4CD03-1E48-426C-B009-278C321D26B9")
    _IStatusBarEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IStatusBarEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IStatusBarEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IStatusBarEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IStatusBarEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IStatusBarEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IStatusBarEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IStatusBarEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IStatusBarEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IStatusBarEventsVtbl;

    interface _IStatusBarEvents
    {
        CONST_VTBL struct _IStatusBarEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IStatusBarEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IStatusBarEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IStatusBarEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IStatusBarEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IStatusBarEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IStatusBarEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IStatusBarEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IStatusBarEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_StatusBar;

#ifdef __cplusplus

class DECLSPEC_UUID("018BF279-9B97-4B20-BA25-6F9FDE58F039")
StatusBar;
#endif

EXTERN_C const CLSID CLSID_Extensibility;

#ifdef __cplusplus

class DECLSPEC_UUID("77DEDAF1-CCAF-4895-9A96-06A207817654")
Extensibility;
#endif

#ifndef ___IGenericPanelEvents_DISPINTERFACE_DEFINED__
#define ___IGenericPanelEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IGenericPanelEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IGenericPanelEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("E463136B-07B4-47B5-831A-1CCFC365201B")
    _IGenericPanelEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IGenericPanelEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IGenericPanelEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IGenericPanelEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IGenericPanelEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IGenericPanelEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IGenericPanelEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IGenericPanelEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IGenericPanelEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IGenericPanelEventsVtbl;

    interface _IGenericPanelEvents
    {
        CONST_VTBL struct _IGenericPanelEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IGenericPanelEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IGenericPanelEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IGenericPanelEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IGenericPanelEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IGenericPanelEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IGenericPanelEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IGenericPanelEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IGenericPanelEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_GenericPanel;

#ifdef __cplusplus

class DECLSPEC_UUID("72E30995-3517-4381-BC06-AE50D120A0A7")
GenericPanel;
#endif

#ifndef ___ITaskManagerEvents_DISPINTERFACE_DEFINED__
#define ___ITaskManagerEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ITaskManagerEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ITaskManagerEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("EF9CA8EF-A855-437B-8AB8-387E381895C2")
    _ITaskManagerEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ITaskManagerEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ITaskManagerEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ITaskManagerEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ITaskManagerEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ITaskManagerEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ITaskManagerEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ITaskManagerEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ITaskManagerEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ITaskManagerEventsVtbl;

    interface _ITaskManagerEvents
    {
        CONST_VTBL struct _ITaskManagerEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ITaskManagerEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ITaskManagerEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ITaskManagerEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ITaskManagerEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ITaskManagerEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ITaskManagerEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ITaskManagerEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ITaskManagerEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_TaskManager;

#ifdef __cplusplus

class DECLSPEC_UUID("F35A47C2-9B46-4588-A502-C0DCDF8193B8")
TaskManager;
#endif

#ifndef ___ISplitterWndEvents_DISPINTERFACE_DEFINED__
#define ___ISplitterWndEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ISplitterWndEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ISplitterWndEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("71259AC0-B787-4B21-9D9C-03C90FF680E7")
    _ISplitterWndEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ISplitterWndEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ISplitterWndEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ISplitterWndEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ISplitterWndEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ISplitterWndEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ISplitterWndEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ISplitterWndEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ISplitterWndEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ISplitterWndEventsVtbl;

    interface _ISplitterWndEvents
    {
        CONST_VTBL struct _ISplitterWndEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ISplitterWndEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ISplitterWndEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ISplitterWndEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ISplitterWndEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ISplitterWndEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ISplitterWndEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ISplitterWndEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ISplitterWndEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_SplitterWnd;

#ifdef __cplusplus

class DECLSPEC_UUID("7B7349E0-371F-46DB-B22D-ABD0644EA6BC")
SplitterWnd;
#endif

#ifndef ___IGenericPanelsEvents_DISPINTERFACE_DEFINED__
#define ___IGenericPanelsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IGenericPanelsEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IGenericPanelsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("E55D1E74-1D97-46A9-B940-CA3135E7AFB5")
    _IGenericPanelsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IGenericPanelsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IGenericPanelsEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IGenericPanelsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IGenericPanelsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IGenericPanelsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IGenericPanelsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IGenericPanelsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IGenericPanelsEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IGenericPanelsEventsVtbl;

    interface _IGenericPanelsEvents
    {
        CONST_VTBL struct _IGenericPanelsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IGenericPanelsEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IGenericPanelsEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IGenericPanelsEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IGenericPanelsEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IGenericPanelsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IGenericPanelsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IGenericPanelsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IGenericPanelsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_GenericPanels;

#ifdef __cplusplus

class DECLSPEC_UUID("7E81334A-133C-4279-AB6F-EE406981AEDB")
GenericPanels;
#endif

#ifndef ___IComFrameworkEvents_DISPINTERFACE_DEFINED__
#define ___IComFrameworkEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IComFrameworkEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IComFrameworkEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("6716391D-CD73-46CC-9545-699193B17BFE")
    _IComFrameworkEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IComFrameworkEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IComFrameworkEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IComFrameworkEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IComFrameworkEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IComFrameworkEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IComFrameworkEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IComFrameworkEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IComFrameworkEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IComFrameworkEventsVtbl;

    interface _IComFrameworkEvents
    {
        CONST_VTBL struct _IComFrameworkEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IComFrameworkEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IComFrameworkEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IComFrameworkEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IComFrameworkEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IComFrameworkEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IComFrameworkEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IComFrameworkEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IComFrameworkEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ComFramework;

#ifdef __cplusplus

class DECLSPEC_UUID("4A32E32C-1199-4754-93A0-53570ADFDCD3")
ComFramework;
#endif

EXTERN_C const CLSID CLSID_LanguagePropPage;

#ifdef __cplusplus

class DECLSPEC_UUID("EB55303F-B8E1-4F79-BF43-57750AFB89ED")
LanguagePropPage;
#endif

#ifndef ___ICommandGroupEvents_DISPINTERFACE_DEFINED__
#define ___ICommandGroupEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ICommandGroupEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ICommandGroupEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("43296964-48A1-4014-8C77-DDFABF4D5E68")
    _ICommandGroupEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ICommandGroupEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ICommandGroupEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ICommandGroupEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ICommandGroupEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ICommandGroupEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ICommandGroupEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ICommandGroupEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ICommandGroupEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ICommandGroupEventsVtbl;

    interface _ICommandGroupEvents
    {
        CONST_VTBL struct _ICommandGroupEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ICommandGroupEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ICommandGroupEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ICommandGroupEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ICommandGroupEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ICommandGroupEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ICommandGroupEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ICommandGroupEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ICommandGroupEvents_DISPINTERFACE_DEFINED__ */


#ifndef ___INavigationBarEvents_DISPINTERFACE_DEFINED__
#define ___INavigationBarEvents_DISPINTERFACE_DEFINED__

/* dispinterface _INavigationBarEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__INavigationBarEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("04480A56-10CA-4427-8A90-6C62184A2F54")
    _INavigationBarEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _INavigationBarEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _INavigationBarEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _INavigationBarEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _INavigationBarEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _INavigationBarEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _INavigationBarEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _INavigationBarEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _INavigationBarEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _INavigationBarEventsVtbl;

    interface _INavigationBarEvents
    {
        CONST_VTBL struct _INavigationBarEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _INavigationBarEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _INavigationBarEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _INavigationBarEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _INavigationBarEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _INavigationBarEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _INavigationBarEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _INavigationBarEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___INavigationBarEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_NavigationBar;

#ifdef __cplusplus

class DECLSPEC_UUID("4F6B451E-2A40-405F-885F-D78CE66ECB63")
NavigationBar;
#endif
#endif /* __ArtFrameworkLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  HBITMAP_UserSize(     unsigned long *, unsigned long            , HBITMAP * ); 
unsigned char * __RPC_USER  HBITMAP_UserMarshal(  unsigned long *, unsigned char *, HBITMAP * ); 
unsigned char * __RPC_USER  HBITMAP_UserUnmarshal(unsigned long *, unsigned char *, HBITMAP * ); 
void                      __RPC_USER  HBITMAP_UserFree(     unsigned long *, HBITMAP * ); 

unsigned long             __RPC_USER  HICON_UserSize(     unsigned long *, unsigned long            , HICON * ); 
unsigned char * __RPC_USER  HICON_UserMarshal(  unsigned long *, unsigned char *, HICON * ); 
unsigned char * __RPC_USER  HICON_UserUnmarshal(unsigned long *, unsigned char *, HICON * ); 
void                      __RPC_USER  HICON_UserFree(     unsigned long *, HICON * ); 

unsigned long             __RPC_USER  HMENU_UserSize(     unsigned long *, unsigned long            , HMENU * ); 
unsigned char * __RPC_USER  HMENU_UserMarshal(  unsigned long *, unsigned char *, HMENU * ); 
unsigned char * __RPC_USER  HMENU_UserUnmarshal(unsigned long *, unsigned char *, HMENU * ); 
void                      __RPC_USER  HMENU_UserFree(     unsigned long *, HMENU * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


