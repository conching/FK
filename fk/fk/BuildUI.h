#ifndef __BuildUI_h__
#define __BuildUI_h__



_FK_BEGIN


struct BUILDUITOOLBAR
{
	DWORD dwSize;
	ICommandBars* pbars;
	INotifyCommand* pncmd;
	HINSTANCE hInstRes;
	HINSTANCE hInst;
	LPCSTR pszPath;
	fkCommandRange* pCmdRange;
};


struct BUILDMENU
{
	DWORD dwSize;
	DWORD dwMask;
	HINSTANCE hInstRes;
	HINSTANCE hInst;
	LPCSTR pszPathXml;
	IPopupMenuBar* pPopupMenuBar;
	INotifyCommand* pncmd;
	UINT uiPos;
	enumInsertMenu umInsertMenu;
};

struct BUILDPOPUPMENUBAR
{
	DWORD dwSize;
	DWORD dwMask;
	IPopupMenuBar** ppPopupMenuBar;
	fk::LAutoPtr* pAutoPtr;
	LModule* pModule;
	INotifyCommand* pNotifyCommand;
	LPCSTR pszPathXml;
};

struct BUILDMAINMENU
{
	DWORD dwSize;
	DWORD dwMask;
	IMainFrame* pMainFrame;
	INotifyCommand* pParentNotifyCmd;			// or NULL
	INotifyCommand* pNotifyCmd;					// or NULL
	LPCSTR pszPathXml;							// no NULL
};

class _FK_OUT_CLASS LBuildUI : public LBaseBuildUI
{
public:

	LBuildUI(void);	
	virtual ~LBuildUI();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv)=0;

	virtual b_call SetBuildUIInstance(fk::LModule* pModule, HINSTANCE hInst) = 0;
	virtual b_call SetBuildUIFile(fk::LModule* pModule, LPCWSTR pszFile) = 0;
	virtual b_call BuildToolsBar(BUILDUITOOLBAR* pInfo) = 0;
	virtual b_call BuildUIMenu(BUILDMENU* pBuildMenu) = 0;
	virtual b_call BuildUIPopupMenuBar(BUILDPOPUPMENUBAR* pbmpm) = 0;
	virtual b_call InserMainMenu(BUILDMAINMENU* pBuildMainMenu) = 0;
	virtual fk::LModule* _call GetModule(void) = 0;

	static b_fncall NewBuildUI(fk::enumClassValue cvClassValue, fk::LBuildUI** ppv);
};


class _FK_OUT_CLASS LBuildUIAuto
{
private:
	LBaseBuildUI* _pbui;

public:
	LBuildUIAuto()
	{
	}

	LBuildUIAuto(LBaseBuildUI* pbui)
	{
		_pbui = pbui;
		if (_pbui!=NULL)
			_pbui->ResFileOpen();
	}

	void OpenRes(LBaseBuildUI* pbui)
	{
		_pbui = pbui;
		if (_pbui!=NULL)
			_pbui->ResFileOpen();
	}

	void CloseRes()
	{
		if (_pbui!=NULL)
			_pbui->ResFileClose();
	}

	virtual ~LBuildUIAuto()
	{
		if (_pbui!=NULL)
			_pbui->ResFileClose();
	}
};


_FK_END


#endif /* __BuildUI_h__ */