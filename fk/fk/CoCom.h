#ifndef __CO_COM_H__
#define  __CO_COM_H__


#ifdef __FK_USER__

#ifndef	__FK_TRACELOG__
#include "fk\atobj.hxx"
#define  CoCreateInstance	CoCreateInstancePtr
#else	//__FK_TRACELOG__
HRESULT WINAPI CoCreateInstancefk(IN REFCLSID rclsid,
								   IN LPUNKNOWN pUnkOuter,
								   IN DWORD dwClsContext,
								   IN REFIID riid,
								   OUT LPVOID FAR* ppv
								   );
#define  CoCreateInstance	CoCreateInstancefk
#endif		//__FK_TRACELOG__

#endif		//__FK_USER__

#endif		// __CO_COM_H__
