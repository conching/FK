//*****************************************************************************
//
//	FK应用程序框架 1.7
//
//	ArtMainFrame.h
//
//	Copyright (C) 2006-2007 Softg Studio All Rights Reserved.
//	
//	Website: http://www.conching.net
//
//  E-Mail: HotSoftG@hotmail.com
//
//	author: 许宗森
//
//	purpose:	
//
//*****************************************************************************
#ifndef __CommandBars_hxx__
#define __CommandBars_hxx__

#include "fk\CommandItem.hxx"
//#include "WndList.h"
//#include "CmdToolBar.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif


_FK_BEGIN
class CMenuBar;
class CCommandBars;

struct COMMANDBARSINFO
{
	DWORD dwSize;
	LONG_PTR hWndParent;
	wchar_t* pszTitle;
	enumCommandBarsType cbt;
	int iLeft;
	int iTop;
	int iRight;
	int iBottom;
};

class CCommandToolBarTemp : public fk::LToolBarCtrl
{
public:
	CCommandToolBarTemp(void);
	virtual ~CCommandToolBarTemp(void);

	FK_COM_ARQ();
};


class CCommandToolBar : public ICommandBar,
						public CCommandToolBarTemp,
						public fk::CCollectionWndItem<CCommandToolBar>
{
private:
protected:
public:
	IID		_iidKey;

	//
	// 这段代码，以后删除。和 fk::LToolBarCtrl::_hWnd 一样。在创建 ToolBarCtrl控件后，
	// 需要设置fk::LToolBarCtrl::_hWnd 到 m_hWnd 变量中。
	//
	HWND	m_hWnd;

	CCommandToolBar(void);
	virtual ~CCommandToolBar(void);

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual void SetParentCommandBars(CCommandBars *lpCommandBars) = 0;
	virtual void QueryStateItems( void ) = 0;
	virtual bool SetIIDKey(IID* piidKey) = 0;

	virtual INotifyCommand* GetNotifyCommand() = 0;
	virtual VARIANT_BOOL IsEnableCommandBar() = 0;
	virtual CCommandBars* GetCommandBars(void) = 0;
	virtual bool SetCommandItemInfo(UINT uiCmdID, CommandItemInfo* pcmdInfo) = 0;
	virtual CommandItemInfo* FindCommandItemInfo(UINT uiCmdID) = 0;
	virtual CommandItemInfo* CreateCommandItemInfo(UINT uiCmdID) = 0;

	//CommandItemInfo* CreateCommandItemInfo(UINT uiCmdID);
	//CommandItemInfo* FindCommandItemInfo(UINT uiCmdID);
	//bool SetCommandItemInfo(UINT uiCmdID, CommandItemInfo* pcmdInfo);
	//int AddBitmapIcon(HBITMAP hBitmap, HICON hIcon);

	static b_fncall NewCommandToolBar(fk::LObject* pparent, IID* piid, LPVOID* ppobj);
};

/*
命令工具条集合窗口。 
1.创建工具条集合。
3.设置 fk::INotifyCommand 类，用来接收工具条按钮命令。默认使用 FK::IMainFrame 在的时候，
4.创建工具条。

其它说明：CMenuBar 类不工作，不可使用。
*/
class CCommandBars : public ICommandBars,
	public fk::CCollectionWndItem<CCommandBars>
{
private:
protected:
public:
	HWND  m_hWnd;
	CCommandBars(void);
	virtual ~CCommandBars(void);

	virtual void CommandBarCreatePopup(COMMANDBARSINFO* pcbsInfo) = 0;
	virtual void SetActiveCommandToolBar(CCommandToolBar* pCommandToolBar) = 0;
	virtual void RemoveCommandToolBar(CCommandToolBar* pcmdToolBar) = 0;
	virtual void AddCommandToolBar(CCommandToolBar* pcmdToolBar) = 0;
	virtual void SetUpdateGuiState(bool bStop) = 0;
	virtual b_call GetUpdateGuiState() = 0;
	virtual void RemoveAll( void ) = 0;
	virtual v_call CloseCommandBars(void) = 0;
	virtual void DoCommand(ULONG ulcmd) = 0;

	virtual CMenuBar* GetMenuBar() = 0;						//其它说明：CMenuBar 类不工作，不可使用。
	virtual LCommandItemInfos* GetCmdInfos(void) = 0;

	virtual LRESULT OnInitMenuPopup(HMENU hMenu, UINT item, BOOL fSystemMenu) = 0;
	virtual LRESULT OnCommand(UINT codeNotify, int id, HWND hwndCtl) = 0;
};
bool WINAPI CreateCommandBarsObject(CCommandBars** ppCommandBars);
bool WINAPI DeleteCommandBarsObject(CCommandBars* ppCommandBars);

class LCommandBarsList : public ICommandBarsList
{
private:
protected:
public:
	LCommandBarsList(void);
	virtual ~LCommandBarsList(void);
};

LCommandBarsList* _fncall GetCommandBarsList(void);
INotifyCommands* _fncall GetNotifyCommands();

_FK_END

#endif
