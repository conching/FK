#ifndef __CommandLineLine_h__
#define __CommandLineLine_h__



_FK_BEGIN

enum enumCommandLineMask
{
	CLIF_EXIST_VALUE	= 0x00000004,			// 参数必须存在值。	ClAddCommandParam 方法设置。 COMMANDLINEITEM.dwMask，可以使用。
	CLIF_SHOWVALUE		= 0x00000005,			// 不带双引号的值， 主要用作显示。
	CLIF_ACTIVE_PARAM	= 0x00000010,			// 这个参数存在命令行中，已经被解析过了。其中包括 CLIF_USER_PARAM 标记的参数。
	CLIF_USER_PARAM		= 0x00000020,			// 这个参数不是预设的参数，但是是参数格式，可能是参数名称输入错误，或者是用户自定义参数。
};

//
typedef struct tagCOMMANDLINEITEM
{
	DWORD dwSize;
	DWORD dwMask;						// 目前设置为 0.
	int iPathIndex;						// 第几个路径。
	int iParamIndex;					// 第几个参数。
	fk::LStringw sParam;				// 解析出来的选项。	
	fk::LStringw sValue;				// 解析出来的值。这个值可能会是空。
	fk::LStringw sShowValue;			// 解析出来的值。
	fk::LStringw sInfo;					// 命令说明。
}COMMANDLINEITEM,*LPCOMMANDLINEITEM;


typedef void*  FCOMMANDLINE;

FCOMMANDLINE  _fncall ClCreateCommandLine();
b_fncall ClDestroyCommandLine(FCOMMANDLINE fCommandLine);

b_fncall ClSetCommandLine(FCOMMANDLINE fCommandLine, LPCWSTR lpszCommandLine);

i_fncall ClGetCount(FCOMMANDLINE fCommandLine);

fk::LPCOMMANDLINEITEM _fncall ClFindParam(FCOMMANDLINE fCommandLine, LPCWSTR lpszParam);
fk::LPCOMMANDLINEITEM _fncall ClFindActiveParam(FCOMMANDLINE fCommandLine, LPCWSTR lpszParam);
fk::LPCOMMANDLINEITEM _fncall ClFindParamFromIndex(FCOMMANDLINE fCommandLine, int iParamIndex);
i_fncall ClGetParamCount(FCOMMANDLINE fCommandLine);

LPCOMMANDLINEITEM _fncall ClFindPathFromIndex(FCOMMANDLINE fCommandLine, int iPathIndex);
i_fncall ClGetPathCount(FCOMMANDLINE fCommandLine);

data_t* _fncall ClGetDatas(FCOMMANDLINE fCommandLine);


b_fncall ClSetFileNameChar(FCOMMANDLINE fCommandLine, LPCWSTR lpszFileNameChar);
b_fncall ClReadFileNameChar(FCOMMANDLINE fCommandLine);

b_fncall ClAddParam(FCOMMANDLINE fCommandLine, DWORD dwMask, LPCWSTR lpszParam, LPCWSTR lpszInfo);

_FK_END


#endif			// __CommandLineLine_h__
