// FileVersion.h: interface for the CFileVersion class.
// by Lichengyi2002@msn.com
//////////////////////////////////////////////////////////////////////

#ifndef __FILEVERSION_H_
#define __FILEVERSION_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

_FK_BEGIN

#pragma comment(lib, "version.lib")



class CFileVersion
{ 
public: 
	//    CString QueryValue(LPCTSTR lpszValueName, DWORD dwLangCharset=0);
	CString GetFileDescription() 
	{
		return QueryValue(_T("FileDescription"));
	};
	CString GetFileVersion()
	{
		return QueryValue(_T("FileVersion"));
	};
	CString GetInternalName()
	{
		return QueryValue(_T("InternalName"));
	};
	CString GetCompanyName()
	{
		return QueryValue(_T("CompanyName"));
	}; 
	CString GetLegalCopyright()
	{
		return QueryValue(_T("LegalCopyright"));
	};
	CString GetOriginalFilename()
	{
		return QueryValue(_T("OriginalFilename"));
	};
	CString GetProductName()
	{
		return QueryValue(_T("ProductName"));
	};
	CString GetProductVersion()
	{
		return QueryValue(_T("ProductVersion"));
	};

	CFileVersion() 
	{ 
		m_lpVersionData=NULL;
		m_dwLangCharset=0;
	}

	~CFileVersion() 
	{ 
		Close();
	} 


	void Close()
	{
		delete[] m_lpVersionData; 
		m_lpVersionData=NULL;
		m_dwLangCharset=0;
	}

	BOOL Open(HINSTANCE hInstance)
	{
		TCHAR lpszPath[MAX_PATH]=_T("");
		::GetModuleFileName(hInstance, lpszPath, MAX_PATH);
		return Open( lpszPath );
	}

	BOOL Open(LPCTSTR lpszModuleName)
	{
		ATLASSERT(_tcslen(lpszModuleName) > 0);
		ATLASSERT(m_lpVersionData == NULL);

		// Get the version information size for allocate the buffer
		DWORD dwHandle;     
		DWORD dwDataSize=::GetFileVersionInfoSize((LPTSTR)lpszModuleName, &dwHandle); 
		if( dwDataSize == 0 ) 
			return FALSE;

		// Allocate buffer and retrieve version information
		m_lpVersionData=new BYTE[dwDataSize]; 
		if(!::GetFileVersionInfo((LPTSTR)lpszModuleName, dwHandle, dwDataSize, 
			(void**)m_lpVersionData) )
		{
			Close();
			return FALSE;
		}

		// Retrieve the first language and character-set identifier
		UINT nQuerySize;
		DWORD* pTransTable;
		if(!::VerQueryValue(m_lpVersionData, _T("\\VarFileInfo\\Translation"),
			(void **)&pTransTable, &nQuerySize) )
		{
			Close();
			return FALSE;
		}

		// Swap the words to have lang-charset in the correct format
		m_dwLangCharset=MAKELONG(HIWORD(pTransTable[0]), LOWORD(pTransTable[0]));

		return TRUE;
	}

	CString QueryValue(LPCTSTR lpszValueName, DWORD dwLangCharset=0)
	{
		// Must call Open() first
		ATLASSERT(m_lpVersionData != NULL);
		if( m_lpVersionData == NULL )
			return (CString)_T("");

		// If no lang-charset specified use default
		if( dwLangCharset == 0 )
			dwLangCharset=m_dwLangCharset;

		// Query version information value
		UINT nQuerySize;
		LPVOID lpData;
		CString strValue, strBlockName;
		strBlockName.Format(
			_T("\\StringFileInfo\\%08lx\\%s"), 
			dwLangCharset,
			lpszValueName
			);
		if( ::VerQueryValue((void **)m_lpVersionData, strBlockName.GetBuffer(0), 
			&lpData, &nQuerySize) )
			strValue=(LPCTSTR)lpData;

		strBlockName.ReleaseBuffer();


		return strValue;
	}

	BOOL GetFixedInfo(VS_FIXEDFILEINFO& vsffi)
	{
		// Must call Open() first
		ATLASSERT(m_lpVersionData != NULL);
		if( m_lpVersionData == NULL )
			return FALSE;

		UINT nQuerySize;
		VS_FIXEDFILEINFO* pVsffi;
		if( ::VerQueryValue(
			(void **)m_lpVersionData,
			_T("\\"),
			(void**)&pVsffi,
			&nQuerySize)
			)
		{
			vsffi=*pVsffi;
			return TRUE;
		}

		return FALSE;
	}

	CString GetFixedFileVersion()
	{
		CString strVersion;
		VS_FIXEDFILEINFO vsffi;

		if( GetFixedInfo(vsffi) )
		{
			strVersion.Format(
				_T("%u.%u.%u.%u"),
				HIWORD(vsffi.dwFileVersionMS),
				LOWORD(vsffi.dwFileVersionMS),
				HIWORD(vsffi.dwFileVersionLS),
				LOWORD(vsffi.dwFileVersionLS)
				);
		}
		return strVersion;
	}

	CString GetFixedProductVersion()
	{
		CString strVersion;
		VS_FIXEDFILEINFO vsffi;

		if( GetFixedInfo(vsffi) )
		{
			strVersion.Format(
				_T("%u.%u.%u.%u"),
				HIWORD(vsffi.dwProductVersionMS),
				LOWORD(vsffi.dwProductVersionMS),
				HIWORD(vsffi.dwProductVersionLS),
				LOWORD(vsffi.dwProductVersionLS)
				);
		}

		return strVersion;
	}


	// Attributes
protected:
	LPBYTE  m_lpVersionData; 
	DWORD   m_dwLangCharset; 
}; 


enum enumCompVersion
{
	enumCompVersionError=0x0,
	enumCompVersionMinServer=0x1,			// 本地版本小于服务器
	enumCompVersionMaxServer=0x2,			// 本地版本大于服务器
	enumCompVersionSameServer=0x3,		// 版本一样。
};


typedef struct tagSAF_FILE_VERSION
{
	UINT nMajor;
	UINT nMinor;
	UINT nRevision;
	UINT nBuild;
}SAF_FILE_VERSION, *PSAF_FILE_VERSION;

#define SAF_STR_VERSION_SPLIT	_T(".")

class CSmallFileVersion : public SAF_FILE_VERSION
{
public:
	CSmallFileVersion()
	{
		memset( this, 0, sizeof(SAF_FILE_VERSION) );
	}
	~CSmallFileVersion()
	{
	}

	//	函数名称：
	//	功能：比较版本号。
	//	参数：
	//
	//	返回值：
	//	< 0		buf1 less than buf2
	//	0		buf1 identical to buf2
	//	> 0		buf1 greater than buf2

	enumCompVersion CompareVersion(SAF_FILE_VERSION &safFileVersion)
	{
		if (	(this->nMajor < safFileVersion.nMajor) ||
			(this->nMinor < safFileVersion.nMinor) ||
			(this->nRevision < safFileVersion.nRevision) ||
			(this->nBuild < safFileVersion.nBuild)	)
		{
			return enumCompVersionMinServer;
		}
		else if( (this->nMajor > safFileVersion.nMajor) ||
			(this->nMinor > safFileVersion.nMinor) ||
			(this->nRevision > safFileVersion.nRevision) ||
			(this->nBuild > safFileVersion.nBuild)	)
		{
			return enumCompVersionMaxServer;
		}
		else if( (this->nMajor == safFileVersion.nMajor) &&
			(this->nMinor == safFileVersion.nMinor) &&
			(this->nRevision == safFileVersion.nRevision) &&
			(this->nBuild == safFileVersion.nBuild)	)
		{
			return enumCompVersionSameServer;
		}

		return enumCompVersionError;
	}

	bool ToString(fk::LStringw& sVerInfo, LPCTSTR lpszSplit)
	{
		if (sVerInfo.Format(_T("%d%s%d%s%d%s%d"),
			this->nMajor, lpszSplit,
			this->nMinor, lpszSplit,
			this->nRevision, lpszSplit,
			this->nBuild, lpszSplit) >= 1)
		{
			return true;
		}

		return false;
	}

	bool ToString3(fk::LStringw& sVerInfo, LPCTSTR lpszSplit)
	{
		sVerInfo.Format(_T("%d%s%d%s%d"), this->nMajor, lpszSplit,
			this->nMinor, lpszSplit, this->nRevision, lpszSplit);

		return true;
	}

	BOOL SetFileVersion(CFileVersion &FileVersion)
	{
		VS_FIXEDFILEINFO vsFixedFileInfo={0};

		if(FileVersion.GetFixedInfo( vsFixedFileInfo ))
		{
			nMajor=HIWORD(vsFixedFileInfo.dwFileVersionMS);
			nMinor=LOWORD(vsFixedFileInfo.dwFileVersionMS);
			nRevision=HIWORD(vsFixedFileInfo.dwFileVersionLS);
			nBuild=LOWORD(vsFixedFileInfo.dwFileVersionLS);
			return TRUE;
		}

		return FALSE;
	}

};


} //end namespace art;

#endif  // __FILEVERSION_H_
