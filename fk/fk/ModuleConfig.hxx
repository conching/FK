#ifndef __ModuleConfig_hxx__
#define __ModuleConfig_hxx__

#include <windows.h>
#include "fk\guid.hxx"
#include "fk\XmlItem.hxx"

/*	ModuleConfig.hxx 文件中，只有一个 DnaLink 不同功能连接点基类，需要用户继承。*/

_FK_BEGIN
// {B5A0C4F9-09B1-472d-ABEF-9A3B44162988}
static const IID IID_DnaLink = { 0xb5a0c4f9, 0x9b1, 0x472d, { 0xab, 0xef, 0x9a, 0x3b, 0x44, 0x16, 0x29, 0x88 } };

// {956A6F3D-62FD-4595-8096-B1A3C9C60B63}
static const IID IID_DnaItemFull = { 0x956a6f3d, 0x62fd, 0x4595, { 0x80, 0x96, 0xb1, 0xa3, 0xc9, 0xc6, 0xb, 0x63 } };

// {053D888B-8DE5-45bd-B4E7-B9759333C165}
static const IID IID_ModuleConfig = { 0x53d888b, 0x8de5, 0x45bd, { 0xb4, 0xe7, 0xb9, 0x75, 0x93, 0x33, 0xc1, 0x65 } };

// {B5F10450-446B-4BE5-920E-754D16D08E70}
static const IID IID_ModuleConfigRegsvrItem = { 0xb5f10450, 0x446b, 0x4be5, { 0x92, 0xe, 0x75, 0x4d, 0x16, 0xd0, 0x8e, 0x70 } };

// {71DEC2BF-66C2-4d2c-A1FF-5336EE179BDE}
static const IID IID_ModuleConfigManager = { 0x71dec2bf, 0x66c2, 0x4d2c, { 0xa1, 0xff, 0x53, 0x36, 0xee, 0x17, 0x9b, 0xde } };

// {8902494D-2104-4694-89E8-BC5B1378299A}
static const IID IID_DnaClientLink = { 0x8902494d, 0x2104, 0x4694, { 0x89, 0xe8, 0xbc, 0x5b, 0x13, 0x78, 0x29, 0x9a } };

// {0FA9AF12-42FE-48ca-B737-7A1A4788C9F8}
static const IID IID_DnaClientLinkManager = { 0xfa9af12, 0x42fe, 0x48ca, { 0xb7, 0x37, 0x7a, 0x1a, 0x47, 0x88, 0xc9, 0xf8 } };

// {7030DD41-8B72-444F-AB76-E92ADAD8B4F6}
static const IID IID_DnaPosition = { 0x7030dd41, 0x8b72, 0x444f, { 0xab, 0x76, 0xe9, 0x2a, 0xda, 0xd8, 0xb4, 0xf6 } };

// {23904577-987F-473A-A60A-B490BF339BCB}
static const IID IID_DnaPositionFullManager = { 0x23904577, 0x987f, 0x473a, { 0xa6, 0xa, 0xb4, 0x90, 0xbf, 0x33, 0x9b, 0xcb } };

// {17889363-8235-44f6-942A-94BE40FF8745}
static const IID IID_DnaItem = { 0x17889363, 0x8235, 0x44f6, { 0x94, 0x2a, 0x94, 0xbe, 0x40, 0xff, 0x87, 0x45 } };

// {BDED4B23-454A-4287-B5E5-21B8DB71C425}
static const IID IID_DnaClientLinkFullManager = { 0xbded4b23, 0x454a, 0x4287, { 0xb5, 0xe5, 0x21, 0xb8, 0xdb, 0x71, 0xc4, 0x25 } };

// {E3EE258B-BD2F-4604-9812-398E6D997977}	// 控件属性链接点ID。
static const IID IID_DnaPositionControlProp	= {0xe3ee258b, 0xbd2f, 0x4604, { 0x98, 0x12, 0x39, 0x8e, 0x6d, 0x99, 0x79, 0x77 } };

// {9817FC91-D75D-4268-9122-676A7C3D9CE4}
static const IID IID_DnaLinks = { 0x9817fc91, 0xd75d, 0x4268, { 0x91, 0x22, 0x67, 0x6a, 0x7c, 0x3d, 0x9c, 0xe4 } };

// {8C4959EA-0CBA-4F18-95FD-981F2971D866}
static const GUID IID_ModuleConfigRegister = { 0x8c4959ea, 0xcba, 0x4f18, { 0x95, 0xfd, 0x98, 0x1f, 0x29, 0x71, 0xd8, 0x66 } };

class LDnaClientLinkFullManager;
class LDnaClientLinkManager;			// the file
class LDnaClientLink;
class LDnaPosition;
class LModuleConfig;
class LModuleConfigManager;
class LDnaLink;
class LDnaLinks;

/*
//
// 通用所有的链接项的信息。
//
数据项信息分为两种，第一种直接指定某种数据 XmlPath 位置。第二种是直接指定动态链接库文件名称和函数名称。
这两种功能都可以独立的存在，但是第二种可以包含第一种。
执行第二种功能后，如果第一种功能存在，就可以继续执行第一种。
*/
class _FK_OUT_CLASS LDnaItem : public fk::LObject
{
private:
	DWORD				_dwItemMake;		//项的掩码。

	DWORD				_dwPropMake;		// xml Node 项的掩码，
	fk::LModuleConfig*	_pModuleConfig;		// 链接项所在的配置文件。
	fk::LDnaClientLink*	_pdnaClientLink;	// 指向当前DNA File的指针。
	fk::HXMLNODE        _xmlNode;			// ReadItem 函数设置。

	fk::LDnaPosition*	_pdnaPosition;		// 链接项所在链接点。

	// 属性组1
	fk::LStringw		_sfrl;				// 链接者真实文件位置，数据的信息可能是不一样地.

	// 属性组2
	fk::XmlItemRunl*	_pxmlItemRunl;
	fk::XmlItemCom*		_pxmlItemCom;

public:
	LDnaItem(fk::LModuleConfig* pModuleConfig, fk::LDnaPosition* pdnaPosition, fk::LDnaClientLink* pdnaClientLink);
	virtual ~LDnaItem();

	enum enumDnaItemType
	{
		umditError		= 0x0001,
	    umditFrl		= 0x0002,		// 只有第一种属性组。
	    umditRul		= 0x0003,		// 只有第二种属性组。
	    umditRunlFrl	= 0x0004,		// 存在两种属性组。
		umditCom		= 0x0005,		// 是COM 属性组。
		umditComFrl		= 0x0006,		// 是Com 属性组，包含 frl。
	};

	float				_ffkver;
	float				_fsver;
	fk::LStringw		_sfkver;
	fk::LStringw		_ssver;
	fk::LStringw		_sInfo;			// 链接者信息。
	fk::LStringw		_sid;			// 非必写 id 属性，用来区分此链接属性项，是同类中的那一个。

	fk::LObject*        _pItemData;

	v_call clear(void);					// 清空数据信息。
	b_call ReadItem(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNodeLinkItem, fk::LXmlPropError* xpe);		// 读当前链接项的数据。

    b_call GetRunlFullPath(fk::LStringw* psFullPath);

	//virtual b_call RunItem(void);					// 存在第二种属性组，就执行链接的操作。	操作成功返回 true,失败返回 false.

	virtual b_call unlink(void);                  // 链接项不可以用。
	virtual b_call link(void);                    // 链接项可以用。
	virtual b_call IsLink(void);

	virtual fk::rwstring* GetFrl(void);
	virtual dw_call GetPropMake(void);

	virtual fk::XmlItemRunl* GetXmlItemRunl(void);
	virtual fk::XmlItemCom* GetXmlItemCom(void);

	virtual fk::LModuleConfig* GetModuleConfig(void);
	virtual fk::LDnaPosition* GetDnaPosition(void);
	virtual fk::LDnaClientLink* GetClientLink(void);

	virtual fk::LDnaItem::enumDnaItemType _call GetDnaItemType(void);

	static b_fncall NewDnaItem(fk::LModuleConfig* pModuleConfig,
		fk::LDnaPosition* pdnaPosition, 
		fk::LDnaClientLink* pdnaClientLink, 
		fk::LDnaItem** ppdnaItem
		);
};

class _FK_OUT_CLASS LDnaItemFull : public fk::LObject
{
private:
public:
	LDnaItemFull(fk::LObject* pParentObject);
	virtual ~LDnaItemFull(void);

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iItem, fk::LDnaItem** ppDnaItem) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;
	virtual data_t* GetDatas(void) = 0;

	virtual b_call AddDnaItem(fk::LDnaItem* pDnaItem) = 0;
	virtual b_call RemoveDnaItem(fk::LDnaItem* pDnaItem) = 0;

	static b_fncall NewDnaItemFull(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumNotify
	{
		ON_ADD			= 0x00000001,
		ON_REMOVE		= 0x00000002,
		ON_UPDATE		= 0x00000004,
		ON_UPDATE_ITEM	= 0x00000008,
	};

	typedef struct tagAdd
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaItem*	pdnaItem;
	}ADD,*LPADD;

	typedef struct tagRemove
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaItem*	pdnaItem;
	}Remove,*LPRemove;

	typedef struct tagUpdate
	{
		fk::NOTIFYEVENT     ne;
		fk::LDnaItemFull*	pdnaItemFull;
	}Update,*LPUpdate;

	typedef struct tagUpdateItem
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaItem*	pdnaItem;
	}UpdateItem,*LPUpdateItem;
};

//////////////////////////////////////////////////////////////////////////
/*
    一个 链接点 的信息和，链接点下所有链接者对象 fk::LDnaItem 指针，
    真实的 fk::LDnaItem 是在 fkDnaPositionmanager。
*/
class _FK_OUT_CLASS LDnaPosition : public fk::LComponent
{
private:
public:
	enum enumDnaPositionMake
	{
		DPF_Enabled			= 0x00000001,				// 活动状态。
		DPF_Save			= 0x00000002,				// 链接点保存在模块集合中。
		DPF_DesignCreate	= 0x00000004,				// 设计期创建的。
		DPF_DynamicsCreate	= 0x00000008,				// 程序执行期创建的。
		DPF_Load			= 0x00000010,				// 链接点是否载入过链接类 fk::DnaLink
	};

public:
	LDnaPosition(fk::LObject* pParentObject);
	virtual ~LDnaPosition(void);

	virtual v_call clear(void) = 0;
	virtual b_call GetDnaPositionName(fk::LVar* ptext) = 0;

	virtual b_call SetDnaGuid(LPCWSTR lpszGuid) = 0;
	virtual fk::LGuid* _call GetDnaGuid(void) = 0;

	virtual b_call SetInfo(LPCWSTR lpszFrl) = 0;
	virtual b_call GetInfo(fk::LVar* psinfo) = 0;

	virtual b_call SetFrl(LPCWSTR lpszFrl) = 0;
	virtual b_call GetFrl(fk::LVar* pFrl) = 0;

	virtual fk::HXMLNODE _call GetXmlNode(void) = 0;

	virtual b_call ReadXmlNode(fk::LModuleConfig* pModuleConfig, fk::LXmlFile* pxmlFile, fk::HXMLNODE hXmlNode) = 0;
	virtual b_call SaveXmlNode(void) = 0;
	virtual b_call SaveXmlNodeAs(fk::LModuleConfig* pModuleConfig, fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode) = 0;

	virtual b_call AddDnaItem(fk::LDnaItem* pdnaItem) = 0;
	virtual b_call RemoveItem(fk::LDnaItem* pdnaItem) = 0;

	virtual b_call AddDnaClientLink(fk::LDnaClientLink* pdnaClientLink) = 0;
	virtual b_call RemoveDnaClientLink(fk::LDnaClientLink* pdnaClientLink) = 0;

	virtual i_call GetCount(void) = 0;
	virtual fk::LDnaItem* GetItem(int iitem) = 0;
	virtual data_t* _call GetDatas(void) = 0;
	virtual b_call GetEnumDatas(fk::LEnumData** ppEnumData) = 0;

	virtual fk::LModuleConfig* GetModuleConfig(void) = 0;		// 链接点所在配置文件对象。
	virtual b_call GetDnaLink(fk::LDnaLink** ppdnaLink) = 0;		// 获得连接点关联的链接占操作类。
	virtual b_call IsLoad(void) = 0;							// 判断链接点是否已经载入过。
	virtual b_call LoadDnaLink(uint uiorder) = 0;				// 根据链接点的顺序载入链接点。
	virtual b_call UnloadDnaLink(uint uiorder) = 0;				// 卸载载入的链接点。
	virtual ui_call GetDnaPositionOrder(void) = 0;

	virtual fk::XmlItemCom* GetXmlItemCom(void) = 0;

	static b_fncall NewDnaPosition(fk::LModuleConfig* pModuleConfig, fk::LDnaLinks* pdnaLinks, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumNotify
	{
		ON_ADD			= 0x00000001,
		ON_REMOVE		= 0x00000002,
		ON_UPDATE		= 0x00000004,
		ON_UPDATE_ITEM	= 0x00000008,
	};

	typedef struct tagAdd
	{
		fk::NOTIFYEVENT en;
		fk::LDnaItem*	pdnaItem;
	}ADD,*LPADD;

	typedef struct tagRemove
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaItem*	pdnaItem;
	}Remove,*LPRemove;

	typedef struct tagUpdate
	{
		fk::NOTIFYEVENT     ne;
		fk::LDnaPosition*	pdnaPosition;
	}Update,*LPUpdate;

	typedef struct tagUpdateItem
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaItem*	pdnaItem;
	}UpdateItem,*LPUpdateItem;
};

//
// DnaLink 类是每一个不同功能连接点基类。是需要用户继承此类，建立新的类。
//
class _FK_OUT_CLASS LDnaLink : public fk::LObject
{
private:
protected:
	fk::LDnaPosition* _pdnaPosition;

public:
	LDnaLink(fk::LObject* pparent);
	virtual ~LDnaLink(void);

	// 此函数只调用一次，第二次调用没有效果，
	b_call LinkDnaPosition(fk::LDnaPosition* pdnaPosition);		// 创建链接点fk::DnaLink类后，执行LinkDnaPosition函数，连接 fk::DnaPosition 
	b_call GetDnaPosition(fk::LDnaPosition** ppdnaPosition);

	virtual b_call LoadDnaLink(void) = 0;						// 载入链接点 fk::DnaLink 的数据，执行一些功能。有用户制定。
	virtual b_call UnloadDnaLink(void) = 0;						// 卸载链接点 fk::DnaLink.LoadDnaLink 的数据，执行一些功能。有用户制定。
public:
};

class _FK_OUT_CLASS LDnaLinks : public fk::LObject
{
private:
protected:
public:
	LDnaLinks(fk::LModuleConfigManager* pModuleConfigManager);
	virtual ~LDnaLinks(void);

	virtual b_call AddItem(fk::LDnaLink* pdnaLink) = 0;
	virtual b_call RemoveItem(fk::LDnaLink* pdnaLink) = 0;

	virtual i_call GetItemCount(void) = 0;
	virtual b_call GetItem(int iitem, fk::LDnaLink** ppdnaLink) = 0;
	virtual data_t* _call GetItemDatas(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	virtual b_call LoadDnaLinks(void) = 0;
	virtual b_call UnloadDnaLinks(void) = 0;

	static b_fncall NewDnaLinks(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj _FK_OBJECT_POS_H2_);
};


//
// 管理一个可执行程序的所有链接点的索引操作类。只记亿每一个链接点指针，并不是真实数据，
// 真实的链接点分别都在每一个配置文件操作类中ModuleConfig.
// 此对象做为全局对象存在 Application 中。
//
class _FK_OUT_CLASS LDnaPositionFullManager : public fk::LObject
{
private:
public:
	LDnaPositionFullManager(fk::LObject* pParentObject);
	virtual ~LDnaPositionFullManager(void);

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iitem, fk::LDnaPosition** pDnaPosition) = 0;
	virtual data_t* GetDatas(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	// 添加一个已经存的链接点指针。
	// @param pdnaPosition 链接点指针。
	virtual b_call AddDnaPositionPtr(fk::LDnaPosition* pdnaPosition) = 0;
	virtual b_call RemoveDnaPositionPtr(fk::LDnaPosition* pdnaPosition) = 0;

	virtual b_call FindDnaPosition(fk::LDnaPosition* pdnaPosition) = 0;
	virtual b_call FindDnaPosition(fk::LGuid* pguid, fk::LDnaPosition** ppdnaPosition) = 0;
	//virtual b_call FindDnaPositionManager(fk::ModuleConfig* pModuleConfig) = 0;

	static  b_fncall NewDnaPositionFullManager(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumNotify
	{
		ON_ADD = 0x00000001,       // 添加一个链接点。
		ON_REMOVE = 0x00000002,       // 移出一个链接点。
		ON_UPDATE_ITEM = 0x00000004,       // 更新某一个链接点数据。
		ON_UPDATE = 0x00000010,       // 更新所有的数据。
	};
	typedef struct tagADD
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaPositionFullManager* pdnaPositionFullManager;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}ADD, *LPADD;
	typedef struct tagREMOVE
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaPositionFullManager* pdnaPositionFullManager;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}REMOVE, *LPREMOVE;
	typedef struct tagUPDATE_ITEM
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaPositionFullManager* pdnaPositionFullManager;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}UPDATE_ITEM, *LPUPDATE_ITEM;
	typedef struct tagUPDATE
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaPositionFullManager* pdnaPositionFullManager;
		fk::LModuleConfig* pModuleConfig;
	}UPDATE, *LPUPDATE;

};

//
// 某一个客户链接者需要注册到链接点的操作类。只有两个 xml 文件属性 make 和 dna ，这两个属性不能写，只能从 xml 文件中读。
// 节点：//config/client/links/link
//
class _FK_OUT_CLASS LDnaClientLink : public fk::LObject
{
private:

public:
	LDnaClientLink(fk::LObject* pParentObject);
	virtual ~LDnaClientLink(void);

	fk::HXMLNODE		_hxmlNode;				// 当前节点。 //config/client/links/link
	fk::HXMLNODE		_hxmlNodeItems;

	DWORD				_dwMakeClientLink;		//

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iItem, fk::LDnaItem** ppDnaItem) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;
	virtual data_t* _call GetDatas(void) = 0;

	virtual fk::LGuid* _call GetDnaPositionGuid(void) = 0;
	virtual fk::rwstring* _call GetName(void) = 0;

	virtual b_call add(fk::LDnaItem* pDnaItemIn) = 0;
	virtual b_call remove(fk::LDnaItem* pDnaItem) = 0;

	virtual b_call SetXmlNode(fk::HXMLNODE hxmlNode, fk::HXMLNODE hxmlNodeItems) = 0;

	virtual b_call GetXmlNode(fk::HXMLNODE* pphxmlNode) = 0;
	virtual b_call GetXmlNodeItems(fk::HXMLNODE* pphxmlNodeItems) = 0;

	/*
        pdnaPosition 中的 dna IID值和当前 DnaClientLink 的 dna IID 值一样。就把所有的链接者对象指针添加到 dnaPosition 中，让他们这间产生关联。
	*/
	virtual b_call BuildingPositionNexu(fk::LDnaPosition* pdnaPosition) = 0;

	virtual fk::enumReturn _call ReadClientLink(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, fk::LDnaClientLinkManager* pDnaClientManager, fk::HXMLNODE hxmlNode) = 0;

    static b_fncall NewDnaClientLink(fk::LModuleConfig* pModuleConfig, fk::LDnaClientLinkManager* pdnaClientLinkManager, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);
};

/////////////////////////////////////////////////////////////////////////////////////////
//
// 客户链接者组集合。 没有 xml 文件属性。
// 固定节点： //config/dna/client/links
//
class _FK_OUT_CLASS LDnaClientLinkManager : public fk::LObject
{
private:
public:
	LDnaClientLinkManager(fk::LObject* pParentObject);
	virtual ~LDnaClientLinkManager(void);

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iItem, fk::LDnaClientLink** ppDnaClientLink) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	virtual b_call AddClientLink(fk::LDnaPosition* pdnaPosition, fk::LDnaClientLink** ppDnaClient) = 0;
	virtual b_call RemoveClientLink(fk::LDnaClientLink* pdnaClientLink) = 0;

	virtual fk::enumReturn _call ReadClientLinkFull(fk::LModule* pmodule, fk::LXmlFile* pxmlFile) = 0;

	virtual fk::LModuleConfig* _call GetModuleConfig(void) = 0;

    static b_fncall NewDnaClientLinkManager(
		fk::LModuleConfigManager* pModuleConfigManager,
		fk::LModuleConfig* pModuleConfig,
		LPVOID* ppobj,
		REFIID iid  _FK_OBJECT_POS_H2_
		);

public:
	enum enumNotify
	{
		ON_ADD_CLIENT_LINK		= 0x00000001,			// 添加一个 fk::DnaClientLink 对象。
		ON_REMOVE_CLIENT_LINK	= 0x00000002,			// 移出一个 fk::DnaClientLink 对象。
		ON_UPDATE_DATA			= 0x00000004,			// 更新数据，在调用 ReadClientManager 方法结束后发送。
		ON_ADD_DNA_ITEM			= 0x00000008,
		ON_REMOVE_DNA_ITEM		= 0x00000010,
	};

	typedef struct tagADD_CLIENT_LINK
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
	}ADD_CLIENT_LINK,*LPADD_CLIENT_LINK;

	typedef struct tagREMOVE_CLIENT_LINK
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
	}REMOVE_CLIENT_LINK,*LPREMOVE_CLIENT_LINK;

	typedef struct tagUPDATE_DATA
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
	}UPDATE_DATA,*LPRUPDATE_DATA;
};

//
// 管理当前进程配置集合中的所有 fk::DnaClientLink 节点，
//
class _FK_OUT_CLASS LDnaClientLinkFullManager : public fk::LObject
{
private:
public:
	LDnaClientLinkFullManager(fk::LObject* pParentObject);
	virtual ~LDnaClientLinkFullManager(void);

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iItem, fk::LDnaClientLink** ppDnaClientLink) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	virtual b_call AddClientLink(fk::LDnaClientLink* ppDnaClient) = 0;
	virtual b_call RemoveClientLink(fk::LDnaClientLink* pdnaClientLink) = 0;

	// 比较每一个 fk::DnaClientLinkc对象的 dna IID，是否和 参数pdnaPosition的 dna IID相对，
	// 就把所有的链接者对象指针添加到 dnaPosition 中，让他们这间产生关联。
//	virtual b_call BuildingPositionNexu(fk::DnaPosition* pdnaPosition) = 0;
	virtual b_call BuildingDnaPositionNexus(void) = 0;

    static b_fncall NewDnaClientLinkFullManager(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumNotify
	{
		ON_ADD_CLIENT_LINK		= 0x00000001,			// 添加一个 fk::DnaClientLink 对象。
		ON_REMOVE_CLIENT_LINK	= 0x00000002,			// 移出一个 fk::DnaClientLink 对象。
		ON_UPDATE_DATA			= 0x00000004,			// 更新数据，在调用 ReadClientManager 方法结束后发送。
		ON_ADD_DNA_ITEM			= 0x00000008,
		ON_REMOVE_DNA_ITEM		= 0x00000010,
	};

	typedef struct tagADD_CLIENT_LINK
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
		fk::LDnaClientLinkFullManager* pClientLinkFullManager;
	}ADD_CLIENT_LINK,*LPADD_CLIENT_LINK;

	typedef struct tagREMOVE_CLIENT_LINK
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
		fk::LDnaClientLinkFullManager* pClientLinkFullManager;
	}REMOVE_CLIENT_LINK,*LPREMOVE_CLIENT_LINK;

	typedef struct tagUPDATE_DATA
	{
		fk::NOTIFYEVENT ne;
		fk::LDnaClientLinkManager* pClientLinkFull;
		fk::LDnaClientLink* pClientLink;
	}UPDATE_DATA,*LPRUPDATE_DATA;
};

/*
功能：1.管理运行时的配置文件，.axml .uxml两个文件。
功能: 2.在非运行时，DLT 环境使用此类，打开主程序的 .axml .uxml 文件。
使用：
fk::ModuleConfig*  _pModuleConfig;

if (fk::ModuleConfig::NewModuleConfig(g_pmodule, &_pModuleConfig, fk::IID_ModuleConfig))
{
	...
}
*/
/*
    操作一个配置文件。配置文件可能以下几个功能组成。

    1. 链接点功能段。              存在操作的类，内存类。
    2. 链接点下注册的链接项。      存在操作的类。DnaPositionManager 中的 DnaPosition 操作。
    3. 需要注册的链接项。          存在操作的类。外置类。 在向可执行程序的配置文件添加注册的配置文件时，存在
    4. 存在对象集合。
    5. 其它配置散配置项。
*/
	/*
	管理当前 DNA File 中的所有链接点，据有以下功能。
	1.添加一个链接点。
	2.移出一个链接点。
	3.重命名一个链接点。
	4.获得链接点总数。
	5.获得一个链接点项。
	6.获得第一个链接点地址。
	7.获得 fk::LEnumData 枚举链接点对象。
	8.读入当前所有 xml 文件中的链接点。
	9.保存当前所有链接点到文件中。
	10.获得 fk::ModuleConfig 句柄。
	*/
class _FK_OUT_CLASS LModuleConfig : public fk::LObject
{
private:
public:
	LModuleConfig(fk::LObject* pParentObject);
	virtual ~LModuleConfig(void);

	virtual b_call OpenConfig(fk::LModule* pmodule, fk::HXMLCONFIG hconfig) = 0;
	virtual b_call OpenConfig(fk::LModule* pmodule, LPCWSTR lpszFile) = 0;

	virtual b_call AlretConfig(void) = 0;
	virtual b_call CloseConfig(void) = 0;

	virtual fk::LGuid* _call GetGuid(void) = 0;		// 已经无法确定这个 guid 是什么功能。可能是项目的唯一 guid 。 

	virtual b_call IsDnaClient(void) = 0;		// 配置文件中存在 //config/dna/client 链接者项。
	virtual b_call IsDnaLinks(void) = 0;		// 配置文件中存在 //config/dna/links 链接点。

	virtual b_call IsOpen(void) = 0;
	virtual b_call GetXmlFile(fk::LXmlFile** ppxmlFile) = 0;
	virtual b_call GetFullFile(fk::LStringw* psFullFile) = 0;
	virtual b_call GetFonts(fk::fonts** pfonts) = 0;

	virtual b_call ReadDnaPosition(fk::LDnaLinks* pdnaLinks) = 0;
	virtual b_call ReadDnaClientGroup(void) = 0;

	// 获得模块配置文件的宿主 fk::LModule 变量。
	virtual b_call GetModuel(fk::LModule** ppmodule) = 0;

	// 添加一个链接点。
	virtual b_call AddItem(fk::LModule* pmodule, DWORD dwMake, LPCWSTR lpszName, LPGUID pguid, LPCWSTR lpszInfo) = 0;
	virtual b_call RemoveDnaPosition(fk::LDnaPosition* pdnaPosition) = 0;

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iitem, fk::LDnaPosition** pDnaPosition) = 0;
	virtual data_t* GetDatas(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	//virtual b_call SetModuleConfig(fk::ModuleConfig* pModuleConfig) = 0;
	//virtual fk::ModuleConfig* GetModuleConfig(void) = 0;

	virtual b_call ReadXmlFullPosition(fk::LDnaLinks* pdnaLinks) = 0;
	virtual b_call SaveXmlFullPosition(fk::LModuleConfig* pModuleConfig) = 0;

	virtual b_call FindDnaPosition(fk::LDnaPosition* pdnaPosition) = 0;
	virtual b_call FindDnaPosition(fk::LGuid* pguid, fk::LDnaPosition** ppdnaPosition) = 0;

	static  b_fncall NewModuleConfig(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumONConfigMgr
	{
		ON_CM_OPEN		= 0x0000001,	// 打开配置文件事件。
		ON_CM_CLOSE		= 0x0000002,	// 关闭文件事件。
		ON_ADD			= 0x00000001,       // 添加一个链接点。
		ON_REMOVE		= 0x00000002,       // 移出一个链接点。
		ON_UPDATE_ITEM	= 0x00000004,       // 更新某一个链接点数据。
		ON_UPDATE		= 0x00000010,       // 更新所有的数据。
	};

	typedef struct tagADD
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}ADD, *LPADD;
	typedef struct tagREMOVE
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}REMOVE, *LPREMOVE;
	typedef struct tagUPDATE_ITEM
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pModuleConfig;
		fk::LDnaPosition* pdnaPosition;
	}UPDATE_ITEM, *LPUPDATE_ITEM;
	typedef struct tagUPDATE
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pModuleConfig;
	}UPDATE, *LPUPDATE;

	typedef struct tagON_OPEN
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pConfigMgr;
	}ON_OPEN,*LPON_OPEN;

	typedef struct tagON_CLOSE
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfig* pConfigMgr;
	}ON_CLOSE,*LPON_CLOSE;
};


class _FK_OUT_CLASS LModuleConfigRegsvrItem : public fk::LObject
{
private:
protected:
	fk::LModuleConfigManager*	_pModuleConfigManager;
	fk::LModuleConfig*			_pModuleConfig;				// 配置文件对象指针。
	fk::HXMLNODE				_xmlNode;					// 配置文件在 xml 文件中的位置。

public:
	LModuleConfigRegsvrItem(fk::LModuleConfigManager* pModuleConfigManager);
	virtual ~LModuleConfigRegsvrItem(void);

	DWORD			_dwMakeItem;	// 添加的配置项掩码。
	fk::LStringw	_sFullName;		// 配置文件名称。
	fk::LGuid		_guid;			// 配置文件中的 guid 。

	v_call clear(void);
	b_call ReadModuleConfigRegsvr(fk::LModuleConfig* pModuleConfigMain);

	b_call SetXmlNode(fk::HXMLNODE xmlNodeItem);

	fk::LModuleConfig* _call GetModuleConfig(void);
	fk::HXMLNODE _call GetXmlNode(void);

	static b_fncall NewModuleConfigRegsvrItem(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppv, REFIID riid _FK_OBJECT_POS_H2_);
};


/********************************************************************************************

调用动态链接库中的 GetDllDnaXmlFile 函数，获得 dna xml 文件，并注册到程序的主配置文件中。
或者单独的注册一个 dna .xml 文件到程序的主配置中。

********************************************************************************************/
class _FK_OUT_CLASS LModuleConfigRegister : public fk::LObject
{
private:
public:
	LModuleConfigRegister(fk::LObject* pParentObject);
	virtual ~LModuleConfigRegister(void);

	virtual b_call DeleteFull(void) = 0;						// 删除 //config/DnaModule/items 全部子节点。
	virtual fk::enumReturn _call RegisterFull(void) = 0;		// 注册已经注册的DLL 文件中的所有 DNA xml 文件到主配置文件中。
	virtual b_call RegisterModule(fk::LModule* pmodule) = 0;	// 注册一个 Module dll 文件中的所有配置文件到主配置文件中。
	virtual b_call RegisterDnaXml(LPCWSTR lpszDnaXml) = 0;		// 注册一个 DNA xml 文件到配置文件中。

	static b_fncall NewModuleConfigRegister(fk::LModuleConfigManager* pModuleConfigManager, LPVOID* ppobj, REFIID riid = GUID_NULL  _FK_OBJECT_POS_H2_);
};


/*******************************************************************************************

管理所有注册的DNA配置文件列表，打开，生成，关闭，添加，移出等操作。

使用方式：
	fk::ModuleConfigManager* pmcm;

	pmcm->SetModuleConfigMain(pModuleConfig);	首先主 ModuleConfig 对象。然后在进行其它操作。
	pmcm->ReadFullModuleConfig();				//程序开始时，读入所有的配置文件，生成系列功能。然后进程程序启动。
	完成初始化工作。

********************************************************************************************/
class _FK_OUT_CLASS LModuleConfigManager : public fk::LObject
{
private:
public:
	enum enumModuleConfigItem
	{
		umModuleConfigItemEnable	= 0x00000001,	// 载入配置文件。
		//ummcriDisable	= 0x00000002,	// 配置文件禁止载入。
	};

public:
	LModuleConfigManager(fk::LObject* pParentObject);
	virtual ~LModuleConfigManager(void);

	/*
	向主程序的配置文件，注册一个配置文件，这配置文件中可能不包含 dna 接点功能。
	如果注册的配置文件中，存在需要注册的链接项。会把链接项注册到链接点中。

	@param pModuleConfig 主程序配置文件操作类,需要注册的配置文件添加到这个对象中。
	@param dwMake 需要注册的配置文件掩码。
	@param lpszDnaModuleFull 需要注册的配置文件绝对路径。
	*/
	virtual b_call AddModuleConfig(DWORD dwModuleConfigItemMake, LPCWSTR lpszDnaModueFull, fk::LModuleConfig** ppModuleConfig) = 0;
	virtual b_call save(void) = 0;

	virtual b_call GetMainModuleConfig(fk::LModuleConfig** pModuleConfig) = 0;
	virtual b_call GetModuleConfigRegister(fk::LModuleConfigRegister** ppModuleConfigRegister) = 0;
	virtual b_call GetDnaPositionFullManager(fk::LDnaPositionFullManager** ppPositionFullManager) = 0;
	virtual b_call GetDnaClientLinkFullManager(fk::LDnaClientLinkFullManager** ppClientLinkFullManager) = 0;
	virtual b_call GetDnaItemFull(fk::LDnaItemFull** ppdnaItemFull) = 0;

	virtual i_call GetCount(void) = 0;
	virtual b_call GetItem(int iitem, fk::LModuleConfig** ppModuleConfig) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;
    virtual data_t* _call GetDatas(void) = 0;

	// 在程序主配置 .pxml 文件中，查找 dna xml 是否存在。
	virtual b_call FindDnaXmlFormConfig(LPCWSTR lpszDnaXml) = 0;

	virtual b_call find(LPCWSTR lpszDnaModuleFull, fk::LModuleConfig** ppModuleConfig) = 0;

	virtual b_call find(IID* pguid, fk::LModuleConfig** ppModuleConfig) = 0;
	virtual b_call find(IID* pguid, fk::LModuleConfigRegsvrItem** ppModuleConfigRegsrItem) = 0;
	virtual b_call FindDnaPosition(fk::LGuid* pguid, fk::LDnaPosition** ppdnaPosition) = 0;

	virtual b_call IsExist(LPCWSTR lpszDnaModuleFull) = 0;
	virtual b_call GetDnaLinks(fk::LDnaLinks** ppdnaLinks) = 0;

	static  b_fncall NewModuleConfigManager(fk::LObject* pParentObject, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumNotify
	{
		ON_ADD			= 0x00000001,		// 添加一个 ModuleConfig.xml t文件。
		ON_REMOVE		= 0x00000002,		// 移出一个 ModuleConfig.xml 文件。
		ON_UPDATE_DATA	= 0x00000004,		// 更新数据。
		ON_CLOSE		= 0x00000008,		// 关闭 ModuleConfigManager 类.
	};	

	typedef struct tagOnAdd
	{
		fk::NOTIFYEVENT		ne;
		fk::LModuleConfig* 	pModuleConfig;
	}OnAdd,*LPOnAdd;
	typedef struct tagOnRemvoe
	{
		fk::NOTIFYEVENT 	ne;
		fk::LModuleConfig* 	pModuleConfig;
	}OnRemvoe,*LPOnRemvoe;
	typedef struct tagOnUpdateData
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfigManager* pmcm;
	}OnUpdateData,*LPOnUpdateData;
	typedef struct tagClose
	{
		fk::NOTIFYEVENT ne;
		fk::LModuleConfigManager* pmcm;
	}OnClose,*LPOnClose;
};

#ifdef __cplusplus
extern "C" {
#endif
	b_fncall GetModuleConfigManager(fk::LModuleConfigManager** ppModuleConfigManager);
    b_fncall GetModuleConfigFieldsXmlFile(fk::LXmlFile** ppxmlFile);
	b_fncall InitJointFK(void);
	v_fncall UninitJointFK(void);
#ifdef __cplusplus
}
#endif

_FK_END

#ifndef __Not_User_Joint_FK_lib__
#pragma comment(lib, "joint.fk.lib")
#endif

#endif		// __ModuleConfig_hxx__
