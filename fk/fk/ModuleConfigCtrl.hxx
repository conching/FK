#ifndef __ModuleConfigCtrl_hxx__
#define __ModuleConfigCtrl_hxx__


_FK_BEGIN
// {F478FE38-811D-4bc2-B085-31E9776DF26A}
static const GUID IID_ModuleConfigManagerListBox = { 0xf478fe38, 0x811d, 0x4bc2, { 0xb0, 0x85, 0x31, 0xe9, 0x77, 0x6d, 0xf2, 0x6a } };

// {B3537474-9032-47fc-961C-AAF744A6E108}
static const GUID IID_DnaPositionManagerListView32 = { 0xb3537474, 0x9032, 0x47fc, { 0x96, 0x1c, 0xaa, 0xf7, 0x44, 0xa6, 0xe1, 0x8 } };

//
//class LDataSource : public fk::LObject
//{
//private:
//    enum enumMakeDataSource
//    {
//        umMakeIsEdit    = 0x00000001,
//        umMakeIsEnabled = 0x00000002,       // 存在此值，说明是可以载入数据。
//    };
//
//    DWORD _dwMakeDataSource;
//    fk::LTable* _ptable;
//
//    v_call DoStateChange(void);
//    v_call DoUpdateData(void);
//
//public:
//	virtual b_call Assign(LObject* pobject);
//	virtual ULONG STDMETHODCALLTYPE Release();
//	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
//
//	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
//	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
//						fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
//	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);
//
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
//
//public:
//	LDataSource(fk::LObject* pParentObject);
//	virtual ~LDataSource(void);
//
//	virtual v_call SetEdit(bool bEdit);
//	virtual b_call GetEdit(void);
//
//	virtual b_call SetTable(fk::LTable* ptable);
//    virtual b_call GetTable(fk::LTable** pptable);
//
//    virtual b_call SetEnabled(bool bEnabled);
//    virtual b_call GetEnabled(void);
//
//    static b_fncall NewDataSource(fk::LObject* pParentObject, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);
//
//public:
//
//    enum PROPID
//    {
//        PROPID_SetTable = 2001,         // 设置数据集合。
//        PROPID_SetEdit = 2002,
//        PROPID_SetEnabled = 2003,
//    };
//
//    enum enumEvent
//    {
//        ON_StateChange  = 0x0000000000000001,
//        ON_UpdateData   = 0x0000000000000002,
//    };
//
//    typedef struct tagDataSouceEvent
//    {
//        fk::NOTIFYEVENT     en;
//        fk::LDataSource*    pDataSource;
//    }DataSouceEvent,*LPDataSouceEvent;
//};
//
//

b_call LoadColumnXmlFile(fk::LSysListView32* psysListView32, fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath);

// 显示全部配置文件。
class _FK_OUT_CLASS ModuleConfigManagerListBox : public fk::LListBox
{
private:

public:
	enum enumItemType;

	ModuleConfigManagerListBox(fk::LWindowMuch* pparent);
	virtual ~ModuleConfigManagerListBox(void);
    i_call FindDataPtr(DWORD_PTR pdata);
    b_call DeleteItemDataPtr(DWORD_PTR pdata);

	virtual b_call SetModuleConfigManager(fk::LModuleConfigManager* pModuleConfigManager) = 0;
    virtual b_call ShowDefaultModuleConfigManager(void) = 0;
    virtual b_call IsSetModuleConfigManager(void) = 0;
    virtual b_call IsDefaultModuleConfigManager(void) = 0;

    virtual b_call ShowFullButton(bool bShow) = 0;
    virtual b_call IsShowFullButton(void) = 0;

    virtual enumItemType _call ItemIsModuleConfig(int iitem) = 0;

    virtual b_call GetActiveModuleConfig(fk::LModuleConfig** ppModuleConfig) = 0;
    virtual b_call SetActiveModuleConfig(fk::LModuleConfig* pModuleConfig) = 0;
    virtual b_call SetActiveItem(int iitem) = 0;

	static ModuleConfigManagerListBox* _call NewModuleConfigManagerListBox(fk::LWindowMuch* pparent);

public:
	enum enumItemType
	{
		umIsError	= 0x0,
		umIsModuleConfig = 0x1,
		umIsLineButton = 0x2,
		umIsFullButton = 0x3,
	};

    enum enumPROP_ITEM
    {
        PROPID_SET_MODULECONFIGFULLMANAGER      = 2001,     // 设置 SetModuleConfigManager 方式。
        PROPID_SHOW_DEFAULT_MODULECONFIGMANAGER = 2002,     // 设置 ShowDefaultModuleConfigManager 方式。
    };

    enum enumNotify
    {
        ON_SLECT_ITEM   = 0x000000000000001,                // 在 ListBox 控件中，选择了某一项。
    };

    typedef struct tagSelectItem
    {
        fk::NOTIFYEVENT						ne;
 		fk::LModuleConfig*					pModuleConfig;
		fk::ModuleConfigManagerListBox*		pModuleConfigManagerList;
    }SelectItem,*LPSelectItem;
};

//
// 功能：显示 fk::DnaPositionManager, fk::DnaPositionFullManager, fk::ModuleConfigManagerListBox 这 3 个
// 不同类的节点列表，他们之间的互斥，只有一个类在工作状态。
//
// fk::LDnaPositionManagerlistView32 控件有两个显示功能，这两个显示功能是互斥，只能显示一个对象的内容，不能同时显示两个对象的内容。
// 1.显示 fk::ModuleConfig 文件中的链接点列表。
// 2.显示 fk::DnaPositionFullManager 对象中的链接点。
// 3. 显示 fk::ModuleConfigManagerListBox 控件，选择的 fk::ModuleConfig 的对象中的 fk::DnaPositionManager 节点列表。
//
// 使用方式1：
//      1. 创建窗口之后调用以下方式。
//      2. 调用 SetDnaPositionManager 方式。
//
// 使用方式2：
//      1. 创建窗口之后调用以下方式。
//      2. 调用：SetDnaPositionFullManager 方式。
//
// 使用方式3：
//      1. 创建窗口之后调用以下方式。
//      2. 调用：SetModuleConfigManagerListBox 方式。
//
class _FK_OUT_CLASS DnaPositionManagerListView32 : public fk::LSysListView32
{
private:
public:
	enum enumInputObjectType;

	DnaPositionManagerListView32(fk::LWindow* pParentWindow);
	virtual ~DnaPositionManagerListView32(void);

	//virtual b_call SetModuleConfig(fk::ModuleConfig* pModuleConfig) = 0;						// 显示文件中的节点。
	virtual b_call SetDnaPositionFullManager(fk::LDnaPositionFullManager* pdnaPositionFullManager) = 0;			// 显示一个配置文件集合中的列表。
	virtual b_call SetModuleConfigManagerListBox(fk::ModuleConfigManagerListBox* pModuleConfigManagerList) = 0;	// 和显示配置集合列表对象，关联。

	virtual b_call GetSelectDnaPosition(fk::LDnaPosition** ppdnaPosition) = 0;
	virtual b_call SetSelectOnaPosition(fk::LDnaPosition* pdnaPosition) = 0;
	virtual b_call SetSelectItem(int iitem) = 0;

//	virtual b_call SetModuleConfigManager(fk::ModuleConfigManager* pModuleConfigManager) = 0;					//

	virtual fk::DnaPositionManagerListView32::enumInputObjectType _call GetInputObjectType(void) = 0;

	static DnaPositionManagerListView32* _call NewDnaPositionManagerListView32(fk::LWindow* pParentWindow);

public:
    enum enumInputObjectType
    {
		umIsError = 0x0,
        umIsDnaPositionManager = 0x1,
        umIsDnaPositionFullManager = 0x2,
        umIsModuleConfigManagerListBox = 0x3,
    };

    enum enumPROP_ID
    {
        PROPID_SetModuleConfigManagerListBox= 2001,     // 字符串。设置 fk::LModuleConfigManagerList 变量名。
        PROPID_SetDnaPositionManager        = 2002,     // 字符串。设置 fk::DnaPositionManager 变量名。
        PROPID_SetDnaPositionFullManager    = 2003,     // 字符串。设置 fk::DnaPositionFullManager 变量名。
        PROPID_SetDBListView                = 2004,     // 字符串。设置 fk::LDBListView 变量名。
		PROPID_SetModuleConfigManager		= 2005,
    };

    enum enumNotify
    {
        ON_SELECT_ITEM = 0x00000001,                 // 选择了某一项。
    };

    typedef struct tagSelectItem
    {
        fk::NOTIFYEVENT		ne;
		fk::LDnaPosition*	pdnaPosition;
        fk::DnaPositionManagerListView32* pDnaPositionManagerListView32;
    }SelectItem,*LPSelectItem;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  DnaPositionItemListView 有两个显示功能。这两个显示功能是互斥，只能显示一个对象，不能同时显示两个对象。
//  1.显示 fk::DnaPosition 链接点对象下的所有链接者。
//  2.显示 fk::LDnaItemFull 全部链接者。
//
class _FK_OUT_CLASS DnaPositionItemListView : public fk::LSysListView32
{
private:
public:
	enum enumInputObjectType;

	DnaPositionItemListView(fk::LWindow* pParentWindow);
	virtual ~DnaPositionItemListView(void);

	// 这三个互斥。只有对象有做用。
	//
	virtual b_call SetDnaPosition(fk::LDnaPosition* pdnaPosition) = 0;
	virtual b_call SetDnaPositionFullManager(fk::LDnaItemFull* pdnaItemFull) = 0;
    virtual b_call SetDnaPositionManagerListView32(fk::DnaPositionManagerListView32* pDnaPositionManagerListView32) = 0;

	virtual fk::DnaPositionItemListView::enumInputObjectType _call GetInputObjectType(void) = 0;

	static DnaPositionItemListView* _call NewDnaPositionItemListView(fk::LWindow* pParentObject);

public:
	enum enumPropID
	{
		PropID_SetModuleConfigManagerListBox = 2001,	//
	};

	enum enumInputObjectType
	{
	    umIsError = 0x0,
		umIsDnaPosition = 0x1,
		umIsDnaItemFull = 0x2,
		umIsModuleConfigManagerListView32 = 0x3,
	};

	typedef struct tagSelectItem
	{
		fk::NOTIFYEVENT ne;
		fk::DnaPositionItemListView* pdnaPositionItemListView32;
	}SelectItem,*LPSelectItem;
};


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// 显示 fk::ModuleConfig 文件中的链接者组。
//
class _FK_OUT_CLASS DnaClientLinkTreeView32 : public fk::LSysTreeView32
{
private:
public:
    enum enumInputObjectType;

	DnaClientLinkTreeView32(fk::LWindow* pParentObject);
	virtual ~DnaClientLinkTreeView32(void);

	// 这三个函数互斥。
	//
	virtual b_call SetDnaClientLinkManager(fk::LDnaClientLinkManager* pdnaClientLinkManager) = 0;
	virtual b_call SetDnaClientLinkFullManager(fk::LDnaClientLinkFullManager* pdnaClientLinkFullManager) = 0;
	virtual b_call SetModuleConfigManagerListBox(fk::ModuleConfigManagerListBox* pModuleConfigManagerList) = 0;

	virtual enumInputObjectType _call GetInputObjectType(void) = 0;

	static DnaClientLinkTreeView32* _call NewDnaClientLinkTreeView32(fk::LWindow* pParentObject);
public:
	enum enumPropID
	{
		PropID_SetModuleConfigManagerListBox = 2001,	//
	};

	enum enumInputObjectType
	{
	    umIsError   = 0x0,
		umIsDnaClientLink = 0x1,
		umIsDnaClientLinkFullManager = 0x2,
		umIsModuleConfigManagerListBox = 0x3,
	};

	typedef struct tagSelectItem
	{
		fk::NOTIFYEVENT ne;
		fk::DnaClientLinkTreeView32* pdnaClientLinkTreeView32;
	}SelectItem,*LPSelectItem;
};


_FK_END

#ifndef __Not_User_ModuleConfigCtrl_lib__
#pragma comment(lib, "fkdna.lib")
#endif

#endif			//__ModuleConfigCtrl_hxx__
