#ifndef __PlushType_h__
#define __PlushType_h__


//
// 此单元可以不用。
//

_FK_BEGIN

class _FK_OUT_CLASS LInt : public LVar
{
public:
	//INT _int;

	LInt();
	virtual ~LInt();
	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();

	int operator = (LPCWSTR lpszIntValue)
	{
		Assignw(lpszIntValue);
		return _int;
	}

	int operator = (LPCSTR lpszIntValue)
	{
		Assigna(lpszIntValue);
		return _int;
	}
};



class _FK_OUT_CLASS LUInt : public LVar
{
public:
	//UINT _uint;

	LUInt();
	virtual ~LUInt();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();

	int operator = (LPCWSTR lpszIntValue)
	{
		Assignw(lpszIntValue);
		return _int;
	}

	int operator = (LPCSTR lpszIntValue)
	{
		Assigna(lpszIntValue);
		return _int;
	}
};


class _FK_OUT_CLASS  LULong : public fk::LVar
{
private:
public:
	//ULONG _ulong;

	LULong();
	virtual ~LULong();

	virtual b_call AssignValue(fk::LVar* pbaseData);

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call  SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);

	virtual b_call GetBool();
	virtual b_call SetBoolStr(bool bValue, fk::umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call  SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(fk::LStringw* ptext);
	virtual b_call AssignStringw(fk::LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1); /* = 0; */
	virtual b_call Assigna(const char* pstr, int uiLen=-1); /* = 0; */

	virtual b_call SetDataType(UINT uiType);
	virtual v_call clear();

	virtual b_call IsEmpty();
};

b_fncall ULongCreate(fk::LULong** ppULong);
b_fncall ULongDestroy(fk::LULong** pULong);

#ifdef _DEBUG
#define ULongNew(ppULong)	fk::ULongCreate(ppULong)
#else
#define ULongNew(pULong)		fk::ULongCreate(pULong)
#endif

#define ULongDelete(pULong)		fk::ULongDestroy(pULong)



class _FK_OUT_CLASS  LLong : public fk::LVar
{
private:
public:
	LLong();
	virtual ~LLong();

	virtual b_call AssignValue(fk::LVar* pbaseData);

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call  SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);

	virtual b_call GetBool();
	virtual b_call SetBoolStr(bool bValue, fk::umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call  SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(fk::LStringw* ptext);
	virtual b_call AssignStringw(fk::LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1); /* = 0; */
	virtual b_call Assigna(const char* pstr, int uiLen=-1); /* = 0; */

	virtual b_call SetDataType(UINT uiType);
	virtual v_call clear();

	virtual b_call IsEmpty();
};


class _FK_OUT_CLASS LBool : public LVar
{
public:
	LBool();
	virtual ~LBool();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);

	virtual v_call clear();

	virtual b_call IsEmpty();

};


class _FK_OUT_CLASS LVariantBool : public LVar
{
public:
	LVariantBool();
	virtual ~LVariantBool();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();
};


class _FK_OUT_CLASS LFloat : public LVar
{
public:
	LFloat();
	virtual ~LFloat();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();
};


class _FK_OUT_CLASS LDouble : public LVar
{
public:
	LDouble();
	virtual ~LDouble();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();
};

//
//class _FK_OUT_CLASS LIID : public LVar
//{
//public:
//	LIID();
//	virtual ~LIID();
//
//	virtual b_call AssignBaseData(fk::LVar* pbaseData);   /* = 0; */
//
//	virtual l_call GetLong();
//	virtual b_call SetLong(long lValue, int radix);
//
//	virtual ul_call GetULong();
//	virtual b_call SetULong(ULONG ulValue, int radix);
//
//	virtual i_call GetInt();
//	virtual b_call SetInt(int iValue, int radix);
//
//	virtual ui_call GetUInt();
//	virtual b_call SetUInt(UINT uiValue, int radix);
//
//	virtual be_call GetByte();
//	virtual b_call SetByte(BYTE byte, int radix);
//
//	virtual f_call GetFloat();
//	virtual b_call SetFloat(float fValue);
//
//	virtual d_call GetDouble();
//	virtual b_call SetDouble(double dValue);
//
//	virtual b_call SetBool(bool bValue);
//
//	virtual b3_call GetVariantBool();
//	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);
//
//	virtual us_call GetUShort();
//	virtual b_call SetUShort(USHORT usValue, int radix);
//
//	virtual b_call GetStringw(LStringw* ptext);
//	virtual b_call AssignStringw(LStringw* ptext);
//
//	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);
//
//	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
//	virtual b_call Assigna(const char* pstr, int uiLen=-1);
//	virtual v_call clear();
//
//	virtual b_call IsEmpty();
//
//	b_call SetIID(const IID* piid);
//	b_call GetIID(IID* piid);
//};
//

class _FK_OUT_CLASS LSystemTime : public LVar
{
public:
	SYSTEMTIME _SystemTime;

	LSystemTime();
	virtual ~LSystemTime();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();

	virtual b_call SetSystemTime(LPSYSTEMTIME pSystemTime);
	b_call GetSystemTime(LPSYSTEMTIME lpSystemTime);
};


class _FK_OUT_CLASS LRect : public LVar
{
public:
	RECT _rect;
	LRect();
	virtual ~LRect();

	virtual b_call AssignValue(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, fk::umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsEmpty();

	virtual b_call SetRect(LPCRECT lpcRect);
	b_call GetRect(LPRECT lpRect);
};


//
//	根据参数 udt 创建出某个数据类对象,但是不设置数据。
//
// 输入:
//		enumDataType 
//		data_t
// 输出：
//		ppBaseData 
//		ppDataPtr		这个是指向 LVar 成员数据的指针。
b_fncall BaseDataCreateOutDataPtr(__in fk::enumDataType udt, __out fk::LVar** ppBaseData, __out data_t** ppDataPtr);
b_fncall BaseDataCreateDataTypeObject(__in fk::enumDataType umdt, __out fk::LVar** ppBaseData);

//
//	根据参数 udt 和 pdata 创建出某个数据类对象。
//
// 输入:
//		enumDataType 
//		data_t
// 输出：
//		ppBaseData 
//		ppDataPtr		这个是指向 LVar 成员数据的指针。

b_fncall BaseDataCreateFormData(__in fk::enumDataType udt,
								__in data_t* pdata,
								__out fk::LVar** ppBaseData,
								__out data_t** ppDataPtr
								);

_FK_END

#endif // __PlushType_h__
