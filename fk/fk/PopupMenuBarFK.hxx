#ifndef __PopupMenuBarFK_hxx__
#define __PopupMenuBarFK_hxx__

_FK_BEGIN

static char* S_CLASS_POPUPMENUBAR = "fk::LPopupMenuBar";

class _FK_OUT_CLASS LPopupMenuBar : public fk::LComponent
{
	_FK_RTTI_;

private:
	IPopupMenuBar*  _pPopupMenuBar;
	fk::LStringa	_sXmlPath;
	fk::HXMLCONFIG	_hxmlConfig;

protected:
public:
	LPopupMenuBar(fk::LObject* pParentObject);
	virtual ~LPopupMenuBar(void);

	b_call get_Item(long nIndex, ICommandItem **pItem);
	b_call DeleteItem(COMMANDSTRUCT *lpCommandStruct, int iCommandCount, BOOL bDelSeparator);
	b_call DeleteItumCommandRange(UINT iBeginCmd, UINT iEndCmd);
	b_call ModifyMenuItem(COMMANDSTRUCT *pCmdOld, COMMANDSTRUCT *pCmdNew);
	b_call FindCommandStruct(COMMANDSTRUCT *pCmdFind, COMMANDSTRUCT **pCmdOut);
	b_call IsMenuBar( void);
	b_call CreatePopupMenuBar(void);
	b_call TrackPopupMenu(HWND hwndCmd, LONG x, LONG y);

	b_call get_MenuHandle(HMENU *pVal);
	b_call put_MenuHandle(HMENU newVal);
	b_call DeletePopupMenu( void);

	b_call InsertCommand(ULONG nIndex, UINT fByPosition, COMMANDSTRUCT **lpCommandStruct, int iCommandCount);
	b_call AppendMenu(fk::LModule* pmodule, UINT uFlags, UINT uiNewItem, LPCWSTR lpszNewItem, ULONG* pdata);

	b_call InstallNotifyCommand(int emInstallEvent_, INotifyCommand *pNotifyCommand);
	b_call get_NotifyCommand(INotifyCommand **ppNotifyCommand);

	b_call GetItem(VARIANT *varItem, IUnknown **pCommandItem);
	b_call get_CmdItem(long nCmd, IUnknown **pCommandItem);
	b_call GetCount(long *lCount);
	b_call InsertSeparator(int iIndex);

	b_call ModifyItemPopup(int iPosition, UINT fByPosition, IPopupMenuBar **ppPopupMenuBar);
	b_call AddNotifyCommandDelete(IID *piidKey, INotifyCommand *pNotifyCommand);
	b_call GetPopupMenuBarItem(int iPosition, UINT fByPosition, IPopupMenuBar **ppPopupMenuBar);

	b_call SetXmlPath(fk::LStringw* psXmlPath);
	b_call SetXmlPath(fk::LStringa* psXmlPath);

	b_call SetXmlConfig(fk::HXMLCONFIG hxmlConfig);

	/*
		为什么这两个函数不能使用 INotifyCommand* pNotifyCommand 做为参数？其它库连接 lib 文件时，提示：
		1>NotifyView.obj : error LNK2019: unresolved external symbol "public: bool __thiscall fk::LPopupMenuBar::InsertEnumPlus(class fk::LModule *,class fk::LEnumPlus *,int,struct fk::INotifyCommand *)" (?InsertEnumPlus@LPopupMenuBar@fk@@QAE_NPAVLModule@2@PAVLEnumPlus@2@HPAUINotifyCommand@2@@Z) referenced in function "private: void __thiscall dlt::LNotifyViewImpl::LoadClassNamePopupMenu(class fk::LPropItem *,class fk::LPopupMenuBar *)" (?LoadClassNamePopupMenu@LNotifyViewImpl@dlt@@AAEXPAVLPropItem@fk@@PAVLPopupMenuBar@4@@Z)

		现在使用 void* pNotifyCommand 做为参数。
	*/
	b_call ReadPopupMenuBar(fk::LModule* pmodule, fk::HXMLCONFIG hxmlConfig, LPCSTR lpszXmlPath, void* pNotifyCommand); //, INotifyCommand* pNotifyCommand);
	b_call InsertEnumPlus(fk::LModule* pmodule, fk::LEnumPlus* pEnumPlus, int iBeginID, void* pNotifyCommand);

	static LPopupMenuBar* _fncall NewPopupMenuBar(fk::LObject* pParentObject);
};

_FK_END

#endif
