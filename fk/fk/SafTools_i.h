

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:03 2022
 */
/* Compiler settings for .\safTools.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __safTools_i_h__
#define __safTools_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDataView_FWD_DEFINED__
#define __IDataView_FWD_DEFINED__
typedef interface IDataView IDataView;
#endif 	/* __IDataView_FWD_DEFINED__ */


#ifndef __IEnumDataView_FWD_DEFINED__
#define __IEnumDataView_FWD_DEFINED__
typedef interface IEnumDataView IEnumDataView;
#endif 	/* __IEnumDataView_FWD_DEFINED__ */


#ifndef __IDataViewsCallBack_FWD_DEFINED__
#define __IDataViewsCallBack_FWD_DEFINED__
typedef interface IDataViewsCallBack IDataViewsCallBack;
#endif 	/* __IDataViewsCallBack_FWD_DEFINED__ */


#ifndef __IDataViews_FWD_DEFINED__
#define __IDataViews_FWD_DEFINED__
typedef interface IDataViews IDataViews;
#endif 	/* __IDataViews_FWD_DEFINED__ */


#ifndef __IThreadNotify_FWD_DEFINED__
#define __IThreadNotify_FWD_DEFINED__
typedef interface IThreadNotify IThreadNotify;
#endif 	/* __IThreadNotify_FWD_DEFINED__ */


#ifndef __INotifyTextInfo_FWD_DEFINED__
#define __INotifyTextInfo_FWD_DEFINED__
typedef interface INotifyTextInfo INotifyTextInfo;
#endif 	/* __INotifyTextInfo_FWD_DEFINED__ */


#ifndef __IProgressNotifyText_FWD_DEFINED__
#define __IProgressNotifyText_FWD_DEFINED__
typedef interface IProgressNotifyText IProgressNotifyText;
#endif 	/* __IProgressNotifyText_FWD_DEFINED__ */


#ifndef __IProgressWait_FWD_DEFINED__
#define __IProgressWait_FWD_DEFINED__
typedef interface IProgressWait IProgressWait;
#endif 	/* __IProgressWait_FWD_DEFINED__ */


#ifndef __IThreadNotifys_FWD_DEFINED__
#define __IThreadNotifys_FWD_DEFINED__
typedef interface IThreadNotifys IThreadNotifys;
#endif 	/* __IThreadNotifys_FWD_DEFINED__ */


#ifndef __IHeaderItem_FWD_DEFINED__
#define __IHeaderItem_FWD_DEFINED__
typedef interface IHeaderItem IHeaderItem;
#endif 	/* __IHeaderItem_FWD_DEFINED__ */


#ifndef __IHeaderCtrlEx_FWD_DEFINED__
#define __IHeaderCtrlEx_FWD_DEFINED__
typedef interface IHeaderCtrlEx IHeaderCtrlEx;
#endif 	/* __IHeaderCtrlEx_FWD_DEFINED__ */


#ifndef __IListCtrlColumn_FWD_DEFINED__
#define __IListCtrlColumn_FWD_DEFINED__
typedef interface IListCtrlColumn IListCtrlColumn;
#endif 	/* __IListCtrlColumn_FWD_DEFINED__ */


#ifndef __ITreePropertySheet_FWD_DEFINED__
#define __ITreePropertySheet_FWD_DEFINED__
typedef interface ITreePropertySheet ITreePropertySheet;
#endif 	/* __ITreePropertySheet_FWD_DEFINED__ */


#ifndef __IRegsvrDll_FWD_DEFINED__
#define __IRegsvrDll_FWD_DEFINED__
typedef interface IRegsvrDll IRegsvrDll;
#endif 	/* __IRegsvrDll_FWD_DEFINED__ */


#ifndef __IThreadNotifyInfoDlg_FWD_DEFINED__
#define __IThreadNotifyInfoDlg_FWD_DEFINED__
typedef interface IThreadNotifyInfoDlg IThreadNotifyInfoDlg;
#endif 	/* __IThreadNotifyInfoDlg_FWD_DEFINED__ */


#ifndef __IManagerCallback_FWD_DEFINED__
#define __IManagerCallback_FWD_DEFINED__
typedef interface IManagerCallback IManagerCallback;
#endif 	/* __IManagerCallback_FWD_DEFINED__ */


#ifndef __ISheetPanel_FWD_DEFINED__
#define __ISheetPanel_FWD_DEFINED__
typedef interface ISheetPanel ISheetPanel;
#endif 	/* __ISheetPanel_FWD_DEFINED__ */


#ifndef __IThumbnailInfo_FWD_DEFINED__
#define __IThumbnailInfo_FWD_DEFINED__
typedef interface IThumbnailInfo IThumbnailInfo;
#endif 	/* __IThumbnailInfo_FWD_DEFINED__ */


#ifndef __ICommandItem_FWD_DEFINED__
#define __ICommandItem_FWD_DEFINED__
typedef interface ICommandItem ICommandItem;
#endif 	/* __ICommandItem_FWD_DEFINED__ */


#ifndef __INotifyCommand_FWD_DEFINED__
#define __INotifyCommand_FWD_DEFINED__
typedef interface INotifyCommand INotifyCommand;
#endif 	/* __INotifyCommand_FWD_DEFINED__ */


#ifndef __INotifyCommands_FWD_DEFINED__
#define __INotifyCommands_FWD_DEFINED__
typedef interface INotifyCommands INotifyCommands;
#endif 	/* __INotifyCommands_FWD_DEFINED__ */


#ifndef __IPopupMenuBar_FWD_DEFINED__
#define __IPopupMenuBar_FWD_DEFINED__
typedef interface IPopupMenuBar IPopupMenuBar;
#endif 	/* __IPopupMenuBar_FWD_DEFINED__ */


#ifndef __ICommandBar_FWD_DEFINED__
#define __ICommandBar_FWD_DEFINED__
typedef interface ICommandBar ICommandBar;
#endif 	/* __ICommandBar_FWD_DEFINED__ */


#ifndef __ICommandBars_FWD_DEFINED__
#define __ICommandBars_FWD_DEFINED__
typedef interface ICommandBars ICommandBars;
#endif 	/* __ICommandBars_FWD_DEFINED__ */


#ifndef __ICommandGroup_FWD_DEFINED__
#define __ICommandGroup_FWD_DEFINED__
typedef interface ICommandGroup ICommandGroup;
#endif 	/* __ICommandGroup_FWD_DEFINED__ */


#ifndef __ICommandBarsList_FWD_DEFINED__
#define __ICommandBarsList_FWD_DEFINED__
typedef interface ICommandBarsList ICommandBarsList;
#endif 	/* __ICommandBarsList_FWD_DEFINED__ */


#ifndef __IMenuBar_FWD_DEFINED__
#define __IMenuBar_FWD_DEFINED__
typedef interface IMenuBar IMenuBar;
#endif 	/* __IMenuBar_FWD_DEFINED__ */


#ifndef __IPropertyPageDlg_FWD_DEFINED__
#define __IPropertyPageDlg_FWD_DEFINED__
typedef interface IPropertyPageDlg IPropertyPageDlg;
#endif 	/* __IPropertyPageDlg_FWD_DEFINED__ */


#ifndef ___INotifyCommandsEvents_FWD_DEFINED__
#define ___INotifyCommandsEvents_FWD_DEFINED__
typedef interface _INotifyCommandsEvents _INotifyCommandsEvents;
#endif 	/* ___INotifyCommandsEvents_FWD_DEFINED__ */


#ifndef __CommandBars_FWD_DEFINED__
#define __CommandBars_FWD_DEFINED__

#ifdef __cplusplus
typedef class CommandBars CommandBars;
#else
typedef struct CommandBars CommandBars;
#endif /* __cplusplus */

#endif 	/* __CommandBars_FWD_DEFINED__ */


#ifndef ___ICommandBarsListEvents_FWD_DEFINED__
#define ___ICommandBarsListEvents_FWD_DEFINED__
typedef interface _ICommandBarsListEvents _ICommandBarsListEvents;
#endif 	/* ___ICommandBarsListEvents_FWD_DEFINED__ */


#ifndef __CommandBarsList_FWD_DEFINED__
#define __CommandBarsList_FWD_DEFINED__

#ifdef __cplusplus
typedef class CommandBarsList CommandBarsList;
#else
typedef struct CommandBarsList CommandBarsList;
#endif /* __cplusplus */

#endif 	/* __CommandBarsList_FWD_DEFINED__ */


#ifndef __NotifyCommands_FWD_DEFINED__
#define __NotifyCommands_FWD_DEFINED__

#ifdef __cplusplus
typedef class NotifyCommands NotifyCommands;
#else
typedef struct NotifyCommands NotifyCommands;
#endif /* __cplusplus */

#endif 	/* __NotifyCommands_FWD_DEFINED__ */


#ifndef __CommandGroup_FWD_DEFINED__
#define __CommandGroup_FWD_DEFINED__

#ifdef __cplusplus
typedef class CommandGroup CommandGroup;
#else
typedef struct CommandGroup CommandGroup;
#endif /* __cplusplus */

#endif 	/* __CommandGroup_FWD_DEFINED__ */


#ifndef ___IPopupMenuBarEvents_FWD_DEFINED__
#define ___IPopupMenuBarEvents_FWD_DEFINED__
typedef interface _IPopupMenuBarEvents _IPopupMenuBarEvents;
#endif 	/* ___IPopupMenuBarEvents_FWD_DEFINED__ */


#ifndef __PopupMenuBar_FWD_DEFINED__
#define __PopupMenuBar_FWD_DEFINED__

#ifdef __cplusplus
typedef class PopupMenuBar PopupMenuBar;
#else
typedef struct PopupMenuBar PopupMenuBar;
#endif /* __cplusplus */

#endif 	/* __PopupMenuBar_FWD_DEFINED__ */


#ifndef ___IHeaderCtrlExEvents_FWD_DEFINED__
#define ___IHeaderCtrlExEvents_FWD_DEFINED__
typedef interface _IHeaderCtrlExEvents _IHeaderCtrlExEvents;
#endif 	/* ___IHeaderCtrlExEvents_FWD_DEFINED__ */


#ifndef __HeaderCtrlEx_FWD_DEFINED__
#define __HeaderCtrlEx_FWD_DEFINED__

#ifdef __cplusplus
typedef class HeaderCtrlEx HeaderCtrlEx;
#else
typedef struct HeaderCtrlEx HeaderCtrlEx;
#endif /* __cplusplus */

#endif 	/* __HeaderCtrlEx_FWD_DEFINED__ */


#ifndef ___IListCtrlColumnEvents_FWD_DEFINED__
#define ___IListCtrlColumnEvents_FWD_DEFINED__
typedef interface _IListCtrlColumnEvents _IListCtrlColumnEvents;
#endif 	/* ___IListCtrlColumnEvents_FWD_DEFINED__ */


#ifndef __ListCtrlColumn_FWD_DEFINED__
#define __ListCtrlColumn_FWD_DEFINED__

#ifdef __cplusplus
typedef class ListCtrlColumn ListCtrlColumn;
#else
typedef struct ListCtrlColumn ListCtrlColumn;
#endif /* __cplusplus */

#endif 	/* __ListCtrlColumn_FWD_DEFINED__ */


#ifndef ___IDataViewsEvents_FWD_DEFINED__
#define ___IDataViewsEvents_FWD_DEFINED__
typedef interface _IDataViewsEvents _IDataViewsEvents;
#endif 	/* ___IDataViewsEvents_FWD_DEFINED__ */


#ifndef __DataViews_FWD_DEFINED__
#define __DataViews_FWD_DEFINED__

#ifdef __cplusplus
typedef class DataViews DataViews;
#else
typedef struct DataViews DataViews;
#endif /* __cplusplus */

#endif 	/* __DataViews_FWD_DEFINED__ */


#ifndef ___ITreePropertySheetEvents_FWD_DEFINED__
#define ___ITreePropertySheetEvents_FWD_DEFINED__
typedef interface _ITreePropertySheetEvents _ITreePropertySheetEvents;
#endif 	/* ___ITreePropertySheetEvents_FWD_DEFINED__ */


#ifndef __TreePropertySheet_FWD_DEFINED__
#define __TreePropertySheet_FWD_DEFINED__

#ifdef __cplusplus
typedef class TreePropertySheet TreePropertySheet;
#else
typedef struct TreePropertySheet TreePropertySheet;
#endif /* __cplusplus */

#endif 	/* __TreePropertySheet_FWD_DEFINED__ */


#ifndef ___IRegsvrDllEvents_FWD_DEFINED__
#define ___IRegsvrDllEvents_FWD_DEFINED__
typedef interface _IRegsvrDllEvents _IRegsvrDllEvents;
#endif 	/* ___IRegsvrDllEvents_FWD_DEFINED__ */


#ifndef __RegsvrDll_FWD_DEFINED__
#define __RegsvrDll_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegsvrDll RegsvrDll;
#else
typedef struct RegsvrDll RegsvrDll;
#endif /* __cplusplus */

#endif 	/* __RegsvrDll_FWD_DEFINED__ */


#ifndef ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__
#define ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__
typedef interface _IThreadNotifyInfoDlgEvents _IThreadNotifyInfoDlgEvents;
#endif 	/* ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__ */


#ifndef __ThreadNotifyInfoDlg_FWD_DEFINED__
#define __ThreadNotifyInfoDlg_FWD_DEFINED__

#ifdef __cplusplus
typedef class ThreadNotifyInfoDlg ThreadNotifyInfoDlg;
#else
typedef struct ThreadNotifyInfoDlg ThreadNotifyInfoDlg;
#endif /* __cplusplus */

#endif 	/* __ThreadNotifyInfoDlg_FWD_DEFINED__ */


#ifndef ___IManagerCallbackEvents_FWD_DEFINED__
#define ___IManagerCallbackEvents_FWD_DEFINED__
typedef interface _IManagerCallbackEvents _IManagerCallbackEvents;
#endif 	/* ___IManagerCallbackEvents_FWD_DEFINED__ */


#ifndef __ManagerCallback_FWD_DEFINED__
#define __ManagerCallback_FWD_DEFINED__

#ifdef __cplusplus
typedef class ManagerCallback ManagerCallback;
#else
typedef struct ManagerCallback ManagerCallback;
#endif /* __cplusplus */

#endif 	/* __ManagerCallback_FWD_DEFINED__ */


#ifndef __SheetPanel_FWD_DEFINED__
#define __SheetPanel_FWD_DEFINED__

#ifdef __cplusplus
typedef class SheetPanel SheetPanel;
#else
typedef struct SheetPanel SheetPanel;
#endif /* __cplusplus */

#endif 	/* __SheetPanel_FWD_DEFINED__ */


#ifndef __ThumbnailInfo_FWD_DEFINED__
#define __ThumbnailInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class ThumbnailInfo ThumbnailInfo;
#else
typedef struct ThumbnailInfo ThumbnailInfo;
#endif /* __cplusplus */

#endif 	/* __ThumbnailInfo_FWD_DEFINED__ */


#ifndef __PropertyPageDlg_FWD_DEFINED__
#define __PropertyPageDlg_FWD_DEFINED__

#ifdef __cplusplus
typedef class PropertyPageDlg PropertyPageDlg;
#else
typedef struct PropertyPageDlg PropertyPageDlg;
#endif /* __cplusplus */

#endif 	/* __PropertyPageDlg_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_safTools_0000 */
/* [local] */ 










typedef /* [helpstring][uuid] */  DECLSPEC_UUID("9C4292D9-0867-42c1-BF1C-C0C192E498E9") struct COMMANDSTRUCT
    {
    DWORD dwSize;
    UINT fMask;
    DWORD dwState;
    int nCommandId;
    wchar_t *szText;
    UINT cch;
    HBITMAP hBitmap;
    HICON hIcon;
    INotifyCommand *pNotifyCommand;
    LPARAM dwItemData;
    GUID *pGuid;
    } 	COMMANDSTRUCT;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("CFC4AB51-7B1A-429c-B29B-11FF9A78AC4E") 
enum enumCommandItemState
    {	CMDS_CHECKED	= 0x2L,
	CMDS_HILITE	= 0x4L,
	CMDS_ENABLED	= 0x8L,
	CMDS_DEFAULT	= 0x20L,
	CMDS_RADIO	= 0x40L,
	CMDS_VISIBLE	= 0x80L
    } 	enumCommandItemState;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("318868C7-B4BD-44eb-8063-2D065625CD93") struct COMMANDNOTIFYINFO
    {
    ULONG dwSize;
    ULONG dwMask;
    ULONG hInst;
    ULONG hInstRes;
    } 	COMMANDNOTIFYINFO;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("00D57C8E-AFBC-4077-AE99-49D7D782F6C4") 
enum enumCommand
    {	umCommandIndex	= 0,
	umCommandID	= 1
    } 	enumCommand;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("CB5EB5E4-CBB5-11DB-9821-005056C00008") 
enum enumCmdItemType
    {	umCmdItemTypeMenuItem	= 3,
	umCmdItemTypeBarButton	= 4,
	umCmdItemTypeComboBox	= 5,
	umCmdItemTypeEdit	= 6,
	umCmdItemTypeCmd	= 7,
	umCmdItemTypeDropDown	= 9,
	umCmdItemTypeDropDownArrow	= 10,
	safCommandItemUser	= 11,
	umCmdItemTypeSatic	= 12
    } 	enumCmdItemType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("A584B344-E50F-424F-B459-5DF689FC1EB7") 
enum enumCommandBarsType
    {	umCommandBarsTypeLeft	= 0x1,
	umCommandBarsTypeTop	= 0x2,
	umCommandBarsTypeRight	= 0x3,
	umCommandBarsTypeBottom	= 0x4
    } 	enumCommandBarsType;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("261AE582-B94D-4a43-9DE2-3D8F24A5A668") 
enum enumInsertMenu
    {	umInsertMenuPopup	= 0x1,
	umInsertMenuItem	= 0x2
    } 	enumInsertMenu;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("6A597B0E-8780-4aff-ACFC-FE4AA02C0F5C") 
enum LVCF_Column
    {	LVCF_MYEX_MORE	= 0x10000,
	LVCF_MYEX_COLUMN	= 0x40000,
	LVCF_MYEX_SHOW_MENU	= 0x80000,
	LVCF_MYEX_FIXED	= 0x100000
    } 	LVCF_Column;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("16234748-BCB2-442d-919D-F0D1077F65CF") struct LVCOLUMN_HEADER_ITEM
    {
    int nColumnKey;
    IHeaderItem *pHeaderItem;
    } 	LVCOLUMN_HEADER_ITEM;



extern RPC_IF_HANDLE __MIDL_itf_safTools_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_safTools_0000_v0_0_s_ifspec;

#ifndef __IDataView_INTERFACE_DEFINED__
#define __IDataView_INTERFACE_DEFINED__

/* interface IDataView */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IDataView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1521B76B-F685-42a5-975A-7F58FA42C6F1")
    IDataView : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUpdateState( 
            /* [in] */ IDataViews *pDataViews,
            UINT uiUserState,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvDataViewEnable( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            LONG lParam,
            VARIANT_BOOL bEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUpdateData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObj,
            /* [in] */ ULONG pAttachData,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvWriteData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvReadData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUserCommand( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam,
            UINT nCommand,
            ULONG pUserData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataView * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDataView * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDataView * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDataView * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDataView * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUpdateState )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            UINT uiUserState,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvDataViewEnable )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            LONG lParam,
            VARIANT_BOOL bEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUpdateData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObj,
            /* [in] */ ULONG pAttachData,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvWriteData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvReadData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUserCommand )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam,
            UINT nCommand,
            ULONG pUserData);
        
        END_INTERFACE
    } IDataViewVtbl;

    interface IDataView
    {
        CONST_VTBL struct IDataViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataView_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataView_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataView_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataView_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDataView_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDataView_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDataView_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDataView_DvUpdateState(This,pDataViews,uiUserState,pDataObj,lParam)	\
    (This)->lpVtbl -> DvUpdateState(This,pDataViews,uiUserState,pDataObj,lParam)

#define IDataView_DvDataViewEnable(This,pDataViews,pDataObj,lParam,bEnable)	\
    (This)->lpVtbl -> DvDataViewEnable(This,pDataViews,pDataObj,lParam,bEnable)

#define IDataView_DvUpdateData(This,pDataViews,uiUpdateType,pDataObj,pAttachData,lParam)	\
    (This)->lpVtbl -> DvUpdateData(This,pDataViews,uiUpdateType,pDataObj,pAttachData,lParam)

#define IDataView_DvWriteData(This,pDataViews,pDataObj,lParam)	\
    (This)->lpVtbl -> DvWriteData(This,pDataViews,pDataObj,lParam)

#define IDataView_DvReadData(This,pDataViews,pDataObj,lParam)	\
    (This)->lpVtbl -> DvReadData(This,pDataViews,pDataObj,lParam)

#define IDataView_DvUserCommand(This,pDataViews,pDataObj,lParam,nCommand,pUserData)	\
    (This)->lpVtbl -> DvUserCommand(This,pDataViews,pDataObj,lParam,nCommand,pUserData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUpdateState_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    UINT uiUserState,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvUpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvDataViewEnable_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    LONG lParam,
    VARIANT_BOOL bEnable);


void __RPC_STUB IDataView_DvDataViewEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUpdateData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ UINT uiUpdateType,
    /* [in] */ ULONG pDataObj,
    /* [in] */ ULONG pAttachData,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvUpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvWriteData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvWriteData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvReadData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvReadData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUserCommand_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam,
    UINT nCommand,
    ULONG pUserData);


void __RPC_STUB IDataView_DvUserCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataView_INTERFACE_DEFINED__ */


#ifndef __IEnumDataView_INTERFACE_DEFINED__
#define __IEnumDataView_INTERFACE_DEFINED__

/* interface IEnumDataView */
/* [uuid][object] */ 


EXTERN_C const IID IID_IEnumDataView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0F243D5A-2120-4033-AF11-4620609DAD89")
    IEnumDataView : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Next( 
            ULONG celt,
            IDataView **ppDataView,
            ULONG *pceltFetched) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Skip( 
            ULONG celt) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Clone( 
            IEnumDataView **ppEnumDataView) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumDataViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumDataView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumDataView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumDataView * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumDataView * This,
            ULONG celt,
            IDataView **ppDataView,
            ULONG *pceltFetched);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Skip )( 
            IEnumDataView * This,
            ULONG celt);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumDataView * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IEnumDataView * This,
            IEnumDataView **ppEnumDataView);
        
        END_INTERFACE
    } IEnumDataViewVtbl;

    interface IEnumDataView
    {
        CONST_VTBL struct IEnumDataViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumDataView_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEnumDataView_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEnumDataView_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEnumDataView_Next(This,celt,ppDataView,pceltFetched)	\
    (This)->lpVtbl -> Next(This,celt,ppDataView,pceltFetched)

#define IEnumDataView_Skip(This,celt)	\
    (This)->lpVtbl -> Skip(This,celt)

#define IEnumDataView_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IEnumDataView_Clone(This,ppEnumDataView)	\
    (This)->lpVtbl -> Clone(This,ppEnumDataView)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Next_Proxy( 
    IEnumDataView * This,
    ULONG celt,
    IDataView **ppDataView,
    ULONG *pceltFetched);


void __RPC_STUB IEnumDataView_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Skip_Proxy( 
    IEnumDataView * This,
    ULONG celt);


void __RPC_STUB IEnumDataView_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Reset_Proxy( 
    IEnumDataView * This);


void __RPC_STUB IEnumDataView_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Clone_Proxy( 
    IEnumDataView * This,
    IEnumDataView **ppEnumDataView);


void __RPC_STUB IEnumDataView_Clone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEnumDataView_INTERFACE_DEFINED__ */


#ifndef __IDataViewsCallBack_INTERFACE_DEFINED__
#define __IDataViewsCallBack_INTERFACE_DEFINED__

/* interface IDataViewsCallBack */
/* [uuid][object] */ 


EXTERN_C const IID IID_IDataViewsCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("95ED5428-7152-456b-990E-EC4B6BA1D62F")
    IDataViewsCallBack : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateData( 
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUpdateType,
            ULONG pDataObj,
            ULONG_PTR pParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateState( 
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUserState,
            ULONG pDataObj,
            ULONG_PTR pParam) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewsCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataViewsCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataViewsCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataViewsCallBack * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateData )( 
            IDataViewsCallBack * This,
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUpdateType,
            ULONG pDataObj,
            ULONG_PTR pParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateState )( 
            IDataViewsCallBack * This,
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUserState,
            ULONG pDataObj,
            ULONG_PTR pParam);
        
        END_INTERFACE
    } IDataViewsCallBackVtbl;

    interface IDataViewsCallBack
    {
        CONST_VTBL struct IDataViewsCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataViewsCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataViewsCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataViewsCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataViewsCallBack_UpdateData(This,pDataViews,pDataView,uiUpdateType,pDataObj,pParam)	\
    (This)->lpVtbl -> UpdateData(This,pDataViews,pDataView,uiUpdateType,pDataObj,pParam)

#define IDataViewsCallBack_UpdateState(This,pDataViews,pDataView,uiUserState,pDataObj,pParam)	\
    (This)->lpVtbl -> UpdateState(This,pDataViews,pDataView,uiUserState,pDataObj,pParam)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataViewsCallBack_UpdateData_Proxy( 
    IDataViewsCallBack * This,
    IDataViews *pDataViews,
    IDataView *pDataView,
    UINT uiUpdateType,
    ULONG pDataObj,
    ULONG_PTR pParam);


void __RPC_STUB IDataViewsCallBack_UpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViewsCallBack_UpdateState_Proxy( 
    IDataViewsCallBack * This,
    IDataViews *pDataViews,
    IDataView *pDataView,
    UINT uiUserState,
    ULONG pDataObj,
    ULONG_PTR pParam);


void __RPC_STUB IDataViewsCallBack_UpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataViewsCallBack_INTERFACE_DEFINED__ */


#ifndef __IDataViews_INTERFACE_DEFINED__
#define __IDataViews_INTERFACE_DEFINED__

/* interface IDataViews */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IDataViews;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("43EC817A-32E9-47AC-AA13-7E1081EFDF40")
    IDataViews : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateState( 
            /* [in] */ UINT uiUserState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateData( 
            /* [in] */ UINT uiUpdateType) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateDataEx( 
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE WriteData( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadData( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Param( 
            /* [retval][out] */ ULONG *ppDataParam) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Param( 
            /* [in] */ ULONG pDataParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddDataView( 
            VARIANT *pVariantKey,
            IDataView *pDataView) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveDataViewKey( 
            VARIANT *pVariantKey) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DataObject( 
            /* [retval][out] */ ULONG *pDataObject) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_DataObject( 
            /* [in] */ ULONG pDataObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Item( 
            VARIANT *varItemKey,
            IDataView **pDataView) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ LONG *lCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveDataView( 
            IDataView *pDataView) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Link( 
            VARIANT *pVariantKey,
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LinkDataView( 
            IDataView *pDataView,
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_LinkCount( 
            /* [retval][out] */ LONG *pCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_UnlinkCount( 
            /* [retval][out] */ LONG *pCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AllDataViewLink( 
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewLock( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewUnlock( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewEnable( 
            VARIANT_BOOL bEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UserCommand( 
            UINT nCommand,
            ULONG pUserParam) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get__EnumDataView( 
            /* [retval][out] */ IEnumDataView **ppEnumDataView) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DataViewsCallBack( 
            /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_DataViewsCallBack( 
            /* [in] */ IDataViewsCallBack *pDataViewsCallBack) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataViews * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataViews * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataViews * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDataViews * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDataViews * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDataViews * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDataViews * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateState )( 
            IDataViews * This,
            /* [in] */ UINT uiUserState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateData )( 
            IDataViews * This,
            /* [in] */ UINT uiUpdateType);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateDataEx )( 
            IDataViews * This,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *WriteData )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadData )( 
            IDataViews * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Param )( 
            IDataViews * This,
            /* [retval][out] */ ULONG *ppDataParam);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Param )( 
            IDataViews * This,
            /* [in] */ ULONG pDataParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddDataView )( 
            IDataViews * This,
            VARIANT *pVariantKey,
            IDataView *pDataView);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveDataViewKey )( 
            IDataViews * This,
            VARIANT *pVariantKey);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataObject )( 
            IDataViews * This,
            /* [retval][out] */ ULONG *pDataObject);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DataObject )( 
            IDataViews * This,
            /* [in] */ ULONG pDataObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Item )( 
            IDataViews * This,
            VARIANT *varItemKey,
            IDataView **pDataView);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IDataViews * This,
            /* [retval][out] */ LONG *lCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveDataView )( 
            IDataViews * This,
            IDataView *pDataView);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Link )( 
            IDataViews * This,
            VARIANT *pVariantKey,
            VARIANT_BOOL bLink);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LinkDataView )( 
            IDataViews * This,
            IDataView *pDataView,
            VARIANT_BOOL bLink);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LinkCount )( 
            IDataViews * This,
            /* [retval][out] */ LONG *pCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UnlinkCount )( 
            IDataViews * This,
            /* [retval][out] */ LONG *pCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AllDataViewLink )( 
            IDataViews * This,
            VARIANT_BOOL bLink);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewLock )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewUnlock )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewEnable )( 
            IDataViews * This,
            VARIANT_BOOL bEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UserCommand )( 
            IDataViews * This,
            UINT nCommand,
            ULONG pUserParam);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get__EnumDataView )( 
            IDataViews * This,
            /* [retval][out] */ IEnumDataView **ppEnumDataView);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataViewsCallBack )( 
            IDataViews * This,
            /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DataViewsCallBack )( 
            IDataViews * This,
            /* [in] */ IDataViewsCallBack *pDataViewsCallBack);
        
        END_INTERFACE
    } IDataViewsVtbl;

    interface IDataViews
    {
        CONST_VTBL struct IDataViewsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataViews_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataViews_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataViews_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataViews_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDataViews_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDataViews_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDataViews_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDataViews_UpdateState(This,uiUserState)	\
    (This)->lpVtbl -> UpdateState(This,uiUserState)

#define IDataViews_UpdateData(This,uiUpdateType)	\
    (This)->lpVtbl -> UpdateData(This,uiUpdateType)

#define IDataViews_UpdateDataEx(This,uiUpdateType,pDataObject)	\
    (This)->lpVtbl -> UpdateDataEx(This,uiUpdateType,pDataObject)

#define IDataViews_WriteData(This)	\
    (This)->lpVtbl -> WriteData(This)

#define IDataViews_ReadData(This)	\
    (This)->lpVtbl -> ReadData(This)

#define IDataViews_get_Param(This,ppDataParam)	\
    (This)->lpVtbl -> get_Param(This,ppDataParam)

#define IDataViews_put_Param(This,pDataParam)	\
    (This)->lpVtbl -> put_Param(This,pDataParam)

#define IDataViews_AddDataView(This,pVariantKey,pDataView)	\
    (This)->lpVtbl -> AddDataView(This,pVariantKey,pDataView)

#define IDataViews_RemoveDataViewKey(This,pVariantKey)	\
    (This)->lpVtbl -> RemoveDataViewKey(This,pVariantKey)

#define IDataViews_get_DataObject(This,pDataObject)	\
    (This)->lpVtbl -> get_DataObject(This,pDataObject)

#define IDataViews_put_DataObject(This,pDataObject)	\
    (This)->lpVtbl -> put_DataObject(This,pDataObject)

#define IDataViews_Item(This,varItemKey,pDataView)	\
    (This)->lpVtbl -> Item(This,varItemKey,pDataView)

#define IDataViews_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IDataViews_RemoveDataView(This,pDataView)	\
    (This)->lpVtbl -> RemoveDataView(This,pDataView)

#define IDataViews_Link(This,pVariantKey,bLink)	\
    (This)->lpVtbl -> Link(This,pVariantKey,bLink)

#define IDataViews_LinkDataView(This,pDataView,bLink)	\
    (This)->lpVtbl -> LinkDataView(This,pDataView,bLink)

#define IDataViews_get_LinkCount(This,pCount)	\
    (This)->lpVtbl -> get_LinkCount(This,pCount)

#define IDataViews_get_UnlinkCount(This,pCount)	\
    (This)->lpVtbl -> get_UnlinkCount(This,pCount)

#define IDataViews_AllDataViewLink(This,bLink)	\
    (This)->lpVtbl -> AllDataViewLink(This,bLink)

#define IDataViews_DataViewLock(This)	\
    (This)->lpVtbl -> DataViewLock(This)

#define IDataViews_DataViewUnlock(This)	\
    (This)->lpVtbl -> DataViewUnlock(This)

#define IDataViews_DataViewEnable(This,bEnable)	\
    (This)->lpVtbl -> DataViewEnable(This,bEnable)

#define IDataViews_UserCommand(This,nCommand,pUserParam)	\
    (This)->lpVtbl -> UserCommand(This,nCommand,pUserParam)

#define IDataViews_get__EnumDataView(This,ppEnumDataView)	\
    (This)->lpVtbl -> get__EnumDataView(This,ppEnumDataView)

#define IDataViews_get_DataViewsCallBack(This,ppDataViewsCallBack)	\
    (This)->lpVtbl -> get_DataViewsCallBack(This,ppDataViewsCallBack)

#define IDataViews_put_DataViewsCallBack(This,pDataViewsCallBack)	\
    (This)->lpVtbl -> put_DataViewsCallBack(This,pDataViewsCallBack)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateState_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUserState);


void __RPC_STUB IDataViews_UpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateData_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUpdateType);


void __RPC_STUB IDataViews_UpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateDataEx_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUpdateType,
    /* [in] */ ULONG pDataObject);


void __RPC_STUB IDataViews_UpdateDataEx_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_WriteData_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_WriteData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_ReadData_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_ReadData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_Param_Proxy( 
    IDataViews * This,
    /* [retval][out] */ ULONG *ppDataParam);


void __RPC_STUB IDataViews_get_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_Param_Proxy( 
    IDataViews * This,
    /* [in] */ ULONG pDataParam);


void __RPC_STUB IDataViews_put_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_AddDataView_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey,
    IDataView *pDataView);


void __RPC_STUB IDataViews_AddDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_RemoveDataViewKey_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey);


void __RPC_STUB IDataViews_RemoveDataViewKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_DataObject_Proxy( 
    IDataViews * This,
    /* [retval][out] */ ULONG *pDataObject);


void __RPC_STUB IDataViews_get_DataObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_DataObject_Proxy( 
    IDataViews * This,
    /* [in] */ ULONG pDataObject);


void __RPC_STUB IDataViews_put_DataObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_Item_Proxy( 
    IDataViews * This,
    VARIANT *varItemKey,
    IDataView **pDataView);


void __RPC_STUB IDataViews_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_Count_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *lCount);


void __RPC_STUB IDataViews_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_RemoveDataView_Proxy( 
    IDataViews * This,
    IDataView *pDataView);


void __RPC_STUB IDataViews_RemoveDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_Link_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_Link_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_LinkDataView_Proxy( 
    IDataViews * This,
    IDataView *pDataView,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_LinkDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_LinkCount_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *pCount);


void __RPC_STUB IDataViews_get_LinkCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_UnlinkCount_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *pCount);


void __RPC_STUB IDataViews_get_UnlinkCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_AllDataViewLink_Proxy( 
    IDataViews * This,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_AllDataViewLink_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewLock_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_DataViewLock_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewUnlock_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_DataViewUnlock_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewEnable_Proxy( 
    IDataViews * This,
    VARIANT_BOOL bEnable);


void __RPC_STUB IDataViews_DataViewEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UserCommand_Proxy( 
    IDataViews * This,
    UINT nCommand,
    ULONG pUserParam);


void __RPC_STUB IDataViews_UserCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get__EnumDataView_Proxy( 
    IDataViews * This,
    /* [retval][out] */ IEnumDataView **ppEnumDataView);


void __RPC_STUB IDataViews_get__EnumDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_DataViewsCallBack_Proxy( 
    IDataViews * This,
    /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack);


void __RPC_STUB IDataViews_get_DataViewsCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_DataViewsCallBack_Proxy( 
    IDataViews * This,
    /* [in] */ IDataViewsCallBack *pDataViewsCallBack);


void __RPC_STUB IDataViews_put_DataViewsCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataViews_INTERFACE_DEFINED__ */


#ifndef __IThreadNotify_INTERFACE_DEFINED__
#define __IThreadNotify_INTERFACE_DEFINED__

/* interface IThreadNotify */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotify;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F91784BE-E329-4739-BFAA-01C3FE41539E")
    IThreadNotify : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_Begin( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_End( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotify * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotify * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_Begin )( 
            IThreadNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_End )( 
            IThreadNotify * This);
        
        END_INTERFACE
    } IThreadNotifyVtbl;

    interface IThreadNotify
    {
        CONST_VTBL struct IThreadNotifyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotify_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotify_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotify_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotify_ThreadNotify_Begin(This)	\
    (This)->lpVtbl -> ThreadNotify_Begin(This)

#define IThreadNotify_ThreadNotify_End(This)	\
    (This)->lpVtbl -> ThreadNotify_End(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotify_ThreadNotify_Begin_Proxy( 
    IThreadNotify * This);


void __RPC_STUB IThreadNotify_ThreadNotify_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotify_ThreadNotify_End_Proxy( 
    IThreadNotify * This);


void __RPC_STUB IThreadNotify_ThreadNotify_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThreadNotify_INTERFACE_DEFINED__ */


#ifndef __INotifyTextInfo_INTERFACE_DEFINED__
#define __INotifyTextInfo_INTERFACE_DEFINED__

/* interface INotifyTextInfo */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INotifyTextInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2D1A46CF-11A3-4786-A644-2FDB6920E433")
    INotifyTextInfo : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_AppendBSTR( 
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_InsertLine( 
            int nLine,
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_Begin( 
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_End( 
            BSTR bstrText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INotifyTextInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INotifyTextInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INotifyTextInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INotifyTextInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INotifyTextInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INotifyTextInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INotifyTextInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INotifyTextInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_AppendBSTR )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_InsertLine )( 
            INotifyTextInfo * This,
            int nLine,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_Begin )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_End )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        END_INTERFACE
    } INotifyTextInfoVtbl;

    interface INotifyTextInfo
    {
        CONST_VTBL struct INotifyTextInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INotifyTextInfo_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define INotifyTextInfo_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define INotifyTextInfo_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define INotifyTextInfo_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define INotifyTextInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define INotifyTextInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define INotifyTextInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define INotifyTextInfo_NotifyTextInfo_AppendBSTR(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_AppendBSTR(This,bstrText)

#define INotifyTextInfo_NotifyTextInfo_InsertLine(This,nLine,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_InsertLine(This,nLine,bstrText)

#define INotifyTextInfo_NotifyTextInfo_Begin(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_Begin(This,bstrText)

#define INotifyTextInfo_NotifyTextInfo_End(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_End(This,bstrText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_AppendBSTR_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_AppendBSTR_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_InsertLine_Proxy( 
    INotifyTextInfo * This,
    int nLine,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_InsertLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_Begin_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_End_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INotifyTextInfo_INTERFACE_DEFINED__ */


#ifndef __IProgressNotifyText_INTERFACE_DEFINED__
#define __IProgressNotifyText_INTERFACE_DEFINED__

/* interface IProgressNotifyText */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressNotifyText;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BDD00F8F-86C9-41b7-9302-02D0E7662D28")
    IProgressNotifyText : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_Progress( 
            int nProgressID,
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_Begin( 
            int nProgressID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_End( 
            int nProgressID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressNotifyTextVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressNotifyText * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressNotifyText * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressNotifyText * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_Progress )( 
            IProgressNotifyText * This,
            int nProgressID,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_Begin )( 
            IProgressNotifyText * This,
            int nProgressID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_End )( 
            IProgressNotifyText * This,
            int nProgressID);
        
        END_INTERFACE
    } IProgressNotifyTextVtbl;

    interface IProgressNotifyText
    {
        CONST_VTBL struct IProgressNotifyTextVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressNotifyText_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressNotifyText_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressNotifyText_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressNotifyText_ProgressNotifyText_Progress(This,nProgressID,bstrText)	\
    (This)->lpVtbl -> ProgressNotifyText_Progress(This,nProgressID,bstrText)

#define IProgressNotifyText_ProgressNotifyText_Begin(This,nProgressID)	\
    (This)->lpVtbl -> ProgressNotifyText_Begin(This,nProgressID)

#define IProgressNotifyText_ProgressNotifyText_End(This,nProgressID)	\
    (This)->lpVtbl -> ProgressNotifyText_End(This,nProgressID)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_Progress_Proxy( 
    IProgressNotifyText * This,
    int nProgressID,
    BSTR bstrText);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_Begin_Proxy( 
    IProgressNotifyText * This,
    int nProgressID);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_End_Proxy( 
    IProgressNotifyText * This,
    int nProgressID);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressNotifyText_INTERFACE_DEFINED__ */


#ifndef __IProgressWait_INTERFACE_DEFINED__
#define __IProgressWait_INTERFACE_DEFINED__

/* interface IProgressWait */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressWait;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6ABE364C-5AD0-41e5-BFB2-72A0474D6427")
    IProgressWait : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressWait_Begin( 
            int nProgressID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressWait_End( 
            int nProgressID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressWaitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressWait * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressWait * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressWait * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressWait_Begin )( 
            IProgressWait * This,
            int nProgressID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressWait_End )( 
            IProgressWait * This,
            int nProgressID);
        
        END_INTERFACE
    } IProgressWaitVtbl;

    interface IProgressWait
    {
        CONST_VTBL struct IProgressWaitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressWait_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressWait_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressWait_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressWait_ProgressWait_Begin(This,nProgressID)	\
    (This)->lpVtbl -> ProgressWait_Begin(This,nProgressID)

#define IProgressWait_ProgressWait_End(This,nProgressID)	\
    (This)->lpVtbl -> ProgressWait_End(This,nProgressID)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IProgressWait_ProgressWait_Begin_Proxy( 
    IProgressWait * This,
    int nProgressID);


void __RPC_STUB IProgressWait_ProgressWait_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressWait_ProgressWait_End_Proxy( 
    IProgressWait * This,
    int nProgressID);


void __RPC_STUB IProgressWait_ProgressWait_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressWait_INTERFACE_DEFINED__ */


#ifndef __IThreadNotifys_INTERFACE_DEFINED__
#define __IThreadNotifys_INTERFACE_DEFINED__

/* interface IThreadNotifys */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotifys;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7A52AA78-08C2-4d5d-AE84-8E42704577E5")
    IThreadNotifys : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ IThreadNotify *pThreadNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Remove( 
            /* [in] */ IThreadNotify *pThreadNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE clear( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_Begin( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_End( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifysVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotifys * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotifys * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IThreadNotifys * This,
            /* [in] */ IThreadNotify *pThreadNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IThreadNotifys * This,
            /* [in] */ IThreadNotify *pThreadNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *clear )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_Begin )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_End )( 
            IThreadNotifys * This);
        
        END_INTERFACE
    } IThreadNotifysVtbl;

    interface IThreadNotifys
    {
        CONST_VTBL struct IThreadNotifysVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotifys_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotifys_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotifys_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotifys_Add(This,pThreadNotify)	\
    (This)->lpVtbl -> Add(This,pThreadNotify)

#define IThreadNotifys_Remove(This,pThreadNotify)	\
    (This)->lpVtbl -> Remove(This,pThreadNotify)

#define IThreadNotifys_clear(This)	\
    (This)->lpVtbl -> clear(This)

#define IThreadNotifys_ThreadNotify_Begin(This)	\
    (This)->lpVtbl -> ThreadNotify_Begin(This)

#define IThreadNotifys_ThreadNotify_End(This)	\
    (This)->lpVtbl -> ThreadNotify_End(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_Add_Proxy( 
    IThreadNotifys * This,
    /* [in] */ IThreadNotify *pThreadNotify);


void __RPC_STUB IThreadNotifys_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_Remove_Proxy( 
    IThreadNotifys * This,
    /* [in] */ IThreadNotify *pThreadNotify);


void __RPC_STUB IThreadNotifys_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_clear_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_clear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_ThreadNotify_Begin_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_ThreadNotify_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_ThreadNotify_End_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_ThreadNotify_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThreadNotifys_INTERFACE_DEFINED__ */


#ifndef __IHeaderItem_INTERFACE_DEFINED__
#define __IHeaderItem_INTERFACE_DEFINED__

/* interface IHeaderItem */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IHeaderItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D9EDECA5-20A6-4db3-95DA-15C07D20721D")
    IHeaderItem : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE HiGetData( 
            LONG_PTR lpLVITEM,
            LONG_PTR pDataObject,
            LONG_PTR pUserData) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE HiSort( 
            LONG_PTR pDataObject,
            LONG_PTR pUserData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHeaderItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHeaderItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHeaderItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHeaderItem * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *HiGetData )( 
            IHeaderItem * This,
            LONG_PTR lpLVITEM,
            LONG_PTR pDataObject,
            LONG_PTR pUserData);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *HiSort )( 
            IHeaderItem * This,
            LONG_PTR pDataObject,
            LONG_PTR pUserData);
        
        END_INTERFACE
    } IHeaderItemVtbl;

    interface IHeaderItem
    {
        CONST_VTBL struct IHeaderItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHeaderItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHeaderItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHeaderItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHeaderItem_HiGetData(This,lpLVITEM,pDataObject,pUserData)	\
    (This)->lpVtbl -> HiGetData(This,lpLVITEM,pDataObject,pUserData)

#define IHeaderItem_HiSort(This,pDataObject,pUserData)	\
    (This)->lpVtbl -> HiSort(This,pDataObject,pUserData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderItem_HiGetData_Proxy( 
    IHeaderItem * This,
    LONG_PTR lpLVITEM,
    LONG_PTR pDataObject,
    LONG_PTR pUserData);


void __RPC_STUB IHeaderItem_HiGetData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderItem_HiSort_Proxy( 
    IHeaderItem * This,
    LONG_PTR pDataObject,
    LONG_PTR pUserData);


void __RPC_STUB IHeaderItem_HiSort_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHeaderItem_INTERFACE_DEFINED__ */


#ifndef __IHeaderCtrlEx_INTERFACE_DEFINED__
#define __IHeaderCtrlEx_INTERFACE_DEFINED__

/* interface IHeaderCtrlEx */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IHeaderCtrlEx;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F8FAF7D0-44C1-4368-9CFE-D2E5B8CB4A09")
    IHeaderCtrlEx : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nPos,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowItem( 
            int nKey,
            int nPos) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindItem( 
            int nKey,
            LONG_PTR *hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoSetting( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SubclassWindow( 
            LONG_PTR hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeItem( 
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Read( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Write( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            LONG *lCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_OtherTitle( 
            /* [retval][out] */ BSTR *strOther) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_OtherTitle( 
            BSTR strOther) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ToColumn( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHeaderCtrlExVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHeaderCtrlEx * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHeaderCtrlEx * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHeaderCtrlEx * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHeaderCtrlEx * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHeaderCtrlEx * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHeaderCtrlEx * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHeaderCtrlEx * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IHeaderCtrlEx * This,
            int nPos,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowItem )( 
            IHeaderCtrlEx * This,
            int nKey,
            int nPos);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindItem )( 
            IHeaderCtrlEx * This,
            int nKey,
            LONG_PTR *hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoSetting )( 
            IHeaderCtrlEx * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SubclassWindow )( 
            IHeaderCtrlEx * This,
            LONG_PTR hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeItem )( 
            IHeaderCtrlEx * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Read )( 
            IHeaderCtrlEx * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Write )( 
            IHeaderCtrlEx * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IHeaderCtrlEx * This,
            LONG *lCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OtherTitle )( 
            IHeaderCtrlEx * This,
            /* [retval][out] */ BSTR *strOther);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OtherTitle )( 
            IHeaderCtrlEx * This,
            BSTR strOther);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ToColumn )( 
            IHeaderCtrlEx * This);
        
        END_INTERFACE
    } IHeaderCtrlExVtbl;

    interface IHeaderCtrlEx
    {
        CONST_VTBL struct IHeaderCtrlExVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHeaderCtrlEx_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHeaderCtrlEx_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHeaderCtrlEx_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHeaderCtrlEx_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHeaderCtrlEx_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHeaderCtrlEx_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHeaderCtrlEx_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHeaderCtrlEx_InsertItem(This,nPos,nKey,hdItem)	\
    (This)->lpVtbl -> InsertItem(This,nPos,nKey,hdItem)

#define IHeaderCtrlEx_ShowItem(This,nKey,nPos)	\
    (This)->lpVtbl -> ShowItem(This,nKey,nPos)

#define IHeaderCtrlEx_FindItem(This,nKey,hdItem)	\
    (This)->lpVtbl -> FindItem(This,nKey,hdItem)

#define IHeaderCtrlEx_DoSetting(This)	\
    (This)->lpVtbl -> DoSetting(This)

#define IHeaderCtrlEx_SubclassWindow(This,hWnd)	\
    (This)->lpVtbl -> SubclassWindow(This,hWnd)

#define IHeaderCtrlEx_ChangeItem(This,dwMask,nKey,hdItem)	\
    (This)->lpVtbl -> ChangeItem(This,dwMask,nKey,hdItem)

#define IHeaderCtrlEx_Read(This,pConfig,strPath)	\
    (This)->lpVtbl -> Read(This,pConfig,strPath)

#define IHeaderCtrlEx_Write(This,pConfig,strPath)	\
    (This)->lpVtbl -> Write(This,pConfig,strPath)

#define IHeaderCtrlEx_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IHeaderCtrlEx_get_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> get_OtherTitle(This,strOther)

#define IHeaderCtrlEx_put_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> put_OtherTitle(This,strOther)

#define IHeaderCtrlEx_ToColumn(This)	\
    (This)->lpVtbl -> ToColumn(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_InsertItem_Proxy( 
    IHeaderCtrlEx * This,
    int nPos,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IHeaderCtrlEx_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ShowItem_Proxy( 
    IHeaderCtrlEx * This,
    int nKey,
    int nPos);


void __RPC_STUB IHeaderCtrlEx_ShowItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_FindItem_Proxy( 
    IHeaderCtrlEx * This,
    int nKey,
    LONG_PTR *hdItem);


void __RPC_STUB IHeaderCtrlEx_FindItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_DoSetting_Proxy( 
    IHeaderCtrlEx * This);


void __RPC_STUB IHeaderCtrlEx_DoSetting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_SubclassWindow_Proxy( 
    IHeaderCtrlEx * This,
    LONG_PTR hWnd);


void __RPC_STUB IHeaderCtrlEx_SubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ChangeItem_Proxy( 
    IHeaderCtrlEx * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IHeaderCtrlEx_ChangeItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_Read_Proxy( 
    IHeaderCtrlEx * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IHeaderCtrlEx_Read_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_Write_Proxy( 
    IHeaderCtrlEx * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IHeaderCtrlEx_Write_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_get_Count_Proxy( 
    IHeaderCtrlEx * This,
    LONG *lCount);


void __RPC_STUB IHeaderCtrlEx_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_get_OtherTitle_Proxy( 
    IHeaderCtrlEx * This,
    /* [retval][out] */ BSTR *strOther);


void __RPC_STUB IHeaderCtrlEx_get_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_put_OtherTitle_Proxy( 
    IHeaderCtrlEx * This,
    BSTR strOther);


void __RPC_STUB IHeaderCtrlEx_put_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ToColumn_Proxy( 
    IHeaderCtrlEx * This);


void __RPC_STUB IHeaderCtrlEx_ToColumn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHeaderCtrlEx_INTERFACE_DEFINED__ */


#ifndef __IListCtrlColumn_INTERFACE_DEFINED__
#define __IListCtrlColumn_INTERFACE_DEFINED__

/* interface IListCtrlColumn */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IListCtrlColumn;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8452A1B5-E8C1-4AB9-A094-CA2D4CC75CF3")
    IListCtrlColumn : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nPos,
            int nKey,
            LONG_PTR lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowItem( 
            int nPos,
            int nKey,
            IHeaderItem *pHeaderItem,
            VARIANT_BOOL bShow) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindItem( 
            int nKey,
            LONG_PTR *lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SubclassWindow( 
            LONG_PTR hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeItem( 
            ULONG dwMask,
            int nKey,
            LONG_PTR lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Read( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Write( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            LONG *lCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_OtherTitle( 
            /* [retval][out] */ BSTR *strOther) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_OtherTitle( 
            BSTR strOther) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ToColumn( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoMenu( 
            int x,
            int y) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE clear( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeHDItemParam( 
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeHDItemParamEx( 
            /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
            /* [in] */ int nSize) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetColumnWidth( 
            int nKey,
            LONG_PTR *nWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UnsubclassWindow( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoSetting( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IListCtrlColumnVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IListCtrlColumn * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IListCtrlColumn * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IListCtrlColumn * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IListCtrlColumn * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IListCtrlColumn * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IListCtrlColumn * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IListCtrlColumn * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IListCtrlColumn * This,
            int nPos,
            int nKey,
            LONG_PTR lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowItem )( 
            IListCtrlColumn * This,
            int nPos,
            int nKey,
            IHeaderItem *pHeaderItem,
            VARIANT_BOOL bShow);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindItem )( 
            IListCtrlColumn * This,
            int nKey,
            LONG_PTR *lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SubclassWindow )( 
            IListCtrlColumn * This,
            LONG_PTR hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeItem )( 
            IListCtrlColumn * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Read )( 
            IListCtrlColumn * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Write )( 
            IListCtrlColumn * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IListCtrlColumn * This,
            LONG *lCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OtherTitle )( 
            IListCtrlColumn * This,
            /* [retval][out] */ BSTR *strOther);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OtherTitle )( 
            IListCtrlColumn * This,
            BSTR strOther);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ToColumn )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoMenu )( 
            IListCtrlColumn * This,
            int x,
            int y);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *clear )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeHDItemParam )( 
            IListCtrlColumn * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeHDItemParamEx )( 
            IListCtrlColumn * This,
            /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
            /* [in] */ int nSize);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetColumnWidth )( 
            IListCtrlColumn * This,
            int nKey,
            LONG_PTR *nWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UnsubclassWindow )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoSetting )( 
            IListCtrlColumn * This);
        
        END_INTERFACE
    } IListCtrlColumnVtbl;

    interface IListCtrlColumn
    {
        CONST_VTBL struct IListCtrlColumnVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IListCtrlColumn_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IListCtrlColumn_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IListCtrlColumn_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IListCtrlColumn_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IListCtrlColumn_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IListCtrlColumn_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IListCtrlColumn_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IListCtrlColumn_InsertItem(This,nPos,nKey,lpColumn)	\
    (This)->lpVtbl -> InsertItem(This,nPos,nKey,lpColumn)

#define IListCtrlColumn_ShowItem(This,nPos,nKey,pHeaderItem,bShow)	\
    (This)->lpVtbl -> ShowItem(This,nPos,nKey,pHeaderItem,bShow)

#define IListCtrlColumn_FindItem(This,nKey,lpColumn)	\
    (This)->lpVtbl -> FindItem(This,nKey,lpColumn)

#define IListCtrlColumn_SubclassWindow(This,hWnd)	\
    (This)->lpVtbl -> SubclassWindow(This,hWnd)

#define IListCtrlColumn_ChangeItem(This,dwMask,nKey,lpColumn)	\
    (This)->lpVtbl -> ChangeItem(This,dwMask,nKey,lpColumn)

#define IListCtrlColumn_Read(This,pConfig,strPath)	\
    (This)->lpVtbl -> Read(This,pConfig,strPath)

#define IListCtrlColumn_Write(This,pConfig,strPath)	\
    (This)->lpVtbl -> Write(This,pConfig,strPath)

#define IListCtrlColumn_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IListCtrlColumn_get_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> get_OtherTitle(This,strOther)

#define IListCtrlColumn_put_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> put_OtherTitle(This,strOther)

#define IListCtrlColumn_ToColumn(This)	\
    (This)->lpVtbl -> ToColumn(This)

#define IListCtrlColumn_DoMenu(This,x,y)	\
    (This)->lpVtbl -> DoMenu(This,x,y)

#define IListCtrlColumn_clear(This)	\
    (This)->lpVtbl -> clear(This)

#define IListCtrlColumn_ChangeHDItemParam(This,dwMask,nKey,hdItem)	\
    (This)->lpVtbl -> ChangeHDItemParam(This,dwMask,nKey,hdItem)

#define IListCtrlColumn_ChangeHDItemParamEx(This,nHeaderItem,nSize)	\
    (This)->lpVtbl -> ChangeHDItemParamEx(This,nHeaderItem,nSize)

#define IListCtrlColumn_GetColumnWidth(This,nKey,nWidth)	\
    (This)->lpVtbl -> GetColumnWidth(This,nKey,nWidth)

#define IListCtrlColumn_UnsubclassWindow(This)	\
    (This)->lpVtbl -> UnsubclassWindow(This)

#define IListCtrlColumn_DoSetting(This)	\
    (This)->lpVtbl -> DoSetting(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_InsertItem_Proxy( 
    IListCtrlColumn * This,
    int nPos,
    int nKey,
    LONG_PTR lpColumn);


void __RPC_STUB IListCtrlColumn_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ShowItem_Proxy( 
    IListCtrlColumn * This,
    int nPos,
    int nKey,
    IHeaderItem *pHeaderItem,
    VARIANT_BOOL bShow);


void __RPC_STUB IListCtrlColumn_ShowItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_FindItem_Proxy( 
    IListCtrlColumn * This,
    int nKey,
    LONG_PTR *lpColumn);


void __RPC_STUB IListCtrlColumn_FindItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_SubclassWindow_Proxy( 
    IListCtrlColumn * This,
    LONG_PTR hWnd);


void __RPC_STUB IListCtrlColumn_SubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeItem_Proxy( 
    IListCtrlColumn * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR lpColumn);


void __RPC_STUB IListCtrlColumn_ChangeItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_Read_Proxy( 
    IListCtrlColumn * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IListCtrlColumn_Read_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_Write_Proxy( 
    IListCtrlColumn * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IListCtrlColumn_Write_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_get_Count_Proxy( 
    IListCtrlColumn * This,
    LONG *lCount);


void __RPC_STUB IListCtrlColumn_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_get_OtherTitle_Proxy( 
    IListCtrlColumn * This,
    /* [retval][out] */ BSTR *strOther);


void __RPC_STUB IListCtrlColumn_get_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_put_OtherTitle_Proxy( 
    IListCtrlColumn * This,
    BSTR strOther);


void __RPC_STUB IListCtrlColumn_put_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ToColumn_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_ToColumn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_DoMenu_Proxy( 
    IListCtrlColumn * This,
    int x,
    int y);


void __RPC_STUB IListCtrlColumn_DoMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_clear_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_clear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeHDItemParam_Proxy( 
    IListCtrlColumn * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IListCtrlColumn_ChangeHDItemParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeHDItemParamEx_Proxy( 
    IListCtrlColumn * This,
    /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
    /* [in] */ int nSize);


void __RPC_STUB IListCtrlColumn_ChangeHDItemParamEx_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_GetColumnWidth_Proxy( 
    IListCtrlColumn * This,
    int nKey,
    LONG_PTR *nWidth);


void __RPC_STUB IListCtrlColumn_GetColumnWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_UnsubclassWindow_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_UnsubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_DoSetting_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_DoSetting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IListCtrlColumn_INTERFACE_DEFINED__ */


#ifndef __ITreePropertySheet_INTERFACE_DEFINED__
#define __ITreePropertySheet_INTERFACE_DEFINED__

/* interface ITreePropertySheet */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITreePropertySheet;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F16A5240-C752-47B6-B34C-3F4ED7231FDC")
    ITreePropertySheet : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Create( 
            HWND hWndCtrl,
            BOOL bChild) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetConfigPos( 
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetTreeCtrlWidth( 
            /* [in] */ int iWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTreeCtrlWidth( 
            /* [in] */ int *ipWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTreeCtrlWnd( 
            /* [out] */ LONG_PTR *hwnd) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITreePropertySheetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITreePropertySheet * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITreePropertySheet * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITreePropertySheet * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITreePropertySheet * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITreePropertySheet * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITreePropertySheet * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITreePropertySheet * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            ITreePropertySheet * This,
            HWND hWndCtrl);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Create )( 
            ITreePropertySheet * This,
            HWND hWndCtrl,
            BOOL bChild);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetConfigPos )( 
            ITreePropertySheet * This,
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetTreeCtrlWidth )( 
            ITreePropertySheet * This,
            /* [in] */ int iWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTreeCtrlWidth )( 
            ITreePropertySheet * This,
            /* [in] */ int *ipWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTreeCtrlWnd )( 
            ITreePropertySheet * This,
            /* [out] */ LONG_PTR *hwnd);
        
        END_INTERFACE
    } ITreePropertySheetVtbl;

    interface ITreePropertySheet
    {
        CONST_VTBL struct ITreePropertySheetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITreePropertySheet_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITreePropertySheet_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITreePropertySheet_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITreePropertySheet_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITreePropertySheet_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITreePropertySheet_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITreePropertySheet_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITreePropertySheet_DoModal(This,hWndCtrl)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl)

#define ITreePropertySheet_Create(This,hWndCtrl,bChild)	\
    (This)->lpVtbl -> Create(This,hWndCtrl,bChild)

#define ITreePropertySheet_SetConfigPos(This,pxmlDoc,bsPath)	\
    (This)->lpVtbl -> SetConfigPos(This,pxmlDoc,bsPath)

#define ITreePropertySheet_SetTreeCtrlWidth(This,iWidth)	\
    (This)->lpVtbl -> SetTreeCtrlWidth(This,iWidth)

#define ITreePropertySheet_GetTreeCtrlWidth(This,ipWidth)	\
    (This)->lpVtbl -> GetTreeCtrlWidth(This,ipWidth)

#define ITreePropertySheet_GetTreeCtrlWnd(This,hwnd)	\
    (This)->lpVtbl -> GetTreeCtrlWnd(This,hwnd)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_DoModal_Proxy( 
    ITreePropertySheet * This,
    HWND hWndCtrl);


void __RPC_STUB ITreePropertySheet_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_Create_Proxy( 
    ITreePropertySheet * This,
    HWND hWndCtrl,
    BOOL bChild);


void __RPC_STUB ITreePropertySheet_Create_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_SetConfigPos_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ ULONG *pxmlDoc,
    /* [in] */ BSTR bsPath);


void __RPC_STUB ITreePropertySheet_SetConfigPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_SetTreeCtrlWidth_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ int iWidth);


void __RPC_STUB ITreePropertySheet_SetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_GetTreeCtrlWidth_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ int *ipWidth);


void __RPC_STUB ITreePropertySheet_GetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_GetTreeCtrlWnd_Proxy( 
    ITreePropertySheet * This,
    /* [out] */ LONG_PTR *hwnd);


void __RPC_STUB ITreePropertySheet_GetTreeCtrlWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITreePropertySheet_INTERFACE_DEFINED__ */


#ifndef __IRegsvrDll_INTERFACE_DEFINED__
#define __IRegsvrDll_INTERFACE_DEFINED__

/* interface IRegsvrDll */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegsvrDll;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("99EA1F94-5EAB-413F-BA1E-359FEBCCFA25")
    IRegsvrDll : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegsvrComFramework( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegsvrComponent( 
            BSTR *bstrItem,
            int nSize) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ProgressNotifyText( 
            /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ProgressNotifyText( 
            /* [in] */ IProgressNotifyText *pProgressNotifyText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRegsvrDllVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegsvrDll * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegsvrDll * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegsvrDll * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegsvrDll * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegsvrDll * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegsvrDll * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegsvrDll * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegsvrComFramework )( 
            IRegsvrDll * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegsvrComponent )( 
            IRegsvrDll * This,
            BSTR *bstrItem,
            int nSize);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgressNotifyText )( 
            IRegsvrDll * This,
            /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProgressNotifyText )( 
            IRegsvrDll * This,
            /* [in] */ IProgressNotifyText *pProgressNotifyText);
        
        END_INTERFACE
    } IRegsvrDllVtbl;

    interface IRegsvrDll
    {
        CONST_VTBL struct IRegsvrDllVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegsvrDll_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegsvrDll_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegsvrDll_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegsvrDll_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegsvrDll_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegsvrDll_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegsvrDll_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IRegsvrDll_RegsvrComFramework(This)	\
    (This)->lpVtbl -> RegsvrComFramework(This)

#define IRegsvrDll_RegsvrComponent(This,bstrItem,nSize)	\
    (This)->lpVtbl -> RegsvrComponent(This,bstrItem,nSize)

#define IRegsvrDll_get_ProgressNotifyText(This,ppProgressNotifyText)	\
    (This)->lpVtbl -> get_ProgressNotifyText(This,ppProgressNotifyText)

#define IRegsvrDll_put_ProgressNotifyText(This,pProgressNotifyText)	\
    (This)->lpVtbl -> put_ProgressNotifyText(This,pProgressNotifyText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_RegsvrComFramework_Proxy( 
    IRegsvrDll * This);


void __RPC_STUB IRegsvrDll_RegsvrComFramework_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_RegsvrComponent_Proxy( 
    IRegsvrDll * This,
    BSTR *bstrItem,
    int nSize);


void __RPC_STUB IRegsvrDll_RegsvrComponent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_get_ProgressNotifyText_Proxy( 
    IRegsvrDll * This,
    /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText);


void __RPC_STUB IRegsvrDll_get_ProgressNotifyText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_put_ProgressNotifyText_Proxy( 
    IRegsvrDll * This,
    /* [in] */ IProgressNotifyText *pProgressNotifyText);


void __RPC_STUB IRegsvrDll_put_ProgressNotifyText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRegsvrDll_INTERFACE_DEFINED__ */


#ifndef __IThreadNotifyInfoDlg_INTERFACE_DEFINED__
#define __IThreadNotifyInfoDlg_INTERFACE_DEFINED__

/* interface IThreadNotifyInfoDlg */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotifyInfoDlg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DF9C66BC-CC8A-48C2-890A-5BDD702B0A17")
    IThreadNotifyInfoDlg : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifyInfoDlgVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotifyInfoDlg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotifyInfoDlg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IThreadNotifyInfoDlg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IThreadNotifyInfoDlgVtbl;

    interface IThreadNotifyInfoDlg
    {
        CONST_VTBL struct IThreadNotifyInfoDlgVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotifyInfoDlg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotifyInfoDlg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotifyInfoDlg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotifyInfoDlg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IThreadNotifyInfoDlg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IThreadNotifyInfoDlg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IThreadNotifyInfoDlg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IThreadNotifyInfoDlg_INTERFACE_DEFINED__ */


#ifndef __IManagerCallback_INTERFACE_DEFINED__
#define __IManagerCallback_INTERFACE_DEFINED__

/* interface IManagerCallback */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IManagerCallback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D0B839F0-B198-4E74-9F1F-C59FAF5E8A60")
    IManagerCallback : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IManagerCallbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IManagerCallback * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IManagerCallback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IManagerCallback * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IManagerCallback * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IManagerCallback * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IManagerCallback * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IManagerCallback * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IManagerCallbackVtbl;

    interface IManagerCallback
    {
        CONST_VTBL struct IManagerCallbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IManagerCallback_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IManagerCallback_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IManagerCallback_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IManagerCallback_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IManagerCallback_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IManagerCallback_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IManagerCallback_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IManagerCallback_INTERFACE_DEFINED__ */


#ifndef __ISheetPanel_INTERFACE_DEFINED__
#define __ISheetPanel_INTERFACE_DEFINED__

/* interface ISheetPanel */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISheetPanel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("42DB35E0-B3B5-4EFD-A841-C9DF0F2561C0")
    ISheetPanel : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl,
            int *piRet) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertPageCLSID( 
            int iIndex,
            REFCLSID clsid) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISheetPanelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISheetPanel * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISheetPanel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISheetPanel * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISheetPanel * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISheetPanel * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISheetPanel * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISheetPanel * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            ISheetPanel * This,
            HWND hWndCtrl,
            int *piRet);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertPageCLSID )( 
            ISheetPanel * This,
            int iIndex,
            REFCLSID clsid);
        
        END_INTERFACE
    } ISheetPanelVtbl;

    interface ISheetPanel
    {
        CONST_VTBL struct ISheetPanelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISheetPanel_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISheetPanel_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISheetPanel_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISheetPanel_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISheetPanel_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISheetPanel_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISheetPanel_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISheetPanel_DoModal(This,hWndCtrl,piRet)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl,piRet)

#define ISheetPanel_InsertPageCLSID(This,iIndex,clsid)	\
    (This)->lpVtbl -> InsertPageCLSID(This,iIndex,clsid)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ISheetPanel_DoModal_Proxy( 
    ISheetPanel * This,
    HWND hWndCtrl,
    int *piRet);


void __RPC_STUB ISheetPanel_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISheetPanel_InsertPageCLSID_Proxy( 
    ISheetPanel * This,
    int iIndex,
    REFCLSID clsid);


void __RPC_STUB ISheetPanel_InsertPageCLSID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISheetPanel_INTERFACE_DEFINED__ */


#ifndef __IThumbnailInfo_INTERFACE_DEFINED__
#define __IThumbnailInfo_INTERFACE_DEFINED__

/* interface IThumbnailInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThumbnailInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E58E96A8-8A36-4669-B873-1031E60A8329")
    IThumbnailInfo : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSize( 
            /* [in] */ UINT uiWidth,
            /* [in] */ UINT uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSize( 
            /* [out] */ UINT *uiWidth,
            /* [out] */ UINT *uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetImageList( 
            /* [out] */ ULONG_PTR *pImageList) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThumbnailInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThumbnailInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThumbnailInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThumbnailInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IThumbnailInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IThumbnailInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IThumbnailInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IThumbnailInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSize )( 
            IThumbnailInfo * This,
            /* [in] */ UINT uiWidth,
            /* [in] */ UINT uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSize )( 
            IThumbnailInfo * This,
            /* [out] */ UINT *uiWidth,
            /* [out] */ UINT *uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetImageList )( 
            IThumbnailInfo * This,
            /* [out] */ ULONG_PTR *pImageList);
        
        END_INTERFACE
    } IThumbnailInfoVtbl;

    interface IThumbnailInfo
    {
        CONST_VTBL struct IThumbnailInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThumbnailInfo_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThumbnailInfo_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThumbnailInfo_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThumbnailInfo_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IThumbnailInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IThumbnailInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IThumbnailInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IThumbnailInfo_CreateSize(This,uiWidth,uiHeight)	\
    (This)->lpVtbl -> CreateSize(This,uiWidth,uiHeight)

#define IThumbnailInfo_GetSize(This,uiWidth,uiHeight)	\
    (This)->lpVtbl -> GetSize(This,uiWidth,uiHeight)

#define IThumbnailInfo_GetImageList(This,pImageList)	\
    (This)->lpVtbl -> GetImageList(This,pImageList)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_CreateSize_Proxy( 
    IThumbnailInfo * This,
    /* [in] */ UINT uiWidth,
    /* [in] */ UINT uiHeight);


void __RPC_STUB IThumbnailInfo_CreateSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_GetSize_Proxy( 
    IThumbnailInfo * This,
    /* [out] */ UINT *uiWidth,
    /* [out] */ UINT *uiHeight);


void __RPC_STUB IThumbnailInfo_GetSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_GetImageList_Proxy( 
    IThumbnailInfo * This,
    /* [out] */ ULONG_PTR *pImageList);


void __RPC_STUB IThumbnailInfo_GetImageList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThumbnailInfo_INTERFACE_DEFINED__ */


#ifndef __ICommandItem_INTERFACE_DEFINED__
#define __ICommandItem_INTERFACE_DEFINED__

/* interface ICommandItem */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5751267A-6133-4570-971D-57AFB7A0C35D")
    ICommandItem : public IDispatch
    {
    public:
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Caption( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Caption( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Enable( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Enable( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Check( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Check( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Radio( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Radio( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Visible( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Visible( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Cmd( 
            /* [retval][out] */ ULONG *nCmdID) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Cmd( 
            /* [in] */ ULONG nCmdID) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Index( 
            /* [retval][out] */ ULONG *nIndex) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CmdItemType( 
            /* [retval][out] */ enumCmdItemType *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_CmdItemType( 
            /* [in] */ enumCmdItemType newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Width( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Width( 
            /* [in] */ LONG newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommand( 
            /* [retval][out] */ IDispatch **pNotifyCommand) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_NotifyCommand( 
            /* [in] */ IDispatch *pNotifyCommand) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NextCmd( 
            /* [retval][out] */ VARIANT_BOOL *pHandle) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_NextCmd( 
            /* [in] */ VARIANT_BOOL pHandle) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_IconIndex( 
            /* [retval][out] */ ULONG *nIndex) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_IconIndex( 
            /* [in] */ ULONG nIndex) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Icon( 
            /* [retval][out] */ HICON *hIcon) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Icon( 
            /* [in] */ HICON hIcon) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Bitmap( 
            /* [retval][out] */ HBITMAP *hBitmap) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Bitmap( 
            /* [in] */ HBITMAP hBitmap) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ItemWindow( 
            /* [retval][out] */ ULONG *phWnd) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ItemWindow( 
            /* [in] */ ULONG hWnd) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Param( 
            /* [retval][out] */ ULONG **pParam) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Param( 
            /* [in] */ ULONG lParam) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Value( 
            /* [retval][out] */ ULONG *pBaseData) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Value( 
            /* [in] */ ULONG *lParam) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ChildCommandBar( 
            /* [retval][out] */ ICommandBar **pCommandBars) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ChildCommandBar( 
            /* [in] */ ICommandBar *pCommandBars) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ParentCommandBar( 
            /* [retval][out] */ ICommandBar **pCommandBars) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE IsChildItem( 
            /* [retval][out] */ VARIANT_BOOL *pChild) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateCommandBar( 
            /* [retval][out] */ ICommandBar **pCommandBars) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MoveItem( 
            /* [in] */ ULONG nNewIndex) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ UINT uPosition,
            UINT uFlags) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandItem * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandItem * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandItem * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandItem * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandItem * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Caption )( 
            ICommandItem * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Caption )( 
            ICommandItem * This,
            /* [in] */ BSTR newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Enable )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Enable )( 
            ICommandItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Check )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Check )( 
            ICommandItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Radio )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Radio )( 
            ICommandItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            ICommandItem * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Cmd )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG *nCmdID);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Cmd )( 
            ICommandItem * This,
            /* [in] */ ULONG nCmdID);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Index )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG *nIndex);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CmdItemType )( 
            ICommandItem * This,
            /* [retval][out] */ enumCmdItemType *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CmdItemType )( 
            ICommandItem * This,
            /* [in] */ enumCmdItemType newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Width )( 
            ICommandItem * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Width )( 
            ICommandItem * This,
            /* [in] */ LONG newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            ICommandItem * This,
            /* [retval][out] */ IDispatch **pNotifyCommand);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyCommand )( 
            ICommandItem * This,
            /* [in] */ IDispatch *pNotifyCommand);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NextCmd )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pHandle);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NextCmd )( 
            ICommandItem * This,
            /* [in] */ VARIANT_BOOL pHandle);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_IconIndex )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG *nIndex);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_IconIndex )( 
            ICommandItem * This,
            /* [in] */ ULONG nIndex);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Icon )( 
            ICommandItem * This,
            /* [retval][out] */ HICON *hIcon);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Icon )( 
            ICommandItem * This,
            /* [in] */ HICON hIcon);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Bitmap )( 
            ICommandItem * This,
            /* [retval][out] */ HBITMAP *hBitmap);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Bitmap )( 
            ICommandItem * This,
            /* [in] */ HBITMAP hBitmap);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ItemWindow )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG *phWnd);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ItemWindow )( 
            ICommandItem * This,
            /* [in] */ ULONG hWnd);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Param )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG **pParam);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Param )( 
            ICommandItem * This,
            /* [in] */ ULONG lParam);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Value )( 
            ICommandItem * This,
            /* [retval][out] */ ULONG *pBaseData);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Value )( 
            ICommandItem * This,
            /* [in] */ ULONG *lParam);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ChildCommandBar )( 
            ICommandItem * This,
            /* [retval][out] */ ICommandBar **pCommandBars);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ChildCommandBar )( 
            ICommandItem * This,
            /* [in] */ ICommandBar *pCommandBars);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ParentCommandBar )( 
            ICommandItem * This,
            /* [retval][out] */ ICommandBar **pCommandBars);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsChildItem )( 
            ICommandItem * This,
            /* [retval][out] */ VARIANT_BOOL *pChild);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateCommandBar )( 
            ICommandItem * This,
            /* [retval][out] */ ICommandBar **pCommandBars);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MoveItem )( 
            ICommandItem * This,
            /* [in] */ ULONG nNewIndex);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            ICommandItem * This,
            /* [in] */ UINT uPosition,
            UINT uFlags);
        
        END_INTERFACE
    } ICommandItemVtbl;

    interface ICommandItem
    {
        CONST_VTBL struct ICommandItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICommandItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICommandItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICommandItem_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICommandItem_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICommandItem_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICommandItem_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICommandItem_get_Caption(This,pVal)	\
    (This)->lpVtbl -> get_Caption(This,pVal)

#define ICommandItem_put_Caption(This,newVal)	\
    (This)->lpVtbl -> put_Caption(This,newVal)

#define ICommandItem_get_Enable(This,pVal)	\
    (This)->lpVtbl -> get_Enable(This,pVal)

#define ICommandItem_put_Enable(This,newVal)	\
    (This)->lpVtbl -> put_Enable(This,newVal)

#define ICommandItem_get_Check(This,pVal)	\
    (This)->lpVtbl -> get_Check(This,pVal)

#define ICommandItem_put_Check(This,newVal)	\
    (This)->lpVtbl -> put_Check(This,newVal)

#define ICommandItem_get_Radio(This,pVal)	\
    (This)->lpVtbl -> get_Radio(This,pVal)

#define ICommandItem_put_Radio(This,newVal)	\
    (This)->lpVtbl -> put_Radio(This,newVal)

#define ICommandItem_get_Visible(This,pVal)	\
    (This)->lpVtbl -> get_Visible(This,pVal)

#define ICommandItem_put_Visible(This,newVal)	\
    (This)->lpVtbl -> put_Visible(This,newVal)

#define ICommandItem_get_Cmd(This,nCmdID)	\
    (This)->lpVtbl -> get_Cmd(This,nCmdID)

#define ICommandItem_put_Cmd(This,nCmdID)	\
    (This)->lpVtbl -> put_Cmd(This,nCmdID)

#define ICommandItem_get_Index(This,nIndex)	\
    (This)->lpVtbl -> get_Index(This,nIndex)

#define ICommandItem_get_CmdItemType(This,pVal)	\
    (This)->lpVtbl -> get_CmdItemType(This,pVal)

#define ICommandItem_put_CmdItemType(This,newVal)	\
    (This)->lpVtbl -> put_CmdItemType(This,newVal)

#define ICommandItem_get_Width(This,pVal)	\
    (This)->lpVtbl -> get_Width(This,pVal)

#define ICommandItem_put_Width(This,newVal)	\
    (This)->lpVtbl -> put_Width(This,newVal)

#define ICommandItem_get_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,pNotifyCommand)

#define ICommandItem_put_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> put_NotifyCommand(This,pNotifyCommand)

#define ICommandItem_get_NextCmd(This,pHandle)	\
    (This)->lpVtbl -> get_NextCmd(This,pHandle)

#define ICommandItem_put_NextCmd(This,pHandle)	\
    (This)->lpVtbl -> put_NextCmd(This,pHandle)

#define ICommandItem_get_IconIndex(This,nIndex)	\
    (This)->lpVtbl -> get_IconIndex(This,nIndex)

#define ICommandItem_put_IconIndex(This,nIndex)	\
    (This)->lpVtbl -> put_IconIndex(This,nIndex)

#define ICommandItem_get_Icon(This,hIcon)	\
    (This)->lpVtbl -> get_Icon(This,hIcon)

#define ICommandItem_put_Icon(This,hIcon)	\
    (This)->lpVtbl -> put_Icon(This,hIcon)

#define ICommandItem_get_Bitmap(This,hBitmap)	\
    (This)->lpVtbl -> get_Bitmap(This,hBitmap)

#define ICommandItem_put_Bitmap(This,hBitmap)	\
    (This)->lpVtbl -> put_Bitmap(This,hBitmap)

#define ICommandItem_get_ItemWindow(This,phWnd)	\
    (This)->lpVtbl -> get_ItemWindow(This,phWnd)

#define ICommandItem_put_ItemWindow(This,hWnd)	\
    (This)->lpVtbl -> put_ItemWindow(This,hWnd)

#define ICommandItem_get_Param(This,pParam)	\
    (This)->lpVtbl -> get_Param(This,pParam)

#define ICommandItem_put_Param(This,lParam)	\
    (This)->lpVtbl -> put_Param(This,lParam)

#define ICommandItem_get_Value(This,pBaseData)	\
    (This)->lpVtbl -> get_Value(This,pBaseData)

#define ICommandItem_put_Value(This,lParam)	\
    (This)->lpVtbl -> put_Value(This,lParam)

#define ICommandItem_get_ChildCommandBar(This,pCommandBars)	\
    (This)->lpVtbl -> get_ChildCommandBar(This,pCommandBars)

#define ICommandItem_put_ChildCommandBar(This,pCommandBars)	\
    (This)->lpVtbl -> put_ChildCommandBar(This,pCommandBars)

#define ICommandItem_get_ParentCommandBar(This,pCommandBars)	\
    (This)->lpVtbl -> get_ParentCommandBar(This,pCommandBars)

#define ICommandItem_IsChildItem(This,pChild)	\
    (This)->lpVtbl -> IsChildItem(This,pChild)

#define ICommandItem_CreateCommandBar(This,pCommandBars)	\
    (This)->lpVtbl -> CreateCommandBar(This,pCommandBars)

#define ICommandItem_MoveItem(This,nNewIndex)	\
    (This)->lpVtbl -> MoveItem(This,nNewIndex)

#define ICommandItem_DeleteItem(This,uPosition,uFlags)	\
    (This)->lpVtbl -> DeleteItem(This,uPosition,uFlags)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Caption_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ICommandItem_get_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Caption_Proxy( 
    ICommandItem * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ICommandItem_put_Caption_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Enable_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICommandItem_get_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Enable_Proxy( 
    ICommandItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICommandItem_put_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Check_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICommandItem_get_Check_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Check_Proxy( 
    ICommandItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICommandItem_put_Check_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Radio_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICommandItem_get_Radio_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Radio_Proxy( 
    ICommandItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICommandItem_put_Radio_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Visible_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICommandItem_get_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Visible_Proxy( 
    ICommandItem * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICommandItem_put_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Cmd_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG *nCmdID);


void __RPC_STUB ICommandItem_get_Cmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Cmd_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG nCmdID);


void __RPC_STUB ICommandItem_put_Cmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Index_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG *nIndex);


void __RPC_STUB ICommandItem_get_Index_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_CmdItemType_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ enumCmdItemType *pVal);


void __RPC_STUB ICommandItem_get_CmdItemType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_CmdItemType_Proxy( 
    ICommandItem * This,
    /* [in] */ enumCmdItemType newVal);


void __RPC_STUB ICommandItem_put_CmdItemType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Width_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB ICommandItem_get_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Width_Proxy( 
    ICommandItem * This,
    /* [in] */ LONG newVal);


void __RPC_STUB ICommandItem_put_Width_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_NotifyCommand_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ IDispatch **pNotifyCommand);


void __RPC_STUB ICommandItem_get_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_NotifyCommand_Proxy( 
    ICommandItem * This,
    /* [in] */ IDispatch *pNotifyCommand);


void __RPC_STUB ICommandItem_put_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_NextCmd_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pHandle);


void __RPC_STUB ICommandItem_get_NextCmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_NextCmd_Proxy( 
    ICommandItem * This,
    /* [in] */ VARIANT_BOOL pHandle);


void __RPC_STUB ICommandItem_put_NextCmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_IconIndex_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG *nIndex);


void __RPC_STUB ICommandItem_get_IconIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_IconIndex_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG nIndex);


void __RPC_STUB ICommandItem_put_IconIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Icon_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ HICON *hIcon);


void __RPC_STUB ICommandItem_get_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Icon_Proxy( 
    ICommandItem * This,
    /* [in] */ HICON hIcon);


void __RPC_STUB ICommandItem_put_Icon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Bitmap_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ HBITMAP *hBitmap);


void __RPC_STUB ICommandItem_get_Bitmap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Bitmap_Proxy( 
    ICommandItem * This,
    /* [in] */ HBITMAP hBitmap);


void __RPC_STUB ICommandItem_put_Bitmap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_ItemWindow_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG *phWnd);


void __RPC_STUB ICommandItem_get_ItemWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_ItemWindow_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG hWnd);


void __RPC_STUB ICommandItem_put_ItemWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Param_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG **pParam);


void __RPC_STUB ICommandItem_get_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Param_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG lParam);


void __RPC_STUB ICommandItem_put_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_Value_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ULONG *pBaseData);


void __RPC_STUB ICommandItem_get_Value_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_Value_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG *lParam);


void __RPC_STUB ICommandItem_put_Value_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_ChildCommandBar_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ICommandBar **pCommandBars);


void __RPC_STUB ICommandItem_get_ChildCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandItem_put_ChildCommandBar_Proxy( 
    ICommandItem * This,
    /* [in] */ ICommandBar *pCommandBars);


void __RPC_STUB ICommandItem_put_ChildCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandItem_get_ParentCommandBar_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ICommandBar **pCommandBars);


void __RPC_STUB ICommandItem_get_ParentCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandItem_IsChildItem_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ VARIANT_BOOL *pChild);


void __RPC_STUB ICommandItem_IsChildItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandItem_CreateCommandBar_Proxy( 
    ICommandItem * This,
    /* [retval][out] */ ICommandBar **pCommandBars);


void __RPC_STUB ICommandItem_CreateCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandItem_MoveItem_Proxy( 
    ICommandItem * This,
    /* [in] */ ULONG nNewIndex);


void __RPC_STUB ICommandItem_MoveItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandItem_DeleteItem_Proxy( 
    ICommandItem * This,
    /* [in] */ UINT uPosition,
    UINT uFlags);


void __RPC_STUB ICommandItem_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICommandItem_INTERFACE_DEFINED__ */


#ifndef __INotifyCommand_INTERFACE_DEFINED__
#define __INotifyCommand_INTERFACE_DEFINED__

/* interface INotifyCommand */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INotifyCommand;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("37B38FC2-B0EE-11DB-B8A6-005056C00008")
    INotifyCommand : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyCommand( 
            UINT codeNotify,
            int nCmdID,
            ICommandItem *pCmdUI) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyCommandPoint( 
            UINT nCmdID,
            POINT *pt,
            ICommandItem *pCmdUI,
            IDispatch *pPopupMenuBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCommandTips( 
            UINT nCmdID,
            BSTR *bstrTips) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryState( 
            UINT nCmdID,
            ICommandItem *pCmdUI) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ExpansionCommand( 
            UINT nCmdID,
            ICommandItem *pCmdUI) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE wmNotify( 
            UINT_PTR wParam,
            LONG_PTR lParam,
            ICommandItem *pCmdUI) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetParam( 
            ULONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BuildMenuNc( 
            IPopupMenuBar *pPopupMenuBar,
            LPCSTR pszPath,
            UINT uiPos,
            enumInsertMenu umInsertMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryChildNotifyCommandParam( 
            IID *piid,
            INotifyCommand *pChildNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryNotifyCommandParam( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetNotifyCommandParent( 
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetNotifyCommandParent( 
            INotifyCommand **ppNotifyCommand) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INotifyCommandVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INotifyCommand * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INotifyCommand * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INotifyCommand * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyCommand )( 
            INotifyCommand * This,
            UINT codeNotify,
            int nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyCommandPoint )( 
            INotifyCommand * This,
            UINT nCmdID,
            POINT *pt,
            ICommandItem *pCmdUI,
            IDispatch *pPopupMenuBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCommandTips )( 
            INotifyCommand * This,
            UINT nCmdID,
            BSTR *bstrTips);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryState )( 
            INotifyCommand * This,
            UINT nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ExpansionCommand )( 
            INotifyCommand * This,
            UINT nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *wmNotify )( 
            INotifyCommand * This,
            UINT_PTR wParam,
            LONG_PTR lParam,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetParam )( 
            INotifyCommand * This,
            ULONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BuildMenuNc )( 
            INotifyCommand * This,
            IPopupMenuBar *pPopupMenuBar,
            LPCSTR pszPath,
            UINT uiPos,
            enumInsertMenu umInsertMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryChildNotifyCommandParam )( 
            INotifyCommand * This,
            IID *piid,
            INotifyCommand *pChildNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryNotifyCommandParam )( 
            INotifyCommand * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetNotifyCommandParent )( 
            INotifyCommand * This,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetNotifyCommandParent )( 
            INotifyCommand * This,
            INotifyCommand **ppNotifyCommand);
        
        END_INTERFACE
    } INotifyCommandVtbl;

    interface INotifyCommand
    {
        CONST_VTBL struct INotifyCommandVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INotifyCommand_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define INotifyCommand_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define INotifyCommand_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define INotifyCommand_NotifyCommand(This,codeNotify,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> NotifyCommand(This,codeNotify,nCmdID,pCmdUI)

#define INotifyCommand_NotifyCommandPoint(This,nCmdID,pt,pCmdUI,pPopupMenuBar)	\
    (This)->lpVtbl -> NotifyCommandPoint(This,nCmdID,pt,pCmdUI,pPopupMenuBar)

#define INotifyCommand_GetCommandTips(This,nCmdID,bstrTips)	\
    (This)->lpVtbl -> GetCommandTips(This,nCmdID,bstrTips)

#define INotifyCommand_QueryState(This,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> QueryState(This,nCmdID,pCmdUI)

#define INotifyCommand_ExpansionCommand(This,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> ExpansionCommand(This,nCmdID,pCmdUI)

#define INotifyCommand_wmNotify(This,wParam,lParam,pCmdUI)	\
    (This)->lpVtbl -> wmNotify(This,wParam,lParam,pCmdUI)

#define INotifyCommand_SetParam(This,lParam)	\
    (This)->lpVtbl -> SetParam(This,lParam)

#define INotifyCommand_BuildMenuNc(This,pPopupMenuBar,pszPath,uiPos,umInsertMenu)	\
    (This)->lpVtbl -> BuildMenuNc(This,pPopupMenuBar,pszPath,uiPos,umInsertMenu)

#define INotifyCommand_QueryChildNotifyCommandParam(This,piid,pChildNotifyCommand)	\
    (This)->lpVtbl -> QueryChildNotifyCommandParam(This,piid,pChildNotifyCommand)

#define INotifyCommand_QueryNotifyCommandParam(This)	\
    (This)->lpVtbl -> QueryNotifyCommandParam(This)

#define INotifyCommand_SetNotifyCommandParent(This,pNotifyCommand)	\
    (This)->lpVtbl -> SetNotifyCommandParent(This,pNotifyCommand)

#define INotifyCommand_GetNotifyCommandParent(This,ppNotifyCommand)	\
    (This)->lpVtbl -> GetNotifyCommandParent(This,ppNotifyCommand)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_NotifyCommand_Proxy( 
    INotifyCommand * This,
    UINT codeNotify,
    int nCmdID,
    ICommandItem *pCmdUI);


void __RPC_STUB INotifyCommand_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_NotifyCommandPoint_Proxy( 
    INotifyCommand * This,
    UINT nCmdID,
    POINT *pt,
    ICommandItem *pCmdUI,
    IDispatch *pPopupMenuBar);


void __RPC_STUB INotifyCommand_NotifyCommandPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_GetCommandTips_Proxy( 
    INotifyCommand * This,
    UINT nCmdID,
    BSTR *bstrTips);


void __RPC_STUB INotifyCommand_GetCommandTips_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_QueryState_Proxy( 
    INotifyCommand * This,
    UINT nCmdID,
    ICommandItem *pCmdUI);


void __RPC_STUB INotifyCommand_QueryState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_ExpansionCommand_Proxy( 
    INotifyCommand * This,
    UINT nCmdID,
    ICommandItem *pCmdUI);


void __RPC_STUB INotifyCommand_ExpansionCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_wmNotify_Proxy( 
    INotifyCommand * This,
    UINT_PTR wParam,
    LONG_PTR lParam,
    ICommandItem *pCmdUI);


void __RPC_STUB INotifyCommand_wmNotify_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_SetParam_Proxy( 
    INotifyCommand * This,
    ULONG lParam);


void __RPC_STUB INotifyCommand_SetParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_BuildMenuNc_Proxy( 
    INotifyCommand * This,
    IPopupMenuBar *pPopupMenuBar,
    LPCSTR pszPath,
    UINT uiPos,
    enumInsertMenu umInsertMenu);


void __RPC_STUB INotifyCommand_BuildMenuNc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_QueryChildNotifyCommandParam_Proxy( 
    INotifyCommand * This,
    IID *piid,
    INotifyCommand *pChildNotifyCommand);


void __RPC_STUB INotifyCommand_QueryChildNotifyCommandParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_QueryNotifyCommandParam_Proxy( 
    INotifyCommand * This);


void __RPC_STUB INotifyCommand_QueryNotifyCommandParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_SetNotifyCommandParent_Proxy( 
    INotifyCommand * This,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB INotifyCommand_SetNotifyCommandParent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommand_GetNotifyCommandParent_Proxy( 
    INotifyCommand * This,
    INotifyCommand **ppNotifyCommand);


void __RPC_STUB INotifyCommand_GetNotifyCommandParent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INotifyCommand_INTERFACE_DEFINED__ */


#ifndef __INotifyCommands_INTERFACE_DEFINED__
#define __INotifyCommands_INTERFACE_DEFINED__

/* interface INotifyCommands */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INotifyCommands;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C58B7294-43DB-463E-86B0-955F45681918")
    INotifyCommands : public INotifyCommand
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG celt,
            /* [out] */ IUnknown **rgelt,
            /* [out] */ ULONG *pceltFetched) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG celt) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Clone( 
            /* [out] */ IEnumUnknown **ppenum) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Add( 
            IID *piidKey,
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Remove( 
            IID *piidKey,
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            LONG *nCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveAll( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            IID *piidKey,
            INotifyCommand **pNotifyCommand) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INotifyCommandsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INotifyCommands * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INotifyCommands * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INotifyCommands * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyCommand )( 
            INotifyCommands * This,
            UINT codeNotify,
            int nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyCommandPoint )( 
            INotifyCommands * This,
            UINT nCmdID,
            POINT *pt,
            ICommandItem *pCmdUI,
            IDispatch *pPopupMenuBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCommandTips )( 
            INotifyCommands * This,
            UINT nCmdID,
            BSTR *bstrTips);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryState )( 
            INotifyCommands * This,
            UINT nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ExpansionCommand )( 
            INotifyCommands * This,
            UINT nCmdID,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *wmNotify )( 
            INotifyCommands * This,
            UINT_PTR wParam,
            LONG_PTR lParam,
            ICommandItem *pCmdUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetParam )( 
            INotifyCommands * This,
            ULONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BuildMenuNc )( 
            INotifyCommands * This,
            IPopupMenuBar *pPopupMenuBar,
            LPCSTR pszPath,
            UINT uiPos,
            enumInsertMenu umInsertMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryChildNotifyCommandParam )( 
            INotifyCommands * This,
            IID *piid,
            INotifyCommand *pChildNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryNotifyCommandParam )( 
            INotifyCommands * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetNotifyCommandParent )( 
            INotifyCommands * This,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetNotifyCommandParent )( 
            INotifyCommands * This,
            INotifyCommand **ppNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            INotifyCommands * This,
            /* [in] */ ULONG celt,
            /* [out] */ IUnknown **rgelt,
            /* [out] */ ULONG *pceltFetched);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Skip )( 
            INotifyCommands * This,
            /* [in] */ ULONG celt);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            INotifyCommands * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            INotifyCommands * This,
            /* [out] */ IEnumUnknown **ppenum);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            INotifyCommands * This,
            IID *piidKey,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            INotifyCommands * This,
            IID *piidKey,
            INotifyCommand *pNotifyCommand);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            INotifyCommands * This,
            LONG *nCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveAll )( 
            INotifyCommands * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            INotifyCommands * This,
            IID *piidKey,
            INotifyCommand **pNotifyCommand);
        
        END_INTERFACE
    } INotifyCommandsVtbl;

    interface INotifyCommands
    {
        CONST_VTBL struct INotifyCommandsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INotifyCommands_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define INotifyCommands_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define INotifyCommands_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define INotifyCommands_NotifyCommand(This,codeNotify,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> NotifyCommand(This,codeNotify,nCmdID,pCmdUI)

#define INotifyCommands_NotifyCommandPoint(This,nCmdID,pt,pCmdUI,pPopupMenuBar)	\
    (This)->lpVtbl -> NotifyCommandPoint(This,nCmdID,pt,pCmdUI,pPopupMenuBar)

#define INotifyCommands_GetCommandTips(This,nCmdID,bstrTips)	\
    (This)->lpVtbl -> GetCommandTips(This,nCmdID,bstrTips)

#define INotifyCommands_QueryState(This,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> QueryState(This,nCmdID,pCmdUI)

#define INotifyCommands_ExpansionCommand(This,nCmdID,pCmdUI)	\
    (This)->lpVtbl -> ExpansionCommand(This,nCmdID,pCmdUI)

#define INotifyCommands_wmNotify(This,wParam,lParam,pCmdUI)	\
    (This)->lpVtbl -> wmNotify(This,wParam,lParam,pCmdUI)

#define INotifyCommands_SetParam(This,lParam)	\
    (This)->lpVtbl -> SetParam(This,lParam)

#define INotifyCommands_BuildMenuNc(This,pPopupMenuBar,pszPath,uiPos,umInsertMenu)	\
    (This)->lpVtbl -> BuildMenuNc(This,pPopupMenuBar,pszPath,uiPos,umInsertMenu)

#define INotifyCommands_QueryChildNotifyCommandParam(This,piid,pChildNotifyCommand)	\
    (This)->lpVtbl -> QueryChildNotifyCommandParam(This,piid,pChildNotifyCommand)

#define INotifyCommands_QueryNotifyCommandParam(This)	\
    (This)->lpVtbl -> QueryNotifyCommandParam(This)

#define INotifyCommands_SetNotifyCommandParent(This,pNotifyCommand)	\
    (This)->lpVtbl -> SetNotifyCommandParent(This,pNotifyCommand)

#define INotifyCommands_GetNotifyCommandParent(This,ppNotifyCommand)	\
    (This)->lpVtbl -> GetNotifyCommandParent(This,ppNotifyCommand)


#define INotifyCommands_Next(This,celt,rgelt,pceltFetched)	\
    (This)->lpVtbl -> Next(This,celt,rgelt,pceltFetched)

#define INotifyCommands_Skip(This,celt)	\
    (This)->lpVtbl -> Skip(This,celt)

#define INotifyCommands_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define INotifyCommands_Clone(This,ppenum)	\
    (This)->lpVtbl -> Clone(This,ppenum)

#define INotifyCommands_Add(This,piidKey,pNotifyCommand)	\
    (This)->lpVtbl -> Add(This,piidKey,pNotifyCommand)

#define INotifyCommands_Remove(This,piidKey,pNotifyCommand)	\
    (This)->lpVtbl -> Remove(This,piidKey,pNotifyCommand)

#define INotifyCommands_get_Count(This,nCount)	\
    (This)->lpVtbl -> get_Count(This,nCount)

#define INotifyCommands_RemoveAll(This)	\
    (This)->lpVtbl -> RemoveAll(This)

#define INotifyCommands_GetItem(This,piidKey,pNotifyCommand)	\
    (This)->lpVtbl -> GetItem(This,piidKey,pNotifyCommand)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Next_Proxy( 
    INotifyCommands * This,
    /* [in] */ ULONG celt,
    /* [out] */ IUnknown **rgelt,
    /* [out] */ ULONG *pceltFetched);


void __RPC_STUB INotifyCommands_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Skip_Proxy( 
    INotifyCommands * This,
    /* [in] */ ULONG celt);


void __RPC_STUB INotifyCommands_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Reset_Proxy( 
    INotifyCommands * This);


void __RPC_STUB INotifyCommands_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Clone_Proxy( 
    INotifyCommands * This,
    /* [out] */ IEnumUnknown **ppenum);


void __RPC_STUB INotifyCommands_Clone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Add_Proxy( 
    INotifyCommands * This,
    IID *piidKey,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB INotifyCommands_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_Remove_Proxy( 
    INotifyCommands * This,
    IID *piidKey,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB INotifyCommands_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE INotifyCommands_get_Count_Proxy( 
    INotifyCommands * This,
    LONG *nCount);


void __RPC_STUB INotifyCommands_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_RemoveAll_Proxy( 
    INotifyCommands * This);


void __RPC_STUB INotifyCommands_RemoveAll_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyCommands_GetItem_Proxy( 
    INotifyCommands * This,
    IID *piidKey,
    INotifyCommand **pNotifyCommand);


void __RPC_STUB INotifyCommands_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INotifyCommands_INTERFACE_DEFINED__ */


#ifndef __IPopupMenuBar_INTERFACE_DEFINED__
#define __IPopupMenuBar_INTERFACE_DEFINED__

/* interface IPopupMenuBar */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IPopupMenuBar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E3EBF4B9-51E5-4462-9FE9-EFBD2251A172")
    IPopupMenuBar : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            long nIndex,
            IUnknown **pItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            COMMANDSTRUCT *lpCommandStruct,
            int iCommandCount,
            BOOL bDelSeparator) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItumCommandRange( 
            UINT iBeginCmd,
            UINT iEndCmd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ModifyMenuItem( 
            COMMANDSTRUCT *pCmdOld,
            COMMANDSTRUCT *pCmdNew) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindCommandStruct( 
            COMMANDSTRUCT *pCmdFind,
            COMMANDSTRUCT **pCmdOut) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE IsMenuBar( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreatePopupMenuBar( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TrackPopupMenu( 
            ULONG hwndCmd,
            LONG x,
            LONG y) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MenuHandle( 
            /* [retval][out] */ HMENU *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_MenuHandle( 
            /* [in] */ HMENU newVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeletePopupMenu( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommand( 
            ULONG nIndex,
            UINT fByPosition,
            COMMANDSTRUCT **lpCommandStruct,
            int iCommandCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AppendMenu( 
            ULONG *lpmodule,
            UINT uFlags,
            UINT uiNewItem,
            LPCWSTR lpszNewItem,
            ULONG *pdata) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommand( 
            /* [retval][out] */ INotifyCommand **ppNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InstallNotifyCommand( 
            int emInstallEvent_,
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            /* [in] */ VARIANT *varItem,
            /* [retval][out] */ IUnknown **pCommandItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE get_CmdItem( 
            /* [in] */ long nCmd,
            /* [retval][out] */ IUnknown **pCommandItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetCount( 
            long *lCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertSeparator( 
            int iIndex) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ModifyItemPopup( 
            int iPosition,
            UINT fByPosition,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddNotifyCommandDelete( 
            IID *piidKey,
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetPopupMenuBarItem( 
            int iPosition,
            UINT fByPosition,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadPopupMenuBar( 
            ULONG *lpmodule,
            ULONG *lpvXmlConfig,
            LPCSTR lpszXmlPath,
            INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertEnumPlus( 
            ULONG *lpmodule,
            ULONG *pEnumPlus,
            int iBeginID,
            INotifyCommand *pNotifyCommand) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPopupMenuBarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPopupMenuBar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPopupMenuBar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPopupMenuBar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IPopupMenuBar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IPopupMenuBar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IPopupMenuBar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IPopupMenuBar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            IPopupMenuBar * This,
            long nIndex,
            IUnknown **pItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            IPopupMenuBar * This,
            COMMANDSTRUCT *lpCommandStruct,
            int iCommandCount,
            BOOL bDelSeparator);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItumCommandRange )( 
            IPopupMenuBar * This,
            UINT iBeginCmd,
            UINT iEndCmd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ModifyMenuItem )( 
            IPopupMenuBar * This,
            COMMANDSTRUCT *pCmdOld,
            COMMANDSTRUCT *pCmdNew);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindCommandStruct )( 
            IPopupMenuBar * This,
            COMMANDSTRUCT *pCmdFind,
            COMMANDSTRUCT **pCmdOut);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *IsMenuBar )( 
            IPopupMenuBar * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreatePopupMenuBar )( 
            IPopupMenuBar * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TrackPopupMenu )( 
            IPopupMenuBar * This,
            ULONG hwndCmd,
            LONG x,
            LONG y);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MenuHandle )( 
            IPopupMenuBar * This,
            /* [retval][out] */ HMENU *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MenuHandle )( 
            IPopupMenuBar * This,
            /* [in] */ HMENU newVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeletePopupMenu )( 
            IPopupMenuBar * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommand )( 
            IPopupMenuBar * This,
            ULONG nIndex,
            UINT fByPosition,
            COMMANDSTRUCT **lpCommandStruct,
            int iCommandCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AppendMenu )( 
            IPopupMenuBar * This,
            ULONG *lpmodule,
            UINT uFlags,
            UINT uiNewItem,
            LPCWSTR lpszNewItem,
            ULONG *pdata);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            IPopupMenuBar * This,
            /* [retval][out] */ INotifyCommand **ppNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InstallNotifyCommand )( 
            IPopupMenuBar * This,
            int emInstallEvent_,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            IPopupMenuBar * This,
            /* [in] */ VARIANT *varItem,
            /* [retval][out] */ IUnknown **pCommandItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *get_CmdItem )( 
            IPopupMenuBar * This,
            /* [in] */ long nCmd,
            /* [retval][out] */ IUnknown **pCommandItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetCount )( 
            IPopupMenuBar * This,
            long *lCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertSeparator )( 
            IPopupMenuBar * This,
            int iIndex);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ModifyItemPopup )( 
            IPopupMenuBar * This,
            int iPosition,
            UINT fByPosition,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddNotifyCommandDelete )( 
            IPopupMenuBar * This,
            IID *piidKey,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetPopupMenuBarItem )( 
            IPopupMenuBar * This,
            int iPosition,
            UINT fByPosition,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadPopupMenuBar )( 
            IPopupMenuBar * This,
            ULONG *lpmodule,
            ULONG *lpvXmlConfig,
            LPCSTR lpszXmlPath,
            INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertEnumPlus )( 
            IPopupMenuBar * This,
            ULONG *lpmodule,
            ULONG *pEnumPlus,
            int iBeginID,
            INotifyCommand *pNotifyCommand);
        
        END_INTERFACE
    } IPopupMenuBarVtbl;

    interface IPopupMenuBar
    {
        CONST_VTBL struct IPopupMenuBarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPopupMenuBar_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPopupMenuBar_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPopupMenuBar_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPopupMenuBar_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPopupMenuBar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPopupMenuBar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPopupMenuBar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPopupMenuBar_get_Item(This,nIndex,pItem)	\
    (This)->lpVtbl -> get_Item(This,nIndex,pItem)

#define IPopupMenuBar_DeleteItem(This,lpCommandStruct,iCommandCount,bDelSeparator)	\
    (This)->lpVtbl -> DeleteItem(This,lpCommandStruct,iCommandCount,bDelSeparator)

#define IPopupMenuBar_DeleteItumCommandRange(This,iBeginCmd,iEndCmd)	\
    (This)->lpVtbl -> DeleteItumCommandRange(This,iBeginCmd,iEndCmd)

#define IPopupMenuBar_ModifyMenuItem(This,pCmdOld,pCmdNew)	\
    (This)->lpVtbl -> ModifyMenuItem(This,pCmdOld,pCmdNew)

#define IPopupMenuBar_FindCommandStruct(This,pCmdFind,pCmdOut)	\
    (This)->lpVtbl -> FindCommandStruct(This,pCmdFind,pCmdOut)

#define IPopupMenuBar_IsMenuBar(This)	\
    (This)->lpVtbl -> IsMenuBar(This)

#define IPopupMenuBar_CreatePopupMenuBar(This)	\
    (This)->lpVtbl -> CreatePopupMenuBar(This)

#define IPopupMenuBar_TrackPopupMenu(This,hwndCmd,x,y)	\
    (This)->lpVtbl -> TrackPopupMenu(This,hwndCmd,x,y)

#define IPopupMenuBar_get_MenuHandle(This,pVal)	\
    (This)->lpVtbl -> get_MenuHandle(This,pVal)

#define IPopupMenuBar_put_MenuHandle(This,newVal)	\
    (This)->lpVtbl -> put_MenuHandle(This,newVal)

#define IPopupMenuBar_DeletePopupMenu(This)	\
    (This)->lpVtbl -> DeletePopupMenu(This)

#define IPopupMenuBar_InsertCommand(This,nIndex,fByPosition,lpCommandStruct,iCommandCount)	\
    (This)->lpVtbl -> InsertCommand(This,nIndex,fByPosition,lpCommandStruct,iCommandCount)

#define IPopupMenuBar_AppendMenu(This,lpmodule,uFlags,uiNewItem,lpszNewItem,pdata)	\
    (This)->lpVtbl -> AppendMenu(This,lpmodule,uFlags,uiNewItem,lpszNewItem,pdata)

#define IPopupMenuBar_get_NotifyCommand(This,ppNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,ppNotifyCommand)

#define IPopupMenuBar_InstallNotifyCommand(This,emInstallEvent_,pNotifyCommand)	\
    (This)->lpVtbl -> InstallNotifyCommand(This,emInstallEvent_,pNotifyCommand)

#define IPopupMenuBar_GetItem(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> GetItem(This,varItem,pCommandItem)

#define IPopupMenuBar_get_CmdItem(This,nCmd,pCommandItem)	\
    (This)->lpVtbl -> get_CmdItem(This,nCmd,pCommandItem)

#define IPopupMenuBar_GetCount(This,lCount)	\
    (This)->lpVtbl -> GetCount(This,lCount)

#define IPopupMenuBar_InsertSeparator(This,iIndex)	\
    (This)->lpVtbl -> InsertSeparator(This,iIndex)

#define IPopupMenuBar_ModifyItemPopup(This,iPosition,fByPosition,ppPopupMenuBar)	\
    (This)->lpVtbl -> ModifyItemPopup(This,iPosition,fByPosition,ppPopupMenuBar)

#define IPopupMenuBar_AddNotifyCommandDelete(This,piidKey,pNotifyCommand)	\
    (This)->lpVtbl -> AddNotifyCommandDelete(This,piidKey,pNotifyCommand)

#define IPopupMenuBar_GetPopupMenuBarItem(This,iPosition,fByPosition,ppPopupMenuBar)	\
    (This)->lpVtbl -> GetPopupMenuBarItem(This,iPosition,fByPosition,ppPopupMenuBar)

#define IPopupMenuBar_ReadPopupMenuBar(This,lpmodule,lpvXmlConfig,lpszXmlPath,pNotifyCommand)	\
    (This)->lpVtbl -> ReadPopupMenuBar(This,lpmodule,lpvXmlConfig,lpszXmlPath,pNotifyCommand)

#define IPopupMenuBar_InsertEnumPlus(This,lpmodule,pEnumPlus,iBeginID,pNotifyCommand)	\
    (This)->lpVtbl -> InsertEnumPlus(This,lpmodule,pEnumPlus,iBeginID,pNotifyCommand)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_get_Item_Proxy( 
    IPopupMenuBar * This,
    long nIndex,
    IUnknown **pItem);


void __RPC_STUB IPopupMenuBar_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_DeleteItem_Proxy( 
    IPopupMenuBar * This,
    COMMANDSTRUCT *lpCommandStruct,
    int iCommandCount,
    BOOL bDelSeparator);


void __RPC_STUB IPopupMenuBar_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_DeleteItumCommandRange_Proxy( 
    IPopupMenuBar * This,
    UINT iBeginCmd,
    UINT iEndCmd);


void __RPC_STUB IPopupMenuBar_DeleteItumCommandRange_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_ModifyMenuItem_Proxy( 
    IPopupMenuBar * This,
    COMMANDSTRUCT *pCmdOld,
    COMMANDSTRUCT *pCmdNew);


void __RPC_STUB IPopupMenuBar_ModifyMenuItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_FindCommandStruct_Proxy( 
    IPopupMenuBar * This,
    COMMANDSTRUCT *pCmdFind,
    COMMANDSTRUCT **pCmdOut);


void __RPC_STUB IPopupMenuBar_FindCommandStruct_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_IsMenuBar_Proxy( 
    IPopupMenuBar * This);


void __RPC_STUB IPopupMenuBar_IsMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_CreatePopupMenuBar_Proxy( 
    IPopupMenuBar * This);


void __RPC_STUB IPopupMenuBar_CreatePopupMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_TrackPopupMenu_Proxy( 
    IPopupMenuBar * This,
    ULONG hwndCmd,
    LONG x,
    LONG y);


void __RPC_STUB IPopupMenuBar_TrackPopupMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_get_MenuHandle_Proxy( 
    IPopupMenuBar * This,
    /* [retval][out] */ HMENU *pVal);


void __RPC_STUB IPopupMenuBar_get_MenuHandle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_put_MenuHandle_Proxy( 
    IPopupMenuBar * This,
    /* [in] */ HMENU newVal);


void __RPC_STUB IPopupMenuBar_put_MenuHandle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_DeletePopupMenu_Proxy( 
    IPopupMenuBar * This);


void __RPC_STUB IPopupMenuBar_DeletePopupMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_InsertCommand_Proxy( 
    IPopupMenuBar * This,
    ULONG nIndex,
    UINT fByPosition,
    COMMANDSTRUCT **lpCommandStruct,
    int iCommandCount);


void __RPC_STUB IPopupMenuBar_InsertCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_AppendMenu_Proxy( 
    IPopupMenuBar * This,
    ULONG *lpmodule,
    UINT uFlags,
    UINT uiNewItem,
    LPCWSTR lpszNewItem,
    ULONG *pdata);


void __RPC_STUB IPopupMenuBar_AppendMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_get_NotifyCommand_Proxy( 
    IPopupMenuBar * This,
    /* [retval][out] */ INotifyCommand **ppNotifyCommand);


void __RPC_STUB IPopupMenuBar_get_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_InstallNotifyCommand_Proxy( 
    IPopupMenuBar * This,
    int emInstallEvent_,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB IPopupMenuBar_InstallNotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_GetItem_Proxy( 
    IPopupMenuBar * This,
    /* [in] */ VARIANT *varItem,
    /* [retval][out] */ IUnknown **pCommandItem);


void __RPC_STUB IPopupMenuBar_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_get_CmdItem_Proxy( 
    IPopupMenuBar * This,
    /* [in] */ long nCmd,
    /* [retval][out] */ IUnknown **pCommandItem);


void __RPC_STUB IPopupMenuBar_get_CmdItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_GetCount_Proxy( 
    IPopupMenuBar * This,
    long *lCount);


void __RPC_STUB IPopupMenuBar_GetCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_InsertSeparator_Proxy( 
    IPopupMenuBar * This,
    int iIndex);


void __RPC_STUB IPopupMenuBar_InsertSeparator_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_ModifyItemPopup_Proxy( 
    IPopupMenuBar * This,
    int iPosition,
    UINT fByPosition,
    /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar);


void __RPC_STUB IPopupMenuBar_ModifyItemPopup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_AddNotifyCommandDelete_Proxy( 
    IPopupMenuBar * This,
    IID *piidKey,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB IPopupMenuBar_AddNotifyCommandDelete_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_GetPopupMenuBarItem_Proxy( 
    IPopupMenuBar * This,
    int iPosition,
    UINT fByPosition,
    /* [retval][out] */ IPopupMenuBar **ppPopupMenuBar);


void __RPC_STUB IPopupMenuBar_GetPopupMenuBarItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_ReadPopupMenuBar_Proxy( 
    IPopupMenuBar * This,
    ULONG *lpmodule,
    ULONG *lpvXmlConfig,
    LPCSTR lpszXmlPath,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB IPopupMenuBar_ReadPopupMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPopupMenuBar_InsertEnumPlus_Proxy( 
    IPopupMenuBar * This,
    ULONG *lpmodule,
    ULONG *pEnumPlus,
    int iBeginID,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB IPopupMenuBar_InsertEnumPlus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPopupMenuBar_INTERFACE_DEFINED__ */


#ifndef __ICommandBar_INTERFACE_DEFINED__
#define __ICommandBar_INTERFACE_DEFINED__

/* interface ICommandBar */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandBar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B62E5FE5-6EAB-44B3-88DD-97A2AA01045E")
    ICommandBar : public IDispatch
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandItem **pCommandItem) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR Name) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Visible( 
            /* [retval][out] */ VARIANT_BOOL *pVisible) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Visible( 
            /* [in] */ VARIANT_BOOL bVisible) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Enable( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Enable( 
            /* [in] */ VARIANT_BOOL newEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommand( 
            LONG nIndex,
            UINT uFlags,
            int nNumButton,
            LONG *lpCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetImageList( 
            /* [in] */ LONG_PTR hInstanceRes,
            int nButtonWidth,
            int nButtonHeight,
            VARIANT *pVariant) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetBuildUI( 
            long *pBuildUI) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItemCmd( 
            LONG nCmdID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ VARIANT *varItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteAllItem( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ParentCommandBars( 
            /* [retval][out] */ ICommandBars **ppCommandBars) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_CmdItem( 
            long nCmd,
            /* [retval][out] */ ICommandItem **pCommandItem) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommand( 
            /* [retval][out] */ INotifyCommand **pNotifyCommand) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_NotifyCommand( 
            /* [in] */ INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetAllBuutonWidt( 
            /* [retval][out] */ LONG *lnWidt) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetItemBitmap( 
            int iIndex,
            LONG lhBitmap) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetItemBitmapFullName( 
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstBitmap) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetItemIcon( 
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstIcon) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TotalButtonWidth( 
            int *piWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertSeparator( 
            int iIndex) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBarWnd( 
            /* [retval][out] */ LONG *hWndBar) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandBarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandBar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandBar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandBar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandBar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandBar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandBar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandBar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ICommandBar * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            ICommandBar * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            ICommandBar * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            ICommandBar * This,
            /* [retval][out] */ BSTR *pName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            ICommandBar * This,
            /* [in] */ BSTR Name);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            ICommandBar * This,
            /* [retval][out] */ VARIANT_BOOL *pVisible);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            ICommandBar * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Enable )( 
            ICommandBar * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Enable )( 
            ICommandBar * This,
            /* [in] */ VARIANT_BOOL newEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommand )( 
            ICommandBar * This,
            LONG nIndex,
            UINT uFlags,
            int nNumButton,
            LONG *lpCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetImageList )( 
            ICommandBar * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int nButtonWidth,
            int nButtonHeight,
            VARIANT *pVariant);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetBuildUI )( 
            ICommandBar * This,
            long *pBuildUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItemCmd )( 
            ICommandBar * This,
            LONG nCmdID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            ICommandBar * This,
            /* [in] */ VARIANT *varItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteAllItem )( 
            ICommandBar * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ParentCommandBars )( 
            ICommandBar * This,
            /* [retval][out] */ ICommandBars **ppCommandBars);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CmdItem )( 
            ICommandBar * This,
            long nCmd,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            ICommandBar * This,
            /* [retval][out] */ INotifyCommand **pNotifyCommand);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyCommand )( 
            ICommandBar * This,
            /* [in] */ INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetAllBuutonWidt )( 
            ICommandBar * This,
            /* [retval][out] */ LONG *lnWidt);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemBitmap )( 
            ICommandBar * This,
            int iIndex,
            LONG lhBitmap);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemBitmapFullName )( 
            ICommandBar * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstBitmap);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemIcon )( 
            ICommandBar * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstIcon);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TotalButtonWidth )( 
            ICommandBar * This,
            int *piWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertSeparator )( 
            ICommandBar * This,
            int iIndex);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBarWnd )( 
            ICommandBar * This,
            /* [retval][out] */ LONG *hWndBar);
        
        END_INTERFACE
    } ICommandBarVtbl;

    interface ICommandBar
    {
        CONST_VTBL struct ICommandBarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandBar_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICommandBar_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICommandBar_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICommandBar_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICommandBar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICommandBar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICommandBar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICommandBar_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define ICommandBar_get_Item(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,pCommandItem)

#define ICommandBar_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define ICommandBar_get_Name(This,pName)	\
    (This)->lpVtbl -> get_Name(This,pName)

#define ICommandBar_put_Name(This,Name)	\
    (This)->lpVtbl -> put_Name(This,Name)

#define ICommandBar_get_Visible(This,pVisible)	\
    (This)->lpVtbl -> get_Visible(This,pVisible)

#define ICommandBar_put_Visible(This,bVisible)	\
    (This)->lpVtbl -> put_Visible(This,bVisible)

#define ICommandBar_get_Enable(This,pEnable)	\
    (This)->lpVtbl -> get_Enable(This,pEnable)

#define ICommandBar_put_Enable(This,newEnable)	\
    (This)->lpVtbl -> put_Enable(This,newEnable)

#define ICommandBar_InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)	\
    (This)->lpVtbl -> InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)

#define ICommandBar_SetImageList(This,hInstanceRes,nButtonWidth,nButtonHeight,pVariant)	\
    (This)->lpVtbl -> SetImageList(This,hInstanceRes,nButtonWidth,nButtonHeight,pVariant)

#define ICommandBar_SetBuildUI(This,pBuildUI)	\
    (This)->lpVtbl -> SetBuildUI(This,pBuildUI)

#define ICommandBar_DeleteItemCmd(This,nCmdID)	\
    (This)->lpVtbl -> DeleteItemCmd(This,nCmdID)

#define ICommandBar_DeleteItem(This,varItem)	\
    (This)->lpVtbl -> DeleteItem(This,varItem)

#define ICommandBar_DeleteAllItem(This)	\
    (This)->lpVtbl -> DeleteAllItem(This)

#define ICommandBar_get_ParentCommandBars(This,ppCommandBars)	\
    (This)->lpVtbl -> get_ParentCommandBars(This,ppCommandBars)

#define ICommandBar_get_CmdItem(This,nCmd,pCommandItem)	\
    (This)->lpVtbl -> get_CmdItem(This,nCmd,pCommandItem)

#define ICommandBar_get_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,pNotifyCommand)

#define ICommandBar_put_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> put_NotifyCommand(This,pNotifyCommand)

#define ICommandBar_GetAllBuutonWidt(This,lnWidt)	\
    (This)->lpVtbl -> GetAllBuutonWidt(This,lnWidt)

#define ICommandBar_SetItemBitmap(This,iIndex,lhBitmap)	\
    (This)->lpVtbl -> SetItemBitmap(This,iIndex,lhBitmap)

#define ICommandBar_SetItemBitmapFullName(This,hInstanceRes,iIndex,bstBitmap)	\
    (This)->lpVtbl -> SetItemBitmapFullName(This,hInstanceRes,iIndex,bstBitmap)

#define ICommandBar_SetItemIcon(This,hInstanceRes,iIndex,bstIcon)	\
    (This)->lpVtbl -> SetItemIcon(This,hInstanceRes,iIndex,bstIcon)

#define ICommandBar_TotalButtonWidth(This,piWidth)	\
    (This)->lpVtbl -> TotalButtonWidth(This,piWidth)

#define ICommandBar_InsertSeparator(This,iIndex)	\
    (This)->lpVtbl -> InsertSeparator(This,iIndex)

#define ICommandBar_get_CommandBarWnd(This,hWndBar)	\
    (This)->lpVtbl -> get_CommandBarWnd(This,hWndBar)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_Count_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB ICommandBar_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_Item_Proxy( 
    ICommandBar * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ ICommandItem **pCommandItem);


void __RPC_STUB ICommandBar_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBar_get__NewEnum_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB ICommandBar_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_Name_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ BSTR *pName);


void __RPC_STUB ICommandBar_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBar_put_Name_Proxy( 
    ICommandBar * This,
    /* [in] */ BSTR Name);


void __RPC_STUB ICommandBar_put_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_Visible_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ VARIANT_BOOL *pVisible);


void __RPC_STUB ICommandBar_get_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBar_put_Visible_Proxy( 
    ICommandBar * This,
    /* [in] */ VARIANT_BOOL bVisible);


void __RPC_STUB ICommandBar_put_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_Enable_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ VARIANT_BOOL *pEnable);


void __RPC_STUB ICommandBar_get_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBar_put_Enable_Proxy( 
    ICommandBar * This,
    /* [in] */ VARIANT_BOOL newEnable);


void __RPC_STUB ICommandBar_put_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_InsertCommand_Proxy( 
    ICommandBar * This,
    LONG nIndex,
    UINT uFlags,
    int nNumButton,
    LONG *lpCommand);


void __RPC_STUB ICommandBar_InsertCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_SetImageList_Proxy( 
    ICommandBar * This,
    /* [in] */ LONG_PTR hInstanceRes,
    int nButtonWidth,
    int nButtonHeight,
    VARIANT *pVariant);


void __RPC_STUB ICommandBar_SetImageList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_SetBuildUI_Proxy( 
    ICommandBar * This,
    long *pBuildUI);


void __RPC_STUB ICommandBar_SetBuildUI_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_DeleteItemCmd_Proxy( 
    ICommandBar * This,
    LONG nCmdID);


void __RPC_STUB ICommandBar_DeleteItemCmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_DeleteItem_Proxy( 
    ICommandBar * This,
    /* [in] */ VARIANT *varItem);


void __RPC_STUB ICommandBar_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_DeleteAllItem_Proxy( 
    ICommandBar * This);


void __RPC_STUB ICommandBar_DeleteAllItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_ParentCommandBars_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ ICommandBars **ppCommandBars);


void __RPC_STUB ICommandBar_get_ParentCommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_CmdItem_Proxy( 
    ICommandBar * This,
    long nCmd,
    /* [retval][out] */ ICommandItem **pCommandItem);


void __RPC_STUB ICommandBar_get_CmdItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_NotifyCommand_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ INotifyCommand **pNotifyCommand);


void __RPC_STUB ICommandBar_get_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBar_put_NotifyCommand_Proxy( 
    ICommandBar * This,
    /* [in] */ INotifyCommand *pNotifyCommand);


void __RPC_STUB ICommandBar_put_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_GetAllBuutonWidt_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ LONG *lnWidt);


void __RPC_STUB ICommandBar_GetAllBuutonWidt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_SetItemBitmap_Proxy( 
    ICommandBar * This,
    int iIndex,
    LONG lhBitmap);


void __RPC_STUB ICommandBar_SetItemBitmap_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_SetItemBitmapFullName_Proxy( 
    ICommandBar * This,
    /* [in] */ LONG_PTR hInstanceRes,
    int iIndex,
    BSTR bstBitmap);


void __RPC_STUB ICommandBar_SetItemBitmapFullName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_SetItemIcon_Proxy( 
    ICommandBar * This,
    /* [in] */ LONG_PTR hInstanceRes,
    int iIndex,
    BSTR bstIcon);


void __RPC_STUB ICommandBar_SetItemIcon_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_TotalButtonWidth_Proxy( 
    ICommandBar * This,
    int *piWidth);


void __RPC_STUB ICommandBar_TotalButtonWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBar_InsertSeparator_Proxy( 
    ICommandBar * This,
    int iIndex);


void __RPC_STUB ICommandBar_InsertSeparator_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBar_get_CommandBarWnd_Proxy( 
    ICommandBar * This,
    /* [retval][out] */ LONG *hWndBar);


void __RPC_STUB ICommandBar_get_CommandBarWnd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICommandBar_INTERFACE_DEFINED__ */


#ifndef __ICommandBars_INTERFACE_DEFINED__
#define __ICommandBars_INTERFACE_DEFINED__

/* interface ICommandBars */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandBars;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B4B48A5-075B-48C8-83A1-D947D2DE385D")
    ICommandBars : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommandBar( 
            BSTR Name,
            long nButtonWidth,
            long nButtonHeight,
            LONG Position,
            IID *piidKey,
            INotifyCommand *pNotifyCommand,
            /* [retval][out] */ ICommandBar **ppCommandBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveCommandBar( 
            /* [in] */ VARIANT varCommandBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryStateItems( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindIIDKey( 
            IID *piidKey,
            /* [retval][out] */ ICommandBar **ppCommandBar) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowMenuBar( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowMenuBar( 
            /* [in] */ VARIANT_BOOL newVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MenuBar( 
            /* [retval][out] */ IMenuBar **ppMenuBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetClientRect( 
            /* [retval][out] */ RECT *lpRect) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateCommandBars( 
            LONG_PTR hWndParent,
            BSTR strTitle,
            enumCommandBarsType cbt,
            IID *piidKey) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateMenuBar( 
            HMENU hMenu,
            /* [out] */ IMenuBar **pMenuBar,
            IID *piidKey) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandBar **pCommandItem) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_NotifyCommand( 
            /* [in] */ INotifyCommand *pNotifyCommand) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_NotifyCommand( 
            /* [retval][out] */ INotifyCommand **pNotifyCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertResourceCommandBar( 
            BSTR Name,
            LONG nResID,
            LONG_PTR hRes,
            LONG Position,
            IID *piidKey,
            /* [retval][out] */ ICommandBar **ppCommandBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertWindowCommandBar( 
            BSTR bstrName,
            LONG lHeight,
            LONG Position,
            /* [retval][out] */ ICommandBar **ppCommandBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommandBarXml( 
            long *pModule,
            long *hXmlConfig,
            unsigned char *szPath,
            int iLen,
            INotifyCommand *pNotifyCommand,
            ICommandBar **pCommandBar) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommandBarsXml( 
            long *pmodule,
            long *hxmlConfig,
            unsigned char *szPath,
            INotifyCommand *pNotifyCommand) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandBarsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandBars * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandBars * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandBars * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandBars * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandBars * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandBars * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandBars * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommandBar )( 
            ICommandBars * This,
            BSTR Name,
            long nButtonWidth,
            long nButtonHeight,
            LONG Position,
            IID *piidKey,
            INotifyCommand *pNotifyCommand,
            /* [retval][out] */ ICommandBar **ppCommandBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveCommandBar )( 
            ICommandBars * This,
            /* [in] */ VARIANT varCommandBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryStateItems )( 
            ICommandBars * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindIIDKey )( 
            ICommandBars * This,
            IID *piidKey,
            /* [retval][out] */ ICommandBar **ppCommandBar);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowMenuBar )( 
            ICommandBars * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowMenuBar )( 
            ICommandBars * This,
            /* [in] */ VARIANT_BOOL newVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MenuBar )( 
            ICommandBars * This,
            /* [retval][out] */ IMenuBar **ppMenuBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetClientRect )( 
            ICommandBars * This,
            /* [retval][out] */ RECT *lpRect);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateCommandBars )( 
            ICommandBars * This,
            LONG_PTR hWndParent,
            BSTR strTitle,
            enumCommandBarsType cbt,
            IID *piidKey);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateMenuBar )( 
            ICommandBars * This,
            HMENU hMenu,
            /* [out] */ IMenuBar **pMenuBar,
            IID *piidKey);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ICommandBars * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            ICommandBars * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandBar **pCommandItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            ICommandBars * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyCommand )( 
            ICommandBars * This,
            /* [in] */ INotifyCommand *pNotifyCommand);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            ICommandBars * This,
            /* [retval][out] */ INotifyCommand **pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertResourceCommandBar )( 
            ICommandBars * This,
            BSTR Name,
            LONG nResID,
            LONG_PTR hRes,
            LONG Position,
            IID *piidKey,
            /* [retval][out] */ ICommandBar **ppCommandBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertWindowCommandBar )( 
            ICommandBars * This,
            BSTR bstrName,
            LONG lHeight,
            LONG Position,
            /* [retval][out] */ ICommandBar **ppCommandBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommandBarXml )( 
            ICommandBars * This,
            long *pModule,
            long *hXmlConfig,
            unsigned char *szPath,
            int iLen,
            INotifyCommand *pNotifyCommand,
            ICommandBar **pCommandBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommandBarsXml )( 
            ICommandBars * This,
            long *pmodule,
            long *hxmlConfig,
            unsigned char *szPath,
            INotifyCommand *pNotifyCommand);
        
        END_INTERFACE
    } ICommandBarsVtbl;

    interface ICommandBars
    {
        CONST_VTBL struct ICommandBarsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandBars_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICommandBars_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICommandBars_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICommandBars_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICommandBars_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICommandBars_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICommandBars_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICommandBars_InsertCommandBar(This,Name,nButtonWidth,nButtonHeight,Position,piidKey,pNotifyCommand,ppCommandBar)	\
    (This)->lpVtbl -> InsertCommandBar(This,Name,nButtonWidth,nButtonHeight,Position,piidKey,pNotifyCommand,ppCommandBar)

#define ICommandBars_RemoveCommandBar(This,varCommandBar)	\
    (This)->lpVtbl -> RemoveCommandBar(This,varCommandBar)

#define ICommandBars_QueryStateItems(This)	\
    (This)->lpVtbl -> QueryStateItems(This)

#define ICommandBars_FindIIDKey(This,piidKey,ppCommandBar)	\
    (This)->lpVtbl -> FindIIDKey(This,piidKey,ppCommandBar)

#define ICommandBars_get_ShowMenuBar(This,pVal)	\
    (This)->lpVtbl -> get_ShowMenuBar(This,pVal)

#define ICommandBars_put_ShowMenuBar(This,newVal)	\
    (This)->lpVtbl -> put_ShowMenuBar(This,newVal)

#define ICommandBars_get_MenuBar(This,ppMenuBar)	\
    (This)->lpVtbl -> get_MenuBar(This,ppMenuBar)

#define ICommandBars_GetClientRect(This,lpRect)	\
    (This)->lpVtbl -> GetClientRect(This,lpRect)

#define ICommandBars_CreateCommandBars(This,hWndParent,strTitle,cbt,piidKey)	\
    (This)->lpVtbl -> CreateCommandBars(This,hWndParent,strTitle,cbt,piidKey)

#define ICommandBars_CreateMenuBar(This,hMenu,pMenuBar,piidKey)	\
    (This)->lpVtbl -> CreateMenuBar(This,hMenu,pMenuBar,piidKey)

#define ICommandBars_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define ICommandBars_get_Item(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,pCommandItem)

#define ICommandBars_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define ICommandBars_put_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> put_NotifyCommand(This,pNotifyCommand)

#define ICommandBars_get_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,pNotifyCommand)

#define ICommandBars_InsertResourceCommandBar(This,Name,nResID,hRes,Position,piidKey,ppCommandBar)	\
    (This)->lpVtbl -> InsertResourceCommandBar(This,Name,nResID,hRes,Position,piidKey,ppCommandBar)

#define ICommandBars_InsertWindowCommandBar(This,bstrName,lHeight,Position,ppCommandBar)	\
    (This)->lpVtbl -> InsertWindowCommandBar(This,bstrName,lHeight,Position,ppCommandBar)

#define ICommandBars_InsertCommandBarXml(This,pModule,hXmlConfig,szPath,iLen,pNotifyCommand,pCommandBar)	\
    (This)->lpVtbl -> InsertCommandBarXml(This,pModule,hXmlConfig,szPath,iLen,pNotifyCommand,pCommandBar)

#define ICommandBars_InsertCommandBarsXml(This,pmodule,hxmlConfig,szPath,pNotifyCommand)	\
    (This)->lpVtbl -> InsertCommandBarsXml(This,pmodule,hxmlConfig,szPath,pNotifyCommand)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_InsertCommandBar_Proxy( 
    ICommandBars * This,
    BSTR Name,
    long nButtonWidth,
    long nButtonHeight,
    LONG Position,
    IID *piidKey,
    INotifyCommand *pNotifyCommand,
    /* [retval][out] */ ICommandBar **ppCommandBar);


void __RPC_STUB ICommandBars_InsertCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_RemoveCommandBar_Proxy( 
    ICommandBars * This,
    /* [in] */ VARIANT varCommandBar);


void __RPC_STUB ICommandBars_RemoveCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_QueryStateItems_Proxy( 
    ICommandBars * This);


void __RPC_STUB ICommandBars_QueryStateItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_FindIIDKey_Proxy( 
    ICommandBars * This,
    IID *piidKey,
    /* [retval][out] */ ICommandBar **ppCommandBar);


void __RPC_STUB ICommandBars_FindIIDKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBars_get_ShowMenuBar_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ICommandBars_get_ShowMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBars_put_ShowMenuBar_Proxy( 
    ICommandBars * This,
    /* [in] */ VARIANT_BOOL newVal);


void __RPC_STUB ICommandBars_put_ShowMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBars_get_MenuBar_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ IMenuBar **ppMenuBar);


void __RPC_STUB ICommandBars_get_MenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_GetClientRect_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ RECT *lpRect);


void __RPC_STUB ICommandBars_GetClientRect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_CreateCommandBars_Proxy( 
    ICommandBars * This,
    LONG_PTR hWndParent,
    BSTR strTitle,
    enumCommandBarsType cbt,
    IID *piidKey);


void __RPC_STUB ICommandBars_CreateCommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_CreateMenuBar_Proxy( 
    ICommandBars * This,
    HMENU hMenu,
    /* [out] */ IMenuBar **pMenuBar,
    IID *piidKey);


void __RPC_STUB ICommandBars_CreateMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBars_get_Count_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB ICommandBars_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBars_get_Item_Proxy( 
    ICommandBars * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ ICommandBar **pCommandItem);


void __RPC_STUB ICommandBars_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBars_get__NewEnum_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB ICommandBars_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandBars_put_NotifyCommand_Proxy( 
    ICommandBars * This,
    /* [in] */ INotifyCommand *pNotifyCommand);


void __RPC_STUB ICommandBars_put_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandBars_get_NotifyCommand_Proxy( 
    ICommandBars * This,
    /* [retval][out] */ INotifyCommand **pNotifyCommand);


void __RPC_STUB ICommandBars_get_NotifyCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_InsertResourceCommandBar_Proxy( 
    ICommandBars * This,
    BSTR Name,
    LONG nResID,
    LONG_PTR hRes,
    LONG Position,
    IID *piidKey,
    /* [retval][out] */ ICommandBar **ppCommandBar);


void __RPC_STUB ICommandBars_InsertResourceCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_InsertWindowCommandBar_Proxy( 
    ICommandBars * This,
    BSTR bstrName,
    LONG lHeight,
    LONG Position,
    /* [retval][out] */ ICommandBar **ppCommandBar);


void __RPC_STUB ICommandBars_InsertWindowCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_InsertCommandBarXml_Proxy( 
    ICommandBars * This,
    long *pModule,
    long *hXmlConfig,
    unsigned char *szPath,
    int iLen,
    INotifyCommand *pNotifyCommand,
    ICommandBar **pCommandBar);


void __RPC_STUB ICommandBars_InsertCommandBarXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBars_InsertCommandBarsXml_Proxy( 
    ICommandBars * This,
    long *pmodule,
    long *hxmlConfig,
    unsigned char *szPath,
    INotifyCommand *pNotifyCommand);


void __RPC_STUB ICommandBars_InsertCommandBarsXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICommandBars_INTERFACE_DEFINED__ */


#ifndef __ICommandGroup_INTERFACE_DEFINED__
#define __ICommandGroup_INTERFACE_DEFINED__

/* interface ICommandGroup */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandGroup;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6A589200-396C-4A48-AE1E-C2994A118F66")
    ICommandGroup : public ICommandBar
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE PopupMenu( 
            int nLeft,
            int nTop,
            ULONG uFlags,
            LONG_PTR *nReturnCMD,
            LONG_PTR hWndCommandWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE clear( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ShowCount( 
            /* [retval][out] */ LONG *nShowCount) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ShowCount( 
            /* [in] */ ULONG nShowCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_TrackSelect( 
            /* [retval][out] */ VARIANT_BOOL *bTrackSelect) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_TrackSelect( 
            /* [in] */ VARIANT_BOOL bTrackSelect) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandGroupVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandGroup * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandGroup * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandGroup * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandGroup * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandGroup * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandGroup * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandGroup * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ICommandGroup * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            ICommandGroup * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            ICommandGroup * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            ICommandGroup * This,
            /* [retval][out] */ BSTR *pName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            ICommandGroup * This,
            /* [in] */ BSTR Name);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            ICommandGroup * This,
            /* [retval][out] */ VARIANT_BOOL *pVisible);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            ICommandGroup * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Enable )( 
            ICommandGroup * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Enable )( 
            ICommandGroup * This,
            /* [in] */ VARIANT_BOOL newEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommand )( 
            ICommandGroup * This,
            LONG nIndex,
            UINT uFlags,
            int nNumButton,
            LONG *lpCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetImageList )( 
            ICommandGroup * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int nButtonWidth,
            int nButtonHeight,
            VARIANT *pVariant);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetBuildUI )( 
            ICommandGroup * This,
            long *pBuildUI);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItemCmd )( 
            ICommandGroup * This,
            LONG nCmdID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            ICommandGroup * This,
            /* [in] */ VARIANT *varItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteAllItem )( 
            ICommandGroup * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ParentCommandBars )( 
            ICommandGroup * This,
            /* [retval][out] */ ICommandBars **ppCommandBars);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_CmdItem )( 
            ICommandGroup * This,
            long nCmd,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_NotifyCommand )( 
            ICommandGroup * This,
            /* [retval][out] */ INotifyCommand **pNotifyCommand);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_NotifyCommand )( 
            ICommandGroup * This,
            /* [in] */ INotifyCommand *pNotifyCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetAllBuutonWidt )( 
            ICommandGroup * This,
            /* [retval][out] */ LONG *lnWidt);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemBitmap )( 
            ICommandGroup * This,
            int iIndex,
            LONG lhBitmap);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemBitmapFullName )( 
            ICommandGroup * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstBitmap);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetItemIcon )( 
            ICommandGroup * This,
            /* [in] */ LONG_PTR hInstanceRes,
            int iIndex,
            BSTR bstIcon);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TotalButtonWidth )( 
            ICommandGroup * This,
            int *piWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertSeparator )( 
            ICommandGroup * This,
            int iIndex);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBarWnd )( 
            ICommandGroup * This,
            /* [retval][out] */ LONG *hWndBar);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *PopupMenu )( 
            ICommandGroup * This,
            int nLeft,
            int nTop,
            ULONG uFlags,
            LONG_PTR *nReturnCMD,
            LONG_PTR hWndCommandWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *clear )( 
            ICommandGroup * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ShowCount )( 
            ICommandGroup * This,
            /* [retval][out] */ LONG *nShowCount);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ShowCount )( 
            ICommandGroup * This,
            /* [in] */ ULONG nShowCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_TrackSelect )( 
            ICommandGroup * This,
            /* [retval][out] */ VARIANT_BOOL *bTrackSelect);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_TrackSelect )( 
            ICommandGroup * This,
            /* [in] */ VARIANT_BOOL bTrackSelect);
        
        END_INTERFACE
    } ICommandGroupVtbl;

    interface ICommandGroup
    {
        CONST_VTBL struct ICommandGroupVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandGroup_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICommandGroup_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICommandGroup_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICommandGroup_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICommandGroup_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICommandGroup_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICommandGroup_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICommandGroup_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define ICommandGroup_get_Item(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> get_Item(This,varItem,pCommandItem)

#define ICommandGroup_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define ICommandGroup_get_Name(This,pName)	\
    (This)->lpVtbl -> get_Name(This,pName)

#define ICommandGroup_put_Name(This,Name)	\
    (This)->lpVtbl -> put_Name(This,Name)

#define ICommandGroup_get_Visible(This,pVisible)	\
    (This)->lpVtbl -> get_Visible(This,pVisible)

#define ICommandGroup_put_Visible(This,bVisible)	\
    (This)->lpVtbl -> put_Visible(This,bVisible)

#define ICommandGroup_get_Enable(This,pEnable)	\
    (This)->lpVtbl -> get_Enable(This,pEnable)

#define ICommandGroup_put_Enable(This,newEnable)	\
    (This)->lpVtbl -> put_Enable(This,newEnable)

#define ICommandGroup_InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)	\
    (This)->lpVtbl -> InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)

#define ICommandGroup_SetImageList(This,hInstanceRes,nButtonWidth,nButtonHeight,pVariant)	\
    (This)->lpVtbl -> SetImageList(This,hInstanceRes,nButtonWidth,nButtonHeight,pVariant)

#define ICommandGroup_SetBuildUI(This,pBuildUI)	\
    (This)->lpVtbl -> SetBuildUI(This,pBuildUI)

#define ICommandGroup_DeleteItemCmd(This,nCmdID)	\
    (This)->lpVtbl -> DeleteItemCmd(This,nCmdID)

#define ICommandGroup_DeleteItem(This,varItem)	\
    (This)->lpVtbl -> DeleteItem(This,varItem)

#define ICommandGroup_DeleteAllItem(This)	\
    (This)->lpVtbl -> DeleteAllItem(This)

#define ICommandGroup_get_ParentCommandBars(This,ppCommandBars)	\
    (This)->lpVtbl -> get_ParentCommandBars(This,ppCommandBars)

#define ICommandGroup_get_CmdItem(This,nCmd,pCommandItem)	\
    (This)->lpVtbl -> get_CmdItem(This,nCmd,pCommandItem)

#define ICommandGroup_get_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> get_NotifyCommand(This,pNotifyCommand)

#define ICommandGroup_put_NotifyCommand(This,pNotifyCommand)	\
    (This)->lpVtbl -> put_NotifyCommand(This,pNotifyCommand)

#define ICommandGroup_GetAllBuutonWidt(This,lnWidt)	\
    (This)->lpVtbl -> GetAllBuutonWidt(This,lnWidt)

#define ICommandGroup_SetItemBitmap(This,iIndex,lhBitmap)	\
    (This)->lpVtbl -> SetItemBitmap(This,iIndex,lhBitmap)

#define ICommandGroup_SetItemBitmapFullName(This,hInstanceRes,iIndex,bstBitmap)	\
    (This)->lpVtbl -> SetItemBitmapFullName(This,hInstanceRes,iIndex,bstBitmap)

#define ICommandGroup_SetItemIcon(This,hInstanceRes,iIndex,bstIcon)	\
    (This)->lpVtbl -> SetItemIcon(This,hInstanceRes,iIndex,bstIcon)

#define ICommandGroup_TotalButtonWidth(This,piWidth)	\
    (This)->lpVtbl -> TotalButtonWidth(This,piWidth)

#define ICommandGroup_InsertSeparator(This,iIndex)	\
    (This)->lpVtbl -> InsertSeparator(This,iIndex)

#define ICommandGroup_get_CommandBarWnd(This,hWndBar)	\
    (This)->lpVtbl -> get_CommandBarWnd(This,hWndBar)


#define ICommandGroup_PopupMenu(This,nLeft,nTop,uFlags,nReturnCMD,hWndCommandWnd)	\
    (This)->lpVtbl -> PopupMenu(This,nLeft,nTop,uFlags,nReturnCMD,hWndCommandWnd)

#define ICommandGroup_clear(This)	\
    (This)->lpVtbl -> clear(This)

#define ICommandGroup_get_ShowCount(This,nShowCount)	\
    (This)->lpVtbl -> get_ShowCount(This,nShowCount)

#define ICommandGroup_put_ShowCount(This,nShowCount)	\
    (This)->lpVtbl -> put_ShowCount(This,nShowCount)

#define ICommandGroup_get_TrackSelect(This,bTrackSelect)	\
    (This)->lpVtbl -> get_TrackSelect(This,bTrackSelect)

#define ICommandGroup_put_TrackSelect(This,bTrackSelect)	\
    (This)->lpVtbl -> put_TrackSelect(This,bTrackSelect)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ICommandGroup_PopupMenu_Proxy( 
    ICommandGroup * This,
    int nLeft,
    int nTop,
    ULONG uFlags,
    LONG_PTR *nReturnCMD,
    LONG_PTR hWndCommandWnd);


void __RPC_STUB ICommandGroup_PopupMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandGroup_clear_Proxy( 
    ICommandGroup * This);


void __RPC_STUB ICommandGroup_clear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandGroup_get_ShowCount_Proxy( 
    ICommandGroup * This,
    /* [retval][out] */ LONG *nShowCount);


void __RPC_STUB ICommandGroup_get_ShowCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandGroup_put_ShowCount_Proxy( 
    ICommandGroup * This,
    /* [in] */ ULONG nShowCount);


void __RPC_STUB ICommandGroup_put_ShowCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ICommandGroup_get_TrackSelect_Proxy( 
    ICommandGroup * This,
    /* [retval][out] */ VARIANT_BOOL *bTrackSelect);


void __RPC_STUB ICommandGroup_get_TrackSelect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ICommandGroup_put_TrackSelect_Proxy( 
    ICommandGroup * This,
    /* [in] */ VARIANT_BOOL bTrackSelect);


void __RPC_STUB ICommandGroup_put_TrackSelect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICommandGroup_INTERFACE_DEFINED__ */


#ifndef __ICommandBarsList_INTERFACE_DEFINED__
#define __ICommandBarsList_INTERFACE_DEFINED__

/* interface ICommandBarsList */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICommandBarsList;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2A79807E-77CC-496E-A448-37D409EDA49E")
    ICommandBarsList : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE QueryStateItems( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LockQuery( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FreeQuery( void) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandBars **ppCommandBars) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ICommandBarsListVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICommandBarsList * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICommandBarsList * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICommandBarsList * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICommandBarsList * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICommandBarsList * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICommandBarsList * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICommandBarsList * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *QueryStateItems )( 
            ICommandBarsList * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LockQuery )( 
            ICommandBarsList * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FreeQuery )( 
            ICommandBarsList * This);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            ICommandBarsList * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            ICommandBarsList * This,
            /* [in] */ VARIANT varItem,
            /* [retval][out] */ ICommandBars **ppCommandBars);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            ICommandBarsList * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        END_INTERFACE
    } ICommandBarsListVtbl;

    interface ICommandBarsList
    {
        CONST_VTBL struct ICommandBarsListVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICommandBarsList_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ICommandBarsList_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ICommandBarsList_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ICommandBarsList_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ICommandBarsList_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ICommandBarsList_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ICommandBarsList_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ICommandBarsList_QueryStateItems(This)	\
    (This)->lpVtbl -> QueryStateItems(This)

#define ICommandBarsList_LockQuery(This)	\
    (This)->lpVtbl -> LockQuery(This)

#define ICommandBarsList_FreeQuery(This)	\
    (This)->lpVtbl -> FreeQuery(This)

#define ICommandBarsList_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define ICommandBarsList_get_Item(This,varItem,ppCommandBars)	\
    (This)->lpVtbl -> get_Item(This,varItem,ppCommandBars)

#define ICommandBarsList_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_QueryStateItems_Proxy( 
    ICommandBarsList * This);


void __RPC_STUB ICommandBarsList_QueryStateItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_LockQuery_Proxy( 
    ICommandBarsList * This);


void __RPC_STUB ICommandBarsList_LockQuery_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_FreeQuery_Proxy( 
    ICommandBarsList * This);


void __RPC_STUB ICommandBarsList_FreeQuery_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_get_Count_Proxy( 
    ICommandBarsList * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB ICommandBarsList_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_get_Item_Proxy( 
    ICommandBarsList * This,
    /* [in] */ VARIANT varItem,
    /* [retval][out] */ ICommandBars **ppCommandBars);


void __RPC_STUB ICommandBarsList_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE ICommandBarsList_get__NewEnum_Proxy( 
    ICommandBarsList * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB ICommandBarsList_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ICommandBarsList_INTERFACE_DEFINED__ */


#ifndef __IMenuBar_INTERFACE_DEFINED__
#define __IMenuBar_INTERFACE_DEFINED__

/* interface IMenuBar */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IMenuBar;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5F150669-5D90-408B-9BDC-A9D16338E84B")
    IMenuBar : public IDispatch
    {
    public:
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ long *pCount) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get_Item( 
            /* [in] */ long Index,
            /* [retval][out] */ ICommandItem **pCommandItem) = 0;
        
        virtual /* [propget][id] */ HRESULT STDMETHODCALLTYPE get__NewEnum( 
            /* [retval][out] */ IUnknown **ppUnk) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Name( 
            /* [retval][out] */ BSTR *pName) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Name( 
            /* [in] */ BSTR Name) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Visible( 
            /* [retval][out] */ VARIANT_BOOL *pVisible) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Visible( 
            /* [in] */ VARIANT_BOOL bVisible) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Enable( 
            /* [retval][out] */ VARIANT_BOOL *pEnable) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Enable( 
            /* [in] */ VARIANT_BOOL newEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertCommand( 
            LONG nIndex,
            UINT uFlags,
            int nNumButton,
            LONG *lpCommand) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddImageList( 
            /* [in] */ LONG_PTR hInstanceRes,
            BSTR strResID,
            /* [retval][out] */ LONG_PTR *pIndex) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItemCmd( 
            LONG nCmdID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetItem( 
            /* [in] */ VARIANT *varItem,
            /* [retval][out] */ ICommandItem **pCommandItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ VARIANT *varItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DeleteAllItem( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBars( 
            /* [retval][out] */ IDispatch **pVal) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Menu( 
            /* [retval][out] */ HMENU *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Menu( 
            /* [in] */ HMENU newVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetPopupMenu( 
            UINT uiBodyIndex,
            enumCommand emcmd,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetPopupMenuGuid( 
            GUID *pguid,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenu) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LoadMenuXml( 
            long *pxmlFile,
            unsigned char *xmlPath) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IMenuBarVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IMenuBar * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IMenuBar * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IMenuBar * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IMenuBar * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IMenuBar * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IMenuBar * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IMenuBar * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IMenuBar * This,
            /* [retval][out] */ long *pCount);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get_Item )( 
            IMenuBar * This,
            /* [in] */ long Index,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [propget][id] */ HRESULT ( STDMETHODCALLTYPE *get__NewEnum )( 
            IMenuBar * This,
            /* [retval][out] */ IUnknown **ppUnk);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Name )( 
            IMenuBar * This,
            /* [retval][out] */ BSTR *pName);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Name )( 
            IMenuBar * This,
            /* [in] */ BSTR Name);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Visible )( 
            IMenuBar * This,
            /* [retval][out] */ VARIANT_BOOL *pVisible);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Visible )( 
            IMenuBar * This,
            /* [in] */ VARIANT_BOOL bVisible);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Enable )( 
            IMenuBar * This,
            /* [retval][out] */ VARIANT_BOOL *pEnable);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Enable )( 
            IMenuBar * This,
            /* [in] */ VARIANT_BOOL newEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertCommand )( 
            IMenuBar * This,
            LONG nIndex,
            UINT uFlags,
            int nNumButton,
            LONG *lpCommand);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddImageList )( 
            IMenuBar * This,
            /* [in] */ LONG_PTR hInstanceRes,
            BSTR strResID,
            /* [retval][out] */ LONG_PTR *pIndex);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItemCmd )( 
            IMenuBar * This,
            LONG nCmdID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetItem )( 
            IMenuBar * This,
            /* [in] */ VARIANT *varItem,
            /* [retval][out] */ ICommandItem **pCommandItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteItem )( 
            IMenuBar * This,
            /* [in] */ VARIANT *varItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DeleteAllItem )( 
            IMenuBar * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBars )( 
            IMenuBar * This,
            /* [retval][out] */ IDispatch **pVal);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Menu )( 
            IMenuBar * This,
            /* [retval][out] */ HMENU *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Menu )( 
            IMenuBar * This,
            /* [in] */ HMENU newVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetPopupMenu )( 
            IMenuBar * This,
            UINT uiBodyIndex,
            enumCommand emcmd,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetPopupMenuGuid )( 
            IMenuBar * This,
            GUID *pguid,
            /* [retval][out] */ IPopupMenuBar **ppPopupMenu);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LoadMenuXml )( 
            IMenuBar * This,
            long *pxmlFile,
            unsigned char *xmlPath);
        
        END_INTERFACE
    } IMenuBarVtbl;

    interface IMenuBar
    {
        CONST_VTBL struct IMenuBarVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IMenuBar_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IMenuBar_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IMenuBar_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IMenuBar_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IMenuBar_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IMenuBar_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IMenuBar_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IMenuBar_get_Count(This,pCount)	\
    (This)->lpVtbl -> get_Count(This,pCount)

#define IMenuBar_get_Item(This,Index,pCommandItem)	\
    (This)->lpVtbl -> get_Item(This,Index,pCommandItem)

#define IMenuBar_get__NewEnum(This,ppUnk)	\
    (This)->lpVtbl -> get__NewEnum(This,ppUnk)

#define IMenuBar_get_Name(This,pName)	\
    (This)->lpVtbl -> get_Name(This,pName)

#define IMenuBar_put_Name(This,Name)	\
    (This)->lpVtbl -> put_Name(This,Name)

#define IMenuBar_get_Visible(This,pVisible)	\
    (This)->lpVtbl -> get_Visible(This,pVisible)

#define IMenuBar_put_Visible(This,bVisible)	\
    (This)->lpVtbl -> put_Visible(This,bVisible)

#define IMenuBar_get_Enable(This,pEnable)	\
    (This)->lpVtbl -> get_Enable(This,pEnable)

#define IMenuBar_put_Enable(This,newEnable)	\
    (This)->lpVtbl -> put_Enable(This,newEnable)

#define IMenuBar_InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)	\
    (This)->lpVtbl -> InsertCommand(This,nIndex,uFlags,nNumButton,lpCommand)

#define IMenuBar_AddImageList(This,hInstanceRes,strResID,pIndex)	\
    (This)->lpVtbl -> AddImageList(This,hInstanceRes,strResID,pIndex)

#define IMenuBar_DeleteItemCmd(This,nCmdID)	\
    (This)->lpVtbl -> DeleteItemCmd(This,nCmdID)

#define IMenuBar_GetItem(This,varItem,pCommandItem)	\
    (This)->lpVtbl -> GetItem(This,varItem,pCommandItem)

#define IMenuBar_DeleteItem(This,varItem)	\
    (This)->lpVtbl -> DeleteItem(This,varItem)

#define IMenuBar_DeleteAllItem(This)	\
    (This)->lpVtbl -> DeleteAllItem(This)

#define IMenuBar_get_CommandBars(This,pVal)	\
    (This)->lpVtbl -> get_CommandBars(This,pVal)

#define IMenuBar_get_Menu(This,pVal)	\
    (This)->lpVtbl -> get_Menu(This,pVal)

#define IMenuBar_put_Menu(This,newVal)	\
    (This)->lpVtbl -> put_Menu(This,newVal)

#define IMenuBar_GetPopupMenu(This,uiBodyIndex,emcmd,ppPopupMenu)	\
    (This)->lpVtbl -> GetPopupMenu(This,uiBodyIndex,emcmd,ppPopupMenu)

#define IMenuBar_GetPopupMenuGuid(This,pguid,ppPopupMenu)	\
    (This)->lpVtbl -> GetPopupMenuGuid(This,pguid,ppPopupMenu)

#define IMenuBar_LoadMenuXml(This,pxmlFile,xmlPath)	\
    (This)->lpVtbl -> LoadMenuXml(This,pxmlFile,xmlPath)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Count_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ long *pCount);


void __RPC_STUB IMenuBar_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Item_Proxy( 
    IMenuBar * This,
    /* [in] */ long Index,
    /* [retval][out] */ ICommandItem **pCommandItem);


void __RPC_STUB IMenuBar_get_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [propget][id] */ HRESULT STDMETHODCALLTYPE IMenuBar_get__NewEnum_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ IUnknown **ppUnk);


void __RPC_STUB IMenuBar_get__NewEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Name_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ BSTR *pName);


void __RPC_STUB IMenuBar_get_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMenuBar_put_Name_Proxy( 
    IMenuBar * This,
    /* [in] */ BSTR Name);


void __RPC_STUB IMenuBar_put_Name_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Visible_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ VARIANT_BOOL *pVisible);


void __RPC_STUB IMenuBar_get_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMenuBar_put_Visible_Proxy( 
    IMenuBar * This,
    /* [in] */ VARIANT_BOOL bVisible);


void __RPC_STUB IMenuBar_put_Visible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Enable_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ VARIANT_BOOL *pEnable);


void __RPC_STUB IMenuBar_get_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMenuBar_put_Enable_Proxy( 
    IMenuBar * This,
    /* [in] */ VARIANT_BOOL newEnable);


void __RPC_STUB IMenuBar_put_Enable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_InsertCommand_Proxy( 
    IMenuBar * This,
    LONG nIndex,
    UINT uFlags,
    int nNumButton,
    LONG *lpCommand);


void __RPC_STUB IMenuBar_InsertCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_AddImageList_Proxy( 
    IMenuBar * This,
    /* [in] */ LONG_PTR hInstanceRes,
    BSTR strResID,
    /* [retval][out] */ LONG_PTR *pIndex);


void __RPC_STUB IMenuBar_AddImageList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_DeleteItemCmd_Proxy( 
    IMenuBar * This,
    LONG nCmdID);


void __RPC_STUB IMenuBar_DeleteItemCmd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_GetItem_Proxy( 
    IMenuBar * This,
    /* [in] */ VARIANT *varItem,
    /* [retval][out] */ ICommandItem **pCommandItem);


void __RPC_STUB IMenuBar_GetItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_DeleteItem_Proxy( 
    IMenuBar * This,
    /* [in] */ VARIANT *varItem);


void __RPC_STUB IMenuBar_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_DeleteAllItem_Proxy( 
    IMenuBar * This);


void __RPC_STUB IMenuBar_DeleteAllItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_CommandBars_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ IDispatch **pVal);


void __RPC_STUB IMenuBar_get_CommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IMenuBar_get_Menu_Proxy( 
    IMenuBar * This,
    /* [retval][out] */ HMENU *pVal);


void __RPC_STUB IMenuBar_get_Menu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IMenuBar_put_Menu_Proxy( 
    IMenuBar * This,
    /* [in] */ HMENU newVal);


void __RPC_STUB IMenuBar_put_Menu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_GetPopupMenu_Proxy( 
    IMenuBar * This,
    UINT uiBodyIndex,
    enumCommand emcmd,
    /* [retval][out] */ IPopupMenuBar **ppPopupMenu);


void __RPC_STUB IMenuBar_GetPopupMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_GetPopupMenuGuid_Proxy( 
    IMenuBar * This,
    GUID *pguid,
    /* [retval][out] */ IPopupMenuBar **ppPopupMenu);


void __RPC_STUB IMenuBar_GetPopupMenuGuid_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IMenuBar_LoadMenuXml_Proxy( 
    IMenuBar * This,
    long *pxmlFile,
    unsigned char *xmlPath);


void __RPC_STUB IMenuBar_LoadMenuXml_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IMenuBar_INTERFACE_DEFINED__ */


#ifndef __IPropertyPageDlg_INTERFACE_DEFINED__
#define __IPropertyPageDlg_INTERFACE_DEFINED__

/* interface IPropertyPageDlg */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IPropertyPageDlg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("69FAA70B-CE69-4898-8C1C-139D11AF4010")
    IPropertyPageDlg : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl,
            UINT uiWidth,
            UINT uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoNoModal( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetConfigPos( 
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetTreeCtrlWidth( 
            /* [in] */ int iWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTreeCtrlWidth( 
            /* [in] */ int *ipWidth) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPropertyPageDlgVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPropertyPageDlg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPropertyPageDlg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPropertyPageDlg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IPropertyPageDlg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IPropertyPageDlg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IPropertyPageDlg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IPropertyPageDlg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            IPropertyPageDlg * This,
            HWND hWndCtrl,
            UINT uiWidth,
            UINT uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoNoModal )( 
            IPropertyPageDlg * This,
            HWND hWndCtrl);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetConfigPos )( 
            IPropertyPageDlg * This,
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetTreeCtrlWidth )( 
            IPropertyPageDlg * This,
            /* [in] */ int iWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTreeCtrlWidth )( 
            IPropertyPageDlg * This,
            /* [in] */ int *ipWidth);
        
        END_INTERFACE
    } IPropertyPageDlgVtbl;

    interface IPropertyPageDlg
    {
        CONST_VTBL struct IPropertyPageDlgVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPropertyPageDlg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPropertyPageDlg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPropertyPageDlg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPropertyPageDlg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPropertyPageDlg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPropertyPageDlg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPropertyPageDlg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPropertyPageDlg_DoModal(This,hWndCtrl,uiWidth,uiHeight)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl,uiWidth,uiHeight)

#define IPropertyPageDlg_DoNoModal(This,hWndCtrl)	\
    (This)->lpVtbl -> DoNoModal(This,hWndCtrl)

#define IPropertyPageDlg_SetConfigPos(This,pxmlDoc,bsPath)	\
    (This)->lpVtbl -> SetConfigPos(This,pxmlDoc,bsPath)

#define IPropertyPageDlg_SetTreeCtrlWidth(This,iWidth)	\
    (This)->lpVtbl -> SetTreeCtrlWidth(This,iWidth)

#define IPropertyPageDlg_GetTreeCtrlWidth(This,ipWidth)	\
    (This)->lpVtbl -> GetTreeCtrlWidth(This,ipWidth)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_DoModal_Proxy( 
    IPropertyPageDlg * This,
    HWND hWndCtrl,
    UINT uiWidth,
    UINT uiHeight);


void __RPC_STUB IPropertyPageDlg_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_DoNoModal_Proxy( 
    IPropertyPageDlg * This,
    HWND hWndCtrl);


void __RPC_STUB IPropertyPageDlg_DoNoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_SetConfigPos_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ ULONG *pxmlDoc,
    /* [in] */ BSTR bsPath);


void __RPC_STUB IPropertyPageDlg_SetConfigPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_SetTreeCtrlWidth_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ int iWidth);


void __RPC_STUB IPropertyPageDlg_SetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_GetTreeCtrlWidth_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ int *ipWidth);


void __RPC_STUB IPropertyPageDlg_GetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPropertyPageDlg_INTERFACE_DEFINED__ */



#ifndef __safToolsLib_LIBRARY_DEFINED__
#define __safToolsLib_LIBRARY_DEFINED__

/* library safToolsLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_safToolsLib;

#ifndef ___INotifyCommandsEvents_DISPINTERFACE_DEFINED__
#define ___INotifyCommandsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _INotifyCommandsEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__INotifyCommandsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C5C64295-71BA-4655-8E68-9FA8BF1C8667")
    _INotifyCommandsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _INotifyCommandsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _INotifyCommandsEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _INotifyCommandsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _INotifyCommandsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _INotifyCommandsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _INotifyCommandsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _INotifyCommandsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _INotifyCommandsEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _INotifyCommandsEventsVtbl;

    interface _INotifyCommandsEvents
    {
        CONST_VTBL struct _INotifyCommandsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _INotifyCommandsEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _INotifyCommandsEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _INotifyCommandsEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _INotifyCommandsEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _INotifyCommandsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _INotifyCommandsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _INotifyCommandsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___INotifyCommandsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_CommandBars;

#ifdef __cplusplus

class DECLSPEC_UUID("42D7E292-FBE0-4D5A-BC95-4D8BB70E3FF4")
CommandBars;
#endif

#ifndef ___ICommandBarsListEvents_DISPINTERFACE_DEFINED__
#define ___ICommandBarsListEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ICommandBarsListEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ICommandBarsListEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("323C5CE0-98A0-4CA0-BA26-80A4AFBAD455")
    _ICommandBarsListEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ICommandBarsListEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ICommandBarsListEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ICommandBarsListEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ICommandBarsListEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ICommandBarsListEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ICommandBarsListEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ICommandBarsListEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ICommandBarsListEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ICommandBarsListEventsVtbl;

    interface _ICommandBarsListEvents
    {
        CONST_VTBL struct _ICommandBarsListEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ICommandBarsListEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ICommandBarsListEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ICommandBarsListEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ICommandBarsListEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ICommandBarsListEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ICommandBarsListEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ICommandBarsListEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ICommandBarsListEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_CommandBarsList;

#ifdef __cplusplus

class DECLSPEC_UUID("CEB591AB-FFB3-42D2-9562-05167277ECF5")
CommandBarsList;
#endif

EXTERN_C const CLSID CLSID_NotifyCommands;

#ifdef __cplusplus

class DECLSPEC_UUID("FDF64DFB-2B84-4F25-90CB-86B740FA9F4F")
NotifyCommands;
#endif

EXTERN_C const CLSID CLSID_CommandGroup;

#ifdef __cplusplus

class DECLSPEC_UUID("929423D2-34AE-467C-9C38-9841F65D00D7")
CommandGroup;
#endif

#ifndef ___IPopupMenuBarEvents_DISPINTERFACE_DEFINED__
#define ___IPopupMenuBarEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IPopupMenuBarEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IPopupMenuBarEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("3E6D0FCC-6F3E-45C0-9B0F-B2FCC6706EAA")
    _IPopupMenuBarEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IPopupMenuBarEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IPopupMenuBarEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IPopupMenuBarEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IPopupMenuBarEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IPopupMenuBarEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IPopupMenuBarEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IPopupMenuBarEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IPopupMenuBarEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IPopupMenuBarEventsVtbl;

    interface _IPopupMenuBarEvents
    {
        CONST_VTBL struct _IPopupMenuBarEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IPopupMenuBarEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IPopupMenuBarEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IPopupMenuBarEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IPopupMenuBarEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IPopupMenuBarEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IPopupMenuBarEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IPopupMenuBarEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IPopupMenuBarEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_PopupMenuBar;

#ifdef __cplusplus

class DECLSPEC_UUID("D2DC57E6-5FCE-44B7-8FFD-74B4C879E65C")
PopupMenuBar;
#endif

#ifndef ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__
#define ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IHeaderCtrlExEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IHeaderCtrlExEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("D2D6AC3B-61BC-4E5A-8889-ADB811268729")
    _IHeaderCtrlExEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IHeaderCtrlExEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IHeaderCtrlExEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IHeaderCtrlExEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IHeaderCtrlExEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IHeaderCtrlExEventsVtbl;

    interface _IHeaderCtrlExEvents
    {
        CONST_VTBL struct _IHeaderCtrlExEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IHeaderCtrlExEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IHeaderCtrlExEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IHeaderCtrlExEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IHeaderCtrlExEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IHeaderCtrlExEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IHeaderCtrlExEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IHeaderCtrlExEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_HeaderCtrlEx;

#ifdef __cplusplus

class DECLSPEC_UUID("B6077849-E15A-40ED-8FCB-A2D12E3B07F2")
HeaderCtrlEx;
#endif

#ifndef ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__
#define ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IListCtrlColumnEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IListCtrlColumnEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("6EC5054B-2BCC-4700-BE85-E094513AAEC0")
    _IListCtrlColumnEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IListCtrlColumnEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IListCtrlColumnEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IListCtrlColumnEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IListCtrlColumnEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IListCtrlColumnEventsVtbl;

    interface _IListCtrlColumnEvents
    {
        CONST_VTBL struct _IListCtrlColumnEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IListCtrlColumnEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IListCtrlColumnEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IListCtrlColumnEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IListCtrlColumnEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IListCtrlColumnEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IListCtrlColumnEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IListCtrlColumnEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ListCtrlColumn;

#ifdef __cplusplus

class DECLSPEC_UUID("CAD97CB7-98BF-4A10-BC5D-0F0A296D11C6")
ListCtrlColumn;
#endif

#ifndef ___IDataViewsEvents_DISPINTERFACE_DEFINED__
#define ___IDataViewsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IDataViewsEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IDataViewsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("BA9140A9-23F3-4383-BE53-11CAF1E28D5B")
    _IDataViewsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IDataViewsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IDataViewsEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IDataViewsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IDataViewsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IDataViewsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IDataViewsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IDataViewsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IDataViewsEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IDataViewsEventsVtbl;

    interface _IDataViewsEvents
    {
        CONST_VTBL struct _IDataViewsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IDataViewsEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IDataViewsEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IDataViewsEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IDataViewsEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IDataViewsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IDataViewsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IDataViewsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IDataViewsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_DataViews;

#ifdef __cplusplus

class DECLSPEC_UUID("E1CDD842-F1EC-4D16-9988-DD067830F9F4")
DataViews;
#endif

#ifndef ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__
#define ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ITreePropertySheetEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ITreePropertySheetEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("2949C4C1-3456-47C4-B73D-4C40EDEFBAE4")
    _ITreePropertySheetEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ITreePropertySheetEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ITreePropertySheetEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ITreePropertySheetEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ITreePropertySheetEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ITreePropertySheetEventsVtbl;

    interface _ITreePropertySheetEvents
    {
        CONST_VTBL struct _ITreePropertySheetEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ITreePropertySheetEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ITreePropertySheetEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ITreePropertySheetEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ITreePropertySheetEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ITreePropertySheetEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ITreePropertySheetEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ITreePropertySheetEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_TreePropertySheet;

#ifdef __cplusplus

class DECLSPEC_UUID("F4FFC7A0-23A8-4E45-9FA1-13644F23AE60")
TreePropertySheet;
#endif

#ifndef ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__
#define ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IRegsvrDllEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IRegsvrDllEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("4872F864-1E3C-42C1-9D5F-7B89E63B2096")
    _IRegsvrDllEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IRegsvrDllEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IRegsvrDllEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IRegsvrDllEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IRegsvrDllEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IRegsvrDllEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IRegsvrDllEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IRegsvrDllEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IRegsvrDllEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IRegsvrDllEventsVtbl;

    interface _IRegsvrDllEvents
    {
        CONST_VTBL struct _IRegsvrDllEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IRegsvrDllEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IRegsvrDllEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IRegsvrDllEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IRegsvrDllEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IRegsvrDllEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IRegsvrDllEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IRegsvrDllEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_RegsvrDll;

#ifdef __cplusplus

class DECLSPEC_UUID("2D8855D8-CBF0-486A-A38A-29507DE5D17E")
RegsvrDll;
#endif

#ifndef ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__
#define ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IThreadNotifyInfoDlgEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IThreadNotifyInfoDlgEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("7236772D-3905-4C8A-9158-C3A9AAC0089C")
    _IThreadNotifyInfoDlgEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IThreadNotifyInfoDlgEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IThreadNotifyInfoDlgEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IThreadNotifyInfoDlgEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IThreadNotifyInfoDlgEventsVtbl;

    interface _IThreadNotifyInfoDlgEvents
    {
        CONST_VTBL struct _IThreadNotifyInfoDlgEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IThreadNotifyInfoDlgEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IThreadNotifyInfoDlgEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IThreadNotifyInfoDlgEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IThreadNotifyInfoDlgEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IThreadNotifyInfoDlgEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IThreadNotifyInfoDlgEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IThreadNotifyInfoDlgEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ThreadNotifyInfoDlg;

#ifdef __cplusplus

class DECLSPEC_UUID("83615B4A-DA05-4F56-B0FC-EE8DE0915A50")
ThreadNotifyInfoDlg;
#endif

#ifndef ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__
#define ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IManagerCallbackEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IManagerCallbackEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("5C2AEA8C-4D17-4BF6-B79D-2BE8CCD205AB")
    _IManagerCallbackEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IManagerCallbackEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IManagerCallbackEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IManagerCallbackEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IManagerCallbackEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IManagerCallbackEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IManagerCallbackEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IManagerCallbackEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IManagerCallbackEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IManagerCallbackEventsVtbl;

    interface _IManagerCallbackEvents
    {
        CONST_VTBL struct _IManagerCallbackEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IManagerCallbackEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IManagerCallbackEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IManagerCallbackEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IManagerCallbackEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IManagerCallbackEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IManagerCallbackEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IManagerCallbackEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ManagerCallback;

#ifdef __cplusplus

class DECLSPEC_UUID("D5E58ADC-12B0-471A-8F05-304A90054349")
ManagerCallback;
#endif

EXTERN_C const CLSID CLSID_SheetPanel;

#ifdef __cplusplus

class DECLSPEC_UUID("BD16F913-A422-4B40-8C4B-35D4688FCF9E")
SheetPanel;
#endif

EXTERN_C const CLSID CLSID_ThumbnailInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("12546ABE-58DA-4AB2-BC78-ECA430BE7576")
ThumbnailInfo;
#endif

EXTERN_C const CLSID CLSID_PropertyPageDlg;

#ifdef __cplusplus

class DECLSPEC_UUID("46556A61-1487-4F04-8375-C9027551F31B")
PropertyPageDlg;
#endif
#endif /* __safToolsLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  HBITMAP_UserSize(     unsigned long *, unsigned long            , HBITMAP * ); 
unsigned char * __RPC_USER  HBITMAP_UserMarshal(  unsigned long *, unsigned char *, HBITMAP * ); 
unsigned char * __RPC_USER  HBITMAP_UserUnmarshal(unsigned long *, unsigned char *, HBITMAP * ); 
void                      __RPC_USER  HBITMAP_UserFree(     unsigned long *, HBITMAP * ); 

unsigned long             __RPC_USER  HICON_UserSize(     unsigned long *, unsigned long            , HICON * ); 
unsigned char * __RPC_USER  HICON_UserMarshal(  unsigned long *, unsigned char *, HICON * ); 
unsigned char * __RPC_USER  HICON_UserUnmarshal(unsigned long *, unsigned char *, HICON * ); 
void                      __RPC_USER  HICON_UserFree(     unsigned long *, HICON * ); 

unsigned long             __RPC_USER  HMENU_UserSize(     unsigned long *, unsigned long            , HMENU * ); 
unsigned char * __RPC_USER  HMENU_UserMarshal(  unsigned long *, unsigned char *, HMENU * ); 
unsigned char * __RPC_USER  HMENU_UserUnmarshal(unsigned long *, unsigned char *, HMENU * ); 
void                      __RPC_USER  HMENU_UserFree(     unsigned long *, HMENU * ); 

unsigned long             __RPC_USER  HWND_UserSize(     unsigned long *, unsigned long            , HWND * ); 
unsigned char * __RPC_USER  HWND_UserMarshal(  unsigned long *, unsigned char *, HWND * ); 
unsigned char * __RPC_USER  HWND_UserUnmarshal(unsigned long *, unsigned char *, HWND * ); 
void                      __RPC_USER  HWND_UserFree(     unsigned long *, HWND * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


