#ifndef __TEMPLTE_PROP_H__
#define __TEMPLTE_PROP_H__

#include "fk\xmlfile.hxx"
#include "fk\BuildObject.hxx"

/*
	控件属性链接点		  控件管理器	   单个控件属性    属性组列表        属性组			单个属性项
fk::LDnaLinkControlProp - LPropTemplates - LObjectProps - LPropItemGroups - LPropItemGroup - LPropItem

*/

_FK_BEGIN

// {42D03F84-7E6C-47bb-9CE3-CA478289A637}
//static const GUID <<name>> = { 0x42d03f84, 0x7e6c, 0x47bb, { 0x9c, 0xe3, 0xca, 0x47, 0x82, 0x89, 0xa6, 0x37 } };
// {1C733C33-AEDE-4611-99D0-48441FB35BE8}
static IID IID_PropItemGroup = {0x1c733c33, 0xaede, 0x4611, { 0x99, 0xd0, 0x48, 0x44, 0x1f, 0xb3, 0x5b, 0xe8 } };
// {4484E371-61FC-4B8E-B092-471D61C408C0}
static IID IID_PropItemGroups = {0x4484e371, 0x61fc, 0x4b8e, { 0xb0, 0x92, 0x47, 0x1d, 0x61, 0xc4, 0x8, 0xc0 } };
// {11E6FB96-40ED-46A5-9152-E5F907BDABFA}
static IID IID_PropItem = {0x11e6fb96, 0x40ed, 0x46a5, { 0x91, 0x52, 0xe5, 0xf9, 0x7, 0xbd, 0xab, 0xfa } };
// {8FE7D11E-6709-43b1-AC41-1431AAA56538}
static const GUID IID_ObjectProps = { 0x8fe7d11e, 0x6709, 0x43b1, { 0xac, 0x41, 0x14, 0x31, 0xaa, 0xa5, 0x65, 0x38 } };
// {12827289-94E0-4565-91A1-4BE78FDC6D23}
static const GUID IID_PropTemplates = { 0x12827289, 0x94e0, 0x4565, { 0x91, 0xa1, 0x4b, 0xe7, 0x8f, 0xdc, 0x6d, 0x23 } };

class LPropGroup;
class LObjectProps;

/*
分解属性获取路径。
格式：@属性组key.属性项key。
实例：@default.caption
使用：
fk::LQuoteDecompose  qd;
fk::LStringw squote(L"@default.caption");
qd.SetQuoteDecompose(squote);
*/
class _FK_OUT_CLASS LQuoteDecompose : public fk::LObject
{
	_FK_RTTI_;

private:
	fk::LStringw	_sRootKey;
	fk::LStringws	_sQuoteItems;
	wchar_t			_lpszPrefix[1];			// 需要分析的字符串，前缀字符。 默认 @ 
	wchar_t			_lpszVisit[1];			// 需要分析的字符串，每个项的分割字符。  默认为 .

public:
	LQuoteDecompose(fk::LObject* pparent=NULL);
	virtual ~LQuoteDecompose(void);

	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual b_call Assign(LObject* pobject);
	virtual v_call clear(void);

	virtual b_call SetPrefix(LPCWSTR lpszPrefix);
	virtual b_call SetVisit(LPCWSTR lpszVisit);
	virtual b_call SetQuoteDecompose(fk::LVar* pQuoteText);					// 设置需要分解的路径。
	virtual fk::LStringw* _call GetRootKey(void);										// 返回属性组 key 名称。

	virtual i_call GetCount(void);
	virtual fk::LStringws* _call GetKeyItems(void);
	virtual fk::LStringw* _call GetItem(int iitem);
	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};

/*
	控件属性模板，有两个读类型，第一种是直接关系数据，第二种是通过调用 
	fk::LWindow::SetPropValue 方法设置数据。
	这两个类只能读出类属性组的只读的项，放在一个数组列表中，有些属性是不读地。
	<item make="0x00000041" caption="Class Name" key="class" 
	PropID="1002" PropIDChild="0" value="" dt="14" info="1111" DisplayDT="14"/>
*/
class _FK_OUT_CLASS LPropItem : public fk::LObject
{
	_FK_RTTI_;
private:
public:
	//
	// base class
	//
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual b_call Assign(LObject* pobject);
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	// 这三个函数属性控件调用，用来操作控件。与其它三个属性不一样。
	virtual b_call SetControlPropValue(fk::LObject* pcontrol, UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetControlPropDisplayValue(fk::LObject* pcontrol, UINT uiPropID, UINT uiPropIDChild,
		const fk::LVar* pBaseDataValueIn, fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetControlPropValue(fk::LObject* pcontrol, UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

public:
	LPropItem(fk::LObject* pparent);
	virtual ~LPropItem(void);

	//
	// 第一位，字通报消息的类型。
	// 
	enum enumNotifyPropertiesMake	{
		umNotifyPropertiesCommand	= 0x00000001,		//	命令格式消息。 LEdit, LButton
		umNotifyPropertiesDialog	= 0x00000002,		//	对话框消息。 WM_XXXXXX	umNotifyPropertiesDialog 和 umNotifyPropertiesWindow 一样。
		umNotifyPropertiesNotify	= 0x00000003,		//  WM_Notify消息， LSysListView, LSysTreeView.
		umNotifyPropertiesTarget	= 0x00000004,		//	TargetProc 消息 
		umNotifyPropertiesWindow	= 0x00000005,		//	窗口消息。 WM_XXXXXX
	};

	enum enumPropertiesItem	{
		PIF_READ					= 0x00000001,		// 项是只读。
		PIF_CHANGE					= 0x00000002,		// 项的值被改变过。
		PIF_THIS_EDIT				= 0x00000004,		// 存在这个标记，值有 PropertiesWindow 控件编辑，不存在这个标记有外部控件编辑。
		PIF_USER_DISPLAY_VALUE		= 0x00000008,		// 使用展示值。 ._sValue 变量。

		PIF_ONE_CTRL				= 0x00000010,		// 使用展示值。 ._sValue 变量。
		PIF_PROP_HIDE				= 0x00000020,		// 属性是隐藏地，不在控件视图中显示。
		PIF_PROP_SAVE				= 0x00000040,		// 属性是保存到 .vxml 文件中。 PIF_PROP_SAVE PIF_PROP_EXIST 这两个属性值需要同步到 fk::LPropItem 类的设置。
		PIF_PROP_EXIST				= 0x00000080,		// 属性是必写项。
		PIF_NOT_INSTALL_DATA		= 0x00010000,		// 属性在创建控件时，不进行安装数据。

		PIF_ITEMOBJ_DIALOG_MODULE	= 0x00000100,		// 项是有模式对话框。
		PIF_ITEMOBJ_DIALOG			= 0x00000200,		// 项是无模式对话框。
		PIF_ITEMOBJ_WINDOW			= 0x00000300,		// 项是无模式窗口。
		PIF_ITEMOBJ_ENUMPLUS		= 0x00000400,		// 项是LEnumPlus。
		PIF_ITEMOBJ_POPUPMENUBAR	= 0x00000500,

		PIF_CTRL_HIDE				= 0x00000000,		// 隐藏编辑框。
		PIF_EDIT					= 0x00001000,		// 编辑框。
		PIF_COMBOBOX_TEXT			= 0x00005000,		// 下接列表框，只有 Text 没有 ItemData。  对应 fk::LEnumPlus	// 同时存在 PIF_ITEMOBJ_ENUMPLUS 标记。
		PIF_COMBOBOX_ITEM_DATA		= 0x00006000,		// 下接列表框，有 Text 和  ItemData。  对应 fk::LEnumPlus		// 同时存在 PIF_ITEMOBJ_ENUMPLUS 标记。
		PIF_COMBOBOX_BOOL			= 0x00007000,		// 下接列表框，有 Text 和  ItemData。  对应 fk::LEnumPlus		// 同时存在 PIF_ITEMOBJ_ENUMPLUS 标记。
		PIF_BUTTON					= 0x0000A000,		// 按钮在最右边，显示 ... 号。
		PIF_EDIT_BUTTON				= 0x0000B000,		// 编辑和按钮。左边编辑框，右边是 ... 号按钮。
		PIF_BUTTON_POPUPMENU		= 0x0000C000,		// 按钮在最右边，显示 ... 号。
		PIF_PROP_NOT_EXIST_CREATE	= 0x00010000,		// 属性不存在，就创建属性。
	};

	fk::LPropGroup*		_pGroupParent;

	//
	// 控件属性读写项。
	//
	DWORD					_dwpiMask;
	fk::LStringa			_cPropNameKey;					// 是属性名称也是属性key,是需要写入到 xml 文件中。
	fk::enumDataType		_umDataType;
	UINT					_uiPropID;
	UINT					_uiPropIDChild;

	//
	// 控件属性编辑器需要属性的读写项。
	//
	DWORD					_dwUserMask;
	fk::LStringw			_scaption;			// 属性的名称。  读入
	fk::LVar*				_pValue;			// 属性的值。		根据 umDataType 类型创建后，读入
	fk::LStringw			_sInfo;				// 属性的说明信息。读入
	fk::enumDataType		_umDisplayValue;	// 展示的数据类型。读入
	fk::LStringw			_sItemObj;
	fk::LStringw			_sfrl;				// 这个值，可能是空。

	fk::LStringw			_sItemClass;		// 属性项是什么类。可能是用户自定类。
	fk::LStringw			_sClassPopupMenu;	// 显示某个类到

	// 根据属性生成的值。
	fk::LStringw			_sDisplayValue;		// 展示的值。		不读入，根据 umDisplayValue 类型显示。
	data_t*					_pSataStruct;		// 项其它数据信息，可能是一个结构，或者是一个对象，什么的。 不是读入。

	fk::LEnumPlus*			_pEnumPlus;			// 这个值，可能是空。 根据

	fk::HXMLNODE			_hxmlNode;			// 项的节点。
	LPARAM					_lparam;			// 用户设置不是读入。

public:
	//
	// LPropItem 类提供的函数和可继承的函数列表。
	//
	v_call SetPropItemGroup(fk::LPropGroup* pPropItemGroup);
	b_call IsItemObjectWindow(void);

	virtual b_call ReadPropItem(fk::LXmlFile* pxmlFile, fk::HXMLNODE hXmlNode, fk::LXmlPropError* pxmlPropError);
	virtual b_call IsSave(void);

	virtual b_call GetPropValue(LPCSTR lpszPropName, fk::LVar* pBaseData);

	static b_fncall NewPropItem(fk::LPropGroup* pPropItemGroup, LPVOID* ppobj, REFIID riid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	//
	// LPropItem 类提供的可选继承函数列表。
	//
	virtual v_call ClickButton(const POINT point);
	virtual v_call ClickButtonPopupMenuBar(const POINT pointIn, fk::LObject* pPopupMenuBarIn);
};

// 属性组类
class _FK_OUT_CLASS LPropGroup : public fk::LObject
{
	_FK_RTTI_;

private:
public:
	enum enumPropItemGroup	{
		OIGF_GlobalGroup= 0x00000001,		// 存在此标记，说明是全局属性组。<dlg make="0x00000001" name="dlg" 
		OIGF_Show		= 0x00000002,		// 属性组隐藏。
		OIGF_Expand		= 0x00000004,		// 展开组。
	};

public:
	LPropGroup(fk::LObject* pParentObject);
	virtual ~LPropGroup(void);

	/*
	读入某个控件属性列表值。新版本属性中使用。
	*/
	virtual b_call ReadPropsGroup(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath) = 0;

	/*
	添加一个属性.
	\param dwMask 
	*/
	//virtual b_call AddProp(DWORD dwMask, LPCSTR lpszPropName, fk::enumDataType umDataType, UINT uiPropID, LPCSTR lpszValue) = 0;
	virtual v_call GroupExpand(bool bExpand) = 0;
	virtual b_call IsExpand(void) = 0;
	virtual v_call GroupShow(bool bShow) = 0;
	virtual b_call IsShow(void) = 0;

	virtual fk::LStringw* _call GetCaption(void) = 0;
	virtual i_call GetItemIndex(fk::LPropItem* pitem) = 0;
	virtual v_call clear(void)  = 0;
	virtual data_t* GetDatas(void) = 0;
	virtual i_call GetItemCount(void) = 0;
	virtual i_call GetShowItemCount(void) = 0;
	virtual fk::LPropItem* _call GetItem(int iitem) = 0;

	virtual b_call FindProp(LPCSTR lpszPropName, fk::LPropItem** ppPropItem) = 0;
	virtual b_call FindPropKey(fk::LVar* pbdkey, fk::LPropItem** ppPropItem) = 0;

	virtual b_call RemoveProp(fk::LPropItem* pti) = 0;
	virtual b_call RemoveProp(LPCSTR lpszPropName) = 0;

	virtual fk::LStringw* _call GetKeyName(void) = 0;
	virtual b_call IsGlobalGroup(void) = 0;
	virtual b_call IsKeyName(fk::LStringw* psKeyName) = 0;
	virtual fk::LXmlFile* _call GetXmlFile(void) = 0;

	virtual v_call SetObjectProps(const fk::LObjectProps* pObjectPropsIn) = 0;
	virtual b_call GetObjectProps(fk::LObjectProps** ppObjectPropsOut) = 0;

	static b_fncall NewPropItemGroup(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid=GUID_NULL  _FK_OBJECT_POS_H2_);
};

// 属性组集合
class _FK_OUT_CLASS LPropGroups : public fk::LObject
{
	_FK_RTTI_;

private:
public:
	LPropGroups(fk::LObject* pParentObject);
	virtual ~LPropGroups(void);

	virtual b_call Append(fk::LPropGroup* pPropItemGroup) = 0;
	virtual b_call GetItem(int iitem, fk::LPropGroup** ppObjectPropsGroup) = 0;
	virtual b_call find(fk::LStringw* pKeyName, fk::LPropGroup** ppObjectPropsGroup) = 0;
	virtual b_call FindPropItem(fk::LQuoteDecompose* pqd, fk::LPropItem** ppPropItem) = 0;

	virtual i_call GetCount(void) = 0;
	virtual data_t* GetDatas(void) = 0;
	virtual v_call clear(void) = 0;
	virtual b_call GeEnumDatas(fk::LEnumData** ppEnumData) = 0;
	virtual b_call Remove(fk::LPropGroup* popg) = 0;

	// 读入全局属性组
	virtual b_call ReadPropGroups(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath) = 0;

	static b_fncall NewPropGroups(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid=GUID_NULL _FK_OBJECT_POS_H2_);
};

/*
	某个控件的属性列表的指针，真实的数据在类的成员变量 fk:ObjectPropsGroups 类中。
	只有属性名，数据类型，属性ID。

	有两个数据格式，一个是老板本，一个是新板本。
*/
class _FK_OUT_CLASS LObjectProps : public fk::LObject
{
	_FK_RTTI_;

protected:
public:
	LObjectProps(fk::LObject* pparent);
	virtual ~LObjectProps(void);

	/*
	读入某个控件属性列表值。
	\param pmoduel
	\param hXmlConfig
	\param lpszfrl
	*/
	virtual b_call ReadObjectPropsFrl(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, LPCSTR lpszfrl) = 0;

	/*
	添加一个属性. AddProp 函数可能会被废除。不在调用。
	\param dwMask 
	*/
	virtual b_call AddProp(DWORD dwMask, LPCSTR lpszPropName, fk::enumDataType umDataType, UINT uiPropID, LPCSTR lpszValue) = 0;
	virtual v_call clear(void)  = 0;

	virtual data_t* GetDatas(void) = 0;
	virtual i_call GetCount(void) = 0;
	virtual fk::LPropItem* _call GetItem(int iitem) = 0;

	virtual fk::LPropItem* _call FindProp(LPCSTR lpszPropName) = 0;
	virtual b_call RemoveProp(fk::LPropItem* pti) = 0;
	virtual b_call RemoveProp(LPCSTR lpszPropName) = 0;

	virtual wp_call GetTemplateClassName(void) = 0;
	virtual fk::LPropGroups* GetPropGroups(void) = 0;

	//
	// 设置一个当前活动类对象，提供给 fk::LPropItem 和 fk::LObjectProps 操作。
	//
	virtual v_call SetActiveObject(const fk::LObject* pActiveObjectIn) = 0;
	virtual b_call GetActiveObject(fk::LObject** pActiveObjectOut) = 0;
	virtual b_call GetActiveObjectIs(const fk::LRuntimeClass* pRuntimeClassIn, fk::LObject** pActiveObjectOut) = 0;

	static LObjectProps* NewObjectProps(fk::LObject* pparent);

public:
	enum enumNotify
	{
		ON_SetActiveObject = 0x00000001,	// 设置新的对象。
	};
};

// 管理所有类的属性列表对象。
class _FK_OUT_CLASS LPropTemplates : public fk::LObject
{
	_FK_RTTI_;

private:
protected:
public:
	LPropTemplates(fk::LObject* pparent);
	virtual ~LPropTemplates(void);

	virtual fk::LObjectProps* _call FindClassProps(LPCWSTR lpszClass) = 0;
	virtual fk::LObjectProps* _call FindClassProps(LPCSTR lpszClass) = 0;
	virtual v_call clear(void) = 0;

	virtual i_call GetCount(void) = 0;
	virtual fk::LObjectProps*  GetItem(int iItem) = 0;
	virtual data_t* _call GetDatas(void) = 0;

	/**
	*	载入<FK>默认的控件列表。
	*	@return 载入成功返回 true，载入失败返回 false。
	*/
	virtual b_call LoadFkDefault(void) = 0;

	/*
	根据配置文件载入控件属性。这个 ReadControlFrls 函数只读新版本控件属性。
	*/
	virtual b_call ReadControlFrls(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath) = 0;

	// 读全局控件属性组。
	virtual b_call ReadGlobalGroup(fk::LModule* pmodule, fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath) = 0;

	/**
	*	根据配置文件中已经设置好的控件属性路径，载入所有控件属性。
	*	\param pmodule 动态库句柄。
	*	\param hConfig xml文件配置句柄。
	*	@return 载入成功返回 true，载入失败返回 false。
	*/
	virtual b_call LoadTemplate(fk::LModule* pmodule, fk::HXMLCONFIG hConfig) = 0;

	/**
	*	根据配置文件中已经设置好的控件属性路径，载入所有控件属性。
	*	\param pmodule 动态库句柄。
	*	\param lpszFile xml文件名称，可以是绝对路径，也可以 cfg 文件夹相对路径。
	*	@return 载入成功返回 true，载入失败返回 false。
	*/
	virtual b_call LoadTemplateXmlFile(fk::LModule* pmodule, LPCWSTR lpszFile) = 0;

	virtual b_call FindQuoteItem(fk::LVar* pQuoteProp, fk::LPropItem** ppPropItem) = 0;

	/*
	全局属性组。
	*/
	virtual fk::LPropGroups* _call GetPropGroupsGlobal(void) = 0;

	/*
	// 设置一个正在被以下5个属性函数，操作中的属性项，能显示更新详细的错误信息。
	b_call SetSelectWindowPropValue(...);	b_call GetSelectWindowPropValue(...);
	b_call GetSelectWindowPropDisplayValue(...);	b_call SetSelectObjectPropValue(...);
	b_call GetSelectObjectPropValue(...);
	*/
	virtual v_call SetOperatePropItem(fk::LPropItem* pPropItem) = 0;
	virtual fk::LPropItem* _call GetOperatePropItem(void) = 0;
	virtual v_call CancelOperatePropItem(void) = 0;

	static LPropTemplates* NewPropTempltes(fk::LObject* pparent);
};

LPropTemplates* _fncall GetPropTemplates(void);
fk::LBuildObjects* _fncall GetBuildObjecsPropItem(void);


_FK_END

#endif
