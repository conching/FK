#ifndef __ThreadWork_hxx__
#define __ThreadWork_hxx__

_FK_BEGIN

enum enumTwnThread
{
	enumTwnThreadRun=1,
	enumTwnThreadNo=2,
};

enum enumTwnClose
{
	enumTwnCloseHand= 0x00000001,
	enumTwnCloseAuto= 0x00000002,
};

class _FK_OUT_CLASS LProgressNotify : public fk::LObject
{
public:
	LProgressNotify(void);
	virtual ~LProgressNotify(void);

	_FK_DBGCLASSINFO_

		virtual void Progress(LONG nMin, LONG nMax)=0;
	virtual void ProgressText(LPCWSTR wcsText)=0;
	virtual void AppendText(LPCWSTR wcsText)=0;
	virtual void Begin(void)=0;
	virtual void End(void)=0;
	virtual bool InstallInfo(void* pData)=0;
	virtual void clear(void)=0;
	virtual void ProgressNotify(WPARAM wParam, LPARAM lParam)=0;
};


// 线程启动、结束过程向外部通报的接口，需要实现这个接口。
class _FK_OUT_CLASS IThreadWork : public fk::LObject
{
public:
	IThreadWork(fk::LObject* pparent);
	virtual ~IThreadWork(void);

	_FK_DBGCLASSINFO_

	virtual STDMETHODIMP ThreadBegin() = 0;
	virtual STDMETHODIMP ThreadEnd() = 0;
	virtual STDMETHODIMP ThreadStop(VARIANT_BOOL *bRetStop) = 0;
	virtual b_call ThreadWork(void) = 0;
	virtual STDMETHODIMP put_ThreadNotify(LONG_PTR pProgressNotify) = 0;
	virtual STDMETHODIMP put_ThreadParam(LONG lParam) = 0;
	virtual STDMETHODIMP get_ThreadParam(LONG* pParam) = 0;
	virtual STDMETHODIMP put_ThreadDataObj(void* pvObj) = 0;
};

// 线程操作接口，系统实现，也可实现一个新的创建线程的类。
struct IThreadOperation
{
	_FK_DBGCLASSINFO_

		virtual bool CreateThread() = 0;
	virtual b_call RunWork() = 0;
	virtual bool StopThread(DWORD dwExitCode) = 0;
	virtual bool EndThread(DWORD dwExitCode) = 0;
	virtual bool SetThreadPriority(int nPriority) = 0;
	virtual i_call GetThreadPriority(void) = 0;
	virtual bool SuspendThread() = 0;
	virtual bool ResumeThread() = 0;
	virtual bool put_ThreadWork(IThreadWork *ptw) = 0;
	virtual bool get_ThreadWork(IThreadWork **ptw) = 0;
	virtual bool WaitGui() = 0;
	virtual bool IsWorking() = 0;
	virtual bool IsStopThread(void) = 0;
	virtual bool IsWaitState(void) = 0;
};


//
//	功能：创建一个线程操作对象。
//	参数：
//		ppThreadNotify - 返回线和操作对象。
//	返回值：返回 com HRESULT 值
//
bool WINAPI ThreadCreateObject(fk::IThreadOperation** ppto);
bool WINAPI ThreadDeleteObject(fk::IThreadOperation* ppto);



//
// 某个功能是否以线程方式执行？
//
enum umRunType	
{
	rtThread=0x1,
	rtNotThread=0x2,
	rtNotRun=3,
};



//
// LThread_dwThreadMask 的值。
//
enum umThreadState
{
	TS_THREADRUN	=0x00000001,							// 线程执行中。
	TS_SUSPEND		=0x00000010,							// 线程在等待状态。
	TS_ENDTHREAD	=0x00000020,							// 存在此标记，线程函数就结束。
	TS_WAIT			=0x00000040,							// 线程在等待状态。
	TS_AUTO_FREE	=0x00000080,							// 线程结束的时候，就自动释放。
};


class _FK_OUT_CLASS LThread : public fk::LObject
{
private:
	HANDLE			_hThread;								// 线程的句柄。
	unsigned int	_ThreadID;								// 线程的ID。
	HANDLE			_hMutex;
	void*			_pArrayChildThread;
	DWORD			_dwExitCode;

protected:
	void*			_pParam;

public:
	LThread(fk::LObject* pParentObject);
	LThread(fk::LObject* pParentObject, DWORD dwExitCode);
	virtual ~LThread();
	DWORD			_dwThreadMask;
	fk::LThread*	_pParentThread;

	b_call CloseHandle(void);	
	void WorkThreadEnd(void);
	void CrateMutex2(void);

	/*--- virtual ----*/
	virtual b_call CreateThread(void* pvParam);
	virtual b_call RunExecute(void);
	virtual b_call execute(void);

	b_call EndThread(DWORD dwExitCode);
	b_call SetThreadPriority(int nPriority);
	i_call GetThreadPriority(void);
	b_call SuspendThread(void);
	b_call ResumeThread(void);
	b_call WaitGui(void);
	b_call IsWorking(void);
	b_call IsStopThread(void);
	b_call IsWaitState(void);
	b_call AutoFree(void);
	b_call AddChildThread(fk::LThread* pChildThread);
	b_call RemoveChildThread(fk::LThread* pChildThread);
	b_call EndThreadFullChild(void);
};
b_fncall ThreadEndFull(DWORD dwExitCode);


class _FK_OUT_CLASS LThreadList : public fk::LComponent
{
private:
	DWORD			_dwThreadList;
	fk::FARRAY		_fArray;
	fk::LThread*	_pThreadWait;
	fk::LThread*	_pThreadEnd;

public:
	LThreadList(fk::LObject* pParentObject);
	virtual ~LThreadList(void);

	b_call AddThread(fk::LThread* pthread, UINT uiExitCode);
	b_call RemoveThread(fk::LThread* pthread);
	b_call EndThreadListAndWait(void);
	b_call CancelEndThreadList(void);
	b_call ModifyEndThreadExitCode(fk::LThread* pthread, DWORD dwExitCode);

	i_call GetThreadCount(void);
	fk::LThread* GetItem(UINT uiItem);
	b_call GetEnum(fk::LEnumData** pEnumData);
	i_call GetWaitingCount(void);
	v_call clear(void);

	b_call RunExecute(void);
	b_call RunThread(void);

	b_call SetThreadPriority(int nPriority);
	i_call GetThreadPriority(void);
	b_call SuspendThread(void);
	b_call ResumeThread(void);
	b_call WaitGui(void);
	b_call IsWorking(void);
	b_call IsStopThread(void);
	b_call IsWaitState(void);
	//b_call AutoFree(void);

	static LThreadList* NewThreadList(fk::LObject* pParentObject);
};

enum enumTLE_EVENT
{
	ON_TLE_ADD						= 0x0000000000000001,			// 添加一个线程。
	ON_TLE_REMOVE					= 0x0000000000000002,			// 移出一个线程。
	ON_TLE_BEGIN_ENDTHREADLIST		= 0x0000000000000004,			// 开始结束线程列表。
	ON_TLE_END_ENDTHREADLIST		= 0x0000000000000008,			// 结束线程列表。
	ON_TLE_BEGIN_CANCELENDTHREADLIST= 0x0000000000000080,			// 开始取消线束线程列表。
	ON_TLE_END_CANCELENDTHREADLIST	= 0x0000000000000100,			// 线束取消线束线程列表。
	ON_TLE_BEGINCLEAR				= 0x0000000000000400,			// 开始清楚所有的线程。
	ON_TLE_ENDCLEAR					= 0x0000000000000800,			// 结束清楚所有的线程。
	ON_TLE_BEGINENDTHREADONE		= 0x0000000000001000,			// 发送每一个需要结束的线程。
	ON_TLE_ENDENDTHREADONE			= 0x0000000000002000,			// 发送每一个需要结束的线程。

	//ON_TLE_CREATETHREAD				= 0x0000000000010000,
	//ON_TLE_BEGIN_ENDTHREAD			= 0x0000000000020000,			
	//ON_TLE_END_ENDTHREAD			= 0x0000000000040000,
};

typedef struct tagTLEADD
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
	UINT uiExitCode;
}TLEADD,* LPTLEADD;

typedef struct tagTLEREMOVE
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
	bool		bRemove;
}TLEREMOVE,* LPTLEREMOVE;

typedef struct tagTLEBEGINENDTHREADLIST
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
}TLEBEGINENDTHREADLIST,* LPTLEBEGINENDTHREADLIST;

typedef struct tagTLEENDENDTHREADLIST
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
}TLEENDENDTHREADLIST,* LPTLEENDENDTHREADLIST;

typedef struct tagTLESETTHREADPRIORITY
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	int iPriority;
	bool bSetPriority;
}TLESETTHREADPRIORITY,* LPTLESETTHREADPRIORITY;

typedef struct tagTLEBEGINCANCELENDTHREADLIST
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
}TLEBEGINCANCELENDTHREADLIST,* LPTLEBEGINCANCELENDTHREADLIST;

typedef struct tagTLEENDCANCELENDTHREADLIST
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
}TLEENDCANCELENDTHREADLIST,* LPTLEENDCANCELENDTHREADLIST;

typedef struct tagTLEBEGINCLEAR
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	bool	bBeginClear;
}TLEBEGINCLEAR,* LPTLEBEGINCLEAR;

typedef struct tagTLEENDCLEAR
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
}TLEENDCLEAR,* LPTLEENDCLEAR;

typedef struct tagTLEBEGINENDTHREADONE
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
	bool bEndThread;
}TLEBEGINENDTHREADONE,* LPTLEBEGINENDTHREADONE;

typedef struct tagTLEENDENDTHREADONE
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
}TLEENDENDTHREADONE,* LPTLEENDENDTHREADONE;

typedef struct tagTLECREATE
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
}TLECREATE,* LPTLECREATE;

typedef struct tagTLEBEGINENDTHREAD
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
}TLEBEGINENDTHREAD,* LPTLEBEGINENDTHREAD;

typedef struct tagTLEENDENDTHREAD
{
	fk::NOTIFYEVENT nEvent;
	fk::LThreadList* pThreadList;
	fk::LThread* pthread;
}TLEENDENDTHREAD,* LPTLEENDENDTHREAD;

LThreadList* _fncall GetThreadList(void);

enum enumThreadWorkingNotifyType
{
	enumtwntText=1,
	enumtwntProgress=2,
	enumtwntOutput=3,
};

//
//	函数名称：FmCreateThreadWorkingNotify
//	作者：许宗森
//	功能：创建一个线程信息显示的对话框接口。
//	参数：
//		ppThreadWorkingNotify - 返回线程信息显示的对话框接口。
//		threadWorkingNotifyType - 指定返回线程信息显示对话框接口的类型。
//	返回值：返回COM 错误值。
//
bool WINAPI fmCreateProgressNotify(fk::LProgressNotify** ppThreadWorkingNotify, enumThreadWorkingNotifyType threadWorkingNotifyType);

_FK_END

#endif
