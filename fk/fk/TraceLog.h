#ifndef __TraceLog_h__
#define __TraceLog_h__


#include "fk\def.hxx"

#ifndef __GNUC__
#ifndef __TraceLog_i_h__
_FK_BEGIN
	#include "fk\TraceLog_i.h"
_FK_END

#else
	#include "fk\TraceLog_i.h"
#endif
#endif // __GNUC__


#include "fk\base.hxx"
#include "fk\array.hxx"
#include "fk\LibXmlObj.h"
#include "fk\trace.hxx"
#include "fk\class.hxx"

#endif //__TraceLog_h__