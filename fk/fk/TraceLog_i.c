

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:14:45 2022
 */
/* Compiler settings for .\TraceLog.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IConfigFile,0xA05507A7,0xF94E,0x4813,0x89,0x65,0x91,0xC7,0xA9,0xAC,0x84,0xF5);


MIDL_DEFINE_GUID(IID, IID_IProcessMacro,0x8E631574,0x79CB,0x4B97,0x83,0x08,0x69,0x7B,0xD9,0x59,0x19,0xA9);


MIDL_DEFINE_GUID(IID, LIBID_TraceLogLib,0xA6293F6D,0x75B5,0x4430,0xB7,0xF3,0x69,0x66,0xD5,0xF3,0x9B,0x01);


MIDL_DEFINE_GUID(IID, DIID__IConfigFileEvents,0x3B60781C,0x9DDE,0x4AE9,0xB1,0xAE,0x14,0x21,0x44,0x6D,0x14,0xCB);


MIDL_DEFINE_GUID(CLSID, CLSID_ConfigFile,0xE3EDD109,0xBDD0,0x41BB,0x92,0xB3,0x76,0xF8,0x21,0x8D,0x6E,0x51);


MIDL_DEFINE_GUID(IID, DIID__IProcessMacroEvents,0x7CFED956,0x2B01,0x4BF9,0xB3,0x3E,0x48,0xD5,0xC7,0xF2,0x2D,0xFB);


MIDL_DEFINE_GUID(CLSID, CLSID_ProcessMacro,0xC9DC2306,0xEB43,0x48CD,0xB3,0x0D,0x6C,0x48,0xA0,0x2C,0xE9,0xC3);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



