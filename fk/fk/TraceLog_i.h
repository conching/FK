

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:14:45 2022
 */
/* Compiler settings for .\TraceLog.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __TraceLog_i_h__
#define __TraceLog_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IConfigFile_FWD_DEFINED__
#define __IConfigFile_FWD_DEFINED__
typedef interface IConfigFile IConfigFile;
#endif 	/* __IConfigFile_FWD_DEFINED__ */


#ifndef __IProcessMacro_FWD_DEFINED__
#define __IProcessMacro_FWD_DEFINED__
typedef interface IProcessMacro IProcessMacro;
#endif 	/* __IProcessMacro_FWD_DEFINED__ */


#ifndef ___IConfigFileEvents_FWD_DEFINED__
#define ___IConfigFileEvents_FWD_DEFINED__
typedef interface _IConfigFileEvents _IConfigFileEvents;
#endif 	/* ___IConfigFileEvents_FWD_DEFINED__ */


#ifndef __ConfigFile_FWD_DEFINED__
#define __ConfigFile_FWD_DEFINED__

#ifdef __cplusplus
typedef class ConfigFile ConfigFile;
#else
typedef struct ConfigFile ConfigFile;
#endif /* __cplusplus */

#endif 	/* __ConfigFile_FWD_DEFINED__ */


#ifndef ___IProcessMacroEvents_FWD_DEFINED__
#define ___IProcessMacroEvents_FWD_DEFINED__
typedef interface _IProcessMacroEvents _IProcessMacroEvents;
#endif 	/* ___IProcessMacroEvents_FWD_DEFINED__ */


#ifndef __ProcessMacro_FWD_DEFINED__
#define __ProcessMacro_FWD_DEFINED__

#ifdef __cplusplus
typedef class ProcessMacro ProcessMacro;
#else
typedef struct ProcessMacro ProcessMacro;
#endif /* __cplusplus */

#endif 	/* __ProcessMacro_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IConfigFile_INTERFACE_DEFINED__
#define __IConfigFile_INTERFACE_DEFINED__

/* interface IConfigFile */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IConfigFile;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A05507A7-F94E-4813-8965-91C7A9AC84F5")
    IConfigFile : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadText( 
            BSTR strPath,
            BSTR *strTextOut,
            BSTR strDefaultIn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadTypedValue( 
            BSTR strPath,
            VARIANT *pvarValueOut,
            const VARIANT *pvalDefaultIn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Load( 
            BSTR strFullName,
            VARIANT_BOOL bNotExistCreate) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_FullName( 
            /* [retval][out] */ BSTR *strFullName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Path( 
            /* [retval][out] */ BSTR *strPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Close( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Save( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SaveAs( 
            BSTR strFullName) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_XMLDOMDocument( 
            /* [retval][out] */ IDispatch **ppXMLDOMDocument) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadVariantBool( 
            BSTR strPath,
            VARIANT_BOOL *bBoolValue,
            VARIANT_BOOL bDefaultValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadLong( 
            /* [in] */ BSTR strPath,
            /* [out] */ LONG *nLong,
            /* [in] */ LONG nDefaultLong) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadVersion( 
            BSTR bstrXmlPath,
            LONG_PTR *lpvsFixedFileInfo) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE TestNode( 
            BSTR bstrXmlPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE selectSingleNode( 
            BSTR bstrXmlPath,
            IDispatch **ppXMLDOMNode) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IConfigFileVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IConfigFile * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IConfigFile * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IConfigFile * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IConfigFile * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IConfigFile * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IConfigFile * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IConfigFile * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadText )( 
            IConfigFile * This,
            BSTR strPath,
            BSTR *strTextOut,
            BSTR strDefaultIn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadTypedValue )( 
            IConfigFile * This,
            BSTR strPath,
            VARIANT *pvarValueOut,
            const VARIANT *pvalDefaultIn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Load )( 
            IConfigFile * This,
            BSTR strFullName,
            VARIANT_BOOL bNotExistCreate);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FullName )( 
            IConfigFile * This,
            /* [retval][out] */ BSTR *strFullName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Path )( 
            IConfigFile * This,
            /* [retval][out] */ BSTR *strPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Close )( 
            IConfigFile * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Save )( 
            IConfigFile * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SaveAs )( 
            IConfigFile * This,
            BSTR strFullName);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_XMLDOMDocument )( 
            IConfigFile * This,
            /* [retval][out] */ IDispatch **ppXMLDOMDocument);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadVariantBool )( 
            IConfigFile * This,
            BSTR strPath,
            VARIANT_BOOL *bBoolValue,
            VARIANT_BOOL bDefaultValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadLong )( 
            IConfigFile * This,
            /* [in] */ BSTR strPath,
            /* [out] */ LONG *nLong,
            /* [in] */ LONG nDefaultLong);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadVersion )( 
            IConfigFile * This,
            BSTR bstrXmlPath,
            LONG_PTR *lpvsFixedFileInfo);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *TestNode )( 
            IConfigFile * This,
            BSTR bstrXmlPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *selectSingleNode )( 
            IConfigFile * This,
            BSTR bstrXmlPath,
            IDispatch **ppXMLDOMNode);
        
        END_INTERFACE
    } IConfigFileVtbl;

    interface IConfigFile
    {
        CONST_VTBL struct IConfigFileVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IConfigFile_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IConfigFile_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IConfigFile_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IConfigFile_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IConfigFile_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IConfigFile_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IConfigFile_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IConfigFile_ReadText(This,strPath,strTextOut,strDefaultIn)	\
    (This)->lpVtbl -> ReadText(This,strPath,strTextOut,strDefaultIn)

#define IConfigFile_ReadTypedValue(This,strPath,pvarValueOut,pvalDefaultIn)	\
    (This)->lpVtbl -> ReadTypedValue(This,strPath,pvarValueOut,pvalDefaultIn)

#define IConfigFile_Load(This,strFullName,bNotExistCreate)	\
    (This)->lpVtbl -> Load(This,strFullName,bNotExistCreate)

#define IConfigFile_get_FullName(This,strFullName)	\
    (This)->lpVtbl -> get_FullName(This,strFullName)

#define IConfigFile_get_Path(This,strPath)	\
    (This)->lpVtbl -> get_Path(This,strPath)

#define IConfigFile_Close(This)	\
    (This)->lpVtbl -> Close(This)

#define IConfigFile_Save(This)	\
    (This)->lpVtbl -> Save(This)

#define IConfigFile_SaveAs(This,strFullName)	\
    (This)->lpVtbl -> SaveAs(This,strFullName)

#define IConfigFile_get_XMLDOMDocument(This,ppXMLDOMDocument)	\
    (This)->lpVtbl -> get_XMLDOMDocument(This,ppXMLDOMDocument)

#define IConfigFile_ReadVariantBool(This,strPath,bBoolValue,bDefaultValue)	\
    (This)->lpVtbl -> ReadVariantBool(This,strPath,bBoolValue,bDefaultValue)

#define IConfigFile_ReadLong(This,strPath,nLong,nDefaultLong)	\
    (This)->lpVtbl -> ReadLong(This,strPath,nLong,nDefaultLong)

#define IConfigFile_ReadVersion(This,bstrXmlPath,lpvsFixedFileInfo)	\
    (This)->lpVtbl -> ReadVersion(This,bstrXmlPath,lpvsFixedFileInfo)

#define IConfigFile_TestNode(This,bstrXmlPath)	\
    (This)->lpVtbl -> TestNode(This,bstrXmlPath)

#define IConfigFile_selectSingleNode(This,bstrXmlPath,ppXMLDOMNode)	\
    (This)->lpVtbl -> selectSingleNode(This,bstrXmlPath,ppXMLDOMNode)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_ReadText_Proxy( 
    IConfigFile * This,
    BSTR strPath,
    BSTR *strTextOut,
    BSTR strDefaultIn);


void __RPC_STUB IConfigFile_ReadText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_ReadTypedValue_Proxy( 
    IConfigFile * This,
    BSTR strPath,
    VARIANT *pvarValueOut,
    const VARIANT *pvalDefaultIn);


void __RPC_STUB IConfigFile_ReadTypedValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_Load_Proxy( 
    IConfigFile * This,
    BSTR strFullName,
    VARIANT_BOOL bNotExistCreate);


void __RPC_STUB IConfigFile_Load_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IConfigFile_get_FullName_Proxy( 
    IConfigFile * This,
    /* [retval][out] */ BSTR *strFullName);


void __RPC_STUB IConfigFile_get_FullName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IConfigFile_get_Path_Proxy( 
    IConfigFile * This,
    /* [retval][out] */ BSTR *strPath);


void __RPC_STUB IConfigFile_get_Path_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_Close_Proxy( 
    IConfigFile * This);


void __RPC_STUB IConfigFile_Close_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_Save_Proxy( 
    IConfigFile * This);


void __RPC_STUB IConfigFile_Save_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_SaveAs_Proxy( 
    IConfigFile * This,
    BSTR strFullName);


void __RPC_STUB IConfigFile_SaveAs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IConfigFile_get_XMLDOMDocument_Proxy( 
    IConfigFile * This,
    /* [retval][out] */ IDispatch **ppXMLDOMDocument);


void __RPC_STUB IConfigFile_get_XMLDOMDocument_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_ReadVariantBool_Proxy( 
    IConfigFile * This,
    BSTR strPath,
    VARIANT_BOOL *bBoolValue,
    VARIANT_BOOL bDefaultValue);


void __RPC_STUB IConfigFile_ReadVariantBool_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_ReadLong_Proxy( 
    IConfigFile * This,
    /* [in] */ BSTR strPath,
    /* [out] */ LONG *nLong,
    /* [in] */ LONG nDefaultLong);


void __RPC_STUB IConfigFile_ReadLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_ReadVersion_Proxy( 
    IConfigFile * This,
    BSTR bstrXmlPath,
    LONG_PTR *lpvsFixedFileInfo);


void __RPC_STUB IConfigFile_ReadVersion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_TestNode_Proxy( 
    IConfigFile * This,
    BSTR bstrXmlPath);


void __RPC_STUB IConfigFile_TestNode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConfigFile_selectSingleNode_Proxy( 
    IConfigFile * This,
    BSTR bstrXmlPath,
    IDispatch **ppXMLDOMNode);


void __RPC_STUB IConfigFile_selectSingleNode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IConfigFile_INTERFACE_DEFINED__ */


#ifndef __IProcessMacro_INTERFACE_DEFINED__
#define __IProcessMacro_INTERFACE_DEFINED__

/* interface IProcessMacro */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IProcessMacro;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8E631574-79CB-4B97-8308-697BD95919A9")
    IProcessMacro : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ BSTR strName,
            /* [in] */ BSTR strValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MakeCopy( 
            /* [in] */ BSTR strMake,
            /* [retval][out] */ BSTR *strOutValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Make( 
            /* [retval][out] */ BSTR *strMake) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MakeCopyWChar( 
            /* [in] */ wchar_t *strMake,
            /* [retval][out] */ wchar_t **strOutValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Edit( 
            BSTR strName,
            BSTR strValue) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Remove( 
            BSTR strName) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowInfo( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadXmlPath( 
            long *hXmlConfig,
            unsigned char *xmlMacroPath) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProcessMacroVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProcessMacro * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProcessMacro * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProcessMacro * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IProcessMacro * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IProcessMacro * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IProcessMacro * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IProcessMacro * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IProcessMacro * This,
            /* [in] */ BSTR strName,
            /* [in] */ BSTR strValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MakeCopy )( 
            IProcessMacro * This,
            /* [in] */ BSTR strMake,
            /* [retval][out] */ BSTR *strOutValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Make )( 
            IProcessMacro * This,
            /* [retval][out] */ BSTR *strMake);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *MakeCopyWChar )( 
            IProcessMacro * This,
            /* [in] */ wchar_t *strMake,
            /* [retval][out] */ wchar_t **strOutValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Edit )( 
            IProcessMacro * This,
            BSTR strName,
            BSTR strValue);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IProcessMacro * This,
            BSTR strName);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowInfo )( 
            IProcessMacro * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadXmlPath )( 
            IProcessMacro * This,
            long *hXmlConfig,
            unsigned char *xmlMacroPath);
        
        END_INTERFACE
    } IProcessMacroVtbl;

    interface IProcessMacro
    {
        CONST_VTBL struct IProcessMacroVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProcessMacro_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProcessMacro_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProcessMacro_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProcessMacro_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IProcessMacro_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IProcessMacro_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IProcessMacro_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IProcessMacro_Add(This,strName,strValue)	\
    (This)->lpVtbl -> Add(This,strName,strValue)

#define IProcessMacro_MakeCopy(This,strMake,strOutValue)	\
    (This)->lpVtbl -> MakeCopy(This,strMake,strOutValue)

#define IProcessMacro_Make(This,strMake)	\
    (This)->lpVtbl -> Make(This,strMake)

#define IProcessMacro_MakeCopyWChar(This,strMake,strOutValue)	\
    (This)->lpVtbl -> MakeCopyWChar(This,strMake,strOutValue)

#define IProcessMacro_Edit(This,strName,strValue)	\
    (This)->lpVtbl -> Edit(This,strName,strValue)

#define IProcessMacro_Remove(This,strName)	\
    (This)->lpVtbl -> Remove(This,strName)

#define IProcessMacro_ShowInfo(This)	\
    (This)->lpVtbl -> ShowInfo(This)

#define IProcessMacro_ReadXmlPath(This,hXmlConfig,xmlMacroPath)	\
    (This)->lpVtbl -> ReadXmlPath(This,hXmlConfig,xmlMacroPath)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_Add_Proxy( 
    IProcessMacro * This,
    /* [in] */ BSTR strName,
    /* [in] */ BSTR strValue);


void __RPC_STUB IProcessMacro_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_MakeCopy_Proxy( 
    IProcessMacro * This,
    /* [in] */ BSTR strMake,
    /* [retval][out] */ BSTR *strOutValue);


void __RPC_STUB IProcessMacro_MakeCopy_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_Make_Proxy( 
    IProcessMacro * This,
    /* [retval][out] */ BSTR *strMake);


void __RPC_STUB IProcessMacro_Make_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_MakeCopyWChar_Proxy( 
    IProcessMacro * This,
    /* [in] */ wchar_t *strMake,
    /* [retval][out] */ wchar_t **strOutValue);


void __RPC_STUB IProcessMacro_MakeCopyWChar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_Edit_Proxy( 
    IProcessMacro * This,
    BSTR strName,
    BSTR strValue);


void __RPC_STUB IProcessMacro_Edit_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_Remove_Proxy( 
    IProcessMacro * This,
    BSTR strName);


void __RPC_STUB IProcessMacro_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_ShowInfo_Proxy( 
    IProcessMacro * This);


void __RPC_STUB IProcessMacro_ShowInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProcessMacro_ReadXmlPath_Proxy( 
    IProcessMacro * This,
    long *hXmlConfig,
    unsigned char *xmlMacroPath);


void __RPC_STUB IProcessMacro_ReadXmlPath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProcessMacro_INTERFACE_DEFINED__ */



#ifndef __TraceLogLib_LIBRARY_DEFINED__
#define __TraceLogLib_LIBRARY_DEFINED__

/* library TraceLogLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_TraceLogLib;

#ifndef ___IConfigFileEvents_DISPINTERFACE_DEFINED__
#define ___IConfigFileEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IConfigFileEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IConfigFileEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("3B60781C-9DDE-4AE9-B1AE-1421446D14CB")
    _IConfigFileEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IConfigFileEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IConfigFileEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IConfigFileEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IConfigFileEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IConfigFileEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IConfigFileEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IConfigFileEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IConfigFileEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IConfigFileEventsVtbl;

    interface _IConfigFileEvents
    {
        CONST_VTBL struct _IConfigFileEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IConfigFileEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IConfigFileEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IConfigFileEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IConfigFileEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IConfigFileEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IConfigFileEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IConfigFileEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IConfigFileEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ConfigFile;

#ifdef __cplusplus

class DECLSPEC_UUID("E3EDD109-BDD0-41BB-92B3-76F8218D6E51")
ConfigFile;
#endif

#ifndef ___IProcessMacroEvents_DISPINTERFACE_DEFINED__
#define ___IProcessMacroEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IProcessMacroEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IProcessMacroEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("7CFED956-2B01-4BF9-B33E-48D5C7F22DFB")
    _IProcessMacroEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IProcessMacroEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IProcessMacroEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IProcessMacroEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IProcessMacroEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IProcessMacroEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IProcessMacroEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IProcessMacroEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IProcessMacroEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IProcessMacroEventsVtbl;

    interface _IProcessMacroEvents
    {
        CONST_VTBL struct _IProcessMacroEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IProcessMacroEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IProcessMacroEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IProcessMacroEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IProcessMacroEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IProcessMacroEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IProcessMacroEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IProcessMacroEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IProcessMacroEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ProcessMacro;

#ifdef __cplusplus

class DECLSPEC_UUID("C9DC2306-EB43-48CD-B30D-6C48A02CE9C3")
ProcessMacro;
#endif
#endif /* __TraceLogLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


