#ifndef __WINCTRL_H__
#define __WINCTRL_H__


#include <Tlhelp32.h>
#include <shlobj.h>

#include "fk\base.hxx"
#include "fk\trace.hxx"
#include "fk\ctrls.hxx"



/*--------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */


_FK_BEGIN


struct LEnumBase
{
	virtual bool IntToEnum(int iEnum)=0;
	virtual int GetCurrentEnum(void)=0;
	virtual bool SetEnum(int iEnumValue)=0;
	virtual bool IsEnum(int iEnumValue)=0;
	virtual b_call LoadMark(int iEnumValue, wchar_t* pwcText)=0;
};



/*--------------------------------------------------------------------------*/
#define MY_WS_FLOATING		0x00000001L
#define SAF_SHOW_BOX		0x00000001L		//提示对话框
#define SAF_WRITE_LOG		0x00000002L		//写入日志





/*--------------------------------------------------------------------------*/

BOOL WINAPI ListView_DeleteColumnFull(HWND hWndCtrl);

/*
** 指定从当前选择项向下第几个为当前选择项
** @param hWndCtrl	窗口句柄
** @param nCount	向下指定第几项为当前选择项
** @return 成功返回TRUE,失败和已经是最后一个是FALSE.
*/
BOOL WINAPI ListViewNextItem(HWND hWndCtrl, int nCount);

BOOL WINAPI ListViewPrevItem(HWND hWndCtrl, int nCount);

/*
** 得到当前窗口可见区域显示项的总数。
** @param hWndCtrl 指定ListCtrl控件的窗口句柄。
** @param nFirst 返回可见区域第一项。
** @param nLast 返回可见区域最后一项。
** @return 操作成功返回TRUE,
** 操作失败返回FALSE。
*/
struct LVVisibleItem
{
	DWORD dwSize;
	int iFirst;
	int iLast;
};
BOOL WINAPI ListViewGetVisibleItem(HWND hWndCtrl, LVVisibleItem* plvvi);

//
// Description:
//		释放ListView控件pParam参数中的ComObject对象。pParam是IUnknown接口。
// ListView_ParamReleaseComObject
//
bool WINAPI ListViewParamReleaseComObject(HWND hWndListView);

b_fncall ListViewModifyExtendedStyleEx(HWND hwndCtrl, DWORD dwRemove, DWORD dwAdd);
bool WINAPI ListViewCreateColumnObject(fk::enumClassValue cvClassValue, void** pplcc);

int WINAPI HeaderFindParam(HWND hWndCtrl, LPARAM lParam);

//	功能：设置每一个自画属性。
//	参数：
//		hWndHeaderCtrl - 控件句柄
BOOL WINAPI HeaderCtrlSetColumnDraw(HWND hWndHeaderCtrl);


bool WINAPI TreeViewIsSibling(__in const HWND hWndCtrl,
								__in const HTREEITEM hTreeItemNew,
								__in const HTREEITEM hTreeItem
								);
void WINAPI TreeViewDeleteChildItem(HWND hWndCtrl, HTREEITEM hParentItem);
int WINAPI TabCtrlFindItem(HWND hWndCtrl, TCITEM *lptcItem);
int WINAPI TabCtrlFindCaption(HWND hWndCtrl, LPCTSTR lpszText, UINT nLength);


int WINAPI MenuGetPosFromID(HMENU hmenu, UINT id);
int WINAPI ComboBoxFindItemData(HWND hWndCtrl, DWORD dwData);

/*
** Int 编辑框 
*/
typedef void* HINTEDIT;
struct UTINTEDIT
{
	DWORD dwMask;
	int iMin;
	int iMax;
};
HINTEDIT WINAPI EditIntSubclass(HWND hwndEdit, UTINTEDIT* pie);
bool WINAPI EditIntUnsubclass(HWND hwndEdit);


typedef void* HFILEEDIT;
HFILEEDIT WINAPI EditFileSubclass(HWND hwndEdit);
bool WINAPI EditFileUnsubclass(HWND hwndEdit);

b_call EnableWindowArray(HWND hWndParent, int* iArray, int iArrayCount, bool bEnable);

/************************************************************************
函数名:		ReBarCtrl_FindBandInfo
函数功能		查找 Band 项信息。
参数名 
hWndCtrl:	窗口句柄。
rebiOut:	根据rebi.fMask的值，返回的 LPREBARBANDINF结构。
rebiFind:	根据rebiFind中的信息进行查找。

返回值
True : 成功返回。
False:	失败返回。
调用范例:

REBARBANDINFO rebi={0};
REBARBANDINFO rebiFind={0};

//通过窗口句柄得到Band索引。
rebi.fMask			= RBBIM_ID | RBBIM_CHILD; //结构返回的信息。

rebiFind.fMask		= RBBIM_CHILD;	//查找的信息。
rebiFind.hwndChild	= hWndCtrl;
ATLVERIFY(
fk::ReBarCtrl_FindBandInfo(
barCtrl.m_hWnd,
&rebi,
&rebiFind)
);

开发者
开发时间
修改者
修改时间
版本号
*/

BOOL WINAPI ReBarCtrlFindBandInfo(HWND hWndCtrl, LPREBARBANDINFO rebiOut, LPCREBARBANDINFO rebiFind);

/*
** 获得是否存在存超出边界。
*/
BOOL WINAPI ToolBarCtrlOutButton(HWND hWndCtrl);

/*
** 把nBeginIndex 和 nEndIndex范围索引内的按钮隐藏或者显示出来。
*/
BOOL WINAPI ToolBarCtrlHideButton(HWND hWndCtrl, int nBeginIndex, int nEndIndex, BOOL bHide);

/*
** 在窗口中查找同类的第一个窗口。
*/
HWND WINAPI FindClassName(HWND hWndCtrl, LPCTSTR strClassName);

b_call ErrorComWindowNoCreate(HWND hWndCtrl, LPCTSTR lpszFuntion, const CLSID& clsid, const IID& iid);

b_fncall LoadVersion(HMODULE hModule, HWND hWndCtrl);
b_fncall SetMainFrameHWnd(UINT iItem, HWND hWndMainFrame);
HWND _fncall GetMainFrameHWnd(UINT uiItem);


/*--------------------------------------------------------------------------*/
BOOL WINAPI PathIsDirectoryBoxA(HWND hWndCtrl, LPCSTR lpszPath);
BOOL WINAPI PathIsDirectoryBoxW(HWND hWndCtrl, LPCWSTR lpszPath);
#ifdef _UNICODE
#define  PathIsDirectoryBox PathIsDirectoryBoxW
#else
#define  PathIsDirectoryBox PathIsDirectoryBoxA
#endif

BOOL WINAPI CheckFileExistsA(HWND hWnd, LPCSTR lpszFileName, int nFileNameCount, LPCSTR lpszPath, bool bHint);
BOOL WINAPI CheckFileExistsW(HWND hWnd, LPCWSTR *lpszFileName, int nFileNameCount, LPCWSTR lpszPath, bool bHint);
#ifdef _UNICODE
#define  fmCheckFileExists CheckFileExistsW
#else
#define  fmCheckFileExists CheckFileExistsA
#endif

enum enumBrowseInfo
{
	umBrowseInfoNil=0x0,
	umBrowseInfoEdit=0x1,
	umBrowseInfoComboBox=0x2
};

/*
** 弹出选择文件夹对话框，并把选择的文件夹绝对路径写入到 EDIT 或者 ComBoBox 控件中。
*/
struct FMBROWSEINFO
{
	DWORD			dwSize;
	DWORD			dwMask;
	HINSTANCE		hRes;
	HWND			hWndCtrl;
	enumBrowseInfo	umControlType;
	LPITEMIDLIST	lpidRet;
	fk::LStringw*	spPathRet;			// 这个值可能是空的。
	BROWSEINFO		bi;
};

b_call BrowseForFolder(FMBROWSEINFO* pbi);

#define SAF_FMB_LAYERED_CLOSE			0x00000001		//窗口采用透明方式关闭窗口。
#define SAF_FMB_MODULE					0x00000002		//窗口采用不透明方式关闭窗口。

//
//	函数名称：MessageBoxFlash
//	功能：显示一个消息没有标题的对话框，几秒后，自动关闭。
//	参数：
//		fuStyle - 指定消息框风格，渐变出现，渐变关闭。
//		hInstance - 指字符串资源句柄
//		hWndParent - 设置父窗口句柄
//		hIcon - 文件前面显示的图标
//		nTime - 几秒钟后窗口关闭
//		lpszHint - 提示的文字，也可以是资源字符串ID
//		fuStyle - 指定窗口一些属性
//	返回值：
BOOL WINAPI MessageBoxFlash(HINSTANCE hInstance, HWND hWndParent, HICON hIcon, DWORD nTime,
							  LPCWSTR lpszHint,
							  UINT fuStyle,
							  ...
							  );

#define SBC_PARAM			0x00000002
#define SBC_ICON			0x00000004
#define SBC_BITMAP			0x00000008

typedef	struct COMMANDBOX
{
	DWORD	 dwSize;
	UINT     fMask;
	DWORD	 fType;
	DWORD	 dwState;
	HINSTANCE hRes;
	int      nCommandId;
	UINT     cch;
	wchar_t* szText;
	LPARAM	 lParam;
	union
	{
		HICON		hIcon;
		HBITMAP		hBitMap;
	};
}COMMANDBOX, *PCOMMANDBOX;

#define FKSB_RES	0x00000001
struct FK_SELECTBOXUIW
{
	DWORD dwSize;
	DWORD dwMask;
	HINSTANCE hRes;
	HWND hwndParent;
	wchar_t* pszTitle;
	wchar_t* pszInfo;
};
#if _DEBUG
#define FK_SELECTBOXUI	FK_SELECTBOXUIW
#else
#define FK_SELECTBOXUI	FK_SELECTBOXUIW
#endif
i_fncall fk_SelectBox(FK_SELECTBOXUI* psbui, PCOMMANDBOX pcmdarray, UINT uiArrayCount);

/*--------------------------------------------------------------------------*/
// 这里需要重构，添加到 中。
class LThumbnailList : public fk::LImageList
{
public:
	ULONG	muiImageWidth;
	ULONG	muiImageHeight;

	LThumbnailList()
	{
	}
	~LThumbnailList()
	{
	}
};

int WINAPI GetDropDownWidth(HWND hwndCtrl);

/*--------------------------------------------------------------------------*/
// *File
/*
**	判断是否为合法文件名称字符
*/
BOOL WINAPI FileIsCharA(char pChar);
BOOL WINAPI FileIsCharW(wchar_t pChar);
#ifdef UNICODE
#define  FileIsChar	FileIsCharW
#else
#define  FileIsChar	FileIsCharA
#endif

BOOL WINAPI FileNameIsValidA(const char *pFileName);
BOOL WINAPI FileNameIsValidW(const wchar_t *pFileName);
#ifdef UNICODE
#define FileNameIsValid	FileNameIsValidW
#else
#define FileNameIsValid	FileNameIsValidA
#endif

LPCWSTR WINAPI PathCombinationResFile(LPCWSTR szResName, LPWSTR szOutFile);
wp_fncall PathCombinationConfigFile(LPCWSTR szConfigName, LPWSTR szOutFile);

void WINAPI SetupLogObject(void *lpLogObject);
void* WINAPI GetLogObject();

bool WINAPI fmRectIsScreen(const RECT* prcIn);
bool WINAPI ModifyScreenRect(RECT* prcIn);

/*
** 根据hInstance得到它的测试文件配置xml文件。
*/
int WINAPI GetTestFileW(HINSTANCE hInstance, LPWSTR lpszFullName, DWORD nMaxCount);
int WINAPI GetTestFileA(HINSTANCE hInstance, LPSTR lpszFileName, DWORD nMaxCount);
#ifdef _UNICODE
#define  safGetTestFile GetTestFileW
#else
#define  safGetTestFile GetTestFileA
#endif

//函 数 名：DeleteAllFileW
//函数功能：删除某一个文件夹下的所有文件。
//输入参数：
//	lpszPath - 指定文件夹路径。
//输出参数：无
//修 改 人：
//修改时间：
//备    注：

bool WINAPI DeleteAllFileW(LPCWSTR lpszPath);
#ifdef _UNICODE
#define safDeleteAllFile DeleteAllFileW
#else
#define safDeleteAllFile safDeleteAllFileA
#endif

/*--------------------------------------------------------------------------*/
BOOL WINAPI CheckRegisterComFramework(void);
HRESULT WINAPI RegisterSafToolsDLL();

BOOL WINAPI WriteXmlW(LPCWSTR lpszFullName);
#ifdef UNICODE
#define WriteXml		WriteXmlW
#else
#define WriteXml		WriteXmlA
#endif

//
//	函 数 名：GetFromModuleFullNameW
//	函数功能：输入一个文件名称，根据hInstance返回文件名称的全部路径。
int WINAPI GetFromModuleFullNameW(HINSTANCE hInstance,
									LPCWSTR lpszFileName,
									LPWSTR lpszOutFullName,
									DWORD nSize
									);
#ifdef _UNICODE
#define GetFromModuleFullName		GetFromModuleFullNameW
#else
#endif

int WINAPI FindProcessW(LPCWSTR lpszProcesName, LPPROCESSENTRY32W lpProcessEntry32);
int WINAPI FindProcessA(LPCSTR lpszProcesName, LPPROCESSENTRY32 lpProcessEntry32);
#ifdef _UNICODE                        
#define safFindProcess FindProcessW
#else
#define safFindProcess FindProcessA
#endif

int WINAPI TerminateProcessA(LPCSTR lpszProcesName, BOOL bHint);
int WINAPI TerminateProcessW(LPCWSTR lpszProcesName, BOOL bHint);
#ifdef _UNICODE
#define safTerminateProcess TerminateProcessW
#else
#define safTerminateProcess TerminateProcessA
#endif

//
// 设置COMObject错误信息。
//
//	clsid - Com Object 
//	lpszDesc - 错误信息，可以设置为空符串，也可以设置字符串在资源中的ID。 支持 %s %d 功能类似于 printf方法。
//	dwhelpID - 帮助ID,	可以设置为空。
//  lpszHelpFile -帮助的文件名称.可以设置为空。
//	iid - ComObject的 IID_ 值。
//  hRes - 字符串资源所在的文件句柄,
//	hInst - 当前的hInst.
//  ... - 支持printf方式。
HRESULT WINAPI SetComErrorInfoFormatW(HINSTANCE hInst,
										LPCOLESTR lpszDesc,
										const CLSID& clsid,
										DWORD dwHelpID,
										LPCOLESTR lpszHelpFile,
										const IID& iid,
										HRESULT hRes,...
										);
#ifdef _UNICODE
#define  FmSetComErrorInfoFormat SetComErrorInfoFormatW
#else
#define  FmSetComErrorInfoFormat SetComErrorInfoFormatW
#endif

BOOL WINAPI fk_CompareVSFixedFileInfo(const VS_FIXEDFILEINFO &vsFixedFileInfo1,
									 const VS_FIXEDFILEINFO &vsFixedfileInfo2
									 );
LONG WINAPI _FmCustomUnhandledExceptionFilter(PEXCEPTION_POINTERS pExInfo);

//	函数名称：OpenWebsite
//	功能：
//	参数：
//	返回值：
BOOL WINAPI OpenWebsite(LPCWSTR lpszUrl);

#define safOpenProc(lpszProc)  OpenWebsite(lpszProc);

//
//	函数名称：safTreeView_ExpandAllChild
//	功能：展开一个节点下的所有子节点。
//	参数：
//		hWndCtrl - TreeView控件窗口句柄
//		hTreeItem - 指定父节点
//		dwState - 指定是展开，还是关闭节点
//					TVE_COLLAPSE
//					TVE_EXPAND
//					TVE_TOGGLE
//	返回值：成功返回TRUE,失败返回FALSE，错误信息通过GetLestError函数得到。
//
BOOL WINAPI TreeViewExpandAllSibling(HWND hWndCtrl, HTREEITEM hTreeItem, UINT dwState);

enum enumShowHelp
{
	enumShowHelpLanagerFolder	=0x1,	// 载入语言目录中的帮助文件。
	enumShowHelpFullName		=0x2,	// 载入自定义帮助文件。
};

//
//	函数名称：fmShowHelp
//	参数：
//		pszHelpFileName - 指定帮助文件名称。
//		showHelp - 设置pszHelpFileName参数的帮助文件类型。
//					1.设置为enumShowHelpLanagerFolder，pszHelpFileName参数值是:FileName.chm，
//						也可以设置为空，系统将根据软件设置的语言载入帮助。
//					2.设置为enumShowHelpFullName,pszHelpFileName参数值绝对路径:c:\\Folder\FileName.chm
//						也可以是相对路径。
//	返回值：调用成功返回TREU，调用失败返回FALSE。
//
BOOL WINAPI fmShowHelp(LPCWSTR pszHelpFileName, enumShowHelp showHelp);
//
//	函数名称：FmGetErrorInfo
//	功能：获得一个错误信息。
//	参数：
//	返回值：返回IErrorInfo接口。
//
IErrorInfo* WINAPI FmGetErrorInfo(void);
IDispatch* WINAPI GetFrameworkIConfigFile();

BOOL WINAPI CheckComFrameworkFile(HWND hWnd, bool bHint);

BOOL WINAPI FileIsCharA(char pChar);
BOOL WINAPI FileIsCharW(wchar_t pChar);

#ifdef _UNICODE
#define FileIsChar	FileIsCharW
#else
#define FileIsChar	FileIsCharA
#endif


HRESULT WINAPI QueryOleWindowWnd(IDispatch *pComObjectWin, HWND &hWndCtrl);
BOOL WINAPI ImageListToImageList(HIMAGELIST hImageList, HIMAGELIST hImageListTarget);

//
// 判断一个文件夹中是否存在子文件。
// @param lpszPath 指定文件夹路径。
// @return 

#define	PECS_ERROR			0		//路径错误.
#define	PECS_EXISTS			1		//存在子文件夹路径.
#define	PECS_NOEXISTS		2		//不存在子文件夹路径.

DWORD WINAPI PathExistChildSaf(LPCWSTR lpszPath);
enum enumControlType
{
	enumControlTypeEdit=0,
	enumControlTypeListView=1,
	enumControlTypeComboBox=2,
	enumControlTypeListBox=3,
	enumControlTypeTreeView=4,
	enumControlTypeButton=5,
	enumControlTypePage=6,
};
void WINAPI C811733C_AFCB_4288_9424_6286BE1870F9W(UINT uiCtrlID, enumControlType controlType);

#define	fmSetGetDlgItemError	C811733C_AFCB_4288_9424_6286BE1870F9W

// 语言格式名称是：_T("zh-CN"),
bool WINAPI xmlfSetDefaultLangA(xmlDocPtr pDocPtr, LPCWSTR lpLang);
bool WINAPI xmlfSetDefaultLangW(xmlDocPtr pDocPtr, LPWSTR lpLang);

#ifdef _UNICODE
#define xmlfSetDefaultLang	xmlfSetDefaultLangW
#else
#define xmlfSetDefaultLang  xmlfSetDefaultLangA
#endif

typedef void*	HSHEET;
HSHEET  WINAPI FmSheetCreate();

bool  WINAPI FmSheetInserPageCLSID(HSHEET hSheet, int iIndex, REFCLSID clsid);
int   WINAPI FmSheetDoModal(HSHEET hSheet, HWND hwndParent);
void  WINAPI FmSheetClose(HSHEET hSheet);

int WINAPI LoadStringEx(HINSTANCE hInstance,
						UINT uID,
						LPTSTR lpBuffer,
						int nBufferMax,
						WORD wLanguage
						);

wchar_t* WINAPI fmGetVersionDisplay(HINSTANCE hInstance, wchar_t* szVerDisplay);


#define FMRDT_DELAY_DELETE_OBJECT_TIME			2000
typedef void(WINAPI* FNDELAYTIME)(LPVOID lpParameter);
BOOL WINAPI DelayRunFunction(int iTime, LPVOID lpParameter, FNDELAYTIME lpfnddoProc);

typedef struct tagDELAYTARGETPROCEVENT
{
	fk::NOTIFYEVENT nEvent;
	fk::LObject* pTargetProc;
	UINT uiid;
}DELAYTARGETPROCEVENT, *PDELAYTARGETPROCEVENT;
BOOL WINAPI DelayRunTargetProc(UINT uiid, int iTime, fk::LObject* pTargetProc);

struct ProgressData
{
	UINT uiMin;
	UINT uiPos;
	UINT uiMax;			
	UINT uiFactor;		// 因子
	UINT uiCount;		// 原始大小数。
};
bool WINAPI fmInitProgressData(ProgressData* ppd, UINT uiMaxCount);

#define RT_BK_WIDTH		550
#define RT_TEXT_LEFT		40
#define RT_BK_HEIGHT		70

struct _tRectText
{
	HDC hdc;
	RECT rcBk;
	RECT rcText;
	UINT uFormat;
};
LThumbnailList* WINAPI FmThumbnailListCreate(UINT nWidth, UINT nHeight);
BOOL WINAPI FmThumbnailListModifySize(LThumbnailList* ptl, UINT uiWidth, UINT uiHeight);
BOOL WINAPI FmThumbnailListDelete(LThumbnailList* ptl);

typedef void* HTHUMBNAILLISTS;

HTHUMBNAILLISTS WINAPI FmThumbnailListsCreate();

BOOL WINAPI FmThumbnailListsDelete(HTHUMBNAILLISTS htls);
BOOL WINAPI FmThumbnailListsAdd(HTHUMBNAILLISTS htls, LThumbnailList* ptl);
BOOL WINAPI FmThumbnailListsRemove(HTHUMBNAILLISTS htls, LThumbnailList* ptl);
BOOL WINAPI FmThumbnailListsModifySize(HTHUMBNAILLISTS htls, UINT uiWidth, UINT uiHeight);

bool WINAPI fmNewThreadNotifys(void** ppThreadNotifys);
bool WINAPI fmDeleteThreadNotifys(void* ppThreadNotifys);

bool WINAPI fmToolsGetObject(REFIID riid, void** ppvClass);

#define CONS_PANEL		"Panel"
#define CONS_CTRL		"Ctrl"
#define _NAME_BUFFER	30

/*
** wsSign 字符长度不能超过 20个，超过20个，fmNameCreateW会自动截断。
** wsNameBuf = wsSign + GUID前7个字符。
*/
int WINAPI fmClassNameCreateW(LPCVOID pcobj, LPWSTR wsNameBuf, UINT nMaxCount, LPCWSTR wsSign);
#ifdef UNICODE
#define fmClassNameCreate fmClassNameCreateW
#else
#define fmClassNameCreate fmClassNameCreateW
#endif
bool WINAPI fmIsSysMenuCommand(UINT uicmd);
bool WINAPI GetConfigPlatform(LPWSTR szCfgPlatform);
bool WINAPI fmNumberVariantToLong(IN const VARIANT *varItem, OUT LONG *ulValue);

#define FIZ_ZIP		0x00000001
#define FIZ_CAB		0x00000002
#define FIZ_ZIP_CAB	(FIZ_ZIP|FIZ_CAB)

dw_call FileIsZip(LPCWSTR pszFile, DWORD dwMask);



/*--------------------------------------------------------------------------*/


bool WINAPI IntCheckRange(int iMin, int iMax, int iValue);



//	函数名称：FindComFramework
//	功能：返回当前进程的IComFramework对象。
//	参数：
//		lpVariant - 目前设置为空，此值没有使用。
//	返回值：

//定义到safComFramework.dll文件中。
IDispatch* WINAPI FindComFramework(VARIANT *lpVariant);
void WINAPI SetComFramework(VARIANT *lpVariant, IDispatch *lpDispatch, void *lpUserData);
UINT WINAPI fmToolsStartup(ULONG_PTR *token);
UINT WINAPI fmToolsShutdown(ULONG_PTR *token);


b_fncall CheckFileRelationA(LPCSTR lpszExt, LPCSTR lpszAppKey);
b_fncall CheckFileRelationW(LPCWSTR lpszExt, LPCWSTR lpszAppKey);
#ifndef _UNICODE
#define CheckFileRelation		CheckFileRelationW
#else
#define CheckFileRelation		CheckFileRelationA
#endif

b_fncall RegisterFileRelationW(LPCWSTR lpszExt, LPCWSTR lpszAppName, LPCWSTR lpszAppKey, LPCWSTR lpszDefaultIcon, LPCWSTR lpszDescribe);

#ifndef _UNICODE
#define RegisterFileRelation		RegisterFileRelationW
#else
#define RegisterFileRelation		RegisterFileRelationW
#endif

/*--------------------------------------------------------------------------*/
/*
** 设置应用程序临时保存配置，以后取消这个方法。
** bSave 设置true，临时保存配置状态。设置为false是真实保存状态。
*/
void WINAPI ConfigTempSaveState(bool bSave);

/*
** 从应用配置文件中取出窗口的扩展风络，如果扩展风络不存在，默认值是 WS_EX_CLIENTEDGE 
*/
DWORD WINAPI ConfigGetWindowExStyle(LPCSTR xmlPath, DWORD dwDefExStyle);

#define xmlfSaveUserConfigTemp()   \
	ConfigTempSaveState(true);\
	SaveConfigUserFile();\
	ConfigTempSaveState(false);


#define fmSaveApplicationConfigTemp()   \
	ConfigTempSaveState(true);\
	SaveConfigApplication();\
	ConfigTempSaveState(false);

enum enumNullString
{
	enumNullStringNil,
	enumNullStringCCCC,
	enumNullStringExist
};
enumNullString WINAPI fk_IsNullString(LPCWSTR szString);

_FK_END

/*--------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif

#endif //__WINCTRL_H__
