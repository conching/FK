#ifndef __FK_XML_ITEM_HXX__
#define __FK_XML_ITEM_HXX__

_FK_BEGIN

//
// 在 xml 配置文件中读出来的，通过动态链接库中的函数创建来的对象。
//
class _FK_OUT_CLASS XmlItemRunl : public fk::LObject
{
private:
protected:
	fk::LStringw		_srunl;
	fk::LStringw		_sfunction;
	HINSTANCE			_hDllInstance;				// 载入的 DLL 句柄。
	fk::LPFN_NewObject  _lpfnNewObject;				// 调用 dll 程序中的函数句柄

public:
	XmlItemRunl(fk::LObject* pparent);
	virtual ~XmlItemRunl(void);

	virtual b_call IsRunl(void);
	virtual b_call ReadRunl(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode);
	//
	// 加载动态链接库和函数。 pxmlFile,hxmlNode这两个参数是可以设置为空，设置参数是为了出现错误，能提示出文件名称和节点路径。
	//
	virtual b_call LoadRunl(fk::LXmlFile* pxmlFile=NULL, fk::HXMLNODE hxmlNode=NULL);

	virtual b_call NewObject(fk::LObject* pparent, LPVOID* ppptr, REFIID riid=GUID_NULL);

	virtual fk::rwstring* GetRunl(void);
	virtual fk::rwstring* GetFunction(void);

	static b_fncall NewXmlItemRunl(fk::LObject* pparent, fk::XmlItemRunl** ppxmlItemRunl);
};

//
// 根据 xml 配置文件节点中的数据，创建出COM对象类。
//
class _FK_OUT_CLASS XmlItemCom  : public fk::LObject
{
private:
protected:
	fk::LGuid	_rclsid;
	DWORD		_dwClsContext;
	fk::LGuid	_riid;

public:
	XmlItemCom(fk::LObject* pparent);
	virtual ~XmlItemCom(void);

	virtual b_call IsCom(void);

	virtual b_call ReadItemCom(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode);
	virtual b_call CreateInstance(LPVOID* ppptr);
//	virtual b_call CreateInstance(fk::LAutoPtr* pAutoPtr);

	virtual fk::LGuid* _call GetRclsid(void);
	virtual dw_call GetClsContext(void);
	virtual fk::LGuid* _call GetRiid(void);

	static b_fncall NewXmlItemCom(fk::LObject* pparent, fk::XmlItemCom** ppXmlItemCom);
};

_FK_END

#endif
