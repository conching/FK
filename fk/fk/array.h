#ifndef __Array_h__
#define __Array_h__


#define  FK_DATA_END	NULL

_FK_BEGIN


typedef void* FARRAY;

static	DWORD DATA_END = 0;		// 为什么这样定义不行。


typedef bool (_fncall* FNARRAYDELETEDATA)(FARRAY farray, data_t* ppArray, UINT iCount);
/*
b_fncall fnArrayDeleteData_____(FARRAY farray, data_t* ppArray, UINT iCount)
{
	int iIndex;
	data_t* pdata;
	__data_object lpItem;

	pdata = ppArray;
	for (iIndex=0; iIndex<iCount; iIndex++)
	{
		lpItem = (__data_object)*pdata;
		delete lpItem;

		*pdata++;
	}

	return true;
}
*/



class LEnumData;


typedef struct tagArrayClass
{
	DWORD	dwSize;
	uint	uiInitCount;
	uint	uiNewCount;
	LPARAM	lpParam;
	//FNARRAYCREATEDATA fnArrayCreateData;
	FNARRAYDELETEDATA fnArrayDeleteData;
}ARRAYCLASS, *LPARRAYCLASS;


//
// 这里是数组的基本操作，和通用操作函数。
//

FARRAY _fncall ArrayCreate(LPARRAYCLASS pac);
FARRAY _fncall ArrayCreateEx(UINT uiInitCount, UINT uiNewCount, LPARAM lParam, fk::FNARRAYDELETEDATA fnArrayDeleteData);
b_fncall ArrayDestroy(FARRAY farray);
i_fncall ArrayGetCount(FARRAY farray);
data_t* ArrayGetDatas(FARRAY harray);
v_call ArrayLock(FARRAY farray);
v_call ArrayUnlock(FARRAY farray);
data_t* _fncall ArrayGetItem(FARRAY farray, UINT uiIndex);
i_fncall ArrayGetIndexFromData(FARRAY farray, data_t* pdata);

b_fncall ArraySetDeleteDataProc(FARRAY farray, fk::FNARRAYDELETEDATA fnArrayDeleteData);


b_fncall ArrayRemoveFull(FARRAY farray);
b_fncall ArrayGetEnumData(FARRAY farray, fk::LEnumData** ppEnumData);



//
// 组合方法1：指针数组。
//			只能向数组中添加指针。和数据数组护互斥。
//
b_fncall ArrayAdd(FARRAY farray, int uiCount, data_t* ppArray);
b_fncall ArrayInsert(FARRAY farray, UINT uiInsIndex, UINT uiInsCount, data_t* ppArray);
i_fncall ArrayFind(FARRAY farray, UINT uiIndex, data_t* pptr);
b_fncall ArrayRemoveData(FARRAY farray, UINT uiIndex, data_t* pptr);
b_fncall ArrayRemoveIndex(FARRAY farray, UINT uiRemoveIndex);
b_fncall ArrayRemoveRange(FARRAY farray, UINT uiRemoveIndex, UINT uiRemoveCount);


b_fncall ArrayDeleteDataFKObject(FARRAY farray, data_t* ppArray, UINT iCount);


//
// 组合方法2：数据数组。
//			只能向数组中添加数据。和指针数组护互斥。
// 目前不支持，稍后提供。
//
b_fncall ArrayAddValues(FARRAY farray, int uiCount, data_t* ppValues);
b_fncall ArrayInsertValues(FARRAY farray, UINT uiIndex, UINT uiCount, data_t* ppValues);
i_fncall ArrayFindValue(FARRAY farray, UINT uiIndex, data_t* pValue);
b_fncall ArrayRemoveValues(FARRAY farray, UINT uiIndex, data_t* ppValues, UINT uiValueSize);
//b_fncall ArrayRemoveMemoryValues(FARRAY farray, UINT uiIndex, UINT uiCount, data_t* ppValue);


#define  fk_array_cast(__class, __pdata)		((__class)*__pdata)

enum enumArrayLock
{
	umArrayLockLock = 0x1,
	umArrayLockUnLock = 0x2,
};

class LArrayLock
{
private:
protected:
	fk::FARRAY _farray;
	enumArrayLock _umArrayLock;

public:
	LArrayLock(void)
	{
		_farray = NULL;
		_umArrayLock = umArrayLockUnLock;
	}

	LArrayLock(FARRAY farray)
	{
		_farray			= farray;
		_umArrayLock	= fk::umArrayLockLock;

		fk::ArrayLock(farray);
	}

	~LArrayLock(void)
	{
		fk::ArrayUnlock(_farray);
	}

	v_call BindLock(FARRAY farray)
	{
		_farray = farray;
		_umArrayLock = umArrayLockLock;

		fk::ArrayLock(farray);
	}

	v_call lock(void)
	{
		if (_umArrayLock == fk::umArrayLockLock)
		{
			fk::ArrayLock(_farray);
			_umArrayLock = umArrayLockLock;
		}
	}

	v_call unlock(void)
	{
		if (_umArrayLock == umArrayLockUnLock)
		{
			_umArrayLock = umArrayLockUnLock;

			fk::ArrayUnlock(_farray);
		}
	}
};


class LArrayBase
{
protected:
	FARRAY _farray;
	virtual b_call Create(LPARRAYCLASS pac);

public:
	LArrayBase();
	virtual ~LArrayBase();

	virtual b_call Delete();
	virtual i_call GetCount(void);
	virtual data_t* _call GetItem(int iIndex);
	data_t* _call GetDatas();

	virtual b_call RemoveFull();
	virtual b_call ClearTrash();
};


class LArray : public LArrayBase
{
private:
public:
	LArray();
	LArray(UINT uiInitCount, UINT uiNewCount, LPARAM lParam=0, fk::FNARRAYDELETEDATA fnArrayDeleteData=NULL);
	virtual ~LArray();
	virtual b_call Create(LPARRAYCLASS pac);
	virtual b_call Create(UINT uiInitCount=10, UINT uiNewCount=10, LPARAM lParam=0, fk::FNARRAYDELETEDATA fnArrayDeleteData=NULL);

	virtual b_call Add(data_t* ppArray);
	virtual b_call Add(int uiCount, data_t* ppArray);
	virtual b_call Insert(UINT uiInsIndex, int uiInsCount, data_t* ppArray);

	//
	// virtual b_call RemoveIndex(UINT uiIndex, UINT uiCount);
	// virtual b_call DeleteIndex(UINT uiIndex, UINT uiCount);
	//

	i_fncall RemoveData(UINT uiIndex, data_t* pptr);
	b_fncall RemoveIndex(UINT uiRemoveIndex);
	i_fncall RemoveRange(UINT uiRemoveIndex, UINT uiRemoveCount);
};


class LArrayValue : public LArrayBase
{
private:
public:
	LArrayValue(void);
	virtual ~LArrayValue(void);

	virtual b_call AddValues(int uiCount, data_t* ppValues);

	virtual b_call InsertValues(UINT uiInsIndex, UINT uiInsCount, data_t* ppValues);
	virtual b_call RemoveValue(UINT uiIndex, data_t* ppArray, UINT uiArraySize);
	virtual b_call DeleteValue(UINT uiIndex, UINT uiCount, data_t* ppValue);
	virtual i_call FindValue(UINT uiIndex, data_t* pValue);
};




/*
__ClassNameImpl 类名称,是写在函数前名，在某些使用情况下，可能会和 __ClassName 设置一样。 实例：LObjectImpl::


__ClassFull 类的全称,定义参数用： fk::LObject;
__ClassNameString 不带前导字符的类名称，比如不带 C  L I 这些前导字符。  ： Object
__ClassPtr 类定义的指针参数： 实例：__ClassFull*  pObject;


1 or 2

1. FK_ARRAY_MIN_VIRTUAL_0(fk::LObject, Object, pObject);

2. FK_ARRAY_MIN_VIRTUAL(fk::LObject, Object, pObject);


.cpp
3. FK_ARRAY_MIN_VIRTUAL_CODE(fk::LObject, fk::LObject, Object, pObject);


使用方式1：
1 2 3

使用方式2：
2 3

*/


#define FK_ARRAY_MIN_VIRTUAL_0(__ClassFull, __ClassNameString, __ClassPtr)\
	virtual b_call Add##__ClassNameString(__ClassFull* __ClassPtr) = 0;\
	virtual b_call Remove##__ClassNameString(__ClassFull* __ClassPtr) = 0;\
	virtual b_call Remove##__ClassNameString(int iIndex) = 0;\
	virtual i_call GetCount(void) = 0;\
	virtual b_call GetEnumData(fk::LEnumData** pEnumData) = 0;\
	virtual __ClassFull* _call GetItem(int iIndex) = 0;\


#define FK_ARRAY_MIN_VIRTUAL(__ClassFull, __ClassNameString, __ClassPtr)\
	virtual b_call Add##__ClassNameString(__ClassFull* __ClassPtr);\
	virtual b_call Remove##__ClassNameString(__ClassFull* __ClassPtr);\
	virtual b_call Remove##__ClassNameString(int iIndex);\
	virtual i_call GetCount(void);\
	virtual b_call GetEnumData(fk::LEnumData** pEnumData);\
	virtual __ClassFull* _call GetItem(int iIndex);\



#define FK_ARRAY_MIN_VIRTUAL_CODE(__ClassNameImpl, __ClassFull, __ClassNameString, __ClassPtr)\
	vb_call __ClassNameImpl::Add##__ClassNameString(__ClassFull* __ClassPtr)\
{\
	fk_ParamVerify1(__ClassPtr==NULL, return false;);\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	if (_fArray!=NULL)\
{\
	if (fk::ArrayAdd(_fArray, 1, (data_t*)__ClassPtr))\
{\
	return true;\
}\
}\
	\
	return false;\
}\
	\
	vb_call __ClassNameImpl::Remove##__ClassNameString(__ClassFull* __ClassPtr)\
{\
	fk_ParamVerify1(__ClassPtr==NULL, return false;);\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	if (_fArray!=NULL)\
{\
	if (fk::ArrayRemoveData(_fArray, 0, (data_t*)__ClassPtr))\
{\
	return true;\
}\
}\
	\
	return false;\
}\
	\
	vb_call __ClassNameImpl::Remove##__ClassNameString(int iIndex)\
{\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	if (_fArray!=NULL)\
{\
	if (fk::ArrayRemoveIndex(_fArray, iIndex))\
{\
	return true;\
}\
}\
	\
	return false;\
}\
	\
	vi_call __ClassNameImpl::GetCount(void)\
{\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	int iCount = 0;\
	\
	if (_fArray!=NULL)\
{\
	iCount = fk::ArrayGetCount(_fArray);\
	\
	return iCount;\
}\
	\
	return -1;\
}\
	\
	\
	b_call __ClassNameImpl::GetEnumData(fk::LEnumData** pEnumData)\
{\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	int iCount = 0;\
	\
	if (_fArray!=NULL)\
{\
	if (fk::ArrayGetEnumData(_fArray, pEnumData))\
	return true;\
}\
	\
	return false;\
}\
	\
	\
	__ClassFull* vir_call __ClassNameImpl::GetItem(int iIndex)\
{\
	fk_ParamVerify1(_fArray==NULL, return false;);\
	\
	int iCount = 0;\
	data_t* pdata = NULL;\
	__ClassFull* pitem = NULL;\
	\
	if (_fArray!=NULL)\
{\
	pdata = fk::ArrayGetItem(_fArray, iIndex);\
	if (pdata!=NULL)\
{\
	pitem = (__ClassFull*)*pdata;\
	return pitem;\
}\
}\
	\
	return NULL;\
}






_FK_END


#endif /* __Array_h__ */
