//*****************************************************************************
//
//	COM Application Framework
//
//	AtlComEx.h
//
//	version: 1.0
//
//	Copyright (C) 2006-2007 Softg Studio All Rights Reserved.
//	
//	Website: http://ww.home369.net/
//
//  E-Mail: ytf1978@163.com
//
//	author: 许宗森
//
//	purpose:
//
//*****************************************************************************
#pragma  once
//#include <atlbase.h>
//#include <atlcom.h>
//#include <atlcoll.h>
//#include <ocidl.h>

#ifndef __STL_TYPE__H
//#error  WinCtrl.h requires safTools_h.h to be included first
#endif


_FK_BEGIN

// CVarTypeInfo

//#if !defined errors_h
//#define errors_h
//
////const HRESULT WZD_ERROR1 = MAKE_HRESULT(
////	1,				// 1=failure, 0=success
////	FACILITY_ITF, 	// COM errors (can also be FACILITY_WINDOWS for window errors, etc.)
////	0x0400			// user defined 0x400 and above
////	);
//
//#define E_ART_WINDOW_HANDLE		MAKE_HRESULT(1, FACILITY_ITF, 0x0401)
//#define E_ART_INDEX				MAKE_HRESULT(1, FACILITY_ITF, 0x0402)
//
////
//// MessageId: E_NO_INIT
////
//// MessageText:
////
////  当前对象需要调用某个函数进行初始化。
////
//#define E_ART_NO_INIT			MAKE_HRESULT(1, FACILITY_ITF, 0x0403)
//
//
//// MessageId: E_ART_RESOURCE
////
//// MessageText:
////
////  资源不存在。
////
////#define E_ART_RESOURCE			MAKE_HRESULT(1, FACILITY_ITF, 0x0404)
//
//
//// MessageId: E_SAF_NO_ACTIVE_ITEM
////
//// MessageText:
////
////  不存在活动项。
////
//
//#define E_SAF_NO_ACTIVE_ITEM	MAKE_HRESULT(1, FACILITY_ITF, 0x0405)
//
//#endif


#define E_ART_ERROR_BASE		0x0400


//使用在 ParseCommandLine(lpCmdLine, &hr)函数，用来判断此函数是否执行过注册。
#define E_ART_TEMP_REGSVR		MAKE_HRESULT(1, FACILITY_ITF, 0x0406)


// CVarTypeInfo

#define	VT_BASE		(73)

/*
**	使用VARIANT结构byref成员传递窗口句柄。
*/
#define		VT_HWND				(VT_BYREF|(VT_BASE+1))
#define		VT_IDLIST			(VT_BYREF|(VT_BASE+2))
#define		VT_IDLIST_FULL		(VT_BYREF|(VT_BASE+3))
#define		VT_BSTR_FULL		(VT_BYREF|(VT_BASE+4))
#define		VT_BSTR_NAME		(VT_BYREF|(VT_BASE+5))

#define		VT_PBOOL			(VT_BYREF|VT_BOOL)

/* 新的类型。 */
#define		VT_SCT_BASE			(VT_BASE+VT_BASE+200)
#define		VT_FULL_STRING		(VT_SCT_BASE + 4)
#define		VT_PARAM			(VT_SCT_BASE + 5)
#define		VT_BSTR_IDNAME		(VT_SCT_BASE + 6)


struct PROPPAGEINFOEX : public tagPROPPAGEINFO
{
	HICON hIcon;
};


//
//		m_dwTitleID = IDS_TITLEFILEINFOPROPPAGE;
//
//		hIcon = m_dwTitleID = IDS_TITLEFILEINFOPROPPAGE;
//
template <class T>
class ATL_NO_VTABLE IPropertyPageImpl : public IPropertyPage
{
public:
	void SetDirty(BOOL bDirty)
	{
		T* pT = static_cast<T*>(this);
		if (pT->m_bDirty != bDirty)
		{
			pT->m_bDirty = bDirty;
			// pT->m_pPageSite->OnStatusChange(bDirty ? PROPPAGESTATUS_DIRTY : PROPPAGESTATUS_CLEAN);
		}
	}

	IPropertyPageImpl()
	{
		T* pT = static_cast<T*>(this);
		pT->m_pPageSite = NULL;
		pT->m_size.cx = 0;
		pT->m_size.cy = 0;
		pT->m_dwTitleID = 0;
		pT->m_dwHelpFileID = 0;
		pT->m_dwDocStringID = 0;
		pT->m_dwHelpContext = 0;
		pT->m_ppUnk = NULL;
		pT->m_nObjects = 0;
		pT->m_bDirty = FALSE;
		pT->_hWnd = NULL;
	}

	~IPropertyPageImpl()
	{
		T* pT = static_cast<T*>(this);
		if (pT->m_pPageSite != NULL)
			pT->m_pPageSite->Release();

		for (UINT i = 0; i < m_nObjects; i++)
			pT->m_ppUnk[i]->Release();

		delete[] pT->m_ppUnk;
	}

	// IPropertyPage
	//
	STDMETHOD(SetPageSite)(IPropertyPageSite *pPageSite)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::SetPageSite\n"));

		if (!pPageSite && pT->m_pPageSite)
		{
			pT->m_pPageSite->Release();
			pT->m_pPageSite = NULL;
			return S_OK;
		}

		if (!pPageSite && !pT->m_pPageSite)
			return S_OK;

		if (pPageSite && pT->m_pPageSite)
		{
			ATLTRACE(atlTraceControls,2,_T("Error : setting page site again with non NULL value\n"));
			return E_UNEXPECTED;
		}

		pT->m_pPageSite = pPageSite;
		pT->m_pPageSite->AddRef();
		return S_OK;
	}
	STDMETHOD(Activate)(HWND hWndParent, LPCRECT pRect, BOOL /* bModal */)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Activate\n"));

		if (pRect == NULL)
		{
			ATLTRACE(atlTraceControls,2,_T("Error : Passed a NULL rect\n"));
			return E_POINTER;
		}

		//pT->_hWnd = pT->Create(hWndParent);
		pT->Create(hWndParent);
		::SetParent(pT->_hWnd, hWndParent);
		
		DWORD dwStyle = ::GetWindowLong(pT->_hWnd, GWL_STYLE);
		dwStyle |= WS_CHILDWINDOW;
		::SetWindowLong(pT->_hWnd, GWL_STYLE, dwStyle);
		Move(pRect);

		m_size.cx = pRect->right - pRect->left;
		m_size.cy = pRect->bottom - pRect->top;

		return S_OK;
	}
	STDMETHOD(Deactivate)( void)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Deactivate\n"));

		if (pT->_hWnd)
		{
			ATLTRACE(atlTraceControls,2,_T("Destroying Dialog\n"));
			if (::IsWindow(pT->_hWnd))
				pT->DestroyWindow();
			pT->_hWnd = NULL;
		}

		return S_OK;

	}

	//STDMETHOD(GetPageInfo)(PROPPAGEINFO *pPageInfo)
	//{
	//	PROPPAGEINFOEX *pEx = NULL;

	//	int nCbSize = sizeof(PROPPAGEINFOEX);
	//	if (pPageInfo->cb == nCbSize)
	//	{
	//		pEx = static_cast<PROPPAGEINFOEX*>(pPageInfo);
	//		pEx->hIcon = LoadIcon(g_pmodule->hRes, MAKEINTRESOURCE(m_dwTitleID));
	//		pEx->cb = nCbSize;
	//	}

	//	T* pT = static_cast<T*>(this);
	//	ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::GetPageInfo\n"));

	//	if (pPageInfo == NULL)
	//	{
	//		ATLTRACE(atlTraceControls,2,_T("Error : PROPPAGEINFO passed == NULL\n"));
	//		return E_POINTER;
	//	}

	//	//HRSRC hRsrc = FindResource(_AtlBaseModule.GetResourceInstance(),
	//	//	MAKEINTRESOURCE(static_cast<T*>(this)->IDD), RT_DIALOG);
	//	//if (hRsrc == NULL)
	//	//{
	//	//	ATLTRACE(atlTraceControls,2,_T("Could not find resource template\n"));
	//	//	return E_UNEXPECTED;
	//	//}

	//	//HGLOBAL hGlob = LoadResource(_AtlBaseModule.GetResourceInstance(), hRsrc);
	//	//DLGTEMPLATE* pDlgTempl = (DLGTEMPLATE*)LockResource(hGlob);
	//	//if (pDlgTempl == NULL)
	//	//{
	//	//	ATLTRACE(atlTraceControls,2,_T("Could not load resource template\n"));
	//	//	return E_UNEXPECTED;
	//	//}
	//	//AtlGetDialogSize(pDlgTempl, &m_size, true);

	//	pPageInfo->cb = sizeof(PROPPAGEINFO);
	//	pPageInfo->pszTitle = LoadStringHelper(pT->m_dwTitleID);
	//	pPageInfo->size = m_size;
	//	pPageInfo->pszHelpFile = LoadStringHelper(pT->m_dwHelpFileID);
	//	pPageInfo->pszDocString = LoadStringHelper(pT->m_dwDocStringID);
	//	pPageInfo->dwHelpContext = pT->m_dwHelpContext;

	//	return S_OK;
	//}

	STDMETHOD(SetObjects)(ULONG nObjects, IUnknown **ppUnk)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::SetObjects\n"));

		if (ppUnk == NULL)
			return E_POINTER;

		if (pT->m_ppUnk != NULL && pT->m_nObjects > 0)
		{
			for (UINT iObj = 0; iObj < pT->m_nObjects; iObj++)
				pT->m_ppUnk[iObj]->Release();

			delete [] pT->m_ppUnk;
		}

		pT->m_ppUnk = NULL;
		ATLTRY(pT->m_ppUnk = new IUnknown*[nObjects]);

		if (pT->m_ppUnk == NULL)
			return E_OUTOFMEMORY;

		for (UINT i = 0; i < nObjects; i++)
		{
			ppUnk[i]->AddRef();
			pT->m_ppUnk[i] = ppUnk[i];
		}

		pT->m_nObjects = nObjects;

		return S_OK;
	}
	STDMETHOD(Show)(UINT nCmdShow)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Show\n"));

		if (pT->_hWnd == NULL)
			return E_UNEXPECTED;

		ShowWindow(pT->_hWnd, nCmdShow);
		return S_OK;
	}
	STDMETHOD(Move)(LPCRECT pRect)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Move\n"));

		if (pT->_hWnd == NULL)
			return E_UNEXPECTED;

		if (pRect == NULL)
			return E_POINTER;

		MoveWindow(pT->_hWnd, pRect->left, pRect->top, pRect->right - pRect->left,
			pRect->bottom - pRect->top, TRUE);

		return S_OK;

	}
	STDMETHOD(IsPageDirty)(void)
	{
		T* pT = static_cast<T*>(this);
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::IsPageDirty\n"));
		return pT->m_bDirty ? S_OK : S_FALSE;
	}
	STDMETHOD(Apply)(void)
	{
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Apply\n"));
		return S_OK;
	}
	STDMETHOD(Help)(LPCOLESTR pszHelpDir)
	{
		T* pT = static_cast<T*>(this);
		USES_CONVERSION_EX;

		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::Help\n"));
		CComBSTR szFullFileName(pszHelpDir);
		CComHeapPtr< OLECHAR > pszFileName(LoadStringHelper(pT->m_dwHelpFileID));
		if (pszFileName == NULL)
			return E_OUTOFMEMORY;
		HRESULT hr=szFullFileName.Append(OLESTR("\\"));
		if(FAILED(hr))
		{
			return hr;
		}
		hr=szFullFileName.Append(pszFileName);
		if(FAILED(hr))
		{
			return hr;
		}
		WinHelp(pT->_hWnd, OLE2CT_EX_DEF(szFullFileName), HELP_CONTEXTPOPUP, NULL);
		return S_OK;
	}
	STDMETHOD(TranslateAccelerator)(MSG *pMsg)
	{
		ATLTRACE(atlTraceControls,2,_T("IPropertyPageImpl::TranslateAccelerator\n"));
		T* pT = static_cast<T*>(this);
		if ((pMsg->message < WM_KEYFIRST || pMsg->message > WM_KEYLAST) &&
			(pMsg->message < WM_MOUSEFIRST || pMsg->message > WM_MOUSELAST))
			return S_FALSE;

		return (IsDialogMessage(pT->_hWnd, pMsg)) ? S_OK : S_FALSE;
	}

	IPropertyPageSite* m_pPageSite;
	IUnknown** m_ppUnk;
	ULONG m_nObjects;
	SIZE m_size;
	UINT m_dwTitleID;
	UINT m_dwHelpFileID;
	UINT m_dwDocStringID;
	DWORD m_dwHelpContext;
	BOOL m_bDirty;

	//methods
public:

//	BEGIN_MSG_MAP(IPropertyPageImpl<T>)
//		MESSAGE_HANDLER(WM_STYLECHANGING, OnStyleChange)
	//END_MSG_MAP()

	LRESULT OnStyleChange(WPARAM wParam, LPARAM lParam)
	{
		if (wParam == GWL_EXSTYLE)
		{
			LPSTYLESTRUCT lpss = (LPSTYLESTRUCT) lParam;
			lpss->styleNew |= WS_EX_CONTROLPARENT;
			return 0;
		}
		return 1;
	}

	LPOLESTR LoadStringHelper(UINT idRes)
	{
		const ATLSTRINGRESOURCEIMAGE* pString = AtlGetStringResourceImage( 
			_AtlBaseModule.GetResourceInstance(), idRes);
		if (pString == NULL)
		{
			ATLTRACE(atlTraceControls,2,_T("Error : Failed to load string from res\n"));
			return NULL;
		}

		CComHeapPtr< OLECHAR > psz;

		psz.Allocate( pString->nLength+1 );
		if (psz != NULL)
		{
			Checked::memcpy_s(psz, (pString->nLength+1)*sizeof(OLECHAR), pString->achString, pString->nLength*sizeof(OLECHAR));
			psz[pString->nLength] = L'\0';
		}

		return psz.Detach();
	}
};





#if (_FK_OLD_VER<=0x0001)
// 以下代码段，不在使用。

#define VERIFY_COM(hr)			ShowErrorHResult(0, hr);

#define COM_ERROR_THROW(hr)		ShowErrorHResult(0, hr, _CRT_WIDE(__FUNCTION__)); \
	if (FAILED(hr))\
	throw(hr);

#define COM_TRY		try{

#define COM_CATCH	}catch(_com_error &com_error)\
{\
	ATLASSERT(0);\
	return com_error.Error();\
}catch (HRESULT _hr)\
{\
	ATLASSERT(0);\
	fk::ShowErrorHResult(GetActiveWindow(), hr, _CRT_WIDE(__FUNCTION__));\
	return hr;\
}

#define COM_CATCH_NO_RETURN	}catch(_com_error &com_error)\
{\
	ATLASSERT(0);\
	return;\
}catch (HRESULT _hr)\
{\
	ATLASSERT(0);\
	fk::ShowErrorHResult(GetActiveWindow(), _hr, _CRT_WIDE(__FUNCTION__));\
	return;\
}

#define COM_CATCH_BOOL	}catch(_com_error &com_error)\
{\
	ATLASSERT(0);\
	return FALSE;\
}catch (HRESULT _hr)\
{\
	ATLASSERT(0);\
	fk::ShowErrorHResult(GetActiveWindow(), _hr, _CRT_WIDE(__FUNCTION__));\
	return FALSE;\
}
//结束逐渐放弃。



template<class _T>
class CCollectionInterfaceObject
{
public:
	CCollectionInterfaceObject():
	  m_lpMainObject(NULL)
	  {

	  }
	  ~CCollectionInterfaceObject()
	  {

	  }

	  void SetCollectionInterfaceObject(_T *lpObject)
	  {
		  m_lpMainObject = lpObject;
	  }

	  _T* GetCollectionInterfaceObject()
	  {
		  return m_lpMainObject;
	  }

	  _T *m_lpMainObject;
};



template<class T, class _TDataObject>
class CCollectionDataObject
{
public:
	CCollectionDataObject()
	{
	}
	~CCollectionDataObject()
	{
	}
	void SetCollectionDataObject(_TDataObject *pDataObject)
	{
		m_pCollDataObject = pDataObject;
	}

	_TDataObject *GetCollectionDataObject()
	{
		this->m_pCollDataObject;
	}

protected:
	_TDataObject *m_pCollDataObject;
};


class CCollectionItem
{
public:
	CCollectionItem()
	{
	}
	~CCollectionItem()
	{
	}

	int CmpICollectionName(LPCTSTR lpszName)	//****
	{
		if (lpszName==NULL || m_strCollName._pwstr==NULL)
			return -1;
		return _tcsicmp(lpszName, m_strCollName.c_str());
	}

	//std::wstring m_strCollName;
	fk::LStringw	m_strCollName;
protected:
};



template<class T>
class CCollectionWndItem
{
public:
	CCollectionWndItem()	{}
	~CCollectionWndItem()	{}

	int CmpICollectionName(LPCTSTR lpszName)	//***
	{
		CComBSTR	strText;
		CWindow		wnd = ((T*)this)->m_hWnd;
		wnd.GetWindowText(&strText);

		return StrCmpI(strText, lpszName);
	}

private:

};


/*
使用方法如下:

namespace _GenericPanelsfCol
{
typedef CGenericPanel  _ClassItem;
typedef IGenericPanel  _Interface;
typedef IGenericPanels _LocalInterface;

typedef std::list<_ClassItem*> _ColType;
typedef fk::_CopyClassToInterface<_ClassItem, _Interface> _CopyVariantLong;
typedef CComEnumOnSTL<IEnumUnknown, &IID_IEnumUnknown, IUnknown*, _CopyVariantLong, _ColType> _EnumType;

typedef fk::ICollectionOnVariantSTLImpl<_LocalInterface, _ColType, _Interface, _CopyVariantLong, _EnumType> _ICollection;
};
*/

template <class T, class CollType, class ItemType, class CopyItem, class EnumType>
class ICollectionOnVariantSTLImpl : public T
{
public:
	typedef typename CollType::const_iterator  const_iterator;
	size_t GetCount()
	{
		return m_coll.size();
	}

	STDMETHOD(get_Count)(long* pcount)
	{
		if (pcount == NULL)
			return E_POINTER;
		ATLASSERT(m_coll.size()<=LONG_MAX);

		*pcount = (long)m_coll.size();

		return S_OK;
	}

	template<class _InIt>
	HRESULT GetItem(VARIANT varItem, _InIt &_Where)
	{
		LONG			nIndex		= 0;
		HRESULT			hr			= S_FALSE;

		hr = fk::AtlFromVariantInt(&varItem, &nIndex);
		if (FAILED(hr))
		{
			if (varItem.vt == VT_BSTR)
			{
				this->_FindItem(V_BSTR(&varItem), _Where);
				hr = S_OK;
			}
		}
		else
		{
			if (nIndex < 1)
				return E_INVALIDARG;
			nIndex--;

			_Where = m_coll.begin();
			std::advance(_Where, nIndex);
		}

		return hr;
	}

	STDMETHOD(get_Item)(VARIANT varItem, ItemType** ppObject)
	{
		LONG			nIndex		= 0;
		HRESULT			hr			= S_FALSE;
		CollType::iterator _Iter;

		if (ppObject == NULL)
			return E_POINTER;

		hr = fk::AtlFromVariantInt(&varItem, &nIndex);
		if (FAILED(hr))
		{
			if (varItem.vt == VT_BSTR)
			{
				this->_FindItem(V_BSTR(&varItem), _Iter);
				hr = S_OK;
			}
		}
		else
		{
			if (nIndex < 1)
				return E_INVALIDARG;
			nIndex--;

			_Iter = m_coll.begin();
			std::advance(_Iter, nIndex);
		}

		if (_Iter != m_coll.end())
		{
			hr = CopyItem::copy(ppObject, &*_Iter);
		}
		else
		{
			hr = E_INVALIDARG;
		}

		return hr;
	}

	STDMETHOD(get__NewEnum)(IUnknown** ppUnk)
	{
		if (ppUnk == NULL)
			return E_POINTER;

		*ppUnk = NULL;
		HRESULT hRes = S_OK;
		CComObject<EnumType>* p;

		hRes = CComObject<EnumType>::CreateInstance(&p);
		if (SUCCEEDED(hRes))
		{
			hRes = p->Init(this, m_coll);
			if (hRes == S_OK)
				hRes = p->QueryInterface(__uuidof(IUnknown), (void**)ppUnk);
		}

		if (hRes != S_OK)
			delete p;

		return hRes;
	}

	template<class _InIt2>
	HRESULT _FindItem(BSTR strName, _InIt2 &_Where)
	{
		_Where = m_coll.begin();
		T *pPt = (T*)this;

		for (; _Where != m_coll.end(); _Where++)
		{
			//if (StrCmpI((*_First)->_GetName().m_str, strName) == 0)
			//	break;

			// 为了兼容 CCollectionWndItem or LCollectionName 类。
			if ((*_Where)->CmpICollectionName(strName) == 0)
			//if (wcsicmp((*_Where)->m_strCollName.c_str(), strName) == 0)
			{
				return S_OK;
			}
		}

		return S_FALSE;
	}

	CollType& GetColl()
	{ 
		return this->m_coll;
	}
protected:

	CollType m_coll;
};


/*
template <class _T
,class _CollType
,class data_item_type
,class _ComObjectItem
>
*/
template <class _T							//父类
,class _CollType				//std的列表类型 std::list  std::map
,class _DataItemType			//数据项类型
,class _ComObjectItem			//COM对象类型
,class _com_interface
>
class ICollectionVariantSTLImpl : public _T
{
public:
	typedef typename _CollType::const_iterator  CollType_const_iterator;
	typedef typename _CollType::iterator		CollType_iterator;
	typedef typename _DataItemType				data_item_type;
	typedef typename _CollType					coll_Type;

	size_t GetCount()
	{
		return m_coll.size();
	}

	STDMETHOD(get_Count)(long* pcount)
	{
		ATLASSERT(pcount != NULL);
		if (pcount == NULL)
			return E_POINTER;

		ATLASSERT(m_coll.size()<=LONG_MAX);

		*pcount = (long)m_coll.size();

		return S_OK;
	}

	CollType_iterator GetItem_Iterator(VARIANT &varItem)
	{
		LONG		nIndex	= 0;
		HRESULT		hr		= S_FALSE;
		CollType_iterator iter = m_coll.end();

		hr = fk::AtlFromVariantInt(&varItem, &nIndex);
		if (FAILED(hr))
		{
			if (varItem.vt == VT_BSTR)
			{
				iter = this->FindCollectionItem(V_BSTR(&varItem));
			}
		}
		else
		{
			iter = m_coll.begin();
			std::advance(iter, nIndex);
		}

		return iter;
	}

	STDMETHOD(get_Item)(VARIANT varItem, _com_interface** ppObject)
	{
		LONG			nIndex		= 0;
		HRESULT			hr			= S_FALSE;
		CollType_iterator			_Iter;

		if (ppObject == NULL)
			return E_POINTER;

		_Iter = GetItem_Iterator(varItem);

		if (_Iter != m_coll.end())
		{
			_ComObjectItem *pComObjectItem = NULL;
			hr = fk::CreateComObject(&pComObjectItem);
			pComObjectItem->SetCollectionDataObject(*_Iter);
			*ppObject = pComObjectItem;
		}
		else
		{
			hr = E_INVALIDARG;
		}

		return hr;
	}

	STDMETHOD(get__NewEnum)(IUnknown** ppUnk)
	{
		if (ppUnk == NULL)
			return E_POINTER;

		*ppUnk = NULL;
		HRESULT hRes = S_OK;
		//CComObject<EnumType>* p;

		//hRes = CComObject<EnumType>::CreateInstance(&p);
		//if (SUCCEEDED(hRes))
		//{
		//	hRes = p->Init(this, m_coll);
		//	if (hRes == S_OK)
		//		hRes = p->QueryInterface(__uuidof(IUnknown), (void**)ppUnk);
		//}

		//if (hRes != S_OK)
		//	delete p;

		return hRes;
	}

	CollType_iterator FindCollectionItem(BSTR strName)
	{
		_CollType::iterator iter = m_coll.begin();
		_T *pPt = (_T*)this;

		for (; iter != m_coll.end(); ++iter)
		{
			//if (StrCmpI((*_First)->_GetName().m_str, strName) == 0)
			//	break;
			if (wcsicmp((*iter)->m_strCollName.c_str(), strName) == 0)
			{
				return iter;
			}
		}

		return m_coll.end();
	}

	_CollType& GetColl()
	{ 
		return this->m_coll;
	}
protected:

	_CollType m_coll;
};
//
//
//class _CopyStlString //<LPOLESTR>
//{
//public:
//	static HRESULT copy(LPOLESTR* p1, const fstd::tstring* pStr)
//	{
//		ATLASSERT(p1 != NULL);
//		ATLASSERT(pStr != NULL);
//
//		HRESULT hr = S_OK;
//		size_t len = pStr->size()+1;
//		(*p1) = SysAllocString(pStr->c_str()); //(LPOLESTR)::ATL::AtlCoTaskMemCAlloc(len, static_cast<ULONG>(sizeof(OLECHAR)));
//		if (*p1 == NULL)
//		{
//			hr = E_OUTOFMEMORY;
//		}
//		else
//		{
//			//if(!ocscpy_s(*p1, len, pStr->c_str()))
//			//{
//			//	hr = E_FAIL;
//			//}
//		}
//		return hr;
//	}
//	static void init(LPOLESTR* p) {*p = NULL;}
//	static void destroy(LPOLESTR* p) { CoTaskMemFree(*p);}
//};
//
//


template <class _FromClass, class _ToInterface>
class _CopyClassToInterface
{
public:
	static HRESULT copy(_ToInterface **p1, _FromClass * const * p2)
	{
		ATLASSERT(p1 != NULL && p2 != NULL);
		HRESULT hr = (*p2)->QueryInterface(__uuidof(_ToInterface), (void**)p1);
		return hr;
	}
	static void init(_ToInterface** ) {}
	static void destroy(_ToInterface** p) {if (*p) (*p)->Release();}

	static HRESULT copy(IUnknown **p1, _FromClass * const * p2)
	{
		ATLASSERT(p1 != NULL && p2 != NULL);
		HRESULT hr = (*p2)->QueryInterface(__uuidof(_ToInterface), (void**)p1);
		return hr;
	}
	static void destroy(IUnknown** p) {if (*p) (*p)->Release();}
};


/*
** 根据 ATL_CopyInterface 类修改。取代 _CopyClassToInterface 类。
** class T 是 Interface,  class _TClass是 Interface 的实现类。
*/
template <class T, const IID* piid, class _TClass>
class _CopyInterfaceFromClass
{
public:
	static HRESULT copy(T* * p1, _TClass * const * p2)
	{
		ATLENSURE(p1 != NULL && p2 != NULL);
		if (*p2)
			return (*p2)->QueryInterface(*piid, (void**)p1);
		return S_OK;
	}
	static void init(T** ) {}
	static void destroy(T** p) {if (*p) (*p)->Release();}
};



template<typename _TComObject>
HRESULT AtlQueryOleWindowWnd(_TComObject pComObjectWin, HWND &hWndCtrl)
{
	CComPtr<IOleWindow> pOleWindow;
	HRESULT hr = S_FALSE;

	hr = pComObjectWin->QueryInterface(IID_IOleWindow, (void**)&pOleWindow);
	if (hr == S_OK)
	{
		hr = pOleWindow->GetWindow( &hWndCtrl );
	}

	return hr;
}

HRESULT inline AtlFromVariantInt(IN const VARIANT *varItem, OUT LONG *nValue)
{
	ATLASSERT(NULL != varItem);

	switch(varItem->vt)
	{
	case VT_INT:
		{
			*nValue = varItem->intVal;
		}break;
	case VT_I4:
		{
			*nValue = varItem->lVal;
		}break;
	case VT_I8:
		{
			*nValue =(LONG)varItem->llVal;
		}break;
	case VT_UINT:
		{
			*nValue = varItem->uintVal;
		}break;
	case VT_UI4:
		{
			*nValue = varItem->ulVal;
		}break;
	case VT_UI8:
		{
			*nValue = (LONG)varItem->ullVal;
		}break;
	default:
		{
			//ATLASSERT(0);
			return E_INVALIDARG;
		}break;
	}

	return S_OK;
}


HRESULT inline AtlFromVariantComBSTR(IN const VARIANT *varItem, OUT ATL::CComBSTR &bstrValue)
{
	ATLASSERT( varItem != NULL);

	switch (varItem->vt)
	{
	case VT_BSTR:
		{
			bstrValue = varItem->bstrVal;
		}return S_OK;
	case VT_BYREF|VT_BSTR:
		{
			bstrValue = *varItem->pbstrVal;
		}return S_OK;
	case VT_LPSTR:
		{
			bstrValue = varItem->pcVal;
		}return S_OK;
	default:
		{
			ATLASSERT(0 && "AtlFromVariantComBSTR  varItem.vt error.");
		}
	}

	return S_FALSE;
}


template <class T>
inline HRESULT CreateComObject(T **ppT)
{
	ATL::CComObject<T> *pT = NULL;
	HRESULT hr = E_FAIL;

	hr = ATL::CComObject<T>::CreateInstance(&pT);
	if (hr == S_OK)
	{
		pT->AddRef();
		*ppT = pT;
		return S_OK;
	}

	return hr;
}

/// 创建一个COM对象。
///	@param		ppT 需要创建的对象。(param描述参数）     
///	@return			返回标准COM错误值。
///	@see
///	@note
template <class T>
inline HRESULT fmCreateInstance(T **ppT)
{
	ATL::CComObject<T> *pT = NULL;
	HRESULT hr;

	hr = ATL::CComObject<T>::CreateInstance(&pT);
	if (S_OK==hr)
	{
		*ppT = pT;
	}

	return hr;
}



//	功能：交换两个CString值.
inline void SwapCString(wchar_t* strText1, wchar_t* strText2)
{
	fk::LStringw strText3 = strText1;

	wcscpy(strText1, strText2);
	wcscpy(strText2, strText3.c_str());
}



// 判断命令是否包含UnregServer 和 RegServer关健字。
bool
inline
CommandLineIsRegServer(LPCTSTR lpCmdLine) throw()
{
	TCHAR szTokens[] = _T("-/");

	LPCTSTR lpszToken = ATL::CAtlModule::FindOneOf(lpCmdLine, szTokens);
	while (lpszToken != NULL)
	{
		if (ATL::CAtlModule::WordCmpI(lpszToken, _T("UnregServer"))==0)
		{
			return true;
		}

		// Register as Local Server
		if (ATL::CAtlModule::WordCmpI(lpszToken, _T("RegServer"))==0)
		{
			return true;
		}

		lpszToken = ATL::CAtlModule::FindOneOf(lpszToken, szTokens);
	}

	return false;
}


/*
* 先调用 Release() 方法之后，把一个COM对象从列表中移出。
*/
template<class _t_list, class _Ty>
inline bool FmListCloseUnknown(_t_list &_list, const _Ty& _val)
{
	_t_list::iterator _iter;

	if (_val!=NULL)	{
		_iter = std::find(_list.begin(), _list.end(), _val);
		if (_iter!=_list.end()) {
			(*_iter)->Release();
			_list.remove(_val);
			return true;
		}
		else {
			return false;
		}
	}

	return false;
}

#endif		//#if (_FK_OLD_VER<=0x0001)


_FK_END
