#ifndef __atobj_h__
#define __atobj_h__


#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */


	HRESULT WINAPI CoCreateInstancePtr(IN REFCLSID rclsid,
		IN LPUNKNOWN pUnkOuter,
		IN DWORD dwClsContext,
		IN REFIID riid,
		OUT LPVOID FAR* ppv
		);



//struct COCREATEINSTANCE
//{
//	DWORD dwSize;
//	fk::FTEXT fText;
//	//fk::LAutoPtr* pAutoPtr;
//	CLSID* rclsid;
//	LPUNKNOWN pUnkOuter;
//	DWORD dwClsContext;
//	LPIID riid;
//	void** ppv;
//
//};




#ifndef _FM_USER_ATOBJ
	#pragma comment(lib, "atobj.lib")
#endif



#ifdef __cplusplus
}
#endif
/*--------------------------------------------------------------------------*/




#endif /* __atobj_h__ */
