#ifndef __BuildObject_h__
#define __BuildObject_h__


_FK_BEGIN

// {EBFD984E-0D10-487F-A55F-C1444DF16AFB}
static const GUID IID_BuildPropItems = {0xebfd984e, 0xd10, 0x487f, { 0xa5, 0x5f, 0xc1, 0x44, 0x4d, 0xf1, 0x6a, 0xfb } };
// {1CB38898-914B-4450-952F-F1AA38F55701}
static const GUID IID_BuildObjects = { 0x1cb38898, 0x914b, 0x4450, { 0x95, 0x2f, 0xf1, 0xaa, 0x38, 0xf5, 0x57, 0x1 } };

// 
// 此类需要继承。用来创建一个属性项。
// 
// 此类临时使用，
//
class _FK_OUT_CLASS LBuildObject : public fk::LObject
{
	_FK_RTTI_

private:
	fk::LStringw	_sClassNameBuildObject;

public:
	LBuildObject(fk::LObject* pparent);
	virtual ~LBuildObject(void);

	virtual fk::LObject* CreateObjectItem(fk::LObject* pparent) = 0;
	virtual b_call DeleteObject(fk::LObject* pobject);

	// 设置当前 fk::LBuildObject 类，创建出对象所需要的类名称。
	v_call SetClassNameBuildObject(LPCWSTR lpszClassNameBuildObject);
	fk::rwstring* _call GetClassNameBuildObject(void);
};

class _FK_OUT_CLASS LBuildObjects : public fk::LObject
{
	_FK_RTTI_

private:
public:
	LBuildObjects(fk::LObject* pparent);
	virtual ~LBuildObjects(void);

	virtual i_call GetCount(void) = 0;
	virtual fk::LBuildObject* _call GetItem(int iitem) = 0;
	virtual b_call GetDataEnum(fk::LEnumData** ppEnumData) = 0;
	virtual data_t* GetDatas(void) = 0;

	virtual b_call AddBuildObject(fk::LBuildObject* pBuildObject) = 0;
	virtual b_call RemoveBuildObject(fk::LBuildObject* pBuildObject) = 0;
	virtual fk::LBuildObject* _call FindBuildObject(LPCWSTR lpszClassName) = 0;

	static b_fncall NewBuildObjects(fk::LObject* powner, LPVOID* ppobj, IID riid=GUID_NULL);
};


_FK_END

#endif
