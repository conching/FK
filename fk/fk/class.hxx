#ifndef __fk_class_h__
#define __fk_class_h__

#include "fk\array.hxx"
#include "fk\map.h"
#include "fk\guid.hxx"


_FK_BEGIN

class LNotify;
class LVxmlFile;
class LXmlFile;
class LObject;
class LObjects;
class LControl;
class LComponent;
class LModule;
class LModules;
class LSyncObject;
class LSyncObjects;
class LEnumPlus;
class LRuntimeClass;
class LRuntimeClasss;

class LDataLink;			// DataLink.hxx
class LDataSource;			// DataLink.hxx

// {DA2B176B-B4E4-439F-B88D-CE7DEC4E6287}
static const GUID IID_CLASS_OBJECT = { 0xda2b176b, 0xb4e4, 0x439f, { 0xb8, 0x8d, 0xce, 0x7d, 0xec, 0x4e, 0x62, 0x87 } };

// {E2F41EFB-A4F4-453C-B19E-304ABFCB1675}
static const GUID IID_CLASS_OBJECTSLIST = { 0xe2f41efb, 0xa4f4, 0x453c, { 0xb1, 0x9e, 0x30, 0x4a, 0xbf, 0xcb, 0x16, 0x75 } };

// {B7D98BE3-DC2D-4064-9A51-562CECF6CFBF}
static const GUID IID_CLASS_ERROR = { 0xb7d98be3, 0xdc2d, 0x4064, { 0x9a, 0x51, 0x56, 0x2c, 0xec, 0xf6, 0xcf, 0xbf } };

// {EEF458D2-80E3-4C4E-8CB8-E1832EC58322}
static const GUID IID_CLASS_XMLPROPERROR = { 0xeef458d2, 0x80e3, 0x4c4e, { 0x8c, 0xb8, 0xe1, 0x83, 0x2e, 0xc5, 0x83, 0x22 } };

// {ACB3A6F7-C580-4B96-9D04-4D1D2D80A08F}
static const GUID IID_CLASS_ENUMPLUS = { 0xacb3a6f7, 0xc580, 0x4b96, { 0x9d, 0x4, 0x4d, 0x1d, 0x2d, 0x80, 0xa0, 0x8f } };

// {587E9B91-BD01-435D-B8E2-6C3ED6A7BA5D}
static const GUID IID_CLASS_MACROPARAMSW = { 0x587e9b91, 0xbd01, 0x435d, { 0xb8, 0xe2, 0x6c, 0x3e, 0xd6, 0xa7, 0xba, 0x5d } };

extern IID IID_IObjects;				// {96CF7164-F739-4CC8-93A2-46D6DF02C4E1}

// {77DD8BED-E9F8-4E9C-856F-F970F31A4048}
static const GUID IID_CLASS_MACROPARAMSA = { 0x77dd8bed, 0xe9f8, 0x4e9c, { 0x85, 0x6f, 0xf9, 0x70, 0xf3, 0x1a, 0x40, 0x48 } };

// {F833F22E-AE5D-43F0-B10D-ADFB9EC57A05}
static const GUID IID_CLASS_FILEOPERATION = { 0xf833f22e, 0xae5d, 0x43f0, { 0xb1, 0xd, 0xad, 0xfb, 0x9e, 0xc5, 0x7a, 0x5 } };

// {EA36EA4B-77DC-4e16-BC74-7F46B1481B9C}
// 返回 fk::LObjects 对象。 safTools.dll fk::LWindowMuch 中使用。
static const GUID IID_CLASS_WINDOW_MUCH_OBJECTS = { 0xea36ea4b, 0x77dc, 0x4e16, { 0xbc, 0x74, 0x7f, 0x46, 0xb1, 0x48, 0x1b, 0x9c } };

extern IID IID_IComponent;			// {0CFF5515-186E-42B4-81C7-35BF67C6FD93}

// {D8AEBD12-9FCD-460f-895D-EAAD176726F1}
static const IID IID_IModules = { 0xd8aebd12, 0x9fcd, 0x460f, { 0x89, 0x5d, 0xea, 0xad, 0x17, 0x67, 0x26, 0xf1 } };

// {9FFDAC1B-BBA4-4faa-9DA6-A3CE8381A1EB}
extern IID IID_IEnumData;

//
// FK 使用的全局事件，使用在不同的类中，发送给当前类的过程。 此 ID 从 fk::LObject 向子类有作用。
//
enum enumNotifyEvent
{
	ON_ADD_OBJECT		= 0x81000000,		// 添加一个对象。
	ON_REMOVE_OBJECT	= 0x82000000,		// 移出一个对象。
	ON_LOAD_VXML_CTRL	= 0x83000000,		// 在创建窗口时，没有设置 vxml 文件。手动调用 LoadVxmlFileCtrl 方法，
											// 会发向当前窗口过程发送 ON_LOAD_VXML_CTRL 消息。不存在消息参数。
};

//
// FK 使用的全局事件，使用在不同的类中，发送给当前类的过程。 此 ID 从 fk::LWindow 向子类有作用。
//
enum enumObjectEvent
{
	ON_DataField		= 0x80001000,		// 设置或者是获得字段对象。
	ON_DataSource		= 0x80002000,		// 设置或者是获得数据源对象。
	ON_DataSet			= 0x80004000,		// 设置或者是获得数据表对象。
	ON_DataConnection	= 0x80008000,		// 设置或者是获得数据链接对象。
};

enum enumPROPIDWindow
{
	PROPID_DataField		= 2001,		// 设置或者是获得字段对象。
	PROPID_DataSource		= 2002,		// 设置或者是获得数据源对象。
	PROPID_DataSet			= 2003,		// 设置或者是获得数据表对象。
	PROPID_DataConnection	= 2004,		// 设置或者是获得数据链接对象。
};

///////////////////////////////////////////////////////////////////////////////////////////////////
class _FK_OUT_CLASS LRuntimeClass
{
private:
	fk::LPFN_NewObject  _lpfnNewObject;

public:
	LRuntimeClass(void);
	virtual ~LRuntimeClass(void);

	enum enumRuntimeClass
	{
		rcfNewObjectStandard	= 0x00000001,   // 使用标准函数创建类对象。
		rcfNewObjectShape		= 0x00000002,   // 创建类的对象，不是标准函数。
		rcfIsEndRuntimeClass	= 0x00000004,	// 当前 RuntimeClss 对象是最后一个对象。
	};

	fk::LStringw    _sClassName;				// 类的名称。
	DWORD           _dwMake;					//
	LRuntimeClass*  _pRuntimeClassNext;			// 指向上一个类的 RTTI.

#ifdef _DEBUG
	//
	// 最后一个类调用的函数，设置当前 LRuntimeClass 对象已经是最后一个。在调试状态使用。
	//
	v_call SetRuntimeClass();
	b_call IsEndRuntimeClass(void);
#endif
};

//
// 这里不知道如何命名这几个函数。
//
// 1 在类中定义 _FK_RTTI_ 宏。
//
//static fk::LRuntimeClass* _call CreateRuntimeClassThis___(LPCSTR lpszClassName); // 创建当前类的 fk::LRuntimeClass 对象。
//static fk::LRuntimeClass* _fncall GetThisRuntimeClass(void);\		// 使用静态方法，获得当前类的 LRuntimeClass 方法。

																	// 在单子类状态下，这两个函数返回的值一样。
//static fk::LRuntimeClass* _fncall GetBaseRuntimeClass(void);\		// 使用静态方法，获得当前类的基类 LRuntimeClass 方法。
//static fk::LRuntimeClass* _fncall GetEndRuntimeClassImpl(void);\	// 使用静态方法，获得最后一个的 LRuntimeClass 方法。

//fk::LRuntimeClass* _call GetCurrentRuntimeClass(void);\			// 使用成员函数，获得当前类的 LRuntimeClass 方法。

																	// 在单子类状态，这两个函数返回的值一样。
//virtual fk::LRuntimeClass* _call GetRuntimeClass(void);\			// 使用成员函数，获得最后一个类的 LRuntimeClass 方法。	
//virtual fk::LRuntimeClass* _call GetRuntimeClassImpl(void);		// 使用成员函数，获得最后一个类的实现 LRuntimeClass 方法。
//

#define _FK_RTTI_ \
public:\
	static fk::LRuntimeClass* _call CreateRuntimeClassThis___(LPCSTR lpszClassName);\
	static fk::LRuntimeClass* _fncall GetThisRuntimeClass(void);\
	static fk::LRuntimeClass* _fncall GetBaseRuntimeClass(void);\
	static fk::LRuntimeClass* _fncall GetEndRuntimeClassImpl(void);\
	fk::LRuntimeClass* _call GetCurrentRuntimeClass(void);\
	virtual fk::LRuntimeClass* _call GetRuntimeClass(void);\
	virtual fk::LRuntimeClass* _call GetRuntimeClassImpl(void) const ;

// 这个宏可能不需要在代码中使用。共享宏，不直接写在代码中。
#define __FK_RTTI_CODE_USER__(_CLASS_NAME_, _BASE_CLASS_NAME_, _CLASS_NAME_VAR_)\
fk::LRuntimeClass* g_pRuntimeClassObject##_CLASS_NAME_VAR_ = NULL;\
	fk::LRuntimeClass* _fncall _CLASS_NAME_::GetThisRuntimeClass(void)\
{\
	if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
		_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
	return g_pRuntimeClassObject##_CLASS_NAME_VAR_;\
}\
fk::LRuntimeClass* _fncall _CLASS_NAME_::GetBaseRuntimeClass(void)\
{\
	if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
		_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		\
	return g_pRuntimeClassObject##_CLASS_NAME_VAR_->_pRuntimeClassNext;\
}\
\
fk::LRuntimeClass* vir_call _CLASS_NAME_::GetRuntimeClass(void)\
{\
	if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
		_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
	return g_pRuntimeClassObject##_CLASS_NAME_VAR_;\
}\
fk::LRuntimeClass* _call _CLASS_NAME_::GetCurrentRuntimeClass(void)\
{\
	if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
		_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
	return g_pRuntimeClassObject##_CLASS_NAME_VAR_;\
}\
fk::LRuntimeClass* _call _CLASS_NAME_::CreateRuntimeClassThis___(LPCSTR lpszClassName) \
{\
fk::LRuntimeClasss*	pRuntimeClasss; \
fk::LRuntimeClass*	pRuntimeClassThis; \
fk::LRuntimeClass*	pRuntimeBaseClass; \
fk::LApplicationInfo* pai; \
\
if (g_pRuntimeClassObject##_CLASS_NAME_VAR_ == NULL) {\
	\
		pai = fk::GetApplicationInfo(); \
		if (pai != NULL)\
		{\
			pRuntimeClasss = pai->GetRuntimeClasss(); \
			g_pRuntimeClassObject##_CLASS_NAME_VAR_ = pRuntimeClasss->CreateRuntimeClass(lpszClassName); \
			\
			pRuntimeBaseClass = _BASE_CLASS_NAME_::GetThisRuntimeClass(); \
			g_pRuntimeClassObject##_CLASS_NAME_VAR_->_pRuntimeClassNext = pRuntimeBaseClass; \
		}\
	}\
	return g_pRuntimeClassObject##_CLASS_NAME_VAR_; \
}

/* 2. 子类宏，写在代码中。
_CLASS_NAME_			当前类名，有域名带上域名。
_BASE_CLASS_NAME_		父类类名，有域名带上域名。 
_CLASS_NAME_VAR_		当前类名，不能带上 namespace 。因为会做为变量名称使用。

.h 文件中写 _FK_RTTI_(fk::LUserObject) 带上区域名称。
.cpp 文件中写 _FK_RTTI_CODE_(fk::LUserObject, fk::LObject, LUserObject);
*/
#define _FK_RTTI_CODE_(_CLASS_NAME_, _BASE_CLASS_NAME_, _CLASS_NAME_VAR_) \
	__FK_RTTI_CODE_USER__(_CLASS_NAME_, _BASE_CLASS_NAME_, _CLASS_NAME_VAR_) \
	fk::LRuntimeClass* _call _CLASS_NAME_::GetEndRuntimeClassImpl(void)\
	{\
		if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
			_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		return g_pRuntimeClassObject##_CLASS_NAME_VAR_;\
	}\
	fk::LRuntimeClass* vir_call _CLASS_NAME_::GetRuntimeClassImpl(void) const \
	{\
		if (g_pRuntimeClassObject##_CLASS_NAME_VAR_==NULL)\
			_CLASS_NAME_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		return g_pRuntimeClassObject##_CLASS_NAME_VAR_;\
	}

/*
_CLASS_NAME_IMPL_		当前类实现类名，有域名带上域名。
_CLASS_NAME_			当前类名，有域名带上域名。
_BASE_CLASS_NAME_		基类类名，有域名带上域名。 

_CLASS_NAME_IMPL_VAR_	当前类实现类名，不能带上 namespace 。 因为会做为变量名称使用。
_CLASS_NAME_VAR_		当前类名，不能带上 namespace 。 因为会做为变量名称使用。

.h 文件中写 _FK_RTTI_(fk::LUserObject) 带上区域名称。
.cpp 文件中写 _FK_RTTI_CODE_(fk::LUserObject, fk::LObject, LUserObject);
*/
#define _FK_RTTI_CODE2_(_CLASS_NAME_IMPL_, _CLASS_NAME_, _BASE_CLASS_NAME_, _CLASS_NAME_IMPL_VAR_, _CLASS_NAME_VAR_)\
	__FK_RTTI_CODE_USER__(_CLASS_NAME_, _BASE_CLASS_NAME_, _CLASS_NAME_VAR_)\
	__FK_RTTI_CODE_USER__(_CLASS_NAME_IMPL_, _CLASS_NAME_, _CLASS_NAME_IMPL_VAR_)\
	fk::LRuntimeClass* _call _CLASS_NAME_::GetEndRuntimeClassImpl(void)\
	{\
		if (g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_==NULL)\
			_CLASS_NAME_IMPL_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		return g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_;\
	}\
	fk::LRuntimeClass* vir_call _CLASS_NAME_::GetRuntimeClassImpl(void) const \
	{\
		if (g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_==NULL)\
			_CLASS_NAME_IMPL_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		return g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_;\
	}\
	fk::LRuntimeClass* vir_call _CLASS_NAME_IMPL_::GetRuntimeClassImpl(void) const \
	{\
		if (g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_==NULL)\
			_CLASS_NAME_IMPL_::CreateRuntimeClassThis___(#_CLASS_NAME_);\
		return g_pRuntimeClassObject##_CLASS_NAME_IMPL_VAR_;\
	}

// _FK_RTTI_INIT_ 不存使用。
#define _FK_RTTI_INIT_(_CLASS_NAME_)   //CreateRuntimeClassThis___(#_CLASS_NAME_);

typedef struct tagLINK_CONTROL_DATA
{
	DWORD				dwSize;
	DWORD				dwMask;
	fk::LModule*		pmodule;
	LPWSTR				lpszVarName;
	fk::HXMLNODE		hXmlNode;
	fk::LVxmlFile*		pVxmlFile;
}LINK_CONTROL_DATA, *LPLINK_CONTROL_DATA;

typedef struct tagObjectInfo
{
	fk::LPFN_NewObject fnNewObject;
	fk::LObject*	powner;
	IID*			pguid;
	fk::LObject**	ppoutObj;
	wchar_t*		lpszVar;
}ObjectInfo, *LPObjectInfo;

class _FK_OUT_CLASS LObject : public IUnknown
{
	_FK_RTTI_

private:
	fk::FARRAY			_farray;				// 没有名子和有名子的对象，子对象列表，调用 AddObject RemoveObject 方法。
												// _pobjects

	b_call CreateDataLink(void);				//

protected:
	long*				_pParam;
	DWORD				_dwObjectMask;			// 对象状态信息项
	long				_dwRef;					// 对象引用基数
	GUID*				_glink;
	fk::LNotify*		_pNotify;				// 对象对外提供的数据信息
	fk::LSyncObjects*	_pSyncObjects;			// 对象同步操作功能
	fk::LObject*		_pParentObject;			// 对象的宿主对象
	fk::LObjects*		_pobjects;				// 当前对象中的不同类型列表。
	fk::LDataLink*		_pDataLink;				// 让一个对象具有数据链接的功能。此对象在需要时创建，不需要时会删除。
	fk::LStringw		_sClassVar;				// 当类的变量名称。

	fk::FRUNINFO		_fRunInfo;
	//fk::LStringw		_sCurrentClassName;

	b_call ObjectRelease(long* plRef);
	v_call ReleaseObjects(void);

public:			// = 0;

	// 这三个函数是属性调用的.
	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValueIn,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	virtual b_call AddObject(fk::LObject* pobject);				// 添加一个子对象。
	virtual b_call RemoveObject(fk::LObject* pobject);			// 移出一个子对象。

	virtual v_call DebugOutputText(DWORD dwtype, DWORD dwmake);

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual b_call IsClassType(LPGUID pClassIID);
	virtual b_call IsClassType(LPCWSTR lpszClassNmae);
	b_call IsKindOf(const fk::LRuntimeClass* pRuntimeClass) const ;

	virtual fk::LStringw& _call GetClassName(void);
	b_call ModifyObject(DWORD dwRemoveMask, DWORD dwAddMask);

public:
	enum umObjectMake
	{
		ofAutoRelease		= 0x00000001,		// 默认等于 0 时，调用 Release 方法删除对象。
		ofLockDelete		= 0x00000002,		// 默认等于 0 时，调用 Release 方法不删除对象。
		ofCreateNotify		= 0x00000004,		// 创建 fk::LNotify 对象。
		ofCreateObjects		= 0x00000008,		// 创建 fk::LObjects 对象。
		ofUpdateVariable	= 0x00000010,		// 变量更新到 CPP or .h 文件中。

		//
		// 在 fk::LObject 类的构造函数中，创建 fk::LObjects fk::LNotify 对象，会产生无限递归。
		// 使用 LObject(fk::LObject* pParentObject, DWORD dwObjectMask) 函数，传入 
		// ofNotCreateObjects 参数，在构造函数中，不创建他们几个对象。
		//
		ofNotCreateObjects	= 0x00000020,		

		ofGlobal			= 0x00000040,		// 对象是全局，可以通过对象路径进行获得。
	};

public:
	LObject(fk::LObject* pParentObject);
	LObject(fk::LObject* pParentObject, DWORD dwObjectMask);
	virtual ~LObject();

	_FK_DBGCLASSINFO_

	fk::LStringw	_sChildClassName;			                        // 最后一个子类名。

	dw_call GetObjectMask(void);

	virtual ULONG STDMETHODCALLTYPE AddRef();
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	int GetRef(void);

	virtual STDMETHODIMP QueryInterface(LPCWSTR lpszFuncation, LPCWSTR lpszLine, REFIID riid, void** ppv);

	virtual b_call SetParentObject(fk::LObject* pParentObject);			// 设置父对象。
	virtual b_call GetParentObject(REFIID riid, void** ppv);			// 获得父对象的，某个接口指针。 TODO:del
	virtual fk::LObjects* _call GetObjects(void);

	virtual b_call Assign(LObject* pobject);
	virtual v_call clear(void);
	virtual b_call create(void);
	virtual b_call CheckSuccess(void);
	virtual b_call DeleteChild(fk::LObject* pChildObject);
	virtual fk::LNotify* _call GetNotify(void);
	virtual b_call LinkControlDataXml(fk::LPLINK_CONTROL_DATA* plcd);
	virtual b_call IsWindow(void);
	virtual b_call SetObjectCaption(LPCWSTR lpszValue);

	b_call CreateObjectArray(fk::LPObjectInfo parray, int icount _FK_OBJECT_POS_H2_);

	// 设置和获得对象的名子。
	fk::LStringw* _call GetObjectVar(void);
	v_call SetObjectVar(LPCWSTR lpszVar);

	b_call AddSyncObject(fk::LSyncObject* pSyncObject);
	b_call RemoveSyncObject(fk::LSyncObject* pSyncObject);
	fk::LSyncObjects* _call GetSyncObjects(void);

	b_call SyncLock(DWORD dwTimeout = INFINITE);
	b_call SyncUnlock();
	virtual b_call SyncWait(LPARAM lparam);

	b_call IsUpdateVariable(void);
	v_call SetUpdateVariableState(bool bUpdate);

	b_call SetDataSource(fk::LDataSource* pDataSource);
	b_call SetField(fk::LField* pField);
	b_call SetFieldName(LPCWSTR lpszFieldName);

	virtual b_call WriteCtrlDataXml(fk::HXMLNODE hXmlParentNode);
};


#ifdef _DEBUG
#define fkQueryInterface(_CallObject, hr, riid, ppobjOut, _ReturnCode)	\
				hr = _CallObject->QueryInterface(riid, (void**)ppobjOut);
#else
#define fkQueryInterface(_CallObject, hr, riid, ppobjOut, _ReturnCode)	\
				hr = _CallObject->QueryInterface(riid, (void**)ppobjOut);
#endif


class _FK_OUT_CLASS LRuntimeClasss : public fk::LObject
{
private:
public:
	LRuntimeClasss(fk::LObject* pParentObject);
	virtual ~LRuntimeClasss(void);

	virtual fk::LRuntimeClass* _call CreateRuntimeClass(LPCSTR lpszClassName) = 0;

	static b_call NewRuntimeClasss(fk::LObject* pparent, LPVOID* ppv, REFIID riid=GUID_NULL);
};

///////////////////////////////////////////////////////////////////////////////////////////////////
enum enumSyncLock
{
	umSyncLock = 0x1,
	umSyncUnlock = 0x2,
};

enum enumSyncObject
{
	umSyncObjectNull			= 0x0,
	umSyncObjectCreateSection	= 0x1,

	umSyncObjectUser			= 0x100,			// 用户自定义同步类型。
};

class _FK_OUT_CLASS  LSyncObject : public LObject
{
private:
protected:
	enumSyncObject _umSyncObject;

public:
	virtual b_call lock(DWORD dwTimeout = INFINITE) = 0;
	virtual b_call unlock() = 0;
	virtual b_call wait(LPARAM lparam=0) = 0;
	virtual vp_call GetfSyncObject(void) = 0;			// 可能是CRITICAL_SECTION  Event 等。

public:
	LSyncObject(enumSyncObject umSyncObject);
	virtual ~LSyncObject(void);

	enumSyncObject _call GetSyncObjectType(void);
};

// 管理一个同步对象列表。
//
class _FK_OUT_CLASS LSyncObjects
{
private:
	fk::FARRAY _farray;

public:
	LSyncObjects();
	virtual ~LSyncObjects();

	virtual b_call lock(DWORD dwTimeout = INFINITE);
	virtual b_call unlock();
	virtual b_call wait(LPARAM lparam);

	i_call GetCount(void);
	data_t* GetDatas(void);

	b_call AddSynObject(fk::LSyncObject* psyncObject);
	b_call RemoveSyncObject(fk::LSyncObject* psyncObject);
};

class _FK_OUT_CLASS LCriticalSection : public LSyncObject
{
protected:
public:
	LCriticalSection();
	virtual ~LCriticalSection();

	CRITICAL_SECTION _sec;

	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call lock(DWORD dwTimeout = INFINITE);
	virtual b_call unlock();
	virtual b_call wait(LPARAM lparam=0);
	virtual vp_call GetfSyncObject(void);							// 可能是CRITICAL_SECTION  Event 等。

	//operator CRITICAL_SECTION*()   { return &_sec; }

	static LCriticalSection* _call NewCriticalSection(void);
};

class _FK_OUT_CLASS LAutoCriticalSection
{
private:
	LCriticalSection* _psec;
	enumSyncLock _umSyncLock;

public:
	LAutoCriticalSection(void);
	LAutoCriticalSection(LCriticalSection* psec, bool block);
	LAutoCriticalSection(LCriticalSection* psec, enumSyncLock umSyncLock=umSyncLock);
	virtual ~LAutoCriticalSection();

	v_call BindLock(LCriticalSection* psec);
};

//
// 自动调用和 fk::LObject 对象中的 SyncLock 和 SyncUnlock
// 使用方式：
// fk::AutoSyncLock  asl;
// asl.bind(pboject);
//
class _FK_OUT_CLASS AutoSyncLock
{
private:
	fk::LObject* _pobject;
	bool _block;

protected:
public:
	AutoSyncLock(void);
	virtual ~AutoSyncLock(void);

	b_call bind(fk::LObject* pobject);
	v_call unbind(void);
};

class _FK_OUT_CLASS LControl : public fk::LObject
{
	_FK_RTTI_;

private:
public:
	LControl(fk::LObject* pparent);
	virtual ~LControl();
};

class LBaseBuildUI;
class _FK_OUT_CLASS LModule : public fk::LObject
{
	_FK_RTTI_
private:

public:

//#ifdef _FK_OLD_VER
	wchar_t*			pszResPath;
	wchar_t*			pszFileUI;
	LBaseBuildUI*	pBaseBuildUI;				// 这个pBaseBuildUI量以后会废除。
	fkCommandRange	cmdRange;					// 当前 .exe or .dll 文件使用的命令区域。 这个量以后也废除。
//#endif

public:
//	UINT			cbSize;						// 以后使用 GetModuleXXXXXX 方法，访问数据成员。
	DWORD			dwMask;
	HINSTANCE		hInst;
	HINSTANCE		hRes;
	DWORD			dwDebugMake;
	fk::LStringw	sModuleFullName;
	fk::FN_REPAIR_PROCESS_COM	fnGetDllDnaXmlFile;

	LModule(void);
	virtual ~LModule(void);

	virtual b_call GetModuleMacroName(fk::LVar* pBaseData) = 0;
	virtual b_call SetModuleMacroName(LPCWSTR lpszMacroName) = 0;						// 这个函数只能调用一次，设置宏名后，就不会在进行修改。
	virtual b_call GetObjectPtr(LPCWSTR lpszObjectKey, fk::LObject** ppobj) = 0;		// 根据对象键，获得对象指针。

	virtual fk::rastring* _call GetXmlPath(void) = 0;
	virtual b_call SetRootXmlPath(LPCSTR lpszRootXmlPath) = 0;							// 此函数只能调用一次。
	virtual b_call BuildXmlPath(fk::LStringa* pcXmlPathItem, LPCSTR lpszPathPart) = 0;	// 设置的根路径加上lpszPathPart参设置的路径节，生成新的完整路径。

	virtual fk::rwstring* _call GetKeyName(void) = 0;			// 返回当前模块的 key 名称。
};
//
// 初始化一个 module 名称，这个函数和参数很重要。
// hIsnt - 执行库的句柄。
// lpszKeyName - module 的名称的 key，当前进程唯一值，需要使用 KeyName 找到 module 对象。如果设置为空，就使用执行库的名称。
//
b_fncall InitializeModule(fk::LModule** ppModule, HINSTANCE hInst, LPCWSTR lpszKeyName=NULL);
b_fncall UninitializeModule(fk::LModule** ppModule);

///////////////////////////////////////////////////////////////////////////////////////////////////
class _FK_OUT_CLASS LModules : public fk::LObject
{
private:
public:
	LModules(void);
	virtual ~LModules(void);

	virtual b_call AddModule(fk::LModule* pmodule) = 0;
	virtual b_call RemoveModule(fk::LModule* pmodule) = 0;
	virtual v_call clear(void) = 0;

	virtual i_call GetCount(void) = 0;
	virtual fk::LModule* _call GetItem(int iitem) = 0;
	virtual data_t* _call GetDatas(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;

	virtual b_call FindModule(HINSTANCE hinst, fk::LModule** ppmodule) = 0;
	virtual b_call FindModule(LPCWSTR lpszkey, fk::LModule** ppmodule) = 0;

	virtual b_call GetObjectPtr(LPCWSTR lpszObjectKey, fk::LObject** ppobj) = 0;		// 根据对象键，获得对象指针。

	static fk::LModules* _call NewModules(void);

public:
	enum enumNotify
	{
		ON_UPDATE	= 0x00000001,	// 发送更新数据信息。
		ON_ADD		= 0x00000002,	// 添加一个Module 对象。
		ON_REMOVE	= 0x00000004,	// 移出一个Module 对象。
	};

	typedef struct tagUpdate
	{
		fk::NOTIFYEVENT ne;
		fk::LModules* pmodules;
	}UPDATE,*LPUPDATE;

	typedef struct tagAdd
	{
		fk::NOTIFYEVENT ne;
		fk::LModules* pmodules;
		fk::LModule* pmodule;
	}ADD,*LPADD;

	typedef struct tagRemove
	{
		fk::NOTIFYEVENT ne;
		fk::LModules* pmodules;
		fk::LModule*	pmodule;
	}REMOVE,*LPREMOVE;
};
b_fncall GetModules(fk::LModules** ppmodules);									// 获得 fk 的全局 fk::LModules 变量。
b_fncall GetModulesObjectPtr(LPCWSTR lpszObjectKey, fk::LObject** ppobj);		// 根据对象键，获得对象指针。
fk::LModule* _fncall GetMainModule(void);
HINSTANCE _fncall GetMainModuleInst(void);
HWND _fncall GetMainWnd(void);
b_fncall SetMainWnd(HWND hMainWnd);				// 代码在 App.cxx 文件中。


/*
** 功能：返回应用程序的的信息 tag_ApplicationInfo
*/

enum enumApplicationInfoMask
{
	AIF_NULL = 0x00000000,
	AIF_FLASHWINDOW_SHOW = 0x00000001,
	AIF_CREATE_CONFIGFILE_APPLICATION = 0x00000002,	// 程序创建pConfigFileUser对象。
	AIF_CREATE_CONFIGFILE_USER = 0x00000004,		// 程序创建pConfigFileUser对象。
	AIF_CREATE_PROCESS_MACRO = 0x00000008,			// 程序创建pProcessMacro对象。
	AIF_ANZHUANGBAN = 0x00000020,					// 存在这个标记是安装版版本。
};

class _FK_OUT_CLASS LApplicationInfo : public fk::LObject
{
private:
protected:
public:
	LApplicationInfo(fk::LObject* pparent);
	virtual ~LApplicationInfo(void);

	DWORD	dwSize;
	DWORD	dwMask;
	LPWSTR	szSoftwareName;					// 软件名称
	LPWSTR	szLanguage;
	FTEXT	fText;							// 日志句柄
	LONG	nFlashWindowTimer;				// Flash Windows显示多少时间后，自动关闭.
	DWORD	dwFlashWindowStyle;				//

	IConfigFile*	pConfigFileApp;		// 应用程序配置文件操作对象			以后移除
	IConfigFile*	pConfigFileUser;		// 应用程序用户保存的信息操作对象	以后移除
	IProcessMacro*	pProcessMacro;			// 应用的宏记忆操作对象				以后移除
	void*			pUserLParam;

	virtual fk::LRuntimeClasss* _call GetRuntimeClasss(void) = 0;

public:
	enum enumNotify
	{
		OnBeforeOpenConfig	= 0x00000001,		// 打开 .pxml .uxml 配置文件已经打开完毕。
		OnAfterCloseConfig	= 0x00000002,		// 关闭 .pxml .uxml 配置文件之前。
		OnBeforeInitModule	= 0x00000004,		// 创建初始化 fk::Module 结束后。
		OnAfterCloseModule	= 0x00000008,		// 关闭 fk::Module 之前。
		OnCreateObjects		= 0x00000010,		// 创建一个 fk::LObjects 对象。
		OnDeleteObjects		= 0x00000020,		// 删除一个 fk::LObjects 对象。
		OnCreateWindowMuch	= 0x00000040,
		OnDeleteWindowMuch	= 0x00000080,
	};
	typedef struct tagBeforeInitModule
	{
		fk::NOTIFYEVENT	ne;
		fk::LModule* pmodule;
	}BeforeInitModule,*LPBeforeInitModule;
	typedef struct tagAfterCloseModule
	{
		fk::NOTIFYEVENT	ne;
		fk::LModule* pmodule;
	}AfterCloseModule,*LPAfterCloseModule;
	typedef struct tagOnCreateObjects
	{
		fk::NOTIFYEVENT	ne;
		fk::LObjects* pobjects;
	}ONCreateObjects,*LPOnCreateObjects;
	typedef struct tagOnDeleteObjects
	{
		fk::NOTIFYEVENT	ne;
		fk::LObjects* pobjects;
	}ONDeleteObjects,*LPOnDeleteObjects;
};
fk::LApplicationInfo* WINAPI GetApplicationInfo();

///////////////////////////////////////////////////////////////////////////////////////////////////
class _FK_OUT_CLASS LEnumData : public fk::LObject
{
public:
	virtual b_call Next(ULONG celt, data_t** rgelt, ULONG *pceltFetched) = 0;
	virtual b_call Skip(ULONG celt) = 0;
	virtual b_call Reset(void) = 0;
	virtual b_call Clone(LEnumData** ppenum) = 0;

public:
	LEnumData(void);
	virtual ~LEnumData(void);
};
// {9FFDAC1B-BBA4-4faa-9DA6-A3CE8381A1EB}
extern IID IID_IEnumData;


//
// 标记配置文件的数据信息。
//
class _FK_OUT_CLASS LConfigHead : public fk::LObject
{
private:
public:
	DWORD			_dwHeadMake;
	fk::LStringw	_sModuleKey;
	fk::LGuid		_ConfigFileGuid;

	LConfigHead(void);
	virtual ~LConfigHead(void);

	virtual b_call ReadConfigHead(fk::LXmlFile* pxmlFile) = 0;

	static LConfigHead* _call NewConfigHead(void);
};


///////////////////////////////////////////////////////////////////////////////////////////////////
//
// 这里是注册一种新的数据类型,这个功能以后会取消。
//
typedef LVar* (_fncall* LPFN_CREATEEDATATYPE)(int iCount);
typedef void (_fncall* LPFN_UNCREATEEDATATYPE)(LVar* pBaseData);
typedef struct tagREGISTERBASEDATA
{
	DWORD						dwSize;
	LPFN_CREATEEDATATYPE		lpfnCreateDataType;
	LPFN_UNCREATEEDATATYPE		lpfnUnCreateDataType;
}REGISTERBASEDATA,* LPREGISTERBASEDATA;
i_fncall BaseDataRegisterType(LPREGISTERBASEDATA lpRegsvrBaseData);
b_fncall BaseDataFindItem(INT iType, LPREGISTERBASEDATA lpRegBaseData);

///////////////////////////////////////////////////////////////////////////////////////////////////
enum enumAutoPtrType
{
	APT_UNKNOWN			=0x1,
	APT_INMALLOC		=0x2,			// 内部使用 malloc 分配置的。
	APT_OBJECT			=0x3,
	APT_INBSTR			=0x5,
	APT_VARIANT			=0x6,
	APT_FKMALLOC		=0x7,			// 外部和内部使用 FkMalloc 分配置的内存。
	APT_OUTMALLOC		=0x8,			// 外面使用 malloc 分配置的内存。 使用 AddChar AddWchar
	APT_BASE			=0x9,
	APT_BASEDATA		=0x10,
	ART_DESTRUCTOR		=0x11,
	APT_GUID			=0x12,
	APT_PITEMIDLIST		=0x13,
	APT_OUTBSTR			=0x14,
	APT_LOCALALLOC		=0X15,
	APT_GLOBALALLOC		=0x16,
	APT_HEAPALLOC		=0x17,
	APT_VIRTUALALLOC	=0x18,
	APT_VIRTUALALLOCEX	=0x19,
	APT_XMLOPENFILE		=0x20,
};
struct AutoPtrItem
{
	void**	pptr;
	int		iType;
};

class _FK_OUT_CLASS LAutoPtrBase
{
protected:
	AutoPtrItem*	_pDatas;
	int				_iArrayIndex;
	int				_iAllocSize;

	bool AddRemalloc(int iAddArray);

public:
	LAutoPtrBase();
	virtual ~LAutoPtrBase();
};

// 建立一个删除的函数指针。
typedef bool (_fncall* FNDESTRUCTOR)(data_t*);

class _FK_OUT_CLASS LAutoPtr : public LAutoPtrBase
{
private:
public:
	LAutoPtr();
	virtual ~LAutoPtr();

	b_call Add(data_t** pptr, int iType);

	b_call AddUnknown(IUnknown** pUnknown);
	b_call AddUnknown2(IUnknown** pUnknown, IUnknown** pp2);
	b_call AddUnknown3(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3);
	b_call AddUnknown4(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4);
	b_call AddUnknown5(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5);
	b_call AddUnknown6(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6);
	b_call AddUnknown7(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6, IUnknown** pp7);
	b_call AddUnknown8(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6, IUnknown** pp7, IUnknown** pp8);

	LObject* AddObject(LObject** pobj);

	fk::FARRAY _fncall ArrayCreateEx2(UINT uiInitCount, UINT uiNewCount, LPARAM lParam, FNARRAYDELETEDATA fnArrayDeleteData);

	HRESULT QueryInterface(IUnknown* pUnknown, REFIID riid, void** ppvObject);
	HRESULT CreateInstance(REFCLSID rclsid, LPUNKNOWN pUnkOuter, DWORD dwClsContext, IN REFIID riid, LPVOID FAR* ppv);

	void* Malloc(size_t _Size);
	HLOCAL LocalAlloc(UINT uFlags, SIZE_T uBytes);
	HGLOBAL GlobalAlloc(UINT uFlags, SIZE_T dwBytes);
	//LPVOID HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
	//LPVOID VirtualAlloc(HANDLE hProcess, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	///LPVOID VirtualAllocEx(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);

	b_call AddDestructor(data_t** ppData, FNDESTRUCTOR fnDestructor);
	b_call AddVariantClear(VARIANT* pvt);

	b_call AddChar(char** pptr);
	b_call AddFKChar(char** pptr);

	b_call AddWchar(wchar_t** pptr);
	b_call AddFKWchar(wchar_t** pptr);

	b_call AddBase(LBase** ppBase);
	b_call AddBaseData(LVar** ppBaseData);

#ifndef __GNUC__
	b_call AddItemIDList(LPITEMIDLIST* lppItemIDList);
	BSTR SysAllocString(const OLECHAR* psz);
	b_call AddSysAllocString(BSTR* bstr);
#endif // __GNUC__


	GUID* MallocGuid();
	b_call clear();

	//
	// 打开 xml 文件名称。
	// szFileName - 可以是绝对路径，也可是 exe 文件的相对路径。
	// ppXmlConfig - 返回 HXMLCONFIG 句柄。
	//
	b_fncall xmlfOpenFile2(LPCWSTR szFileName, LPVOID* ppXmlConfig);

	//
	// 打开资源文件名称。
	// szFileName - 可以是绝对路径，也可是 res 文件相对路径。
	// ppXmlConfig - 返回 HXMLCONFIG 句柄。
	//
	b_fncall OpenXmlResFile(fk::LModule* pmodule, LPCWSTR szFileName, LPVOID* ppXmlConfig);

	//
	// 打开资源文件名称。
	// szFileName - 可以是绝对路径，也可是 res 文件相对路径。
	// pxmlFile - 返回 pxmlFile 句柄。
	//
	b_fncall OpenXmlResFile(fk::LModule* pmodule, LPCWSTR szFileName, fk::LXmlFile** pxmlFile);
};

#define fk_autop(_CLASS1, _OBJ1)	fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				__ap.AddUnknown((IUnknown**)&_OBJ1);

#define fk_autop2(_CLASS1, _OBJ1, _CLASS2, _OBJ2)	fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				__ap.AddUnknown2((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2);

#define fk_autop3(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3)   \
				fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL; \
				__ap.AddUnknown3((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3);

#define fk_autop4(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4)	fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				__ap.AddUnknown4((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4);

#define fk_autop5(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4, _CLASS5, _OBJ5)	fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				_CLASS5 _OBJ5 = NULL;\
				__ap.AddUnknown5((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5);

#define fk_autop6(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4, _CLASS5, _OBJ5, _CLASS6, _OBJ6)\
				fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				_CLASS5 _OBJ5 = NULL;\
				_CLASS6 _OBJ6 = NULL;\
				__ap.AddUnknown6((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6);

#define fk_autop7(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4, _CLASS5, _OBJ5, _CLASS6, _OBJ6, _CLASS7, _OBJ7)\
				fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				_CLASS5 _OBJ5 = NULL;\
				_CLASS6 _OBJ6 = NULL;\
				_CLASS7 _OBJ7 = NULL;\
				__ap.AddUnknown7((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8);

#define fk_autop8(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4, _CLASS5, _OBJ5, _CLASS6, _OBJ6, _CLASS7, _OBJ7, _CLASS8, _OBJ8)\
				fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				_CLASS5 _OBJ5 = NULL;\
				_CLASS6 _OBJ6 = NULL;\
				_CLASS7 _OBJ7 = NULL;\
				_CLASS8 _OBJ8 = NULL;\
				__ap.AddUnknown8((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8);

#define fk_autop9(_CLASS1, _OBJ1, _CLASS2, _OBJ2, _CLASS3, _OBJ3, _CLASS4, _OBJ4, _CLASS5, _OBJ5, _CLASS6, _OBJ6, _CLASS7, _OBJ7, _CLASS8, _OBJ8, _CLASS9, _OBJ9)\
				fk::LAutoPtr __ap;	\
				_CLASS1 _OBJ1 = NULL;\
				_CLASS2 _OBJ2 = NULL;\
				_CLASS3 _OBJ3 = NULL;\
				_CLASS4 _OBJ4 = NULL;\
				_CLASS5 _OBJ5 = NULL;\
				_CLASS6 _OBJ6 = NULL;\
				_CLASS7 _OBJ7 = NULL;\
				_CLASS8 _OBJ8 = NULL;\
				_CLASS9 _OBJ9 = NULL;\
				__ap.AddUnknown9((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8, (IUnknown**)&_OBJ9);


// 类构造函数中使用。
#define fk_AutoPtr(_AUTO_PTR, _OBJ1)		\
				_OBJ1 = NULL;\
				_AUTO_PTR.AddUnknown((IUnknown**)&_OBJ1);

#define fk_AutoPtr2(_AUTO_PTR, _OBJ1, _OBJ2)		\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_AUTO_PTR.AddUnknown2((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2);

#define fk_AutoPtr3(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3)		\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_AUTO_PTR.AddUnknown3((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3);

#define fk_AutoPtr4(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4)		\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_AUTO_PTR.AddUnknown4((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4);

#define fk_AutoPtr5(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4, _OBJ5)		\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_OBJ5 = NULL;\
				_AUTO_PTR.AddUnknown5((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5);

#define fk_AutoPtr6(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4, _OBJ5, _OBJ6)\
					\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_OBJ5 = NULL;\
				_OBJ6 = NULL;\
				_AUTO_PTR.AddUnknown6((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6);

#define fk_AutoPtr7(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4, _OBJ5, _OBJ6, _OBJ7)\
					\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_OBJ5 = NULL;\
				_OBJ6 = NULL;\
				_OBJ7 = NULL;\
				_AUTO_PTR.AddUnknown7((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8);

#define fk_AutoPtr8(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4, _OBJ5, _OBJ6, _OBJ7, _OBJ8)\
					\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_OBJ5 = NULL;\
				_OBJ6 = NULL;\
				_OBJ7 = NULL;\
				_OBJ8 = NULL;\
				_AUTO_PTR.AddUnknown8((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8);

#define fk_AutoPtr9(_AUTO_PTR, _OBJ1, _OBJ2, _OBJ3, _OBJ4, _OBJ5, _OBJ6, _OBJ7, _OBJ8, _OBJ9)\
					\
				_OBJ1 = NULL;\
				_OBJ2 = NULL;\
				_OBJ3 = NULL;\
				_OBJ4 = NULL;\
				_OBJ5 = NULL;\
				_OBJ6 = NULL;\
				_OBJ7 = NULL;\
				_OBJ8 = NULL;\
				_OBJ9 = NULL;\
				_AUTO_PTR.AddUnknown9((IUnknown**)&_OBJ1, (IUnknown**)&_OBJ2, (IUnknown**)&_OBJ3, (IUnknown**)&_OBJ4, (IUnknown**)&_OBJ5, (IUnknown**)&_OBJ6, (IUnknown**)&_OBJ7, (IUnknown**)&_OBJ8, (IUnknown**)&_OBJ9);

class _FK_OUT_CLASS LAutoPtrTl : public fk::LAutoPtr
{
private:
protected:
public:
	LAutoPtrTl();
	virtual ~LAutoPtrTl(void);

	fk::LFileOperation* _call NewFileOperation2(fk::LObject* pParentObject);
};

///////////////////////////////////////////////////////////////////////////////////////////////////
class _FK_OUT_CLASS LComponent : public fk::LObject
{
	_FK_RTTI_

protected:

public:
	LComponent(fk::LObject* pParentObject);
	LComponent(fk::LObject* pParentObject, DWORD dwObjectMask);
	virtual ~LComponent();

	fk::LAutoPtr	_AutoPtr;

	virtual b_call Assign(LObject* pobject);
};

///////////////////////////////////////////////////////////////////////////////////////////////////
class _FK_OUT_CLASS LStringws : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringws();
	virtual ~LStringws(void);

	virtual b_call IsClassType(LPGUID pClassIID);

	virtual fk::LStringw* NewTextw(void);

	virtual void clear(void);
	virtual i_call GetCount(void);
	virtual data_t* _call GetDatas(void);
	virtual data_t* _call begin(void);

	virtual fk::LStringw* _call GetItem(UINT uiIndex);

	virtual i_call FindItemI(LPCWSTR lpszText);
	virtual b_call FullCompose(fk::LStringw* pFullText, LPCWSTR lpszApart);

	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringw* pString);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringws;			//// {0CFF5515-186E-42B4-81C7-35BF67C6FD93}



class _FK_OUT_CLASS LStringwsList : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringwsList(fk::LObject* pParentObject);
	virtual ~LStringwsList(void);

	virtual b_call IsClassType(LPGUID pClassIID);

	virtual fk::LStringws* NewStringws(void);
	virtual void clear(void);
	virtual i_call GetCount(void);

	virtual fk::LStringws* _call GetItem(UINT uiIndex);

	virtual b_call FindItemI(LPCWSTR lpszText, fk::LStringws** pStringwsOut, UINT* uipIndex);
	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringws* pString);

	//	virtual b_call FullCompose(fk::LStringws* pFullText, LPCWSTR lpszApart);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringwsList;		// {2A226983-ADC6-4D9C-9B2E-1A511DD74880}



class _FK_OUT_CLASS LStringas : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringas(void);
	virtual ~LStringas(void);

	virtual b_call IsClassType(LPGUID pClassIID);

	virtual fk::LStringa* NewText(void);

	virtual void clear(void);
	virtual i_call GetCount(void);
	virtual data_t* _call GetDatas(void);
	virtual data_t* _call begin(void);

	virtual fk::LStringa* _call GetItem(UINT uiIndex);

	virtual i_call FindItemI(LPCSTR lpszText);
	virtual b_call FullCompose(fk::LStringa* pFullText, LPCSTR lpszApart);

	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringa* pString);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringas;			// {AF61E718-F5BB-49BC-AF4E-CECCB431099C}


bool WINAPI StrCheckLenA(HWND hwndHint, char* pstr, int iMinLen, int iMaxLen);
void WINAPI ShowErrorHResult(HWND hWnd, HRESULT hr, LPCWSTR lpszFunName);
typedef void (WINAPI* FNLWADDINFOW)(wchar_t*);


///////////////////////////////////////////////////////////////////////////////////////////////////
enum enumVxmlFileType
{
	umVxmlFileMainFrame = 0x0,
	umVxmlFileDialog = 0x1,
	umVxmlFileView = 0x2,
};

class _FK_OUT_CLASS LVxmlFile : public fk::LComponent
{
private:
public:
	LVxmlFile(fk::LObject* pParentObject);
	virtual ~LVxmlFile(void);

	fk::HXMLCONFIG	_hXmlConfig;
	DWORD			_dwVer;

	virtual b_call IsClassType(LPGUID pClassIID);

	virtual b_call OpenXmlFile(fk::LModule* pmodule, LPCWSTR lpszVxmlFile) = 0;
	virtual v_call CloseXmlFile(void) = 0;
	virtual dw_call GeVxmlVersion(void) = 0;

	static LVxmlFile* _call NewVxmlFile(fk::LObject* pParentObject);
};
extern IID IID_IVxmlFile;			// {B9D1FBF0-3D2B-4FE8-8675-42D073579E7E}


///////////////////////////////////////////////////////////////////////////////////////////////////
// 注册一个创建对象列表,   如何没有新的类，可以不调用这里。
//
// b_fncall CreateObject(fk::LModule* pmodule, fk::LObjects* pobject,
//							LPCSTR lpszClassName, fk::HXMLNODE hXmlNode, DWORD dwMake, fk::LObject** ppNewObject);

enum enumCrateObjectDefault
{
	COIF_DATA_XML		= 0x1,		// 创建对象从 xml 文件中. 需要写 hXmlNode 参数。
	COIF_DATA_DEFAULT	= 0x2,		// 创建对象默认值，不从 xml 文件中读，不需要写 hXmlNode 参数。
};

// 这个类是类创建代码块填写,LPFNCREATEOBJECT回调函数试用，并不是用户代码段。
typedef struct tagCREATEOBJECTINFO
{
	DWORD dwSize;
	DWORD dwMask;

	fk::LModule*	pmodule;			// 创建控件的所在的项目句柄。
	fk::LObjects*	pobjects;			//
	LPSTR			lpszClassName;		// 需要创建的类名称。
	fk::LVxmlFile*	pVxmlFile;			// xml 文件操作句柄。
	fk::HXMLNODE	hXmlNode;			// 类项节点名称。
}CREATEOBJECTINFO,*LPCREATEOBJECTINFO;

typedef bool (_fncall* LPFNCREATEOBJECT)(LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject);
typedef bool (_fncall* LPFNLINK_OBJECT)(LPCREATEOBJECTINFO lpcoi, fk::LObject* pwindow);

typedef struct tagREGOBJECTITEM
{
	DWORD				dwSize;
	DWORD				dwMask;
	LPSTR				lpszClass;
	LPFNCREATEOBJECT	lpfnCreateObject;
	LPFNLINK_OBJECT		lpfnLinkObject;
}REGOBJECTITEM,* LPREGOBJECTITEM;

b_fncall ObjectReg(LPREGOBJECTITEM lpRegClassItem);
b_fncall ObjectUnreg(LPFNCREATEOBJECT lpfnCreateObject);

b_fncall ObjectFindW(LPCWSTR lpszClassName);
b_fncall ObjectFindA(LPCSTR lpszClassName);

#ifdef UNICODE
#define ObjectFind		ObjectFindW
#else
#define ObjectFind		ObjectFindA
#endif

class _FK_OUT_CLASS LObjectRegItem : public fk::LObject
{
private:
protected:
public:
	LPFNCREATEOBJECT	lpfnCreateObject;
	LPFNLINK_OBJECT		lpfnLinkObject;

public:
	LObjectRegItem(fk::LObject* pParentObject);
	virtual ~LObjectRegItem(void);
	virtual b_call SetRegObjectItem(LPREGOBJECTITEM proi) = 0;
	virtual b_call IsRegClassName(LPCWSTR lpszClassName) = 0;
	virtual b_call IsRegClassNameA(LPCSTR lpszClassName) = 0;

	static LObjectRegItem* NewObjectRegitem(fk::LObject* pParentObject);
};

class _FK_OUT_CLASS LObjectReg : public fk::LObject
{
private:
protected:

public:
	LObjectReg(fk::LObject* pParentObject);
	virtual ~LObjectReg(void);

	virtual b_call reg(LPREGOBJECTITEM lpRegClassItem) = 0;
	virtual b_call unreg(LPFNCREATEOBJECT lpfnCreateObject) = 0;

	virtual b_call FindW(LPCWSTR lpszClassName) = 0;
	virtual b_call FindA(LPCSTR lpszClassName) = 0;

	virtual i_call GetCount(void) = 0;
	virtual data_t* GetDatas(void) = 0;
	virtual fk::LObjectRegItem* GetItem(int iitem) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;
	virtual b_call CreateObjrectItem(fk::LObject** ppObjectItem) = 0;

	static LObjectReg* _fncall NewObjectReg(fk::LObject* pParentObject);
};

/*
管理每一个注册类。
1.创建一个注册类。
*/
class _FK_OUT_CLASS LObjectRegMgr : public fk::LObject
{
private:
protected:
public:
	LObjectRegMgr(fk::LObject* pParentObject);
	virtual ~LObjectRegMgr(void);

	virtual b_call CreateObjectReg(LPCWSTR szname, fk::LObjectReg** ppObjectRegOut) = 0;
	virtual b_call DeleteObjectReg(fk::LObjectReg* pObjectReg) = 0;

	virtual i_call GetCount(void) = 0;
	//virtual b_call AddObjectReg(fk::LObjectReg* pObjectReg) = 0;
	//virtual b_call RemoveObjectReg(fk::LObjectReg* pObjectReg) = 0;

	virtual fk::LObjectReg* _call GetItem(UINT uiItem) = 0;
	virtual fk::LObjectReg* _call FindName(LPCWSTR lpszName) = 0;
	virtual b_call GetEnumData(fk::LEnumData** pEnumData) = 0;

	static LObjectRegMgr* _fncall GetSysObjectRegs(void);
};


///////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct tagCRAET_OBJECT_DEFAULT
{
	DWORD dwSize;
	DWORD dwMask;
	fk::LModule* pmodule;
	LPSTR lpszClassName;
	LPWSTR lpszVarName;
}CRAET_OBJECT_DEFAULT,*LPCRAET_OBJECT_DEFAULT;

//
//typedef struct tagOBJECT_ITEM			// 向 fk::LWindows 添加的每一个窗口属性。
//{
//	DWORD			dwMake;
//	fk::LStringw	sClassVar;			// 对象的变量名称，在某些情况下，可能是空值。
//	fk::LObject*	pobject;			// 类的变量。
//	LPARAM			lparam;
//}OBJECT_ITEM,*fk::LObject*;
//

class _FK_OUT_CLASS LObjects : public fk::LObject
{
	_FK_RTTI_
private:
protected:
public:
	LObjects(fk::LObject* pParentObject);
	virtual ~LObjects();

	virtual b_call AddObject(LPCWSTR lpszVarName, LObject* pobject, bool bglobal=false) = 0;
	virtual b_call AddObject(LObject* pobject) = 0;

	virtual b_call CraetObjectDefault(LPCRAET_OBJECT_DEFAULT lpcod, fk::LObject** ppNewObject) = 0;

	virtual b_call RemoveObject(LPCWSTR lpszVarName, LObject* pobject) = 0;
	virtual b_call RemoveObject(LObject* pobject) = 0;

	virtual b_call RenObjectVar(LPCWSTR lpszOldVar, LPCWSTR lpszNewVar) = 0;
	virtual b_call SetObjectVar(fk::LObject* pobject, LPCWSTR lpszVar) = 0;

	virtual b_call GetEnumData(LEnumData** ppEnumData) = 0;
	virtual data_t* _call GetDatas(void) = 0;

	virtual fk::LObject* _call FindObject(LPCWSTR lpszVarName) = 0;
	virtual b_call FindObject(fk::LObject* pobject) = 0;

	// 这两个函数只返回全局对象，
	virtual fk::LObject* _call FindGlobalItem(LPCWSTR lpszVarName) = 0;
	virtual b_call FindObjectGlobal(LPCWSTR lpszVarName, fk::LObject** ppobj) = 0;

	virtual fk::LObject* _call GetObjectItem(int iitem) = 0;
	virtual i_call GetCount(void) = 0;
	virtual v_call clear(void) = 0;

	virtual v_call SetObjectsInfo(LPCWSTR lpszObjectsInfo) = 0;

	/* 获得某一个类的所有变量名称和到OBJECT_ITEM 到 fk::LEnumPlus 对象列表中。
	 *	@param lpszClassName 类的名称，例如：fk::LObject
	 *  @param pEnumPlus 指定 pEnumPlus对象.
	 */
	virtual b_call GetClassEnumPlus(LPCWSTR lpszClassName, fk::LEnumPlus* pEnumPlus) = 0;

	/* 从 xml文件中读入对象列表，并创建出对象。
	 * @param pmodule 创建者的 module 和资源。
	 * @param pVxmlFile
	 */
	virtual fk::enumReturn _call ReadObjectXml(fk::LModule* pmodule, fk::LVxmlFile* pVxmlFile, LPCSTR lpszXmlPath) = 0;

	static fk::LObjects* _call NewObjects(fk::LObject* pParentObject);

public:
	enum enumNotifyObjects
	{
		ON_QUERY_REN		= 0x0000004,			// 移出一个对象。
		ON_REN_OBJECT_VAR	= 0x0000008,			// 修改对象变量的名子。
		ON_CLEAR			= 0x0000010,			// 清空所有对象。
		ON_AddObject		= 0x0000020,
		ON_RemoveObject		= 0x0000040,
		ON_RenObject		= 0x0000080,
	};

	typedef struct tagADD_OBJECT
	{
		fk::NOTIFYEVENT		nEvent;
		fk::LObject*	pobject;
	}ADD_OBJECT,*LPADD_OBJECT;

	typedef struct tagREMOVE_OBJECT
	{
		fk::NOTIFYEVENT nEvent;
		fk::LObject*	pobject;
		bool			bremove;				// 可以删除设置 true，不可以删除设置为 false.
	}REMOVE_OBJECT,*LPREMOVE_OBJECT;

	typedef struct tagREN_OBJECT_VAR
	{
		fk::NOTIFYEVENT		nEvent;
		fk::LObject*	pobject;
		LPCWSTR				lpszNewVarName;			// 新的对象变量名称。
		bool				brren;					// 可以删除设置 true，不可以删除设置为 false.
	}REN_OBJECT_VAR,*LPREN_OBJECT_VAR;
};


//----------------------------------------------------------------------------------------------------
//
// 这个函数是管理当前进程设置的 LObjects 对象列表， 并不是当前进程的 LObjects 对象列表。
//
class _FK_OUT_CLASS LObjectsList : public fk::LObject
{
	_FK_RTTI_

private:
	void* _pobjects;

protected:
public:
	LObjectsList(fk::LObject* pParentObject);
	virtual ~LObjectsList(void);

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual b_call IsClassType(LPGUID pClassIID);

	virtual b_call AddObject(LPCWSTR lpszKey, fk::LObjects* pObjects);
	virtual b_call AddObject(fk::LObjects* pObjects);
	virtual b_call RemoveObject(LPCWSTR lpszKey, fk::LObjects* pObjects);
	virtual b_call RemoveObject(fk::LObjects* pObjects);

	virtual b_call GetEnumData(LEnumData** ppEnumData);

	virtual fk::LObjects* Find(LPCWSTR lpszKey);
	virtual ::IUnknown* FindObjectsChildItem(LPCWSTR lpszKey);

	virtual i_call GetCount(void);
	virtual v_call clear();
	//virtual b_call ReadObjectXml(fk::LModule* pModule, fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath);

	static LObjectsList* _fncall NewObjectsList(fk::LObject* pParentObject);
	static LObjectsList* _fncall GetGlobalObjectsList(void);
};


class _FK_OUT_CLASS LError : public fk::LObject
{
private:
protected:
	fk::LStringw	_sErrorInfo;
	bool			_bSuccess;

public:
	LError(void);
	virtual ~LError(void);

	virtual b_call IsClassType(LPGUID pClassIID);

	v_call TraceError(void);
	v_call AddItemComma(LPWSTR lpszErrorItem);				// 使用 _sErrorInfo 变量。 格式：  ", lpszErrorItem"

	virtual b_call IsSuccess(void);
	virtual b_call IsError(void);
	virtual v_call clear(void);

	v_call AddInfo(fk::LVar* pBaseData);

	fk::LStringw&  GetErrorInfo(void);
};


/*
使用顺序：
fk::LXmlPropError xpe;

if (!xpe.IsErrorItem())
	xpe.NewItem();
*/
class _FK_OUT_CLASS LXmlPropError : public fk::LError
{
private:
protected:
	int		_iStripIndex;
	bool	_bErrorPropName;
	bool	_bErrorItem;

	v_call SetPathNode(fk::HXMLNODE hxmlNode=NULL, fk::LVar* bdXmlPath=NULL);

public:
	LXmlPropError(void);
	virtual ~LXmlPropError(void);

	virtual b_call IsClassType(LPGUID pClassIID);
	virtual v_call clear(void);

	v_call AddErrorPropName(LPCSTR lpszPropName);
	v_call AddErrorPropName(LPCSTR lpszPropName, fk::LModule* pmodule, LPCSTR lpszInfo, ...);

	v_call NewLine(void);								// 建立一个新行。
	v_call NewItem(void);								// 开始第几条错误。
	b_call IsErrorItem(void);							// 存在开始第几条错误。
	b_call IsErrorPropName(void);						//

	v_call SetXmlNode(fk::HXMLNODE fxmlNode);
	v_call SetXmlFileA(LPCSTR lpszXmlFile, LPCSTR lpszXmlPath=NULL);
	v_call SetXmlFileW(LPCWSTR lpszXmlFile, LPCWSTR lpszXmlPath=NULL);
	v_call SetXmlFile(fk::LVar* pbdXmlFile, fk::LVar* bdXmlPath=NULL);
};


//
// virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
//
class _FK_OUT_CLASS LNotify : public fk::LObject
{
	_FK_RTTI_;
private:
protected:
public:
	LNotify(fk::LObject* pParentObject);
	virtual ~LNotify();
	FKNOTIFY _hNotify;

	virtual b_call Attach(LObject* pObject);
	virtual STDMETHODIMP QueryInterface(REFIID riid, void** ppvObject);

	/**
	*	注册当前对象通报信息到 pReceiveObject 对象中。
	*	@param pSendObject 通报信息发送着。
	*	@param pNotify 通报发送对象。
	*   @param pReceiveObject 通报信息接收对象。
	*	@param dwEvent 事件值。
	*/
	b_call RegsvrNotifyTo(fk::LObject* pSendObject, fk::LComponent* pReceiveObject, DWORD dwEvent);			// 注册事件到那里。

	/**
	*	注册当前对象通报信息到 pReceiveObject 对象中。此函数可以重载。
	*  @param pReceiveObject 通报信息接收对象。
	*	@param dwEvent 事件值。
	*/
	virtual b_call RegsvrNotifyTo(fk::LObject* pReceiveObject, DWORD dwEvent);			// 注册事件到那里。

	/*
	 *	删除对象通报消息接收着。
	 *	-pSendObject 通报信息发送着。
	 *	-Notify  通报发送对象。
	 *  -pReceiveObject  通报信息接收对象。
	 */
	b_call UnregsvrNotify(fk::LObject* pSendObject, fk::LComponent* pReceiveObject);							// 取消注册的事件。

	/**
	*	删除当前对象通报消息接收着。此函数可以重载。
	*   @param pReceiveObject 通报信息接收对象。
	*/
	virtual b_call UnregsvrNotify(fk::LObject* pReceiveObject);							// 取消注册的事件。

	b_call NotifyAdd(LPNOTIFYITEM pNotifyItem);
	b_call NotifyRemove(LPNOTIFYITEM pNotifyItem);
	b_call NotifyCall(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	b_call NotifyCallObject(void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	//b_call NotifyCallEx(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc);
	b_call NotifyGetCount(UINT* uiCount);
	b_call NotifySetSelf(LPNOTIFYITEM pcnpi);
	b_call NotifyExist(DWORD dwNotify);
	FKNOTIFY GetNotifyHandle(void);
	b_call NotifySetState(DWORD dwNotifyState);
	dw_call NotifyGetState();

	b_call InstallNotifySelfEvent(fk::enumInstallEvent emie,
								void* dwRegObject, DWORD dwObjectID, fk::LObject* pTargetProc);
	//默认接收两个以个消息。 ON_NOTIFY_ADDITEM|ON_NOTIFY_REMOVEITEM;

	static b_fncall NewNotify2(fk::LObject* pParentObject, fk::enumClassValue cvClassValue, fk::LNotify** ppv);
	static fk::LNotify* _fncall NewNotify(fk::LObject* pParentObject);
};
extern IID IID_INotify;				// {06C06794-EF64-4c0a-B09A-D94FF28815B5}


class _FK_OUT_CLASS LNotifys : public fk::LComponent
{
private:
protected:
public:
	LNotifys(fk::LObject* pParentObject);
	virtual ~LNotifys(void);

	virtual b_call Attach(LObject* pObject) = 0;
	virtual b_call Add(fk::LNotify* pNotify) = 0;
	virtual b_call Remove(fk::LNotify* pNotify) = 0;
	virtual b_call Call(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent) = 0;
	virtual b_call CallObject(void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent) = 0;
	//virtual b_call CallEx(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc) = 0;
	virtual b_call GetCount(UINT* uiCount) = 0;
	//virtual b_call SetSelf(LPNOTIFYITEM pcnpi) = 0;
	virtual fk::FKNOTIFYS GetNotifysHandle(void) = 0;
	virtual b_call SetState(DWORD dwNotifyState) = 0;
	virtual dw_call GetState() = 0;

	//virtual b_call InstallNotifySelfEvent(fk::enumInstallEvent emie,
	//	void* dwRegObject, DWORD dwObjectID, fk::LComponent* pTargetProc);
	////默认接收两个以个消息。 ON_NOTIFY_ADDITEM|ON_NOTIFY_REMOVEITEM;

	static fk::LNotifys* _call NewNotifys(fk::LObject* pParentObject);
};
// {20A5F059-6110-4836-8BF9-850EE9F47E30}
extern IID IID_INotifys;


/*
这是新的风格。<item make="0x00000043" name="item 3" id="3400" value="3"/>
*/
class LEnumItem : public fk::LObject
{
public:
	LEnumItem(fk::LObject* pParentObject);
	virtual ~LEnumItem();

	DWORD			_dwmake;
	fk::LStringw	_sname;
	fk::LStringw	_svalue;				// xml value 属性
	int				_id;
	fk::HXMLNODE	_fXmlNode;

	int				_idata;					// xml index  value 属性
	data_t*			_pdata;

	virtual b_call Attach(LObject* pObject);
	virtual v_call clear(void);
	virtual b_call ReadItem(fk::LModule* pmodule, fk::HXMLNODE hxmlNode);

	static LEnumItem* NewEnumItem(fk::LObject* pParentObject);

public:
	enum enumEnumItemMake
	{
		EIF_TEXT			= 0x00000001,
		EIF_NAME_RES_ID		= 0x00000002,
		EIF_VALUE_RES_ID	= 0x00000004,
	};
};

//
// 定义enum 类型使用的类，让 enum 拥有返回自己说明的类。
//
// 可以不使用 NewEnumPlus 方法创建，也能直接使用。
//
class _FK_OUT_CLASS LEnumPlus : public fk::LComponent
{
	_FK_RTTI_

private:
	fk::FARRAY _farray;

protected:
	int _iItemValue;

	v_call DoInsertItem(fk::LEnumItem* pEnumItem);

public:
	LEnumPlus(fk::LObject* pParentObject=NULL);
	virtual ~LEnumPlus();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void** ppvObject);

	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual void SetActiveItem(int dwData);
	virtual int GetActiveItem();
	virtual i_call GetCount(void);

	virtual b_call ReadXmlData(fk::LModule* pModule, fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath);
	virtual data_t* GetDatas(void);
	virtual fk::LEnumItem* _call GetEnumItem(INT iitem);
	virtual b_call FindEnumItemValue(LPCWSTR lpszValue, fk::LEnumItem** ppEnumItem);
	virtual b_call FindEnumItemData(int idata, fk::LEnumItem** ppEnumItem);

	virtual int GetResourceID(int nEnumItem);
	virtual int GetActiveResourceID();
	virtual bool GetDataText(int dwData, wchar_t* wsItemBuf);
	virtual bool GetDataText(int dwData, fk::LStringa* sItemText);
	virtual bool GetDataText(int dwData, fk::LStringw* sItemText);
	virtual bool GetActiveEnumItemText(wchar_t* wsItemBuf);

	virtual b_call InsertEnumItem(int iIndex, LPCWSTR pwsText, int dwData, fk::HXMLNODE hXmlNodeOrNull);
	virtual b_call AddEnumItem(LPCWSTR pwsText, int dwData, fk::HXMLNODE hXmlNodeOrNull);

	virtual b_call DataToComboBox(HWND hWndCtrl);
	virtual b_call DataToListBox(HWND hWndCtrl);

	virtual v_call clear(void);

	static b_fncall NewEnumPlus(fk::LObject* pParentObject, LPVOID* ppobj);

public: // notify
    enum umEnumPlusEvent
    {
        ON_INSERTITEM	=0x00000001,
        ON_REMOVEITEM	=0x00000002,
        ON_READDATA		=0x00000004,
        ON_CLEARDATA	=0x00000008,
    };

    typedef struct tagENUMPLUSINSERT
    {
        fk::NOTIFYEVENT nEvent;
        fk::LEnumPlus*	pEnumPlus;
		fk::LEnumItem*	pEnumItem;
    } ENUMPLUSINSERT,*PENUMPLUSINSERT;

    typedef struct tagENUMPLUSREMOVE
    {
        fk::NOTIFYEVENT nEvent;
        LEnumPlus*		pEnumPlus;
		fk::LEnumItem*	pEnumItem;
    } ENUMPLUSREMOVE,* PENUMPLUSREMOVE;

    typedef struct tagENUMPLUSREADDATA
    {
        fk::NOTIFYEVENT nEvent;
        LEnumPlus*		pEnumPlus;
		fk::LEnumItem*	pEnumItem;
    } ENUMPLUSREADDATA,* PENUMPLUSREADDATA;
};


enum umMacroParam
{
	umMacroParamSuccess	=0x00000001,
	umMacroParamFixed	=0x00000002,			// 固定宏，不能删除和清空。
};

typedef struct tagMacroParam
{
	DWORD dwMask;
	fk::HXMLNODE hxmlNode;
	fk::LStringw sName;
	fk::LStringw sParam;
}MACROPARAM,*LPMACROPARAM;

enum umMakeMacroReturn
{
	mmrExistMacro=0x1,
	mmrMakeFail=0x2,
	mmrSuccess=0x3,
};

class _FK_OUT_CLASS LMacroParamsw : public fk::LComponent
{
	_FK_RTTI_

private:

public:		// = 0
	virtual void clear(void) = 0;
	virtual i_call GetCount(void) = 0;
	virtual data_t* _call GetDatas(void) = 0;
	virtual MACROPARAM* _call FindMacroParamItem(LPCWSTR pwsName) = 0;

	virtual b_call Assign(LObject* pobject) = 0;
	virtual b_call AddMacroItem(LPCWSTR pszName, LPCWSTR pwsParam, DWORD dwMake=umMacroParamSuccess) = 0;

	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCWSTR lpszxmlMacroPath) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCSTR lpszxmlMacroPath) = 0;

	virtual umMakeMacroReturn _call MakeMacro(fk::LStringw* spOutString) = 0;
	virtual b_call ModifyMacro(LPCWSTR pszName, LPCWSTR lpszParam) = 0;
	virtual b_call ModifyMacroXmlNode(LPCWSTR pszName, LPCWSTR lpszParam) = 0;

	virtual b_call CheckStringMacro(const wchar_t* wsOutString) = 0;
	virtual b_call CheckMacroData() = 0;
	virtual v_call PrintfMacroData() = 0;

public:
	LMacroParamsw(fk::LObject* pParentObject);
	virtual ~LMacroParamsw();

	static fk::LMacroParamsw* NewMacroParamsw(fk::LObject* pParentObject);
};


typedef struct tagMacroParamA
{
	DWORD dwMask;
	fk::HXMLNODE hxmlNode;
	fk::LStringa sName;
	fk::LStringa sParam;
}MACROPARAMA,*LPMACROPARAMA;

class _FK_OUT_CLASS LMacroParamsa : public fk::LComponent
{
_FK_RTTI_

private:
public:
	virtual void clear(void) = 0;
	virtual i_call GetCount(void) = 0;
	virtual data_t* _call GetDatas(void) = 0;
	virtual fk::LPMACROPARAMA _call FindMacroParamItem(LPCSTR pwsName) = 0;

	virtual b_call Assign(LObject* pobject) = 0;
	virtual b_call AddMacroItem(LPCSTR pszName, LPCSTR pwsParam, DWORD dwMake=umMacroParamSuccess) = 0;

	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCSTR xmlMacroPath) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCWSTR lpszxmlMacroPath) = 0;

	virtual fk::umMakeMacroReturn _call MakeMacro(fk::LStringa* spOutString) = 0;
	virtual b_call ModifyMacro(LPCSTR pszName, LPCSTR lpszParam) = 0;
	virtual b_call ModifyMacroXmlNode(LPCSTR pszName, LPCSTR lpszParam) = 0;

	virtual b_call CheckStringMacro(LPCSTR wsOutString) = 0;
	virtual b_call CheckMacroData() = 0;
	virtual v_call PrintfMacroData() = 0;

	virtual v_call SetMacroBeginEnd(LPCSTR lpszBegin, LPCSTR lpszEnd) = 0;

public:
	LMacroParamsa(fk::LObject* pParentObject);
	virtual ~LMacroParamsa();

	static fk::LMacroParamsa* NewMacroParamsW(fk::LObject* pParentObject);
};

typedef struct tagFILELINEINFO
{
	int iLine;				// 行。
	int iBeginPos;			// 文件位置。
	int iLineLen;			// 文件行长度位置。
}FILELINEINFO,*LPFILELINEINFO;
class LFileOperation : public fk::LObject
{
public:
	virtual b_call OpenFile(LPCWSTR lpszFileName, LPCWSTR lpszMode) = 0;
	virtual b_call OpenFile(LPCWSTR lpszFileName) = 0;
	virtual i_call fseek(long offset, int origin)=0;
	virtual i_call fputc(int c) = 0;
	virtual b_call IsOpen(void)=0;
	virtual l_call GetFileLength(void)=0;
	virtual v_call Close(void) = 0;
	virtual b_call SetEndOfFile(LONG lEndPos) = 0;

	virtual int WriteText(LPCWSTR lpszText, int nLength)=0;
	virtual int WriteText(LPCSTR lpszText, int nLength)=0;
	virtual i_call ReadText(fk::LStringa* pcText, int nLength)=0;
	virtual i_call ReadFull(fk::LStringa* psFullText) = 0;

	virtual b_call ReadLine(int iLine, fk::LStringw* pwTextLine) = 0;
	virtual b_call ReadLine(int iLine, fk::LStringa* pcTextLine) = 0;
	virtual fk::LPFILELINEINFO _call FindText(LPCSTR lpszText) = 0;
	virtual fk::LPFILELINEINFO _call FindText(LPCWSTR lpszText) = 0;
	virtual i_call FindTextPos(LPCSTR lpszText, int iPos) = 0;
	virtual i_call FindTextPos(LPCWSTR lpszText, int iPos) = 0;

	virtual data_t* _call GetLines(void) = 0;
	virtual i_call GetLineCount(void) = 0;

public:
	LFileOperation(fk::LObject* pParentObject);
	virtual ~LFileOperation();
	FILE*	_pFile;

	static LFileOperation* _fncall NewFileOperation(fk::LObject* pParentObject);
};



#define SIF_TEXT		0x00000001
#define SIF_ASK			0x00000002
#define SIF_STAR		0x00000004
#define SIF_ENGLISH		0x00000008

/*
abc*.exe    abc*.exe
abc*a		abc*
a?*			a*
a?*a		a*
a?bc?d*a	a?bc?d*

忽略*号以后的字符，不忽略.号以后的字符。
如果*号前面是？号，忽略？号。
*/
typedef struct tagSTRITEM
{
	DWORD		dwMask;
	wchar_t*	pwText;
	int			iLen;
}STRITEM, *LPSTRITEM;

#define  STR_COMPART_CHAR	_T("|")
#define  STR_COMPART_C		_T('|')


class _FK_OUT_CLASS LFiltrateString : public LObject
{
public:
	LFiltrateString(fk::LObject* pParentObject);
	virtual ~LFiltrateString();

	/* 设置过滤掉的字符串信息，字符串之间用 ; 号分开 */
	virtual bool SetFiltrateInfo(LPCWSTR lpszFiltrateInfo, LPCTSTR lpszCompart)=0;
	virtual bool SetFiltrateInfoFile(LPCWSTR lpszFiltrateInfo, LPCTSTR lpszCompart) = 0;

	/*
	** 返回过滤掉的字符串信息
	** wsText 指定字符串空间。
	*/
	virtual int GetFiltrateInfo(wchar_t* lpszText, LPCTSTR lpszCompart)=0;
	virtual int GetFiltrateInfoLen(void)=0;
	/*
	** 判断 strItem 中是否包含 CFiltrateTypeList 列表中的某一项。
	** 如何包括返回 True，不包含返回 Flase.
	*/
	//virtual bool wcsicmp(LPCWSTR wsText)=0;
	//virtual bool wcscmp(LPCWSTR wsText)=0;
	virtual bool StrI(LPCWSTR lpszText)=0;
	virtual bool AddTextItem(LPCWSTR lpszText)=0;
	virtual bool AddFileItem(LPCWSTR lpszText)=0;
	virtual v_call clear()=0;
	virtual int GetItemCount() = 0;
	virtual b_call GetItemArray(STRITEM* pstrItem) = 0;

	static fk::LFiltrateString* WINAPI NewFiltrateString(fk::LObject* pParentObject);
	static bool WINAPI DeleteFiltrateString(fk::LFiltrateString* psf);
};
// {7897A235-D8AA-418f-AC62-CD3FD966C067}
static const GUID IID_IFiltrateString = { 0x7897a235, 0xd8aa, 0x418f, { 0xac, 0x62, 0xcd, 0x3f, 0xd9, 0x66, 0xc0, 0x67 } };



//
// 载入语言动态链接库。
//
#pragma warning(disable:4311)
inline bool WINAPI LanguageLoadProjectInline(fk::LModule* pmodule, HINSTANCE hInstance)
{
	HINSTANCE hResourceInstance=NULL;

#ifdef _AFX
	hResourceInstance=fk::LanguageLoadFile(hInstance);

	if(hResourceInstance==NULL)
	{
		// AfxSetResourceHandle(AfxGetInstanceHandle());
		return false;
	}
	else
	{
		AfxSetResourceHandle(hResourceInstance);
		return true;
	}
#endif


#ifdef _ATL
	hResourceInstance = fk::LanguageLoadFile(hInstance);

	if (hResourceInstance==NULL)
	{
		//ATL::_AtlBaseModule.SetResourceInstance(	ATL::_AtlBaseModule.GetModuleInstance()	);
		return false;
	}
	else
	{
		pmodule->hRes = hResourceInstance;
		ATL::_AtlBaseModule.SetResourceInstance(hResourceInstance);
		return true;
	}
#endif

	return false;
}
#pragma warning(default:4311)



//
// 载入语言是项目自身。
//
#pragma warning(disable:4311)
inline bool WINAPI LanguageLoadSelf(fk::LModule* pmodule, HINSTANCE hInstance)
{
	if (hInstance!=NULL)
	{
#ifdef _AFX
		AfxSetResourceHandle(hInstance);
		return true;
#endif


#ifdef _ATL
		pmodule->hRes = hInstance;
		ATL::_AtlBaseModule.SetResourceInstance(hInstance);
		return true;
#endif
	}

	return false;
}

#pragma warning(default:4311)




#if _FK_OLD_VER<=0x0001
//
//--------------------------------------------------------------------------
//  这个类以后也会被移除。
//
b_fncall AllocCommandID(UINT uiCmdCount, fkCommandRange* ppcmdRange);	// 这个函数以后也废除。

class _FK_OUT_CLASS LBaseBuildUI : public fk::LObject
{
public:
	LBaseBuildUI(void);
	virtual ~LBaseBuildUI();

	virtual fk::HXMLCONFIG _call GetConfigHandle() = 0;
	virtual b_call ResFileOpen() = 0;
	virtual b_call ResFileClose() = 0;
	virtual wchar_t* GetFileName() = 0;
};
#endif

_FK_END

#endif
