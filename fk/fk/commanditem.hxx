//*****************************************************************************
//
//	FK应用程序框架 1.7
//
//	CommandItem.hxx
//
//	Copyright (C) 2006-2009 Softg Studio All Rights Reserved.
//	
//	Website: http://www.conching.net
//
//  E-Mail: HotSoftG@hotmail.com
//
//	author: 许宗森
//
//	purpose:
//
//*****************************************************************************

#ifndef		__CommandItem_h__
#define		__CommandItem_h__

_FK_BEGIN

class CMenuBar;
class CPopupMenuBar;
class LCommandItemInfos;

/*
** 没有多么大用处,即将删除。
*/
enum CommandObjectType
{
	CommandObjectTypeMenu	= 0x01,
	CommandObjectTypeButton=0x02,
};

//typedef
//[
//	uuid(F30B4753-6E39-49b3-A389-56ACEB66997D),
//	helpstring("")
//]
enum enumImageList
{
	umImageListResource=100,
	umImageListFile=101,
	umImageListImageList=102,
};
enum umCOMMANDSTRUCT_FLAGS
{
	CSF_BYCOMMAND		= 0x00000000L,
	CSF_BYPOSITION		= 0x00000400L,
};


class CVariantImageList : public ATL::CComVariant
{
public:
	CVariantImageList(fk::enumImageList imageListType, LONG pLongData)
	{
		this->vt	= imageListType;
		this->lVal	= pLongData;
	}
	~CVariantImageList()
	{
	}
};

enum _umButtonMask
{
	bmXmlPath	=0x00000001,
	bmPopupClsid=0x00000002,
};

struct FKTBBUTTON : public TBBUTTON
{
	DWORD cbSize;
	DWORD dwMask;
	char* pszXmlPath;
	CLSID popupClsid;
	HBITMAP hBitmap;
	INotifyCommand* pNotifyCommand;
};

enum umCOMMANDSTRUCT_MAKE
{
	CSM_STATE = 0x00000001,
	CSM_ID = 0x00000002,
	CSM_SUBMENU = 0x00000004,
	CSM_CHECKMARKS = 0x00000008,
	CSM_DATA = 0x00000020,
	CSM_STRING = 0x00000040,
	CSM_BITMAP = 0x00000080,		//	CSM_HBITMAP				= 0x00000100,
	CSM_HICON = 0x00000200,
	CSM_GUID = 0x00000400,
	CSM_RES_NAME = 0x00000800,
	CSM_SEPARATOR = 0x00001000,
	CSM_NOTIFYCOMMAND = 0x00002000,
	CSM_MENUBARBREAK = 0x00004000,
	CSM_CHILD_NOTIFYCOMMAND = 0x00008000,
	CSM_CHILD_POPUPCOMMAND = 0x00010000,
	CSM_CHILD_INSERTCOMMAND = 0x00020000,
};


/*
只在标准windows标准窗口按钮,菜单中使用。
命令分为以下几种状态,按数据顺序读入：
1. 菜单分格条项。
2. 菜单项。

make 
CmdItemType 
id 
caption 

short 
bmp

CmdGuid

key
frl
*/
//DWORD dwSize;
//UINT fMask;
//DWORD dwState;  ***
//int nCommandId;
//wchar_t *szText;
//UINT cch;
//HBITMAP hBitmap;
//HICON hIcon;  ***
//INotifyCommand *pNotifyCommand;
//LPARAM dwItemData;
//GUID *pGuid;  ***

class _FK_OUT_CLASS CommandItemInfo : public fk::LControl
{
	_FK_RTTI_;

private:
	fk::LCommandItemInfos* _pCommandItemInfos;

public:
	CommandItemInfo(fk::LObject* pparent);
	virtual ~CommandItemInfo();

	DWORD				_dwMask;				// 
	DWORD				_dwState;
	enumCmdItemType		_umCmdItemType;	
	CommandObjectType	_umCommandObjectType;	// ** 没有多么大用处, 即将删除。
	UINT				_uiCommandID;			// 命令ID。
	fk::LStringw		_scaption;				// 命令标题。
	HBITMAP				_hbitmap;
	data_t				_dwItemData;			// 命令项附加参数。	fk 库使用的参数。
	LPARAM				_lparam;				// 命令项附加参数。  user 使用的参数。
	fk::LStringa		_sfrl;					// 存在 frl 属性，当前菜单项，存在弹出菜单。

	LGuid*				_pGuidCmd;				// 
	INotifyCommand*		_pNotifyCmd;			// 命令项接收项。

	ICommandBar*		_pCommandBar;			// 命令项所在的命令条。

	HWND				_hwnd;

	//CLSID*			_pPopupClsid;
	//GUID*				_pGuidKey;
	char*				_szXmlPopup;			// 
	fk::LStringw		_BaseValue;

	virtual v_call clear(void) = 0;
	virtual b_call CreateMenuCommandBar() = 0;

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv)=0;
	virtual b_call SetGuidKey(GUID* pguid) = 0;

	// CommandItemInfo
	virtual bool SetPopupCmd(LPCSTR szXmlPopup) = 0;
	virtual bool SetPopupClsid(CLSID* pPopupClsid) = 0;
	virtual void SetWindowWidth(UINT nWidth) = 0;

	virtual b_call ToCommandStruct(MENUITEMINFO* pmii, COMMANDSTRUCT* pcmds) = 0;
	virtual b_call ToMenuItemInfo(MENUITEMINFO* pmii) = 0;

	virtual IID* GetClsid() = 0;

	virtual b_call CreateChildNotifyCommandSelf(INotifyCommand** ppNotifyCommand) = 0;
	virtual b_call CreateChildNotifyCommand(INotifyCommand* pNotifyCommandParent, INotifyCommand** ppNotifyCommand) = 0;

	// 从 xml 读入命令项。
	virtual b_call ReadCommandItemInfo(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode) = 0;

	static b_fncall NewCommandItemInfo(fk::enumClassValue cvClassValue, CommandItemInfo** ppv);
};

class LCommandItemInfos : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LCommandItemInfos();
	virtual ~LCommandItemInfos();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual v_call clear(void);

	CommandItemInfo* CreateCommandItemInfo();

	void AddCmdItem(CommandItemInfo *pcii);
	void RemoveAll();
	bool DeleteItem(CommandItemInfo *pcii);
	bool IsExist(CommandItemInfo *pcmdInfo);

	data_t* GetDatas(void);
	i_call GetCount(void);
	b_call GetItem(int iitem, CommandItemInfo** pcii);

	virtual b_call ReadCommandItemInfos(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode);

#ifdef _DEBUG
	static b_fncall CommandItemInfosCreate(fk::enumClassValue cvClassValue, LCommandItemInfos** ppv, fk::LAutoPtr* pap,
		LPCWSTR szFileName=__TFILE__,
		LPCWSTR szFunction=L"",
		UINT uiLine=__LINE__
		);
#else
	static b_fncall CommandItemInfosCreate(fk::enumClassValue cvClassValue, LCommandItemInfos** ppv, fk::LAutoPtr* pap);
#endif

	static b_fncall NewCommandItemInfos(fk::LObject* pparent, fk::LCommandItemInfos** ppv, REFIID riid=GUID_NULL);
};

class CCommandMenuItem : public ICommandItem
{
private:
protected:
public:
	CCommandMenuItem(void);
	virtual ~CCommandMenuItem(void);

	virtual void SetCommandItemInfo(CommandItemInfo* pcii) = 0;
	virtual void SetMenu(HMENU hMenu, CMenuBar *pMenuBar = NULL) = 0;
	virtual void SetCmdID(int nCmdID) = 0;
	virtual i_call GetCmdID(void) = 0;
	virtual void SetIndex(int nIndex) = 0;

	virtual b_call IsNextCommand(void) = 0;

	static b_fncall CreateCommandMenuItem(CCommandMenuItem** ppobj);
	static b_fncall DeleteCommandMenuItem(CCommandMenuItem* pobj);
};

#ifdef _DEBUG
//bool WINAPI d_CreateCommandMenuItem(CCommandMenuItem** ppobj, LPCWSTR wsFile, LPCWSTR wsFun, UINT uiLine);
#define  CommandMenuItem_New(ppobj)		fk::CCommandMenuItem::CreateCommandMenuItem(ppobj)		//fk::d_CreateCommandMenuItem(ppobj, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)
#else
#define  CommandMenuItem_New(ppobj)		fk::CCommandMenuItem::CreateCommandMenuItem(ppobj)
#endif

#define  CommandMenuItem_Delete(pobj)	fk::CCommandMenuItem::DeleteCommandMenuItem(pobj)

_FK_END

#endif /* __CommandItem_h__ */

