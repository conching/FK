#ifndef __COMMAND_MODE_H__
#define __COMMAND_MODE_H__

//#include <map>



_FK_BEGIN

//
//	使用方法：
//
// 1.申请一个命令类。
//class CXXXX :public fk::ICommand
//{
//public:
//	CXXXX()
//	{
//	}
//	~CXXXX()
//	{
//	}
//	virtual int Execute();
//	void virtual Command_Undo();
//	void virtual Command_Name(BSTR *bstrName);
//	void virtual Command_Describe(BSTR *bstrName);
//protected:
//private:
//};


//void virtual Command_Name(BSTR *bstrName);
//virtual int Execute();
//void virtual Command_Undo();
//void virtual Command_Describe(BSTR *bstrName);



typedef void* HCOMMAND;

typedef BOOL (WINAPI *LPFNCOMMANDEXECUTE)(HCOMMAND hCommand);
typedef BOOL (WINAPI *LPFNCOMMANDSTATE)(HCOMMAND hCommand);
typedef BOOL (WINAPI *LPFNCOMMANDUNDO)(HCOMMAND hCommand);
typedef BOOL (WINAPI *LPFNCOMMANDFREE)(HCOMMAND hCommand);

typedef struct tagCOMMANDDATA
{
	DWORD dwSize;
	LPFNCOMMANDEXECUTE pfnExcecute;
	LPFNCOMMANDSTATE pfnState;
	LPFNCOMMANDUNDO	pfnUndo;
	LPFNCOMMANDFREE pfnFree;
	LPARAM lParam;
}COMMANDDATA,*LPCOMMANDDATA;



HCOMMAND WINAPI FmCreateNullCommand(void);

HCOMMAND WINAPI FmF8SF57CD20(LPCOMMANDDATA lpCmdData);

BOOL WINAPI FmC9104Ad5dBDC49(HCOMMAND hcmd);

// FmCmdState
BOOL WINAPI FmCC3A354FC0(HCOMMAND hcmd);

// FmCmdUndo
BOOL WINAPI FmC8E3CE908661CFA133D(HCOMMAND hcmd);

// FmCmdExecute
BOOL WINAPI FmACB0437A134257BD(HCOMMAND hcmd);

// FmCmdGetParam
LPARAM WINAPI FmF2CE8828B5DB(HCOMMAND hcmd);

#define FmCreateCommand					FmF8SF57CD20
#define FmCmdClose						FmC9104Ad5dBDC49
#define FmCmdState						FmCC3A354FC0
#define FmCmdUndo						FmC8E3CE908661CFA133D
#define FmCmdExecute					FmACB0437A134257BD
#define FmCmdGetParam					FmF2CE8828B5DB

//
///*
// * Dialog Box Command IDs
// */
//#define IDOK                1
//#define IDCANCEL            2
//#define IDABORT             3
//#define IDRETRY             4
//#define IDIGNORE            5
//#define IDYES               6
//#define IDNO                7
//#if(WINVER >= 0x0400)
//#define IDCLOSE         8
//#define IDHELP          9
//#endif /* WINVER >= 0x0400 */
//
//#if(WINVER >= 0x0500)
//#define IDTRYAGAIN      10
//#define IDCONTINUE      11
//#endif /* WINVER >= 0x0500 */
//
//#if(WINVER >= 0x0501)
//#ifndef IDTIMEOUT
//#define IDTIMEOUT 32000
//#endif
//#endif /* WINVER >= 0x0501 */

#define IDCMD_ERROR    -1

class _FK_OUT_CLASS ICommand
{
private:

protected:

public:
	_FK_DBGCLASSINFO_

	ICommand();
	virtual ~ICommand();
	void virtual Command_Name(BSTR *bstrName);
	virtual int Execute()=0;
	virtual int ThreadRunExecute(QUICKNOTIFYPROC* pQuickNotify, LPARAM lParam);
	BOOL virtual QueryCommandEnadle();
	void virtual Command_Undo()=0;
};

#define EVENT_COMMAND_DELETE		0x1

struct CommandEventDelete
{
	fk::NOTIFYEVENT nEvent;
	ICommand* pcmd;
	int iExecute;
	LPARAM lParam;
};


class CCommandInvoker
{
private:
	ICommand* m_pCommand;

public:
	CCommandInvoker(ICommand *pCommand)
	{
		m_pCommand=pCommand;
	}
	~CCommandInvoker()
	{
		Free();
	}
	void Attach( ICommand* p ) throw()
	{
		ATLASSUME( m_pCommand == NULL );
		m_pCommand=p;
	}
	HRESULT Invoker()
	{
		if(m_pCommand!=NULL)
			return m_pCommand->Execute();
		return S_FALSE;
	}
	void Free()
	{
		if(m_pCommand!=NULL)
		{
			delete m_pCommand;
			m_pCommand=NULL;
		}
	}
	ICommand* GetCommand()	{	return m_pCommand;	}
};


_FK_END

#endif // __COMMAND_MODE_H__
