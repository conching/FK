#ifndef __FKCRTLS_H__
#define __FKCRTLS_H__

#include <commctrl.h>
#include <WinDef.h>
#include "win.hxx"

//
//#ifndef __cplusplus
//	#error ATL requires C++ compilation (use a .cpp suffix)
//#endif

//#ifndef __ATLAPP_H__
//	#error atlctrls.h requires atlapp.h to be included first
//#endif
//
//#ifndef __ATLWIN_H__
//	#error atlctrls.h requires atlwin.h to be included first
//#endif

//#if (_WIN32_IE < 0x0300)
//#error atlctrls.h requires IE Version 3.0 or higher
//#endif
//
//#ifndef _WIN32_WCE
//#include <richedit.h>
//#include <richole.h>
//#elif defined(WIN32_PLATFORM_WFSP) && !defined(_WINUSERM_H_)
//#include <winuserm.h>
//#endif // !_WIN32_WCE

// protect template members from windowsx.h macros
#ifdef _INC_WINDOWSX
#undef GetNextSibling
#undef GetPrevSibling
#endif // _INC_WINDOWSX


///////////////////////////////////////////////////////////////////////////////
// Classes in this file:
//
// LStatic
// LButton
// LListBox
// LComboBox
// LEdit
// LScrollBar
//
//
// LSysListView32
// LSysTreeView32
// LHeaderCtrl
// LToolBarCtrl
// CStatusBarCtrl
// LSysTabControl32
//
// LToolTipCtrl
// LTrackBar32
// LUpDownCtrl
// LProgressBarCtrl
// CHotKeyCtrl
// CAnimateCtrl
// CRichEditCtrl
//
// CDragListBox
//
// CReBarCtrl
// LComboBoxEx
// LSysDateTimePick32
// LMonthCalendarCtrl
//
// CFlatScrollBar
// CIPAddressCtrl
// CPagerCtrl
// CLinkCtrl
//




namespace fk
{\



// {899632B1-9110-4FBF-B3AD-640F896A7790}
static GUID IID_CLASS_GROUP_BOX = { 0x899632b1, 0x9110, 0x4fbf, { 0xb3, 0xad, 0x64, 0xf, 0x89, 0x6a, 0x77, 0x90 } };


// --- Standard Windows controls ---

class _FK_OUT_CLASS LStatic : public LWindow
{
	_FK_RTTI_;

public:
	LStatic(void);
	LStatic(fk::LWindowMuch* pWindowMuch);

	bool CreateStatic(LPCTSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight,
						HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCWSTR GetWndClassName();

#ifndef _WIN32_WCE
	HICON GetIcon() const;
	HICON SetIcon(HICON hIcon);
	HENHMETAFILE GetEnhMetaFile() const;
	HENHMETAFILE SetEnhMetaFile(HENHMETAFILE hMetaFile);
	//HICON GetIcon() const;
	//HICON SetIcon(HICON hIcon);
#endif // _WIN32_WCE

	fk::LBitmapHandle GetBitmap() const;
	fk::LBitmapHandle SetBitmap(HBITMAP hBitmap);
	HCURSOR GetCursor() const;
	HCURSOR SetCursor(HCURSOR hCursor);

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);
};


///////////////////////////////////////////////////////////////////////////////
// LButton - client side for a Windows BUTTON control

#define FK_BUTTON_STYLE		(WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON)

class _FK_OUT_CLASS LButton : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LButton(void);
	LButton(fk::LWindowMuch* pWindowMuch);
	virtual ~LButton();

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateButton(LPCTSTR lpWindowName, DWORD dwStyle, int x, int y, int nWidth, int nHeight,
		HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCTSTR GetWndClassName();
	UINT GetState() const;
	void SetState(BOOL bHighlight);
	int GetCheck() const;
	void SetCheck(int nCheck);
	
	void SetCheck(bool bcheck);
	bool IsCheck(void);

	UINT GetButtonStyle() const;
	void SetButtonStyle(UINT nStyle, BOOL bRedraw = TRUE);
#ifndef _WIN32_WCE
	HICON GetIcon() const;
	HICON SetIcon(HICON hIcon);
	LBitmapHandle GetBitmap() const;
	LBitmapHandle SetBitmap(HBITMAP hBitmap);
#endif // !_WIN32_WCE

#if (_WIN32_WINNT >= 0x0501)
	BOOL GetIdealSize(LPSIZE lpSize) const;
	BOOL GetImageList(PBUTTON_IMAGELIST pButtonImagelist) const;
	BOOL SetImageList(PBUTTON_IMAGELIST pButtonImagelist);
	BOOL GetTextMargin(LPRECT lpRect) const;
	BOOL SetTextMargin(LPRECT lpRect);
#endif // (_WIN32_WINNT >= 0x0501)

#if (WINVER >= 0x0600)
	void SetDontClick(BOOL bDontClick);
#endif // (WINVER >= 0x0600)

#if (_WIN32_WINNT >= 0x0600)
	BOOL SetDropDownState(BOOL bDropDown);
	BOOL GetSplitInfo(PBUTTON_SPLITINFO pSplitInfo) const;
	BOOL SetSplitInfo(PBUTTON_SPLITINFO pSplitInfo);
	int GetNoteLength() const;
	BOOL GetNote(LPWSTR lpstrNoteText, int cchNoteText) const;
	BOOL SetNote(LPCWSTR lpstrNoteText);
	LRESULT SetElevationRequiredState(BOOL bSet);
#endif // (_WIN32_WINNT >= 0x0600)

	// Operations
	void Click();
};

class _FK_OUT_CLASS LCheckBox : public fk::LButton
{
	_FK_RTTI_;
	
private:
public:
	LCheckBox(void);
	LCheckBox(fk::LWindowMuch* pWindowMuch);

	virtual ~LCheckBox(void);
};

class _FK_OUT_CLASS LRadioButton : public fk::LButton
{
	_FK_RTTI_;

private:
public:
	LRadioButton(void);
	LRadioButton(fk::LWindowMuch* pWindowMuch);

	virtual ~LRadioButton(void);
};


class _FK_OUT_CLASS LGroupBox : public fk::LButton
{
	_FK_RTTI_;
	
private:
public:
	LGroupBox(void);
	LGroupBox(fk::LWindowMuch* pWindowMuch);

	virtual ~LGroupBox(void);

	virtual b_call IsClassType(LPGUID pClassIID);

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);
};

///////////////////////////////////////////////////////////////////////////////
// LListBox - client side for a Windows LISTBOX control
class _FK_OUT_CLASS LListBox : public LWindow
{
	_FK_RTTI_;

public:
	LListBox(void);
	LListBox(fk::LWindowMuch* pWindowMuch);

	bool CreateListBox(LPCTSTR lpWindowName, DWORD dwStyle,
						int x, int y, int nWidth, int nHeight, fk::LWindowMuch* pParentWindowMuch, int id, PVOID lpParam
						);

	// Attributes
	static LPCTSTR GetWndClassName();
	// for entire listbox
	int GetCount() const;

#ifndef _WIN32_WCE
	int SetCount(int cItems);
#endif // !_WIN32_WCE

	int GetHorizontalExtent() const;
	void SetHorizontalExtent(int cxExtent);
	int GetTopIndex() const;
	int SetTopIndex(int nIndex);
	LCID GetLocale() const;
	LCID SetLocale(LCID nNewLocale);

#if (WINVER >= 0x0500) && !defined(_WIN32_WCE)
	DWORD GetListBoxInfo() const;
#endif // (WINVER >= 0x0500) && !defined(_WIN32_WCE)

	// for single-selection listboxes
	int GetCurSel() const;
	int SetCurSel(int nSelect);
	// for multiple-selection listboxes
	int GetSel(int nIndex) const;
	int SetSel(int nIndex, BOOL bSelect = TRUE);
	int GetSelCount() const;
	int GetSelItems(int nMaxItems, LPINT rgIndex) const;
	int GetAnchorIndex() const;
	void SetAnchorIndex(int nIndex);
	int GetCaretIndex() const;
	int SetCaretIndex(int nIndex, BOOL bScroll = TRUE);
	// for listbox items
	DWORD_PTR GetItemData(int nIndex) const;
	int SetItemData(int nIndex, DWORD_PTR dwItemData);
	void* GetItemDataPtr(int nIndex) const;
	int SetItemDataPtr(int nIndex, void* pData);
	int GetItemRect(int nIndex, LPRECT lpRect) const;
	int GetText(int nIndex, LPTSTR lpszBuffer) const;
#ifndef _ATL_NO_COM
#ifdef _OLEAUTO_H_
	BOOL GetTextBSTR(int nIndex, BSTR& bstrText) const;
#endif // _OLEAUTO_H_
#endif // !_ATL_NO_COM

#if __FK_USER__
	int GetText(int nIndex, fk::LStringw& strText) const;
#else

#if defined(_WTL_USE_CSTRING) || defined(__ATLSTR_H__)
	int GetText(int nIndex, _CSTRING_NS::CString& strText) const;
#endif // defined(_WTL_USE_CSTRING) || defined(__ATLSTR_H__)

#endif

	int GetTextLen(int nIndex) const;
	int GetItemHeight(int nIndex) const;
	int SetItemHeight(int nIndex, UINT cyItemHeight);
	// Settable only attributes
	void SetColumnWidth(int cxWidth);
	BOOL SetTabStops(int nTabStops, LPINT rgTabStops);
	BOOL SetTabStops();
	BOOL SetTabStops(const int& cxEachStop);
	// Operations
	int InitStorage(int nItems, UINT nBytes);
	void ResetContent();
	UINT ItemFromPoint(POINT pt, BOOL& bOutside) const;
	// manipulating listbox items
	int AddString(LPCTSTR lpszItem);
	int DeleteString(UINT nIndex);
	int InsertString(int nIndex, LPCTSTR lpszItem);
#ifndef _WIN32_WCE
	int Dir(UINT attr, LPCTSTR lpszWildCard);
	int AddFile(LPCTSTR lpstrFileName);
#endif // !_WIN32_WCE

	// selection helpers
	int FindString(int nStartAfter, LPCTSTR lpszItem) const;
	int FindStringExact(int nIndexStart, LPCTSTR lpszFind) const;
	int SelectString(int nStartAfter, LPCTSTR lpszItem);
	int SelItemRange(BOOL bSelect, int nFirstItem, int nLastItem);
#ifdef WIN32_PLATFORM_WFSP   // SmartPhone only messages
	DWORD GetInputMode(BOOL bCurrentMode = TRUE);
	BOOL SetInputMode(DWORD dwMode);
#endif // WIN32_PLATFORM_WFSP

	// fk add

	void clear();
	b_call SetStringws(fk::LStringws* pStringws);
};

/////////////////////////////////////////////////////////////////////////////////
//// LComboBox - client side for a Windows COMBOBOX control
//
#ifndef WIN32_PLATFORM_WFSP   // No COMBOBOX on SmartPhones


class _FK_OUT_CLASS LComboBox : public fk::LWindow
{
	_FK_RTTI_;
	
private:
	fk::LStringw		_sEnumPlusName;
	fk::LEnumPlus*		_pEnumPlus;

	b_call LoadItemsData(void);

	b_call SetEnumPlusObject(LPCWSTR lpszEnumPlusName);

public:
	// Constructors
	LComboBox(void);
	LComboBox(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateComboBox(LPCTSTR lpWindowName, DWORD dwStyle,
						int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	b_call SetEnumPlus(fk::LEnumPlus* pEnumPlus);

	int FindData(ULONG ulData);
	i_call SelectData(ULONG ulData);
	dw_call GetSelData();

	// Attributes
	static LPCTSTR GetWndClassName();
	// for entire combo box
	int GetCount() const;

	int GetCurSel() const;

	int SetCurSel(int nSelect);

	LCID GetLocale() const;

	LCID SetLocale(LCID nNewLocale);

	int GetTopIndex() const;

	int SetTopIndex(int nIndex);

	UINT GetHorizontalExtent() const;
	void SetHorizontalExtent(UINT nExtent);
	int GetDroppedWidth() const;
	int SetDroppedWidth(UINT nWidth);
#if ((WINVER >= 0x0500) && !defined(_WIN32_WCE)) || (defined(_WIN32_WCE) && (_WIN32_WCE >= 420))
	BOOL GetComboBoxInfo(PCOMBOBOXINFO pComboBoxInfo) const;
#endif // ((WINVER >= 0x0500) && !defined(_WIN32_WCE)) || (defined(_WIN32_WCE) && (_WIN32_WCE >= 420))

	// for edit control
	DWORD GetEditSel() const;
	BOOL SetEditSel(int nStartChar, int nEndChar);
	// for combobox item
	LONG_PTR GetItemData(int nIndex) const;
	int SetItemData(int nIndex, DWORD_PTR dwItemData);
	void* GetItemDataPtr(int nIndex) const;
	int SetItemDataPtr(int nIndex, void* pData);
	int GetLBText(int nIndex, LPTSTR lpszText) const;

#ifndef _ATL_NO_COM
	BOOL GetLBTextBSTR(int nIndex, BSTR& bstrText) const;
#endif // !_ATL_NO_COM

	int GetLBText(int nIndex, fk::LVar& strText) const;

	int GetLBTextLen(int nIndex) const;
	int GetItemHeight(int nIndex) const;
	int SetItemHeight(int nIndex, UINT cyItemHeight);
	BOOL GetExtendedUI() const;
	int SetExtendedUI(BOOL bExtended = TRUE);
	void GetDroppedControlRect(LPRECT lprect) const;
	BOOL GetDroppedState() const;
#if (_WIN32_WINNT >= 0x0501)
	int GetMinVisible() const;
	BOOL SetMinVisible(int nMinVisible);
	// Vista only
	BOOL GetCueBannerText(LPWSTR lpwText, int cchText) const;
	// Vista only
	BOOL SetCueBannerText(LPCWSTR lpcwText);
#endif // (_WIN32_WINNT >= 0x0501)

	// Operations
	int InitStorage(int nItems, UINT nBytes);
	void ResetContent();
	// for edit control
	BOOL LimitText(int nMaxChars);
	// for drop-down combo boxes
	void ShowDropDown(BOOL bShowIt = TRUE);
	// manipulating listbox items
	int AddString(LPCTSTR lpszString);
	int DeleteString(UINT nIndex);
	int InsertString(int nIndex, LPCTSTR lpszString);
#ifndef _WIN32_WCE
	int Dir(UINT attr, LPCTSTR lpszWildCard);
#endif // !_WIN32_WCE

	// selection helpers
	int FindString(int nStartAfter, LPCTSTR lpszString) const;
	int FindStringExact(int nIndexStart, LPCTSTR lpszFind) const;
	int SelectString(int nStartAfter, LPCTSTR lpszString);
	// Clipboard operations
	void clear();
	void Copy();
	void Cut();
	void Paste();

public:
    enum enumPropID
    {
        PROPID_COMBOBOX_ENUMPLUS = 1400,			// ComboBox 控件使用 EnumPlus 对象。 数据类型fk::dtStringw,  指针 fk::LStringw*;
    };
};


#endif // !WIN32_PLATFORM_WFSP


///////////////////////////////////////////////////////////////////////////////
// LEdit - client side for a Windows EDIT control

enum enumEDIT_STYLE
{
	ESF_FK_ENTER		= 0x00000001,	// 窗口风格向父窗口最顶层次发送回车事件。			SetPropValue(fk::PROPID_FK_STYLE, fk::PIDC_ADD, fk::ESF_FK_ENTER)
	ESF_FK_ENTER_LAYER	= 0x00000002,	// 每一层都发送。
	ESF_FK_ENTER_LAYER1	= 0x00000010,	// 定义次层数. 1 层。
	ESF_FK_ENTER_LAYER2	= 0x00000020,	// 定义次层数. 2 层。
};
enum enumEDIT_FK_EVENT
{
	EN_FK_ENTER = 0x1,				// 回车事件。
};

class _FK_OUT_CLASS LEdit : public LWindow
{
	_FK_RTTI_;

private:
	DWORD _dwFKEditStyle;

public:
	// Constructors
	LEdit(void);
	LEdit(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateEdit(DWORD dwExStyle, LPCTSTR lpWindowName, DWORD dwStyle,
					int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

	// Attributes
	static LPCTSTR GetWndClassName();
	BOOL CanUndo() const;
	int GetLineCount() const;
	BOOL GetModify() const;
	void SetModify(BOOL bModified = TRUE);
	void GetRect(LPRECT lpRect) const;
	DWORD GetSel() const;
	void GetSel(int& nStartChar, int& nEndChar) const;
#ifndef _WIN32_WCE
	HLOCAL GetHandle() const;
	void SetHandle(HLOCAL hBuffer);
#endif // !_WIN32_WCE

	DWORD GetMargins() const;
	void SetMargins(UINT nLeft, UINT nRight);
	UINT GetLimitText() const;
	void SetLimitText(UINT nMax);
	POINT PosFromChar(UINT nChar) const;
	int CharFromPos(POINT pt, int* pLine = NULL) const;
	// NOTE: first word in lpszBuffer must contain the size of the buffer!
	int GetLine(int nIndex, LPTSTR lpszBuffer) const;
	int GetLine(int nIndex, LPTSTR lpszBuffer, int nMaxLength) const;
	TCHAR GetPasswordChar() const;
	void SetPasswordChar(TCHAR ch);
#ifndef _WIN32_WCE
	EDITWORDBREAKPROC GetWordBreakProc() const;
	void SetWordBreakProc(EDITWORDBREAKPROC ewbprc);
#endif // !_WIN32_WCE

	int GetFirstVisibleLine() const;
#ifndef _WIN32_WCE
	int GetThumb() const;
#endif // !_WIN32_WCE

	BOOL SetReadOnly(BOOL bReadOnly = TRUE);
#if (WINVER >= 0x0500) && !defined(_WIN32_WCE)
	UINT GetImeStatus(UINT uStatus) const;
	UINT SetImeStatus(UINT uStatus, UINT uData);
#endif // (WINVER >= 0x0500) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0501)
	BOOL GetCueBannerText(LPCWSTR lpstrText, int cchText) const;
	// bKeepWithFocus - Vista only
	BOOL SetCueBannerText(LPCWSTR lpstrText, BOOL bKeepWithFocus = FALSE);
#endif // (_WIN32_WINNT >= 0x0501)

	// Operations
	void EmptyUndoBuffer();
	BOOL FmtLines(BOOL bAddEOL);
	void LimitText(int nChars = 0);
	int LineFromChar(int nIndex = -1) const;
	int LineIndex(int nLine = -1) const;
	int LineLength(int nLine = -1) const;
	void LineScroll(int nLines, int nChars = 0);
	void ReplaceSel(LPCTSTR lpszNewText, BOOL bCanUndo = FALSE);
	void SetRect(LPCRECT lpRect);
	void SetRectNP(LPCRECT lpRect);
	void SetSel(DWORD dwSelection, BOOL bNoScroll = FALSE);
	void SetSel(int nStartChar, int nEndChar, BOOL bNoScroll = FALSE);
	void SetSelAll(BOOL bNoScroll = FALSE);
	void SetSelNone(BOOL bNoScroll = FALSE);
	BOOL SetTabStops(int nTabStops, LPINT rgTabStops);
	BOOL SetTabStops();
	BOOL SetTabStops(const int& cxEachStop);    // takes an 'int';
	void ScrollCaret();
	int Scroll(int nScrollAction);
	void InsertText(int nInsertAfterChar, LPCTSTR lpstrText, BOOL bNoScroll = FALSE, BOOL bCanUndo = FALSE);
	void AppendText(LPCTSTR lpstrText, BOOL bNoScroll = FALSE, BOOL bCanUndo = FALSE);

#if (_WIN32_WINNT >= 0x0501)
	BOOL ShowBalloonTip(PEDITBALLOONTIP pEditBaloonTip);
	BOOL HideBalloonTip();
#endif // (_WIN32_WINNT >= 0x0501)

#if (_WIN32_WINNT >= 0x0600)
	DWORD GetHilite() const;
	void GetHilite(int& nStartChar, int& nEndChar) const;
	void SetHilite(int nStartChar, int nEndChar);
#endif // (_WIN32_WINNT >= 0x0600)

	// Clipboard operations
	BOOL Undo();
	void clear();
	void Copy();
	void Cut();
	void Paste();
#ifdef WIN32_PLATFORM_WFSP   // SmartPhone only messages
	DWORD GetExtendedStyle();
	DWORD SetExtendedStyle(DWORD dwMask, DWORD dwExStyle);
	DWORD GetInputMode(BOOL bCurrentMode = TRUE);
	BOOL SetInputMode(DWORD dwMode);
	BOOL SetSymbols(LPCTSTR szSymbols);
	BOOL ResetSymbols();
#endif // WIN32_PLATFORM_WFSP

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);
};


///////////////////////////////////////////////////////////////////////////////
// LScrollBar - client side for a Windows SCROLLBAR control

class _FK_OUT_CLASS LScrollBar : public LWindow
{
	_FK_RTTI_;

public:
	LScrollBar(void);
	LScrollBar(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	// Attributes
	static LPCTSTR GetWndClassName();
#ifndef _WIN32_WCE
	int GetScrollPos() const;
#endif // !_WIN32_WCE

	int SetScrollPos(int nPos, BOOL bRedraw = TRUE);
#ifndef _WIN32_WCE
	void GetScrollRange(LPINT lpMinPos, LPINT lpMaxPos) const;
#endif // !_WIN32_WCE

	void SetScrollRange(int nMinPos, int nMaxPos, BOOL bRedraw = TRUE)
		;
	BOOL GetScrollInfo(LPSCROLLINFO lpScrollInfo) const;
	int SetScrollInfo(LPSCROLLINFO lpScrollInfo, BOOL bRedraw = TRUE);
#ifndef _WIN32_WCE
	int GetScrollLimit() const;
#if (WINVER >= 0x0500)
	BOOL GetScrollBarInfo(PSCROLLBARINFO pScrollBarInfo) const;
#endif // (WINVER >= 0x0500)

	// Operations
	void ShowScrollBar(BOOL bShow = TRUE);
	BOOL EnableScrollBar(UINT nArrowFlags = ESB_ENABLE_BOTH);
#endif // !_WIN32_WCE
};

///////////////////////////////////////////////////////////////////////////////
// LImageList
class _FK_OUT_CLASS LImageList
{
public:
	HIMAGELIST m_hImageList;

	// Constructor
	LImageList(HIMAGELIST hImageList = NULL);
	// Operators, etc.
	LImageList& operator =(HIMAGELIST hImageList)
	{
		m_hImageList = hImageList;
		return *this;
	}

	operator HIMAGELIST() const { return m_hImageList; }

	void Attach(HIMAGELIST hImageList);

	HIMAGELIST Detach();

	bool IsNull() const;

	// Attributes
	int GetImageCount() const;

	COLORREF GetBkColor() const;
	COLORREF SetBkColor(COLORREF cr);
	BOOL GetImageInfo(int nImage, IMAGEINFO* pImageInfo) const;
	HICON GetIcon(int nIndex, UINT uFlags = ILD_NORMAL) const;
	BOOL GetIconSize(int& cx, int& cy) const;
	BOOL GetIconSize(SIZE& size) const;
	BOOL SetIconSize(int cx, int cy);
	BOOL SetIconSize(SIZE size);
	BOOL SetImageCount(UINT uNewCount);
	BOOL SetOverlayImage(int nImage, int nOverlay);
	// Operations
	BOOL Create(int cx, int cy, UINT nFlags, int nInitial, int nGrow);
	BOOL Create(LPCTSTR bitmap, int cx, int nGrow, COLORREF crMask);
	BOOL CreateFromImage(LPCTSTR image, int cx, int nGrow, COLORREF crMask, UINT uType, UINT uFlags = LR_DEFAULTCOLOR | LR_DEFAULTSIZE);
	BOOL Merge(HIMAGELIST hImageList1, int nImage1, HIMAGELIST hImageList2, int nImage2, int dx, int dy);
#ifndef _WIN32_WCE
#ifdef __IStream_INTERFACE_DEFINED__
	BOOL CreateFromStream(LPSTREAM lpStream);
#endif // __IStream_INTERFACE_DEFINED__
#endif // !_WIN32_WCE

	BOOL Destroy();
	int Add(HBITMAP hBitmap, HBITMAP hBitmapMask = NULL);
	int Add(HBITMAP hBitmap, COLORREF crMask);
	BOOL Remove(int nImage);
	BOOL RemoveAll();
	BOOL Replace(int nImage, HBITMAP hBitmap, HBITMAP hBitmapMask);
	int AddIcon(HICON hIcon);
	int ReplaceIcon(int nImage, HICON hIcon);
	HICON ExtractIcon(int nImage);
	BOOL Draw(HDC hDC, int nImage, int x, int y, UINT nStyle);
	BOOL Draw(HDC hDC, int nImage, POINT pt, UINT nStyle);
	BOOL DrawEx(int nImage, HDC hDC, int x, int y, int dx, int dy, COLORREF rgbBk, COLORREF rgbFg, UINT fStyle);
	BOOL DrawEx(int nImage, HDC hDC, RECT& rect, COLORREF rgbBk, COLORREF rgbFg, UINT fStyle);
	static BOOL DrawIndirect(IMAGELISTDRAWPARAMS* pimldp);
	BOOL Copy(int nSrc, int nDst, UINT uFlags = ILCF_MOVE);
#ifdef __IStream_INTERFACE_DEFINED__
#ifndef _WIN32_WCE
	static HIMAGELIST Read(LPSTREAM lpStream);
	BOOL Write(LPSTREAM lpStream);
#endif // !_WIN32_WCE

#if (_WIN32_WINNT >= 0x0501)
	static HRESULT ReadEx(DWORD dwFlags, LPSTREAM lpStream, REFIID riid, PVOID* ppv);
	HRESULT WriteEx(DWORD dwFlags, LPSTREAM lpStream);
#endif // (_WIN32_WINNT >= 0x0501)
#endif // __IStream_INTERFACE_DEFINED__

	// Drag operations
	BOOL BeginDrag(int nImage, POINT ptHotSpot);
	BOOL BeginDrag(int nImage, int xHotSpot, int yHotSpot);
	static void EndDrag();
	static BOOL DragMove(POINT pt);
	static BOOL DragMove(int x, int y);
	BOOL SetDragCursorImage(int nDrag, POINT ptHotSpot);
	BOOL SetDragCursorImage(int nDrag, int xHotSpot, int yHotSpot);
	static BOOL DragShowNolock(BOOL bShow = TRUE);
	static LImageList GetDragImage(LPPOINT lpPoint, LPPOINT lpPointHotSpot);
	static BOOL DragEnter(HWND hWnd, POINT point)
		;
	static BOOL DragEnter(HWND hWnd, int x, int y);
	static BOOL DragLeave(HWND hWnd);
#if (_WIN32_IE >= 0x0400)
	LImageList Duplicate() const;
	static LImageList Duplicate(HIMAGELIST hImageList)
		;
#endif // (_WIN32_IE >= 0x0400)
};


///////////////////////////////////////////////////////////////////////////////
// LToolTipCtrl

class _FK_OUT_CLASS LToolInfo : public TOOLINFO
{
public:
	LToolInfo(UINT nFlags, HWND hWnd, UINT nIDTool = 0, LPRECT lpRect = NULL, LPTSTR lpstrText = LPSTR_TEXTCALLBACK, LPARAM lUserParam = NULL);

	operator LPTOOLINFO() { return this; }

	operator LPARAM() { return (LPARAM)this; }

	void Init(UINT nFlags, HWND hWnd, UINT nIDTool = 0, LPRECT lpRect = NULL, LPTSTR lpstrText = LPSTR_TEXTCALLBACK, LPARAM lUserParam = NULL);
};


///////////////////////////////////////////////////////////////////////////////
// LToolTipCtrl
//
class _FK_OUT_CLASS LToolTipCtrl : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LToolTipCtrl(void);
	LToolTipCtrl(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect, LPCTSTR szWindowName,
		DWORD dwStyle, DWORD dwExStyle,
		HMENU MenuOrID, LPVOID lpCreateParam);

	bool CreateToolTip(HWND hWndParent, LPCWSTR lpszWindowName, LPVOID lpCreateParam);

	// Attributes
	LPCTSTR GetWndClassName();
	void GetText(LPTOOLINFO lpToolInfo) const;
	void GetText(LPTSTR lpstrText, HWND hWnd, UINT nIDTool = 0) const;
	BOOL GetToolInfo(LPTOOLINFO lpToolInfo) const;
	BOOL GetToolInfo(HWND hWnd, UINT nIDTool, UINT* puFlags, LPRECT lpRect, LPTSTR lpstrText) const;
	void SetToolInfo(LPTOOLINFO lpToolInfo);
	void SetToolRect(LPTOOLINFO lpToolInfo);
	void SetToolRect(HWND hWnd, UINT nIDTool, LPCRECT lpRect);
	int GetToolCount() const;
	int GetDelayTime(DWORD dwType) const;
	void SetDelayTime(DWORD dwType, int nTime);
	void GetMargin(LPRECT lpRect) const;
	void SetMargin(LPRECT lpRect);
	int GetMaxTipWidth() const;
	int SetMaxTipWidth(int nWidth);
	COLORREF GetTipBkColor() const;
	void SetTipBkColor(COLORREF clr);
	COLORREF GetTipTextColor() const;

	void SetTipTextColor(COLORREF clr);

	BOOL GetCurrentTool(LPTOOLINFO lpToolInfo) const;
#if (_WIN32_IE >= 0x0500)
	SIZE GetBubbleSize(LPTOOLINFO lpToolInfo) const;
	BOOL SetTitle(UINT uIcon, LPCTSTR lpstrTitle);
#endif // (_WIN32_IE >= 0x0500)

#if (_WIN32_WINNT >= 0x0501)
	void GetTitle(PTTGETTITLE pTTGetTitle) const;
	void SetWindowTheme(LPCWSTR lpstrTheme);
#endif // (_WIN32_WINNT >= 0x0501)

	// Operations
	void Activate(BOOL bActivate);
	BOOL AddTool(LPTOOLINFO lpToolInfo);
	BOOL AddTool(HWND hWnd, LPCTSTR text = LPSTR_TEXTCALLBACK, LPCRECT lpRectTool = NULL, UINT nIDTool = 0);
	void DelTool(LPTOOLINFO lpToolInfo);
	void DelTool(HWND hWnd, UINT nIDTool = 0);
	BOOL HitTest(LPTTHITTESTINFO lpHitTestInfo) const;
	BOOL HitTest(HWND hWnd, POINT pt, LPTOOLINFO lpToolInfo) const;
	void RelayEvent(LPMSG lpMsg);
	void UpdateTipText(LPTOOLINFO lpToolInfo);
	void UpdateTipText(LPCTSTR text, HWND hWnd, UINT nIDTool = 0);
	BOOL EnumTools(UINT nTool, LPTOOLINFO lpToolInfo) const;
	void Pop();
	void TrackActivate(LPTOOLINFO lpToolInfo, BOOL bActivate);
	void TrackPosition(int xPos, int yPos);
#if (_WIN32_IE >= 0x0400)
	void LToolTipCtrl::Update();
#endif // (_WIN32_IE >= 0x0400)

#if (_WIN32_IE >= 0x0500)
	BOOL AdjustRect(LPRECT lpRect, BOOL bLarger /*= TRUE*/);
#endif // (_WIN32_IE >= 0x0500)

#if (_WIN32_WINNT >= 0x0501)
	void LToolTipCtrl::Popup();
#endif // (_WIN32_WINNT >= 0x0501)

};


///////////////////////////////////////////////////////////////////////////////
// LHeaderCtrl

class _FK_OUT_CLASS LHeaderCtrl : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LHeaderCtrl(void);
	LHeaderCtrl(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect, LPCTSTR szWindowName,
		DWORD dwStyle, DWORD dwExStyle,
		HMENU MenuOrID, LPVOID lpCreateParam);
	// Attributes
	LPCTSTR GetWndClassName();
	int GetItemCount() const;
	BOOL GetItem(int nIndex, LPHDITEM pHeaderItem) const;
	BOOL SetItem(int nIndex, LPHDITEM pHeaderItem);
	LImageList GetImageList() const;
	LImageList SetImageList(HIMAGELIST hImageList);
	BOOL GetOrderArray(int nSize, int* lpnArray) const;
	BOOL SetOrderArray(int nSize, int* lpnArray);
	BOOL GetItemRect(int nIndex, LPRECT lpItemRect) const;
	int SetHotDivider(BOOL bPos, DWORD dwInputValue);
#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	int GetBitmapMargin() const;
	int SetBitmapMargin(int nWidth);
	int SetFilterChangeTimeout(DWORD dwTimeOut);
#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0600)
	BOOL GetItemDropDownRect(int nIndex, LPRECT lpRect) const;
	BOOL GetOverflowRect(LPRECT lpRect) const;
	int GetFocusedItem() const;
	BOOL SetFocusedItem(int nIndex);
#endif // (_WIN32_WINNT >= 0x0600)

	// Operations
	int InsertItem(int nIndex, LPHDITEM phdi);
	int AddItem(LPHDITEM phdi);
	BOOL DeleteItem(int nIndex);

	BOOL Layout(HD_LAYOUT* pHeaderLayout);
	int HitTest(LPHDHITTESTINFO lpHitTestInfo) const;
	int OrderToIndex(int nOrder);
	LImageList CreateDragImage(int nIndex);
#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	int EditFilter(int nColumn, BOOL bDiscardChanges);

	int ClearFilter(int nColumn);
	int ClearAllFilters();
#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
};


class _FK_OUT_CLASS LHeaderCtrlHandle : public LHeaderCtrl
{
	_FK_RTTI_;
	
private:
protected:
public:
	LHeaderCtrlHandle(void);
	virtual ~LHeaderCtrlHandle(void);

	LHeaderCtrlHandle& operator =(HWND hWnd)
	{
		_hWnd = hWnd;
		return *this;
	}
};


///////////////////////////////////////////////////////////////////////////////
// LToolBarCtrl

class _FK_OUT_CLASS LToolBarCtrl : public LWindow
{
	_FK_RTTI_;

public:

public:
// Construction
	LToolBarCtrl(void);
	LToolBarCtrl(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
			DWORD dwStyle = 0, DWORD dwExStyle = 0,
			HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	HWND _call CreateToolbarEx(HWND hWndParent, DWORD ws, UINT wID, int nBitmaps,
		HINSTANCE hBMInst, UINT_PTR wBMID, LPCTBBUTTON lpButtons,
		int iNumButtons, int dxButton, int dyButton,
		int dxBitmap, int dyBitmap, UINT uStructSize);

// Attributes
	static LPCTSTR GetWndClassName();
	BOOL IsButtonEnabled(int nID) const;
	BOOL IsButtonChecked(int nID) const;
	BOOL IsButtonPressed(int nID) const;
	BOOL IsButtonHidden(int nID) const;
	BOOL IsButtonIndeterminate(int nID) const;
	int GetState(int nID) const;
	BOOL SetState(int nID, UINT nState);
	BOOL GetButton(int nIndex, LPTBBUTTON lpButton) const;
	int GetButtonCount() const;
	BOOL GetItemRect(int nIndex, LPRECT lpRect) const;
	void SetButtonStructSize(int nSize = sizeof(TBBUTTON));
	BOOL SetButtonSize(SIZE size);
	BOOL SetButtonSize(int cx, int cy);
	BOOL SetBitmapSize(SIZE size);
	BOOL SetBitmapSize(int cx, int cy);
#ifndef _WIN32_WCE
	HWND GetToolTips() const;
	void SetToolTips(HWND hWndToolTip);
#endif // !_WIN32_WCE

	void SetNotifyWnd(HWND hWnd);
	int GetRows() const;
	void SetRows(int nRows, BOOL bLarger, LPRECT lpRect);
	BOOL SetCmdID(int nIndex, UINT nID);
	DWORD GetBitmapFlags() const;
	int GetBitmap(int nID) const;
	int GetButtonText(int nID, LPTSTR lpstrText) const;
	// nIndex - IE5 or higher only
	LImageList GetImageList(int nIndex = 0) const;
	// nIndex - IE5 or higher only
	LImageList SetImageList(HIMAGELIST hImageList, int nIndex = 0);
	// nIndex - IE5 or higher only
	LImageList GetDisabledImageList(int nIndex = 0) const;
	// nIndex - IE5 or higher only
	LImageList SetDisabledImageList(HIMAGELIST hImageList, int nIndex = 0);
#ifndef _WIN32_WCE
	// nIndex - IE5 or higher only
	LImageList GetHotImageList(int nIndex = 0) const;
	// nIndex - IE5 or higher only
	LImageList SetHotImageList(HIMAGELIST hImageList, int nIndex = 0);
#endif // !_WIN32_WCE

	DWORD GetStyle() const;
	void SetStyle(DWORD dwStyle);
	DWORD GetButtonSize() const;
	void GetButtonSize(SIZE& size) const;
	BOOL GetRect(int nID, LPRECT lpRect) const;
	int GetTextRows() const;
	BOOL SetButtonWidth(int cxMin, int cxMax);
	BOOL SetIndent(int nIndent);
	BOOL SetMaxTextRows(int nMaxTextRows);
#if (_WIN32_IE >= 0x0400)
#ifndef _WIN32_WCE
	BOOL GetAnchorHighlight() const;
	BOOL SetAnchorHighlight(BOOL bEnable = TRUE);
#endif // !_WIN32_WCE

	int GetButtonInfo(int nID, LPTBBUTTONINFO lptbbi) const;
	BOOL SetButtonInfo(int nID, LPTBBUTTONINFO lptbbi);
	BOOL SetButtonInfo(int nID, DWORD dwMask, BYTE Style, BYTE State, LPCTSTR lpszItem,
	                   int iImage, WORD cx, int iCommand, DWORD_PTR lParam);
#ifndef _WIN32_WCE
	int GetHotItem() const;
	int SetHotItem(int nItem);
#endif // !_WIN32_WCE

	BOOL IsButtonHighlighted(int nButtonID) const;
	DWORD SetDrawTextFlags(DWORD dwMask, DWORD dwFlags);
#ifndef _WIN32_WCE
	BOOL GetColorScheme(LPCOLORSCHEME lpcs) const;
	void SetColorScheme(LPCOLORSCHEME lpcs);
	DWORD GetExtendedStyle() const;
	DWORD SetExtendedStyle(DWORD dwStyle);
	void GetInsertMark(LPTBINSERTMARK lptbim) const;
	void SetInsertMark(LPTBINSERTMARK lptbim);
	COLORREF GetInsertMarkColor() const;
	COLORREF SetInsertMarkColor(COLORREF clr);
	BOOL GetMaxSize(LPSIZE lpSize) const;
	void GetPadding(LPSIZE lpSizePadding) const;
	void SetPadding(int cx, int cy, LPSIZE lpSizePadding = NULL);
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // !_WIN32_WCE
#endif // (_WIN32_IE >= 0x0400)

#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	int GetString(int nString, LPTSTR lpstrString, int cchMaxLen) const;
	int GetStringBSTR(int nString, BSTR& bstrString) const;

	int GetString(int nString, fk::LStringw* pstr) const;

#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0501)
	void GetMetrics(LPTBMETRICS lptbm) const;
	void SetMetrics(LPTBMETRICS lptbm);
	void SetWindowTheme(LPCWSTR lpstrTheme);
#endif // (_WIN32_WINNT >= 0x0501)

#if (_WIN32_WINNT >= 0x0600)
	LImageList GetPressedImageList(int nIndex = 0) const ;
	LImageList SetPressedImageList(HIMAGELIST hImageList, int nIndex = 0);

#endif // (_WIN32_WINNT >= 0x0600)

// Operations
	BOOL EnableButton(int nID, BOOL bEnable = TRUE) ;
	BOOL CheckButton(int nID, BOOL bCheck = TRUE);
	BOOL PressButton(int nID, BOOL bPress = TRUE);
	BOOL HideButton(int nID, BOOL bHide = TRUE);
	BOOL Indeterminate(int nID, BOOL bIndeterminate = TRUE);
	int AddBitmap(int nNumButtons, UINT nBitmapID);
	int AddBitmap(int nNumButtons, HBITMAP hBitmap);
	BOOL AddButtons(int nNumButtons, LPTBBUTTON lpButtons);
	BOOL InsertButton(int nIndex, LPTBBUTTON lpButton);
	BOOL InsertButton(int nIndex, int iCommand, BYTE Style, BYTE State, int iBitmap,
	                  INT_PTR iString, DWORD_PTR lParam);
	BOOL InsertButton(int nIndex, int iCommand, BYTE Style, BYTE State, int iBitmap,
	                  LPCTSTR lpszItem, DWORD_PTR lParam);
	BOOL AddButton(LPTBBUTTON lpButton);
	BOOL AddButton(int iCommand, BYTE Style, BYTE State, int iBitmap, INT_PTR iString, DWORD_PTR lParam);
	BOOL AddButton(int iCommand, BYTE Style, BYTE State, int iBitmap, LPCTSTR lpszItem, DWORD_PTR lParam);
	BOOL DeleteButton(int nIndex);
	UINT CommandToIndex(UINT nID) const;
#ifndef _WIN32_WCE
	void SaveState(HKEY hKeyRoot, LPCTSTR lpszSubKey, LPCTSTR lpszValueName);
	void RestoreState(HKEY hKeyRoot, LPCTSTR lpszSubKey, LPCTSTR lpszValueName);
	void Customize();
#endif // !_WIN32_WCE

	int AddString(UINT nStringID);
	int AddStrings(LPCTSTR lpszStrings);
	void AutoSize();
	BOOL ChangeBitmap(int nID, int nBitmap);
	int LoadImages(int nBitmapID);
	int LoadStdImages(int nBitmapID);
	BOOL ReplaceBitmap(LPTBREPLACEBITMAP ptbrb);
#if (_WIN32_IE >= 0x0400)
	int HitTest(LPPOINT lpPoint) const;
#ifndef _WIN32_WCE
	BOOL InsertMarkHitTest(LPPOINT lpPoint, LPTBINSERTMARK lptbim) const;
	BOOL InsertMarkHitTest(int x, int y, LPTBINSERTMARK lptbim) const;
	BOOL MapAccelerator(TCHAR chAccel, int& nID) const;
	BOOL MarkButton(int nID, BOOL bHighlight = TRUE);
	BOOL MoveButton(int nOldPos, int nNewPos);
	HRESULT GetObject(REFIID iid, LPVOID* ppvObject);
#endif
#endif

};




///////////////////////////////////////////////////////////////////////////////
// CListViewCtrl
class _FK_OUT_CLASS LSysListView32 : public LWindow
{
	_FK_RTTI_;

public:
	LSysListView32(void);
	LSysListView32(fk::LWindow* pWindowParent);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
			DWORD dwStyle = 0, DWORD dwExStyle = 0,
			HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateSysListView32(DWORD dwExStyle, LPCTSTR lpWindowName, DWORD dwStyle,
							 int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCTSTR GetWndClassName();
	COLORREF GetBkColor() const;
	BOOL SetBkColor(COLORREF cr);
	LImageList GetImageList(int nImageListType) const;
	LImageList SetImageList(HIMAGELIST hImageList, int nImageList);
	int GetItemCount() const;
	BOOL SetItemCount(int nItems);
	BOOL GetItem(LPLVITEM pItem) const;
	BOOL SetItem(const LVITEM* pItem);
	BOOL SetItem(int nItem, int nSubItem, UINT nMask, LPCTSTR lpszItem,
		int nImage, UINT nState, UINT nStateMask, LPARAM lParam);
	UINT GetItemState(int nItem, UINT nMask) const;
	BOOL SetItemState(int nItem, UINT nState, UINT nStateMask);
	BOOL SetItemState(int nItem, LPLVITEM pItem);
#ifndef _ATL_NO_COM
	BOOL GetItemText(int nItem, int nSubItem, BSTR& bstrText) const;
#endif // !_ATL_NO_COM

#if __FK_USER__
	int GetItemText(int nItem, int nSubItem, fk::LStringw& strText) const;
	#else
#if defined(_WTL_USE_CSTRING) || defined(__ATLSTR_H__)
	int GetItemText(int nItem, int nSubItem, _CSTRING_NS::CString& strText) const;
#endif // defined(_WTL_USE_CSTRING) || defined(__ATLSTR_H__)

#endif

	int GetItemText(int nItem, int nSubItem, LPTSTR lpszText, int nLen) const;
	BOOL SetItemText(int nItem, int nSubItem, LPCTSTR lpszText);
	DWORD_PTR GetItemData(int nItem) const;
	BOOL SetItemData(int nItem, DWORD_PTR dwData);
	UINT GetCallbackMask() const;
	BOOL SetCallbackMask(UINT nMask);
	BOOL GetItemPosition(int nItem, LPPOINT lpPoint) const;
	BOOL SetItemPosition(int nItem, POINT pt);
	BOOL SetItemPosition(int nItem, int x, int y);
	int GetStringWidth(LPCTSTR lpsz) const;
	HWND GetEditControl() const;
	BOOL GetColumn(int nCol, LVCOLUMN* pColumn) const;
	BOOL SetColumn(int nCol, const LVCOLUMN* pColumn);

	int GetColumnWidth(int nCol) const;
	BOOL SetColumnWidth(int nCol, int cx);
	BOOL GetViewRect(LPRECT lpRect) const;

	COLORREF GetTextColor() const;
	BOOL SetTextColor(COLORREF cr);
	COLORREF GetTextBkColor() const;
	BOOL SetTextBkColor(COLORREF cr);
	int GetTopIndex() const;
	int GetCountPerPage() const;
	BOOL GetOrigin(LPPOINT lpPoint) const;
	UINT GetSelectedCount() const;
	BOOL GetItemRect(int nItem, LPRECT lpRect, UINT nCode) const;

#ifndef _WIN32_WCE
	HCURSOR GetHotCursor() const;

	HCURSOR SetHotCursor(HCURSOR hHotCursor);
	int GetHotItem() const;
	int SetHotItem(int nIndex);
#endif // !_WIN32_WCE

	BOOL GetColumnOrderArray(int nCount, int* lpnArray) const;
	BOOL SetColumnOrderArray(int nCount, int* lpnArray);
	HWND GetHeader() const;
	BOOL GetSubItemRect(int nItem, int nSubItem, int nFlag, LPRECT lpRect) const;
	DWORD SetIconSpacing(int cx, int cy);
	int GetISearchString(LPTSTR lpstr) const;
	void GetItemSpacing(SIZE& sizeSpacing, BOOL bSmallIconView = FALSE) const;
#if (_WIN32_WCE >= 410)
	void SetItemSpacing(INT cySpacing);
#endif // (_WIN32_WCE >= 410)

	// single-selection only
	int GetSelectedIndex() const;
	BOOL GetSelectedItem(LPLVITEM pItem) const;
	// extended list view styles
	DWORD GetExtendedListViewStyle() const;
	// dwExMask = 0 means all styles
	DWORD SetExtendedListViewStyle(DWORD dwExStyle, DWORD dwExMask = 0);
	// checkboxes only
	BOOL GetCheckState(int nIndex) const;
	BOOL SetCheckState(int nItem, BOOL bCheck);
	// view type
	DWORD GetViewType() const;
	DWORD SetViewType(DWORD dwType);
#if (_WIN32_IE >= 0x0400)
#ifndef _WIN32_WCE
	BOOL GetBkImage(LPLVBKIMAGE plvbki) const;
	BOOL SetBkImage(LPLVBKIMAGE plvbki);
#endif // !_WIN32_WCE

	int GetSelectionMark() const;

	int SetSelectionMark(int nIndex);

#ifndef _WIN32_WCE
	BOOL GetWorkAreas(int nWorkAreas, LPRECT lpRect) const;
	BOOL SetWorkAreas(int nWorkAreas, LPRECT lpRect);
	DWORD GetHoverTime() const;
	DWORD SetHoverTime(DWORD dwHoverTime);
	BOOL GetNumberOfWorkAreas(int* pnWorkAreas) const;
#endif // !_WIN32_WCE

	BOOL SetItemCountEx(int nItems, DWORD dwFlags);
#ifndef _WIN32_WCE
	HWND GetToolTips() const;

	HWND SetToolTips(HWND hWndTT);
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // !_WIN32_WCE
#endif // (_WIN32_IE >= 0x0400)

#if (_WIN32_WINNT >= 0x0501)
	int GetSelectedColumn() const;
	void SetSelectedColumn(int nColumn);
	DWORD GetView() const;
	int SetView(DWORD dwView);
	BOOL IsGroupViewEnabled() const;
	int GetGroupInfo(int nGroupID, PLVGROUP pGroup) const;
	int SetGroupInfo(int nGroupID, PLVGROUP pGroup);
	void GetGroupMetrics(PLVGROUPMETRICS pGroupMetrics) const;
	void SetGroupMetrics(PLVGROUPMETRICS pGroupMetrics);
	void GetTileViewInfo(PLVTILEVIEWINFO pTileViewInfo) const;
	BOOL SetTileViewInfo(PLVTILEVIEWINFO pTileViewInfo);
	void GetTileInfo(PLVTILEINFO pTileInfo) const;
	BOOL SetTileInfo(PLVTILEINFO pTileInfo);
	BOOL GetInsertMark(LPLVINSERTMARK pInsertMark) const;
	BOOL SetInsertMark(LPLVINSERTMARK pInsertMark);
	int GetInsertMarkRect(LPRECT lpRect) const;

	COLORREF GetInsertMarkColor() const;
	COLORREF SetInsertMarkColor(COLORREF clr);
	COLORREF GetOutlineColor() const;
	COLORREF SetOutlineColor(COLORREF clr);
#endif // (_WIN32_WINNT >= 0x0501)

#if (_WIN32_WINNT >= 0x0600)
	int GetGroupCount() const;
	BOOL GetGroupInfoByIndex(int nIndex, PLVGROUP pGroup) const;
	BOOL GetGroupRect(int nGroupID, int nType, LPRECT lpRect) const;
	UINT GetGroupState(int nGroupID, UINT uMask) const;
	int GetFocusedGroup() const;
	BOOL GetEmptyText(LPWSTR lpstrText, int cchText) const;
	BOOL GetFooterRect(LPRECT lpRect) const;
	BOOL GetFooterInfo(LPLVFOOTERINFO lpFooterInfo) const;
	BOOL GetFooterItemRect(int nItem, LPRECT lpRect) const;
	BOOL GetFooterItem(int nItem, LPLVFOOTERITEM lpFooterItem) const;
	BOOL GetItemIndexRect(PLVITEMINDEX pItemIndex, int nSubItem, int nType, LPRECT lpRect) const;
	BOOL SetItemIndexState(PLVITEMINDEX pItemIndex, UINT uState, UINT dwMask);
	BOOL GetNextItemIndex(PLVITEMINDEX pItemIndex, WORD wFlags) const;
#endif // (_WIN32_WINNT >= 0x0600)

// Operations
	int InsertColumn(int nCol, const LVCOLUMN* pColumn);
	int InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat = LVCFMT_LEFT,
			int nWidth = -1, int nSubItem = -1, int iImage = -1, int iOrder = -1);
	BOOL DeleteColumn(int nCol);
	int InsertItem(UINT nMask, int nItem, LPCTSTR lpszItem, UINT nState, UINT nStateMask, int nImage, LPARAM lParam);
	int InsertItem(const LVITEM* pItem);
	int InsertItem(int nItem, LPCTSTR lpszItem);
	int InsertItem(int nItem, LPCTSTR lpszItem, int nImage);
	int GetNextItem(int nItem, int nFlags) const;
	BOOL DeleteItem(int nItem);
	BOOL DeleteAllItems();
	int FindItem(LVFINDINFO* pFindInfo, int nStart) const;
	int HitTest(LVHITTESTINFO* pHitTestInfo) const;
	int HitTest(POINT pt, UINT* pFlags) const;
	BOOL EnsureVisible(int nItem, BOOL bPartialOK);
	BOOL Scroll(SIZE size);
	BOOL RedrawItems(int nFirst, int nLast);
	BOOL Arrange(UINT nCode);
	HWND EditLabel(int nItem);
	BOOL Update(int nItem);
	BOOL SortItems(PFNLVCOMPARE pfnCompare, LPARAM lParamSort);
	LImageList RemoveImageList(int nImageList);
	LImageList CreateDragImage(int nItem, LPPOINT lpPoint);
	DWORD ApproximateViewRect(int cx = -1, int cy = -1, int nCount = -1);
	int SubItemHitTest(LPLVHITTESTINFO lpInfo) const;
	int AddColumn(LPCTSTR strItem, int nItem, int nSubItem = -1,
			int nMask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM,
			int nFmt = LVCFMT_LEFT);
	int AddItem(int nItem, int nSubItem, LPCTSTR strItem, int nImageIndex = -1);
#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	BOOL SortItemsEx(PFNLVCOMPARE pfnCompare, LPARAM lParamSort);
#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0501)
	int InsertGroup(int nItem, PLVGROUP pGroup);
	int AddGroup(PLVGROUP pGroup);
	int RemoveGroup(int nGroupID);
	void MoveGroup(int nGroupID, int nItem);
	void MoveItemToGroup(int nItem, int nGroupID);
	int EnableGroupView(BOOL bEnable);
	int SortGroups(PFNLVGROUPCOMPARE pCompareFunc, LPVOID lpVoid = NULL);
	void InsertGroupSorted(PLVINSERTGROUPSORTED pInsertGroupSorted);
	void RemoveAllGroups();
	BOOL HasGroup(int nGroupID);
	BOOL InsertMarkHitTest(LPPOINT lpPoint, LPLVINSERTMARK pInsertMark) const;
	BOOL SetInfoTip(PLVSETINFOTIP pSetInfoTip);
	void CancelEditLabel();
	UINT MapIndexToID(int nIndex) const;
	int MapIDToIndex(UINT uID) const;
#endif // (_WIN32_WINNT >= 0x0501)

#if (_WIN32_WINNT >= 0x0600)
	int HitTestEx(LPLVHITTESTINFO lpHitTestInfo) const;
	int HitTestEx(POINT pt, UINT* pFlags) const;
	int SubItemHitTestEx(LPLVHITTESTINFO lpHitTestInfo) const;
#endif // (_WIN32_WINNT >= 0x0600)

	// single-selection only
	BOOL SelectItem(int nIndex);

	enum umMoveType
	{
		mtFirst = 0x0,		// 第一个
		mtPrev	= 0x1,		// 上一个
		mtCur	= 0x2,		// 当前
		mtNext	= 0x3,		// 下一个
		mtlast	= 0x4,		// 最后一个
	};

	bool MoveItemPos(int iIndex, umMoveType moveType);
	bool DeleteFullColumnsItems();

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	//b_call SetXmlFile(fk::LXmlFile* pxmlFile);
	//b_call GetXmlFile(fk::LXmlFile** pxmlFile);

	//b_call SetXmlPath(LPCWSTR lpszXmlPath);
	//b_call GetXmlPath(fk::LStringw* pszXmlPath);

	enum  enumEvent
	{
		
	};

	enum enumPropID
	{
		PROPID_XmlFile = 3000,
		PROPID_XmlPath = 3001,
	};
};


//
// LSysTreeView32
// 如果有需要添加 FK_NOTIFY_OBJECT_EVENT(_pObjectsSysTreeView32, _pObjectsSysTreeView32->WmNotifyRefract(lpnmhdr));
//
// virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr) = 0;
//
class _FK_OUT_CLASS LSysTreeView32 : public LWindow
{
	_FK_RTTI_;

public:
	LSysTreeView32(void);
	LSysTreeView32(fk::LWindow* pWindowParent);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateTreeViewCtrl(DWORD dwExStyle, LPCTSTR lpWindowName, DWORD dwStyle,
							int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	HTREEITEM FindItemParam(HTREEITEM hitem, LPARAM lparam);
	HTREEITEM FindItemParamFull(HTREEITEM hitem, LPARAM lparam);

	// Attributes
	static LPCTSTR GetWndClassName();

	UINT GetCount() const;
	UINT GetIndent() const;
	void SetIndent(UINT nIndent);
	LImageList GetImageList(int nImageListType = TVSIL_NORMAL) const;
	LImageList SetImageList(HIMAGELIST hImageList, int nImageListType = TVSIL_NORMAL);
	BOOL GetItem(LPTVITEM pItem) const;
	BOOL SetItem(LPTVITEM pItem);
	BOOL SetItem(HTREEITEM hItem, UINT nMask, LPCTSTR lpszItem, int nImage,
		int nSelectedImage, UINT nState, UINT nStateMask, LPARAM lParam);
	BOOL GetItemText(HTREEITEM hItem, LPTSTR lpstrText, int nLen) const;
#ifndef _ATL_NO_COM
	BOOL GetItemText(HTREEITEM hItem, BSTR& bstrText) const;
#endif // !_ATL_NO_COM

	BOOL GetItemText(HTREEITEM hItem, fk::LStringw* pstr) const;

	BOOL SetItemText(HTREEITEM hItem, LPCTSTR lpszItem);
	BOOL GetItemImage(HTREEITEM hItem, int& nImage, int& nSelectedImage) const;
	BOOL SetItemImage(HTREEITEM hItem, int nImage, int nSelectedImage);
	UINT GetItemState(HTREEITEM hItem, UINT nStateMask) const;

	BOOL SetItemState(HTREEITEM hItem, UINT nState, UINT nStateMask);
	DWORD_PTR GetItemData(HTREEITEM hItem) const;
	BOOL SetItemData(HTREEITEM hItem, DWORD_PTR dwData);
	HWND GetEditControl() const;
	UINT GetVisibleCount() const;
	BOOL GetItemRect(HTREEITEM hItem, LPRECT lpRect, BOOL bTextOnly) const;
	BOOL ItemHasChildren(HTREEITEM hItem) const;
#ifndef _WIN32_WCE
	HWND GetToolTips() const;
	HWND SetToolTips(HWND hWndTT);
#endif // !_WIN32_WCE

	int GetISearchString(LPTSTR lpstr) const;
	// checkboxes only
	BOOL GetCheckState(HTREEITEM hItem) const;
	BOOL SetCheckState(HTREEITEM hItem, BOOL bCheck);
#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	COLORREF GetBkColor() const;
	COLORREF SetBkColor(COLORREF clr);
	COLORREF GetInsertMarkColor() const;
	COLORREF SetInsertMarkColor(COLORREF clr);
	int GetItemHeight() const;
	int SetItemHeight(int cyHeight);
	int GetScrollTime() const;
	int SetScrollTime(int nScrollTime);
	COLORREF GetTextColor() const;
	COLORREF SetTextColor(COLORREF clr);
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	COLORREF GetLineColor() const;
	COLORREF SetLineColor(COLORREF clrNew /*= CLR_DEFAULT*/);
#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)

#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	BOOL GetItem(LPTVITEMEX pItem) const;

	BOOL SetItem(LPTVITEMEX pItem);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

	DWORD GetExtendedStyle() const;

	DWORD SetExtendedStyle(DWORD dwStyle, DWORD dwMask);
#if (_WIN32_WINNT >= 0x0600)
	BOOL SetAutoScrollInfo(UINT uPixPerSec, UINT uUpdateTime);
	DWORD GetSelectedCount() const;
	BOOL GetItemPartRect(HTREEITEM hItem, TVITEMPART partID, LPRECT lpRect) const;
#endif // (_WIN32_WINNT >= 0x0600)

	// Operations
	HTREEITEM InsertItem(LPTVINSERTSTRUCT lpInsertStruct);
	HTREEITEM InsertItem(LPCTSTR lpszItem, int nImage,
		int nSelectedImage, HTREEITEM hParent, HTREEITEM hInsertAfter);
	HTREEITEM InsertItem(LPCTSTR lpszItem, HTREEITEM hParent, HTREEITEM hInsertAfter);

	HTREEITEM InsertItem(UINT nMask, LPCTSTR lpszItem, int nImage,
		int nSelectedImage, UINT nState, UINT nStateMask, LPARAM lParam,
		HTREEITEM hParent, HTREEITEM hInsertAfter);
	BOOL DeleteItem(HTREEITEM hItem);
	BOOL DeleteAllItems();
	BOOL Expand(HTREEITEM hItem, UINT nCode = TVE_EXPAND);
	HTREEITEM GetNextItem(HTREEITEM hItem, UINT nCode) const;
	HTREEITEM GetChildItem(HTREEITEM hItem) const;
	HTREEITEM GetNextSiblingItem(HTREEITEM hItem) const;
	HTREEITEM GetPrevSiblingItem(HTREEITEM hItem) const;
	HTREEITEM GetParentItem(HTREEITEM hItem) const;
	HTREEITEM GetFirstVisibleItem() const;
	HTREEITEM GetNextVisibleItem(HTREEITEM hItem) const;
	HTREEITEM GetPrevVisibleItem(HTREEITEM hItem) const;

	HTREEITEM GetSelectedItem() const;

	HTREEITEM GetDropHilightItem() const;
	HTREEITEM GetRootItem() const;
#if !defined(_WIN32_WCE) && (_WIN32_IE >= 0x0400)
	HTREEITEM GetLastVisibleItem() const;
#endif // !defined(_WIN32_WCE) && (_WIN32_IE >= 0x0400)

#if (_WIN32_IE >= 0x0600)
	HTREEITEM GetNextSelectedItem() const;
#endif // (_WIN32_IE >= 0x0600)

	BOOL Select(HTREEITEM hItem, UINT nCode);
	BOOL SelectItem(HTREEITEM hItem);
	BOOL SelectDropTarget(HTREEITEM hItem);

	BOOL SelectSetFirstVisible(HTREEITEM hItem);
	HWND EditLabel(HTREEITEM hItem);
	BOOL EndEditLabelNow(BOOL bCancel);
	HTREEITEM HitTest(TVHITTESTINFO* pHitTestInfo) const;
	HTREEITEM HitTest(POINT pt, UINT* pFlags) const;
	BOOL SortChildren(HTREEITEM hItem, BOOL bRecurse = FALSE);
	BOOL EnsureVisible(HTREEITEM hItem);
	BOOL SortChildrenCB(LPTVSORTCB pSort, BOOL bRecurse = FALSE);
	LImageList RemoveImageList(int nImageList);
	LImageList CreateDragImage(HTREEITEM hItem);
#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	BOOL SetInsertMark(HTREEITEM hTreeItem, BOOL bAfter);

	BOOL RemoveInsertMark();
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0501)
	HTREEITEM MapAccIDToHTREEITEM(UINT uID) const;
	UINT MapHTREEITEMToAccID(HTREEITEM hTreeItem) const;
#endif // (_WIN32_WINNT >= 0x0501)

#if (_WIN32_WINNT >= 0x0600)
	void ShowInfoTip(HTREEITEM hItem);
#endif // (_WIN32_WINNT >= 0x0600)
};



///////////////////////////////////////////////////////////////////////////////
// LSysTabControl32


class _FK_OUT_CLASS LSysTabControl32 : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LSysTabControl32(HWND hWnd = NULL);
	LSysTabControl32(fk::LWindowMuch* pWindowMuch);
	LSysTabControl32& operator =(HWND hWnd)
	{
		_hWnd = hWnd;
		return *this;
	}

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateTabCtrl(DWORD dwExStyle, LPCTSTR lpWindowName, DWORD dwStyle,
		int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	LPCTSTR GetWndClassName();

	LImageList GetImageList() const;
	LImageList SetImageList(HIMAGELIST hImageList);
	int GetItemCount() const;
	BOOL GetItem(int nItem, LPTCITEM pTabCtrlItem) const;
	BOOL SetItem(int nItem, LPTCITEM pTabCtrlItem);
	int SetItem(int nItem, UINT mask, LPCTSTR lpszItem, DWORD dwState, DWORD dwStateMask, int iImage, LPARAM lParam);
	BOOL GetItemRect(int nItem, LPRECT lpRect) const;
	int GetCurSel() const;
	int SetCurSel(int nItem);
	SIZE SetItemSize(SIZE size);
	void SetItemSize(int cx, int cy);
	void SetPadding(SIZE size);
	int GetRowCount() const;
#ifndef _WIN32_WCE
	HWND GetTooltips() const;
	void SetTooltips(HWND hWndToolTip);
#endif // !_WIN32_WCE

	int GetCurFocus() const;
	void SetCurFocus(int nItem);
	BOOL SetItemExtra(int cbExtra);
	int SetMinTabWidth(int nWidth = -1);
#if (_WIN32_IE >= 0x0400)
	DWORD GetExtendedStyle() const;
	DWORD SetExtendedStyle(DWORD dwExMask, DWORD dwExStyle);
#ifndef _WIN32_WCE
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // !_WIN32_WCE
#endif // (_WIN32_IE >= 0x0400)

	// Operations
	int InsertItem(int nItem, LPTCITEM pTabCtrlItem);
	int InsertItem(int nItem, UINT mask, LPCTSTR lpszItem, int iImage, LPARAM lParam);
	int InsertItem(int nItem, LPCTSTR lpszItem);
	int AddItem(LPTCITEM pTabCtrlItem);
	int AddItem(UINT mask, LPCTSTR lpszItem, int iImage, LPARAM lParam);

	int AddItem(LPCTSTR lpszItem);
	BOOL DeleteItem(int nItem);
	BOOL DeleteAllItems();
	void AdjustRect(BOOL bLarger, LPRECT lpRect);
	void RemoveImage(int nImage);
	int HitTest(TC_HITTESTINFO* pHitTestInfo) const;
	void DeselectAll(BOOL bExcludeFocus = TRUE);
#if (_WIN32_IE >= 0x0400)
	BOOL HighlightItem(int nIndex, BOOL bHighlight = TRUE);
#endif // (_WIN32_IE >= 0x0400)
};





///////////////////////////////////////////////////////////////////////////////
// LTrackBar32


class _FK_OUT_CLASS LTrackBar32 : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LTrackBar32(HWND hWnd = NULL);
	LTrackBar32(fk::LWindowMuch* pWindowMuch);
	LTrackBar32& operator =(HWND hWnd)
	{
		_hWnd = hWnd;
		return *this;
	}

	bool CreateTrackBarCtrl(DWORD dwStyle,
		int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCTSTR GetWndClassName();
	int GetLineSize() const;
	int SetLineSize(int nSize);
	int GetPageSize() const;
	int SetPageSize(int nSize);
	int GetRangeMin() const;
	void SetRangeMin(int nMin, BOOL bRedraw = FALSE);
	int GetRangeMax() const;
	void SetRangeMax(int nMax, BOOL bRedraw = FALSE);
	void GetRange(int& nMin, int& nMax) const;
	void SetRange(int nMin, int nMax, BOOL bRedraw = TRUE);
	int GetSelStart() const;
	void SetSelStart(int nMin);
	int GetSelEnd() const;
	void SetSelEnd(int nMax);
	void GetSelection(int& nMin, int& nMax) const;
	void SetSelection(int nMin, int nMax);
	void GetChannelRect(LPRECT lprc) const;
	void GetThumbRect(LPRECT lprc) const;
	int GetPos() const;
	void SetPos(int nPos);
	UINT GetNumTics() const;
	DWORD* GetTicArray() const;

	int GetTic(int nTic) const;
	BOOL SetTic(int nTic);
	int GetTicPos(int nTic) const;
	void SetTicFreq(int nFreq);
	int GetThumbLength() const;
	void SetThumbLength(int nLength);
	void SetSel(int nStart, int nEnd, BOOL bRedraw = TRUE);
	//ATL::CWindow GetBuddy(BOOL bLeft = TRUE) const;
	//ATL::CWindow SetBuddy(HWND hWndBuddy, BOOL bLeft = TRUE);
#ifndef _WIN32_WCE
	HWND GetToolTips() const;

	void SetToolTips(HWND hWndTT);
	int SetTipSide(int nSide);
#endif // !_WIN32_WCE

#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

	// Operations
	void ClearSel(BOOL bRedraw = FALSE);
	void VerifyPos();
	void ClearTics(BOOL bRedraw = FALSE);
};

///////////////////////////////////////////////////////////////////////////////
// CUpDownCtrl
class _FK_OUT_CLASS LUpDownCtrl : public LWindow
{
	_FK_RTTI_;

public:
	// Constructors
	LUpDownCtrl(HWND hWnd = NULL);
	LUpDownCtrl(fk::LWindowMuch* pWindowMuch);
	LUpDownCtrl& operator =(HWND hWnd)
	{
		_hWnd = hWnd;
		return *this;
	}

	bool CreateUpDownCtrl(LPCTSTR lpWindowName, DWORD dwStyle,
		int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	LPCTSTR GetWndClassName();
	UINT GetAccel(int nAccel, UDACCEL* pAccel) const;
	BOOL SetAccel(int nAccel, UDACCEL* pAccel);
	UINT GetBase() const;
	int SetBase(int nBase);
	HWND GetBuddy() const;
	HWND SetBuddy(HWND hWndBuddy);
	int GetPos(LPBOOL lpbError = NULL) const;

	int SetPos(int nPos);

	DWORD GetRange() const;

	void GetRange(int& nLower, int& nUpper) const;
	void SetRange(int nLower, int nUpper);
#if (_WIN32_IE >= 0x0400)
	void SetRange32(int nLower, int nUpper);
	void GetRange32(int& nLower, int& nUpper) const;
#ifndef _WIN32_WCE
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // !_WIN32_WCE
#endif // (_WIN32_IE >= 0x0400)

#if (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
	int GetPos32(LPBOOL lpbError = NULL) const;
	int SetPos32(int nPos);
#endif // (_WIN32_IE >= 0x0500) && !defined(_WIN32_WCE)
};

///////////////////////////////////////////////////////////////////////////////
// LProgressBarCtrl

class _FK_OUT_CLASS LProgressBarCtrl : public LWindow
{
	_FK_RTTI_;

public:
	LProgressBarCtrl(void);
	LProgressBarCtrl(fk::LWindowMuch* pWindowMuch);

	HWND Create(HWND hWndParent, LPRECT pRect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL);

	bool CreateProgressBarCtrl(LPCTSTR lpWindowName, DWORD dwStyle,
								int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCTSTR GetWndClassName();

	DWORD SetRange(int nLower, int nUpper);
	int SetPos(int nPos);
	int OffsetPos(int nPos);
	int SetStep(int nStep);

	UINT GetPos() const;

	void GetRange(PPBRANGE pPBRange) const;
	void GetRange(int& nLower, int& nUpper) const;
	int GetRangeLimit(BOOL bLowLimit) const;
	DWORD SetRange32(int nMin, int nMax);
#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	COLORREF SetBarColor(COLORREF clr);
	COLORREF SetBkColor(COLORREF clr);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

#if (_WIN32_WINNT >= 0x0501) && defined(PBM_SETMARQUEE)
	BOOL SetMarquee(BOOL bMarquee, UINT uUpdateTime = 0U);
#endif // (_WIN32_WINNT >= 0x0501) && defined(PBM_SETMARQUEE)

#if (_WIN32_WINNT >= 0x0600)
	int GetStep() const;
	COLORREF GetBkColor() const;
	COLORREF GetBarColor() const;
	int GetState() const;
	int SetState(int nState);
#endif // (_WIN32_WINNT >= 0x0600)

	// Operations
	int StepIt();
};


///////////////////////////////////////////////////////////////////////////////
// CMonthCalendarCtrl
class _FK_OUT_CLASS LMonthCalendarCtrl : public LWindow
{
	_FK_RTTI_;

public:
	LMonthCalendarCtrl(void);
	LMonthCalendarCtrl(fk::LWindowMuch* pWindowMuch);

	bool CreateMonthCalendarCtrl(DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Attributes
	static LPCTSTR GetWndClassName();
	COLORREF GetColor(int nColorType) const;
	COLORREF SetColor(int nColorType, COLORREF clr);
	BOOL GetCurSel(LPSYSTEMTIME lpSysTime) const;
	BOOL SetCurSel(LPSYSTEMTIME lpSysTime);
	int GetFirstDayOfWeek(BOOL* pbLocaleVal = NULL) const;
	int SetFirstDayOfWeek(int nDay, BOOL* pbLocaleVal = NULL);
	int GetMaxSelCount() const;
	BOOL SetMaxSelCount(int nMax);
	int GetMonthDelta() const;
	int SetMonthDelta(int nDelta);
	DWORD GetRange(LPSYSTEMTIME lprgSysTimeArray) const;
	BOOL SetRange(DWORD dwFlags, LPSYSTEMTIME lprgSysTimeArray);
	BOOL GetSelRange(LPSYSTEMTIME lprgSysTimeArray) const;
	BOOL SetSelRange(LPSYSTEMTIME lprgSysTimeArray);
	BOOL GetToday(LPSYSTEMTIME lpSysTime) const;
	void SetToday(LPSYSTEMTIME lpSysTime);
	BOOL GetMinReqRect(LPRECT lpRectInfo) const;
	int GetMaxTodayWidth() const;
#if (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)
	BOOL GetUnicodeFormat() const;
	BOOL SetUnicodeFormat(BOOL bUnicode = TRUE);
#endif // (_WIN32_IE >= 0x0400) && !defined(_WIN32_WCE)

#if defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)
	DWORD GetCurrentView() const;
	BOOL SetCurrentView(DWORD dwView);
	DWORD GetCalendarCount() const;
	//BOOL GetCalendarGridInfo(PMCGRIDINFO pGridInfo) const;
	CALID GetCALID() const;
	void SetCALID(CALID calid);
	int GetCalendarBorder() const;
	void SetCalendarBorder(int cxyBorder, BOOL bSet = TRUE);
#endif // defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)

	// Operations
	int GetMonthRange(DWORD dwFlags, LPSYSTEMTIME lprgSysTimeArray) const;
	BOOL SetDayState(int nMonths, LPMONTHDAYSTATE lpDayStateArray);
	DWORD HitTest(PMCHITTESTINFO pMCHitTest) const;
#if defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)
	void SizeRectToMin(LPRECT lpRect);
#endif // defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)
};


///////////////////////////////////////////////////////////////////////////////
// LSysDateTimePick32
class _FK_OUT_CLASS LSysDateTimePick32 : public LWindow
{
	_FK_RTTI_;

public:
	LSysDateTimePick32(void);
	LSysDateTimePick32(fk::LWindowMuch* pWindowMuch);

	bool CreateDateTimePickerCtrl(DWORD dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, int id, PVOID lpParam);

	// Operations
	static LPCTSTR GetWndClassName();
	BOOL SetFormat(LPCTSTR lpszFormat);
	COLORREF GetMonthCalColor(int nColorType) const;
	COLORREF SetMonthCalColor(int nColorType, COLORREF clr);
	DWORD GetRange(LPSYSTEMTIME lpSysTimeArray) const;
	BOOL SetRange(DWORD dwFlags, LPSYSTEMTIME lpSysTimeArray);
	DWORD GetSystemTime(LPSYSTEMTIME lpSysTime) const;
	BOOL SetSystemTime(DWORD dwFlags, LPSYSTEMTIME lpSysTime);
	HWND GetMonthCal() const;
#if (_WIN32_IE >= 0x0400)
	//CFontHandle GetMonthCalFont() const;
	void SetMonthCalFont(HFONT hFont, BOOL bRedraw = TRUE);
#endif // (_WIN32_IE >= 0x0400)

#if defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)
	DWORD GetMonthCalStyle();
	DWORD SetMonthCalStyle(DWORD dwStyle);
	void GetDateTimePickerInfo(LPDATETIMEPICKERINFO lpPickerInfo);
	BOOL GetIdealSize(LPSIZE lpSize);
	void CloseMonthCal();
#endif // defined(NTDDI_VERSION) && (NTDDI_VERSION >= NTDDI_LONGHORN)
};




}; // namespace fk



#endif // __FKCRTLS_H__
