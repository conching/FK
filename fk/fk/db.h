#ifndef __db_h__
#define __db_h__

#include "fk\DataLink.hxx"

_FK_BEGIN

/* 提前声明的类。 */
class LTable;
class LField;
class LDataConnection;

/*
数据库控件类关联顺序。
DBControl DataCource  Table Connection 

数据库控件基类服务类，这些类写入到 TraceLog 项目中。
TDataLink 
TDataLink = class(TPersistent)
private
[Weak]FDataSource: TDataSource;
FNext: TDataLink;
FBufferCount: Integer;
FFirstRecord: Integer;
FReadOnly: Boolean;
FActive: Boolean;
FVisualControl: Boolean;
FEditing: Boolean;
FUpdating: Boolean;
FDataSourceFixed: Boolean;

*/

/* 管理数据表和 DB 控件之间的联系。 */
enum umLinkState
{
	lsDisconnect=0x1,		/* 断开连接。 */
	lsActive	=0x2,		/* 打开连接。 */
	lsDisable	=0x3,		/* 在连接状态，但是不接收信息。*/
};

class _FK_OUT_CLASS LResValue : public fk::LStringw
{
public:
	LResValue();
	virtual ~LResValue();

	virtual b_call SetResValue(UINT uiResID);

	UINT _uiResID;
};

// {5560D491-F8C2-4500-BE2D-74C6B7D8C3EA}
extern IID  IID_IFields;

//
//// 这三个函数是属性调用的
//vb_call LFields::SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue)
//{
//	switch (uiPropID)
//	{
//	case fk::LFields::PROPID_SET_MODULE:
//		{
//
//		}return true;
//
//	case fk::LFields::PROPID_SET_DATA_CONNECTION_XML:
//		{
//		}return true;
//
//	case fk::LFields::PROPID_SET_XML_PATH:
//		{
//		}return true;
//	}
//
//	return fk::LObject::SetPropValue(uiPropID, uiPropIDChild, lParamValue);
//}
//
//vb_call LFields::GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValueIn,
//								   fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut)
//{
//	return fk::LObject::GetPropDisplayValue(uiPropID, uiPropIDChild, pBaseDataValueIn, umDisplayValue, spDisplayValueOut);
//}
//
//
//vb_call LFields::GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData)
//{
//	return fk::LObject::GetPropValue(uiPropID, uiPropIDChild, pBaseData);
//}
//


enum enumTableRowState
{
	umTableRowStateError	=0x00000001,		// 
	umTableRowStateNew		=0x00000002,			// 数据行是新的。
	umTableRowStateSave		=0x00000004,		// 数据已经保存过
	umTableRowStateEdit		=0x00000008,		// 数据行在被修改了状态
	umTableRowStateDiscon	=0x00000010,		// 数据行是从数据表中断开状态
	umTableRowStateNotGui	=0x00000020,		// 
};

class _FK_OUT_CLASS LEnumTableRow : public IUnknown
{
public:
	LEnumTableRow();
	virtual ~LEnumTableRow();

	_FK_DBGCLASSINFO_

	virtual HRESULT STDMETHODCALLTYPE Next(ULONG celt, LTableRow** ppTableRow, ULONG *pcFetched) = 0;
	virtual HRESULT STDMETHODCALLTYPE Skip(ULONG celt) = 0;
	virtual HRESULT STDMETHODCALLTYPE Reset( void) = 0;
	virtual HRESULT STDMETHODCALLTYPE Clone(LEnumTableRow **ppEnum) = 0;
};
extern IID IID_IEnumTableRow;

//-----------------------------------------

enum enumTableOpen
{
	umTableOpenOpen				= 0x1,
	umTableOpenNotExistCreate	= 0x2,
};

enum enumTableMask
{
	umTableOpenConnectionOpenTable = 0x00000001,
};

// public IUnknown,
class _FK_OUT_CLASS LTable : public fk::LDataSet
{
	_FK_RTTI_;

private:

protected:
	DWORD					_dwMakeTable;
	CRITICAL_SECTION		_sec;
	fk::LDataConnection*	_pDataConnection;

	b_call SetDataConnectionOpen(fk::LDataConnection* pDataConnection);

	v_call DoTableEventAfterOpen(void);
	v_call DoTableEventAfterClose(void);
	v_call DoTableEventClearData(void);
	v_call DoTableEventUpdateData(void);
public:
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

public:
	fk::LTableRow*		_pCurTableRow;
	fk::LModule*		_pmodule;
	fk::LFields*		_pFields;
	ULONG				_uCurrendRec;

	LTable(fk::LObject* pParentObject);
	virtual ~LTable();

	virtual b_call SwapTableRow(fk::LTableRow* pFromTableRow, fk::LTableRow* pToTableRow) = 0;
	virtual b_call SwapTableRow(UINT uiFromTableRow, UINT uiToTableRow) = 0;

	virtual b_call SetTableName(const wchar_t* pwstr, enumTableOpen umTableOpen) = 0;
	virtual b_call GetTableName(LStringw* ptext) = 0;

	virtual b_call InsertTableRow(int iIndex, fk::LTableRow* pTableRow, int iArray) = 0;
	virtual b_call InsertRow(UINT uiIndex, fk::LTableRow* pTableRow) = 0;
	virtual b_call AppendRow(fk::LTableRow* pTableRow) = 0;
	virtual v_call clear(void) = 0;
	virtual b_call SetFilter(data_t* pDataFilter, bool bFilter) = 0;
	virtual b_call SetFiltered(bool bFilter) = 0;
	virtual b_call GetFiltered() = 0;

	virtual enumReturn _call DeleteTableRow(HWND hWndHint, fk::LTableRow** pTableRow, int iArray) = 0;
	virtual b_call DeleteTableRowIndex(HWND hWndHint, __int64 piIndex) = 0;

	virtual b_call GetEnumTableRow(LEnumTableRow** pEnumTableRow) = 0;
	virtual b_call CreateTableRowDiscon(fk::LTableRow** ppTableRow) = 0;
	virtual b_call DeleteTableRowDiscon(fk::LTableRow** ppTableRow) = 0;
	virtual b_call TableRowExist(fk::LTableRow* pTableRow) = 0;

	virtual b_call SetEditState(bool bState);
	virtual b_call GetEditState();
	virtual b_call SetReadOnly(bool bRead);
	virtual b_call GetReadOnly();
	virtual b_call ShowRowBox(fk::LTableRow* pTableRow);
			v_call ModifyTableMask(DWORD dwRemove, DWORD dwAdd);

	virtual fk::LTableRow* _call Find(LPTABLEFINDVALUE lpTableFindValue, int iCount) = 0;
	virtual __int64 _call FindTableRow(LPTABLEFINDVALUE lpTableFindValue, int iCount) = 0;

	virtual fk::LTableRow* _call GetCurrentTableRow();

	LFields* _call GetFields();

	v_call EnableUpdateUI(bool bEnable);
	b_call IsEnableUpdateUI();


	static fk::LTable* _call NewXmlTable(fk::LDataConnection* pDataConnection, fk::LModule* pModule, LPCWSTR lpszTableName);
	static fk::LTable* _call NewNodeXmlTable(fk::LDataConnection* pDataConnection, fk::LModule* pModule, fk::HXMLNODE hNodeTable);
};
extern IID IID_ITable;					// {6DD38890-542E-4601-B607-04B99F390FF5}

/*
** obj, msg, event
*/
enum tagTABLEEVENTTYPE{
	ON_TB_AFTER_OPEN		=0x00000001,		//	TableEventAfterOpen
	ON_TB_AFTER_CLOSE		=0x00000002,		//	TableEventAfterClose

	ON_TB_ERROR				=0x00000004,		//	TableEventError
	ON_TB_AFTER_READ		=0x00000008,
	ON_TB_DELETEFIELD		=0x00000010,
	ON_TB_CLEARALLFIELD		=0x00000020,
	ON_TB_TABLEROWCLEAR		=0x00000040,
	ON_TB_UPDATE_DATA		=0x00000080,		/* 更新控件中的所有数据。*/
	ON_TB_CHANGE_ROW		=0x00000100,		/* 改变行，定位新的行。*/
	ON_TB_INSER_ROW			=0x00000200,		/* 新插入一行数据。*/
	ON_TB_DELETE_ROW		=0x00000400,		/* 删除某一行数据。*/
	ON_TB_EDIT_ROW			=0x00000800,		/* 某个行的新数据。*/
	ON_TB_EDIT_STATE		=0x00001000,		/* 数据是否在编辑，还是只读状态。*/
	ON_TB_CLEAR_DATA		=0x00002000,		/* 清空表的消息。*/
	ON_TB_TABLE_SAVE		=0x00004000,		/* 保存表格 */
	ON_TB_TABLEROW_STATE	=0x00008000,		/* 改变了行的状态。 */
	ON_TB_TABLE_READONLY	=0x00010000,		/* 表是读或者写状态。 */
	ON_TB_SWAPTABLEROW		=0x00020000,		/* 交换表格的行。*/
};

struct TableEvent
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
};
typedef TableEvent TableEventAfterOpen;
typedef TableEvent TableEventAfterClose;
typedef TableEvent	TableEventClearData;
typedef TableEvent	TableEventTableSave;

struct TableEventEditState
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	bool bEdit;
};

struct TableEventReadOnly
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	bool bReadOnly;
};

struct TableEventError
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	fk::LStringw text;
};

struct TableEventDeleteField
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	fk::LField* pField;
};

struct TableEventInsertRow
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	fk::LTableRow* pTableRow;
	int iArray;
};

struct TableEventDeleteRow
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	fk::LTableRow** pTableRowArray;
	int iArray;
	//bool bDelete;
};

struct TableEventChangeRow
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
	fk::LTableRow* pTableRow;
};

//ON_TB_UPDATE_DATA
struct TableEventUpdateData
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable* pTable;
};

struct TableEventTableRowState
{
	fk::NOTIFYEVENT nEvent;
	bool bChange;
	fk::LTable* pTable;
	fk::LTableRow* pTableRow;
	DWORD dwTableRowStateOld;
	DWORD dwTableRowStateNew;
};

typedef struct tagTableEventSwapTableRow
{
	fk::NOTIFYEVENT		nEvent;
	fk::LTable*			pTable;
	UINT				uiFromTableRow;
	UINT				uiToTableRow;
	fk::LTableRow*		pTableRow1;
	fk::LTableRow*		pTableRow2;
}TableEventSwapTableRow;


enum umWindowType
{
	wtError					=0x0,
	wtDBEdit				=0x1,
	wtDBComboBox			=0x2,
	wtDBComboBoxSelect		=0x2,
	wtDBListBox				=0x3,
	wtDBButton				=0x4,
	wtDBListView			=0x5,
	wtDBSysDateTimePick		=0x6,

	//wtEdit				=0x1,
	//wtComboBox			=0x2,
	//wtComboBoxSelect		=0x2,
	//wtListBox				=0x3,
	//wtCheckButton			=0x4,
	//wtListView			=0x5,
	//SysDateTimePick32		=0x6,
};

fk::LWindow* _call CreateLinkDBWindow(HWND hWndParent,
									  int iCtrlID,
									  fk::umWindowType* pWindowType
									  );

class _FK_OUT_CLASS LDataConnection : public fk::LCustomConnection
{
	_FK_RTTI_;

private:
	DWORD _dwdcMask;

protected:
	fk::LStringw _sFileExt;
	fk::LStringw _sFileFull;

	v_call DoEventOpen(void);
	v_call DoEventClose(void);
	v_call DoEventLoadData(fk::LTable* pTable);
	v_call DoEventAddTable(fk::LTable* pTable);
	v_call DoEventRemoveTable(fk::LTable* pTable);
	v_call DoEventUpdateUI(fk::LTable* pTable);
	v_call DoEventUpdateUIObject(fk::LTable* pTargetTable);
	v_call DoEventEdit(void);

public:
	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild,
					const fk::LVar* pBaseDataValueIn, fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

public:
	LDataConnection(fk::LObject* pParentObject);
	virtual ~LDataConnection();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv) = 0;

	virtual b_call AddTable(LPCWSTR lpszKey, LTable* pTable) = 0;
	virtual b_call RemoveTable(LPCWSTR lpszKey, LTable* pTable) = 0;
	virtual LTable* _call NewTable(fk::LModule* pModule, LPCWSTR lpszTableName) = 0;
	virtual LTable* Find(LPCWSTR lpszKey) = 0;
	virtual i_call GetCount(void) = 0;
	virtual vp_call GetFileHandle(void) = 0;
	virtual v_call clear(void) = 0;
	virtual b_call OpenConnection(void) = 0;
	virtual b_call CloseConnection(void) = 0;
	//virtual b_call SetConnectionString(LPCWSTR psFileName) = 0;
	//virtual b_call GetConnectionString(LPWSTR lpszFileName) = 0;
	virtual b_call IsOpen(void) = 0;
	virtual b_call GetTableEnumPlus(fk::LEnumPlus* pEnumPlus) = 0;
	virtual b_call GetFileName(fk::LStringw* psFileName)  = 0;
	//virtual	v_call ModifyDataConnectionMask(DWORD dwRemove, DWORD dwAdd)  = 0;
	virtual b_call Save(void) = 0;

	virtual b_call SetEdit(bool bEdit);
	virtual b_call IsEdit(void);

	virtual b_call SetFileFull(LPCWSTR lpszFileFull);
	virtual v_call GetFileFull(fk::LStringw* psFileFull);

	virtual b_call SetFileExt(LPCWSTR lpszFileExt);
	virtual v_call GetFileExt(fk::LStringw* psFileExt);

	//
	// 跟据 xml 文件中的设置，把数据表和控件连接起来，并 new 出控件类，添加到 pWindows 类中。
	// 在表对象已经存在之后调用。
	// 这个函数已经消除。
	//
	virtual b_call LinkFieldNewControl(HWND hWndParent, fk::LWindows* pWindows,
						fk::LModule* pModule, fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath);

	void operator delete(void *pObject)
	{
		((LDataConnection*)pObject)->Release();
	}

public:

    enum enumPROPID
    {
	// 3200 - 3800		DataConnection2      2
	PROPID_FILE			=	3200,			// 设置文件名称.	fk::LDataConnection
	PROPID_ACTIVE		=	3201,			// 打开或者关闭文件.
	PROPID_EDIT			=	3202,			// 文件在编辑状态,还是非编辑文件.
	PROPID_FILE_EXT		=	3203,			// 文件扩展名称.	fk::LDataConnection

	PROPID_XML_TYPE		=	3400,			// xml文件连接类型。
    };
};

enum tagDATACONNECTIONEVENT
{
	ON_DC_OPENCONNECTION	= 0x0000000000000001,			//fk::NOTIFYEVENT
	ON_DC_CLOSECONNECTION	= 0x0000000000000002,			//fk::NOTIFYEVENT
	ON_DC_UPDATEDATA		= 0x0000000000000004,
	ON_DC_ADDTABLE			= 0x0000000000000008,
	ON_DC_REMOVE_TABLE		= 0x0000000000000020,
	ON_DC_EDIT				= 0x0000000000000040,			//fk::NOTIFYEVENT
};

typedef struct tagDCO_OPEN
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
}DCO_OPEN,*LPDCO_OPEN;

typedef struct tagDCO_CLOSE
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
}DCO_CLOSE,*LPDCO_CLOSE;

typedef struct tagDCO_LOADDATA
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
	fk::LTable*				pTable;
}DCO_LOADDATA,*LPDCO_LOADDATA;

typedef struct tagDCO_ADDTABLE
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
	fk::LTable*				pTable;
}DCO_ADDTABLE,*LPDCO_ADDTABLE;

typedef struct tagDCO_UPDATEUI
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
	fk::LTable*				pTable;
}DCO_UPDATEUI,*LPDCO_UPDATEUI;

typedef struct tagDCO_REMOVETABLE
{
	fk::NOTIFYEVENT			nEvent;
	fk::LDataConnection*	pDataConnection;
	fk::LTable*				pTable;
}DCO_REMOVETABLE,*LPDCO_REMOVETABLE;

///////////////////////////////////////////////////////////////////////////////////////////////////

enum enumDataConectionType
{
	dctNull			=0x0,			//  什么也没有连接。
	dctApplication	=0x1,			//	连接 .pxml 文件。不需要调用 LDataConnection::OpenConnection方法。
	dctUser			=0x2,			//	连接 .uxml 文件。不需要调用 LDataConnection::OpenConnection方法。
	dctXml			=0x3,			//	链接 xml 文件。
	//dctData			=0x3,			//	连接 二进制 文件。
	dctResXml		=0x5,			//	链接 res xml 文件。
	//dctUserType		=0x100,			//  用户自定义类型。
};

class _FK_OUT_CLASS LDataConnectionXml : public fk::LDataConnection
{
	_FK_RTTI_;

protected:
	using fk::LDataConnection::_sFileFull;
	using fk::LDataConnection::_sFileExt;

public:
	LDataConnectionXml(fk::LObject* pParentObject);
	virtual ~LDataConnectionXml();

	virtual b_call SetDataConnectionType(fk::enumDataConectionType umDataConectionType) = 0;
	virtual fk::enumDataConectionType _call GetDataConnectionType(void) = 0;

	static fk::LDataConnectionXml* _call NewDataConnectionXml(fk::LObject* pParentObject);

public:
    enum enumPROPID
    {
        PROPID_SET_XMLFILE = 2001,           // 设置 xmlFile.
    };
};

/*
** 	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent)=0;
*/
class _FK_OUT_CLASS LBaseDataCtrl
{
protected:
	fk::LTable*			_pTable;
	fk::LField*			_pField;
	fk::LVar*		_pBaseData;
	fk::umLinkState		_umLinkState;
	fk::LStringw		_sFieldName;

	virtual b_call LinkEvent() = 0;				// 子类重写函数。

public:
	LBaseDataCtrl();
	virtual ~LBaseDataCtrl();

	// 这几个方法以后逐渐不在使用。
	virtual b_call SetTable(fk::LTable* pTable);
	virtual b_call GetTable(fk::LTable** pTable);
	virtual b_call SetField(fk::LField* pField);
	virtual b_call GetField(fk::LField** pField);
	virtual b_call SetFieldName(LPCWSTR szFieldName);
	virtual b_call GetFieldName(LPWSTR szFieldName);

	virtual b_call SetLinkTableField(fk::LTable* pTable, fk::LField* pField);
	virtual b_call SetLinkTableFieldName(fk::LTable* pTable, LPCWSTR szFieldName);

	virtual b_call SetLinkState(fk::umLinkState linkState);
	virtual fk::umLinkState GetLinkState();
};

#ifndef _FM_TYPE_LIB_
#pragma comment(lib, "typelib.lib")
#endif

_FK_END

#endif	/* __db_h__ */
