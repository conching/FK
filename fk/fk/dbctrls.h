#ifndef __DbListView_h__
#define __DbListView_h__



_FK_BEGIN

class LDBListView;

//
//enum umDBMESSAGE
//{
//	dbmUpdateData	=0x00000001,
//	dbmSaveData		=0x00000002,
//	dbmDataReadState=0x00000004,
//};


typedef struct _tagDBDATAREADSTATE
{
	fk::NOTIFYEVENT nEvent;
	fk::LTable*		pTable;
	bool			bRead;
}DBDATAREADSTATE, *LPDBDATAREADSTATE;


class _FK_OUT_CLASS LFieldHeaderItem : public fk::IHeaderItem
{
public:
	_FK_DBGCLASSINFO_

		LFieldHeaderItem();
	virtual ~LFieldHeaderItem();

	virtual fk::LField* GetField() = 0;

	virtual STDMETHODIMP HiGetData(LONG lpLVITEM, LONG pDataObject, LONG pUserData) = 0;
	virtual STDMETHODIMP HiSort(LONG pDataObject, LONG pUserData) = 0;
};

extern IID IID_IFieldHeaderItem;





class _FK_OUT_CLASS LFieldHeaderItems
{
private:
protected:

public:
	_FK_DBGCLASSINFO_

	LFieldHeaderItems(fk::LDBListView* pdbListView);
	virtual ~LFieldHeaderItems();

	virtual b_call InsertFieldHeaderItem(int iIndex, fk::LFieldHeaderItem* pfhi) = 0;
	virtual b_call RemoveIndex(int iIndex) = 0;
	virtual v_call clear(void) = 0;
	virtual b_call RemoveFieldHeaderitem(fk::LFieldHeaderItem* pfhi) = 0;
	virtual b_call HideIndex(int iIndex) = 0;
	virtual b_call HideFieldHeaderitem(fk::LFieldHeaderItem* pfhi) = 0;
	virtual b_call GetIndex(fk::LFieldHeaderItem** pfhi) = 0;
	virtual b_call GetFieldHeaderitem(fk::LFieldHeaderItem** pfhi) = 0;
	virtual b_call ShowFullField(void) = 0;

	static b_fncall NewFieldHeaderItemMgr(fk::enumClassValue cvClassValue,
		fk::LFieldHeaderItems** pfhiMgr,
		fk::LDBListView* pdbListView);
};




enum umDBListView
{
	dblvEdit		= 0x00000001,
	dblvDelete		= 0x00000002,
	dblvContextMenu	= 0x00000004,
};

class _FK_OUT_CLASS LDBListView : public fk::LBaseDataCtrl,
									public fk::LSysListView32
{
private:
protected:

public:
	LDBListView(fk::LWindow* pWindowParent);
	virtual ~LDBListView();

	virtual void Attach(HWND hWndNew) = 0;
	virtual fk::LFieldHeaderItems* GetFieldHeaderItemMgr() = 0;
	virtual enumReturn _call DeleteSelectItem(HWND hWndHint) = 0;
	virtual fk::LTableRow* GetSelectTableRow() = 0;
	virtual fk::LTableRow* GetItemTableRow(int iIndex) = 0;
	virtual LRESULT wmNotify(LPNMHDR lpnmhdr) = 0;
	virtual b_call ModifyDBListViewStyle(DWORD dwRemove, DWORD dwAdd) = 0;

	static fk::LDBListView* _fncall NewDBListView(fk::LWindow* pWindowParent);
};
extern IID IID_DbListView;


struct DbListViewEventDBlick
{
	fk::NOTIFYEVENT		nEvent;
	fk::LTable*			pTable;
	fk::LDBListView*	pDbListView;
	fk::LTableRow*		pTableRow;
};



class _FK_OUT_CLASS LDBButton : public fk::LBaseDataCtrl,
								public fk::LButton
{
private:
protected:
	virtual b_call LinkEvent();

public:
	LDBButton();
	virtual ~LDBButton();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual LRESULT _call OnCommand(WPARAM wParam, LPARAM lParam);

	static LDBButton* _fncall NewDBCheckButton();
};





typedef struct _tagDBDATACOMBOBOX
{
	fk::NOTIFYEVENT		nEvent;
	fk::LTable*			pTable;
	fk::LField*			pField;
	fk::LVar*		pBaseData;
}DBDATACOMBOBOX, *LPDBDATACOMBOBOX;


class _FK_OUT_CLASS LDBComboBox : 	public fk::LComboBox,
									public fk::LBaseDataCtrl
{
private:
protected:
	virtual b_call LinkEvent();

public:
	LDBComboBox();
	virtual ~LDBComboBox();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual LRESULT _call OnCommand(WPARAM wParam, LPARAM lParam);

	static LDBComboBox* _fncall NewDBComboBox();
};




class _FK_OUT_CLASS LDBEdit :   public fk::LEdit,
								public fk::LBaseDataCtrl
{
private:
protected:
	virtual b_call LinkEvent();

public:
	LDBEdit(void);
	virtual ~LDBEdit(void);

	virtual STDMETHODIMP QueryInterface(REFIID riid, void** ppv);
	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual LRESULT _call OnCommand(WPARAM wParam, LPARAM lParam);
	static LDBEdit* _fncall NewDBEdit();
};





class _FK_OUT_CLASS LDBDateTimePickerCtrl :	public fk::LSysDateTimePick32,
											public fk::LBaseDataCtrl
{
private:
protected:

	virtual b_call LinkEvent();

public:
	LDBDateTimePickerCtrl();
	virtual ~LDBDateTimePickerCtrl();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual LRESULT _call OnCommand(WPARAM wParam, LPARAM lParam);
	static LDBDateTimePickerCtrl* _fncall NewDBDateTimePickerCtrl();
};





class _FK_OUT_CLASS LDBListBox :	public fk::LListBox,
									public fk::LBaseDataCtrl
{
private:
protected:

	virtual b_call LinkEvent();

public:
	LDBListBox();
	virtual ~LDBListBox();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);
	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual LRESULT _call OnCommand(WPARAM wParam, LPARAM lParam);

	static LDBListBox* _fncall NewDBListBox();
};



_FK_END



#endif /* __DbListView_h__ */
