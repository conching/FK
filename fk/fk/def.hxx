#ifndef __FK_DEF_H__
#define __FK_DEF_H__

/*
LVar
LAutoPtr
LAutoPtrBase
*/

#ifdef __GNUC__
#include <assert.h>
#else
// 使用CRT调试API
#include <crtdbg.h>
#include <assert.h>
#endif // __FK_VC__


#define _FK_BEGIN			namespace fk{
#define _FK_END				}

#ifdef __GNUC__
typedef unsigned long   DWORD;
typedef unsigned char   BYTE;
typedef short               VARIANT_BOOL;
typedef int                 BOOL;
typedef unsigned int        UINT;
//typedef unsigned long	    LPARAM;
typedef const char*		LPCSTR;
typedef const wchar_t*	LPCWSTR;
//typedef short    _VARIANT_BOOL;
#define VARIANT_TRUE            ((VARIANT_BOOL)0xffff)
#define VARIANT_FALSE           ((VARIANT_BOOL)0)
#define FALSE 0
#define TRUE 1

#endif // __GNUC__


typedef unsigned int	uint;
typedef unsigned long   ulong;


_FK_BEGIN

#ifdef __FK_VC__
#pragma warning(disable:4100)       // format parameter not USED
#pragma warning(disable:4267)       // conversion from 'x' to 'y', possible loss of data
#pragma warning(disable:4127)       // conditional expression is constant
#pragma warning(disable:4706)       // assignment within conditional expression
#pragma warning(disable:4244)       // conversion from 'X' to 'Y', possible loss of data
#pragma warning(disable:4312)       // conversion from 'X' to 'Y' of greater size
#pragma warning(disable:4311)       // pointer truncation from 'X' to 'Y'
#pragma warning(disable:4018)       // '<' : signed/unsigned mismatch
#pragma warning(disable:249)        // '==' : signed/unsigned mismatch
#pragma warning(disable:4389)       // '!=' : signed/unsigned mismatch
#pragma warning(disable:4800)       // forcing value to bool 'true' or 'false'
#pragma warning(disable:4996)
#endif // __FK_VC__


#ifndef __in
#define __in
#endif

#ifndef __out
#define __out
#endif


//
// 定义这几个类是来自其它基于 fkdef.h 文件。
//
class LStringw;			// LStringw.h
class LField;


/*
** class call
*/
#define  wp_call	wchar_t*
#define  cp_call	char*
#define  b_call		bool
#define b2_call		BOOL
#define b3_call		VARIANT_BOOL
#define  v_call		void
#define vp_call		void*
#define  i_call		int
#define ui_call		unsigned int
#define be_call		BYTE
#define  l_call		long
#define ul_call		unsigned long
#define  f_call		float
#define  d_call		double
#define us_call		unsigned short
#define hr_call		HRESULT
#define dw_call		DWORD
#define dwp_call	DWORD_PTR
#define   _call

//
// class virutal
//
#define vwp_call	wchar_t*
#define vcp_call	char*
#define  vb_call	bool
#define vb2_call	BOOL
#define vb3_call	VARIANT_BOOL
#define  vv_call	void
#define vvp_call	void*
#define  vi_call	int
#define vui_call	unsigned int
#define vbe_call	BYTE
#define  vl_call	long
#define vul_call	unsigned long
#define  vf_call	float
#define  vd_call	double
#define vus_call	unsigned short
#define vhr_call	HRESULT
#define vdw_call	DWORD
#define vdwp_call   DWORD_PTR
#define vir_call							// virtual LObject*  LXXXXX::AAA();	 LObject* vir_call LXXXXX::AAA();

/*
** fn call
*/
#define  wp_fncall		wchar_t* __stdcall
#define  cp_fncall		char* __stdcall
#define b2_fncall		BOOL __stdcall
#define  b_fncall		bool __stdcall
#define b3_fncall		VARIANT_BOOL __stdcall
#define  v_fncall		void __stdcall
#define vp_fncall		void* __stdcall
#define  i_fncall		int __stdcall
#define ui_fncall		unsigned int __stdcall
#define be_fncall		BYTE __stdcall
#define  l_fncall		long __stdcall
#define ul_fncall		unsigned long __stdcall
#define  f_fncall		float __stdcall
#define  d_fncall		double __stdcall
#define us_fncall		unsigned short __stdcall
#define hr_fncall		HRESULT __stdcall
#define dw_fncall		DWORD __stdcall
#define dwp_fncall		DWORD_PTR __stdcall
#define  _fncall		__stdcall

//
//#ifndef __FK_VC__
//#define WINAPI   __stdcall
//#endif // __FK_VC__
//

#define FK_0x2C				0xCC
#define FK_0x4C				0xCCCC
#define FK_0x8C				0xCCCCCCCC
#define FK_0x8c				0xcccccccc
#define FK_0x8cd			0xcdcdcdcd
#define FK_0xba				0xbaadf00d
#define FK_0xBA				0xBAADF00D
#define FK_0x16C			0xCCCCCCCCCCCCCCCC
#define FK_0x16c			0xcccccccccccccccc
#define FK_0x16cd			0xcdcdcdcdcdcdcdcd

#define WIDEN2(x)			L ## x
#define WIDEN(x)			WIDEN2(x)

/*
** export class call
*/
/*
** 数据类型
*/
#define data_t  unsigned long
typedef void*			FTEXT;

#define FKASSERT(expr)		assert(expr);

#ifdef __GNUC__
#define _CRT_WIDE2(_String)	    L ## _String
#define _CRT_WIDE3(X)           _CRT_WIDE2(X)
#define _CRT_WIDE(_String)      _CRT_WIDE2(_String)

#ifdef UNICODE
#define __TFILE__			_CRT_WIDE3(__FILE__)
#else
#define __TFILE__			__FILE__
#endif

#else   // __GNUC__

//#define __STR2WSTR(str)    L'"' ## str
//#define _STR2WSTR(str)     __STR2WSTR(str)
//
//#define __FILEW__          _STR2WSTR(__FILE__)
//#define __FUNCTIONW__      _STR2WSTR(__FUNCTION__)
//
//#ifndef _CRT_WIDE
//#define __CRT_WIDE(_String) L ## _String
//#define _CRT_WIDE(_String) __CRT_WIDE(_String)
//#endif
#ifdef UNICODE
#define __TFILE__			_CRT_WIDE(__FILE__)
#else
#define __TFILE__			__FILE__
#endif
#endif // __GNUC__



//#define  BTNS_COMBOBOX	0x00010000
//#define  BTNS_EDIT		0x00020000
//#define  BTNS_MENU		0x00030000


#ifdef _FK_DEF_OUT_CLASS
#define _FK_OUT_CLASS __declspec(dllexport)
#else
#define _FK_OUT_CLASS __declspec(dllimport)
#endif

//
// 取出某个位,位置不变。
//
#ifdef _WIN32
#define FK_DWORD_1(dwValue) (dwValue & 0x0000000F)
#define FK_DWORD_2(dwValue) (dwValue & 0x000000F0)
#define FK_DWORD_3(dwValue) (dwValue & 0x00000F00)
#define FK_DWORD_4(dwValue) (dwValue & 0x0000F000)
#define FK_DWORD_5(dwValue) (dwValue & 0x000F0000)
#define FK_DWORD_6(dwValue) (dwValue & 0x00F00000)
#define FK_DWORD_7(dwValue) (dwValue & 0x0F000000)
#define FK_DWORD_8(dwValue) (dwValue & 0xF0000000)
#endif // _WIN32
#ifdef _WIN64
#define FK_DWORD_1(dwValue)  (dwValue & 0x000000000000000F)
#define FK_DWORD_2(dwValue)  (dwValue & 0x00000000000000F0)
#define FK_DWORD_3(dwValue)  (dwValue & 0x0000000000000F00)
#define FK_DWORD_4(dwValue)  (dwValue & 0x000000000000F000)
#define FK_DWORD_5(dwValue)  (dwValue & 0x00000000000F0000)
#define FK_DWORD_6(dwValue)  (dwValue & 0x0000000000F00000)
#define FK_DWORD_7(dwValue)  (dwValue & 0x000000000F000000)
#define FK_DWORD_8(dwValue)  (dwValue & 0x00000000F0000000)
#define FK_DWORD_9(dwValue)  (dwValue & 0x0000000F00000000)
#define FK_DWORD_10(dwValue) (dwValue & 0x000000F000000000)
#define FK_DWORD_11(dwValue) (dwValue & 0x00000F0000000000)
#define FK_DWORD_12(dwValue) (dwValue & 0x0000F00000000000)
#define FK_DWORD_13(dwValue) (dwValue & 0x000F000000000000)
#define FK_DWORD_14(dwValue) (dwValue & 0x00F0000000000000)
#define FK_DWORD_15(dwValue) (dwValue & 0x0F00000000000000)
#define FK_DWORD_16(dwValue) (dwValue & 0xF000000000000000)
#endif	// _WIN64


//
// 某个位变位 0
//
#ifdef _WIN32
#define FK_DWORD_01(dwValue) (dwValue &~ 0x0000000F)
#define FK_DWORD_02(dwValue) (dwValue &~ 0x000000F0)
#define FK_DWORD_03(dwValue) (dwValue &~ 0x00000F00)
#define FK_DWORD_04(dwValue) (dwValue &~ 0x0000F000)
#define FK_DWORD_05(dwValue) (dwValue &~ 0x000F0000)
#define FK_DWORD_06(dwValue) (dwValue &~ 0x00F00000)
#define FK_DWORD_07(dwValue) (dwValue &~ 0x0F000000)
#define FK_DWORD_08(dwValue) (dwValue &~ 0xF0000000)

#define FK_DWORD_0LO(dwValue) (dwValue &~ 0x0000FFFF)
#define FK_DWORD_0HI(dwValue) (dwValue &~ 0xFFFF0000)

#endif // _WIN32
#ifdef _WIN64
#define FK_DWORD_01(dwValue)  (dwValue &~ 0x000000000000000F)
#define FK_DWORD_02(dwValue)  (dwValue &~ 0x00000000000000F0)
#define FK_DWORD_03(dwValue)  (dwValue &~ 0x0000000000000F00)
#define FK_DWORD_04(dwValue)  (dwValue &~ 0x000000000000F000)
#define FK_DWORD_05(dwValue)  (dwValue &~ 0x00000000000F0000)
#define FK_DWORD_06(dwValue)  (dwValue &~ 0x0000000000F00000)
#define FK_DWORD_07(dwValue)  (dwValue &~ 0x000000000F000000)
#define FK_DWORD_08(dwValue)  (dwValue &~ 0x00000000F0000000)
#define FK_DWORD_09(dwValue)  (dwValue &~ 0x0000000F00000000)
#define FK_DWORD_010(dwValue) (dwValue &~ 0x000000F000000000)
#define FK_DWORD_011(dwValue) (dwValue &~ 0x00000F0000000000)
#define FK_DWORD_012(dwValue) (dwValue &~ 0x0000F00000000000)
#define FK_DWORD_013(dwValue) (dwValue &~ 0x000F000000000000)
#define FK_DWORD_014(dwValue) (dwValue &~ 0x00F0000000000000)
#define FK_DWORD_015(dwValue) (dwValue &~ 0x0F00000000000000)
#define FK_DWORD_016(dwValue) (dwValue &~ 0xF000000000000000)
#endif	// _WIN64


// 取出某个位的值，成为基数值。
#ifdef _WIN32
#define FK_DWORD_V1(dwValue) (dwValue		& 0x0000000F)
#define FK_DWORD_V2(dwValue) ((dwValue>> 4) & 0x0000000F)
#define FK_DWORD_V3(dwValue) ((dwValue>> 8) & 0x0000000F)
#define FK_DWORD_V4(dwValue) ((dwValue>>12) & 0x0000000F)
#define FK_DWORD_V5(dwValue) ((dwValue>>16) & 0x0000000F)
#define FK_DWORD_V6(dwValue) ((dwValue>>20) & 0x0000000F)
#define FK_DWORD_V7(dwValue) ((dwValue>>24) & 0x0000000F)
#define FK_DWORD_V8(dwValue) ((dwValue>>28) & 0x0000000F)
#endif // _WIN32

#ifdef _WIN64
#define FK_DWORD_V1(dwValue)  (dwValue       & 0x000000000000000F)
#define FK_DWORD_V2(dwValue)  ((dwValue>> 4) & 0x000000000000000F)
#define FK_DWORD_V3(dwValue)  ((dwValue>> 8) & 0x000000000000000F)
#define FK_DWORD_V4(dwValue)  ((dwValue>>12) & 0x000000000000000F)
#define FK_DWORD_V5(dwValue)  ((dwValue>>16) & 0x000000000000000F)
#define FK_DWORD_V6(dwValue)  ((dwValue>>20) & 0x000000000000000F)
#define FK_DWORD_V7(dwValue)  ((dwValue>>24) & 0x000000000000000F)
#define FK_DWORD_V8(dwValue)  ((dwValue>>28) & 0x000000000000000F)
#define FK_DWORD_V9(dwValue)  ((dwValue>>32) & 0x000000000000000F)
#define FK_DWORD_V10(dwValue) ((dwValue>>36) & 0x000000000000000F)
#define FK_DWORD_V11(dwValue) ((dwValue>>40) & 0x000000000000000F)
#define FK_DWORD_V12(dwValue) ((dwValue>>44) & 0x000000000000000F)
#define FK_DWORD_V13(dwValue) ((dwValue>>48) & 0x000000000000000F)
#define FK_DWORD_V14(dwValue) ((dwValue>>52) & 0x000000000000000F)
#define FK_DWORD_V15(dwValue) ((dwValue>>56) & 0x000000000000000F)
#define FK_DWORD_V16(dwValue) ((dwValue>>60) & 0x000000000000000F)
#endif	// _WIN64

#define MAKELONG64(a, b)      ((LONG64)(((DWORD)((LONG64)(a) & 0xffffffff)) | ((LONG64)((DWORD)((LONG64)(b) & 0xffffffff))) << 32))


#define FK_BEGIN_CODE(...)
#define FK_END_CODE(...)
#define FK_NOTIFY_POS


//TCHAR SpecialCha[]={'/','\\','\"',':','*','?','<','>','|','\t'};

#define MAX_WNDCLASS_NAME				128
#define HWND_NULL						NULL
#define STRNIL							0
#define HRESNIL							0

#define	SAF_DEFAULT_FONT				L"Tahoma"
#define	SAF_DEFAULT_FONT_SIZE			12

#define TOOLBAR_BUTTON_WIDTH			23
#define TOOLBAR_BUTTON_HEIGHT			22
#define COMMAND_BAR_BUTTON_WIDTH		16
#define COMMAND_BAR_BUTTON_HEIGHT		16

#ifdef _WIN32
#define INT_S_LEN		12
#endif
#ifdef _WIN64
#define INT_S_LEN		21
#endif

enum enumFreeSelf
{
	umFreeSelfYes	= 0x01,
	umFreeSelfNo	= 0x02,
};

inline bool ModifyDWord(DWORD *dwMask, DWORD dwRemove, DWORD dwAdd)
{
	DWORD dwMaskTemp=(*dwMask& ~dwRemove) | dwAdd;
	if(dwMaskTemp == *dwMask)
		return false;

	*dwMask=dwMaskTemp;
	return true;
}

inline bool ModifyByte(BYTE *dwMask, BYTE dwRemove, BYTE dwAdd)
{
	BYTE dwMaskTemp=(*dwMask& ~dwRemove) | dwAdd;
	if(dwMaskTemp == *dwMask)
		return false;

	*dwMask=dwMaskTemp;
	return true;
}

inline bool ModifyUINT(UINT *dwMask, UINT dwRemove, UINT dwAdd)
{
	UINT dwMaskTemp=(*dwMask& ~dwRemove) | dwAdd;

	if(dwMaskTemp == *dwMask)
		return false;

	*dwMask=dwMaskTemp;
	return true;
}

enum enumReturn
{
	umReturnError=0x1,			// 操作失败,致命错误。
	umReturnSuccess=0x2,		// 操作成功。
	umReturnCancel=0x3,			// 操作取消。
	umReturnNoOperation=0x4,	// 操作取消，但是没有操作。
	umReturnWarning=0x5,		// 操作成功，有错误信息存在，但是不致命，可以忽略。
};


#define _FK_TEXTLINE_			"---------------------------------------------------------"

/////////////////////////////////////////////////////////////////////////////////////////
#define FK_COM_ARQ() \
	LONG _dwRef;\
virtual ULONG STDMETHODCALLTYPE AddRef();\
virtual ULONG STDMETHODCALLTYPE Release();\
virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

#define FK_COM_A_CODE(__ClassName) \
	ULONG STDMETHODCALLTYPE __ClassName::AddRef()\
{\
	if (_dwRef<0)\
	_dwRef = 0;\
	InterlockedIncrement(&_dwRef);\
	fk_TraceDebug(FTEXTNIL, L"%s::AddRef _dwRef=%d\n", (wchar_t*)_CRT_WIDE(#__ClassName), _dwRef);\
	return _dwRef;\
	}\

//------------------------------------------------------------------------------------
#define FK_COM_AR_DELETE_CODE(__ClassName) \
	ULONG STDMETHODCALLTYPE __ClassName::AddRef()\
{\
	if (_dwRef<0)\
		_dwRef = 0;\
	InterlockedIncrement(&_dwRef);\
	fk_TraceDebug(FTEXTNIL, L"%s::AddRef _dwRef=%d\n", (wchar_t*)_CRT_WIDE(#__ClassName), _dwRef);\
	return _dwRef;\
}\
\
	ULONG STDMETHODCALLTYPE __ClassName::Release()\
{\
	InterlockedDecrement(&_dwRef);\
	if(_dwRef == 0)\
	{\
		fk_TraceDebug(FTEXTNIL, L"%s::Release _dwRef=%d\n  delete this;\n", (wchar_t*)_CRT_WIDE(#__ClassName), _dwRef);\
		delete this;\
	}\
	else if(_dwRef <= 0)\
	{\
		fk::DbgLockRelease(&_dwRef, _CRT_WIDE(#__ClassName), _CRT_WIDE(__FILE__), __LINE__);\
		FKASSERT(0)\
	}\
	\
	return _dwRef;\
}

//------------------------------------------------------------------------------------
#define FK_COM_AR_NOTDELETE_CODE(__ClassName) \
	ULONG STDMETHODCALLTYPE __ClassName::AddRef()\
{\
	if (_dwRef<0)\
		_dwRef = 0;\
	InterlockedIncrement(&_dwRef);\
	fk::DbgLockAddRef(&_dwRef, _CRT_WIDE(#__ClassName), _CRT_WIDE(__FILE__), __LINE__);\
	return _dwRef;\
}\
\
	ULONG STDMETHODCALLTYPE __ClassName::Release()\
{\
	InterlockedDecrement(&_dwRef);\
	fk_TraceDebug(FTEXTNIL, L"%s::Release AR_NOTDELETE _dwRef=%d\n", _CRT_WIDE(#__ClassName), _dwRef);\
	if(_dwRef < 0)\
	{\
		fk::DbgLockRelease(&_dwRef, _CRT_WIDE(#__ClassName), _CRT_WIDE(__FILE__), __LINE__);\
		FKASSERT(0)\
	}\
\
	return _dwRef;\
}

//------------------------------------------------------------------------------------
//		fk_TraceDebug(FTEXTNIL, L"____::QueryInterface _dwRef=%d", _dwRef);
#ifdef __GNUC__
#define FK_QUERY_IID(riid, iidRet, _interface, ppv, pobj)  \
if (::IsEqualIID(riid, iidRet)) \
{ \
	((_interface*)pobj)->AddRef(); \
	*ppv = static_cast<_interface*>(pobj); \
	fk::DbgLockQueryInterfaceA(riid, ppv, &_dwRef, __FUNCTION__, _CRT_WIDE(__FILE__), __LINE__);\
	return S_OK; \
}

#define FK_QUERY_IID_ATL(riid, iidRet, _interface, ppv, pobj)  \
if (::IsEqualIID(riid, iidRet)) \
{ \
	((_interface*)pobj)->AddRef(); \
	*ppv = static_cast<_interface*>(pobj); \
	fk::DbgLockQueryInterfaceA(riid, ppv, &m_dwRef, __FUNCTION__, _CRT_WIDE(__FILE__), __LINE__);\
	return S_OK; \
}
#else
#define FK_QUERY_IID(riid, iidRet, _interface, ppv, pobj)  \
if (::IsEqualIID(riid, iidRet)) \
{ \
	((_interface*)pobj)->AddRef(); \
	*ppv = static_cast<_interface*>(pobj); \
	fk::DbgLockQueryInterface(riid, ppv, &_dwRef, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__);\
	return S_OK; \
}

#define FK_QUERY_IID_ATL(riid, iidRet, _interface, ppv, pobj)  \
if (::IsEqualIID(riid, iidRet)) \
{ \
	((_interface*)pobj)->AddRef(); \
	*ppv = static_cast<_interface*>(pobj); \
	fk::DbgLockQueryInterface(riid, ppv, &m_dwRef, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__);\
	return S_OK; \
}
#endif // __GNUC__

//------------------------------------------------------------------------------------
#define FK_COM_DISPATCH()  \
virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(UINT *pctinfo);\
virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo** ppTInfo);\
virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId);\
virtual HRESULT STDMETHODCALLTYPE Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr);

#define FK_COM_DISPATCH_CODE(__ClassName) \
HRESULT STDMETHODCALLTYPE __ClassName::GetTypeInfoCount(UINT *pctinfo) {  return E_NOTIMPL;	}\
HRESULT STDMETHODCALLTYPE __ClassName::GetTypeInfo(UINT iTInfo, LCID lcid, ITypeInfo** ppTInfo) {  return E_NOTIMPL;	}\
HRESULT STDMETHODCALLTYPE __ClassName::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId) {  return E_NOTIMPL;	} \
HRESULT STDMETHODCALLTYPE __ClassName::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult, EXCEPINFO *pExcepInfo, UINT *puArgErr) {  return E_NOTIMPL;	}


/////////////////////////////////////////////////////////////////////////////////////////
inline bool VariantBoolToBool(VARIANT_BOOL vbValue)
{
	if(vbValue)
		return true;
	return false;
}
BOOL inline VariantBoolToBOOL(VARIANT_BOOL vbValue)
{
	if(vbValue)
		return true;

	return false;
}

bool inline BOOLToBool(BOOL bValue)
{
	if(bValue)
		return true;
	return false;
}
VARIANT_BOOL inline BOOLToVariantBool(BOOL bValue)
{
	if(bValue)
		return VARIANT_TRUE;

	return VARIANT_FALSE;
}

BOOL inline BoolToBOOL(bool bValue)
{
	if(bValue)
		return TRUE;

	return FALSE;
}
VARIANT_BOOL inline BoolToVariantBool(bool bValue)
{
	if(bValue)
		return VARIANT_TRUE;
	return VARIANT_FALSE;
}

#ifdef __GNUC__
#else
inline COLORREF LightenColor(long lScale, COLORREF lColor)
{
	long R = MulDiv(255-GetRValue(lColor),lScale,255)+GetRValue(lColor);
	long G = MulDiv(255-GetGValue(lColor),lScale,255)+GetGValue(lColor);
	long B = MulDiv(255-GetBValue(lColor),lScale,255)+GetBValue(lColor);

	return RGB(R, G, B);
}
#endif // __GNUC__







/*--------------------------------------------------------------------------*/
// 以下代码逐渐不在使用。
//

#if (_FK_OLD_VER<=0x0001)



#define STR_FILE_NAME_VALUEA			"/\\\":*?<>|\t"
#define STR_FILE_NAME_VALUEW			L"/\\\":*?<>|\t"

#ifdef _UNICODE
#define STR_FILE_NAME_VALUE				STR_FILE_NAMEW
#else
#define STR_FILE_NAME_VALUE				STR_FILE_NAMEA
#endif


/*
** 菜单使用。
** WPARAM wParam,	Bitmap size
** LPARAM lParam,	Button size
** ::SendMessage(hWndCtrl, WM_USER_MENU_ITEM_SIZE, (WPARAM)&m_szBitmap, (LPARAM)&m_szButton);
*/
#define  WM_MY_USER						WM_USER
#define  WM_USER_MENU_ITEM_SIZE			(WM_MY_USER+1)


/*
** 菜单使用。
** LPARAM lParam, return ImageList
** ::SendMessage(hWndCtrl, WM_USER_MENU_ITEM_SIZE, 0, (LPARAM)&m_hImageList);
*/
#define  WM_USER_IMAGELIST			(WM_MY_USER+2)
#define  WM_USER_APPLY				(WM_MY_USER+3)
#define  WM_BASE					(WM_USER+1)
/*
**	lParam - 指定 wParam 参数的类型。
**  wParam - 指定窗口句柄 或者是 IDataView对象.
*/
#define  WM_USER_CHILDACTIVE	(WM_BASE+1)

/*
** 定义文件夹下的文件改动后的通报消息。
** lParam 参数指定一共有多少文件改动。
*/
#define MYMSG_SYN_EXIST_SMALL_SHOW_INFO		(WM_BASE + 1000)
#define MYMSG_SYN_NOTEXIST_SMALL_SHOW_INFO	(WM_BASE + 1001)
#define MYMSG_NOTIFY_SHELLFIND_FILECOUNT	(WM_BASE + 1002)
#define WM__SumChildFileCount				(WM_BASE + 1003)


#define  UCA_DATAVIEW						0
#define  UCA_HWND							1
#define  UCA_GENERIC_PANEL					2



//
//	激活窗口中的应用按钮
//	lParam :
//	::SendMessage(hWndCtrl, WM_USER_APPLAY, WS_APPLY_ENABLE, 0);
//
#define  WS_APPLY_ENABLE					1			//Apply按钮变为活动状态
#define  WS_APPLY_DISABLED   				2			//Apply按钮变为禁止状态
#define  WS_APPLY_APPLY						3			//触发Apply按钮命令。

#define SAF_MAINFRMAE_COMMAND_BAR			L"_CommandBar"
#define SAF_TOOLBARGENERICPANEL				L"ToolGenericPanel"
#define STR_ComFrameworkName				_T("COM Framework")

#define MACRO_ComFrameworkPath				_T("$(ComFrameworkPath)")
#define MACRO_SAFTOOLS_DLL_PATH				_T("$(SafToolsDllPath)")
#define MACRO_ProductPath					_T("$(ProductPath)")
#define MACRO_ComFrameworkName				_T("$(COM_FrameworkName)")
#define MACRO_TempFolder					_T("$(TempFolder)")
#define MACRO_ComFramework_Folder			_T("$(ComFrameworkFolder)")
#define MACRO_CONFIG_EXE_PATH				_T("$(ConfigExePath)")
#define MACRO_CONFIG_IMAGE_PATH				_T("$(ConfigImagesPath)")
#define MACRO_CONFIG_PATH					_T("$(ConfigPath)")
#define MACRO_RES_PATH						_T("ResPath")

#define MACRO_SOFTWARE_NAME					_T("{73B9189A675DBF44BE53}")
#define MACRO_SOFTWARE_CHILD_NAME			_T("{B31162FEE8692759B8FB}")
#define STR_FRAMEWORK_FOLDER_NAMEW			L"ComFramework"
#define STR_FRAMEWORK_FOLDER_NAMEA			"ComFramework"
#define STR_LOG_EXT_NAMEW					L".txt"
#define STR_LOG_EXT_NAMEA					".txt"

//#define STR_SAF_TOOLS_DLLA					"safTools.dll"
//#define STR_SAF_TOOLS_DLLW					_CRT_WIDE(STR_SAF_TOOLS_DLLA)
//
//
//#ifdef UNICODE
//#define STR_SAF_TOOLS_DLL		STR_SAF_TOOLS_DLLW
//#else
//#define STR_SAF_TOOLS_DLL		STR_SAF_TOOLS_DLLA
//#endif

//
//#define STR_WEB_LINK_LONGA					"http://www.conching.net"
//#define STR_WEB_LINK_LONGW					_CRT_WIDE(STR_WEB_LINK_LONGA)
//
//static const char*		STR_WEB_LINKA = "http://www.conching.net";
//static const wchar_t*	STR_WEB_LINKW = L"http://www.conching.net";
//
//#ifdef UNICODE
//#define STR_WEB_LINK	STR_WEB_LINKW
//#else
//#define STR_WEB_LINK	STR_WEB_LINKA
//#endif
//
//
//#define STR_GUESTBOOK_LINK_LONGA			"http://www.conching.net/bbs"
//#define STR_GUESTBOOK_LINK_LONGW			_CRT_WIDE(STR_GUESTBOOK_LINK_LONGA)
//#define STR_GUESTBOOK_LINKA					"http://www.conching.net/bbs"
//#define STR_GUESTBOOK_LINKW					_CRT_WIDE(STR_GUESTBOOK_LINKA)
//
//#ifdef UNICODE
//#define STR_WEB_LINK_LONG					STR_WEB_LINK_LONGW
//#define STR_WEB_LINK						STR_WEB_LINKW
//#define STR_GUESTBOOK_LINK_LONG				STR_GUESTBOOK_LINK_LONGW
//#define STR_GUESTBOOK_LINK					STR_GUESTBOOK_LINKW
//#else
//#define STR_WEB_LINK_LONG					STR_WEB_LINK_LONGA
//#define STR_WEB_LINK						STR_WEB_LINKA
//#define STR_GUESTBOOK_LINK					STR_GUESTBOOK_LINKA
//#define STR_GUESTBOOK_LINK_LONG				STR_GUESTBOOK_LINK_LONGA
//#endif
//

//定义数据存放在注册表的什么位置。
#define REG_CONCHING_CONFIG_PATH					_T("Software\\Conching\\Config")
#define REG_CONCHING_THEME_PATH_KEY					_T("ThemePath")
#define REG_CONCHING_CONFIG_PATH_KEY		 		_T("ConfigPath")
#define REG_CONCHING_TESTCONFIG_PATH_KEY			_T("TestConfigPath")
#define REG_CONCHING_SHELLTREE_CHANGE_KEY			_T("ShellTreeChange")


#ifdef UNICODE
#define STR_FRAMEWORK_FOLDER_NAME		STR_FRAMEWORK_FOLDER_NAMEW
#else
#define STR_FRAMEWORK_FOLDER_NAME		STR_FRAMEWORK_FOLDER_NAMEA
#endif

#ifdef UNICODE
#define STR_LOG_EXT_NAME		STR_LOG_EXT_NAMEW
#else
#define STR_LOG_EXT_NAME		STR_LOG_EXT_NAMEA
#endif



#define _FM_BEGIN			namespace fm {
#define _FM_END				}



enum enumClassValue
{
	cvNew			= 0x1,
	cvDelete		= 0x2,
	cvNewArray		= 0x3,
	cvDeleteArray	= 0x4,
};


//
// 以下代码逐渐不在使用。
//

//
//操作一个DWORD的值.
class CItemMask
{
public:
	CItemMask(DWORD dwMask=0):
	  m_dwMask(dwMask)
	  {
	  }

	  BOOL TestStyle(DWORD dwMask)
	  {
		  if(m_dwMask & dwMask)
			  return TRUE;
		  return FALSE;
	  }

	  BOOL Modify(DWORD dwRemove, DWORD dwAdd)
	  {
		  DWORD dwMask=(m_dwMask& ~dwRemove) | dwAdd;
		  if(dwMask == m_dwMask)
			  return FALSE;
		  m_dwMask=dwMask;
		  return TRUE;
	  }

	  CItemMask operator=(DWORD dwMask)
	  {
		  m_dwMask=dwMask;
		  return *this;
	  }
	  operator DWORD()
	  {
		  return m_dwMask;
	  }
	  DWORD  m_dwMask;

protected:

private:

};


#endif  // _FK_OLD_VER <= 0x0001

_FK_END


#endif		/* __FK_DEF_H__ */
