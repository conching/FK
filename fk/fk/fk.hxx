#ifndef __FK_H__
#define __FK_H__


//
// fk 预览片。逐渐，添加，修改，为最佳状态。
//
// stdafx.h 文件中包含 fk.h 文件。
//
// stdafx.cpp 文件中包含 fk.c 文件。
//

#ifndef __GNUC__
#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>
using namespace ATL;
#endif

// Tracelog.dll
#include "fk\def.hxx"
#include "fk\TraceLog.h"
#include "fk\base.hxx"
#include "fk\array.hxx"
#include "fk\trace.hxx"
#include "fk\LibXmlObj.h"
#include "fk\xmlFile.hxx"
#include "fk\class.hxx"
#include "fk\RunInfo.hxx"
#include "fk\raddef.hxx"
#include "fk\CommandLine.hxx"
#include "fk\CommandMode.hxx"
#include "fk\fonts.hxx"

#ifndef __GNUC__
#include "fk\atlex.h"
#endif

// atobj.dll
#include "fk\atobj.hxx"

// joint.fk.dll
#include "fk\ModuleConfig.hxx"

// safTools.dll
#include "fk\fkgdi.h"
#include "fk\win.hxx"
#include "fk\ctrls.hxx"
#include "fk\WinCtrl.hxx"
#include "fk\safTools.h"

#include "fk\ModuleConfigCtrl.hxx"

// ArtFramework.dll
#ifndef __GNUC__
#include "fk\fkimpl.h"
#endif

#endif // __FK_H__
