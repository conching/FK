#ifndef __Trace_h__
#define __Trace_h__


#ifdef __FK_VC__
#include <atldef.h>
#include <comdef.h>
#include <stdio.h>
#include <tchar.h>

#endif // __FK_VC__


#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */



_FK_BEGIN

class LStringws;
class LStringas;


#define FTEXTNIL					0


#ifdef UNICODE
#define __Function__	(LPCWSTR)__FUNCTIONW__
#else
#define __Function__	(LPCSTR)__FUNCTION__
#endif


#ifdef _FK_USER_CALL_LINE_PARAM_
#define _FK_CALL_LINE_PARAM_H1		LPCWSTR szFileName=__TFILE__, LPCWSTR szFunction=L"", UINT uiLine=__LINE__
#define _FK_CALL_LINE_PARAM_CPP1	LPCWSTR szFileName, LPCWSTR szFunction, UINT uiLine

#define _FK_CALL_LINE_PARAM_H2		,LPCWSTR szFileName=__TFILE__, LPCWSTR szFunction=L"", UINT uiLine=__LINE__
#define _FK_CALL_LINE_PARAM_CPP2	,LPCWSTR szFileName, LPCWSTR szFunction, UINT uiLine
#else
#define _FK_CALL_LINE_PARAM_H1
#define _FK_CALL_LINE_PARAM_CPP1

#define _FK_CALL_LINE_PARAM_H2
#define _FK_CALL_LINE_PARAM_CPP2
#endif



#define STR_LOG_EXT_NAMEW			L".txt"
#define STR_LOG_EXT_NAMEA			".txt"

#define LOG_ERROR_INFO				"Error:"			/* 显示错误信息！，Debug Release 都有作用，同时写入 .log 文件。 */
#define LOG_WARNING_INFO			"Warning:"
#define LOG_RUN_INFO				"RunInfo:"			/* 程序执行中的一些信息。 */
#define LOG_DEBUG_INFO				"Debug:"

//#define LOG_FATAL_INFO				_T("Fatal:")			/* 程序功能执行失败的信息，下一版本去掉 这个功能。 */
//#define LOG_SUCCESS_INFO			_T("Success:")			/* 程序功能执行成功的信息。 */			/* 合并 Fatal Sucess功能到 Execute中。 */
#define LOG_INFO_EXECUTE			"Execute:"			/* 程序功能执行成功的信息。 */

#define LOG_INFO_MEMORY				_T("Memory:")
#define LOG_TEST_INFO				_T("Test:")

#define LOG_ERROR_INFO_LEN			_tcslen(LOG_ERROR_INFO)
#define LOG_INFO_INFO_LEN			_tcslen(LOG_INFO)
#define LOG_WARNING_INFO_LEN		_tcslen(LOG_RUN_INFO)
#define LOG_FATAL_INFO_LEN			_tcslen(LOG_FATAL_INFO)


#define CLSM_NOTIFY_TEXT_INFO	0x00000001
#define CLSM_STYLE				0x00000002



enum TRACE_LOG_INFO
{
	tliShowBox	=0X00000001,
	tliWriteLog	=0X00000002,
	tliBreak	=0X00000004,
	tliTraceText=0X00000008,
};

FTEXT WINAPI CreateDefaultLog();

#define CLS_NULL_LOG_DELETE		0x00000001		// 如果日志是空，可以删除文件。
#define CLS_NEW_LOG				0x00000002		// 每次都建立一个新的文件。
#define CLS_ERROR				0x00000100
//#define CLS_FAIL 				0x00000200
#define CLS_WARNING				0x00000400
#define CLS_DEBUG				0x00000800
//#define CLS_SUCCESS				0x00002000
#define CLS_EXECUTE				0x00008000
#define CLS_INFO				0x00010000
#define CLS_MEMORY				0x00020000
#define CLS_LOG_ALL				(CLS_DEBUG|CLS_INFO|CLS_WARNING|CLS_ERROR|CLS_MEMORY|CLS_EXECUTE)		//CLS_FAIL|CLS_SUCCESS|

/*
** 建立一个日志文件。如果 szFileName 参数设置为空，日志会自己定义一个日志文件名称。
 */
FTEXT WINAPI CreateLogA(LPCSTR szFileName, WORD dwStyle);
FTEXT WINAPI CreateLogW(LPCWSTR szFileName, DWORD dwStyle);
#ifdef UNICODE
#define CreateLog CreateLogW
#else
#define CreateLog CreateLogA
#endif

BOOL WINAPI CloseLogFileW(FTEXT lpLogFile);
BOOL WINAPI CloseLogFileA(FTEXT lpLogFile);
#ifdef UNICODE
#define CloseLogFile CloseLogFileW
#else
#define CloseLogFile CloseLogFileA
#endif

BOOL WINAPI SaveLogFile(FTEXT ftext);


enum umRunState
{
	rsDebug	=0x0001,
	rsTest	=0x0002,
};

b_call SetRunState(DWORD dwMask);
dw_call GetRunState(void);

//#define FKRS_BEGINREPAIRHINT	0x00000001
//#define FKRS_REPAIRHINT			0x00000002

#define FKRS_SHOWBEGINBOX		0x00000002
#define FKRS_SHOWENDBOX			0x00000002

b_fncall RepairFK(HWND hWnd, DWORD dwStyle);
b_fncall RepairReadInfo(UINT* uipSeelp, DWORD* dwBeginHint, DWORD* dwEndHint);

typedef void (_fncall* FN_REPAIR_PROCESS_COM)(fk::LStringws* psFiles);
b_fncall RepairAddProcess(FN_REPAIR_PROCESS_COM fnRepairProcessCom);


BOOL WINAPI TraceModifyShowType(FTEXT ftext, DWORD dwRemoveStyle, DWORD dwAddStyle);


typedef void (WINAPI* FNOUTPUTINFOPROCA)(const char*, int);
typedef void (WINAPI* FNOUTPUTINFOPROCW)(const wchar_t*, int);
void WINAPI SetOutputInfoProcA(FNOUTPUTINFOPROCA fnOutputInfoProc);
void WINAPI SetOutputInfoProcW(FNOUTPUTINFOPROCW fnOutputInfoProc);
#ifdef _UNICODE
#define FmSetOutputInfoProc	SetOutputInfoProcW
#else
#define FmSetOutputInfoProc	SetOutputInfoProcA
#endif

b_fncall OutputWindowIsLink();
b_fncall OutputWindowLink();
b_fncall OutputWindowUnlink();
typedef void (_fncall* FNSHOWOUTPUTWINDOWLINK)();

v_fncall OutputWindowSetShow(FNSHOWOUTPUTWINDOWLINK fnShowOutputWindowLink);



void WINAPI TraceFileNameLine(FTEXT hlogFile, LPCWSTR szFileName, LPCWSTR szFunctionName, UINT uiLine);
void WINAPI TraceErrorBoxFileLine(FTEXT hlogFile, LPCWSTR szFileName, LPCWSTR szFunctionName, UINT uiLine);
#ifdef _DEBUG
#define fk_TraceFileNameLine(_hLogFile)		fk::TraceFileNameLine(_hLogFile, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#else
#define fk_TraceFileNameLine
#endif
void WINAPI ErrorSetFileNameLinePos(FTEXT hlogFile, LPCWSTR szFileName, LPCWSTR szFunctionName, UINT uiLine);

#define ATLMSI_SHOWTIME			0x00000001
#define ATLMSI_SHOWFILENAME		0x00000002
#define ATLMSI_SHOWFUNCTION		0x00000004
#define ATLMSI_SHOWLINE			0x00000008
#define ATLMSI_INFOLINEFEED		0x00000010
#define ATLMSI_SHOWFULLNAME		0x00000020

#define ATLMSI_ALL		(ATLMSI_SHOWTIME|ATLMSI_SHOWFILENAME|ATLMSI_SHOWFUNCTION|ATLMSI_SHOWLINE)

BOOL WINAPI TraceModifyShowInfo(FTEXT ftext, DWORD dwRemoveStyle, DWORD dwAddStyle);

BOOL WINAPI TraceAddRoot(FTEXT ftext);
BOOL WINAPI TraceAddNode(FTEXT ftext);
BOOL WINAPI TraceBackNode(FTEXT ftext);

bool WINAPI fmEndalbeModule(FTEXT hLog, HMODULE hModule, bool bEnable);
typedef struct tagLOGCONFIG
{
	DWORD dwSize;
	DWORD dwLogMask;
}LOGCONFIG,*PLOGCONFIG;

typedef bool (WINAPI* CONFIGPROC)(PLOGCONFIG plogConfig);
typedef struct tagLOGMODULE
{
	DWORD dwSize;
	HMODULE hModule;
	CONFIGPROC pfnConfigProc;
}LOGMODULE,*PLOGMODULE;
bool WINAPI fmRegisterModule(FTEXT hLog, PLOGMODULE plogModule);
bool WINAPI fmUnregisterModule(FTEXT hLog, HMODULE hModule);


BOOL WINAPI FmSetTraceTypeVW(FTEXT ftext,
							 LPCWSTR szType,
							 LPCWSTR szFile,
							 LPCWSTR szFunctionName,
							 int nLine,
							 LPCWSTR szFormat,
							 va_list argList
							 );
BOOL WINAPI FmSetTraceTypeW(FTEXT ftext,
							LPCWSTR szType,
							LPCWSTR szFile,
							LPCWSTR szFunctionName,
							int	nLine,
							LPCWSTR szFormat,
							...
							);
// Description:
//		创建文件夹地址条到父窗口中，并不自己建立父窗口。与CreateAddressBandRoot
//	函数互斥。
//
// See Also:
//
// Arguments:
//  	s -
// Return Value:
//
// Summary:
BOOL WINAPI TraceTypePrintfVW(FTEXT ftext,
								LPCWSTR szType,
								LPCWSTR szFile,
								LPCWSTR szFunctionName,
								int	nLine,
								LPCWSTR szFormat,
								va_list argList
								);
BOOL WINAPI TraceTypePrintfW(FTEXT ftext,
							   LPCWSTR szType,
							   LPCWSTR szFile,
							   LPCWSTR szFunctionName,int nLine,
							   LPCWSTR szFormat,
							   ...
							   );

BOOL WINAPI TraceSaveErrorInfo(FTEXT ftext);
BOOL WINAPI TraceError(FTEXT ftext, HINSTANCE hRes, LPCWSTR szFormat, ...);
BOOL WINAPI TraceAttackError(FTEXT ftext, HINSTANCE hRes, LPCWSTR szFormat, ...);	// 在错误信息的后面附加信息。
v_fncall TraceClearErrorStep(FTEXT fLog);											//
v_fncall TraceClearError(FTEXT flog);

bool WINAPI TraceNilLineEx(FTEXT ftext, DWORD dwMask, UINT iLine);
BOOL WINAPI TraceWarningEx(FTEXT ftext, LPCWSTR szFormat, ...);
BOOL WINAPI TraceDebug(FTEXT ftext, LPCWSTR szFormat, ...);
BOOL WINAPI TraceExecute(FTEXT ftext, LPCWSTR szFormat, ...);
BOOL WINAPI TraceAttach(FTEXT ftext, LPCWSTR szFormat, ...);
BOOL WINAPI TraceMemory(FTEXT ftext, LPCWSTR szFormat, ...);
BOOL WINAPI GetLogFileName(HINSTANCE hInstance, LPWSTR szFileName, DWORD nBufferSize);

b_fncall TraceTestNodeW(FTEXT ftext, int iSpaceLine, LPCWSTR szTestNode, ...);
#ifdef  _UNICODE
#define TraceTestNode TraceTestNodeW
#else
#define TraceTestNode TraceTestNodeA
#endif

BOOL WINAPI TraceTextA(FTEXT ftext, LPCSTR szText, ...);
BOOL WINAPI TraceTextW(FTEXT ftext, LPCWSTR szText, ...);
#ifdef UNICODE
#define TraceText TraceTextW
#else
#define TraceText TraceTextA
#endif
enum enumTraceTestState
{
	umTraceTestSuccess = 0x1,
	umTraceTestFail = 0x2,
};
BOOL WINAPI TraceTest(FTEXT ftext, fk::enumTraceTestState umTraceTest, LPCWSTR lpszCallCodeNull, LPCWSTR lpszIfCode, LPCWSTR szFormat, ...);
BOOL WINAPI TraceTestText(FTEXT ftext, LPCWSTR szFormat, ...);

//BOOL WINAPI TraceAddFunctionEx(FTEXT ftext, LPCWSTR szFile, LPCWSTR szFunctionName, int nLine, LPCWSTR szFormat, ...);
//BOOL WINAPI TraceRemoveFunctionEx(FTEXT ftext, LPCWSTR szFile, LPCWSTR szFunctionName, int nLine, LPCWSTR szFormat, ...);

//#define fk_TraceAddFunction(ftext, szFormat, ...)			fk::TraceAddFunctionEx(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__, szFormat, __VA_ARGS__);
//#define fk_TraceRemoveFunction(ftext, szFormat, ...)		fk::TraceRemoveFunctionEx(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__, szFormat, __VA_ARGS__);


BOOL WINAPI TraceFormatMessageEx(FTEXT ftext, HINSTANCE hInstance, LPCTSTR szText, DWORD dwMessageID, ...);

#ifdef _DEBUG
#define fk_TraceFormatMessage(ftext, hInstance, szText, dwMessageID, ...) { \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);\
	fk::TraceFormatMessageEx(ftext, hInstance, szText, dwMessageID, __VA_ARGS__ ); }
#else
#define fk_TraceFormatMessage(ftext, hInstance, szText, dwMessageID, ...) 	fk::TraceFormatMessageEx(ftext, hInstance, szText, dwMessageID, __VA_ARGS__ );
#endif


i_fncall ShowErrorBox(HWND hWndParent, UINT uType);

#ifdef _DEBUG
#define fk_ErrorBox()	{ \
	if (fk::ShowErrorBox(GetActiveWindow(), MB_YESNO|MB_ICONERROR)==IDYES) \
	_CrtDbgBreak(); }
#else
#define fk_ErrorBox()	fk::ShowErrorBox(GetActiveWindow(), MB_OK|MB_ICONERROR)
#endif


//typedef bool (__fncall* FNSET_RUN_ADD_PROC)(LPCWSTR lpszID1, LPCWSTR lpszID2, LPCWSTR lpszID3, HINSTANCE hres, LPCWSTR lpszText, ...);
typedef bool (_fncall* FNSET_RUN_ADD_PROC)(LPCWSTR, LPCWSTR, LPCWSTR, HINSTANCE hres, LPCWSTR, ...);
//typedef LRESULT (CALLBACK* WNDPROC)(HWND, UINT, WPARAM, LPARAM);

b_fncall SetRunAddProc(FNSET_RUN_ADD_PROC fnSetRunAddProc);

/*--------------------------------------------------------------------------*/
typedef void* FRUNINFO;

typedef struct tagRUNINFOITEM
{
	DWORD dwSize;
	DWORD dwMask;
	int iKey;
	fk::LStringw sText;
}RUNINFOITEM,*LPRUNINFOITEM;

FRUNINFO _fncall RunInfoCreate(fk::FTEXT ftext);
b_fncall RunInfoDestroy(fk::FRUNINFO fRunInfo);
b_fncall RunInfoAddItem(fk::FRUNINFO fRunInfo, LPRUNINFOITEM lpRunInfoItem);
LPRUNINFOITEM _fncall RunInfoFindItem(fk::FRUNINFO fRunInfo, int iKey);
b_fncall RunInfoRemoveItemKey(fk::FRUNINFO fRunInfo, int iKey);
b_fncall RunInfoRemoveItemPtr(fk::FRUNINFO fRunInfo, fk::LPRUNINFOITEM lpRunInfoItem);

typedef void* HRUNINFOS;
typedef struct tagRUNINFOSITEM
{
	LStringw	_sRunInfo;
	LStringw	_sFile;
	LStringw	_sFun;
	LStringw	_sRunEndInfo;
	int			_iLine;
	FTEXT		_hLogFile;
	bool		bRunSuccess;
	HRUNINFOS	hFunInfos;
}RUNINFOSITEM, *PRUNINFOSITEM;

class _FK_OUT_CLASS LRunInfo
{
private:
	PRUNINFOSITEM _pRunInfosItem;

public:
	LRunInfo(HRUNINFOS hFunInfos, FTEXT ftext, wchar_t* szRunInfo, wchar_t* szFun, wchar_t* szFile, int iLine);
	LRunInfo(void);
	virtual ~LRunInfo(void);

	int OutInitInfo(HRUNINFOS hFunInfos, FTEXT ftext, wchar_t* szRunInfo, wchar_t* szFun, wchar_t* szFile, int iLine, ...);
	void RunSuccess();
	void RunInfo2(const wchar_t* psInfo);
	void Attach(LRunInfo* pRunInfo);
};

#ifdef _DEBUG
#define FK_RUN(ftext, szRunInfo, ...)	fk::LRunInfo __RunInfo;\
	__RunInfo.OutInitInfo(NULL, ftext, szRunInfo, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__, __VA_ARGS__);
#define FK_RUN_SUCCESS()				__RunInfo.RunSuccess();
#define FK_RUN_INFO(szInfo)				__RunInfo.RunInfo2(szInfo);
#else
#define FK_RUN(ftext, szRunInfo, ...)
#define FK_RUN_SUCCESS()
#define FK_RUN_INFO(szInfo)
#endif



#ifdef _DEBUG
#define fk_NotifysCreate2(cvClassValue, ppNotify) \
	NewNotify(cvClassValue, ppNotify) && \
	fk_DbgDebugClass((void*)*ppNotify, &(*ppNotify->__pdbgClassInfo), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)
#else
#define fk_NotifysCreate2(cvClassValue, ppNotify)	NewNotify(cvClassValue, ppNotify)
#endif


#ifdef _DEBUG
#define fk_DbListViewClassVar2(cvClassValue, ppDbListView, pTable) \
	fk::fk_DbListViewClassVar(cvClassValue, ppDbListView, pTable) && \
	fk_DbgDebugClass((void*)*ppDbListView, &(*ppDbListView->__pdbgClassInfo), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)
#else
#define fk_DbListViewClassVar2(cvClassValue, ppDbListView, pTable)	fk::fk_DbListViewClassVar(cvClassValue, ppDbListView, pTable)
#endif


void WINAPI GetErrorStringW(FTEXT ftext, wchar_t* szErrorString);
#ifdef _UNICODE
#define GetErrorString GetErrorStringW
#else
#define GetErrorString TraceGetErrorStringA
#endif




/*
Description:
把VARIANT vt类型转换为字符串。
转换后的格式如下：VT_BSTR,VT_I4
See Also:

Arguments:
s -
Return Value:

Summary:
*/
int WINAPI VarTypeToStr(VARTYPE *vtpSupportArray, VARTYPE vtArraySize, LPWSTR lpszString, UINT uiBufferSize);

/*
Description:
提示VARIANT vt参数发现不支持的值。
See Also:

Arguments:
ftext - 日志文件指针。
vt - 错误的 VARIANT vt 参数类型。
vtpSupportVTArray - 支持的 VT 数组。
vtSupportArraySize - 支持的 VT 数组大小。
tliType - 当前信息的提示状态。
Return Value:
没有返回值。
Summary:
*/

void WINAPI TraceVarTypeError(FTEXT ftext, LPCWSTR pszErrorVt, LPCWSTR szSupportVt);

#ifdef _DEBUG
#define  fk_TraceVarTypeError(ftext, pszErrorVt, szSupportVt) {\
	fk::TraceVarTypeError(ftext, pszErrorVt, szSupportVt);\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); }
#else
#define  fk_TraceVarTypeError(ftext, pszErrorVt, szSupportVt) 		fk::TraceVarTypeError(ftext, pszErrorVt, szSupportVt);
#endif

void WINAPI TraceVTError(FTEXT ftext, VARTYPE *pvtSupportVt, VARTYPE tArraySize, VARTYPE tErrorVT);

#ifdef _DEBUG
#define fk_TraceVTError(ftext, vtSupportVTArray, vtArraySize, vtError) 	{\
	fk::TraceVTError(ftext, vtSupportVTArray, vtArraySize, vtError);\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); }
#else
#define fk_TraceVTError(ftext, vtSupportVTArray, vtArraySize, vtError) 		fk::TraceVTError(ftext, vtSupportVTArray, vtArraySize, vtError);
#endif




/*
Description:
调此函数说明，某个函数没有实现。 设置函数没有实现的信息到日志文件中，同时可以设置是否显示错误对话框和异常。

See Also:

Arguments:
fifType - 设置FUNCTION_IMPLE_FORMAT函数。
Return Value:
没有返回值。
*/
void WINAPI TraceFuImpleError(FTEXT ftext,
							  DWORD dwTraceText,
							  LPCWSTR szFile,
							  LPCWSTR szFunctionName,
							  int iLine
							  );

#define  fk_TraceFuImpleError(ftext, tliType ) {	fk::TraceFuImpleError(ftext, tliType, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);	}




/*
** 调用COM接口方法出现错误后，调用fk::TraceUnknownError方法，可以设置和获得错误信息并写入日志文件中。
 */
void WINAPI TraceUnknownError(FTEXT ftext,
								IUnknown *pUnknown,
								REFIID riid,
								HRESULT hrError,
								HINSTANCE hInstance,
								LPCTSTR lpszInfo,
								...
								);
#ifdef __DEBUG
#define fk_TraceUnknownError(ftext, pUnknown, riid, hrError, hInstance, lpszInfo, ...)	{\
	fk::TraceUnknownError(ftext, pUnknown, riid, hrError, hInstance, lpszInfo, __VA_ARGS__);\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);	}
#else
#define fk_TraceUnknownError(ftext, pUnknown, riid, hrError, hInstance, lpszInfo, ...)	 fk::TraceUnknownError(ftext, pUnknown, riid, hrError, hInstance, lpszInfo, __VA_ARGS__);
#endif



/*
** 显示使用 CoCreateInstance 方法创建对象失败信息对话框。
 */
void WINAPI TraceCreateInstanceErrorEx(FTEXT ftext,
									   REFCLSID rclsid,
									   REFIID riid,
									   HRESULT hResultError
									   );
#ifdef _DEBUG
#define TraceCreateInstanceError(ftext, rclsid, riid, hResultError)	{\
	fk::TraceCreateInstanceErrorEx(ftext, rclsid, riid, hResultError); \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); }

#define TraceHResultError(ftext, rclsid, riid, hResultError)		TraceCreateInstanceError(ftext, rclsid, riid, hResultError);

#else
#define TraceCreateInstanceError(ftext, rclsid, riid, hResultError)	{ fk::TraceCreateInstanceErrorEx(ftext, rclsid, riid, hResultError); }

#define TraceHResultError(ftext, rclsid, riid, hResultError)		fk::TraceCreateInstanceErrorEx(ftext, rclsid, riid, hResultError);

#endif







#ifdef _DEBUG
#define fk_TraceNilLine(ftext, clsMask, iLine)			fk::TraceNilLineEx(ftext, clsMask, iLine);

#define fk_TraceError(ftext, hRes, szFormat, ...)	{\
				fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
				fk::TraceError(ftext, hRes, szFormat, __VA_ARGS__); }

#define fk_TraceAddError(ftext, hRes, szFormat, ...)	{\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
	fk::TraceAttackError(ftext, hRes, szFormat, __VA_ARGS__); }

/*
** _DEBUG版本输出的调试信息。Release 版本没有输出。
*/
#define fk_TraceDebug(ftext, szFormat, ...)  { \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
	fk::TraceDebug(ftext, szFormat, __VA_ARGS__); }

#define fk_TraceDebugText(ftext, szFormat, ...)		fk_TraceText(ftext, szFormat, __VA_ARGS__);

/*
** 输出一些文本信息。
*/
#define fk_TraceText(ftext, szFormat, ...)  { \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
	fk::TraceText(ftext, szFormat, __VA_ARGS__); }

/*
** 程序执行过程和功能轨迹输出。
*/
#define fk_TraceExecute(ftext, szFormat, ...) { \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
	fk::TraceExecute(ftext, szFormat, __VA_ARGS__);}


#define fk_TraceWarning(ftext, szFormat, ...)	{\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);\
	fk::TraceWarningEx(ftext, szFormat, __VA_ARGS__);\
}

#define fk_TraceTest(ftext, szFormat, ...)	{\
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);\
	fk::TraceTestText(ftext, szFormat, __VA_ARGS__); }

#define	TRACE_FUNCTION()	fk_TraceText(FTEXTNIL, L"正在执行:%s", _CRT_WIDE(__FUNCTION__))

#else

#define fk_TraceError(ftext, hRes, szFormat, ...)		fk::TraceError(ftext, hRes, szFormat, __VA_ARGS__);
#define fk_TraceAddError(ftext, hRes, szFormat, ...)		fk::TraceAttackError(ftext, hRes, szFormat, __VA_ARGS__);

#define fk_TraceText(ftext, szFormat, ...)
#define fk_TraceExecute(ftext, szFormat, ...)
#define fk_TraceDebug(ftext, szFormat, ...)
#define fk_TraceDebugText(ftext, szFormat, ...)
#define fk_TraceWarning(ftext, szFormat, ...)
#define fk_TraceNilLine(ftext, clsMask, iLine)
#define fk_TraceTest(ftext, szFormat, ...)

#define	TRACE_FUNCTION()

#endif


//
//	函数名称：ParamNullBoxEx
//	功能：提示某个函数的参数不能设置为空。
//	参数：
//
//	返回值：
int WINAPI ParamNullBoxEx(__in LPCWSTR lpszParamName,		//不能为空的参数名称。
							__in LPCWSTR lpszFile,			//函数所在的文件名称。
							__in LPCWSTR lpszFunction,		//函数名称
							__in int nLine
							);

#define FmParamNullBox( lpszParamName )		fk::ParamNullBoxEx(lpszParamName, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#define FmWaringParam(expr)					fk_TraceWarning(FTEXTNIL, L"<%s> param is null.", _CRT_WIDE(#expr);





/*--------------------------------------------------------------------------*/
/*  Error function.
*/
v_fncall ErrorStruct(LPCWSTR pszStruct, DWORD dwStructSize, DWORD dwAfferentSize);
v_fncall ErrorStructMask(LPCWSTR pszStruct, LPCWSTR pszSection, DWORD dwValue);
v_fncall ErrorIndexMax(int iMax, int iIndex);
v_fncall ErrorRangeIntIndex(int pMin, int iMax, int iValue);
v_fncall ErrorStringGuid(LPCWSTR pszGuid);
v_fncall ErrorStringGuidLen(int iLen);
v_fncall ErrorXmlPathNotExistWA(FTEXT ftext, LPCWSTR lpszXmlFile, LPCSTR pszXmlPath);
v_fncall ErrorXmlPathNotExistW(FTEXT ftext, LPCWSTR lpszXmlFile, LPCWSTR pszXmlPath);
v_fncall ErrorXmlPropA(FTEXT ftext, LPCSTR lpszXmlFile, LPCSTR pszXmlPath, LPCSTR pszProp);

v_fncall ErrorStrParamNameIsNullW1(FTEXT ftext, LPCWSTR wzParamName);
v_fncall ErrorInPtrNullW(FTEXT ftext, LPCWSTR lpszObject, LPCWSTR lpszPtr);
v_fncall ErrorInPtrExist(FTEXT ftext, LPCWSTR lpszObject, LPCWSTR lpszPtr);
v_fncall ErrorDirectoryNotExists(FTEXT ftext, LPCWSTR szDirectory);
v_fncall ErrorResourceIsNull(FTEXT ftext);

v_fncall ErrorXmlReadFileA(char* pszFullName);
v_fncall ErrorClearStep(FTEXT fLog);
v_fncall ErrorCreateDirectory(LPCWSTR lpszPath);
v_fncall ErrorDataType(fk::enumDataType udt, LPCWSTR lpszFunction);
v_fncall ErrorDataTypeImple(fk::enumDataType udt, LPCWSTR lpszFunction);
v_fncall ErrorLinkObjectVariable(fk::LStringws* pStringws);
v_fncall ErrorFileExists(fk::LStringws* pFileExistsList);
v_fncall ErrorInfoStrignws(fk::LStringws* pStringws);
v_fncall ErrorInfoStrignas(fk::LStringas* pStringas);

v_fncall ErrorEnumTypeRangeW(LPCWSTR psEnumType, int iMin, int iMax, int iCurrentVal);
v_fncall ErrorEnumTypeRangeA(LPCSTR psEnumType, int iMin, int iMax, int iCurrentVal);
#if UNICODE
#define	ErrorEnumTypeRange	 ErrorEnumTypeRangeW
#else
#define	ErrorEnumTypeRange	 ErrorEnumTypeRangeA
#endif

b_fncall EnumCheckRangValueW(LPCWSTR psEnumType, int iMin, int iMax, int iCurrentVal);
#if UNICODE
#define	EnumCheckRangValue	 EnumCheckRangValueW
#else
#define	EnumCheckRangValue	 EnumCheckRangValueW
#endif

v_fncall  ErrorEnumTypeNotTextW(LPCWSTR lpszEnumType, LPCWSTR lpszEnumValue);
#if UNICODE
#define	ErrorEnumTypeNotText	 ErrorEnumTypeNotTextW
#else
#define	ErrorEnumTypeNotText	 ErrorEnumTypeNotTextW
#endif



#define fk_ErrorXmlPathNotExistWA(ftext, lpszXmlFile, pszXmlPath)	{ \
			fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
			fk::ErrorXmlPathNotExistWA(ftext, lpszXmlFile, pszXmlPath); }

/*
**	提示信息：
**	L"<bsParamName> 字符串是空的，无法设置名子。";
*/
#ifdef _DEBUG
#define ErrorStrParamNameIsNull(ftext, strParamName)	\
	fk::ErrorStrParamNameIsNullW1(ftext, strParamName); \
	fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#else
#define ErrorStrParamNameIsNull(ftext, strParamName)			fk::ErrorStrParamNameIsNullW1(ftext, strParamName);
#endif

#ifdef	_DEBUG
#define fk_ErrorInPtrNull(ftext, lpszObject, lpszPtr)	\
			fk::ErrorInPtrNullW(ftext, lpszObject, lpszPtr);	\
			fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#else
#define	fk_ErrorInPtrNull(ftext, lpszObject, lpszPtr)	fk::ErrorInPtrNullW(ftext, lpszObject, lpszPtr)
#endif


#ifdef	_DEBUG
#define fk_ErrorInPtrExist(ftext, lpszObject, lpszPtr)	\
			fk::ErrorInPtrExist(ftext, lpszObject, lpszPtr);	\
			fk::TraceFileNameLine(ftext, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#else
#define	fk_ErrorInPtrExist(ftext, lpszObject, lpszPtr)	fk::ErrorInPtrExist(ftext, lpszObject, lpszPtr)
#endif

#define FK_S_LPCWSTR	"LPCWSTR"
#define FK_S_LPWSTR		"LPWSTR"
#define FK_S_HINSTANCE	"HINSTANCE"


// 	fk_VerifyPtrNull(FTEXTNIL, szRes, FK_S_LPCWSTR, return -1;)
// TODO
#define fk_VerifyPtrNull(__hLog, __ptr, __type, __return)	\
		if (__ptr==NULL) \
		{ \
			fk::ErrorInPtrNullW(__hLog, _CRT_WIDE(__type), _CRT_WIDE(#__ptr)); \
			fk::TraceFileNameLine(__hLog, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); \
			__return; \
		}



/*--------------------------------------------------------------------------*/
class _FK_OUT_CLASS DBGNEWCLASSINFO
{
public:
	DBGNEWCLASSINFO();
	virtual ~DBGNEWCLASSINFO();

	DWORD dwSize;
	LStringw sFile;
	LStringw sFun;
	LStringw sNode;
	LStringw sTime;
	LStringw sUser;
	SYSTEMTIME systemTime;
	UINT niLine;
};

#pragma warning( disable: 4251 )
class _FK_OUT_CLASS LDebugClassInfo
{
public:
	DWORD				_dwSize;
	DWORD				_dwMemorySize;
	UINT				_uiNewID;
	DBGNEWCLASSINFO*	_pDbgClassInfo;				//

	LDebugClassInfo()
	{
		_dwSize			= sizeof(LDebugClassInfo);
		_dwMemorySize	= 0;
		_uiNewID		= 0;
		_pDbgClassInfo  = NULL;
	}

	virtual ~LDebugClassInfo()
	{
	}
};

#ifdef _DEBUG
#define _FK_DBGCLASSINFO_	\
		fk::LDebugClassInfo		__pdbgClassInfo;\
		fk::LStringw			__ClassNameWk;
#else
#define _FK_DBGCLASSINFO_
#endif

#ifdef _DEBUG
#define _FK_DBGCLASSINFO_INIT_(sClassName) \
	__ClassNameWk.Assignw(sClassName, -1);\
	__pdbgClassInfo._dwMemorySize = sizeof(fk::LDebugClassInfo);
#else
#define _FK_DBGCLASSINFO_INIT_(sClassName)
#endif


b_fncall DebugClass__(void* pobj, LDebugClassInfo* pDebugInfo, LPCWSTR wzFile, LPCWSTR wzFun, UINT uiLine);		/*  */
b_fncall DebugDelete__(void* pobj);
b_fncall DbgbugRelease__(IUnknown* ptr);
v_fncall DebugPrint__();

//
// 使用 new 创建对象并注册注册到 DebugClass 中。
// 1. fknew( ptr = new LPtr(), ptr);
// 2. fknew( ptr = NewPtr(), ptr);
// 3. fknew( ptr = GetPtr(), ptr);
//
// fkdelete(ptr)  or fkrelease(ptr)
//
#ifdef _DEBUG
#define fknew(_new_class_code_, _class_ptr_)		_new_class_code_; \
			fk::DebugClass__((void*)_class_ptr_, &(_class_ptr_->__pdbgClassInfo), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__ - 1);
#define fknew2(_new_class_code_, _class_ptr_, __AutoPtr__)		_new_class_code_; \
			fk::DebugClass__((void*)_class_ptr_, &(_class_ptr_->__pdbgClassInfo), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__ - 1);\
			__AutoPtr__.AddUnknown((IUnknown**)&_class_ptr_);
#else
#define fknew(_new_class_code_, _class_ptr_)					_new_class_code_;
#define fknew2(_new_class_code_, _class_ptr_, __AutoPtr__)		_new_class_code_; __AutoPtr__.AddUnknown((IUnknown**)&_class_ptr_);
#endif

#ifdef _DEBUG
#define fkdelete(_class_ptr_)						fk::DebugDelete__((void*)_class_ptr_);	delete _class_ptr_;
#else
#define	fkdelete(_class_ptr_)						delete _class_ptr_;
#endif

v_fncall SetCallPosition(LPCWSTR wzFile, LPCWSTR wzFun, UINT uiLine);
#ifdef _DEBUG
#define fkcall(_code)	fk::SetCallPosition(_CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__ - 1); _code;
#else
#define fkcall(_code)   _code;
#endif

void WINAPI DbgPrintSEC2(LPCRITICAL_SECTION lpsec, LPCWSTR lpInfo);
#ifdef _DEBUG
#define fmDbgPrintSEC(_SEC, _wsInfo)	fk::DbgPrintSEC2(_SEC, _wsInfo)
#else
#define fmDbgPrintSEC(_SEC, _wsInfo)
#endif



v_fncall DbgLockAddRef(LONG* dwRef, LPCWSTR wcsFn, LPCWSTR wcsFile, int iLine);
v_fncall DbgLockRelease(LONG* dwRef, LPCWSTR wcsFn, LPCWSTR wcsFile, int iLine);
v_fncall DbgLockQueryInterface(REFIID riid, void** ppv, LONG* dwRef, LPCWSTR wcsFn, LPCWSTR wcsFile, int iLine);
//v_fncall DbgDeleteRelease(LONG* dwRef, LPCWSTR wcsFn, LPCWSTR wcsFile, int iLine);

b_fncall DbgCheckRelease(int dwRef, LPCWSTR wcsFn, LPCWSTR wcsFile, int iLine);
#ifdef _DEBUG
#define fk_DbgCheckRelease(fun_release)	ATLVERIFY( fk::DbgCheckRelease(fun_release, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__) )
#else
#define fk_DbgCheckRelease(fun_release)	fun_release;
#endif

#ifdef _DEBUG
#define fk_DebugPrint()						fk::DebugPrint__
#else
#define fk_DebugPrint()
#endif

#define INISEN_LANGUAGE			_T("Language")
#define INISEN_SYSTEM			_T("System")





#ifdef __GNUC__
#define  _FK_EXPR_PARAM_COMMA(_EXPR_PARAM) __CRT_WIDE(",\n"#_EXPR_PARAM)
#else
#define  _FK_EXPR_PARAM_COMMA(_EXPR_PARAM) __CRT_WIDE(#_EXPR_PARAM)
#endif


#define  _FK_EXPR_PARAM(_EXPR_PARAM) __CRT_WIDE(#_EXPR_PARAM)


#define _FK_BEGIN_CHECK_PARAM \
{\
	bool			___param_error = false; \
	fk::LStringw	___sParam(fk::umStringBuffer, MAX_PATH);

#ifdef __GNUC__
#define _FK_CHECK_PARAM_INDEX(_EXPR_)	\
	if (_EXPR_) \
	{\
		if (___param_error)\
			___sParam.Append(__CRT_WIDE(",\n"#_EXPR_)); \
		else\
			___sParam.Append(__CRT_WIDE(#_EXPR_)); \
		___param_error = true; \
	}
#else
#define _FK_CHECK_PARAM_INDEX(_EXPR_)	\
	if (_EXPR_) \
	{\
		if (___sParam.IsEmpty())\
		{\
		___sParam.MallocBuffer(MAX_PATH);\
		}\
		if (___param_error)\
		{\
			___sParam.Append(L",\n"); \
			___sParam.Append(__CRT_WIDE(#_EXPR_)); \
		}\
		else\
		{\
			___sParam.Append(__CRT_WIDE(#_EXPR_)); \
		}\
		___param_error = true; \
	}
#endif

//_CrtDbgReportW(_CRT_ASSERT, _CRT_WIDE(__FILE__), __LINE__, _CRT_WIDE(__FUNCTION__), ___sParam._pwstr);\

#ifdef __GUND__
#else
#define _FK_END_CHECK_PARAM(_return_code) \
	if (___param_error)\
	{\
		if (fk::ParamNullBoxEx(___sParam._pwstr, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)==IDYES) \
		_CrtDbgBreak(); \
		_return_code;\
	}\
}
#endif // __GUND__

#define  fk_ParamVerify1(_EXPR_1, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamVerify2(_EXPR_1, _EXPR_2, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamVerify3(_EXPR_1, _EXPR_2, _EXPR_3, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamVerify4(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamVerify5(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamVerify6(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamVerify7(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamVerify8(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_CHECK_PARAM_INDEX(_EXPR_8) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamVerify9(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _EXPR_9, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_CHECK_PARAM_INDEX(_EXPR_8) \
	_FK_CHECK_PARAM_INDEX(_EXPR_9) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_CheckParamGuidNullVerify(__guid, _return_code)  \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(memcmp((void*)__guid, &GUID_NULL, sizeof(GUID)) \
	_FK_END_CHECK_PARAM(_return_code)
//
//#else
//
//#define  fk_ParamVerify1(_EXPR_1, _return_code)
//#define  fk_ParamVerify2(_EXPR_1, _EXPR_2, _return_code)
//#define  fk_ParamVerify3(_EXPR_1, _EXPR_2, _EXPR_3, _return_code)
//#define  fk_ParamVerify4(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _return_code)
//#define  fk_ParamVerify5(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _return_code)
//#define  fk_ParamVerify6(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _return_code)
//#define  fk_ParamVerify7(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _return_code)
//#define  fk_ParamVerify8(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _return_code)
//#define  fk_ParamVerify9(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _EXPR_9, _return_code)
//
//#define fk_CheckParamGuidNullVerify(__guid, _return_code)
//
//#endif  // _DEBUG



#ifdef _DEBUG

#define  fk_ParamAssert1(_EXPR_1, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamAssert2(_EXPR_1, _EXPR_2, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamAssert3(_EXPR_1, _EXPR_2, _EXPR_3, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamAssert4(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_END_CHECK_PARAM(_return_code)

#define  fk_ParamAssert5(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamAssert6(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamAssert7(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamAssert8(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_CHECK_PARAM_INDEX(_EXPR_8) \
	_FK_END_CHECK_PARAM(_return_code)


#define  fk_ParamAssert9(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _EXPR_9, _return_code) \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(_EXPR_1) \
	_FK_CHECK_PARAM_INDEX(_EXPR_2) \
	_FK_CHECK_PARAM_INDEX(_EXPR_3) \
	_FK_CHECK_PARAM_INDEX(_EXPR_4) \
	_FK_CHECK_PARAM_INDEX(_EXPR_5) \
	_FK_CHECK_PARAM_INDEX(_EXPR_6) \
	_FK_CHECK_PARAM_INDEX(_EXPR_7) \
	_FK_CHECK_PARAM_INDEX(_EXPR_8) \
	_FK_CHECK_PARAM_INDEX(_EXPR_9) \
	_FK_END_CHECK_PARAM(_return_code)


#define fk_CheckParamGuidNullAssert(__guid, code)  \
	_FK_BEGIN_CHECK_PARAM \
	_FK_CHECK_PARAM_INDEX(memcmp((void*)&__guid, &GUID_NULL, sizeof(GUID)) \
	_FK_END_CHECK_PARAM(_return_code)

#else
#define  fk_ParamAssert1(_EXPR_1, _return_code)
#define  fk_ParamAssert2(_EXPR_1, _EXPR_2, _return_code)
#define  fk_ParamAssert3(_EXPR_1, _EXPR_2, _EXPR_3, _return_code)
#define  fk_ParamAssert4(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _return_code)
#define  fk_ParamAssert5(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _return_code)
#define  fk_ParamAssert6(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _return_code)
#define  fk_ParamAssert7(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _return_code)
#define  fk_ParamAssert8(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _return_code)
#define  fk_ParamAssert9(_EXPR_1, _EXPR_2, _EXPR_3, _EXPR_4, _EXPR_5, _EXPR_6, _EXPR_7, _EXPR_8, _EXPR_9, _return_code)
#define  fk_CheckParamGuidNullVerify(__guid, _return_code)

#endif  // _DEBUG




#if (_FK_OLD_VER<=0x0001)

/*--------------------------------------------------------------------------*/
// 这是旧的指针检测。
// 检测指针的宏。一种保留 Rlease 也有做用，一种不保留,只在 Debug 有做用。

//
// 把一个使用 fknew fknew 宏建立的对象，移出 DbgDebugClass 列表。
//
#ifdef _DEBUG
#define fk_DebugDelete(_class_ptr_)					fk::DebugDelete__((void*)_class_ptr_);
#else
#define fk_DebugDelete(_class_ptr_)
#endif


#ifdef _DEBUG

#define fk_NullPtrError__(_ptr, code) \
	if(_ptr==NULL || _ptr==(void*)FK_0x8c || _ptr==(void*)FK_0x8cd) \
{\
	_CrtDbgReportW(_CRT_ASSERT, _CRT_WIDE(__FILE__), __LINE__, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(#_ptr));\
	if (fk::ParamNullBoxEx(_CRT_WIDE(#_ptr), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)==IDYES) \
	_CrtDbgBreak(); \
	code;\
}
#else		/* _DEBUG */
#define fk_NullPtrError__(_ptr, code) \
	if(_ptr==NULL || _ptr==(void*)FK_0x8c || _ptr==(void*)FK_0x8cd) \
{\
	if (fk::ParamNullBoxEx(_CRT_WIDE(#_ptr), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)==IDYES) \
	_CrtDbgBreak(); \
	code;\
}
#endif		/* _DEBUG */


#define fk_NullPtrGuid(_ptr, code) 			(_ptr==NULL || _ptr==(GUID*)FK_0xba || _ptr==(GUID*)FK_0x8c || _ptr==(GUID*)FK_0x8cd)

#ifdef _DEBUG
#define fk_NullPtr(_ptr)					((DWORD)_ptr==NULL || (DWORD)_ptr==FK_0x8c || (DWORD)_ptr==FK_0x8cd|| (DWORD)_ptr==FK_0xBA)
#else
#define fk_NullPtr(_ptr)					((DWORD)_ptr==NULL || (DWORD)_ptr==FK_0x8c || (DWORD)_ptr==FK_0x8cd|| (DWORD)_ptr==FK_0xba)
#endif

// 结束 这是旧的指针检测。

#ifdef _DEBUG

#define	__FK_DBGDEBUGCLASS_CALL(_class_ptr_)			fk::DebugClass__((void*)_class_ptr_, &_class_ptr_->__pdbgClassInfo, szFileName, szFunction, uiLine);

#define fk_DBGCREATE_IFCALL(CallFunction, pRegObj)		CallFunction && fk::DebugClass__((void*)pRegObj, &(pRegObj->__pdbgClassInfo), _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)
/* 没有 LDebugClassInfo */
#define fk_DBGCREATE_ND(CallFunction, pRegObj)			CallFunction && fk::DebugClass__((void*)pRegObj, NULL, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__)

#define fk_DbgDebugClass(_class_ptr_)					fk::DebugClass__(((void*)_class_ptr_, NULL, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__);
#else
#define	__FK_DBGDEBUGCLASS_CALL(_class_ptr_)
#define fk_DebugDelete(_class_ptr_)
#define fk_DBGCREATE_IFCALL(CallFunction, pRegObj)			CallFunction
#define fk_DBGCREATE_ND(CallFunction, pRegObj)				CallFunction
#endif


v_fncall fk_DbgReleaseInfo(LONG dwRef, LPCWSTR pcwClassName);

#ifdef FK_DBG_RELEASEINFO_USER
#define FK_DBG_RELEASEINFO(dwRef, pcwClassName)					fk_DbgReleaseInfo(dwRef, pcsClassName); ATLASSERT(0);
#else
#define FK_DBG_RELEASEINFO(dwRef, pcwClassName)
#endif



bool WINAPI DbgSetNewPos__(LPCWSTR wzFile, LPCWSTR wzFun, UINT uiLine);				/* 记住位置。 用 fk_DbgDebugClass代替。*/
void WINAPI DbgSetNew__(void* pptr, LDebugClassInfo* pdebugInfo, UINT niSize);	/* 记住指针。用 fk_DbgDebugClass代替。 */

#ifdef _DEBUG
#define fk_DbgSetNewPos							DbgSetNewPos__
#define fk_DbgSetNew(pptr, pdebugInfo, niSize)	fk::DbgSetNew__(pptr, pdebugInfo, niSize)
#else
#define fk_DbgSetNewPos
#define fk_DbgSetNew(pptr, pdebugInfo, niSize)
#endif


void WINAPI DebugNew__(void* pNew, UINT uiSize, LPCWSTR wzFile, LPCWSTR wzFun, UINT uiLine);
void WINAPI DbgSetNode__(void* pptr, LPCWSTR sNode);

#ifdef _DEBUG
#define fk_DebugNew								fk::DebugNew__
#define fk_DbgSetNode							fk::DbgSetNode__
#else
#define fk_DebugNew
#define fk_DbgSetNode
#endif


class CSafException
{
public:
	CSafException()
	{
	}
	CSafException(LPCWSTR lpszInfo, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		_strErrorInfo=lpszInfo;
	}
	CSafException(int nResID, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);

		LoadString(nResID);
	}

	virtual void SetErrorFunction(LPCTSTR lpszFunction)
	{
		_strFunction=lpszFunction;
		_strFunction.Appendw(L"( ", -1);
	}

	void SetErrorFileInfo(LPCTSTR pszFile, DWORD dwLine)
	{
#ifdef _DEBUG
		_strFullFile=pszFile;
		m_dwLine	=dwLine;
#endif
	}
	void SetErrorInfo(BSTR strInfo)
	{
		_strErrorInfo = strInfo;
	}
	void SetErrorInfo(int nResID)
	{
		LoadString(nResID);
	}

	virtual void Delete()
	{
		delete this;
	}
	virtual const wchar_t* GetErrorInfo()
	{

#ifdef _DEBUG
		if(_strErrorInfo.GetLength() > 0)
		{
			_strErrorInfo.AppendFormat(NULL, _T("\r\nFile:%s\r\nLine:%d"), _strFullFile._pwstr, m_dwLine);
		}
		else
		{
			_strErrorInfo.AppendFormat(NULL, _T("File:%s\r\nLine:%d"), _strFullFile._pwstr, m_dwLine);
		}
#endif

		if(_strFunction.GetLength() > 0)
		{
			_strErrorInfo.Appendw(L"\r\n", -1);
			_strErrorInfo.Appendw(_strFunction._pwstr, -1);
		}

		return _strErrorInfo._pwstr;
	}

	void AddParamErrorInfo(BSTR bstrParamName)
	{
		_strErrorInfo.Appendw(L"Param:", -1);
		_strErrorInfo.Appendw(bstrParamName, -1);
	}

	virtual HRESULT ShowErrorBox()
	{
		MessageBox(GetActiveWindow(), GetErrorInfo(),  L"Error info", MB_OK|MB_ICONERROR);
		return m_hr;
	}

	void LoadString( UINT nID) throw()
	{
#ifdef _AFX
		_strErrorInfo.LoadStr(AfxGetInstanceHandle(), nID);
#endif
#ifdef _ATL
		_strErrorInfo.LoadStr(ATL::_AtlBaseModule.m_hInstResource, nID);
#endif
	}
	HRESULT			m_hr;
protected:

	DWORD			m_dwLine;
	fk::LStringw	_strFullFile;
	fk::LStringw	_strErrorInfo;
	fk::LStringw	_strFunction;
};


class CWin32Exception : public CSafException
{
public:
	CWin32Exception( LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_dwError=::GetLastError();
	}

	CWin32Exception(INT nResID, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_dwError=::GetLastError();
		LoadString(nResID);
	}

	CWin32Exception(DWORD dwError, BSTR strInt, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_dwError=::GetLastError();
		_strErrorInfo=strInt;
	}

	CWin32Exception(DWORD dwError, LPCTSTR strInt, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_dwError=::GetLastError();
		_strErrorInfo=strInt;
	}

	CWin32Exception(LRESULT lr, BSTR strInt, LPCTSTR pszFile=__TFILE__, IN DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_dwError=(DWORD)lr;
		_strErrorInfo=strInt;
	}

	operator DWORD() const throw()
	{
		return( m_dwError );
	}

	virtual const wchar_t* GetErrorInfo()
	{
		return _strErrorInfo._pwstr;
	}
public:
	DWORD m_dwError;
};

class CComException : public CSafException
{
public:
	CComException(HRESULT hr, LPCTSTR lpszInfo=_T(""), LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
		m_hr=hr;
		_strErrorInfo=lpszInfo;
	}

	CComException(IUnknown *pUnknown, REFIID riid, LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		SetErrorFileInfo(pszFile, dwLine);
	}

	CComException()
	{
	}
#ifdef _DEBUG
	CComException(IUnknown *pUnknown, REFIID riid, HRESULT hr, HINSTANCE hInstance=0, LPCTSTR lpszInfo=_T(""),
		LPCTSTR pszFile=__TFILE__,  DWORD dwLine=__LINE__)
	{
		TraceUnknownError(FTEXTNIL, pUnknown, riid, hr, hInstance, lpszInfo, pszFile, dwLine);
	}
#else
	CComException(IUnknown *pUnknown, REFIID riid, HRESULT hr, HINSTANCE hInstance=0, LPCTSTR lpszInfo=_T(""))
	{
		TraceUnknownError(FTEXTNIL, pUnknown, riid, hr, hInstance, lpszInfo, NULL, __LINE__);
	}
#endif

	virtual HRESULT ShowErrorBox()
	{
		CSafException::ShowErrorBox();
		return m_hr;
	}

public:

private:

};




#ifdef _DEBUG
#define TraceLastErrorInfo(hInstance, szText, ...)  {\
	fk::TraceFormatMessageEx(FTEXTNIL, hInstance, szText, GetLastError(), __VA_ARGS__); \
	fk::TraceFileNameLine(FTEXTNIL, _CRT_WIDE(__FILE__), _CRT_WIDE(__FUNCTION__), __LINE__); }
#else
#define TraceLastErrorInfo(hInstance, szText, ...)		fk::TraceFormatMessageEx(FTEXTNIL, hInstance, szText, GetLastError(), __VA_ARGS__);
#endif



#define VERIFY_COM(hr)   ShowErrorHResult(0, hr);

#ifdef _DEBUG

#define ASSERT_COM(hr)				fk::ShowErrorHResult(0, hr, _CRT_WIDE(__FUNCTION__));
#define ASSERT_COM_THROW(hr)		fk::ShowErrorHResult(0, hr, _CRT_WIDE(__FUNCTION__)); \
	if(FAILED(hr))\
	throw(hr);

#define VERIFY_COM_THROW(_hr)	if(FAILED(_hr))\
{\
	ATLASSERT(0);\
	_com_issue_error(_hr);\
}

#else	// _DEBUG
#define ASSERT_COM(hr)
#define VERIFY_COM_THROW(_hr)
#endif	// End _DEBUG


#define COM_TRY		try{

#define SAF_TRY try{


#define SAF_CATCH }catch(fk::CSafException *exception)\
{\
	exception->SetErrorFunction(__Function__);\
	exception->ShowErrorBox();\
	return exception->m_hr;\
}catch(fk::CSafException &exception)\
{\
	exception.SetErrorFunction(__Function__);\
	exception.ShowErrorBox();\
	return exception.m_hr;\
}catch (...)\
{\
}

#define SAF_CATCH_NO_RETURN }catch(fk::CSafException *exception)\
{\
	exception->SetErrorFunction(__Function__);\
	exception->ShowErrorBox();\
}catch(fk::CSafException &exception)\
{\
	exception.SetErrorFunction(__Function__);\
	exception.ShowErrorBox();\
}catch (...)\
{\
}


#define FORMAT_STRING_PARAM(x, Function, Param) x.Format(_T("%s, param: %s is illegal."), Function, L#Param);

#define ASSERT_PARAM_THROW(Function, Param)

#define SafThrowImpl(hr)
//if(S_OK != hr) \
//	throw fk::CComException(hr)

#define SafThrow	SafThrowImpl

#endif  // _FK_OLD_VER  -------------------------------------------------------------------------------------







#if !defined errors_h
#define errors_h

//const HRESULT WZD_ERROR1=MAKE_HRESULT(
//	1,				// 1=failure, 0=success
//	FACILITY_ITF, 	// COM errors (can also be FACILITY_WINDOWS for window errors, etc.)
//	0x0400			// user defined 0x400 and above
//	);

#define E_ART_WINDOW_HANDLE		MAKE_HRESULT(1, FACILITY_ITF, 0x0401)
#define E_ART_INDEX				MAKE_HRESULT(1, FACILITY_ITF, 0x0402)

//
// MessageId: E_NO_INIT
//
// MessageText:
//
//  当前对象需要调用某个函数进行初始化。
//
#define E_ART_NO_INIT			MAKE_HRESULT(1, FACILITY_ITF, 0x0403)

#endif


#ifndef __FM_TRACELOG
#pragma comment(lib, "TraceLog.lib")
#endif /* __FM_USER_TRACELOG */


_FK_END





#ifdef __cplusplus
}
#endif



#endif	// __Trace_h__

