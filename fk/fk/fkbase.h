#ifndef __FkBase_h__
#define __FkBase_h__

/*
**	Classes in this file:
**	-------------------------
**	LBaseData
**	LAutoPtrBase
**	LAutoPtr
**	LCriticalSection
**	LAutoCriticalSection
class LStringw;			
class LStringa;
*/

/*
**  Function in this file:
**	-------------------------
**	Notify function
**	UserTime function		// FUSERTIME
**  fkFree  fkMalloc
**
*/ 

#include "fk\array.h"


#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */


_FK_BEGIN

struct LModule;
class LObjects;
class LStringw;			// this .h
class LStringa;			// this .h
class LFileOperation;


#ifdef _DEBUG
enum enumNotifyBaseConvert
{
	umNotifyBaseConvertTrue=0x1,
	umNotifyBaseConvertFalse=0x2,
};

#endif

typedef struct tagNOTIFYEVENT
{
	void* pobj;
	UINT uiMsg;
	UINT uiCode;
	BOOL bNextRun;
	LRESULT lResult;

	GUID* pGuid;

#ifdef _DEBUG
	enumNotifyBaseConvert emConvert;
	wchar_t wsEventName[MAX_PATH];
#endif
}NOTIFYEVENT, *PNOTIFYEVENT;
typedef const NOTIFYEVENT* PCNOTIFYEVENT;

#ifdef _DEBUG
#define NOTIFYBASE_TRUE_FKDBG(_NotifyBase, _wsEventName)  _NotifyBase.emConvert = fk::umNotifyBaseConvertTrue; \
															_NotifyBase.wsEventName[0] = 0; \
															wcscpy(_NotifyBase.wsEventName, _CRT_WIDE(_wsEventName) );

#define NOTIFYBASE_FALSE_FKDBG(_NotifyBase, _wsEventName)  _NotifyBase.emConvert = fk::umNotifyBaseConvertFalse; \
															_NotifyBase.wsEventName[0] = 0; \
															wcscpy(_NotifyBase.wsEventName, _CRT_WIDE(_wsEventName) );

#else
#define NOTIFYBASE_TRUE_FKDBG(_NotifyBase, _wsEventName)
#define NOTIFYBASE_FALSE_FKDBG(_NotifyBase, _wsEventName)
#endif


typedef bool (__stdcall* FNTARGETPROC)(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

#define FK_BEGIN_TARGETPROC(_CLASS_NAME_)	vb_call _CLASS_NAME_::TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent)		{
#define FK_TARGETPROC_EVENT(_OBJECT_, _msg_, _func_) \
		if ((pRegObj==_OBJECT_) && uMsg==_msg_)\
		{\
			_func_;\
			return true;\
		}

#define FK_END_TARGETPROC() 	return true;		}





// pobj umsg id event
typedef void* FKNOTIFY;

enum umInstallPart
{
	umInstallPartInstall = 0x1,
	umInstallPartUninstall = 0x2
};

enum enumInstallEvent
{
	umInstallEventInstall = 0x1,
	umInstallEventUninstall = 0x2
};

typedef b_call (CALLBACK* NOTIFYCALLPROC)(FKNOTIFY hMessageMgr, fk::PNOTIFYEVENT pEvent);

#define QNPF_FNNOTIFYPROC			0x00000001
#define QNPF_CLASSPROC				0x00000002
#define QNPF_AUTODELETE				0x00000004

struct QUICKNOTIFYPROC
{
	DWORD dwSize;
	DWORD dwMask;								/* 使用 NIF_FNNOTIFYPROC NIF_CLASSPROC 宏 */

	union
	{
		FNTARGETPROC pfnTargetProc;
		LObject* pTargetProc;
	};
};


enum umNotifyItemFlag
{
	NIF_FNNOTIFYPROC	=0x00000001,		// 使用函数进行对外通报。
	NIF_CLASSPROC		=0x00000002,		// 使用类虚拟函数向外通讯事件。
	NIF_SEND_NOT_ADD	=0x00000004,		//
	NIF_SEND_NOT_REMOVE =0x00000008,		//
	NIF_PARAM			=0x00000010,

	nifActiveNotify		=0x00000020,		//打开连接。
	nifDisableNotify	=0x00000040,		//在连接状态，但是不接收信息。
};


typedef struct tagREGSVRNOTIFYITEM
{
	DWORD	dwSize;								// 命令项结构大小
	DWORD	dwMask;								// 结构项的使用项	umNotifyItemFlag
	DWORD64 dwActiveEvent;						// 活动事件项
	void*	dwRegObject;						// 注册的事件对象指针。是事件的所有者。
	DWORD	dwObjectID;							// 对象ID, 也可以是对象指针。
	union
	{
		FNTARGETPROC pfnTargetProc;				// 使用函数指针向外通讯事件。
		LObject* pTargetProc;					// 使用类虚拟函数向外通讯事件。
	};
#ifdef _DEBUG
	wchar_t sRegClass[MAX_PATH];
#endif
	LPARAM	lParam;								// 参数

}NOTIFYITEM, *LPNOTIFYITEM;
//typedef const NOTIFYITEM*  PCNOTIFYITEM;

#ifdef _DEBUG
#define FK_DEBUG_TARGETPROC_REGCLASS(__NotifyItem, sRgeClass)  wcscpy(__NotifyItem.sRegClass, sRgeClass);
#else
#define FK_DEBUG_TARGETPROC_REGCLASS(__NotifyItem, sRgeClass)	
#endif


FKNOTIFY _fncall NotifyCreate();
b_fncall NotifyDestroy(FKNOTIFY hNotify);
b_fncall NotifyAttach(FKNOTIFY hNotify);

b_fncall NotifyAdd(FKNOTIFY hNotify, LPNOTIFYITEM pNotifyItem);
b_fncall NotifyRemove(FKNOTIFY hNotify, LPNOTIFYITEM pNotifyItem);
b_fncall NotifyGetCount(FKNOTIFY hNotify, UINT* uiCount);
b_fncall NotifyFindItem(FKNOTIFY fnotify, void* pobj, fk::LPNOTIFYITEM* ppni);
data_t* _fncall NotifyGetDatas(FKNOTIFY fnotify);
v_fncall NotifyLock(FKNOTIFY fnotify);
v_fncall NotifyUnlock(FKNOTIFY fnotify);

b_fncall NotifyCall(FKNOTIFY hNotify, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
b_fncall NotifyCallEx(FKNOTIFY hNotify, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc);

b_fncall NotifyCallObject(FKNOTIFY hNotify, void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
b_fncall NotifyCallOne(NOTIFYITEM* pnpi, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);


b_fncall NotifySetState(FKNOTIFY hNotify, DWORD dwNotifyState);
dw_fncall NotifyGetState(FKNOTIFY hNotify);
b_fncall NotifyItemCopy(LPNOTIFYITEM pItemSrc, LPNOTIFYITEM pItemNew);

#define ON_NOTIFY_ADDITEM			0x00000001			// 添加一个事件对象。
#define ON_NOTIFY_REMOVEITEM		0x00000002			// 移出一个事件对象。
struct NotifysEvent
{
	fk::NOTIFYEVENT		nEvent;
	LPNOTIFYITEM			pNotifyItem;
};
b_fncall NotifySetSelf(FKNOTIFY hNotify, LPNOTIFYITEM pNotifyItem);


typedef void*  FKNOTIFYS;
FKNOTIFYS _fncall NotifysCreate();
b_fncall NotifysDelete(FKNOTIFYS hNotify);
b_fncall NotifysAttach(FKNOTIFYS hNotify);

b_fncall NotifysAdd(FKNOTIFYS hNotifys, FKNOTIFY fkNotify);
b_fncall NotifysRemove(FKNOTIFYS hNotifys, FKNOTIFY fkNotify);
b_fncall NotifysCall(FKNOTIFYS hNotifys, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
b_fncall NotifysCallEx(FKNOTIFYS hNotifys, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc);
b_fncall NotifysCallObject(FKNOTIFYS hNotifys, void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
b_fncall NotifysCallOne(NOTIFYITEM* pnpi, void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

b_fncall NotifysGetCount(FKNOTIFYS hNotifys, UINT* uiCount);
b_fncall NotifysSetState(FKNOTIFYS hNotifys, DWORD dwNotifyState);
dw_fncall NotifysGetState(FKNOTIFYS hNotify);
 



typedef void* FUSERTIME;
FUSERTIME WINAPI UserTimeStart(void);
bool WINAPI UserTimeAfreshStart(FUSERTIME hUserTime);
bool WINAPI UserTimeClose(FUSERTIME hUserTime);
bool WINAPI UserTimeStop(FUSERTIME hUserTime);
bool WINAPI UserTimeGet(FUSERTIME hUserTime, struct tm* ptm);

#define _USERTIME_MAX	21
bool WINAPI UserTimeGetText(FUSERTIME hut, LPWSTR wzText);
b_fncall UserTimeGetStopText(FUSERTIME hut, LPWSTR szText);



/*--------------------------------------------------------------------------*/
void WINAPI fkFree(void *memblock); 
void* WINAPI fkMalloc(size_t size);



// 这些函数先定义在这里。
typedef DWORD  FILE_INFO_MASK;
enum enumFileBaseInfo
{
	umFileBaseInfoFullName			=0x00000001,
	umFileBaseInfoFileName			=0x00000002,
	umFileBaseInfoCreationTime		=0x00000004,
	umFileBaseInfoLastAccessTime	=0x00000008,
	umFileBaseInfoLastWriteTime		=0x00000010,
	umFileBaseInfoFileSize			=0x00000020,

	umFileBaseInfoImageSize			=0x00001000,				// 目前这两个项，不支持。
	umFileBaseInfoImageColor		=0x00002000,
};

bool CALLBACK GetFileBaseInfo(LPCWSTR pszFileName, LPWSTR szSeparate, LPWSTR szOutInfo, FILE_INFO_MASK dwFileInfoMask);
int WINAPI FileFormatUnit(LPWSTR lpszStr, double nNum);

b_call HintDeleteFile(LPCWSTR lpszFull, HWND hwnd, bool bHint);


/*--------------------------------------------------------------------------*/
// 判断调用DLL的进程是否是Regsvr32.exe文件。
BOOL WINAPI IsRegsvr32Exe(HINSTANCE hInst);
typedef HRESULT (STDAPICALLTYPE* LPFN_DLL_REGISTERSERVER)(void);
HRESULT WINAPI DLL_RegisterServerW(LPCWSTR lpszDLLFile);
HRESULT WINAPI DLL_RegisterServerA(LPCSTR lpszDLLFile);
#ifdef UNICODE
#define DLL_RegisterServer		DLL_RegisterServerW
#else
#define DLL_RegisterServer		DLL_RegisterServerA
#endif

typedef HRESULT (STDAPICALLTYPE* FNCoCreateInstance)(REFCLSID, LPUNKNOWN, DWORD, REFIID, LPVOID);
void _fncall SetCoCreateInstancePtr(FNCoCreateInstance pCoCreateInstance);

/*
** 功能：返回应用程序的的信息 tag_ApplicationInfo
*/

enum enumApplicationInfoMask
{
	AIF_NULL = 0x00000000,
	AIF_FLASHWINDOW_SHOW = 0x00000001,
	AIF_CREATE_CONFIGFILE_APPLICATION = 0x00000002,	// 程序创建pConfigFileUser对象。
	AIF_CREATE_CONFIGFILE_USER = 0x00000004,		// 程序创建pConfigFileUser对象。
	AIF_CREATE_PROCESS_MACRO = 0x00000008,			// 程序创建pProcessMacro对象。
	AIF_ANZHUANGBAN = 0x00000020,					// 存在这个标记是安装版版本。
};

class _FK_OUT_CLASS LApplicationInfo
{
private:
protected:
public:
	DWORD				dwSize;
	DWORD				dwMask;
	LPWSTR				szSoftwareName;			// 软件名称
	LPWSTR				szLanguage;
	FTEXT				fText;					// 日志句柄
	LONG				nFlashWindowTimer;		// Flash Windows显示多少时间后，自动关闭.
	DWORD				dwFlashWindowStyle;		// 
	IConfigFile			*pConfigFileApp;		// 应用程序配置文件操作对象			以后移除
	IConfigFile			*pConfigFileUser;		// 应用程序用户保存的信息操作对象	以后移除
	IProcessMacro		*pProcessMacro;			// 应用的宏记忆操作对象				以后移除
	void				*pUserLParam;

	LApplicationInfo(void);
	virtual ~LApplicationInfo(void);
};

fk::LApplicationInfo* WINAPI ApplicationFindInfo();
fk::LModule* _fncall GetMainModule(void);
HINSTANCE _fncall GetMainModuleInst(void);
HWND _fncall GetMainWnd(void);
b_fncall SetMainWnd(HWND hMainWnd);				// 代码在 App.cxx 文件中。

#define IAF_SOFTWARE_NAME		0x00000001
#define IAF_USER_CONFIG			0x00000002
#define IAF_APP_CONCIFG			0x00000004
#define IAF_TEST_CONFIG			0x00000008
#define IAF_LPARAM				0x00000010
typedef struct tagINITAPPLICATIONW
{
	DWORD dwSize;
	DWORD dwMask;
	HMODULE hMainModule;
	DWORD dwAppStyle;
	LPWSTR wsSoftwareName;			// 软件名称
	LPWSTR wsUserConfig;
	LPWSTR wsAppConfig;
	LPWSTR wsTestConfig;

	LPWSTR wsLogFile;
	DWORD dwLogStyle;
	LPARAM lParam;
	DWORD dwErrorMake;
	fk::LModule*  pMainModule;
}INITAPPLICATIONW;

typedef INITAPPLICATIONW* PINITAPPLICATIONW;
typedef const INITAPPLICATIONW* PCINITAPPLICATIONW;

b_fncall ApplicationInitW(PINITAPPLICATIONW pInitApp);
b_fncall ApplicationUninit();
#ifndef UNICODE
#define ApplicationInit	ApplicationInitW
#else 
#define ApplicationInit	ApplicationInitW
#endif 

b_fncall GetDebugState();
VOID WINAPI safSetApplicationInfoUserLParam(void* pUserLParam );


typedef void (_fncall* LPFNUpdateMainFrameProc)();
void _fncall SetUpdateMainFrameFunction(LPFNUpdateMainFrameProc lpfnUpdateMainFrameProc);
void _fncall UpdateMainFrameUI();
LPWSTR WINAPI ConfigGetFileFromInputW(const wchar_t* szFileName, wchar_t* szFullName);


enum enumPathType
{
	umPathTypeError	=0x0,
	umPathTypeRes	=0x1,
	umPathExe		=0x2,
	umPathTest		=0x3,
	umPathTemp		=0x4,
	umPathConfig	=0x5,
	umPathHistory	=0x7,
	umPathDonwload	=0x8,
	umPathTheme		=0x9,
};
b_call PathGetHistoryFile(__in fk::LStringw* pHistoryFile, __out fk::LStringw*  pHistoryFileN);
b_fncall PathBuildFull(LPCWSTR lpszFolder, fk::LStringw* psOutPath);							// 设置文件夹路径 "Folder"，输出文件夹路径："e:\xx\Folder"

b_fncall PathGetModulePathA(__in fk::LModule* pmodule, __in enumPathType umPathType, LPSTR lpszOutPath);  // lpszPath = MAX_PATH.
b_fncall PathGetModulePathW(__in fk::LModule* pmodule, __in enumPathType umPathType, LPWSTR lpszOutPath);  // lpszPath = MAX_PATH.
#ifdef UNICODE
#define PathGetModulePath	PathGetModulePathA
#else
#define PathGetModulePath	PathGetModulePathW
#endif

int WINAPI GetFrameworkPathA(LPSTR lpszPath, DWORD nBufferSize);
int WINAPI GetFrameworkPathW(LPWSTR lpszPath, DWORD nBufferSize);
#ifdef _UNICODE
#define GetFrameworkPath GetFrameworkPathW
#else
#define GetFrameworkPath GetFrameworkPathA
#endif

enum enumXmlLocalShinePath
{
	XMLLSP_CONFIG			= 0x00000001,	// 同时映射配置路径。
	XMLLSP_RES_PATH			= 0x00000002,	// 映射资源路径。
	XMLLSP_RUNL_FULL_PATH	= 0x00000004,	// RUNL 路径是绝对路径，这个掩码 xmlfLocalAddShineProject 会自己计算。
};
b_call xmlfLocalAddShineProject(LPCWSTR lpszProjectPath, LPCWSTR lpszProjectName, DWORD dwXMLLSPMake);

int WINAPI PathGetComFrameworkA(__out LPSTR lpszFullPath, __in DWORD nSize);
int WINAPI PathGetComFrameworkW(__out LPWSTR lpszFullPath, __in DWORD nSize);
#ifdef UNICODE
#define PathGetComFramework		PathGetComFrameworkW
#else
#define PathGetComFramework		PathGetComFrameworkA
#endif

BOOL WINAPI PathFileExistsBoxA(HWND hWndCtrl, LPCSTR lpszFile);
BOOL WINAPI PathFileExistsBoxW(HWND hWndCtrl, LPCWSTR lpszFile);
#ifdef _UNICODE
#define  PathFileExistsBox PathFileExistsBoxW
#else
#define  PathFileExistsBox PathFileExistsBoxA
#endif

b_fncall PathIsWebFilesW(LPCWSTR lpszPath);
#ifdef _UNICODE
#define  PathIsWebFiles PathIsWebFilesW
#else
#define  PathIsWebFiles PathIsWebFilesA
#endif

b_fncall PathIsRootW(LPCWSTR lpszPath);
b_fncall PathIsRootA(LPCSTR lpszPath);
#ifdef _UNICODE
#define  PathIsRoot PathIsRootW
#else
#define  PathIsRoot PathIsRootA
#endif

b_fncall PathReplaceWebFilesW(LPWSTR lpszPathInOut256);
#ifdef _UNICODE
#define  PathReplaceWebFiles PathReplaceWebFilesW
#else
#define  PathReplaceWebFiles PathReplaceWebFilesA
#endif

b_fncall PathReplaceWebFilesCopyW(LPCWSTR lpszPathIn, LPWSTR lpszPathOut256);
#ifdef _UNICODE
#define  PathReplaceWebFilesCopy PathReplaceWebFilesCopyW
#else
#define  PathReplaceWebFilesCopy PathReplaceWebFilesCopyA
#endif


int WINAPI MessageBox2W(HWND hWnd, HINSTANCE hInstance, LPCWSTR lpszInfo, UINT uType, ...);
int WINAPI MessageBox2A(HWND hWnd, HINSTANCE hInstance, LPCSTR lpszInfo, UINT uType, ...);
#ifdef _UNICODE
#define fk_MessageBox2 fk::MessageBox2W
#else
#define fk_MessageBox2 fk::MessageBox2A
#endif

#ifdef _UNICODE
#define MessageBox2 MessageBox2W
#else
#define MessageBox2 MessageBox2A
#endif

void WINAPI RunCheckVersion(bool bAutoClose, LPCWSTR wsUpdateFile);
void WINAPI RunCheckVersionEx(HINSTANCE hModule, bool bAutoClose, LPCWSTR wsUpdateFile);
b_fncall IsFirstRun(LPCWSTR lpszRegSubKey, LPCWSTR lpszKeyName);

//--------------------------------------------------------------------------
// *gdi

enum enumUIFontType
{
	enumUIFontTypeWindowText=1,
};

HGDIOBJ WINAPI fk_GetStockObject(int fnObject);
HFONT WINAPI safGetFont(__in DWORD dwType);
bool WINAPI WindowModifyFont(HWND hWndCtrl);
COLORREF _fncall MakeGrayAlphablend(HBITMAP hBitmap, int weighting, COLORREF blendcolor);
HICON WINAPI ResGetFileIcon(LPCWSTR szResName);


b_call CheckIntRange(int nVal, int nMin, int nMax, bool bErrorHint);
b_call CheckLongRange(long nVal, long nMin, long nMax, bool bErrorHint);

class _FK_OUT_CLASS LBase		// 这个类以后放奇
{
protected:
	DWORD _dwBaseMakeObject;
	LONG _dwBaseRef;

public:
	LBase();
	virtual ~LBase();

	ULONG AddRefBase(void);
	ULONG ReleaseBase(void);
};


/*
	new api.
*/
enum enumDataType
{
	dtLong				=1,
	dtULong				=2,
	dtFloat				=3,
	dtDouble			=4,
	dtInt				=5,
	dtUInt				=6,
	dtChar				=7,
	dtCharBuf			=8,
	dtWChar				=11,
	dtWCharBuf			=12,
	dtBool				=13,
	dtStringw			=14,				// fk::LStringw
	dtStringa			=15,
	dtBaseData			=16,
	dtGuidPtr			=17,
	//dtGuidWBuf	=17,
	//dtGuidABuf	=18,
	dtData				=18,
	dtRect				=19,
	dtResID				=20,				// _uint
	dtVariantBool		=21,
	dtWinSystemTime		=22,
	dtRegsvrDataType	=50,
	dtEmpty				=0x00020000,
	dtHex				=0x00010000,
	dtRes				=0x00030000,
};


#define FK_DATA_TYPE_LO(dwValue)	(dwValue & 0x0000FFFF)
#define FK_DATA_TYPE_HI(dwValue)	(dwValue & 0xFFFF0000)



enum umBoolType
{
	umBoolType_t_f			=0x1,
	umBoolType_T_F			=0x2,
	umBoolType_True_False	=0x3,
	umBoolType_Lang			=0x4,
};

//enum enumDataType 
//{
//	ddtBoolText = 0x1,
//	ddtHexText	= 0x2,
//	ddtIntText	= 0x3,
//	ddtText		= 0x4,
//};

class _FK_OUT_CLASS LBaseData// : public LBase
{
public:
	UINT		_uiVT;
	LField*		_pField;

	union
	{
		//     LONG lVal;
		ULONG	_ulong;
		ULONG	_long;
		INT		_int;
		UINT	_uint;
		//     BYTE bVal;
		//     SHORT iVal;
		float  _float;
		//     DOUBLE dblVal;
		VARIANT_BOOL _VariantBool;
		//     USHORT usVal;
		wchar_t*	_pwstr;
		char*		_pstr;
		bool		_bool;
		double		_double;
		//     LONG* plVal;
		//     ULONG* pulVal;
		//     INT* pintVal;
		//     UINT* puintVal;
		//     BYTE* pbVal;
		//     SHORT* piVal;
		//     FLOAT* pfltVal;
		//     DOUBLE* pdblVal;
		//     VARIANT_BOOL* pboolVal;
		//     USHORT* pusVal;

		RECT*			_prect;
		IID				_iid;
		LStringw*		_pStringw;
		LStringa*		_pStringa;
		SYSTEMTIME*		_pSystemTime;
		data_t*			_pdata;
		GUID*			_pguid;
	};

	//_FK_DBGCLASSINFO_

	LBaseData();
	virtual ~LBaseData();

	virtual b_call AssignBaseData(LBaseData* pbaseData);   /* = 0; */

	virtual l_call GetLong();   /* = 0; */
	virtual b_call SetLong(long lValue, int radix);   /* = 0; */

	virtual ul_call GetULong();   /* = 0; */
	virtual b_call SetULong(ULONG ulValue, int radix);   /* = 0; */

	virtual i_call GetInt();   /* = 0; */
	virtual b_call SetInt(int iValue, int radix);   /* = 0; */

	virtual ui_call GetUInt();   /* = 0; */
	virtual b_call SetUInt(UINT uiValue, int radix);   /* = 0; */

	virtual be_call GetByte();   /* = 0; */
	virtual b_call  SetByte(BYTE byte, int radix);   /* = 0; */

	virtual f_call GetFloat();   /* = 0; */
	virtual b_call SetFloat(float fValue);   /* = 0; */

	virtual d_call GetDouble();   /* = 0; */
	virtual b_call SetDouble(double dValue);   /* = 0; */	

	virtual b3_call GetVariantBool();   /* = 0; */
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);   /* = 0; */

	virtual b_call GetBool();   /* = 0; */
	virtual b_call SetBoolStr(bool bValue, umBoolType boolType);   /* = 0; */
	virtual b_call SetBool(bool bValue);

	virtual us_call GetUShort();   /* = 0; */
	virtual b_call  SetUShort(USHORT usValue, int radix);   /* = 0; */

	virtual b_call SetGuid(GUID* pguid);
	virtual b_call SetGuid(LPCWSTR lpszGuid);
	virtual b_call SetGuid(LPCSTR lpszGuid);
	virtual GUID* _call GetGuid(void);
	virtual b_call GetGuidText(fk::LStringw* pStringw);
	virtual b_call GetGuidText(fk::LStringa* pStringa);

	virtual b_call SetSystemTime(LPSYSTEMTIME pSystemTime);
	virtual b_call SetRect(LPCRECT lpcRect);
	virtual b_call SetResValue(UINT uiResID);

	virtual b_call GetStringw(LStringw* ptext);   /* = 0; */
	virtual b_call AssignStringw(LStringw* ptext);   /* = 0; */
	virtual b_call GetBufw(wchar_t* pstr, UINT uiBuf);   /* = 0; */

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1, int iBufPlusCount=0); /* = 0; */
	virtual b_call Assigna(const char* pstr, int uiLen=-1, int iBufPlusCount=0); /* = 0; */
	virtual v_call Clear();	/* = 0; */
	virtual v_call Free();

	virtual b_call SetDataType(UINT uiType);
	virtual b_call SetData(UINT uiType, data_t* pdata);

	virtual LStringw* _call GetDisplayName();
	virtual b_call GetDisplayName(wchar_t* pstr, UINT uiBuf);
	virtual b_call SetDisplayName(const wchar_t* pstr, UINT uiBuf);

	virtual b_call IsEmpty();
	virtual b_call IsExist();

	virtual b_call Compares(LBaseData* pBaseData);

	b_call IsCharType();
	b_call IsWcharType();
	b_call IsNumber();
};

#define fkwstr(__pStrType)		__pStrType._pwstr
#define fkpwstr(__pStrType)		__pStrType->_pwstr

#define fkcstr(__pStrType)		__pStrType._pstr
#define fkpcstr(__pStrType)		__pStrType->_pstr

#ifdef _UNICODE
#define fkstr(__pStrType)		__pStrType._pwstr
#define fkpstr(__pStrType)		__pStrType->_pwstr
#else
#define fkstr(__pStrType)		__pStrType._pstr
#define fkpstr(__pStrType)		__pStrType->_pstr
#endif

#define __ascii_towlower(c)     ( (((c) >= L'A') && ((c) <= L'Z')) ? ((c) - L'A' + L'a') : (c) )
#define __ascii_towupper(c)     ( (((c) >= L'a') && ((c) <= L'z')) ? ((c) - L'a' + L'A') : (c) )

enum umStringMask
{
	umStringAuto	=0x1,	
	umStringBuffer	=0x2,	
};

class _FK_OUT_CLASS LStringw  : public LBaseData
{
private:
	int		_iLen;											// 这个长度还不太准确。
	DWORD	_dwMask;

	b_call Adjust(UINT uiNewLength);

public:
	int		_iBufSize;

	LStringw();
	LStringw(int iBufSize);	 //, DWORD dwMask);				// 默认	umStringBuffer, dwMask = umStringMask item. 
	LStringw(LPCWSTR wcsText);
	LStringw(const char* sText);
	LStringw(fk::LStringw* pwsText);
	LStringw(fk::umStringMask stringMask, int iBufSize);
	LStringw(fk::umStringMask stringMask, LPCWSTR lpszText, int iBufSize=-1);
	virtual ~LStringw();

	/*
	** LBaseData
	*/
	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual b_call GetBool();
	virtual b_call SetBoolStr(bool bValue, umBoolType boolType);
	virtual b_call SetBool(bool bValue);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call SetGuid(GUID* pguid);
	virtual b_call SetGuid(LPCWSTR lpszGuid);
	virtual b_call SetGuid(LPCSTR lpszGuid);
	virtual GUID* _call GetGuid(void);
	virtual b_call GetGuidText(fk::LStringw* pStringw);
	virtual b_call GetGuidText(fk::LStringa* pStringa);

	virtual b_call AssignBaseData(fk::LBaseData* pbaseData);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1, int iBufPlusCount=0);
	virtual b_call Assigna(const char* pstr, int uiLen=-1, int iBufPlusCount=0);
	virtual b_call Assigna(const fk::LStringa* pctext,int uiLen=-1, int iBufPlusCount=0);

	virtual b_call Assign(const wchar_t* pwstr, int uiLen=-1, int iBufPlusCount=0);
	virtual b_call Assign(const fk::LStringw* pwtext,int uiLen=-1, int iBufPlusCount=0);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual fk::LStringw* _call GetDisplayName();
	virtual b_call GetDisplayName(wchar_t* pwstr, UINT uiBuf);
	virtual b_call SetDisplayName(const wchar_t* pwstr, UINT uiBuf);

	virtual b_call IsEmpty();
	virtual b_call IsExist();

	virtual b_call Compares(fk::LBaseData* pBaseData);

	virtual v_call Clear();
	virtual v_call Free();

	wp_call MallocAuto(int iLen);
	wp_call MallocBuffer(int iLen);

	i_call GetLength();
	b_call IfLengthMax(int iMaxLen);
	//wp_call GetBuffer(UINT uiBuf);

	i_call LoadStr(HINSTANCE hInst, UINT uID);
	i_call LoadFormat(HINSTANCE hInst, UINT uID, ...);
	i_call LoadFormatArgList(HINSTANCE hInstance, LPCWSTR szRes, va_list argList);
	i_call FormatArgList(HINSTANCE hInstance, LPCWSTR szRes, va_list argList);
	i_call Format(LPCWSTR szRes, ...);
	i_call FormatL(LPCWSTR szRes, int nBufferMax, ...);

	fk::LStringw& _call AppendFormat(HINSTANCE hInst, LPCWSTR szRes, ...);
	fk::LStringw& _call AppendLoadStr(HINSTANCE hInst, UINT uID);

	b_call ToMbs(char* strOut, int iBufSize);

	fk::LStringw& Appendw(const wchar_t* pwstr, int uiLen=-1);
	fk::LStringw& Appenda(const char* pstr, int uiLen=-1);
	fk::LStringw& Append(const wchar_t* pwstr, int uiLen=-1);

	b_call Delete(int iPos, int iLen);
	i_call StrICmp(const wchar_t* pwstr);
	i_call StrCmp(const wchar_t* pwstr);
	fk::LStringw& _call TrimRL(void);
	fk::LStringw& _call TrimR(void);
	fk::LStringw& _call TrimL(void);
	i_call Insert(int iIndex, const wchar_t* pwstr, int iLen=-1);
	v_call ToLower(void);
	v_call ToUpper(void);

	wp_call FindLower(void);
	wp_call FindUpper(void);

	int FindLastOf(wchar_t* szStr, int iOff);
	int FindI(LPCWSTR szFind, int iOff);
	int Find(LPCWSTR szFind, int iOff);

	b_call Substr(int iIndex, int iCount, fk::LStringw* pw);
	b_call Replace(LPCWSTR lpszOld, LPCWSTR lpszNew);
	b_call remove(LPCWSTR lpszDelString, int iRemoveCount, int iMaxFreeBuf);

// new fu
	b_call IfEndAddRL(LPCWSTR szIfEnd, LPCWSTR szLeft, LPCWSTR szRight);
	b_call AppendTextCount(LPCWSTR lpszText, UINT uiCount);
	i_call FindEnd(LPCWSTR lpszText);
	i_call FindEndI(LPCWSTR lpszText);

	wp_call c_str();

	// operator
	fk::LStringw& operator=(const fk::LStringw& wsSrc)
	{
		Assignw(wsSrc._pwstr, -1);
		return( *this );
	}
	fk::LStringw& operator=(const wchar_t pwsSrc)
	{
		Assignw(&pwsSrc, 1);
		return( *this );
	}
	fk::LStringw& operator=(const wchar_t* pwsSrc)
	{
		Assignw(pwsSrc, -1);
		return( *this );
	}
	fk::LStringw& operator=(const fk::LStringw* pwsSrc)
	{
		Assignw(pwsSrc->_pwstr, -1);
		return( *this );
	}


	fk::LStringw& operator+=(wchar_t* pszsrc)
	{
		Appendw(pszsrc, -1);
		return (*this);
	}
	fk::LStringw& operator +=(const wchar_t* pszsrc)
	{
		Appendw(pszsrc, -1);
		return (*this);
	}
	fk::LStringw& operator+=(const fk::LStringw& pszsrc)
	{
		Append(pszsrc._pwstr, -1);
		return (*this);
	}
	fk::LStringw& operator+=(const fk::LStringw* pszsrc)
	{
		Appendw(pszsrc->_pwstr, -1);
		return (*this);
	}
	fk::LStringw& operator+=(fk::LStringw* pszsrc)
	{
		Appendw(pszsrc->_pwstr, -1);
		return (*this);
	}
	fk::LStringw& operator+=(fk::LStringw& pszsrc)
	{
		Appendw(pszsrc._pwstr, -1);
		return (*this);
	}


	fk::LStringw& operator +(const wchar_t* pszsrc)
	{
		Appendw(pszsrc, -1);
		return (*this);
	}
	fk::LStringw& operator+(const fk::LStringw* pszsrc)
	{
		Appendw(pszsrc->_pwstr, -1);
		return (*this);
	}
	fk::LStringw& operator+(const fk::LStringw& pszsrc)
	{
		Appendw(pszsrc._pwstr, -1);
		return (*this);
	}

	operator const wchar_t*()	{	return _pwstr;	}
	operator	   wchar_t*()	{	return _pwstr;	}

	wchar_t operator[](int iChar)  {	return _pwstr[iChar];	}

	bool operator == (const wchar_t* lpIn)
	{
		if (StrCmp(lpIn)==0)
			return true;

		return false;
	}
};

class _FK_OUT_CLASS LStringa  : public fk::LBaseData
{
private:
	int		_iLen;
	DWORD	_dwMask;
	int		_iBufSize;

	b_call Adjust(UINT uiNewLength);

public:

	LStringa();
	LStringa(int iBufSize);
	LStringa(LPCWSTR wcsText);
	LStringa(LPCSTR csText);
	LStringa(fk::LStringa* pwsText);
	LStringa(umStringMask stringMask, int iBufSize);

	virtual ~LStringa();

	/*
	** LBaseData
	*/

	i_call GetBufSize()		{ return _iBufSize; }

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual b_call GetBool();
	virtual b_call SetBoolStr(bool bValue, umBoolType boolType);
	virtual b_call SetBool(bool bValue);


	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call AssignBaseData(fk::LBaseData* pbaseData);
	virtual b_call AssignStringw(LStringa* ptext);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual b_call Assign(const char* pstr, int uiLen=-1);

	virtual b_call GetStringw(LStringa* ptext);
	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual fk::LStringw* _call GetDisplayName();
	virtual b_call GetDisplayName(char* pwstr, UINT uiBuf);
	virtual b_call SetDisplayName(const char* pwstr, UINT uiBuf);

	virtual b_call IsEmpty();
	virtual b_call IsExist();

	virtual b_call Compares(fk::LBaseData* pBaseData);

	virtual v_call Clear();
	virtual v_call Free();

	cp_call MallocAuto(int iLen);
	cp_call MallocBuffer(int iLen);

	i_call GetLength();
	b_call IfLengthMax(int iMaxLen);
	//cp_call GetBuffer(UINT uiBuf);

	i_call LoadStr(HINSTANCE hInst, UINT uID);
	i_call LoadFormat(HINSTANCE hInst, UINT uID, ...);
	i_call LoadFormatArgList(HINSTANCE hInstance, LPCSTR szRes, va_list argList);
	i_call FormatArgList(HINSTANCE hInstance, LPCSTR szRes, va_list argList);
	i_call Format(LPCSTR szRes, ...);
	i_call FormatL(LPCSTR szRes, int nBufferMax, ...);
	b_call ToMbs(char* strOut, int iBufSize);

	fk::LStringa& _call AppendFormat(HINSTANCE hInstance, LPCSTR szRes, ...);
	fk::LStringa& _call AppendLoadStr(HINSTANCE hInst, UINT uID);

	fk::LStringa& Appendw(const wchar_t* pwstr, int uiLen=-1);
	fk::LStringa& Appenda(const char* pstr, int uiLen=-1);
	fk::LStringa& Append(const char* pstr, int uiLen=-1);

	b_call Delete(int iPos, int iLen);
	i_call StrICmp(const char* pstr);
	i_call StrCmp(const char* pstr);
	b_call TrimRL(void);
	b_call TrimR(void);
	b_call TrimL(void);
	i_call Insert(int iIndex, const char* pstr, int iLen);
	v_call ToLower(void);
	v_call ToUpper(void);

	cp_call FindLower(void);
	cp_call FindUpper(void);

	int FindLastOf(LPCSTR szStr, int iOff);
	int FindI(LPCSTR szFind, int iOff);
	int Find(LPCSTR szFind, int iOff);

	b_call Substr(int iIndex, int iCount, fk::LStringa* pw);
	b_call Replace(LPCSTR lpszOld, LPCSTR lpszNew);
	b_call remove(LPCSTR lpszDelString, int iRemoveCount, int iMaxFreeBuf);

	b_call IfEndAddRL(LPCSTR szStr, LPCSTR szLeft, LPCSTR szRight);
	b_call AppendTextCount(LPCSTR lpszText, UINT uiCount);

	cp_call c_str();

	fk::LStringa& operator=(const fk::LStringa& wsSrc)
	{
		Assigna(wsSrc._pstr, -1);
		return( *this );
	}
	fk::LStringa& operator=(const char pwsSrc)
	{
		Assigna(&pwsSrc, 1);
		return( *this );
	}
	fk::LStringa& operator=(const char* pwsSrc)
	{
		Assigna(pwsSrc, -1);
		return( *this );
	}
	fk::LStringa& operator=(unsigned char* pwsSrc)
	{
		Assigna((char*)pwsSrc, -1);
		return( *this );
	}
	fk::LStringa& operator=(const fk::LStringa* pwsSrc)
	{
		Assigna(pwsSrc->_pstr, -1);
		return( *this );
	}
	fk::LStringa& operator=(const fk::LStringw& pszsrc)
	{
		Assignw(pszsrc._pwstr, -1);
		return (*this);
	}
	fk::LStringa& operator=(const wchar_t* lpszText)
	{
		Assignw(lpszText, -1);
		return (*this);
	}


	fk::LStringa& operator+=(char* pszsrc)
	{
		Assigna(pszsrc, -1);
		return (*this);
	}
	fk::LStringa& operator +=(const char* pszsrc)
	{
		Assigna(pszsrc, -1);
		return (*this);
	}
	fk::LStringa& operator +=(const wchar_t* pszsrc)
	{
		Assignw(pszsrc, -1);
		return (*this);
	}
	fk::LStringa& operator+=(const fk::LStringa& pszsrc)
	{
		Append(pszsrc._pstr, -1);
		return (*this);
	}
	fk::LStringa& operator+=(const fk::LStringa* pszsrc)
	{
		Appenda(pszsrc->_pstr, -1);
		return (*this);
	}
	fk::LStringa& operator+=(fk::LStringw* pszsrc)
	{
		Appendw(pszsrc->_pwstr, -1);
		return (*this);
	}


	fk::LStringa& operator +(const char* pszsrc)
	{
		Appenda(pszsrc, -1);
		return (*this);
	}
	fk::LStringa& operator+(const fk::LStringa* pszsrc)
	{
		Appenda(pszsrc->_pstr, -1);
		return (*this);
	}
	fk::LStringa& operator+(const fk::LStringa& pszsrc)
	{
		Appenda(pszsrc._pstr, -1);
		return (*this);
	}
	fk::LStringa& operator+(const fk::LStringw& pszsrc)
	{
		Appendw(pszsrc._pwstr, -1);
		return (*this);
	}

	operator const char*()	{	return _pstr;	}
	operator	   char*()	{	return _pstr;	}

	char operator[](int iChar) const {	return _pstr[iChar];	}

	bool operator == (const char* lpIn)
	{
		if (StrCmp(lpIn)==0)
			return true;
		return false;
	}
};

i_fncall fkGetWindowTextW(HWND hWnd, fk::LStringw* pstr);
i_fncall fkGetWindowTextA(HWND hWnd, fk::LStringa* pstr);
#ifdef UNICODE
#define fkGetWindowText		fk::fkGetWindowTextW
#else
#define fkGetWindowText		fk::fkGetWindowTextA
#endif

// 文字长度不能小于 %d 大于 %d。
bool WINAPI StrCheckLenA(HWND hwndHint, char* pstr, int iMinLen, int iMaxLen);
b_call StrExistA(LPCSTR lpszText);						//字符串是空。 没有字符，或者是 NULL。
b_call StrExistW(LPCWSTR lpszText);

b_fncall StrCheckShortNameW(LPCWSTR lpszName);			// 只能有  A-Z  a-z  _ 0-9

b_fncall IsDigitW(LPCWSTR lpcszDigit);
b_fncall IsDigitA(LPCSTR lpcszDigit);
#ifdef UNICODE
#define  IsDigit IsDigitW
#else
#define  IsDigit IsDigitA
#endif


#define FK_IN_TRUE		true
#define FK_IN_FALSE		false
#define FK_OUT_TRUE		true
#define FK_OUT_FALSE	false

class LAutoBool
{
public:
	bool*	_pbValue;
	bool	_bInValue;
	bool	_bOutValue;

	LAutoBool()
	{
	}

	LAutoBool(bool* pbValue, bool bInValue, bool bOutValue)
	{
		_pbValue	= pbValue;
		*_pbValue	= bInValue;
		_bInValue	= bInValue;
		_bOutValue	= bOutValue;
	}

	~LAutoBool()
	{
		*_pbValue = _bOutValue;
	}

	v_call SetAutoBool(bool* pbValue, bool bInValue, bool bOutValue)
	{
		_pbValue	= pbValue;
		*_pbValue	= bInValue;
		_bInValue	= bInValue;
		_bOutValue	= bOutValue;
	}
};

class LEnumData : public IUnknown
{
public:
	LEnumData(void);
	virtual ~LEnumData(void);

	virtual b_call Next(ULONG celt, data_t** rgelt, ULONG *pceltFetched) = 0;
	virtual b_call Skip(ULONG celt) = 0;
	virtual b_call Reset(void) = 0;
	virtual b_call Clone(LEnumData** ppenum) = 0;
};
// {9FFDAC1B-BBA4-4faa-9DA6-A3CE8381A1EB}
extern IID IID_IEnumData;



/*--------------------------------------------------------------------------*/
enum enumLanguageType
{
	umLanguageTypeSoftware=0x1,
	umLanguageTypeApp=0x2,
};

//
//	函数名称：LanguageLoadFile
//	功能：根据hInstance参数返回语言DLL的句柄。
//	参数：
//	返回值：返回语言DLL的句柄。
HINSTANCE WINAPI LanguageLoadFile(HINSTANCE hInstance);
b_fncall LanguageTypeSet(enumLanguageType lt, HINSTANCE hInst);
b_fncall LanguageTypeSetFileW(enumLanguageType lt, LPCWSTR pszFile);
b_fncall LanguageTypeSetFileA(enumLanguageType lt, LPCSTR pszFile);
#ifdef _UNICODE
#define LanguageTypeSetFile		LanguageTypeSetFileW
#else
#define LanguageTypeSetFile		LanguageTypeSetFileA
#endif

#define LIBXML_DEFAULT_LANG_PATH		"/config/Setting/DefaultLang"
#define DEFAULT_LANGUAGE				L"zh-CN"

enum umLoadLanguage
{
	umLoadLanguageInstall=0x1,			/* 开始调用语言过程安装语言。 */
	umLoadLanguageUninstall=0x2,		/* 开始调用语言过程卸载语言。 */
};
typedef bool (_fncall* LOADLANGUAGEPROC)(fk::umLoadLanguage loadLanguage);
/*
** 开始执行载入语言过程，此方法会调用每一个注册的语言过程。
*/
b_fncall LoadLanguage(fk::umLoadLanguage loadLanguage);
b_fncall IsRunLoadLanguage(void);	


/*
** 注册一个语言过程。  卸载一个语言过程。
*/
bool  WINAPI LanguageRegsvrLoad(fk::LOADLANGUAGEPROC lpfnLoadLanguageProc);
bool  WINAPI LanguageUnregsvrLoad(fk::LOADLANGUAGEPROC lpfnLoadLanguageProc);

//	功能：返回当前语言的绝对路径。
//	参数：
//		lpszFullPath - MAX_PATH大小的字符串空间。
//	返回值：返回路径长度，错误返回-1。
//
int WINAPI LanagerGetCurrentPath(LPWSTR lpszFullPath);



#define FMSL_LID				0x0001
#define FMSL_LOCALE				0x0002
#define FMSL_ISO_LOCALE			0x0004
#define FMSL_HTML_CHARSET		0x0008
#define FMSL_ALL				(FMSL_LID|FMSL_LOCALE|FMSL_ISO_LOCALE|FMSL_HTML_CHARSET)

typedef struct tagSLanguage {
	DWORD			dwMask;
	LANGID			lid;
	fk::LStringw	pszLocale;
	BOOL			bSupported;
	fk::LStringw	pszISOLocale;		//_T("zh-CN"),
	UINT			uCodepage;
	fk::LStringw	pszHtmlCharset;
}SLanguage;

bool  WINAPI CBLoadSupportLanguageW(HWND hwndComboBox, LPCWSTR lpPath);
#ifdef _UNICODE
#define FmCBLoadSupportLanguage	CBLoadSupportLanguageW
#else
#define FmCBLoadSupportLanguage	CBLoadSupportLanguageW
#endif

/* 根据某个字段项查找，返回它的全部成员，字段类型 dwMask 指定。*/
bool  WINAPI FmFindLanguage(SLanguage* pLanguageInOut);

// 语言格式名称是：_T("zh-CN"),
bool  WINAPI CBSetCurrentLangW(HWND hwndComboBox, LPCWSTR lpCurrentLang);
#ifdef _UNICODE
#define FmCBSetCurrentLang	CBSetCurrentLangW
#else
#define FmCBSetCurrentLang  CBSetCurrentLangW
#endif

// 语言格式名称是：_T("zh-CN"),
bool  WINAPI FmCBGetCurrentLangW(HWND hwndComboBox, LPWSTR lpCurrentLang);
#ifdef _UNICODE
#define FmCBGetCurrentLang	FmCBGetCurrentLangW
#else
#define FmCBGetCurrentLang  FmCBGetCurrentLangW
#endif

// 语言格式名称是：_T("zh-CN"),
bool WINAPI CBSaveLangSetting(HWND hwndComboBox);



#if  (_FK_OLD_VER<=0x0001)

struct fkCommandRange
{
	UINT uiCmdBegin;
	UINT uiCmdEndPos;
	UINT uiCmdEnd;
};

LPWSTR WINAPI PathGetSetup(LPWSTR lpszPath);
LPWSTR WINAPI PathGetBin(LPWSTR lpszPath);
LPWSTR WINAPI PathGetPath(__in enumPathType umPathType, __out LPWSTR szPath);
#endif



_FK_END

/*--------------------------------------------------------------------------*/
#ifdef __cplusplus
}
#endif



#endif //__FkBase_h__