#ifndef __fk_class_h__
#define __fk_class_h__


#include "fk\array.h"
#include "fk\map.h"


_FK_BEGIN

class LNotify;
class LVxmlFile;
class LObjects;
class LComponent;
struct LModule;
class LSyncObject;
class LSyncObjects;

enum umObjectMake
{
	umObjectMakeAutoRelease	= 0x00000001,				// 默认等于 0 时，调用 Release 方法删除对象。
	umObjectMakeLockDelete	= 0x00000002,				// 默认等于 0 时，调用 Release 方法不删除对象。
	umObjectMakeNotify		= 0x00000004,				//
	umObjectMakeUpdateVariable  = 0x00000008,				// 变量更新到 CPP or .h 文件中。
};

class LBaseBuildUI;
struct LModule
{
	UINT			cbSize;						// 以后使用 GetModuleXXXXXX 方法，访问数据成员。
	DWORD			dwMask;
	HINSTANCE		hInst;
	HINSTANCE		hRes;
	LObjects*		pobjects;

#ifdef _FK_OLD_VER
	wchar_t*		pszResPath;
	wchar_t*		pszFileUI;
	LBaseBuildUI*	pBaseBuildUI;				// 这个pBaseBuildUI量以后会废除。
	fkCommandRange	cmdRange;					// 当前 .exe or .dll 文件使用的命令区域。 这个量以后也废除。
#endif
};


b_fncall InitializeModule(fk::LModule** ppModule, HINSTANCE hInst);
b_fncall UninitializeModule(fk::LModule** ppModule);
HINSTANCE _fncall GetModuleInst(fk::LModule* pModule);
HINSTANCE _fncall GetModuleInstRes(fk::LModule* pModule);
fk::LObject* _fncall GetModuleObjects(fk::LModule* pModule);


#ifdef  _FK_OLD_VER
b_fncall AllocCommandID(UINT uiCmdCount, fkCommandRange* ppcmdRange);	// 这个函数以后也废除。
#endif


class _FK_OUT_CLASS LObject : public IUnknown
{
protected:
	long*	_pParam;
	DWORD	_dwObjectMask;
	long	_dwRef;
	GUID*	_glink;
	fk::LNotify*	_pNotify;
	fk::LSyncObjects*  _pSyncObjects;

	b_call ModifyObject(DWORD dwRemoveMask, DWORD dwAddMask);
	b_call ObjectRelease(long* plRef);

public:
	_FK_DBGCLASSINFO_

	//wchar_t*		_sChildClassName;
	fk::LStringw	_sChildClassName;			// 最后一个子类名。

	LObject();
	LObject(DWORD dwObjectMask);
	virtual ~LObject();

	int GetRef(void);

	virtual ULONG STDMETHODCALLTYPE AddRef();
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual b_call Assign(LObject* pobject);
	virtual b_call CheckSuccess(void);
	virtual b_call DeleteChild(fk::LObject* pChildObject);
	virtual fk::LNotify* _call GetNotify();

	b_call AddSyncObject(fk::LSyncObject* pSyncObject);					// 这函数只能调用一次，不支持第二次安装
	b_call RemoveSyncObject(fk::LSyncObject* pSyncObject);	
	fk::LSyncObjects* _call GetSyncObjects(void);

	b_call SyncLock(DWORD dwTimeout = INFINITE);
	b_call SyncUnlock();
	virtual b_call SyncWait(LPARAM lparam);

	/**
	*	注册当前对象通报信息到 pReceiveObject 对象中。
	*	@param pSendObject 通报信息发送着。
	*	@param pNotify 通报发送对象。
	*   @param pReceiveObject 通报信息接收对象。
	*	@param dwEvent 事件值。
	*/
	b_call RegsvrNotifyTo(fk::LObject* pSendObject, fk::LNotify* pNotify, fk::LComponent* pReceiveObject, DWORD dwEvent);			// 注册事件到那里。

	/**
	*	注册当前对象通报信息到 pReceiveObject 对象中。此函数可以重载。
	*  @param pReceiveObject 通报信息接收对象。
	*	@param dwEvent 事件值。
	*/
	virtual b_call RegsvrNotifyTo(fk::LObject* pReceiveObject, DWORD dwEvent);			// 注册事件到那里。

	/**
	 *	删除对象通报消息接收着。
	 *	@param pSendObject 通报信息发送着。
	 *	@param pNotify 通报发送对象。
	 *   @param pReceiveObject 通报信息接收对象。
	 */
	b_call UnregsvrNotify(fk::LObject* pSendObject, fk::LNotify* pNotify, fk::LComponent* pReceiveObject);							// 取消注册的事件。

	/**
	*	删除当前对象通报消息接收着。此函数可以重载。
	*   @param pReceiveObject 通报信息接收对象。
	*/
	virtual b_call UnregsvrNotify(fk::LObject* pReceiveObject);							// 取消注册的事件。

	b_call IsUpdateVariable(void);
	v_call SetUpdateVariableState(bool bUpdate);

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LBaseData* pBaseDataValueIn,
										fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LBaseData* pBaseData);

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	virtual b_call WriteCtrlDataXml(fk::HXMLNODE hXmlParentNode);
};


enum enumSyncObject
{
	umSyncObjectNull			= 0x0,
	umSyncObjectCreateSection	= 0x1,

	umSyncObjectUser			= 0x100,			// 用户自定义同步类型。
};

class LSyncObject : public LObject
{
private:
protected:
	enumSyncObject _umSyncObject;

public:
	LSyncObject(enumSyncObject umSyncObject);
	virtual ~LSyncObject(void);

	enumSyncObject _call GetSyncObjectType(void);

	virtual b_call lock(DWORD dwTimeout = INFINITE) = 0;
	virtual b_call unlock() = 0;
	virtual b_call wait(LPARAM lparam) = 0;
	virtual vp_call GetfSyncObject(void) = 0;			// 可能是CRITICAL_SECTION  Event 等。
};

class LSyncObjects
{
private:
	fk::FARRAY _farray;

public:
	LSyncObjects();
	virtual ~LSyncObjects();

	virtual b_call lock(DWORD dwTimeout = INFINITE);
	virtual b_call unlock();
	virtual b_call wait(LPARAM lparam);

	i_call GetCount(void);
	data_t* GetDatas(void);

	b_call AddSynObject(fk::LSyncObject* psyncObject);
	b_call RemoveSyncObject(fk::LSyncObject* psyncObject);
};

class LCriticalSection : public LSyncObject
{
protected:
	CRITICAL_SECTION _sec;

public:
	LCriticalSection();
	virtual ~LCriticalSection();

	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call lock(DWORD dwTimeout = INFINITE);
	virtual b_call unlock();
	virtual b_call wait(LPARAM lparam);
	virtual vp_call GetfSyncObject(void);			// 可能是CRITICAL_SECTION  Event 等。

	//operator CRITICAL_SECTION*()   { return &_sec; }

	static LCriticalSection* _call NewCriticalSection(void);
};

class LAutoCriticalSection
{
private:
	LCriticalSection* _psec;
	bool _blocked;

public:
	LAutoCriticalSection(LCriticalSection* psec, bool bInitialLock);
	virtual ~LAutoCriticalSection();
};

//
// 这里是注册一种新的数据类型。
//
typedef LBaseData* (_fncall* LPFN_CREATEEDATATYPE)(int iCount);
typedef void (_fncall* LPFN_UNCREATEEDATATYPE)(LBaseData* pBaseData);
typedef struct tagREGISTERBASEDATA
{
	DWORD						dwSize;
	LPFN_CREATEEDATATYPE		lpfnCreateDataType;
	LPFN_UNCREATEEDATATYPE		lpfnUnCreateDataType;
}REGISTERBASEDATA,* LPREGISTERBASEDATA;
i_fncall BaseDataRegisterType(LPREGISTERBASEDATA lpRegsvrBaseData);
b_fncall BaseDataFindItem(INT iType, LPREGISTERBASEDATA lpRegBaseData);

enum enumAutoPtrType
{
	APT_UNKNOWN			=0x1,
	APT_INMALLOC		=0x2,			// 内部使用 malloc 分配置的。
	APT_OBJECT			=0x3,
	APT_INBSTR			=0x5,
	APT_VARIANT			=0x6,
	APT_FKMALLOC		=0x7,			// 外部和内部使用 FkMalloc 分配置的内存。
	APT_OUTMALLOC		=0x8,			// 外面使用 malloc 分配置的内存。 使用 AddChar AddWchar 
	APT_BASE			=0x9,
	APT_BASEDATA		=0x10,
	ART_DESTRUCTOR		=0x11,
	APT_GUID			=0x12,
	APT_PITEMIDLIST		=0x13,
	APT_OUTBSTR			=0x14,
	APT_LOCALALLOC		=0X15,
	APT_GLOBALALLOC		=0x16,
	APT_HEAPALLOC		=0x17,
	APT_VIRTUALALLOC	=0x18,
	APT_VIRTUALALLOCEX	=0x19,
	APT_XMLOPENFILE		=0x20,
};
struct AutoPtrItem
{
	void**	pptr;
	int		iType;
};

class _FK_OUT_CLASS LAutoPtrBase
{
protected:
	AutoPtrItem*	_pDatas;
	int				_iArrayIndex;
	int				_iAllocSize;

	bool AddRemalloc(int iAddArray);

public:
	LAutoPtrBase();
	virtual ~LAutoPtrBase();
};

// 建立一个删除的函数指针。
typedef bool (_fncall* FNDESTRUCTOR)(data_t*);

class _FK_OUT_CLASS LAutoPtr : public LAutoPtrBase
{
private:
public:
	LAutoPtr();
	virtual ~LAutoPtr();


	b_call Add(data_t** pptr, int iType);

	b_call AddUnknown(IUnknown** pUnknown);
	b_call AddUnknown2(IUnknown** pUnknown, IUnknown** pp2);
	b_call AddUnknown3(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3);
	b_call AddUnknown4(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4);
	b_call AddUnknown5(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5);
	b_call AddUnknown6(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6);
	b_call AddUnknown7(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6, IUnknown** pp7);
	b_call AddUnknown8(IUnknown** pUnknown, IUnknown** pp2, IUnknown** pp3, IUnknown** pp4, IUnknown** pp5, IUnknown** pp6, IUnknown** pp7, IUnknown** pp8);

	LObject* AddObject(LObject** pobj);

	fk::FARRAY _fncall ArrayCreateEx2(UINT uiInitCount, UINT uiNewCount, LPARAM lParam, FNARRAYDELETEDATA fnArrayDeleteData);

	HRESULT QueryInterface(IUnknown* pUnknown, REFIID riid, void** ppvObject);
	HRESULT CreateInstance(REFCLSID rclsid, LPUNKNOWN pUnkOuter, DWORD dwClsContext, IN REFIID riid, LPVOID FAR* ppv);

	void* Malloc(size_t _Size);
	HLOCAL LocalAlloc(UINT uFlags, SIZE_T uBytes);
	HGLOBAL GlobalAlloc(UINT uFlags, SIZE_T dwBytes);
	//LPVOID HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
	//LPVOID VirtualAlloc(HANDLE hProcess, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	///LPVOID VirtualAllocEx(HANDLE hProcess, LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);

	b_call AddDestructor(data_t** ppData, FNDESTRUCTOR fnDestructor);
	b_call AddVariantClear(VARIANT* pvt);

	b_call AddChar(char** pptr);
	b_call AddFKChar(char** pptr);

	b_call AddWchar(wchar_t** pptr);
	b_call AddFKWchar(wchar_t** pptr);

	b_call AddBase(LBase** ppBase);
	b_call AddBaseData(LBaseData** ppBaseData);
	b_call AddItemIDList(LPITEMIDLIST* lppItemIDList);

	BSTR SysAllocString(const OLECHAR* psz);
	b_call AddSysAllocString(BSTR* bstr);

	GUID* MallocGuid();
	b_call Clear();

	//
	// 打开 xml 文件名称。
	// szFileName - 可以是绝对路径，也可是 exe 文件的相对路径。
	// ppXmlConfig - 返回 HXMLCONFIG 句柄。
	//
	b_fncall xmlfOpenFile2(LPCWSTR szFileName, LPVOID* ppXmlConfig);

	//
	// 打开资源文件名称。
	// szFileName - 可以是绝对路径，也可是 res 文件相对路径。
	// ppXmlConfig - 返回 HXMLCONFIG 句柄。
	//
	b_fncall xmlfOpenResFile2(fk::LModule* pmodule, LPCWSTR szFileName, LPVOID* ppXmlConfig);
};

#define fk_AddUnknown(ap, p1)						ap.AddUnknown((IUnknown**)&p1)
#define fk_AddUnknown2(ap, p1, p2)					ap.AddUnknown2((IUnknown**)&p1, (IUnknown**)&p2)
#define fk_AddUnknown3(ap, p1, p2, p3)				ap.AddUnknown3((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3)
#define fk_AddUnknown4(ap, p1, p2, p3, p4)			ap.AddUnknown4((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3, (IUnknown**)&p4)
#define fk_AddUnknown5(ap, p1, p2, p3, p4, p5)				ap.AddUnknown5((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3, (IUnknown**)&p4, (IUnknown**)&p5)
#define fk_AddUnknown6(ap, p1, p2, p3, p4, p5, p6)			ap.AddUnknown6((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3, (IUnknown**)&p4, (IUnknown**)&p5, (IUnknown**)&p6)
#define fk_AddUnknown7(ap, p1, p2, p3, p4, p5, p6, p7)		ap.AddUnknown7((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3, (IUnknown**)&p4, (IUnknown**)&p5, (IUnknown**)&p6, (IUnknown**)&p7)
#define fk_AddUnknown8(ap, p1, p2, p3, p4, p5, p6, p7, p8)	ap.AddUnknown8((IUnknown**)&p1, (IUnknown**)&p2, (IUnknown**)&p3, (IUnknown**)&p4, (IUnknown**)&p5, (IUnknown**)&p6, (IUnknown**)&p7, (IUnknown**)&p8)



class _FK_OUT_CLASS LAutoPtrTl : public fk::LAutoPtr
{
private:
protected:
public:
	LAutoPtrTl();
	virtual ~LAutoPtrTl(void);

	fk::LFileOperation* _call NewFileOperation2(void);
};


class _FK_OUT_CLASS LComponent : public fk::LObject
{
protected:

public:
	LComponent();
	LComponent(DWORD dwObjectMask);
	virtual ~LComponent();

	fk::LAutoPtr	_AutoPtr;

	virtual b_call Assign(LObject* pobject);
};
extern IID IID_IComponent;			// {0CFF5515-186E-42B4-81C7-35BF67C6FD93}


class _FK_OUT_CLASS LStringws : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringws(void);
	virtual ~LStringws(void);

	virtual fk::LStringw* NewTextw(void);

	virtual void clear(void);
	virtual i_call GetCount(void);
	virtual data_t* _call GetDatas(void);
	virtual data_t* _call begin(void);
	
	virtual fk::LStringw* _call GetItem(UINT uiIndex);

	virtual i_call FindItemI(LPCWSTR lpszText);
	virtual b_call FullCompose(fk::LStringw* pFullText, LPCWSTR lpszApart);

	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringw* pString);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringws;			//// {0CFF5515-186E-42B4-81C7-35BF67C6FD93}



class _FK_OUT_CLASS LStringwsList : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringwsList(void);
	virtual ~LStringwsList(void);

	virtual fk::LStringws* NewStringws(void);
	virtual void Clear(void);
	virtual i_call GetCount(void);

	virtual fk::LStringws* _call GetItem(UINT uiIndex);

	virtual b_call FindItemI(LPCWSTR lpszText, fk::LStringws** pStringwsOut, UINT* uipIndex);
	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringws* pString);
	
	//	virtual b_call FullCompose(fk::LStringws* pFullText, LPCWSTR lpszApart);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringwsList;		// {2A226983-ADC6-4D9C-9B2E-1A511DD74880}



class _FK_OUT_CLASS LStringas : public fk::LObject
{
private:
	fk::FARRAY _farray;

public:
	LStringas(void);
	virtual ~LStringas(void);

	virtual fk::LStringa* NewText(void);

	virtual void clear(void);
	virtual i_call GetCount(void);
	virtual data_t* _call GetDatas(void);
	virtual data_t* _call begin(void);

	virtual fk::LStringa* _call GetItem(UINT uiIndex);

	virtual i_call FindItemI(LPCSTR lpszText);
	virtual b_call FullCompose(fk::LStringa* pFullText, LPCSTR lpszApart);

	virtual b_call DeleteItem(UINT uiIndex);
	virtual b_call Delete(fk::LStringa* pString);

	virtual b_call GetEnumData(fk::LEnumData** pEnumData);
};
extern IID IID_IStringas;			// {AF61E718-F5BB-49BC-AF4E-CECCB431099C}


bool WINAPI StrCheckLenA(HWND hwndHint, char* pstr, int iMinLen, int iMaxLen);
void WINAPI ShowErrorHResult(HWND hWnd, HRESULT hr, LPCWSTR lpszFunName);
typedef void (WINAPI* FNLWADDINFOW)(wchar_t*);



enum enumVxmlFileType
{
	umVxmlFileMainFrame = 0x0,
	umVxmlFileDialog = 0x1,
	umVxmlFileView = 0x2,
};

class _FK_OUT_CLASS LVxmlFile : public fk::LComponent
{
private:
public:
	LVxmlFile(void);
	virtual ~LVxmlFile(void);

	fk::HXMLCONFIG	_hXmlConfig;
	DWORD			_dwVer;

	virtual b_call OpenXmlFile(fk::LModule* pmodule, LPCWSTR lpszVxmlFile) = 0;
	virtual v_call CloseXmlFile(void) = 0;
	virtual dw_call GeVxmlVersion(void) = 0;

	static LVxmlFile* _call NewVxmlFile(void);
};
extern IID IID_IVxmlFile;			// {B9D1FBF0-3D2B-4FE8-8675-42D073579E7E}

//
// 注册一个创建对象列表,   如何没有新的类，可以不调用这里。
//
// b_fncall CreateObject(fk::LModule* pmodule, fk::LObjects* pobject,
//							LPCSTR lpszClassName, fk::HXMLNODE hXmlNode, DWORD dwMake, fk::LObject** ppNewObject);
//

enum enumCrateObjectDefault
{
	CODF_DATA_XML		= 0x1,		// 创建对象从 xml 文件中. 需要写 hXmlNode 参数。
	CODF_DATA_DEFAULT	= 0x2,		// 创建对象默认值，不从 xml 文件中读，不需要写 hXmlNode 参数。
};

typedef struct tagCREATEOBJECTINFO
{
	DWORD dwSize;
	DWORD dwMask;

	fk::LModule* pmodule;
	fk::LObjects* pobjects;
	LPSTR lpszClassName;
	fk::LVxmlFile* pVxmlFile;
	fk::HXMLNODE hXmlNode;
}CREATEOBJECTINFO,*LPCREATEOBJECTINFO;


typedef bool (_fncall* LPFNCREATEOBJECT)(LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject);
typedef bool (_fncall* LPFNLINK_OBJECT)(LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow);

typedef struct tagREGOBJECTITEM
{
	DWORD				dwSize;
	DWORD				dwMask;
	LPSTR				lpszClass;
	LPFNCREATEOBJECT	lpfnCreateObject;
	LPFNLINK_OBJECT		lpfnLinkObject;
}REGOBJECTITEM,* LPREGOBJECTITEM;

b_fncall ObjectReg(LPREGOBJECTITEM lpRegClassItem);
b_fncall ObjectUnreg(LPFNCREATEOBJECT lpfnCreateObject);

b_fncall ObjectFindW(LPCWSTR lpszClassName);
b_fncall ObjectFindA(LPCSTR lpszClassName);

#ifdef UNICODE
#define ObjectFind		ObjectFindW
#else
#define ObjectFind		ObjectFindA
#endif



typedef struct tagCRAET_OBJECT_DEFAULT
{
	DWORD dwSize;
	DWORD dwMask;
	fk::LModule* pmodule;
	LPSTR lpszClassName;
	LPWSTR lpszVarName;
}CRAET_OBJECT_DEFAULT,*LPCRAET_OBJECT_DEFAULT;

class _FK_OUT_CLASS LObjects : public fk::LObject
{
private:
	void* _pobjects;

protected:
public:
	LObjects();
	virtual ~LObjects();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual b_call AddObject(LPCWSTR lpszVarName, IUnknown* pUnknown);
	virtual b_call AddObject(IUnknown* pUnknown);
	virtual b_call CraetObjectDefault(LPCRAET_OBJECT_DEFAULT lpcod, fk::LObject** ppNewObject);
	virtual b_call RemoveObject(LPCWSTR lpszVarName, IUnknown* pUnknown);
	virtual b_call RemoveObject(IUnknown* pUnknown);
	virtual b_call RenObjectVar(LPCWSTR lpszOldVar, LPCWSTR lpszNewVar);

	virtual b_call GetEnumData(LEnumData** ppEnumData);
	//virtual b_call GetDatas(void);

	virtual IUnknown* Find(LPCWSTR lpszVarName);
	virtual IUnknown* GetItem(int iitem);
	virtual i_call GetCount(void);
	virtual v_call Clear();
	virtual fk::enumReturnInfo _call ReadObjectXml(fk::LModule* pmodule, fk::LVxmlFile* pVxmlFile, LPCSTR lpszXmlPath);

	static fk::LObjects* _call NewObjects();
};
extern IID IID_IObjects;				// {96CF7164-F739-4CC8-93A2-46D6DF02C4E1}


//
// 这个函数是管理当前进程设置的 LObjects 对象列表， 并不是当前进程的 LObjects 对象列表。
//
class _FK_OUT_CLASS LObjectsList : public fk::LObject
{
private:
	void* _pobjects;

protected:
public:
	LObjectsList(void);
	virtual ~LObjectsList(void);

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual b_call AddObject(LPCWSTR lpszKey, fk::LObjects* pObjects);
	virtual b_call AddObject(fk::LObjects* pObjects);
	virtual b_call RemoveObject(LPCWSTR lpszKey, fk::LObjects* pObjects);
	virtual b_call RemoveObject(fk::LObjects* pObjects);

	virtual b_call GetEnumData(LEnumData** ppEnumData);

	virtual fk::LObjects* Find(LPCWSTR lpszKey);
	virtual ::IUnknown* FindObjectsChildItem(LPCWSTR lpszKey);

	virtual i_call GetCount(void);
	virtual v_call Clear();
	//virtual b_call ReadObjectXml(fk::LModule* pModule, fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath);

	static LObjectsList* _fncall NewObjectsList(void);
	static LObjectsList* _fncall GetGlobalObjectsList(void);
};

//
//class _FK_OUT_CLASS LError : public fk::LObject
//{
//private:
//protected:
//	fk::LStringw	_sErrorInfo;
//	fk::LStringw	_sErrorHint;
//	bool			_bSuccess;
//
//public:
//	LError(void);
//	virtual ~LError(void);
//
//	v_call TraceError(void);
//	fk::LStringw* _call GetErrorHint(void);
//
//	virtual b_call IsSuccess(void);
//	virtual v_fncall ShowErrorBox(HWND hWndParent, UINT uType);
//};


class _FK_OUT_CLASS LError : public fk::LObject
{
private:
protected:
	fk::LStringw	_sErrorInfo;
	bool			_bSuccess;

public:
	LError(void);
	virtual ~LError(void);

	v_call TraceError(void);
	v_call AddItemComma(LPWSTR lpszErrorItem);				// 使用 _sErrorInfo 变量。 格式：  ", lpszErrorItem"

	virtual b_call IsSuccess(void);
	virtual v_call clear(void);

	v_call AddInfo(LPCWSTR lpszInfo);
	fk::LStringw&  GetErrorInfo(void);

//	virtual v_fncall ShowErrorBox(HWND hWndParent, UINT uType);
};


/*
使用顺序：
fk::LXmlPropError xpe;

if (!xpe.IsErrorStrip())
	xpe.NewStrip();
*/
class _FK_OUT_CLASS LXmlPropError : public fk::LError
{
private:
protected:
	int				_iStripIndex;
	bool			_bErrorPropName;
	bool			_bErrorStrip;
	fk::LStringw	_sXmlFile;

public:

	LXmlPropError(void);
	virtual ~LXmlPropError(void);

	virtual v_call clear(void);

	b_call AddErrorPropName(LPCSTR lpszPropName);
	v_call NewLine(void);								// 建立一个新行。
	v_call NewStrip(void);								// 开始第几条错误。
	b_call IsErrorStrip(void);							// 存在开始第几条错误。
	b_call IsErrorPropName(void);						// 
	v_call SetErrorXmlFile(LPCSTR lpszXmlFile);
	//v_call ShowErrorBox(HWND hWndParent);
};


//
// virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
//
class _FK_OUT_CLASS LNotify : public fk::LObject
{
private:
protected:
public:
	FKNOTIFY _hNotify;

	LNotify();
	virtual ~LNotify();

	virtual b_call Attach(LObject* pObject);

	virtual STDMETHODIMP QueryInterface(REFIID riid, void** ppvObject);

	b_call NotifyAdd(LPNOTIFYITEM pNotifyItem);
	b_call NotifyRemove(LPNOTIFYITEM pNotifyItem);
	b_call NotifyCall(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	b_call NotifyCallObject(void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	b_call NotifyCallEx(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc);
	b_call NotifyGetCount(UINT* uiCount);
	b_call NotifySetSelf(LPNOTIFYITEM pcnpi);
	FKNOTIFY GetNotifyHandle(void);
	b_call NotifySetState(DWORD dwNotifyState);
	dw_call NotifyGetState();

	b_call InstallNotifySelfEvent(fk::enumInstallEvent emie,
								void* dwRegObject, DWORD dwObjectID, fk::LObject* pTargetProc);
	//默认接收两个以个消息。 ON_NOTIFY_ADDITEM|ON_NOTIFY_REMOVEITEM;

	static b_fncall NewNotify2(fk::enumClassValue cvClassValue, fk::LNotify** ppv);
	static fk::LNotify* _fncall NewNotify();
};
extern IID IID_INotify;				// {06C06794-EF64-4c0a-B09A-D94FF28815B5}


class _FK_OUT_CLASS LNotifys : public fk::LComponent
{
private:
protected:
public:
	LNotifys(void);
	virtual ~LNotifys(void);

	virtual b_call Attach(LObject* pObject) = 0;
	virtual b_call Add(fk::LNotify* pNotify) = 0;
	virtual b_call Remove(fk::LNotify* pNotify) = 0;
	virtual b_call Call(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent) = 0;
	virtual b_call CallObject(void* pobj, void* pTargetObj, UINT uMsg, fk::PNOTIFYEVENT pEvent) = 0;
	virtual b_call CallEx(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent, NOTIFYCALLPROC pCallProc) = 0;
	virtual b_call GetCount(UINT* uiCount) = 0;
	//virtual b_call SetSelf(LPNOTIFYITEM pcnpi) = 0;
	virtual fk::FKNOTIFYS GetNotifysHandle(void) = 0;
	virtual b_call SetState(DWORD dwNotifyState) = 0;
	virtual dw_call GetState() = 0;

	//virtual b_call InstallNotifySelfEvent(fk::enumInstallEvent emie,
	//	void* dwRegObject, DWORD dwObjectID, fk::LComponent* pTargetProc);
	////默认接收两个以个消息。 ON_NOTIFY_ADDITEM|ON_NOTIFY_REMOVEITEM;

	static fk::LNotifys* _call NewNotifys(void);
};
// {20A5F059-6110-4836-8BF9-850EE9F47E30}
extern IID IID_INotifys;



class LEnumItem : public fk::LObject
{
public:
	LEnumItem();
	virtual ~LEnumItem();

	fk::LStringw _sValue;				// xml value
	int _idata;							// xml index
	fk::HXMLNODE _fXmlNode;

	static LEnumItem* NewEnumItem();
};

//
// 定义enum 类型使用的类，让 enum 拥有返回自己说明的类。
//
// 可以不使用 NewEnumPlus 方法创建，也能直接使用。
//
class _FK_OUT_CLASS LEnumPlus : public fk::LComponent
{
private:
	fk::FARRAY _farray;

protected:
	int _iItemValue;

public:
	LEnumPlus();
	virtual ~LEnumPlus();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void** ppvObject);

	virtual b_call TargetProc(void* pobj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual void SetActiveItem(int dwData);
	virtual int GetActiveItem();
	virtual i_call GetCount(void);

	virtual b_call ReadXmlData(fk::LModule* pModule, fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath);
	virtual data_t* GetDatas(void);
	virtual fk::LEnumItem* _call GetEnumItem(INT iitem);

	virtual int GetResourceID(int nEnumItem);
	virtual int GetActiveResourceID();
	virtual bool GetDataText(int dwData, wchar_t* wsItemBuf);
	virtual bool GetDataText(int dwData, fk::LStringa* sItemText);
	virtual bool GetDataText(int dwData, fk::LStringw* sItemText);
	virtual bool GetActiveEnumItemText(wchar_t* wsItemBuf);

	virtual b_call InsertEnumItem(int iIndex, wchar_t* pwsText, int dwData, fk::HXMLNODE hXmlNodeOrNull);
	virtual b_call AddEnumItem(wchar_t* pwsText, int dwData, fk::HXMLNODE hXmlNodeOrNull);

	virtual b_call DataToComboBox(HWND hWndCtrl);
	virtual b_call DataToListBox(HWND hWndCtrl);

	virtual v_call Clear(void);

	static fk::LEnumPlus* NewEnumPlus(void);
};

enum umEnumPlusEvent
{
	ON_INSERTITEM	=0x00000001,
	ON_REMOVEITEM	=0x00000002,
	ON_READDATA		=0x00000004,
	ON_CLEARDATA	=0x00000008,
};

typedef struct tagENUMPLUSINSERT
{
	fk::NOTIFYEVENT nEvent;
	LEnumPlus*		pEnumPlus;
	int				iInsert;
	wchar_t*		wsText;
	DWORD			dwData;
}ENUMPLUSINSERT,*PENUMPLUSINSERT;


typedef struct tagENUMPLUSREMOVE
{
	fk::NOTIFYEVENT nEvent;
	LEnumPlus*		pEnumPlus;
	int				iInsert;
	wchar_t*		wsText;
	DWORD			dwData;
}ENUMPLUSREMOVE,* PENUMPLUSREMOVE;


typedef struct tagENUMPLUSREADDATA
{
	fk::NOTIFYEVENT nEvent;
	LEnumPlus*		pEnumPlus;
}ENUMPLUSREADDATA,* PENUMPLUSREADDATA;



enum umMacroParam
{
	umMacroParamSuccess	=0x00000001,
	umMacroParamFixed	=0x00000002,			// 固定宏，不能删除和清空。
};

typedef struct tagMacroParam
{
	DWORD dwMask;
	fk::LStringw sName;
	fk::LStringw sParam;
}MACROPARAM,*LPMACROPARAM;

enum umMakeMacroReturn
{
	mmrExistMacro=0x1,
	mmrMakeFail=0x2,
	mmrSuccess=0x3,
};



class _FK_OUT_CLASS LMacroParamsw : public fk::LComponent
{
private:

public:
	LMacroParamsw();
	virtual ~LMacroParamsw();

	virtual void Clear(void) = 0;
	virtual i_call GetCount(void) = 0;
	virtual data_t* _call GetDatas(void) = 0;

	virtual b_call Assign(LObject* pobject) = 0;
	virtual b_call AddMacroItem(LPCWSTR pszName, LPCWSTR pwsParam, DWORD dwMake=umMacroParamSuccess) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCWSTR lpszxmlMacroPath) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCSTR lpszxmlMacroPath) = 0;

	virtual MACROPARAM* _call FindMacroParamItem(LPCWSTR pwsName) = 0;

	virtual umMakeMacroReturn _call MakeMacro(fk::LStringw* spOutString) = 0;
	virtual b_call ModifyMacro(LPCWSTR pszName, LPCWSTR lpszParam) = 0;
	virtual b_call CheckStringMacro(const wchar_t* wsOutString) = 0;
	virtual b_call CheckMacroData() = 0;
	virtual v_call PrintfMacroData() = 0;

	static fk::LMacroParamsw* NewMacroParamsw(void);
};


typedef struct tagMacroParamA
{
	DWORD dwMask;
	fk::LStringa sName;
	fk::LStringa sParam;
}MACROPARAMA,*LPMACROPARAMA;

class _FK_OUT_CLASS LMacroParamsa : public fk::LComponent
{
private:

public:
	LMacroParamsa();
	virtual ~LMacroParamsa();

	virtual void Clear(void) = 0;
	virtual i_call GetCount(void) = 0;
	virtual data_t* _call GetDatas(void) = 0;

	virtual b_call Assign(LObject* pobject) = 0;
	virtual b_call AddMacroItem(LPCSTR pszName, LPCSTR pwsParam, DWORD dwMake=umMacroParamSuccess) = 0;

	virtual b_call ReadMacroItems(fk::HXMLNODE hxmlNodeItems) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCSTR xmlMacroPath) = 0;
	virtual b_call ReadMacroItems(fk::HXMLCONFIG hXmlConfig, LPCWSTR lpszxmlMacroPath) = 0;

	virtual fk::LPMACROPARAMA _call FindMacroParamItem(LPCSTR pwsName) = 0;

	virtual umMakeMacroReturn _call MakeMacro(fk::LStringa* spOutString) = 0;
	virtual b_call ModifyMacro(LPCSTR pszName, LPCSTR lpszParam) = 0;
	virtual b_call CheckStringMacro(LPCSTR wsOutString) = 0;
	virtual b_call CheckMacroData() = 0;
	virtual v_call PrintfMacroData() = 0;

	virtual v_call SetMacroBeginEnd(LPCSTR lpszBegin, LPCSTR lpszEnd) = 0;

	static fk::LMacroParamsa* NewMacroParamsW(void);
};

typedef struct tagFILELINEINFO
{
	int iLine;				// 行。
	int iBeginPos;			// 文件位置。
	int iLineLen;			// 文件行长度位置。
}FILELINEINFO,*LPFILELINEINFO;
class LFileOperation : public fk::LObject
{
public:
	LFileOperation();
	virtual ~LFileOperation();
	FILE*	_pFile;

	virtual b_call OpenFile(LPCWSTR lpszFileName, LPCWSTR lpszMode) = 0;
	virtual b_call OpenFile(LPCWSTR lpszFileName) = 0;
	virtual i_call fseek(long offset, int origin)=0;
	virtual i_call fputc(int c) = 0;
	virtual b_call IsOpen(void)=0;
	virtual l_call GetFileLength(void)=0;
	virtual v_call Close(void) = 0;
	virtual b_call SetEndOfFile(LONG lEndPos) = 0;

	virtual int WriteText(LPCWSTR lpszText, int nLength)=0;
	virtual int WriteText(LPCSTR lpszText, int nLength)=0;
	virtual i_call ReadText(fk::LStringa* pcText, int nLength)=0;
	virtual i_call ReadFull(fk::LStringa* psFullText) = 0;

	virtual b_call ReadLine(int iLine, fk::LStringw* pwTextLine) = 0;
	virtual b_call ReadLine(int iLine, fk::LStringa* pcTextLine) = 0;
	virtual fk::LPFILELINEINFO _call FindText(LPCSTR lpszText) = 0;
	virtual fk::LPFILELINEINFO _call FindText(LPCWSTR lpszText) = 0;
	virtual i_call FindTextPos(LPCSTR lpszText, int iPos) = 0;
	virtual i_call FindTextPos(LPCWSTR lpszText, int iPos) = 0;

	virtual data_t* _call GetLines(void) = 0;
	virtual i_call GetLineCount(void) = 0;

	static LFileOperation* _fncall NewFileOperation(void);
};



#define SIF_TEXT		0x00000001
#define SIF_ASK			0x00000002
#define SIF_STAR		0x00000004
#define SIF_ENGLISH		0x00000008

/*
abc*.exe    abc*.exe
abc*a		abc*
a?*			a*
a?*a		a*
a?bc?d*a	a?bc?d*

忽略*号以后的字符，不忽略.号以后的字符。
如果*号前面是？号，忽略？号。
*/
typedef struct tagSTRITEM
{
	DWORD		dwMask;
	wchar_t*	pwText;
	int			iLen;
}STRITEM, *LPSTRITEM;

#define  STR_COMPART_CHAR	_T("|")
#define  STR_COMPART_C		_T('|')


class _FK_OUT_CLASS LFiltrateString : public LObject
{
public:
	LFiltrateString();
	virtual ~LFiltrateString();

	/* 设置过滤掉的字符串信息，字符串之间用 ; 号分开 */
	virtual bool SetFiltrateInfo(LPCWSTR lpszFiltrateInfo, LPCTSTR lpszCompart)=0;
	virtual bool SetFiltrateInfoFile(LPCWSTR lpszFiltrateInfo, LPCTSTR lpszCompart) = 0;

	/*
	** 返回过滤掉的字符串信息
	** wsText 指定字符串空间。
	*/
	virtual int GetFiltrateInfo(wchar_t* lpszText, LPCTSTR lpszCompart)=0;
	virtual int GetFiltrateInfoLen(void)=0;
	/*
	** 判断 strItem 中是否包含 CFiltrateTypeList 列表中的某一项。
	** 如何包括返回 True，不包含返回 Flase.
	*/
	//virtual bool wcsicmp(LPCWSTR wsText)=0;
	//virtual bool wcscmp(LPCWSTR wsText)=0;
	virtual bool StrI(LPCWSTR lpszText)=0;
	virtual bool AddTextItem(LPCWSTR lpszText)=0;
	virtual bool AddFileItem(LPCWSTR lpszText)=0;
	virtual void Clear()=0;
	virtual int GetItemCount() = 0;
	virtual b_call GetItemArray(STRITEM* pstrItem) = 0;

	static fk::LFiltrateString* WINAPI NewFiltrateString();
	static bool WINAPI DeleteFiltrateString(fk::LFiltrateString* psf);
};
// {7897A235-D8AA-418f-AC62-CD3FD966C067}
static const GUID IID_IFiltrateString = { 0x7897a235, 0xd8aa, 0x418f, { 0xac, 0x62, 0xcd, 0x3f, 0xd9, 0x66, 0xc0, 0x67 } };



//
// 载入语言动态链接库。
//
#pragma warning(disable:4311)
inline bool WINAPI LanguageLoadProjectInline(fk::LModule* pmodule, HINSTANCE hInstance)
{
	HINSTANCE hResourceInstance=NULL;

#ifdef _AFX
	hResourceInstance=fk::LanguageLoadFile(hInstance);

	if(hResourceInstance==NULL)
	{
		// AfxSetResourceHandle(AfxGetInstanceHandle());
		return false;
	}
	else
	{
		AfxSetResourceHandle(hResourceInstance);
		return true;
	}
#endif


#ifdef _ATL
	hResourceInstance = fk::LanguageLoadFile(hInstance);

	if (hResourceInstance==NULL)
	{
		//ATL::_AtlBaseModule.SetResourceInstance(	ATL::_AtlBaseModule.GetModuleInstance()	);
		return false;
	}
	else
	{
		pmodule->hRes = hResourceInstance;
		ATL::_AtlBaseModule.SetResourceInstance(hResourceInstance);
		return true;
	}
#endif

	return false;
}
#pragma warning(default:4311)



//
// 载入语言是项目自身。
//
#pragma warning(disable:4311)
inline bool WINAPI LanguageLoadSelf(fk::LModule* pmodule, HINSTANCE hInstance)
{
	if (hInstance!=NULL)
	{
#ifdef _AFX
		AfxSetResourceHandle(hInstance);
		return true;
#endif


#ifdef _ATL
		pmodule->hRes = hInstance;
		ATL::_AtlBaseModule.SetResourceInstance(hInstance);
		return true;
#endif
	}

	return false;
}

#pragma warning(default:4311)




#if _FK_OLD_VER<=0x0001
//
//--------------------------------------------------------------------------
//  这个类以后也会被移除。
//
class _FK_OUT_CLASS LBaseBuildUI : public fk::LObject
{
public:
	LBaseBuildUI(void);
	virtual ~LBaseBuildUI();

	virtual fk::HXMLCONFIG _call GetConfigHandle() = 0;
	virtual b_call ResFileOpen() = 0;
	virtual b_call ResFileClose() = 0;
	virtual wchar_t* GetFileName() = 0;
};
#endif

_FK_END

#endif
