#ifndef __FK_FRAMEWORKIMPL_H__
#define __FK_FRAMEWORKIMPL_H__


#ifdef __ArtFramework_FkUser_h__

#include "fk\ArtFramework_i.h"

#else

_FK_BEGIN
#include "fk\ArtFramework_i.h"
_FK_END

#endif

#include "BuildUI.h"

#include "fk\Thread.hxx"



#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */





_FK_BEGIN

//
// IComFramework 事件 ID
//
#define ID_ComFramework_Quit					1
#define ID_ComFramework_ActiveMainFrame			2
#define ID_ComFramework_PopupMenu				3
#define ID_ComFramework_CloseMainFrame			4


#define ArtShellTreeCtrlEventType				50
#define ArtShellListEventType					51




// ShellListCtrl event.
static const int DISPID_SLC_OnSelectItem		= 1;
static const int DISPID_SLC_OnSelectItems		= 2;
static const int DISPID_SLC_OnDBClickItem		= 3;
static const int DISPID_SLC_OnChangFolder		= 4;

static ATL::_ATL_FUNC_INFO SLC_OnSelectItem={CC_STDCALL, VT_EMPTY, 4, {VT_BSTR, VT_BYREF, VT_BSTR, VT_I8}};
static ATL::_ATL_FUNC_INFO SLC_OnSelectItems={CC_STDCALL, VT_EMPTY, 4, {VT_BSTR, VT_I4, VT_BSTR, VT_I8}};
static ATL::_ATL_FUNC_INFO SLC_OnDBClickItem={CC_STDCALL, VT_EMPTY, 2, {VT_DISPATCH, VT_BYREF}};
static ATL::_ATL_FUNC_INFO SLC_OnChangFolder={CC_STDCALL, VT_EMPTY, 2, {VT_DISPATCH, VT_BYREF}};


#define  DIID_MdiContainer_InsertItem	1
#define  DIID_MdiContainer_RemoveItem	2
#define  DIID_MdiContainer_ActiveItem	3




#define  DIID_MainFrames_ActiveMainFrame	1
#define  DIID_MainFrames_CloseMainFrame		2
#define  DIID_MainFrames_CreateMainFrame	3
#define  DIID_MainFrames_CreateMainFrames	4
#define  DIID_MainFrames_CloseMainFrames	5


#define  DIID_MainFrame_BeginCreate				1
#define  DIID_MainFrame_EndCreate				2
#define  DIID_MainFrame_CloseWindowPanel		3
#define  DIID_MainFrame_InstallUserDataUI		4
#define  DIID_MainFrame_UninstallUserDataUI		5
#define  DIID_MainFrame_CloseMainFrame			6
#define  DIID_MainFrame_ActiveMainFrame			7


#define  DIID_ComFramework_Quit				1
#define  DIID_ComFramework_ActiveMainFrame	2
#define  DIID_ComFramework_PopupMenu		3
#define ID_ComFramework_Quit					1
#define ID_ComFramework_ActiveMainFrame			2
#define ID_ComFramework_PopupMenu				3
#define ID_ComFramework_InstallUserData			4
#define ID_ComFramework_UninstallUserData		5



enum enumMainFrameEvent
{
	ON_MF_ACTIVE			=0x00000000000000001,
	ON_MF_QUERYCLOSE		=0x00000000000000002,
	ON_MF_CLOSEPAGE			=0x00000000000000004,
	ON_MF_MAINFRAMECLOSE	=0x00000000000000008,
	ON_MF_INSERTITEM		=0x00000000000000010,
	ON_MF_ACTIVEITEM		=0x00000000000000020,
	ON_MF_DRAGFILES			=0x00000000000000040,		// 这个消息先设置在这里。
	ON_MF_WM_USER			=0x00000000000000080,		// WM_USER
	ON_MF_WM_COPYDATA		=0x00000000000000100,		// WM_COPYDATA
};

typedef struct tagONMainFrameActive
{
	fk::NOTIFYEVENT nEvent;
	IComFramework* pfk;
	IMainFrames* pMainFrames;
	IMainFrame* pMainFrame;
	UINT uintState;
}ONMainFrameActive,*LPONMainFrameActive;

typedef struct ONMainFrameQueryClose
{
	fk::NOTIFYEVENT nEvent;
	IComFramework* pfk;
	IMainFrames* pMainFrames;
	IMainFrame* pMainFrame;
	bool bExit;
}ONMainFrameQueryClose,*LPONMainFrameQueryClose;

typedef struct ONMainFrameClosePage
{
	fk::NOTIFYEVENT nEvent;
	IComFramework* pfk;
	IMainFrames* pMainFrames;
	IMainFrame* pMainFrame;
	bool bExit;
}ONMainFrameClosePage,*LPONMainFrameClosePage;

typedef struct ONMainFrameClose
{
	fk::NOTIFYEVENT nEvent;
	IComFramework* pfk;
	IMainFrames* pMainFrames;
	IMainFrame* pMainFrame;
}ONMainFrameClose,*LPONMainFrameClose;

typedef struct ONMainFrameInsertItem
{
	fk::NOTIFYEVENT nEvent;
	IComFramework*	pfk;
	IMainFrames*	pMainFrames;
	IMainFrame*		pMainFrame;
	ITabPageItem*	pTabPageItem;
}ONMainFrameInsertItem,*LPONMainFrameInsertItem;

typedef struct ONMainFrameActiveItem
{
	fk::NOTIFYEVENT nEvent;
	IComFramework*	pfk;
	IMainFrames*	pMainFrames;
	IMainFrame*		pMainFrame;
	ITabPageItem*	pTabPageItem;
	VARIANT_BOOL*	bvarActive;
}ONMainFrameActiveItem,*LPONMainFrameActiveItem;


typedef struct tagMainFrameDragFiles
{
	fk::NOTIFYEVENT nEvent;
	IComFramework*	pfk;
	IMainFrames*	pMainFrames;
	IMainFrame*		pMainFrame;
	HDROP			hDrop;
}MainFrameDragFiles,*LPMainFrameDragFiles;

typedef struct tagMainFrameWMUser
{
	fk::NOTIFYEVENT nEvent;
	IComFramework*	pfk;
	IMainFrames*	pMainFrames;
	IMainFrame*		pMainFrame;
	WPARAM			wParam;					// 这个设置子消息。
	LPARAM			lParam;					// 这个设置消息需要的数据。
}MainFrameWMUser,*LPMainFrameWMUser;


typedef struct tagMainFrameCopyData
{
	fk::NOTIFYEVENT nEvent;
	IComFramework*	pfk;
	IMainFrames*	pMainFrames;
	IMainFrame*		pMainFrame;
	WPARAM			wParam;					// 这个设置子消息。
	PCOPYDATASTRUCT	lpcds;					// 这个设置消息需要的数据。
}MAINFRAMECOPYDATA,*LPMAINFRAMECOPYDATA;



enum __umStatusBarEventOn
{
	ON_SB_HOTITEM	= 0x0000000000000001,
	ON_SB_LOSE		= 0x0000000000000002,
};

typedef struct tagSBITEM
{
	DWORD			dwSize;
	fk::NOTIFYEVENT nEvent;
	int				iRet;
	IStatusBar*		pStatusBar;
	IStatusBarItem*	pStatusBarItem;
	int				iItem;
	RECT			rcItem;
}SBITEM, *LPSBITEM;


enum __umGenericPanelOn
{
	ON_GP_ACTIVE= 0x0000000000000001,
	ON_GP_LOSE	= 0x0000000000000004,
};

typedef struct tagGenericPanelItem
{
	DWORD dwSize;
	fk::NOTIFYEVENT nEvent;
	int iRet;
	IGenericPanel* pGenericPanel;
	IGenericPanel* pGenericPanelItem;
}GENERICPANELITEM, *LPGENERICPANELITEM;


//UINT     cbSize;
//UINT     fMask;
//UINT     fType;         // used if MIIM_TYPE (4.0) or MIIM_FTYPE (>4.0)
//UINT     fState;        // used if MIIM_STATE
//UINT     wID;           // used if MIIM_ID
//HMENU    hSubMenu;      // used if MIIM_SUBMENU
//HBITMAP  hbmpChecked;   // used if MIIM_CHECKMARKS
//HBITMAP  hbmpUnchecked; // used if MIIM_CHECKMARKS
//ULONG_PTR dwItemData;   // used if MIIM_DATA
//LPWSTR   dwTypeData;    // used if MIIM_TYPE (4.0) or MIIM_STRING (>4.0)
//UINT     cch;           // used if MIIM_TYPE (4.0) or MIIM_STRING (>4.0)
//HBITMAP  hbmpItem;      // used if MIIM_BITMAP


//
//#define MIIM_STATE       0x00000001
//#define MIIM_ID          0x00000002
//#define MIIM_SUBMENU     0x00000004
//#define MIIM_CHECKMARKS  0x00000008
//#define MIIM_TYPE        0x00000010
//#define MIIM_DATA        0x00000020
//#endif /* WINVER >= 0x0400 */
//
//#if(WINVER >= 0x0500)
//#define MIIM_STRING      0x00000040
//#define MIIM_BITMAP      0x00000080
//#define MIIM_FTYPE       0x00000100
//

//#define MF_STRING           0x00000000L
//#define MF_BITMAP           0x00000004L
//#define MF_SEPARATOR        0x00000800L
//
//#define MFT_STRING          MF_STRING
//#define MFT_BITMAP          MF_BITMAP
//#define MFT_SEPARATOR       MF_SEPARATOR

//enum umCOMMANDSTRUCT_TYPE
//{
//	CST_STRING			= 0x00000000,
//	CST_BITMAP			= 0x00000004,
//	CST_SEPARATOR		= 0x00000800,
//	//CST_MENUBARBREAK	= 0x00000002,
//	//CST_MENUBREAK		= 0x00000004,
//	//CST_OWNERDRAW		= 0x00000008,
//	//CST_RADIOCHECK	= 0x00000010,
//	//CST_RIGHTJUSTIFY	= 0x00000020,
//	//CST_RIGHTORDER	= 0x00000040,
//};
//
//#define MFS_GRAYED          0x00000003L
//#define MFS_DISABLED        MFS_GRAYED
//#define MFS_CHECKED         MF_CHECKED
//#define MFS_HILITE          MF_HILITE
//#define MFS_ENABLED         MF_ENABLED
//#define MFS_UNCHECKED       MF_UNCHECKED
//#define MFS_UNHILITE        MF_UNHILITE
//#define MFS_DEFAULT         MF_DEFAULT
//
enum umCOMMANDSTRUCT_STATE
{
	CSS_CHECKED			= 0x00000008L,
	CSS_DEFAULT			= 0x00001000L,
	CSS_DISABLED		= 0x00000002L,
	CSS_ENABLED			= 0x00000000L,
	CSS_GRAYED			= 0x00000003L,
	CSS_HILITE			= 0x00000080L,
	CSS_UNCHECKED		= 0x00000000L,
	CSS_UNHILITE		= 0x00000000L,
};

//
//struct COMMANDSTRUCT
//{
//	DWORD	 dwSize;
//	UINT     fMask;
//	DWORD	 fType;
//	DWORD	 dwState;	
//	//DWORD    dwFlags;
//	//LONG	 hRes;
//	int      nCommandId;
//	//int      iBitmap;
//	wchar_t* szText;
//	UINT     cch;
////	wchar_t* szKeyParam;
//	HBITMAP hBitmap;
//	INotifyCommand* pNotifyCommand;
//	LPARAM	 lParam;
//}COMMANDSTRUCT;


#define MDIIF_INDEX				0x00000001
#define MDIIF_NAME				0x00000002
#define MDIIF_TITLE				0x00000004
#define MDIIF_HICON				0x00000008
#define MDIIF_WINDOWPANEL		0x00000010
#define MDIIF_NOTIFYCOMMAND		0x00000020

struct MDIITEM
{
	DWORD dwSize;
	DWORD dwMask;
	int iItem;
	wchar_t* pwsName;
	wchar_t* pwsTitle;
	HICON hIcon;
	IWindowPanel* pWindowPanel;
	INotifyCommand* pNotifyCommand;
	LPARAM lParam;
};

// {A680C149-9511-4fe3-8322-C40FBF977E60}
extern IID IID_MainFrameCommandBars;


//
// IComFramework 消息通报和结构。
//
enum ON_COMFRAMEWORK
{
	ON_CF_QUITQUERY	= 0x0000000000000001,			// ComFramework 退出时，查询程序是否退出的消息。
};

typedef struct tagONComFrameworkQueryQuit
{
	fk::NOTIFYEVENT			nEvent;
	IComFramework*			pComFramework;
	bool					bExit;
}ONComFrameworkQueryQuit,* LPONComFrameworkQueryQuit;



b_call GetComFramework(void** ppFramework);
b_call GetMainFrames(void** pmfs);
b_call GetMainFrame(wchar_t* szId, void** ppmf);			// temp. delete.

bool WINAPI ThreadNotifyCreateStatusBar(void* pStatusBarItem, fk::LProgressNotify** pptwn);
typedef bool (WINAPI* FnFmCreateStatusBarThreadNotify)(void*, fk::LProgressNotify**);

bool WINAPI GetGenericPanels(void** ppobj);


/*
** 设置当前主框架的名字，可以通过这个名字，找到主框架变量。
** 以后会删除，不要使用。
*/
bool WINAPI MainFrameActiveOnIdle(LPCWSTR szName);


/*
**   NotifyCommand
*/
#define FK_BEGIN_NC_MAP(_class) \
HRESULT STDMETHODCALLTYPE _class::NotifyCommand(UINT codeNotify, int iCmdID, fk::ICommandItem *pcmd)\
{\
	ATLTRACE(_T("Notify command ID:%d\n"), iCmdID);

#define  FK_END_NC_MAP()\
	pcmd->put_NextCmd(VARIANT_TRUE);\
	return S_OK; \
}





#define  FK_QSTATE_ENABLE(_cmdid)\
	if(_cmdid == iCmdID)\
{\
	pCmdUI->put_Enable(VARIANT_TRUE);\
	return S_OK;\
}

#define  FK_QSTATE_DISABLE(_cmdid)\
	if(_cmdid == iCmdID)\
{\
	pCmdUI->put_Enable(VARIANT_FALSE);\
	return S_OK;\
}

#define FK_QSTATE_CMDUI(_cmdid, func)\
	if(_cmdid == iCmdID)\
{\
	func(iCmdID, pCmdUI);\
	return S_OK;\
}

#define FK_QSTATE_CMD(_cmdid, func)\
	if(_cmdid == iCmdID)\
{\
	func(pCmdUI);\
	return S_OK;\
}

#define FK_QSTATE_RANGE_FULL(idFirst, idLast, func) \
	if((iCmdID >= idFirst) && (iCmdID <= idLast)){\
	func(iCmdID, pCmdUI);\
	return S_OK;\
	}

#define END_QUERY_STATE()\
	pCmdUI->put_Enable(VARIANT_TRUE);\
	pCmdUI->put_NextCmd(VARIANT_TRUE);





#define FK_GET_COMMANDTIPS(NotifyCmdID, iCmdID, HintID, bstrTips)	\
	if (NotifyCmdID==iCmdID)\
{\
	fk::LStringw wsHint;\
	wsHint.LoadStr(g_pmodule->hRes, HintID);\
	*bstrTips = ::SysAllocString(wsHint._pwstr);\
}


class _FK_OUT_CLASS LSplitterWnd : public fk::LWindow
{
private:
	ISplitterWnd* _pSplitterWnd;

protected:
public:
	LSplitterWnd(fk::LWindowMuch* pWindowMuch);
	virtual ~LSplitterWnd(void);

	/**
	*	注册当前对象通报信息到 pReceiveObject 对象中。此函数可以重载。
	*  @param pReceiveObject 通报信息接收对象。
	*	@param dwEvent 事件值。
	*/
	virtual b_call RegsvrNotifyTo(fk::LComponent* pReceiveObject, DWORD dwEvent);			// 注册事件到那里。

	/**
	*	删除当前对象通报消息接收着。此函数可以重载。
	*   @param pReceiveObject 通报信息接收对象。
	*/
	virtual b_call UnregsvrNotify(fk::LComponent* pReceiveObject);							// 取消注册的事件。

	b_call CreateSplitterWnd(fk::LWindowMuch* pWindowMcuhParent, HWND hWnd, long nLeft, long nTop, long nRight, long nButtom);
	b_call RemoveCell(LPCWSTR lpszName, enumRemoveCell fmrc);
	b_call MoveWnd(long nLeft, long nTop, long nRight, long nButtom);
	b_call GetWnd(ULONG* phWnd);
	b_call LoadSplitterWnd(fk::LModule* pmodule, fk::HXMLCONFIG hXmlConfig, wchar_t* pszXmlPath);
	b_call CloseCell();

	b_call get_SplitterWidth(LONG* pVal);
	b_call put_SplitterWidth(LONG newVal);
	b_call UpdateLayout(void);
	//b_call get_MainSplitterCell(ISplitterCell** pVal);
	b_call get_ShowDragging(VARIANT_BOOL* pVal);
	b_call put_ShowDragging(VARIANT_BOOL newVal);
	b_call GetActiveCell(ISplitterCell** ppActiveCell, IGenericPanel** ppGenericPanel);
	b_call get_NotifyCommands(INotifyCommands** ppNotifyCommands);
	b_call SetActiveCell(VARIANT varActiveCell);
	b_call get_LeftCell(ISplitterCell** ppLeftCell);
	b_call get_RightCell(ISplitterCell** ppRightCell);
	b_call CreateSplitterItem(enumSplitterType swt,
		enumResizePos fmrp,
		enumCellType umCellType,
		long nWidth,
		ISplitterCell** ppLeft,
		ISplitterCell** ppRight,
		DWORD dwLeftPanelStyle,
		IGenericPanel** ppLeftGenericPanel,
		DWORD dwRightPanelStyle, 
		IGenericPanel** ppRightGenericPanel
		);
	b_call get_ShowWindow(VARIANT_BOOL* bShow);
	b_call put_ShowWindow(VARIANT_BOOL bShow);

	static LSplitterWnd* NewSplitterWnd(fk::LWindowMuch* pWindowMuch);
};

enum enumSplitterWnd
{
	ON_SW_INSERT_PAGE			= 0x0000000000000001,		// 添加一个页面项。
	ON_SW_REMOVE_PAGE			= 0x0000000000000002,		// 移出一个页面面。
	ON_SW_ADD_CELL				= 0x0000000000000004,		// 添加一个 CELL。
	ON_SW_REMOVE_CELL			= 0x0000000000000008,		// 移出一个 CELL。
	ON_SW_CREATE_SPLITTER_WND	= 0x0000000000000010,		// 创建一个SplitterWnd
	ON_SW_DELETE_SPLITTER_WND	= 0x0000000000000020,		// 删除一个SplitterWnd
};

typedef struct tagONSWInsertPage
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
	ISplitterCell*		pSplitterCell;
	IGenericPanel*		pGenericPanel;
}ONSWInsertPage, *LPONSWInsertPage;

typedef struct tagONSWRemovePage
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
	ISplitterCell*		pSplitterCell;
	IGenericPanel*		pGenericPanel;
}ONSWRemovePage, *LPONSWRemovePage;

typedef struct tagONSWAddCell
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
	ISplitterCell*		pSplitterCell;
}ONSWAddCell, *LPONSWAddCell;

typedef struct tagONSWRemoveCell
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
	ISplitterCell*		pSplitterCell;
}ONSWRemoveCell, *LPONSWRemoveCell;

typedef struct tagONSWCreateSplitterWnd
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
}ONSWCreateSplitterWnd, *LPONSWCreateSplitterWnd;

typedef struct tagONSWDeleteSplitterWnd
{
	fk::NOTIFYEVENT			nEvent;
	fk::LSplitterWnd*		pSplitterWnd;
}ONSWDeleteSplitterWnd, *LPONSWDeleteSplitterWnd;




_FK_END

#ifdef __cplusplus
}
#endif



#ifndef __FM_INFRAMEWORK_h__
#pragma comment(lib, "ArtFramework.lib")
#endif



#endif	//__FK_FRAMEWORKIMPL_H__
