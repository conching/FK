#ifndef __FONTS_HXX__
#define __FONTS_HXX__

#include "fk\xmlfile.hxx"



_FK_BEGIN

class fonts;

// {79C7FD37-EF24-4449-9540-55A89C755347}
static const IID IID_Font = { 0x79c7fd37, 0xef24, 0x4449, { 0x95, 0x40, 0x55, 0xa8, 0x9c, 0x75, 0x53, 0x47 } };
// {0C7E0687-2A4D-4bba-9505-D84922EF9C31}
static const IID IID_Fonts = { 0xc7e0687, 0x2a4d, 0x4bba, { 0x95, 0x5, 0xd8, 0x49, 0x22, 0xef, 0x9c, 0x31 } };

////////////////////////////////////////////////////////////////////////////////////////////////
enum enumFontPropType
{
	fptSetFontName = 0x11000001,
	fptGetFontName = 0x11000002,
	fptSetFontSize = 0x11000003,
	fptGetFontSize = 0x11000004,
};

class _FK_OUT_CLASS font : public fk::LObject
{
private:
protected:
	HFONT		    _hFont;
	LOGFONT		    _LogFont;
	int			    _iPixelsPerInch;
	fk::fonts*	    _pfonts;
	fk::HXMLNODE    _hxmlNode;
	DWORD           _dwFontMake;

	v_call DoFondUpdate(void);

	b_call SaveFontNode(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode);

public:
	virtual b_call Assign(LObject* pobject);
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual v_call clear(void);
	virtual b_call DeleteObject();

	// 这三个函数是属性调用的.
	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValueIn,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

public:
	font(fk::LObject* pParentObject);
	virtual ~font();

	v_call SetDefaultData(void);

	b_call CreateFont(void);
	b_call CreateFont(int isize);
	b_call CreateFont(LPCWSTR lpszFontName, int isize);

	b_call SetFontSize(int isize);
	i_call GetFontSize(void);

	HFONT GetHFont(void);

	b_call SetFontName(LPCWSTR lpszFontName);
	b_call GetFontName(fk::LStringw* pFontName);

	b_call SetFonts(fk::fonts* pfonts);
	b_call ReadFont(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNode);
	
	//b_call SaveFontAs(fk::LXmlFile* pxmlFile, fk::HXMLNODE hxmlNodeParent);
	b_call SaveFont(fk::LXmlFile* pxmlFile);

	static b_fncall NewFont(fk::LObject* pParentObject, LPVOID* ppobj, REFIID iid=GUID_NULL  _FK_OBJECT_POS_H2_);

public:
	enum enumFontPropMake
	{
		fpFullItem = 0x00000001,			// 项是全结构数据。
	};

	enum enumFontNotify
	{
		ON_FONT_SET_FONT_NAME   = 0x0000000000000001,
		ON_FONT_SET_FONT_SIZE   = 0x0000000000000002,
		ON_FONT_UPDATE          = 0x0000000000000004,
	};

	typedef struct tagONFONTUPDATE
	{
		fk::NOTIFYEVENT nEvent;
		fk::font* pfont;
	}ONFONTUPDATE,*LPONFONTUPDATE;
};


class _FK_OUT_CLASS fonts : public fk::LObject
{
private:
public:
	fonts(fk::LObject* pParentObject);
	virtual ~fonts(void);

	virtual b_call add(fk::font* ppfont) = 0;
	virtual b_call remove(fk::font* pfont) = 0;
	virtual b_call remove(LPCWSTR lpszObjectName) = 0;
	virtual b_call ReadXml(fk::LXmlFile* pxmlFile, LPCWSTR lpszXmlPath) = 0;
	virtual b_call ReadXml(fk::LXmlFile* pxmlFile, LPCSTR lpszXmlPath) = 0;

	virtual i_call GetCount(void) = 0;
	virtual fk::font* _call GetFont(int iitem) = 0;
	virtual fk::font* _call FindFontObjectName(LPCWSTR lpszObjectName) = 0;
	virtual b_call IsExist(LPCWSTR lpszObjectName) = 0;
	virtual b_call GetEnumData(fk::LEnumData** ppEnumData) = 0;
	virtual data_t* GetDatas(void) = 0;

	static b_fncall NewFonts(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid=GUID_NULL _FK_OBJECT_POS_H2_);
};

_FK_END


#endif
