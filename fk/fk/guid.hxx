#ifndef  __GUID_H__
#define  __GUID_H__


_FK_BEGIN

class _FK_OUT_CLASS LGuid : public LVar
{
public:
	GUID	_guid;

	LGuid();
	virtual ~LGuid();

	virtual b_call AssignBaseData(fk::LVar* pbaseData);   /* = 0; */

	virtual l_call GetLong();
	virtual b_call SetLong(long lValue, int radix);

	virtual ul_call GetULong();
	virtual b_call SetULong(ULONG ulValue, int radix);

	virtual i_call GetInt();
	virtual b_call SetInt(int iValue, int radix);

	virtual ui_call GetUInt();
	virtual b_call SetUInt(UINT uiValue, int radix);

	virtual be_call GetByte();
	virtual b_call SetByte(BYTE byte, int radix);

	virtual f_call GetFloat();
	virtual b_call SetFloat(float fValue);

	virtual d_call GetDouble();
	virtual b_call SetDouble(double dValue);

	virtual b_call SetBool(bool bValue);

	virtual b3_call GetVariantBool();
	virtual b_call SetVariantBool(VARIANT_BOOL b2Value, umBoolType boolType);

	virtual us_call GetUShort();
	virtual b_call SetUShort(USHORT usValue, int radix);

	virtual b_call SetGuid(GUID* pguid);
	virtual b_call SetGuid(LPCWSTR lpszGuid);
	virtual b_call SetGuid(LPCSTR lpszGuid);

	virtual b_call GetStringw(LStringw* ptext);
	virtual b_call AssignStringw(LStringw* ptext);

	virtual b_call GetBufw(wchar_t* pwstr, UINT uiBuf);

	virtual b_call Assignw(const wchar_t* pwstr, int uiLen=-1);
	virtual b_call Assigna(const char* pstr, int uiLen=-1);
	virtual v_call clear();

	virtual b_call IsExist();

	virtual b_call IsEmpty();

	b_call CreateGuid();

	b_call GetGuidText(fk::LVar* ptext);
	b_call GetHexText(fk::LVar* ptext, bool bracket);

	b_call GetGuidRemoveBracket(fk::LVar* ptext);


	operator GUID*() const throw()
	{
		return (GUID*)&_guid;
	}
	//GUID* operator&() const throw()
	//{
	//	return (GUID*)&_guid;
	//}
	//fk::LGuid* operator&(void) const throw()
	//{
	//	return this;
	//}
};

_FK_END


#endif
