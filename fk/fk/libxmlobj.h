#ifndef __LIBXMLOBJ_H__
#define __LIBXMLOBJ_H__

#ifndef  __LIBXML
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlstring.h>
#include <libxml/xpath.h>
#include <iconv.h>
#endif // __LIBXML


#define _XMLF_USER_AUTOPTR 1

_FK_BEGIN

#define STR_VARIANT_TRUE	"-1"
#define STR_VARIANT_FALSE	"0"

#define STR_VARIANT_T	"t"
#define STR_VARIANT_F	"f"

#define STR_FALSE		"false"
#define STR_TRUE		"true"

#pragma comment(lib, "libxml2.lib")
#pragma comment(lib, "iconv.lib")


#define APF_XMLCHAR(a) (const xmlChar*)(char*)a

typedef _xmlDoc*			HXMLCONFIG;
typedef _xmlNode*			HXMLNODE;
typedef _xmlAttr*			HXMLATTR;
typedef unsigned long		HCONFIG;

enum umConfig
{
	HCONFIG_USER		= 0x00000001,
	HCONFIG_APPLICATION	= 0x00000002,
	HCONFIG_APP_TEST	= 0x00000003,
	HCONFIG_TEST		= 0x00000004,
	//HCONFIG_FK			= 0x00000005,
};

enum enumXMLGSN
{
	umNodeCreate,				// 创建节点路径和节点。
	umNodeCreateNoExist,		// 如果节点不存在，就创建。
	umNodeErrorNoCreate,		// 不存在，不创建。	
	umNodeNoCreate,				// 不创建节点，如果节点不存在，写入错误。
};

class LXmlFile;

/*
**  打开 xml 文件，并返回xml文件对象指针。
 */
//b_fncall xmlfOpenFile(LPCWSTR szFullName, HXMLCONFIG* ppConfig);
//b_fncall xmlfCloseFile(HXMLCONFIG pConfig);

fk::FARRAY _fncall ArrayCreateFromXml(fk::HXMLCONFIG hXmlConfig, LPCSTR lpszXmlPath, LPARAM lParam, fk::FNARRAYDELETEDATA fnArrayDeleteData);

b_fncall xmlTextDataToOutData(LPCSTR lpszText, enumDataType dtDataType, void* pOutData);
b_fncall xmlDataToOutData(enumDataType dtDataType, void* pData, enumDataType dtDataTypeTo, void* pOutData);

//
// 这四个函数是一个组。
//
b_fncall	xmlfOpenFile(fk::LModule* pmodule, LPCWSTR szFileName, HXMLCONFIG* ppConfig);
b_fncall	xmlfSaveFile(LPCWSTR szFull, HXMLCONFIG hxmlConfig);
b_fncall	xmlfCloseFile(HXMLCONFIG pConfig);
b_fncall	xmlGetFileFromConfig(__in HXMLCONFIG hXmlConfig, __out LPWSTR lpszFullNameBuf256);

b_call xmlfOpenResFile(fk::LModule* pmoudle, LPCWSTR szResFileName, HXMLCONFIG* ppConfig);
b_call xmlfOpenConfigFile(fk::LModule* pmoudle, LPCWSTR szCfgFileName, HXMLCONFIG* ppConfig);

//
// 根据 szResFileName 设置的文件名称，生成绝对路径。
//
b_call xmlfBuildResFullFileW(__in fk::LModule* pmoudle, __in LPCWSTR szResFileName, __out LPWSTR lpszFullName);
#ifdef UNICODE
#define xmlfBuildResFullFile  xmlfBuildResFullFileW
#else
#define xmlfBuildResFullFile  xmlfBuildResFullFileA
#endif

b_fncall GetConfig(fk::HCONFIG hConfig, HXMLCONFIG* ppDocPtr);
b_fncall SaveConfigUserFile(void);
b_fncall SaveConfigApplication(void);
HXMLCONFIG _fncall GetConfigUser(void);
HXMLCONFIG _fncall GetConfigApplication(void);
HXMLCONFIG _fncall GetConfigTest(void);

b_fncall GetConfigUserFile(xmlDocPtr* ppDocPtr);					// 替换。
b_fncall GetConfigUserXmlFile(fk::LXmlFile** pxmlFile);
b_fncall GetConfigApplicationXmlFile(fk::LXmlFile** pxmlFile);

b_fncall GetConfigNodeVariant(fk::HCONFIG hConfig, LPCSTR lpszXmlPath, enumXMLGSN xmlgsnIn, VARIANT* pvtValue);
b_fncall ConfigSetNodeVariant(fk::HCONFIG hConfig, LPCSTR lpszXmlPath, enumXMLGSN xmlgsnIn, VARIANT* pvtValue);

// 语言格式名称是：_T("zh-CN"),
b_fncall xmlfGetDefaultLangA(xmlDocPtr pDocPtr, LPCWSTR lpLang);
b_fncall xmlfGetDefaultLangW(xmlDocPtr pDocPtr, LPWSTR lpLang);
#ifdef _UNICODE
#define xmlfGetDefaultLang	xmlfGetDefaultLangW
#else
#define xmlfGetDefaultLang  xmlfGetDefaultLangW
#endif

/**
 	锁定节点，不能进行删除节点和删除属性功能。但是可以添加属性。
 */
b_fncall xmlfNodeLock(fk::HXMLCONFIG hxmlConfig, fk::HXMLNODE hxmlNode);

/** 解锁节点，可以进行删除节点和删除属性功能。 */
b_fncall xmlfNodeUnlock(fk::HXMLCONFIG hxmlConfig, fk::HXMLNODE hxmlNode);

/*
** 根据 xml 路径返回单个接点对象。
** @
** @ retrun 返回存在的节点，或者是空节点。
*/
xmlNodePtr WINAPI xmlfGetNodeA(xmlDocPtr pxmlDoc, const char* szXPath, enumXMLGSN xmlgsnIn);
xmlNodePtr WINAPI xmlfGetNodeW(xmlDocPtr pxmlDoc, const wchar_t* szXPath, enumXMLGSN xmlgsnIn);

#define xmlfGetNodeChild(_pxmlNode)		_pxmlNode->children
#define xmlfGetNodeNext(_pxmlNode)		_pxmlNode->next
#define xmlfGetNodePrior(_pxmlNode)		_pxmlNode->prev
#define xmlfReleaseNode(_pxmlNode)      ;
#define xmlfIsElementNode(_xmlNode)		(_xmlNode->type == XML_ELEMENT_NODE)

//
// 根据节点设置和获得数据。
//
b_fncall xmlfGetNodeData(HXMLNODE xmlNode, enumDataType dtDataType, void* pDataQuote);
b_fncall xmlfSetNodeData(HXMLNODE xmlNode, enumDataType dtDataType, void* pData);

//
// 根据 HCONFIG类型和XML路径，设置和获得数据。
//
b_fncall xmlfGetNodeDataPathA(fk::HCONFIG hConfig, LPCSTR lpszXmlPath, enumXMLGSN xmlgsnIn, enumDataType dtDataType, void* pDataQuote);
b_fncall xmlfSetNodeDataPathA(fk::HCONFIG hConfig, LPCSTR lpszXmlPath, enumXMLGSN xmlgsnIn, enumDataType dtDataType, void* pData);

b_fncall xmlfGetPropDataA(const HXMLNODE xmlNode, LPCSTR szAttribName, enumDataType dtDataType, void* pDataQuote);
b_fncall xmlfGetPropDataW(const HXMLNODE xmlNode, LPCWSTR szAttribName, enumDataType dtDataType, void* pDataQuote);
b_fncall xmlfSetPropDataA(const HXMLNODE xmlNode, LPCSTR szAttribName, enumDataType dtDataType, data_t* pData);
b_fncall xmlfSetPropDataW(const HXMLNODE xmlNode, LPCWSTR szAttribName, enumDataType dtDataType, data_t* pData);

b_fncall xmlfFindProp(const HXMLNODE hxmlNode, LPCSTR lpszPropName, ::xmlAttrPtr* ppprop);

//
// 获得 hXmlNode 子节点中 lpszName 兄弟节点。
//
HXMLNODE _call xmlfFindChildSiblingNodeA(const HXMLNODE hXmlNode, LPCSTR lpszName);

//
// 获得相对路经节点。路径前面可以加 / 或者不加。
//
HXMLNODE _call xmlfGetNodeRelativePathA(HXMLNODE hXmlNode, LPCSTR szRelativePath);

//
// 获得节点路径。
//
b_fncall xmlfGetNodeFullPath(fk::HXMLNODE fxmlNode, fk::LStringa* pcXmlPath);


/*--------------------------------------------------------------------------*/

/*
** 根据配置文件修改窗口样子。
** hConfig 指定样子所在的配置文件。
** wcsXmlPath 指定 libXml 路径。
** hWndCtrl 指定需要修改的窗口名柄。
** return - 修改成功返回 true, 修改失败返回 false。
*/
b_fncall ConfigModifyWindowStyle(fk::HCONFIG hConfig, LPCSTR wcsXmlPath, HWND hWndCtrl);


xmlNodePtr WINAPI xmlfNewChild(xmlNodePtr pParentNode,
							   xmlNsPtr ns, const char* pszName, const char* pszContent);

xmlNodePtr WINAPI xmlfNewChildPathA(xmlNodePtr pParentNode, xmlNsPtr ns,
									const char* szXmlPath, const char* pszContent, enumXMLGSN xmlgsn);
xmlNodePtr WINAPI xmlfNewChildPathW(xmlNodePtr pParentNode, xmlNsPtr ns,
									const wchar_t* szXmlPath, const wchar_t* pszContent, enumXMLGSN xmlgsn);
#ifdef _UNICODE
#define xmlfNewChildPath xmlfNewChildPathW
#else
#define xmlfNewChildPath xmlfNewChildPathA
#endif


enum enumFreeNode
{
	umFreeNodeCurrent	= 0x01,		/* 当前子节点。*/
	umFreeNodeChilds	= 0x02,		/* 当前子节点下的所有子节点。*/
};

/*
** 删除设置的 XML 路径节点，支持当前节点和所有子节点。
** @param pxmlDoc  xml文档操作指针对象 xmlDocPtr.
** @param xpath xml 节点路径。
** @param xmlfNodeNexus 删除的节点类型。当前节点和所有子节点。参见 enum enumFreeNode
*/
b_fncall xmlfFreePathNode(xmlDocPtr pxmlDoc, LPCSTR xpath, enumFreeNode xmlfNodeNexus);

/*
** 从XML文档中删除一个节点，如果删除的节点存在下一个节点，返回下一个节点，如果不存在返回空。
** 如果删除的节点存在下一个节点，返回下一个节点，如果不存在返回空。
*/
b_fncall xmlfFreeNode(xmlNodePtr pxmlFreeNode, enumFreeNode xmlfNodeNexus);





/*--------------------------------------------------------------------------*/
/* 以下函数逐渐废除。
 */
b_call StrToVariantBoolA(LPCSTR szBStr, VARIANT_BOOL *pbValue);


/*
** 根据 iIndex 返回节点，如果不存在返回空。
** iIndex - 节点索引。
*/
xmlNodePtr WINAPI xmlfFindNodeIndex(const xmlNodePtr pxmlParentNode, int iIndex);


/*
** 在子节点中查找一个值，并返回值的节点,如果没有找到返回空。
** pxmlNode - 指定节点对象。
** 指定查找的值。目前支持 VT_I4 VT_I2 VT_I8 VT_BSTR
*/
xmlNodePtr WINAPI xmlfSearchNodeValue(xmlNodePtr pxmlNode, const VARIANT* pvarValue);


/*
** 根据路径查找一个节点的值，并返回节点，如果没有查找到返回空。
*/
xmlNodePtr WINAPI xmlfSearchPathNodeValue(const xmlDocPtr pxmlDoc, const VARIANTARG* pvarPath, const VARIANTARG* pvarValue);


/*
** 根据 xml 路径返回接点对列表。
** @param pxmlDoc xmlDocPtr对象。</param>
** @param xpath  xml路径，格式：/config/keyword/items</param>
** @return 返回 xmlXPathObjectPtr 对象， 调用xmlXPathFreeObject方法释放。
*/
xmlXPathObjectPtr WINAPI xmlfGetSelectNodeXPathA(xmlDocPtr pxmlDoc, const char* xpath, enumXMLGSN xmlgsnIn);
xmlXPathObjectPtr WINAPI xmlfGetSelectNodeXPathW(xmlDocPtr pxmlDoc, const wchar_t* xpath, enumXMLGSN xmlgsnIn);
#ifdef _UNICODE
#define xmlfGetSelectNodeXPath xmlfGetSelectNodeXPathW
#else
#define xmlfGetSelectNodeXPath xmlfGetSelectNodeXPathA
#endif


b_fncall fmSupportVT(const VARTYPE varType, const VARTYPE* pVarTypeArray, const unsigned int iArrayCount);


xmlAttrPtr WINAPI xmlfSetPropVariantBool(const xmlNodePtr xmlNode, LPCSTR szAttribName, VARIANT_BOOL bValue);
b_fncall xmlfGetPropVariantBool(const xmlNodePtr xmlNode, LPCSTR szAttribName, VARIANT_BOOL* pbValue);




b_fncall xmlfGetPropVariantW(const xmlNodePtr xmlNode, LPCSTR szAttribName, VARIANT* pvarValue);
xmlAttrPtr WINAPI xmlfSetPropVariantW(const xmlNodePtr xmlNode, LPCSTR szAttribName, const VARIANT* pvarValue);
#ifdef _UNICODE
#define xmlfGetPropVariant xmlfGetPropVariantW
#define xmlfSetPropVariant xmlfSetPropVariantW
#else
#define xmlfGetPropVariant xmlfGetPropVariantW
#define xmlfSetPropVariant xmlfSetPropVariantW
#endif



b_fncall xmlfGetNodeValue(const xmlNodePtr xmlNode, VARIANT* pNodeValue);
b_fncall xmlfSetNodeValue(const xmlNodePtr xmlNode, const VARIANT* pNodeValue);

b_fncall xmlfSetNodeRect(xmlNodePtr xmlNode, LPCRECT lpRect);
b_fncall xmlfGetNodeRect(xmlNodePtr xmlNode, LPRECT lpRect);

b_fncall xmlfGetValue(xmlDocPtr pxmldoc, LPCSTR szPath, VARIANT* pNodeValue, enumXMLGSN xmlGSN);
b_fncall xmlfSetValue(xmlDocPtr pxmldoc, LPCSTR szPath, const VARIANT* pNodeValue, enumXMLGSN xmlGSN);


/*
** 以上函数逐渐放弃。
*/


_FK_END


#endif	//__LIBXMLOBJ_H__

