#ifndef __map_h__
#define __map_h__



_FK_BEGIN

//
// FMAP 预览。
//


typedef void*  FMAP;

// map<int, int>   map<int, string>  map<int, pObject>
typedef bool (_fncall* FNARRAYDELETEDATA)(FARRAY hArray, data_t* ppArray, uint iCount);


/*
/info 创建一个map。
param:
	/p uiInitCount	- 初始化数据项大小。
	/p uiNewCount	- 每次添加数据项大小。
	/p uiMapType	- 默认支持的数据项类型。
	/p lpfnFreeMapItem	- 释放数据项函数指针。默认 uiMapType 类型有释放数据项方法，也可以自己定义。

/return:
	返回 FMAP 句柄。
*/

FMAP _fncall MapCreate(UINT uiInitCount, UINT uiNewCount, fk::FNARRAYDELETEDATA lpfnFreeMapItem);

b_fncall MapDestroy(FMAP hArray);
b_fncall MapAdd(FMAP fMap, void* pItem);
b_fncall MapDeleteItem(FMAP fMap, void* pItem);
b_fncall MapDeleteIndex(FMAP fMap, int iItem);
b_fncall MapDeleteFull(FMAP fMap);
i_fncall MapGetCount(FMAP fMap);
data_t* _fncall MapGetDatas(FMAP fMap);
data_t* _fncall MapGetItem(FMAP fMap, int iItem);
b_fncall MapGetEnumData(FMAP fMap, fk::LEnumData* ppEnumData);




typedef struct tagMAPINTWCHAR
{
	int			iKey;
	wchar_t*	pText;
}MAPINTWCHAR,*LPMAPINTWCHAR;
LPMAPINTWCHAR _fncall MapMallocIntWChar(FMAP fmap,int iKey, LPCWSTR pszKey, int iLen);
b_fncall MapFreeIntWChar(FMAP fmap, LPMAPINTWCHAR lpMapIntWChar);


typedef struct tagMAPINTPTR
{
	INT		iKey;
	void*	pObj;
}MAPINTPTR,*LPMAPINTPTR;
#define MapMallocIntPtr(pMapIntPtr, pInt, pvObj)\
		pMapIntPtr = (fk::LPMAPINTPTR)::malloc(sizeof(fk::MAPINTPTR));\
		pMapIntPtr->iKey = iKey;\
		pMapIntPtr->pObj = pvObj;

#define MapFreeIntPtr(pMapIntPtr)	free(pMapIntPtr);
vp_fncall MapFindIntPtr(FMAP fMap, int iKey);


typedef struct tagMAPPTRUINT
{
	void*	pObject;
	UINT	uiValue;
	LPARAM	lpParam;
}MAPPTRUINT,*LPMAPPTRUINT;
#define fk_MapAddPtrUInt(fMap, pMapPtrInt, pObject, uiValue, lParam) \
	pMapPtrInt			= (fk::LPMAPPTRUINT)::malloc(sizeof(fk::MAPPTRUINT));\
	pMapPtrInt->uiValue = uiValue;\
	pMapPtrInt->pObject = pObject;\
	pMapPtrInt->lParam	= lParam;\
	fk::MapAdd(fMap, (void*)pMapPtrInt);

#define fk_MapFreePtrInt(pMapPtrInt)		free(pMapPtrInt);
vp_fncall MapFindPtrUInt(FMAP fMap, void* vpObject);


typedef struct tagMAPWPTR
{
	LPWSTR		lpszKey;
	void*		pobject;
}MAP_WPTR, *LPMAP_WPTR;

LPMAP_WPTR _fncall MapMallocWPtr(FMAP fMap, LPCWSTR pszKey, int iLen, void* pObj);
b_fncall MapFreeWPtr(FMAP fMap, LPMAP_WPTR lpMapWPtr);
//vp_fncall MapFindWPtr(FMAP fmap, LPCWSTR lpszKey);
fk::LPMAP_WPTR _fncall MapFindWPtr(FMAP fMap, LPCWSTR pszKey);


typedef struct tagMAP_LSTRINGW
{
	fk::LStringw sKey;
	fk::LStringw sText;
}MAP_LSTRINGW, *LPMAP_LSTRINGW;
fk::LPMAP_LSTRINGW _fncall MapStringwMalloc(FMAP fMap, LPCWSTR lpszkey, LPCWSTR lpsztext);
b_fncall MapStringwFree(FMAP fMap, fk::LPMAP_LSTRINGW lpMapStringw);
b_fncall MapStringwFreeKey(FMAP fMap, LPCWSTR lpszkey);
fk::LPMAP_LSTRINGW _fncall MapStringwFindKey(FMAP fMap, LPCWSTR pszKey);
b_fncall MapStringwDeleteData(FARRAY hArray, data_t* ppArray, uint iCount);


typedef struct tagMAPCHARPTR
{
	char*	pszKey;
	void*	pObj;
}MAPCHARPTR, *LPMAPCHARPTR;
LPMAPCHARPTR _fncall MapMallocCharPtr(LPCSTR pszKey, int iLen, void* pObj);
b_fncall MapFreeCharPtr(LPMAPCHARPTR lpMapCharPtr);
vp_fncall MapFindCharPtr(FMAP fMap, char* pszKey);


typedef struct tagMAPDWORDPTR
{
	DWORD	dwdey;
	void*	pobject;
}MAPDWORDPTR, *LPMAPDWORDPTR;
data_t* _fncall MapFindDWordPtrKey(FMAP fmap, DWORD dwkey);
data_t* _fncall MapFindDWordPtrObject(FMAP fmap, void* pobject);


_FK_END


#endif // __map_h__
