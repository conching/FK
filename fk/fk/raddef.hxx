#ifndef __XML_CONTROL_H__
#define __XML_CONTROL_H__

#include "fk\xmldef.hxx"


_FK_BEGIN

static char*  XMLF_CONTROL_TEMPLATE_PATH = "//config/ControlTemplates";


class CTRLPROPITEM
{
public:
	DWORD dwSize;
	DWORD dwMake;
	fk::LStringa sPropName;
	fk::enumDataType umDataType;
	//fk::LVar* pBaseData;
	void* pNodeValue;

	_FK_DBGCLASSINFO_;

	static CTRLPROPITEM* _fncall NewCtrlPropItem();

}; //CTRLPROPITEM,*LPCTRLPROPITEM;

typedef CTRLPROPITEM* LPCTRLPROPITEM;

enum enumPROPID
{
	PROPID_PROPERTIES_BASE_VALUE = 0,	// 这个是控件通报项的属性ID，这个值什么也不操作，直接返回。

	PROPID_MAKE			=	1000,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_STYLEEX		=	1001,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_CLASS_NAME	=	1002,
	PROPID_CAPTION		=	1003,
	PROPID_STYLE		=	1004,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_RECT			=	1005,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_CTRLID		=	1006,
	PROPID_FONT			=	1007,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_GLINK		=	1008,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。

	PROPID_OBJ_NAME		=	1009,		// PropIDChild="0" 设置新的值，PropIDChild="1" 设置单个值。
	PROPID_TEXT			=	1010,
	PROPID_FK_STYLE		=	1011,				// FK 自定义的窗口风格。
	PROPID_OBJECT_MAKE			=	1012,		// 自定义对象风格。
	PROPID_CONTEXTMENU_PARENT	=	1013,		// FK 自定义的窗口风格,子类化当前窗口，WM_CONTEXTMENU消息发送到父窗口。

	// ofUpdateVariable
	PROPID_UPDATE_VARIABLE		=	1014,		// 是否生成变量名称。

	PROPID_DIALOG_BORDER	= 1508,
	PROPID_DIALOG_STYLE		= 1520,


	// 3000 - 3200		table 对象 ID
	PROPID_TABLE_FILE	=	3000,
	PROPID_TABLE_ACTIVE	=	3001,
	PROPID_TABLE_EDIT	=	3002,
	PROPID_TABLE_SET_DATA_CONNECTION	=	3003,
};

enum enumPropIDConfigMgr
{
    PROPID_CONFIGMGR_ = 20000,
};

enum enumPROPIDCHILD
{
	PIDC_SET	= 0,
	PIDC_ADD	= 1,
	PIDC_REMOVE = 2,
};

enum enumPROP_ID_CHILD_RECT
{
	PIDC_RECT_SET			= 0,
	PIDC_RECT_LEFT_OPERATOR	= 1,
	PIDC_RECT_TOP_OPERATOR	= 2,
	PIDC_RECT_RIGHT_OPERATOR= 3,
	PIDC_RECT_BOTTOM_OPERATOR= 4,

	PIDC_RECT_SET_LEFT	= 5,
	PIDC_RECT_SET_TOP	= 6,
	PIDC_RECT_SET_RIGHT = 7,
	PIDC_RECT_SET_BOTTOM= 8,
};


enum enumPROPID_VersionStatic
{
	//UMVSF_SET_RUL	= 0x1,
	PROPID_SHOW_POS	= 4000,
	PROPID_RUL_NAME = 4001,
};

enum enumPROPID_VersionItem
{
	PROPID_CHILD_SET_ONE = 0x1,
	PROPID_CHILD_SET_TWO = 0x2,
	PROPID_CHILD_SET_THREE = 0x3,
	PROPID_CHILD_SET_FOUR = 0x4,
};

enum enumPROPID_CHILD_SETGET
{
	PROPID_CHILD_SET = 1,			// 设置设置数据。
	PROPID_CHILD_GET = 2,			// 获得设置数据。
};

//enum enumSTDCONTROL
//{
//	SCF_USER_CTRLID = 0x00000001,					// 控件可以不先创建，而是在调用 windows.GetWindow 方法的时候创建。
//};
enum enumXUIPROPSERROR__
{
	XPE_GES_EXISTOPTION				= 0x00000001,			// 属性是必写项。
	XPE_GES_OPTION					= 0x00000002,			// 属性是可选项。
	XPE_GES_EXISTOPTION_VALUE_NULL	= 0x00000004,			// 属性存在，但是没有写值。

	XPE_GES_EXISTOPTION_EXPLAIN		= 0x00000010,			// 属性是必写项，在前面添加一段文字："必写属性错误项："。
	XPE_GES_OPTION_EXPLAIN			= 0x00000020,			// 属性是可选项，在前面添加一段文字："可写属性错误项："。

	XPE_GES_LINE					= 0x00000040,			// 换行
	XPE_GES_APART					= 0x00000080,			// 属性项之间用什么字符分开。
	XPE_GES_ALL = XPE_GES_EXISTOPTION | XPE_GES_OPTION | XPE_GES_EXISTOPTION_VALUE_NULL | XPE_GES_EXISTOPTION_EXPLAIN | XPE_GES_OPTION_EXPLAIN | XPE_GES_LINE | XPE_GES_APART,
};

class _FK_OUT_CLASS LCtrlPropsError : public fk::LError
{
private:
	fk::FARRAY _fArray;

protected:
public:
	LCtrlPropsError(void);
	virtual ~LCtrlPropsError(void);

	virtual v_fncall ShowErrorBox(HWND hWndParent, UINT uType);

	b_call AddErrorXmlPropItem(fk::LPCTRLPROPITEM lpXmlPropItem);
	b_call RemovePropName(LPCSTR lpszPropName);
	b_call GetErrorString(fk::LStringw* pString, LPWSTR lpszApart, DWORD dwXPEGES);
	v_call clear(void);
	i_call GetCount(void);
	b_fncall ArraySetDeleteDataProc2(fk::FNARRAYDELETEDATA fnArrayDeleteData);

	// 返回错误的属性项中，是否包含这两种属性类型，
	dw_call GetErrorMake(void);					// XPE_GES_EXISTOPTION    XPE_GES_OPTION
};

// 这个不是真实的数据结构，是用来循环读每一个 XML 属性用的，数据需要设置。
// 常用属性，是为了快速读属性数据用的。
class _FK_OUT_CLASS LXmlPropPtrItem : public fk::LObject
{
private:
public:
	LXmlPropPtrItem(void);
	virtual ~LXmlPropPtrItem(void);

	DWORD dwsize;					// 结构大小。
	DWORD dwMake;					// 结构标记。
	DWORD dwItemMask;				// 每一个属性项的标记。
	DWORD* pNodeValue;				// 每一个属性的地址。
	DWORD* dwPropName;				// 每一个属性项的属性名称。
	fk::enumDataType umdt;			// 每一个属性项的数据类型。

	static LXmlPropPtrItem* _call NewXmlPropPtrItem(void);
};

typedef struct tagREADXULITEM
{
	DWORD dwsize;					// 结构大小。
	DWORD dwMake;					// 结构标记。
	DWORD dwItemMask;				// 每一个属性项的标记。
	DWORD* pNodeValue;				// 每一个属性的地址。
	DWORD* dwPropName;				// 每一个属性项的属性名称。
	fk::enumDataType umdt;			// 每一个属性项的数据类型。
}READXULITEM, *LPREADXULITEM;

class _FK_OUT_CLASS LStdControlProps : public fk::LComponent
{
private:

public:
	LStdControlProps(fk::LObject* pParentObject);
	virtual ~LStdControlProps();
	DWORD dwSize;
	DWORD dwMask;
	DWORD dwConrolMask;							//0

	DWORD dwStyleEx;
	char szClassName[FK_XML_CLASSNAME_LNG];		// 这里用 char 定义，因为是直接从 xml 文件中读出来的。
	fk::LStringw sCaption;
	DWORD dwStyle;
	RECT rcObject;
	DWORD uiCtrlID;

	fk::LStringw sObj;							// [FK_XML_CLASSNAME_LNG] 这里用 char 定义，因为是直接从 xml 文件中读出来的。
	DWORD dwObjectMake;							// LObject::ObjectMake;

	HXMLNODE hXmlNode;							// 数据所在节点。

	DWORD dwItemCount;							// 结构项的数，比 FK_READXULITEM_COUNT 小。
	fk::LPREADXULITEM _pReadXulItems;			//[FK_READXULITEM_COUNT];

	int iItemObject;							//

	virtual data_t* _call GetDatas(void) = 0;
	virtual fk::LXmlPropPtrItem* _call GetItem(int iitem) = 0;
	virtual i_call GetCount(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** pEnumData) = 0;

	virtual v_call clear(void) = 0;
	virtual enumReturn _call ReadData(fk::LVxmlFile* pvxmlFile, fk::HXMLNODE hXmlNodeItem, fk::LCtrlPropsError* pxpe) = 0;

	static LStdControlProps*  NewStdControlProps(fk::LObject* pParentObject);
};

// 根据变量名称，去掉变量名称前面的 _ 线和小写字符，并返回后面的字符串。
// 例如：_pInfoName  返回 InfoName.
wp_fncall FormVarNameGetCaptionW(LPCWSTR lpszVarName);
cp_fncall FormVarNameGetCaptionA(LPCSTR lpszVarName);

#ifdef UNICODE
#define  FormVarNameGetCaption		FormVarNameGetCaptionW
#else
#define  FormVarNameGetCaption		FormVarNameGetCaptionA
#endif



class _FK_OUT_CLASS LStdObjectProps : public fk::LComponent
{
private:
public:
	LStdObjectProps(fk::LObject* pParentObject);
	virtual ~LStdObjectProps(void);

	DWORD			dwMask;
	fk::LStringa	_cClassName;
	fk::LStringa	_cObjectName;
	fk::LStringa	_cfrl;

	virtual data_t* _call GetDatas(void) = 0;
	virtual fk::LXmlPropPtrItem* _call GetItem(int iitem) = 0;
	virtual i_call GetCount(void) = 0;
	virtual b_call GetEnumData(fk::LEnumData** pEnumData) = 0;

	virtual v_call clear(void) = 0;
	virtual enumReturn _call ReadData(fk::LVxmlFile* pvxmlFile, fk::HXMLNODE hXmlNodeItem, fk::LCtrlPropsError* pxpe) = 0;

	static LStdObjectProps* _call NewStdObjectProps(fk::LObject* pParentObject);
};


_FK_END


#endif



