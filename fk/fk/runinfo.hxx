#ifndef __RunInfo_hxx__
#define __RunInfo_hxx__


#ifdef __FK_VC__
#include <atldef.h>
#include <comdef.h>
#include <stdio.h>
#include <tchar.h>
#endif // __FK_VC__


#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  /* __cplusplus */


_FK_BEGIN

typedef void* HRUNINFOS;

class _FK_OUT_CLASS LRunInfoItem : public fk::LObject
{
private:
protected:
public:
	enum enumTraceExecute
	{
		TEF_FILE	= 0x00000001,
		TEF_FUNCTION= 0x00000002,
		TEF_LINE	= 0x00000004,
		TEF_ALL		= 0x00000007,
	};

public:
	LRunInfoItem(fk::LObject* pParentObject);
	virtual ~LRunInfoItem(void);

	LStringw _sRunInfo;
	LStringw _sFile;
	LStringw _sFun;
	LStringw _sRunEndInfo;
	int			_iLine;
	int			iKey;
	FTEXT		_hLogFile;
	bool			bRunSuccess;
	HRUNINFOS	hFunInfos;

	v_call WriteRunInfosItem(HRUNINFOS hFunInfos, wchar_t* szLRunInfo, wchar_t* szFun, wchar_t* szFile, int iLine);

	virtual v_call TraceExecute(DWORD dwFormat=TEF_ALL);
	static b_fncall NewRunInfoItem(fk::LObject* powner, LPVOID* ppobj, REFIID riid=GUID_NULL  _FK_OBJECT_POS_H2_);
};

FRUNINFO _fncall RunInfoCreate(fk::FTEXT ftext);
b_fncall RunInfoDestroy(fk::FRUNINFO fRunInfo);
b_fncall RunInfoAddItem(fk::FRUNINFO fRunInfo, fk::LRunInfoItem* lpRunInfoItem);
fk::LRunInfoItem* _fncall RunInfoFindItem(fk::FRUNINFO fRunInfo, int iKey);
b_fncall RunInfoRemoveItemKey(fk::FRUNINFO fRunInfo, int iKey);
b_fncall RunInfoRemoveItemPtr(fk::FRUNINFO fRunInfo, fk::LRunInfoItem* lpRunInfoItem);


class _FK_OUT_CLASS LRunInfo : public fk::LObject
{
private:
	fk::LRunInfoItem* _pRunInfosItem;

public:
	LRunInfo(fk::LObject* pParentObject, HRUNINFOS hFunInfos, FTEXT ftext, wchar_t* szRunInfo, wchar_t* szFun, wchar_t* szFile, int iLine);
	LRunInfo(fk::LObject* pParentObject);
	virtual ~LRunInfo(void);

	int OutInitInfo(HRUNINFOS hFunInfos, FTEXT ftext, wchar_t* szRunInfo, wchar_t* szFun, wchar_t* szFile, int iLine, ...);
	void RunSuccess();
	void RunInfo2(const wchar_t* psInfo);
	void Attach(LRunInfo* pRunInfo);
};

#ifdef _DEBUG
#define FK_RUN_NULL(ftext, szRunInfo, ...)	fk::LRunInfo __RunInfo(NULL);\
	__RunInfo.OutInitInfo(NULL, ftext, szRunInfo, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__, __VA_ARGS__);

#define FK_RUN(ftext, szRunInfo, ...)	fk::LRunInfo __RunInfo(this);\
	__RunInfo.OutInitInfo(NULL, ftext, szRunInfo, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__, __VA_ARGS__);

#define  FK_RUN_POS(_MODULE, GOTO1, GOTO2, GOTO3)	fk::LRunInfo __runInfo(NULL); \
									//__runInfo(_MODULE, GOTO1, GOTO2, GOTO3);

#define FK_RUN_SUCCESS()				__RunInfo.RunSuccess();
#define FK_RUN_INFO(szInfo)				__RunInfo.RunInfo2(szInfo);
#else
#define FK_RUN_NULL(ftext, szRunInfo, ...)
#define FK_RUN_NULL(ftext, szRunInfo, ...)
#define FK_RUN_SUCCESS()
#define FK_RUN_INFO(szInfo)
#endif



#ifndef __FM_TRACELOG
#pragma comment(lib, "TraceLog.lib")
#endif /* __FM_USER_TRACELOG */


_FK_END





#ifdef __cplusplus
}
#endif



#endif	// __Trace_h__

