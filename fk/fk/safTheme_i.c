

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:24 2022
 */
/* Compiler settings for .\safTheme.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_ITheme,0x8BC7DB12,0x2911,0x4B05,0x87,0x14,0x25,0xAC,0xC9,0x4E,0x95,0xB0);


MIDL_DEFINE_GUID(IID, LIBID_safThemeLib,0x9E86AA73,0xD8DA,0x4FEE,0x9C,0xE5,0x8B,0x8D,0xD5,0x97,0xA3,0x6C);


MIDL_DEFINE_GUID(IID, DIID__IThemeEvents,0xBA46F095,0x6BC6,0x4FD0,0xB0,0x51,0x4E,0xDB,0x0B,0xB7,0xDE,0xD1);


MIDL_DEFINE_GUID(CLSID, CLSID_Theme,0xADCAE1F5,0x7EB1,0x492E,0x89,0xC9,0xB1,0xB8,0xB2,0x73,0xD0,0x9B);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



