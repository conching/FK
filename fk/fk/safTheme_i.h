

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:24 2022
 */
/* Compiler settings for .\safTheme.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __safTheme_i_h__
#define __safTheme_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ITheme_FWD_DEFINED__
#define __ITheme_FWD_DEFINED__
typedef interface ITheme ITheme;
#endif 	/* __ITheme_FWD_DEFINED__ */


#ifndef ___IThemeEvents_FWD_DEFINED__
#define ___IThemeEvents_FWD_DEFINED__
typedef interface _IThemeEvents _IThemeEvents;
#endif 	/* ___IThemeEvents_FWD_DEFINED__ */


#ifndef __Theme_FWD_DEFINED__
#define __Theme_FWD_DEFINED__

#ifdef __cplusplus
typedef class Theme Theme;
#else
typedef struct Theme Theme;
#endif /* __cplusplus */

#endif 	/* __Theme_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_safTheme_0000 */
/* [local] */ 

const int WS_MY_AddressFoldeerBar_Draw=0x00004000;


extern RPC_IF_HANDLE __MIDL_itf_safTheme_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_safTheme_0000_v0_0_s_ifspec;

#ifndef __ITheme_INTERFACE_DEFINED__
#define __ITheme_INTERFACE_DEFINED__

/* interface ITheme */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITheme;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8BC7DB12-2911-4B05-8714-25ACC94E95B0")
    ITheme : public IDispatch
    {
    public:
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ThemeName( 
            /* [retval][out] */ BSTR *bstrClassName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartTheme( 
            BSTR strThemeName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CloseTheme( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenMenuBar( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MenuBar( 
            /* [retval][out] */ VARIANT_BOOL *pVal) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_MenuBar( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenCommandBar( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBar( 
            /* [retval][out] */ VARIANT_BOOL *pState) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_CommandBar( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenCommandBars( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_CommandBars( 
            /* [retval][out] */ VARIANT_BOOL *pState) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_CommandBars( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenMDITab( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_MDITabCtrl( 
            /* [retval][out] */ VARIANT_BOOL *pState) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_MDITabCtrl( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenFrameTabCtrl( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_FrameTabCtrl( 
            /* [retval][out] */ VARIANT_BOOL *pState) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_FrameTabCtrl( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_ThemePath( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_ThemePath( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenSplitterWindow( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE DoSetup( 
            HWND hWnd) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsTheme( 
            BSTR strTheme) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenHeaderCtrl( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CloseHeaderCtrl( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_HeaderCtrl( 
            /* [retval][out] */ VARIANT_BOOL *pState) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_HeaderCtrl( 
            /* [in] */ VARIANT_BOOL newState) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE StartDefaultTheme( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE OpenAddressFolderBar( 
            HWND hWndCtrl,
            VARIANT_BOOL bState) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThemeVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITheme * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITheme * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITheme * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITheme * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITheme * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITheme * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITheme * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ThemeName )( 
            ITheme * This,
            /* [retval][out] */ BSTR *bstrClassName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartTheme )( 
            ITheme * This,
            BSTR strThemeName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CloseTheme )( 
            ITheme * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenMenuBar )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MenuBar )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pVal);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MenuBar )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenCommandBar )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBar )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pState);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CommandBar )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenCommandBars )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_CommandBars )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pState);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_CommandBars )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenMDITab )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_MDITabCtrl )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pState);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_MDITabCtrl )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenFrameTabCtrl )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_FrameTabCtrl )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pState);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_FrameTabCtrl )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ThemePath )( 
            ITheme * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ThemePath )( 
            ITheme * This,
            /* [in] */ BSTR newVal);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenSplitterWindow )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *DoSetup )( 
            ITheme * This,
            HWND hWnd);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsTheme )( 
            ITheme * This,
            BSTR strTheme);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenHeaderCtrl )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CloseHeaderCtrl )( 
            ITheme * This,
            HWND hWndCtrl);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_HeaderCtrl )( 
            ITheme * This,
            /* [retval][out] */ VARIANT_BOOL *pState);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_HeaderCtrl )( 
            ITheme * This,
            /* [in] */ VARIANT_BOOL newState);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *StartDefaultTheme )( 
            ITheme * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *OpenAddressFolderBar )( 
            ITheme * This,
            HWND hWndCtrl,
            VARIANT_BOOL bState);
        
        END_INTERFACE
    } IThemeVtbl;

    interface ITheme
    {
        CONST_VTBL struct IThemeVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITheme_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITheme_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITheme_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITheme_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITheme_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITheme_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITheme_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITheme_get_ThemeName(This,bstrClassName)	\
    (This)->lpVtbl -> get_ThemeName(This,bstrClassName)

#define ITheme_StartTheme(This,strThemeName)	\
    (This)->lpVtbl -> StartTheme(This,strThemeName)

#define ITheme_CloseTheme(This)	\
    (This)->lpVtbl -> CloseTheme(This)

#define ITheme_OpenMenuBar(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenMenuBar(This,hWndCtrl,bState)

#define ITheme_get_MenuBar(This,pVal)	\
    (This)->lpVtbl -> get_MenuBar(This,pVal)

#define ITheme_put_MenuBar(This,newState)	\
    (This)->lpVtbl -> put_MenuBar(This,newState)

#define ITheme_OpenCommandBar(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenCommandBar(This,hWndCtrl,bState)

#define ITheme_get_CommandBar(This,pState)	\
    (This)->lpVtbl -> get_CommandBar(This,pState)

#define ITheme_put_CommandBar(This,newState)	\
    (This)->lpVtbl -> put_CommandBar(This,newState)

#define ITheme_OpenCommandBars(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenCommandBars(This,hWndCtrl,bState)

#define ITheme_get_CommandBars(This,pState)	\
    (This)->lpVtbl -> get_CommandBars(This,pState)

#define ITheme_put_CommandBars(This,newState)	\
    (This)->lpVtbl -> put_CommandBars(This,newState)

#define ITheme_OpenMDITab(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenMDITab(This,hWndCtrl,bState)

#define ITheme_get_MDITabCtrl(This,pState)	\
    (This)->lpVtbl -> get_MDITabCtrl(This,pState)

#define ITheme_put_MDITabCtrl(This,newState)	\
    (This)->lpVtbl -> put_MDITabCtrl(This,newState)

#define ITheme_OpenFrameTabCtrl(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenFrameTabCtrl(This,hWndCtrl,bState)

#define ITheme_get_FrameTabCtrl(This,pState)	\
    (This)->lpVtbl -> get_FrameTabCtrl(This,pState)

#define ITheme_put_FrameTabCtrl(This,newState)	\
    (This)->lpVtbl -> put_FrameTabCtrl(This,newState)

#define ITheme_get_ThemePath(This,pVal)	\
    (This)->lpVtbl -> get_ThemePath(This,pVal)

#define ITheme_put_ThemePath(This,newVal)	\
    (This)->lpVtbl -> put_ThemePath(This,newVal)

#define ITheme_OpenSplitterWindow(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenSplitterWindow(This,hWndCtrl,bState)

#define ITheme_DoSetup(This,hWnd)	\
    (This)->lpVtbl -> DoSetup(This,hWnd)

#define ITheme_IsTheme(This,strTheme)	\
    (This)->lpVtbl -> IsTheme(This,strTheme)

#define ITheme_OpenHeaderCtrl(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenHeaderCtrl(This,hWndCtrl,bState)

#define ITheme_CloseHeaderCtrl(This,hWndCtrl)	\
    (This)->lpVtbl -> CloseHeaderCtrl(This,hWndCtrl)

#define ITheme_get_HeaderCtrl(This,pState)	\
    (This)->lpVtbl -> get_HeaderCtrl(This,pState)

#define ITheme_put_HeaderCtrl(This,newState)	\
    (This)->lpVtbl -> put_HeaderCtrl(This,newState)

#define ITheme_StartDefaultTheme(This)	\
    (This)->lpVtbl -> StartDefaultTheme(This)

#define ITheme_OpenAddressFolderBar(This,hWndCtrl,bState)	\
    (This)->lpVtbl -> OpenAddressFolderBar(This,hWndCtrl,bState)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_ThemeName_Proxy( 
    ITheme * This,
    /* [retval][out] */ BSTR *bstrClassName);


void __RPC_STUB ITheme_get_ThemeName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITheme_StartTheme_Proxy( 
    ITheme * This,
    BSTR strThemeName);


void __RPC_STUB ITheme_StartTheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITheme_CloseTheme_Proxy( 
    ITheme * This);


void __RPC_STUB ITheme_CloseTheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenMenuBar_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenMenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_MenuBar_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pVal);


void __RPC_STUB ITheme_get_MenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_MenuBar_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_MenuBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenCommandBar_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenCommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_CommandBar_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pState);


void __RPC_STUB ITheme_get_CommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_CommandBar_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_CommandBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenCommandBars_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenCommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_CommandBars_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pState);


void __RPC_STUB ITheme_get_CommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_CommandBars_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_CommandBars_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenMDITab_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenMDITab_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_MDITabCtrl_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pState);


void __RPC_STUB ITheme_get_MDITabCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_MDITabCtrl_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_MDITabCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenFrameTabCtrl_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenFrameTabCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_FrameTabCtrl_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pState);


void __RPC_STUB ITheme_get_FrameTabCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_FrameTabCtrl_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_FrameTabCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_ThemePath_Proxy( 
    ITheme * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB ITheme_get_ThemePath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_ThemePath_Proxy( 
    ITheme * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB ITheme_put_ThemePath_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenSplitterWindow_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenSplitterWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITheme_DoSetup_Proxy( 
    ITheme * This,
    HWND hWnd);


void __RPC_STUB ITheme_DoSetup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITheme_IsTheme_Proxy( 
    ITheme * This,
    BSTR strTheme);


void __RPC_STUB ITheme_IsTheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenHeaderCtrl_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenHeaderCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_CloseHeaderCtrl_Proxy( 
    ITheme * This,
    HWND hWndCtrl);


void __RPC_STUB ITheme_CloseHeaderCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE ITheme_get_HeaderCtrl_Proxy( 
    ITheme * This,
    /* [retval][out] */ VARIANT_BOOL *pState);


void __RPC_STUB ITheme_get_HeaderCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE ITheme_put_HeaderCtrl_Proxy( 
    ITheme * This,
    /* [in] */ VARIANT_BOOL newState);


void __RPC_STUB ITheme_put_HeaderCtrl_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ITheme_StartDefaultTheme_Proxy( 
    ITheme * This);


void __RPC_STUB ITheme_StartDefaultTheme_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITheme_OpenAddressFolderBar_Proxy( 
    ITheme * This,
    HWND hWndCtrl,
    VARIANT_BOOL bState);


void __RPC_STUB ITheme_OpenAddressFolderBar_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITheme_INTERFACE_DEFINED__ */



#ifndef __safThemeLib_LIBRARY_DEFINED__
#define __safThemeLib_LIBRARY_DEFINED__

/* library safThemeLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_safThemeLib;

#ifndef ___IThemeEvents_DISPINTERFACE_DEFINED__
#define ___IThemeEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IThemeEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IThemeEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("BA46F095-6BC6-4FD0-B051-4EDB0BB7DED1")
    _IThemeEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IThemeEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IThemeEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IThemeEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IThemeEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IThemeEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IThemeEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IThemeEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IThemeEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IThemeEventsVtbl;

    interface _IThemeEvents
    {
        CONST_VTBL struct _IThemeEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IThemeEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IThemeEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IThemeEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IThemeEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IThemeEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IThemeEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IThemeEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IThemeEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_Theme;

#ifdef __cplusplus

class DECLSPEC_UUID("ADCAE1F5-7EB1-492E-89C9-B1B8B273D09B")
Theme;
#endif
#endif /* __safThemeLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  HWND_UserSize(     unsigned long *, unsigned long            , HWND * ); 
unsigned char * __RPC_USER  HWND_UserMarshal(  unsigned long *, unsigned char *, HWND * ); 
unsigned char * __RPC_USER  HWND_UserUnmarshal(unsigned long *, unsigned char *, HWND * ); 
void                      __RPC_USER  HWND_UserFree(     unsigned long *, HWND * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


