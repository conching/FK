

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Wed May 01 19:52:46 2019
 */
/* Compiler settings for .\safTools.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __safTools_h_h__
#define __safTools_h_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDataView_FWD_DEFINED__
#define __IDataView_FWD_DEFINED__
typedef interface IDataView IDataView;
#endif 	/* __IDataView_FWD_DEFINED__ */


#ifndef __IEnumDataView_FWD_DEFINED__
#define __IEnumDataView_FWD_DEFINED__
typedef interface IEnumDataView IEnumDataView;
#endif 	/* __IEnumDataView_FWD_DEFINED__ */


#ifndef __IDataViewsCallBack_FWD_DEFINED__
#define __IDataViewsCallBack_FWD_DEFINED__
typedef interface IDataViewsCallBack IDataViewsCallBack;
#endif 	/* __IDataViewsCallBack_FWD_DEFINED__ */


#ifndef __IDataViews_FWD_DEFINED__
#define __IDataViews_FWD_DEFINED__
typedef interface IDataViews IDataViews;
#endif 	/* __IDataViews_FWD_DEFINED__ */


#ifndef __IThreadNotify_FWD_DEFINED__
#define __IThreadNotify_FWD_DEFINED__
typedef interface IThreadNotify IThreadNotify;
#endif 	/* __IThreadNotify_FWD_DEFINED__ */


#ifndef __INotifyTextInfo_FWD_DEFINED__
#define __INotifyTextInfo_FWD_DEFINED__
typedef interface INotifyTextInfo INotifyTextInfo;
#endif 	/* __INotifyTextInfo_FWD_DEFINED__ */


#ifndef __IProgressNotifyText_FWD_DEFINED__
#define __IProgressNotifyText_FWD_DEFINED__
typedef interface IProgressNotifyText IProgressNotifyText;
#endif 	/* __IProgressNotifyText_FWD_DEFINED__ */


#ifndef __IProgressWait_FWD_DEFINED__
#define __IProgressWait_FWD_DEFINED__
typedef interface IProgressWait IProgressWait;
#endif 	/* __IProgressWait_FWD_DEFINED__ */


#ifndef __IThreadNotifys_FWD_DEFINED__
#define __IThreadNotifys_FWD_DEFINED__
typedef interface IThreadNotifys IThreadNotifys;
#endif 	/* __IThreadNotifys_FWD_DEFINED__ */


#ifndef __IHeaderItem_FWD_DEFINED__
#define __IHeaderItem_FWD_DEFINED__
typedef interface IHeaderItem IHeaderItem;
#endif 	/* __IHeaderItem_FWD_DEFINED__ */


#ifndef __IHeaderCtrlEx_FWD_DEFINED__
#define __IHeaderCtrlEx_FWD_DEFINED__
typedef interface IHeaderCtrlEx IHeaderCtrlEx;
#endif 	/* __IHeaderCtrlEx_FWD_DEFINED__ */


#ifndef __IListCtrlColumn_FWD_DEFINED__
#define __IListCtrlColumn_FWD_DEFINED__
typedef interface IListCtrlColumn IListCtrlColumn;
#endif 	/* __IListCtrlColumn_FWD_DEFINED__ */


#ifndef __ITreePropertySheet_FWD_DEFINED__
#define __ITreePropertySheet_FWD_DEFINED__
typedef interface ITreePropertySheet ITreePropertySheet;
#endif 	/* __ITreePropertySheet_FWD_DEFINED__ */


#ifndef __IRegsvrDll_FWD_DEFINED__
#define __IRegsvrDll_FWD_DEFINED__
typedef interface IRegsvrDll IRegsvrDll;
#endif 	/* __IRegsvrDll_FWD_DEFINED__ */


#ifndef __IThreadNotifyInfoDlg_FWD_DEFINED__
#define __IThreadNotifyInfoDlg_FWD_DEFINED__
typedef interface IThreadNotifyInfoDlg IThreadNotifyInfoDlg;
#endif 	/* __IThreadNotifyInfoDlg_FWD_DEFINED__ */


#ifndef __IManagerCallback_FWD_DEFINED__
#define __IManagerCallback_FWD_DEFINED__
typedef interface IManagerCallback IManagerCallback;
#endif 	/* __IManagerCallback_FWD_DEFINED__ */


#ifndef __ISheetPanel_FWD_DEFINED__
#define __ISheetPanel_FWD_DEFINED__
typedef interface ISheetPanel ISheetPanel;
#endif 	/* __ISheetPanel_FWD_DEFINED__ */


#ifndef __IThumbnailInfo_FWD_DEFINED__
#define __IThumbnailInfo_FWD_DEFINED__
typedef interface IThumbnailInfo IThumbnailInfo;
#endif 	/* __IThumbnailInfo_FWD_DEFINED__ */


#ifndef __IPropertyPageDlg_FWD_DEFINED__
#define __IPropertyPageDlg_FWD_DEFINED__
typedef interface IPropertyPageDlg IPropertyPageDlg;
#endif 	/* __IPropertyPageDlg_FWD_DEFINED__ */


#ifndef ___IHeaderCtrlExEvents_FWD_DEFINED__
#define ___IHeaderCtrlExEvents_FWD_DEFINED__
typedef interface _IHeaderCtrlExEvents _IHeaderCtrlExEvents;
#endif 	/* ___IHeaderCtrlExEvents_FWD_DEFINED__ */


#ifndef __HeaderCtrlEx_FWD_DEFINED__
#define __HeaderCtrlEx_FWD_DEFINED__

#ifdef __cplusplus
typedef class HeaderCtrlEx HeaderCtrlEx;
#else
typedef struct HeaderCtrlEx HeaderCtrlEx;
#endif /* __cplusplus */

#endif 	/* __HeaderCtrlEx_FWD_DEFINED__ */


#ifndef ___IListCtrlColumnEvents_FWD_DEFINED__
#define ___IListCtrlColumnEvents_FWD_DEFINED__
typedef interface _IListCtrlColumnEvents _IListCtrlColumnEvents;
#endif 	/* ___IListCtrlColumnEvents_FWD_DEFINED__ */


#ifndef __ListCtrlColumn_FWD_DEFINED__
#define __ListCtrlColumn_FWD_DEFINED__

#ifdef __cplusplus
typedef class ListCtrlColumn ListCtrlColumn;
#else
typedef struct ListCtrlColumn ListCtrlColumn;
#endif /* __cplusplus */

#endif 	/* __ListCtrlColumn_FWD_DEFINED__ */


#ifndef ___IDataViewsEvents_FWD_DEFINED__
#define ___IDataViewsEvents_FWD_DEFINED__
typedef interface _IDataViewsEvents _IDataViewsEvents;
#endif 	/* ___IDataViewsEvents_FWD_DEFINED__ */


#ifndef __DataViews_FWD_DEFINED__
#define __DataViews_FWD_DEFINED__

#ifdef __cplusplus
typedef class DataViews DataViews;
#else
typedef struct DataViews DataViews;
#endif /* __cplusplus */

#endif 	/* __DataViews_FWD_DEFINED__ */


#ifndef ___ITreePropertySheetEvents_FWD_DEFINED__
#define ___ITreePropertySheetEvents_FWD_DEFINED__
typedef interface _ITreePropertySheetEvents _ITreePropertySheetEvents;
#endif 	/* ___ITreePropertySheetEvents_FWD_DEFINED__ */


#ifndef __TreePropertySheet_FWD_DEFINED__
#define __TreePropertySheet_FWD_DEFINED__

#ifdef __cplusplus
typedef class TreePropertySheet TreePropertySheet;
#else
typedef struct TreePropertySheet TreePropertySheet;
#endif /* __cplusplus */

#endif 	/* __TreePropertySheet_FWD_DEFINED__ */


#ifndef ___IRegsvrDllEvents_FWD_DEFINED__
#define ___IRegsvrDllEvents_FWD_DEFINED__
typedef interface _IRegsvrDllEvents _IRegsvrDllEvents;
#endif 	/* ___IRegsvrDllEvents_FWD_DEFINED__ */


#ifndef __RegsvrDll_FWD_DEFINED__
#define __RegsvrDll_FWD_DEFINED__

#ifdef __cplusplus
typedef class RegsvrDll RegsvrDll;
#else
typedef struct RegsvrDll RegsvrDll;
#endif /* __cplusplus */

#endif 	/* __RegsvrDll_FWD_DEFINED__ */


#ifndef ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__
#define ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__
typedef interface _IThreadNotifyInfoDlgEvents _IThreadNotifyInfoDlgEvents;
#endif 	/* ___IThreadNotifyInfoDlgEvents_FWD_DEFINED__ */


#ifndef __ThreadNotifyInfoDlg_FWD_DEFINED__
#define __ThreadNotifyInfoDlg_FWD_DEFINED__

#ifdef __cplusplus
typedef class ThreadNotifyInfoDlg ThreadNotifyInfoDlg;
#else
typedef struct ThreadNotifyInfoDlg ThreadNotifyInfoDlg;
#endif /* __cplusplus */

#endif 	/* __ThreadNotifyInfoDlg_FWD_DEFINED__ */


#ifndef ___IManagerCallbackEvents_FWD_DEFINED__
#define ___IManagerCallbackEvents_FWD_DEFINED__
typedef interface _IManagerCallbackEvents _IManagerCallbackEvents;
#endif 	/* ___IManagerCallbackEvents_FWD_DEFINED__ */


#ifndef __ManagerCallback_FWD_DEFINED__
#define __ManagerCallback_FWD_DEFINED__

#ifdef __cplusplus
typedef class ManagerCallback ManagerCallback;
#else
typedef struct ManagerCallback ManagerCallback;
#endif /* __cplusplus */

#endif 	/* __ManagerCallback_FWD_DEFINED__ */


#ifndef __SheetPanel_FWD_DEFINED__
#define __SheetPanel_FWD_DEFINED__

#ifdef __cplusplus
typedef class SheetPanel SheetPanel;
#else
typedef struct SheetPanel SheetPanel;
#endif /* __cplusplus */

#endif 	/* __SheetPanel_FWD_DEFINED__ */


#ifndef __ThumbnailInfo_FWD_DEFINED__
#define __ThumbnailInfo_FWD_DEFINED__

#ifdef __cplusplus
typedef class ThumbnailInfo ThumbnailInfo;
#else
typedef struct ThumbnailInfo ThumbnailInfo;
#endif /* __cplusplus */

#endif 	/* __ThumbnailInfo_FWD_DEFINED__ */


#ifndef __PropertyPageDlg_FWD_DEFINED__
#define __PropertyPageDlg_FWD_DEFINED__

#ifdef __cplusplus
typedef class PropertyPageDlg PropertyPageDlg;
#else
typedef struct PropertyPageDlg PropertyPageDlg;
#endif /* __cplusplus */

#endif 	/* __PropertyPageDlg_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

/* interface __MIDL_itf_safTools_0000 */
/* [local] */ 




typedef /* [helpstring][uuid] */  DECLSPEC_UUID("6A597B0E-8780-4aff-ACFC-FE4AA02C0F5C") 
enum LVCF_Column
    {	LVCF_MYEX_MORE	= 0x10000,
	LVCF_MYEX_COLUMN	= 0x40000,
	LVCF_MYEX_SHOW_MENU	= 0x80000,
	LVCF_MYEX_FIXED	= 0x100000
    } 	LVCF_Column;

typedef /* [helpstring][uuid] */  DECLSPEC_UUID("16234748-BCB2-442d-919D-F0D1077F65CF") struct LVCOLUMN_HEADER_ITEM
    {
    int nColumnKey;
    IHeaderItem *pHeaderItem;
    } 	LVCOLUMN_HEADER_ITEM;



extern RPC_IF_HANDLE __MIDL_itf_safTools_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_safTools_0000_v0_0_s_ifspec;

#ifndef __IDataView_INTERFACE_DEFINED__
#define __IDataView_INTERFACE_DEFINED__

/* interface IDataView */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IDataView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("1521B76B-F685-42a5-975A-7F58FA42C6F1")
    IDataView : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUpdateState( 
            /* [in] */ IDataViews *pDataViews,
            UINT uiUserState,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvDataViewEnable( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            LONG lParam,
            VARIANT_BOOL bEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUpdateData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObj,
            /* [in] */ ULONG pAttachData,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvWriteData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvReadData( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DvUserCommand( 
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam,
            UINT nCommand,
            ULONG pUserData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataView * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDataView * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDataView * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDataView * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDataView * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUpdateState )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            UINT uiUserState,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvDataViewEnable )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            LONG lParam,
            VARIANT_BOOL bEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUpdateData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObj,
            /* [in] */ ULONG pAttachData,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvWriteData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvReadData )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DvUserCommand )( 
            IDataView * This,
            /* [in] */ IDataViews *pDataViews,
            /* [in] */ ULONG pDataObj,
            /* [in] */ LONG lParam,
            UINT nCommand,
            ULONG pUserData);
        
        END_INTERFACE
    } IDataViewVtbl;

    interface IDataView
    {
        CONST_VTBL struct IDataViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataView_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataView_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataView_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataView_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDataView_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDataView_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDataView_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDataView_DvUpdateState(This,pDataViews,uiUserState,pDataObj,lParam)	\
    (This)->lpVtbl -> DvUpdateState(This,pDataViews,uiUserState,pDataObj,lParam)

#define IDataView_DvDataViewEnable(This,pDataViews,pDataObj,lParam,bEnable)	\
    (This)->lpVtbl -> DvDataViewEnable(This,pDataViews,pDataObj,lParam,bEnable)

#define IDataView_DvUpdateData(This,pDataViews,uiUpdateType,pDataObj,pAttachData,lParam)	\
    (This)->lpVtbl -> DvUpdateData(This,pDataViews,uiUpdateType,pDataObj,pAttachData,lParam)

#define IDataView_DvWriteData(This,pDataViews,pDataObj,lParam)	\
    (This)->lpVtbl -> DvWriteData(This,pDataViews,pDataObj,lParam)

#define IDataView_DvReadData(This,pDataViews,pDataObj,lParam)	\
    (This)->lpVtbl -> DvReadData(This,pDataViews,pDataObj,lParam)

#define IDataView_DvUserCommand(This,pDataViews,pDataObj,lParam,nCommand,pUserData)	\
    (This)->lpVtbl -> DvUserCommand(This,pDataViews,pDataObj,lParam,nCommand,pUserData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUpdateState_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    UINT uiUserState,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvUpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvDataViewEnable_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    LONG lParam,
    VARIANT_BOOL bEnable);


void __RPC_STUB IDataView_DvDataViewEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUpdateData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ UINT uiUpdateType,
    /* [in] */ ULONG pDataObj,
    /* [in] */ ULONG pAttachData,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvUpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvWriteData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvWriteData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvReadData_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam);


void __RPC_STUB IDataView_DvReadData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataView_DvUserCommand_Proxy( 
    IDataView * This,
    /* [in] */ IDataViews *pDataViews,
    /* [in] */ ULONG pDataObj,
    /* [in] */ LONG lParam,
    UINT nCommand,
    ULONG pUserData);


void __RPC_STUB IDataView_DvUserCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataView_INTERFACE_DEFINED__ */


#ifndef __IEnumDataView_INTERFACE_DEFINED__
#define __IEnumDataView_INTERFACE_DEFINED__

/* interface IEnumDataView */
/* [uuid][object] */ 


EXTERN_C const IID IID_IEnumDataView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0F243D5A-2120-4033-AF11-4620609DAD89")
    IEnumDataView : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Next( 
            ULONG celt,
            IDataView **ppDataView,
            ULONG *pceltFetched) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Skip( 
            ULONG celt) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Reset( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Clone( 
            IEnumDataView **ppEnumDataView) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEnumDataViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IEnumDataView * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IEnumDataView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IEnumDataView * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Next )( 
            IEnumDataView * This,
            ULONG celt,
            IDataView **ppDataView,
            ULONG *pceltFetched);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Skip )( 
            IEnumDataView * This,
            ULONG celt);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Reset )( 
            IEnumDataView * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Clone )( 
            IEnumDataView * This,
            IEnumDataView **ppEnumDataView);
        
        END_INTERFACE
    } IEnumDataViewVtbl;

    interface IEnumDataView
    {
        CONST_VTBL struct IEnumDataViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEnumDataView_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEnumDataView_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEnumDataView_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEnumDataView_Next(This,celt,ppDataView,pceltFetched)	\
    (This)->lpVtbl -> Next(This,celt,ppDataView,pceltFetched)

#define IEnumDataView_Skip(This,celt)	\
    (This)->lpVtbl -> Skip(This,celt)

#define IEnumDataView_Reset(This)	\
    (This)->lpVtbl -> Reset(This)

#define IEnumDataView_Clone(This,ppEnumDataView)	\
    (This)->lpVtbl -> Clone(This,ppEnumDataView)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Next_Proxy( 
    IEnumDataView * This,
    ULONG celt,
    IDataView **ppDataView,
    ULONG *pceltFetched);


void __RPC_STUB IEnumDataView_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Skip_Proxy( 
    IEnumDataView * This,
    ULONG celt);


void __RPC_STUB IEnumDataView_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Reset_Proxy( 
    IEnumDataView * This);


void __RPC_STUB IEnumDataView_Reset_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IEnumDataView_Clone_Proxy( 
    IEnumDataView * This,
    IEnumDataView **ppEnumDataView);


void __RPC_STUB IEnumDataView_Clone_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEnumDataView_INTERFACE_DEFINED__ */


#ifndef __IDataViewsCallBack_INTERFACE_DEFINED__
#define __IDataViewsCallBack_INTERFACE_DEFINED__

/* interface IDataViewsCallBack */
/* [uuid][object] */ 


EXTERN_C const IID IID_IDataViewsCallBack;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("95ED5428-7152-456b-990E-EC4B6BA1D62F")
    IDataViewsCallBack : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateData( 
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUpdateType,
            ULONG pDataObj,
            ULONG_PTR pParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateState( 
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUserState,
            ULONG pDataObj,
            ULONG_PTR pParam) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewsCallBackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataViewsCallBack * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataViewsCallBack * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataViewsCallBack * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateData )( 
            IDataViewsCallBack * This,
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUpdateType,
            ULONG pDataObj,
            ULONG_PTR pParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateState )( 
            IDataViewsCallBack * This,
            IDataViews *pDataViews,
            IDataView *pDataView,
            UINT uiUserState,
            ULONG pDataObj,
            ULONG_PTR pParam);
        
        END_INTERFACE
    } IDataViewsCallBackVtbl;

    interface IDataViewsCallBack
    {
        CONST_VTBL struct IDataViewsCallBackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataViewsCallBack_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataViewsCallBack_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataViewsCallBack_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataViewsCallBack_UpdateData(This,pDataViews,pDataView,uiUpdateType,pDataObj,pParam)	\
    (This)->lpVtbl -> UpdateData(This,pDataViews,pDataView,uiUpdateType,pDataObj,pParam)

#define IDataViewsCallBack_UpdateState(This,pDataViews,pDataView,uiUserState,pDataObj,pParam)	\
    (This)->lpVtbl -> UpdateState(This,pDataViews,pDataView,uiUserState,pDataObj,pParam)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataViewsCallBack_UpdateData_Proxy( 
    IDataViewsCallBack * This,
    IDataViews *pDataViews,
    IDataView *pDataView,
    UINT uiUpdateType,
    ULONG pDataObj,
    ULONG_PTR pParam);


void __RPC_STUB IDataViewsCallBack_UpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViewsCallBack_UpdateState_Proxy( 
    IDataViewsCallBack * This,
    IDataViews *pDataViews,
    IDataView *pDataView,
    UINT uiUserState,
    ULONG pDataObj,
    ULONG_PTR pParam);


void __RPC_STUB IDataViewsCallBack_UpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataViewsCallBack_INTERFACE_DEFINED__ */


#ifndef __IDataViews_INTERFACE_DEFINED__
#define __IDataViews_INTERFACE_DEFINED__

/* interface IDataViews */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IDataViews;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("43EC817A-32E9-47AC-AA13-7E1081EFDF40")
    IDataViews : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateState( 
            /* [in] */ UINT uiUserState) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateData( 
            /* [in] */ UINT uiUpdateType) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UpdateDataEx( 
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE WriteData( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ReadData( void) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Param( 
            /* [retval][out] */ ULONG *ppDataParam) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_Param( 
            /* [in] */ ULONG pDataParam) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AddDataView( 
            VARIANT *pVariantKey,
            IDataView *pDataView) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveDataViewKey( 
            VARIANT *pVariantKey) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DataObject( 
            /* [retval][out] */ ULONG *pDataObject) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_DataObject( 
            /* [in] */ ULONG pDataObject) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Item( 
            VARIANT *varItemKey,
            IDataView **pDataView) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            /* [retval][out] */ LONG *lCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RemoveDataView( 
            IDataView *pDataView) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Link( 
            VARIANT *pVariantKey,
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE LinkDataView( 
            IDataView *pDataView,
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_LinkCount( 
            /* [retval][out] */ LONG *pCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_UnlinkCount( 
            /* [retval][out] */ LONG *pCount) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE AllDataViewLink( 
            VARIANT_BOOL bLink) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewLock( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewUnlock( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DataViewEnable( 
            VARIANT_BOOL bEnable) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UserCommand( 
            UINT nCommand,
            ULONG pUserParam) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get__EnumDataView( 
            /* [retval][out] */ IEnumDataView **ppEnumDataView) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_DataViewsCallBack( 
            /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_DataViewsCallBack( 
            /* [in] */ IDataViewsCallBack *pDataViewsCallBack) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IDataViewsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataViews * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataViews * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataViews * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDataViews * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDataViews * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDataViews * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDataViews * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateState )( 
            IDataViews * This,
            /* [in] */ UINT uiUserState);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateData )( 
            IDataViews * This,
            /* [in] */ UINT uiUpdateType);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UpdateDataEx )( 
            IDataViews * This,
            /* [in] */ UINT uiUpdateType,
            /* [in] */ ULONG pDataObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *WriteData )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ReadData )( 
            IDataViews * This);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Param )( 
            IDataViews * This,
            /* [retval][out] */ ULONG *ppDataParam);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_Param )( 
            IDataViews * This,
            /* [in] */ ULONG pDataParam);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AddDataView )( 
            IDataViews * This,
            VARIANT *pVariantKey,
            IDataView *pDataView);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveDataViewKey )( 
            IDataViews * This,
            VARIANT *pVariantKey);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataObject )( 
            IDataViews * This,
            /* [retval][out] */ ULONG *pDataObject);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DataObject )( 
            IDataViews * This,
            /* [in] */ ULONG pDataObject);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Item )( 
            IDataViews * This,
            VARIANT *varItemKey,
            IDataView **pDataView);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IDataViews * This,
            /* [retval][out] */ LONG *lCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RemoveDataView )( 
            IDataViews * This,
            IDataView *pDataView);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Link )( 
            IDataViews * This,
            VARIANT *pVariantKey,
            VARIANT_BOOL bLink);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *LinkDataView )( 
            IDataViews * This,
            IDataView *pDataView,
            VARIANT_BOOL bLink);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_LinkCount )( 
            IDataViews * This,
            /* [retval][out] */ LONG *pCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_UnlinkCount )( 
            IDataViews * This,
            /* [retval][out] */ LONG *pCount);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *AllDataViewLink )( 
            IDataViews * This,
            VARIANT_BOOL bLink);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewLock )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewUnlock )( 
            IDataViews * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DataViewEnable )( 
            IDataViews * This,
            VARIANT_BOOL bEnable);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UserCommand )( 
            IDataViews * This,
            UINT nCommand,
            ULONG pUserParam);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get__EnumDataView )( 
            IDataViews * This,
            /* [retval][out] */ IEnumDataView **ppEnumDataView);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_DataViewsCallBack )( 
            IDataViews * This,
            /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_DataViewsCallBack )( 
            IDataViews * This,
            /* [in] */ IDataViewsCallBack *pDataViewsCallBack);
        
        END_INTERFACE
    } IDataViewsVtbl;

    interface IDataViews
    {
        CONST_VTBL struct IDataViewsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataViews_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataViews_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataViews_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataViews_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDataViews_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDataViews_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDataViews_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IDataViews_UpdateState(This,uiUserState)	\
    (This)->lpVtbl -> UpdateState(This,uiUserState)

#define IDataViews_UpdateData(This,uiUpdateType)	\
    (This)->lpVtbl -> UpdateData(This,uiUpdateType)

#define IDataViews_UpdateDataEx(This,uiUpdateType,pDataObject)	\
    (This)->lpVtbl -> UpdateDataEx(This,uiUpdateType,pDataObject)

#define IDataViews_WriteData(This)	\
    (This)->lpVtbl -> WriteData(This)

#define IDataViews_ReadData(This)	\
    (This)->lpVtbl -> ReadData(This)

#define IDataViews_get_Param(This,ppDataParam)	\
    (This)->lpVtbl -> get_Param(This,ppDataParam)

#define IDataViews_put_Param(This,pDataParam)	\
    (This)->lpVtbl -> put_Param(This,pDataParam)

#define IDataViews_AddDataView(This,pVariantKey,pDataView)	\
    (This)->lpVtbl -> AddDataView(This,pVariantKey,pDataView)

#define IDataViews_RemoveDataViewKey(This,pVariantKey)	\
    (This)->lpVtbl -> RemoveDataViewKey(This,pVariantKey)

#define IDataViews_get_DataObject(This,pDataObject)	\
    (This)->lpVtbl -> get_DataObject(This,pDataObject)

#define IDataViews_put_DataObject(This,pDataObject)	\
    (This)->lpVtbl -> put_DataObject(This,pDataObject)

#define IDataViews_Item(This,varItemKey,pDataView)	\
    (This)->lpVtbl -> Item(This,varItemKey,pDataView)

#define IDataViews_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IDataViews_RemoveDataView(This,pDataView)	\
    (This)->lpVtbl -> RemoveDataView(This,pDataView)

#define IDataViews_Link(This,pVariantKey,bLink)	\
    (This)->lpVtbl -> Link(This,pVariantKey,bLink)

#define IDataViews_LinkDataView(This,pDataView,bLink)	\
    (This)->lpVtbl -> LinkDataView(This,pDataView,bLink)

#define IDataViews_get_LinkCount(This,pCount)	\
    (This)->lpVtbl -> get_LinkCount(This,pCount)

#define IDataViews_get_UnlinkCount(This,pCount)	\
    (This)->lpVtbl -> get_UnlinkCount(This,pCount)

#define IDataViews_AllDataViewLink(This,bLink)	\
    (This)->lpVtbl -> AllDataViewLink(This,bLink)

#define IDataViews_DataViewLock(This)	\
    (This)->lpVtbl -> DataViewLock(This)

#define IDataViews_DataViewUnlock(This)	\
    (This)->lpVtbl -> DataViewUnlock(This)

#define IDataViews_DataViewEnable(This,bEnable)	\
    (This)->lpVtbl -> DataViewEnable(This,bEnable)

#define IDataViews_UserCommand(This,nCommand,pUserParam)	\
    (This)->lpVtbl -> UserCommand(This,nCommand,pUserParam)

#define IDataViews_get__EnumDataView(This,ppEnumDataView)	\
    (This)->lpVtbl -> get__EnumDataView(This,ppEnumDataView)

#define IDataViews_get_DataViewsCallBack(This,ppDataViewsCallBack)	\
    (This)->lpVtbl -> get_DataViewsCallBack(This,ppDataViewsCallBack)

#define IDataViews_put_DataViewsCallBack(This,pDataViewsCallBack)	\
    (This)->lpVtbl -> put_DataViewsCallBack(This,pDataViewsCallBack)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateState_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUserState);


void __RPC_STUB IDataViews_UpdateState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateData_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUpdateType);


void __RPC_STUB IDataViews_UpdateData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UpdateDataEx_Proxy( 
    IDataViews * This,
    /* [in] */ UINT uiUpdateType,
    /* [in] */ ULONG pDataObject);


void __RPC_STUB IDataViews_UpdateDataEx_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_WriteData_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_WriteData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_ReadData_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_ReadData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_Param_Proxy( 
    IDataViews * This,
    /* [retval][out] */ ULONG *ppDataParam);


void __RPC_STUB IDataViews_get_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_Param_Proxy( 
    IDataViews * This,
    /* [in] */ ULONG pDataParam);


void __RPC_STUB IDataViews_put_Param_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_AddDataView_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey,
    IDataView *pDataView);


void __RPC_STUB IDataViews_AddDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_RemoveDataViewKey_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey);


void __RPC_STUB IDataViews_RemoveDataViewKey_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_DataObject_Proxy( 
    IDataViews * This,
    /* [retval][out] */ ULONG *pDataObject);


void __RPC_STUB IDataViews_get_DataObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_DataObject_Proxy( 
    IDataViews * This,
    /* [in] */ ULONG pDataObject);


void __RPC_STUB IDataViews_put_DataObject_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_Item_Proxy( 
    IDataViews * This,
    VARIANT *varItemKey,
    IDataView **pDataView);


void __RPC_STUB IDataViews_Item_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_Count_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *lCount);


void __RPC_STUB IDataViews_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_RemoveDataView_Proxy( 
    IDataViews * This,
    IDataView *pDataView);


void __RPC_STUB IDataViews_RemoveDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_Link_Proxy( 
    IDataViews * This,
    VARIANT *pVariantKey,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_Link_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_LinkDataView_Proxy( 
    IDataViews * This,
    IDataView *pDataView,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_LinkDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_LinkCount_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *pCount);


void __RPC_STUB IDataViews_get_LinkCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_UnlinkCount_Proxy( 
    IDataViews * This,
    /* [retval][out] */ LONG *pCount);


void __RPC_STUB IDataViews_get_UnlinkCount_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_AllDataViewLink_Proxy( 
    IDataViews * This,
    VARIANT_BOOL bLink);


void __RPC_STUB IDataViews_AllDataViewLink_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewLock_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_DataViewLock_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewUnlock_Proxy( 
    IDataViews * This);


void __RPC_STUB IDataViews_DataViewUnlock_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_DataViewEnable_Proxy( 
    IDataViews * This,
    VARIANT_BOOL bEnable);


void __RPC_STUB IDataViews_DataViewEnable_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IDataViews_UserCommand_Proxy( 
    IDataViews * This,
    UINT nCommand,
    ULONG pUserParam);


void __RPC_STUB IDataViews_UserCommand_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get__EnumDataView_Proxy( 
    IDataViews * This,
    /* [retval][out] */ IEnumDataView **ppEnumDataView);


void __RPC_STUB IDataViews_get__EnumDataView_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IDataViews_get_DataViewsCallBack_Proxy( 
    IDataViews * This,
    /* [retval][out] */ IDataViewsCallBack **ppDataViewsCallBack);


void __RPC_STUB IDataViews_get_DataViewsCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IDataViews_put_DataViewsCallBack_Proxy( 
    IDataViews * This,
    /* [in] */ IDataViewsCallBack *pDataViewsCallBack);


void __RPC_STUB IDataViews_put_DataViewsCallBack_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IDataViews_INTERFACE_DEFINED__ */


#ifndef __IThreadNotify_INTERFACE_DEFINED__
#define __IThreadNotify_INTERFACE_DEFINED__

/* interface IThreadNotify */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotify;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F91784BE-E329-4739-BFAA-01C3FE41539E")
    IThreadNotify : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_Begin( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_End( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotify * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotify * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_Begin )( 
            IThreadNotify * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_End )( 
            IThreadNotify * This);
        
        END_INTERFACE
    } IThreadNotifyVtbl;

    interface IThreadNotify
    {
        CONST_VTBL struct IThreadNotifyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotify_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotify_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotify_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotify_ThreadNotify_Begin(This)	\
    (This)->lpVtbl -> ThreadNotify_Begin(This)

#define IThreadNotify_ThreadNotify_End(This)	\
    (This)->lpVtbl -> ThreadNotify_End(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotify_ThreadNotify_Begin_Proxy( 
    IThreadNotify * This);


void __RPC_STUB IThreadNotify_ThreadNotify_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotify_ThreadNotify_End_Proxy( 
    IThreadNotify * This);


void __RPC_STUB IThreadNotify_ThreadNotify_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThreadNotify_INTERFACE_DEFINED__ */


#ifndef __INotifyTextInfo_INTERFACE_DEFINED__
#define __INotifyTextInfo_INTERFACE_DEFINED__

/* interface INotifyTextInfo */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INotifyTextInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2D1A46CF-11A3-4786-A644-2FDB6920E433")
    INotifyTextInfo : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_AppendBSTR( 
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_InsertLine( 
            int nLine,
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_Begin( 
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE NotifyTextInfo_End( 
            BSTR bstrText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INotifyTextInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INotifyTextInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INotifyTextInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INotifyTextInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INotifyTextInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INotifyTextInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INotifyTextInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INotifyTextInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_AppendBSTR )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_InsertLine )( 
            INotifyTextInfo * This,
            int nLine,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_Begin )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *NotifyTextInfo_End )( 
            INotifyTextInfo * This,
            BSTR bstrText);
        
        END_INTERFACE
    } INotifyTextInfoVtbl;

    interface INotifyTextInfo
    {
        CONST_VTBL struct INotifyTextInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INotifyTextInfo_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define INotifyTextInfo_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define INotifyTextInfo_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define INotifyTextInfo_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define INotifyTextInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define INotifyTextInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define INotifyTextInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define INotifyTextInfo_NotifyTextInfo_AppendBSTR(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_AppendBSTR(This,bstrText)

#define INotifyTextInfo_NotifyTextInfo_InsertLine(This,nLine,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_InsertLine(This,nLine,bstrText)

#define INotifyTextInfo_NotifyTextInfo_Begin(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_Begin(This,bstrText)

#define INotifyTextInfo_NotifyTextInfo_End(This,bstrText)	\
    (This)->lpVtbl -> NotifyTextInfo_End(This,bstrText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_AppendBSTR_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_AppendBSTR_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_InsertLine_Proxy( 
    INotifyTextInfo * This,
    int nLine,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_InsertLine_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_Begin_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE INotifyTextInfo_NotifyTextInfo_End_Proxy( 
    INotifyTextInfo * This,
    BSTR bstrText);


void __RPC_STUB INotifyTextInfo_NotifyTextInfo_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __INotifyTextInfo_INTERFACE_DEFINED__ */


#ifndef __IProgressNotifyText_INTERFACE_DEFINED__
#define __IProgressNotifyText_INTERFACE_DEFINED__

/* interface IProgressNotifyText */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressNotifyText;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BDD00F8F-86C9-41b7-9302-02D0E7662D28")
    IProgressNotifyText : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_Progress( 
            int nProgressID,
            BSTR bstrText) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_Begin( 
            int nProgressID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressNotifyText_End( 
            int nProgressID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressNotifyTextVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressNotifyText * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressNotifyText * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressNotifyText * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_Progress )( 
            IProgressNotifyText * This,
            int nProgressID,
            BSTR bstrText);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_Begin )( 
            IProgressNotifyText * This,
            int nProgressID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressNotifyText_End )( 
            IProgressNotifyText * This,
            int nProgressID);
        
        END_INTERFACE
    } IProgressNotifyTextVtbl;

    interface IProgressNotifyText
    {
        CONST_VTBL struct IProgressNotifyTextVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressNotifyText_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressNotifyText_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressNotifyText_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressNotifyText_ProgressNotifyText_Progress(This,nProgressID,bstrText)	\
    (This)->lpVtbl -> ProgressNotifyText_Progress(This,nProgressID,bstrText)

#define IProgressNotifyText_ProgressNotifyText_Begin(This,nProgressID)	\
    (This)->lpVtbl -> ProgressNotifyText_Begin(This,nProgressID)

#define IProgressNotifyText_ProgressNotifyText_End(This,nProgressID)	\
    (This)->lpVtbl -> ProgressNotifyText_End(This,nProgressID)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_Progress_Proxy( 
    IProgressNotifyText * This,
    int nProgressID,
    BSTR bstrText);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_Progress_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_Begin_Proxy( 
    IProgressNotifyText * This,
    int nProgressID);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressNotifyText_ProgressNotifyText_End_Proxy( 
    IProgressNotifyText * This,
    int nProgressID);


void __RPC_STUB IProgressNotifyText_ProgressNotifyText_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressNotifyText_INTERFACE_DEFINED__ */


#ifndef __IProgressWait_INTERFACE_DEFINED__
#define __IProgressWait_INTERFACE_DEFINED__

/* interface IProgressWait */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IProgressWait;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6ABE364C-5AD0-41e5-BFB2-72A0474D6427")
    IProgressWait : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressWait_Begin( 
            int nProgressID) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ProgressWait_End( 
            int nProgressID) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IProgressWaitVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IProgressWait * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IProgressWait * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IProgressWait * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressWait_Begin )( 
            IProgressWait * This,
            int nProgressID);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ProgressWait_End )( 
            IProgressWait * This,
            int nProgressID);
        
        END_INTERFACE
    } IProgressWaitVtbl;

    interface IProgressWait
    {
        CONST_VTBL struct IProgressWaitVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IProgressWait_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IProgressWait_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IProgressWait_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IProgressWait_ProgressWait_Begin(This,nProgressID)	\
    (This)->lpVtbl -> ProgressWait_Begin(This,nProgressID)

#define IProgressWait_ProgressWait_End(This,nProgressID)	\
    (This)->lpVtbl -> ProgressWait_End(This,nProgressID)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IProgressWait_ProgressWait_Begin_Proxy( 
    IProgressWait * This,
    int nProgressID);


void __RPC_STUB IProgressWait_ProgressWait_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IProgressWait_ProgressWait_End_Proxy( 
    IProgressWait * This,
    int nProgressID);


void __RPC_STUB IProgressWait_ProgressWait_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IProgressWait_INTERFACE_DEFINED__ */


#ifndef __IThreadNotifys_INTERFACE_DEFINED__
#define __IThreadNotifys_INTERFACE_DEFINED__

/* interface IThreadNotifys */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotifys;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7A52AA78-08C2-4d5d-AE84-8E42704577E5")
    IThreadNotifys : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ IThreadNotify *pThreadNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Remove( 
            /* [in] */ IThreadNotify *pThreadNotify) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Clear( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_Begin( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ThreadNotify_End( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifysVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotifys * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotifys * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Add )( 
            IThreadNotifys * This,
            /* [in] */ IThreadNotify *pThreadNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Remove )( 
            IThreadNotifys * This,
            /* [in] */ IThreadNotify *pThreadNotify);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Clear )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_Begin )( 
            IThreadNotifys * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ThreadNotify_End )( 
            IThreadNotifys * This);
        
        END_INTERFACE
    } IThreadNotifysVtbl;

    interface IThreadNotifys
    {
        CONST_VTBL struct IThreadNotifysVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotifys_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotifys_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotifys_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotifys_Add(This,pThreadNotify)	\
    (This)->lpVtbl -> Add(This,pThreadNotify)

#define IThreadNotifys_Remove(This,pThreadNotify)	\
    (This)->lpVtbl -> Remove(This,pThreadNotify)

#define IThreadNotifys_Clear(This)	\
    (This)->lpVtbl -> Clear(This)

#define IThreadNotifys_ThreadNotify_Begin(This)	\
    (This)->lpVtbl -> ThreadNotify_Begin(This)

#define IThreadNotifys_ThreadNotify_End(This)	\
    (This)->lpVtbl -> ThreadNotify_End(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_Add_Proxy( 
    IThreadNotifys * This,
    /* [in] */ IThreadNotify *pThreadNotify);


void __RPC_STUB IThreadNotifys_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_Remove_Proxy( 
    IThreadNotifys * This,
    /* [in] */ IThreadNotify *pThreadNotify);


void __RPC_STUB IThreadNotifys_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_Clear_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_Clear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_ThreadNotify_Begin_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_ThreadNotify_Begin_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThreadNotifys_ThreadNotify_End_Proxy( 
    IThreadNotifys * This);


void __RPC_STUB IThreadNotifys_ThreadNotify_End_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThreadNotifys_INTERFACE_DEFINED__ */


#ifndef __IHeaderItem_INTERFACE_DEFINED__
#define __IHeaderItem_INTERFACE_DEFINED__

/* interface IHeaderItem */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IHeaderItem;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D9EDECA5-20A6-4db3-95DA-15C07D20721D")
    IHeaderItem : public IUnknown
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE HiGetData( 
            LONG_PTR lpLVITEM,
            LONG_PTR pDataObject,
            LONG_PTR pUserData) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE HiSort( 
            LONG_PTR pDataObject,
            LONG_PTR pUserData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHeaderItemVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHeaderItem * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHeaderItem * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHeaderItem * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *HiGetData )( 
            IHeaderItem * This,
            LONG_PTR lpLVITEM,
            LONG_PTR pDataObject,
            LONG_PTR pUserData);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *HiSort )( 
            IHeaderItem * This,
            LONG_PTR pDataObject,
            LONG_PTR pUserData);
        
        END_INTERFACE
    } IHeaderItemVtbl;

    interface IHeaderItem
    {
        CONST_VTBL struct IHeaderItemVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHeaderItem_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHeaderItem_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHeaderItem_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHeaderItem_HiGetData(This,lpLVITEM,pDataObject,pUserData)	\
    (This)->lpVtbl -> HiGetData(This,lpLVITEM,pDataObject,pUserData)

#define IHeaderItem_HiSort(This,pDataObject,pUserData)	\
    (This)->lpVtbl -> HiSort(This,pDataObject,pUserData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderItem_HiGetData_Proxy( 
    IHeaderItem * This,
    LONG_PTR lpLVITEM,
    LONG_PTR pDataObject,
    LONG_PTR pUserData);


void __RPC_STUB IHeaderItem_HiGetData_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderItem_HiSort_Proxy( 
    IHeaderItem * This,
    LONG_PTR pDataObject,
    LONG_PTR pUserData);


void __RPC_STUB IHeaderItem_HiSort_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHeaderItem_INTERFACE_DEFINED__ */


#ifndef __IHeaderCtrlEx_INTERFACE_DEFINED__
#define __IHeaderCtrlEx_INTERFACE_DEFINED__

/* interface IHeaderCtrlEx */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IHeaderCtrlEx;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F8FAF7D0-44C1-4368-9CFE-D2E5B8CB4A09")
    IHeaderCtrlEx : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nPos,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowItem( 
            int nKey,
            int nPos) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindItem( 
            int nKey,
            LONG_PTR *hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoSetting( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SubclassWindow( 
            LONG_PTR hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeItem( 
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Read( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Write( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            LONG *lCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_OtherTitle( 
            /* [retval][out] */ BSTR *strOther) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_OtherTitle( 
            BSTR strOther) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ToColumn( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHeaderCtrlExVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHeaderCtrlEx * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHeaderCtrlEx * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHeaderCtrlEx * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHeaderCtrlEx * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHeaderCtrlEx * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHeaderCtrlEx * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHeaderCtrlEx * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IHeaderCtrlEx * This,
            int nPos,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowItem )( 
            IHeaderCtrlEx * This,
            int nKey,
            int nPos);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindItem )( 
            IHeaderCtrlEx * This,
            int nKey,
            LONG_PTR *hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoSetting )( 
            IHeaderCtrlEx * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SubclassWindow )( 
            IHeaderCtrlEx * This,
            LONG_PTR hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeItem )( 
            IHeaderCtrlEx * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Read )( 
            IHeaderCtrlEx * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Write )( 
            IHeaderCtrlEx * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IHeaderCtrlEx * This,
            LONG *lCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OtherTitle )( 
            IHeaderCtrlEx * This,
            /* [retval][out] */ BSTR *strOther);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OtherTitle )( 
            IHeaderCtrlEx * This,
            BSTR strOther);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ToColumn )( 
            IHeaderCtrlEx * This);
        
        END_INTERFACE
    } IHeaderCtrlExVtbl;

    interface IHeaderCtrlEx
    {
        CONST_VTBL struct IHeaderCtrlExVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHeaderCtrlEx_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHeaderCtrlEx_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHeaderCtrlEx_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHeaderCtrlEx_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHeaderCtrlEx_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHeaderCtrlEx_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHeaderCtrlEx_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHeaderCtrlEx_InsertItem(This,nPos,nKey,hdItem)	\
    (This)->lpVtbl -> InsertItem(This,nPos,nKey,hdItem)

#define IHeaderCtrlEx_ShowItem(This,nKey,nPos)	\
    (This)->lpVtbl -> ShowItem(This,nKey,nPos)

#define IHeaderCtrlEx_FindItem(This,nKey,hdItem)	\
    (This)->lpVtbl -> FindItem(This,nKey,hdItem)

#define IHeaderCtrlEx_DoSetting(This)	\
    (This)->lpVtbl -> DoSetting(This)

#define IHeaderCtrlEx_SubclassWindow(This,hWnd)	\
    (This)->lpVtbl -> SubclassWindow(This,hWnd)

#define IHeaderCtrlEx_ChangeItem(This,dwMask,nKey,hdItem)	\
    (This)->lpVtbl -> ChangeItem(This,dwMask,nKey,hdItem)

#define IHeaderCtrlEx_Read(This,pConfig,strPath)	\
    (This)->lpVtbl -> Read(This,pConfig,strPath)

#define IHeaderCtrlEx_Write(This,pConfig,strPath)	\
    (This)->lpVtbl -> Write(This,pConfig,strPath)

#define IHeaderCtrlEx_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IHeaderCtrlEx_get_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> get_OtherTitle(This,strOther)

#define IHeaderCtrlEx_put_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> put_OtherTitle(This,strOther)

#define IHeaderCtrlEx_ToColumn(This)	\
    (This)->lpVtbl -> ToColumn(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_InsertItem_Proxy( 
    IHeaderCtrlEx * This,
    int nPos,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IHeaderCtrlEx_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ShowItem_Proxy( 
    IHeaderCtrlEx * This,
    int nKey,
    int nPos);


void __RPC_STUB IHeaderCtrlEx_ShowItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_FindItem_Proxy( 
    IHeaderCtrlEx * This,
    int nKey,
    LONG_PTR *hdItem);


void __RPC_STUB IHeaderCtrlEx_FindItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_DoSetting_Proxy( 
    IHeaderCtrlEx * This);


void __RPC_STUB IHeaderCtrlEx_DoSetting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_SubclassWindow_Proxy( 
    IHeaderCtrlEx * This,
    LONG_PTR hWnd);


void __RPC_STUB IHeaderCtrlEx_SubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ChangeItem_Proxy( 
    IHeaderCtrlEx * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IHeaderCtrlEx_ChangeItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_Read_Proxy( 
    IHeaderCtrlEx * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IHeaderCtrlEx_Read_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_Write_Proxy( 
    IHeaderCtrlEx * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IHeaderCtrlEx_Write_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_get_Count_Proxy( 
    IHeaderCtrlEx * This,
    LONG *lCount);


void __RPC_STUB IHeaderCtrlEx_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_get_OtherTitle_Proxy( 
    IHeaderCtrlEx * This,
    /* [retval][out] */ BSTR *strOther);


void __RPC_STUB IHeaderCtrlEx_get_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_put_OtherTitle_Proxy( 
    IHeaderCtrlEx * This,
    BSTR strOther);


void __RPC_STUB IHeaderCtrlEx_put_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IHeaderCtrlEx_ToColumn_Proxy( 
    IHeaderCtrlEx * This);


void __RPC_STUB IHeaderCtrlEx_ToColumn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHeaderCtrlEx_INTERFACE_DEFINED__ */


#ifndef __IListCtrlColumn_INTERFACE_DEFINED__
#define __IListCtrlColumn_INTERFACE_DEFINED__

/* interface IListCtrlColumn */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IListCtrlColumn;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("8452A1B5-E8C1-4AB9-A094-CA2D4CC75CF3")
    IListCtrlColumn : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            int nPos,
            int nKey,
            LONG_PTR lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ShowItem( 
            int nPos,
            int nKey,
            IHeaderItem *pHeaderItem,
            VARIANT_BOOL bShow) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE FindItem( 
            int nKey,
            LONG_PTR *lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SubclassWindow( 
            LONG_PTR hWnd) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeItem( 
            ULONG dwMask,
            int nKey,
            LONG_PTR lpColumn) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Read( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Write( 
            IDispatch *pConfig,
            BSTR strPath) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_Count( 
            LONG *lCount) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_OtherTitle( 
            /* [retval][out] */ BSTR *strOther) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_OtherTitle( 
            BSTR strOther) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ToColumn( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoMenu( 
            int x,
            int y) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Clear( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeHDItemParam( 
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ChangeHDItemParamEx( 
            /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
            /* [in] */ int nSize) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetColumnWidth( 
            int nKey,
            LONG_PTR *nWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE UnsubclassWindow( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoSetting( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IListCtrlColumnVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IListCtrlColumn * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IListCtrlColumn * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IListCtrlColumn * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IListCtrlColumn * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IListCtrlColumn * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IListCtrlColumn * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IListCtrlColumn * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertItem )( 
            IListCtrlColumn * This,
            int nPos,
            int nKey,
            LONG_PTR lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ShowItem )( 
            IListCtrlColumn * This,
            int nPos,
            int nKey,
            IHeaderItem *pHeaderItem,
            VARIANT_BOOL bShow);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *FindItem )( 
            IListCtrlColumn * This,
            int nKey,
            LONG_PTR *lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SubclassWindow )( 
            IListCtrlColumn * This,
            LONG_PTR hWnd);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeItem )( 
            IListCtrlColumn * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR lpColumn);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Read )( 
            IListCtrlColumn * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Write )( 
            IListCtrlColumn * This,
            IDispatch *pConfig,
            BSTR strPath);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_Count )( 
            IListCtrlColumn * This,
            LONG *lCount);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_OtherTitle )( 
            IListCtrlColumn * This,
            /* [retval][out] */ BSTR *strOther);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_OtherTitle )( 
            IListCtrlColumn * This,
            BSTR strOther);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ToColumn )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoMenu )( 
            IListCtrlColumn * This,
            int x,
            int y);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *Clear )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeHDItemParam )( 
            IListCtrlColumn * This,
            ULONG dwMask,
            int nKey,
            LONG_PTR hdItem);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ChangeHDItemParamEx )( 
            IListCtrlColumn * This,
            /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
            /* [in] */ int nSize);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetColumnWidth )( 
            IListCtrlColumn * This,
            int nKey,
            LONG_PTR *nWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *UnsubclassWindow )( 
            IListCtrlColumn * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoSetting )( 
            IListCtrlColumn * This);
        
        END_INTERFACE
    } IListCtrlColumnVtbl;

    interface IListCtrlColumn
    {
        CONST_VTBL struct IListCtrlColumnVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IListCtrlColumn_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IListCtrlColumn_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IListCtrlColumn_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IListCtrlColumn_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IListCtrlColumn_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IListCtrlColumn_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IListCtrlColumn_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IListCtrlColumn_InsertItem(This,nPos,nKey,lpColumn)	\
    (This)->lpVtbl -> InsertItem(This,nPos,nKey,lpColumn)

#define IListCtrlColumn_ShowItem(This,nPos,nKey,pHeaderItem,bShow)	\
    (This)->lpVtbl -> ShowItem(This,nPos,nKey,pHeaderItem,bShow)

#define IListCtrlColumn_FindItem(This,nKey,lpColumn)	\
    (This)->lpVtbl -> FindItem(This,nKey,lpColumn)

#define IListCtrlColumn_SubclassWindow(This,hWnd)	\
    (This)->lpVtbl -> SubclassWindow(This,hWnd)

#define IListCtrlColumn_ChangeItem(This,dwMask,nKey,lpColumn)	\
    (This)->lpVtbl -> ChangeItem(This,dwMask,nKey,lpColumn)

#define IListCtrlColumn_Read(This,pConfig,strPath)	\
    (This)->lpVtbl -> Read(This,pConfig,strPath)

#define IListCtrlColumn_Write(This,pConfig,strPath)	\
    (This)->lpVtbl -> Write(This,pConfig,strPath)

#define IListCtrlColumn_get_Count(This,lCount)	\
    (This)->lpVtbl -> get_Count(This,lCount)

#define IListCtrlColumn_get_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> get_OtherTitle(This,strOther)

#define IListCtrlColumn_put_OtherTitle(This,strOther)	\
    (This)->lpVtbl -> put_OtherTitle(This,strOther)

#define IListCtrlColumn_ToColumn(This)	\
    (This)->lpVtbl -> ToColumn(This)

#define IListCtrlColumn_DoMenu(This,x,y)	\
    (This)->lpVtbl -> DoMenu(This,x,y)

#define IListCtrlColumn_Clear(This)	\
    (This)->lpVtbl -> Clear(This)

#define IListCtrlColumn_ChangeHDItemParam(This,dwMask,nKey,hdItem)	\
    (This)->lpVtbl -> ChangeHDItemParam(This,dwMask,nKey,hdItem)

#define IListCtrlColumn_ChangeHDItemParamEx(This,nHeaderItem,nSize)	\
    (This)->lpVtbl -> ChangeHDItemParamEx(This,nHeaderItem,nSize)

#define IListCtrlColumn_GetColumnWidth(This,nKey,nWidth)	\
    (This)->lpVtbl -> GetColumnWidth(This,nKey,nWidth)

#define IListCtrlColumn_UnsubclassWindow(This)	\
    (This)->lpVtbl -> UnsubclassWindow(This)

#define IListCtrlColumn_DoSetting(This)	\
    (This)->lpVtbl -> DoSetting(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_InsertItem_Proxy( 
    IListCtrlColumn * This,
    int nPos,
    int nKey,
    LONG_PTR lpColumn);


void __RPC_STUB IListCtrlColumn_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ShowItem_Proxy( 
    IListCtrlColumn * This,
    int nPos,
    int nKey,
    IHeaderItem *pHeaderItem,
    VARIANT_BOOL bShow);


void __RPC_STUB IListCtrlColumn_ShowItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_FindItem_Proxy( 
    IListCtrlColumn * This,
    int nKey,
    LONG_PTR *lpColumn);


void __RPC_STUB IListCtrlColumn_FindItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_SubclassWindow_Proxy( 
    IListCtrlColumn * This,
    LONG_PTR hWnd);


void __RPC_STUB IListCtrlColumn_SubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeItem_Proxy( 
    IListCtrlColumn * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR lpColumn);


void __RPC_STUB IListCtrlColumn_ChangeItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_Read_Proxy( 
    IListCtrlColumn * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IListCtrlColumn_Read_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_Write_Proxy( 
    IListCtrlColumn * This,
    IDispatch *pConfig,
    BSTR strPath);


void __RPC_STUB IListCtrlColumn_Write_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_get_Count_Proxy( 
    IListCtrlColumn * This,
    LONG *lCount);


void __RPC_STUB IListCtrlColumn_get_Count_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_get_OtherTitle_Proxy( 
    IListCtrlColumn * This,
    /* [retval][out] */ BSTR *strOther);


void __RPC_STUB IListCtrlColumn_get_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_put_OtherTitle_Proxy( 
    IListCtrlColumn * This,
    BSTR strOther);


void __RPC_STUB IListCtrlColumn_put_OtherTitle_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ToColumn_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_ToColumn_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_DoMenu_Proxy( 
    IListCtrlColumn * This,
    int x,
    int y);


void __RPC_STUB IListCtrlColumn_DoMenu_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_Clear_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_Clear_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeHDItemParam_Proxy( 
    IListCtrlColumn * This,
    ULONG dwMask,
    int nKey,
    LONG_PTR hdItem);


void __RPC_STUB IListCtrlColumn_ChangeHDItemParam_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_ChangeHDItemParamEx_Proxy( 
    IListCtrlColumn * This,
    /* [in] */ LVCOLUMN_HEADER_ITEM *nHeaderItem,
    /* [in] */ int nSize);


void __RPC_STUB IListCtrlColumn_ChangeHDItemParamEx_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_GetColumnWidth_Proxy( 
    IListCtrlColumn * This,
    int nKey,
    LONG_PTR *nWidth);


void __RPC_STUB IListCtrlColumn_GetColumnWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_UnsubclassWindow_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_UnsubclassWindow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IListCtrlColumn_DoSetting_Proxy( 
    IListCtrlColumn * This);


void __RPC_STUB IListCtrlColumn_DoSetting_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IListCtrlColumn_INTERFACE_DEFINED__ */


#ifndef __ITreePropertySheet_INTERFACE_DEFINED__
#define __ITreePropertySheet_INTERFACE_DEFINED__

/* interface ITreePropertySheet */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITreePropertySheet;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F16A5240-C752-47B6-B34C-3F4ED7231FDC")
    ITreePropertySheet : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoNoModal( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetConfigPos( 
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetTreeCtrlWidth( 
            /* [in] */ int iWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTreeCtrlWidth( 
            /* [in] */ int *ipWidth) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITreePropertySheetVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITreePropertySheet * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITreePropertySheet * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITreePropertySheet * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITreePropertySheet * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITreePropertySheet * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITreePropertySheet * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITreePropertySheet * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            ITreePropertySheet * This,
            HWND hWndCtrl);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoNoModal )( 
            ITreePropertySheet * This,
            HWND hWndCtrl);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetConfigPos )( 
            ITreePropertySheet * This,
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetTreeCtrlWidth )( 
            ITreePropertySheet * This,
            /* [in] */ int iWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTreeCtrlWidth )( 
            ITreePropertySheet * This,
            /* [in] */ int *ipWidth);
        
        END_INTERFACE
    } ITreePropertySheetVtbl;

    interface ITreePropertySheet
    {
        CONST_VTBL struct ITreePropertySheetVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITreePropertySheet_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITreePropertySheet_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITreePropertySheet_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITreePropertySheet_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITreePropertySheet_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITreePropertySheet_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITreePropertySheet_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ITreePropertySheet_DoModal(This,hWndCtrl)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl)

#define ITreePropertySheet_DoNoModal(This,hWndCtrl)	\
    (This)->lpVtbl -> DoNoModal(This,hWndCtrl)

#define ITreePropertySheet_SetConfigPos(This,pxmlDoc,bsPath)	\
    (This)->lpVtbl -> SetConfigPos(This,pxmlDoc,bsPath)

#define ITreePropertySheet_SetTreeCtrlWidth(This,iWidth)	\
    (This)->lpVtbl -> SetTreeCtrlWidth(This,iWidth)

#define ITreePropertySheet_GetTreeCtrlWidth(This,ipWidth)	\
    (This)->lpVtbl -> GetTreeCtrlWidth(This,ipWidth)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_DoModal_Proxy( 
    ITreePropertySheet * This,
    HWND hWndCtrl);


void __RPC_STUB ITreePropertySheet_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_DoNoModal_Proxy( 
    ITreePropertySheet * This,
    HWND hWndCtrl);


void __RPC_STUB ITreePropertySheet_DoNoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_SetConfigPos_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ ULONG *pxmlDoc,
    /* [in] */ BSTR bsPath);


void __RPC_STUB ITreePropertySheet_SetConfigPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_SetTreeCtrlWidth_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ int iWidth);


void __RPC_STUB ITreePropertySheet_SetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ITreePropertySheet_GetTreeCtrlWidth_Proxy( 
    ITreePropertySheet * This,
    /* [in] */ int *ipWidth);


void __RPC_STUB ITreePropertySheet_GetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ITreePropertySheet_INTERFACE_DEFINED__ */


#ifndef __IRegsvrDll_INTERFACE_DEFINED__
#define __IRegsvrDll_INTERFACE_DEFINED__

/* interface IRegsvrDll */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IRegsvrDll;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("99EA1F94-5EAB-413F-BA1E-359FEBCCFA25")
    IRegsvrDll : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegsvrComFramework( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RegsvrComponent( 
            BSTR *bstrItem,
            int nSize) = 0;
        
        virtual /* [id][propget] */ HRESULT STDMETHODCALLTYPE get_ProgressNotifyText( 
            /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText) = 0;
        
        virtual /* [id][propput] */ HRESULT STDMETHODCALLTYPE put_ProgressNotifyText( 
            /* [in] */ IProgressNotifyText *pProgressNotifyText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IRegsvrDllVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IRegsvrDll * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IRegsvrDll * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IRegsvrDll * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IRegsvrDll * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IRegsvrDll * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IRegsvrDll * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IRegsvrDll * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegsvrComFramework )( 
            IRegsvrDll * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RegsvrComponent )( 
            IRegsvrDll * This,
            BSTR *bstrItem,
            int nSize);
        
        /* [id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_ProgressNotifyText )( 
            IRegsvrDll * This,
            /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText);
        
        /* [id][propput] */ HRESULT ( STDMETHODCALLTYPE *put_ProgressNotifyText )( 
            IRegsvrDll * This,
            /* [in] */ IProgressNotifyText *pProgressNotifyText);
        
        END_INTERFACE
    } IRegsvrDllVtbl;

    interface IRegsvrDll
    {
        CONST_VTBL struct IRegsvrDllVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IRegsvrDll_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IRegsvrDll_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IRegsvrDll_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IRegsvrDll_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IRegsvrDll_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IRegsvrDll_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IRegsvrDll_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IRegsvrDll_RegsvrComFramework(This)	\
    (This)->lpVtbl -> RegsvrComFramework(This)

#define IRegsvrDll_RegsvrComponent(This,bstrItem,nSize)	\
    (This)->lpVtbl -> RegsvrComponent(This,bstrItem,nSize)

#define IRegsvrDll_get_ProgressNotifyText(This,ppProgressNotifyText)	\
    (This)->lpVtbl -> get_ProgressNotifyText(This,ppProgressNotifyText)

#define IRegsvrDll_put_ProgressNotifyText(This,pProgressNotifyText)	\
    (This)->lpVtbl -> put_ProgressNotifyText(This,pProgressNotifyText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_RegsvrComFramework_Proxy( 
    IRegsvrDll * This);


void __RPC_STUB IRegsvrDll_RegsvrComFramework_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_RegsvrComponent_Proxy( 
    IRegsvrDll * This,
    BSTR *bstrItem,
    int nSize);


void __RPC_STUB IRegsvrDll_RegsvrComponent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propget] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_get_ProgressNotifyText_Proxy( 
    IRegsvrDll * This,
    /* [retval][out] */ IProgressNotifyText **ppProgressNotifyText);


void __RPC_STUB IRegsvrDll_get_ProgressNotifyText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id][propput] */ HRESULT STDMETHODCALLTYPE IRegsvrDll_put_ProgressNotifyText_Proxy( 
    IRegsvrDll * This,
    /* [in] */ IProgressNotifyText *pProgressNotifyText);


void __RPC_STUB IRegsvrDll_put_ProgressNotifyText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IRegsvrDll_INTERFACE_DEFINED__ */


#ifndef __IThreadNotifyInfoDlg_INTERFACE_DEFINED__
#define __IThreadNotifyInfoDlg_INTERFACE_DEFINED__

/* interface IThreadNotifyInfoDlg */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThreadNotifyInfoDlg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DF9C66BC-CC8A-48C2-890A-5BDD702B0A17")
    IThreadNotifyInfoDlg : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IThreadNotifyInfoDlgVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThreadNotifyInfoDlg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThreadNotifyInfoDlg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IThreadNotifyInfoDlg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IThreadNotifyInfoDlg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IThreadNotifyInfoDlgVtbl;

    interface IThreadNotifyInfoDlg
    {
        CONST_VTBL struct IThreadNotifyInfoDlgVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThreadNotifyInfoDlg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThreadNotifyInfoDlg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThreadNotifyInfoDlg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThreadNotifyInfoDlg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IThreadNotifyInfoDlg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IThreadNotifyInfoDlg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IThreadNotifyInfoDlg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IThreadNotifyInfoDlg_INTERFACE_DEFINED__ */


#ifndef __IManagerCallback_INTERFACE_DEFINED__
#define __IManagerCallback_INTERFACE_DEFINED__

/* interface IManagerCallback */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IManagerCallback;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D0B839F0-B198-4E74-9F1F-C59FAF5E8A60")
    IManagerCallback : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IManagerCallbackVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IManagerCallback * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IManagerCallback * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IManagerCallback * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IManagerCallback * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IManagerCallback * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IManagerCallback * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IManagerCallback * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IManagerCallbackVtbl;

    interface IManagerCallback
    {
        CONST_VTBL struct IManagerCallbackVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IManagerCallback_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IManagerCallback_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IManagerCallback_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IManagerCallback_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IManagerCallback_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IManagerCallback_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IManagerCallback_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IManagerCallback_INTERFACE_DEFINED__ */


#ifndef __ISheetPanel_INTERFACE_DEFINED__
#define __ISheetPanel_INTERFACE_DEFINED__

/* interface ISheetPanel */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ISheetPanel;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("42DB35E0-B3B5-4EFD-A841-C9DF0F2561C0")
    ISheetPanel : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl,
            int *piRet) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE InsertPageCLSID( 
            int iIndex,
            REFCLSID clsid) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ISheetPanelVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ISheetPanel * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ISheetPanel * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ISheetPanel * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ISheetPanel * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ISheetPanel * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ISheetPanel * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ISheetPanel * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            ISheetPanel * This,
            HWND hWndCtrl,
            int *piRet);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *InsertPageCLSID )( 
            ISheetPanel * This,
            int iIndex,
            REFCLSID clsid);
        
        END_INTERFACE
    } ISheetPanelVtbl;

    interface ISheetPanel
    {
        CONST_VTBL struct ISheetPanelVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ISheetPanel_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ISheetPanel_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ISheetPanel_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ISheetPanel_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ISheetPanel_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ISheetPanel_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ISheetPanel_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define ISheetPanel_DoModal(This,hWndCtrl,piRet)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl,piRet)

#define ISheetPanel_InsertPageCLSID(This,iIndex,clsid)	\
    (This)->lpVtbl -> InsertPageCLSID(This,iIndex,clsid)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE ISheetPanel_DoModal_Proxy( 
    ISheetPanel * This,
    HWND hWndCtrl,
    int *piRet);


void __RPC_STUB ISheetPanel_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE ISheetPanel_InsertPageCLSID_Proxy( 
    ISheetPanel * This,
    int iIndex,
    REFCLSID clsid);


void __RPC_STUB ISheetPanel_InsertPageCLSID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __ISheetPanel_INTERFACE_DEFINED__ */


#ifndef __IThumbnailInfo_INTERFACE_DEFINED__
#define __IThumbnailInfo_INTERFACE_DEFINED__

/* interface IThumbnailInfo */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IThumbnailInfo;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E58E96A8-8A36-4669-B873-1031E60A8329")
    IThumbnailInfo : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CreateSize( 
            /* [in] */ UINT uiWidth,
            /* [in] */ UINT uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSize( 
            /* [out] */ UINT *uiWidth,
            /* [out] */ UINT *uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetImageList( 
            /* [out] */ ULONG_PTR *pImageList) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IThumbnailInfoVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IThumbnailInfo * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IThumbnailInfo * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IThumbnailInfo * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IThumbnailInfo * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IThumbnailInfo * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IThumbnailInfo * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IThumbnailInfo * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CreateSize )( 
            IThumbnailInfo * This,
            /* [in] */ UINT uiWidth,
            /* [in] */ UINT uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetSize )( 
            IThumbnailInfo * This,
            /* [out] */ UINT *uiWidth,
            /* [out] */ UINT *uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetImageList )( 
            IThumbnailInfo * This,
            /* [out] */ ULONG_PTR *pImageList);
        
        END_INTERFACE
    } IThumbnailInfoVtbl;

    interface IThumbnailInfo
    {
        CONST_VTBL struct IThumbnailInfoVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IThumbnailInfo_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IThumbnailInfo_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IThumbnailInfo_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IThumbnailInfo_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IThumbnailInfo_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IThumbnailInfo_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IThumbnailInfo_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IThumbnailInfo_CreateSize(This,uiWidth,uiHeight)	\
    (This)->lpVtbl -> CreateSize(This,uiWidth,uiHeight)

#define IThumbnailInfo_GetSize(This,uiWidth,uiHeight)	\
    (This)->lpVtbl -> GetSize(This,uiWidth,uiHeight)

#define IThumbnailInfo_GetImageList(This,pImageList)	\
    (This)->lpVtbl -> GetImageList(This,pImageList)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_CreateSize_Proxy( 
    IThumbnailInfo * This,
    /* [in] */ UINT uiWidth,
    /* [in] */ UINT uiHeight);


void __RPC_STUB IThumbnailInfo_CreateSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_GetSize_Proxy( 
    IThumbnailInfo * This,
    /* [out] */ UINT *uiWidth,
    /* [out] */ UINT *uiHeight);


void __RPC_STUB IThumbnailInfo_GetSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IThumbnailInfo_GetImageList_Proxy( 
    IThumbnailInfo * This,
    /* [out] */ ULONG_PTR *pImageList);


void __RPC_STUB IThumbnailInfo_GetImageList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IThumbnailInfo_INTERFACE_DEFINED__ */


#ifndef __IPropertyPageDlg_INTERFACE_DEFINED__
#define __IPropertyPageDlg_INTERFACE_DEFINED__

/* interface IPropertyPageDlg */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IPropertyPageDlg;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("69FAA70B-CE69-4898-8C1C-139D11AF4010")
    IPropertyPageDlg : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoModal( 
            HWND hWndCtrl,
            UINT uiWidth,
            UINT uiHeight) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE DoNoModal( 
            HWND hWndCtrl) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetConfigPos( 
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetTreeCtrlWidth( 
            /* [in] */ int iWidth) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetTreeCtrlWidth( 
            /* [in] */ int *ipWidth) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPropertyPageDlgVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IPropertyPageDlg * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IPropertyPageDlg * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IPropertyPageDlg * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IPropertyPageDlg * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IPropertyPageDlg * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IPropertyPageDlg * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IPropertyPageDlg * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoModal )( 
            IPropertyPageDlg * This,
            HWND hWndCtrl,
            UINT uiWidth,
            UINT uiHeight);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *DoNoModal )( 
            IPropertyPageDlg * This,
            HWND hWndCtrl);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetConfigPos )( 
            IPropertyPageDlg * This,
            /* [in] */ ULONG *pxmlDoc,
            /* [in] */ BSTR bsPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetTreeCtrlWidth )( 
            IPropertyPageDlg * This,
            /* [in] */ int iWidth);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetTreeCtrlWidth )( 
            IPropertyPageDlg * This,
            /* [in] */ int *ipWidth);
        
        END_INTERFACE
    } IPropertyPageDlgVtbl;

    interface IPropertyPageDlg
    {
        CONST_VTBL struct IPropertyPageDlgVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPropertyPageDlg_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPropertyPageDlg_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPropertyPageDlg_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPropertyPageDlg_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IPropertyPageDlg_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IPropertyPageDlg_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IPropertyPageDlg_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IPropertyPageDlg_DoModal(This,hWndCtrl,uiWidth,uiHeight)	\
    (This)->lpVtbl -> DoModal(This,hWndCtrl,uiWidth,uiHeight)

#define IPropertyPageDlg_DoNoModal(This,hWndCtrl)	\
    (This)->lpVtbl -> DoNoModal(This,hWndCtrl)

#define IPropertyPageDlg_SetConfigPos(This,pxmlDoc,bsPath)	\
    (This)->lpVtbl -> SetConfigPos(This,pxmlDoc,bsPath)

#define IPropertyPageDlg_SetTreeCtrlWidth(This,iWidth)	\
    (This)->lpVtbl -> SetTreeCtrlWidth(This,iWidth)

#define IPropertyPageDlg_GetTreeCtrlWidth(This,ipWidth)	\
    (This)->lpVtbl -> GetTreeCtrlWidth(This,ipWidth)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_DoModal_Proxy( 
    IPropertyPageDlg * This,
    HWND hWndCtrl,
    UINT uiWidth,
    UINT uiHeight);


void __RPC_STUB IPropertyPageDlg_DoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_DoNoModal_Proxy( 
    IPropertyPageDlg * This,
    HWND hWndCtrl);


void __RPC_STUB IPropertyPageDlg_DoNoModal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_SetConfigPos_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ ULONG *pxmlDoc,
    /* [in] */ BSTR bsPath);


void __RPC_STUB IPropertyPageDlg_SetConfigPos_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_SetTreeCtrlWidth_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ int iWidth);


void __RPC_STUB IPropertyPageDlg_SetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IPropertyPageDlg_GetTreeCtrlWidth_Proxy( 
    IPropertyPageDlg * This,
    /* [in] */ int *ipWidth);


void __RPC_STUB IPropertyPageDlg_GetTreeCtrlWidth_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPropertyPageDlg_INTERFACE_DEFINED__ */



#ifndef __safToolsLib_LIBRARY_DEFINED__
#define __safToolsLib_LIBRARY_DEFINED__

/* library safToolsLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_safToolsLib;

#ifndef ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__
#define ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IHeaderCtrlExEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IHeaderCtrlExEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("D2D6AC3B-61BC-4E5A-8889-ADB811268729")
    _IHeaderCtrlExEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IHeaderCtrlExEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IHeaderCtrlExEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IHeaderCtrlExEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IHeaderCtrlExEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IHeaderCtrlExEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IHeaderCtrlExEventsVtbl;

    interface _IHeaderCtrlExEvents
    {
        CONST_VTBL struct _IHeaderCtrlExEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IHeaderCtrlExEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IHeaderCtrlExEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IHeaderCtrlExEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IHeaderCtrlExEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IHeaderCtrlExEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IHeaderCtrlExEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IHeaderCtrlExEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IHeaderCtrlExEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_HeaderCtrlEx;

#ifdef __cplusplus

class DECLSPEC_UUID("B6077849-E15A-40ED-8FCB-A2D12E3B07F2")
HeaderCtrlEx;
#endif

#ifndef ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__
#define ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IListCtrlColumnEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IListCtrlColumnEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("6EC5054B-2BCC-4700-BE85-E094513AAEC0")
    _IListCtrlColumnEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IListCtrlColumnEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IListCtrlColumnEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IListCtrlColumnEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IListCtrlColumnEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IListCtrlColumnEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IListCtrlColumnEventsVtbl;

    interface _IListCtrlColumnEvents
    {
        CONST_VTBL struct _IListCtrlColumnEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IListCtrlColumnEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IListCtrlColumnEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IListCtrlColumnEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IListCtrlColumnEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IListCtrlColumnEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IListCtrlColumnEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IListCtrlColumnEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IListCtrlColumnEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ListCtrlColumn;

#ifdef __cplusplus

class DECLSPEC_UUID("CAD97CB7-98BF-4A10-BC5D-0F0A296D11C6")
ListCtrlColumn;
#endif

#ifndef ___IDataViewsEvents_DISPINTERFACE_DEFINED__
#define ___IDataViewsEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IDataViewsEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IDataViewsEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("BA9140A9-23F3-4383-BE53-11CAF1E28D5B")
    _IDataViewsEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IDataViewsEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IDataViewsEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IDataViewsEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IDataViewsEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IDataViewsEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IDataViewsEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IDataViewsEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IDataViewsEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IDataViewsEventsVtbl;

    interface _IDataViewsEvents
    {
        CONST_VTBL struct _IDataViewsEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IDataViewsEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IDataViewsEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IDataViewsEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IDataViewsEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IDataViewsEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IDataViewsEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IDataViewsEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IDataViewsEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_DataViews;

#ifdef __cplusplus

class DECLSPEC_UUID("E1CDD842-F1EC-4D16-9988-DD067830F9F4")
DataViews;
#endif

#ifndef ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__
#define ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ITreePropertySheetEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__ITreePropertySheetEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("2949C4C1-3456-47C4-B73D-4C40EDEFBAE4")
    _ITreePropertySheetEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ITreePropertySheetEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ITreePropertySheetEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ITreePropertySheetEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ITreePropertySheetEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ITreePropertySheetEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _ITreePropertySheetEventsVtbl;

    interface _ITreePropertySheetEvents
    {
        CONST_VTBL struct _ITreePropertySheetEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ITreePropertySheetEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _ITreePropertySheetEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _ITreePropertySheetEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _ITreePropertySheetEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _ITreePropertySheetEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _ITreePropertySheetEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _ITreePropertySheetEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ITreePropertySheetEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_TreePropertySheet;

#ifdef __cplusplus

class DECLSPEC_UUID("F4FFC7A0-23A8-4E45-9FA1-13644F23AE60")
TreePropertySheet;
#endif

#ifndef ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__
#define ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IRegsvrDllEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IRegsvrDllEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("4872F864-1E3C-42C1-9D5F-7B89E63B2096")
    _IRegsvrDllEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IRegsvrDllEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IRegsvrDllEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IRegsvrDllEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IRegsvrDllEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IRegsvrDllEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IRegsvrDllEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IRegsvrDllEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IRegsvrDllEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IRegsvrDllEventsVtbl;

    interface _IRegsvrDllEvents
    {
        CONST_VTBL struct _IRegsvrDllEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IRegsvrDllEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IRegsvrDllEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IRegsvrDllEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IRegsvrDllEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IRegsvrDllEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IRegsvrDllEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IRegsvrDllEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IRegsvrDllEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_RegsvrDll;

#ifdef __cplusplus

class DECLSPEC_UUID("2D8855D8-CBF0-486A-A38A-29507DE5D17E")
RegsvrDll;
#endif

#ifndef ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__
#define ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IThreadNotifyInfoDlgEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IThreadNotifyInfoDlgEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("7236772D-3905-4C8A-9158-C3A9AAC0089C")
    _IThreadNotifyInfoDlgEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IThreadNotifyInfoDlgEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IThreadNotifyInfoDlgEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IThreadNotifyInfoDlgEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IThreadNotifyInfoDlgEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IThreadNotifyInfoDlgEventsVtbl;

    interface _IThreadNotifyInfoDlgEvents
    {
        CONST_VTBL struct _IThreadNotifyInfoDlgEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IThreadNotifyInfoDlgEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IThreadNotifyInfoDlgEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IThreadNotifyInfoDlgEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IThreadNotifyInfoDlgEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IThreadNotifyInfoDlgEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IThreadNotifyInfoDlgEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IThreadNotifyInfoDlgEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IThreadNotifyInfoDlgEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ThreadNotifyInfoDlg;

#ifdef __cplusplus

class DECLSPEC_UUID("83615B4A-DA05-4F56-B0FC-EE8DE0915A50")
ThreadNotifyInfoDlg;
#endif

#ifndef ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__
#define ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IManagerCallbackEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IManagerCallbackEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("5C2AEA8C-4D17-4BF6-B79D-2BE8CCD205AB")
    _IManagerCallbackEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IManagerCallbackEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IManagerCallbackEvents * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IManagerCallbackEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IManagerCallbackEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IManagerCallbackEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IManagerCallbackEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IManagerCallbackEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IManagerCallbackEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IManagerCallbackEventsVtbl;

    interface _IManagerCallbackEvents
    {
        CONST_VTBL struct _IManagerCallbackEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IManagerCallbackEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IManagerCallbackEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IManagerCallbackEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IManagerCallbackEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IManagerCallbackEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IManagerCallbackEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IManagerCallbackEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IManagerCallbackEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_ManagerCallback;

#ifdef __cplusplus

class DECLSPEC_UUID("D5E58ADC-12B0-471A-8F05-304A90054349")
ManagerCallback;
#endif

EXTERN_C const CLSID CLSID_SheetPanel;

#ifdef __cplusplus

class DECLSPEC_UUID("BD16F913-A422-4B40-8C4B-35D4688FCF9E")
SheetPanel;
#endif

EXTERN_C const CLSID CLSID_ThumbnailInfo;

#ifdef __cplusplus

class DECLSPEC_UUID("12546ABE-58DA-4AB2-BC78-ECA430BE7576")
ThumbnailInfo;
#endif

EXTERN_C const CLSID CLSID_PropertyPageDlg;

#ifdef __cplusplus

class DECLSPEC_UUID("46556A61-1487-4F04-8375-C9027551F31B")
PropertyPageDlg;
#endif
#endif /* __safToolsLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  HWND_UserSize(     unsigned long *, unsigned long            , HWND * ); 
unsigned char * __RPC_USER  HWND_UserMarshal(  unsigned long *, unsigned char *, HWND * ); 
unsigned char * __RPC_USER  HWND_UserUnmarshal(unsigned long *, unsigned char *, HWND * ); 
void                      __RPC_USER  HWND_UserFree(     unsigned long *, HWND * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


