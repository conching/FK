#ifndef __test_h__
#define __test_h__

#define FK_TEST_BOOL(fn, _b)\
{\
	if (fn==_b)\
	{\
		fk_TraceTest(FTEXTNIL, L"success, %s\n", _CRT_WIDE(#fn));\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail, %s\n", _CRT_WIDE(#fn));\
	}\
}


#define FK_TEST_IF(if_code)\
{\
	if (if_code)\
	{\
		fk_TraceTest(FTEXTNIL, L"success, %s\n", _CRT_WIDE(#if_code));\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail, %s\n", _CRT_WIDE(#if_code));\
	}\
}


#define FK_TEST_CALL_IF(call_code, if_code)\
{\
	call_code;\
	if (if_code)\
	{\
	fk_TraceTest(FTEXTNIL, L"success, call:%s if:%s\n", _CRT_WIDE(#call_code), _CRT_WIDE(#if_code));\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail, call:%s if:%s\n", _CRT_WIDE(#call_code), _CRT_WIDE(#if_code));\
	}\
}


#define FK_TEST_HRESULT_PTR(fn, _p)\
{\
	hr=fn;\
	if (hr==S_OK)\
	{\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail %s hr:%d\n", _CRT_WIDE(#fn), hr);\
	}\
}


#define FK_TEST_HRESULT(fn, _hr)\
{\
	hr=fn;\
	if (hr==_hr)\
	{\
		fk_TraceTest(FTEXTNIL, L"success, %s hr:%d\n", _CRT_WIDE(#fn), hr);\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail��?%s hr:%d\n", _CRT_WIDE(#fn), hr);\
	}\
}


#define FK_TEST_STRICMP(s1, s2, _ret)\
{\
	int iret;\
	iret=_tcsicmp(s1, s2);\
	if (iret==_ret)\
	{\
		fk_TraceTest(FTEXTNIL, L"success, _tcsicmp(%s, %s) iret=%d\n", s1, s2, iret);\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail_tcsicmp(%s, %s) iret=:%d\n", s1, s2, iret);\
	}\
}


#define FK_TEST_RETURN(fn)\
{\
	if (fn)\
	{\
		fk_TraceTest(FTEXTNIL, L"success, %s hr:%d\n", _CRT_WIDE(#fn));\
	}\
	else\
	{\
		fk_TraceTest(FTEXTNIL, L"fail��?%s hr:%d\n", _CRT_WIDE(#fn));\
	}\
}


#define FK_TEST_RUN(fText, szRunInfo, ...)	fk::LRunInfo	__RunInfo(NULL);\
	__RunInfo.OutInitInfo(NULL, fText, szRunInfo, _CRT_WIDE(__FUNCTION__), _CRT_WIDE(__FILE__), __LINE__, __VA_ARGS__);
#define FK_TEST_RUN_SUCCESS()				__RunInfo.RunSuccess();
#define FK_TEST_RUN_INFO(szInfo)			__RunInfo.RunInfo2(szInfo);
#define FK_TEST_RUN_FAIL()


#endif /* __test_h__ */
