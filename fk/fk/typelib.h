#ifndef __typelib_h__
#define __typelib_h__

#include "fk\plustype.h"


_FK_BEGIN

b_fncall InitTypeLib(void);
b_fncall UninitTypeLib(void);

_FK_END


#ifndef _FM_TYPE_LIB_
#pragma comment(lib, "typelib.lib")
#endif


#endif /* __typelib_h__ */
