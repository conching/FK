#ifndef __FkWin_h__
#define __FkWin_h__

#include "fk\TemplteProp.hxx"


_FK_BEGIN
extern IID IID_IDialog;			//{20B25255-9F7C-4889-BF7D-7F0422C06B4D}

class LWindow;
class LWindows;
class LWindowMuch;
class LRadNotifyMgr;

struct WINDOWCREATEINFO
{
	DWORD dwExStyle;
	LPCWSTR lpClassName;
	LPCWSTR lpWindowName;
	DWORD dwStyle;
	int X;
	int Y;
	int nWidth;
	int nHeight;
	HWND hWndParent;
	HMENU hMenu;
	//HINSTANCE hInstance;
	LPVOID lpParam;
};

#define FKCLASSNAME					"fkClassName"
#define FK_RAD_STATIC_DATA			"RAD_STATIC_DATA"


b_fncall ScintillaExLoad();
v_fncall ScintillaExUnload();


#define SWP_FK_TOP		0x00010000
//  = class LWindows

///////////////////////////////////////////////////////////////////////////////////////////////////
enum enumCreateWindwoObject
{
	// 这两个数据是单项选择。
	CWOF_DATA_XML		= 0x0001000,		// 创建的窗口数据, 从 xml 读出来的，必须写 hXmlNode 参数。  这两个数据 or 
	CWOF_DATA_DEFAULT	= 0x0002000,		// 创建窗口，初始化默认数据,不需要写 hXmlNode 参数。

	CWOF_DATA_RAD_STATE = 0x0000001,		// 窗口数据工作在 RAD 状态。			这个数据只在 Rad 状态存在。
	CWOF_DATA_VCRES_ID  = 0x0000002,		// 窗口是用 vc res 创建的，
};

typedef struct tagCREATE_WINDOW_OBJECT
{
	DWORD				dwSize;
	DWORD				dwMask;

	fk::LModule*		pmodule;
	fk::LWindowMuch*	pWindowMuchParent;

	LPSTR				lpszClassName;
	LPWSTR				lpszVarName;
	RECT				rcCtrl;

	fk::HXMLNODE		hXmlNode;
	fk::LVxmlFile*		pVxmlFile;
}CREATE_WINDOW_OBJECT,*LPCREATE_WINDOW_OBJECT;

typedef bool (_fncall* LPFNCREATE_WINDOW_OBJECT)(LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow** ppWindow);
typedef bool (_fncall* LPFNLINK_WINDOW_OBJECT)(LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow* pwindow);

typedef struct tagREGWINDOWCLASSITEM
{
	DWORD						dwSize;
	DWORD						dwMask;
	LPSTR						lpszClass;
	LPFNCREATE_WINDOW_OBJECT	lpfnCreateWindowObject;
	LPFNLINK_WINDOW_OBJECT		lpfnLinkWindowObject;
}REGWINDOWCLASSITEM,* LPREGWINDOWCLASSITEM;
b_fncall RegWindowObject(LPREGWINDOWCLASSITEM lpRegWindowItem);
b_fncall UnregWindowObject(LPFNCREATE_WINDOW_OBJECT lpfnCreateWindowObject);

b_fncall WindowObjectFindW(LPCWSTR lpszClassName);
b_fncall WindowObjectFindA(LPCSTR lpszClassName);

#ifdef UNICODE
#define WindowObjectFind		WindowObjectFindW
#else
#define WindowObjectFind		WindowObjectFindA
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
enum enumWINDOW_MASK
{
	WMF_WINDOWCREATE		= 0x00000001,				// 窗口是创建的。
	WMF_SUBCLASSWINDOW		= 0x00000002,				// 窗口是子类化。
	WMF_DIALOG_RES_TOOLS	= 0x00000004,				// 对话框的资源是使用 tools.dll 中的资源。
	WMF_CONTEXTMENU_PARENT	= 0x00000008,				// 把菜单发送给父窗口。

	//
	// 窗口集合，创建时存在 WM_VISIBLE ，_MaskView 变量中存在 WMF_WINDOW_MUCH_VISIBLE
	//
	// 在窗口创建结束后，在设置窗口 为 WS_VIVIBLE 风格。然后从 _MaskView 变量中移出 WMF_WINDOW_MUCH_VISIBLE
	//
	WMF_WINDOW_MUCH_VISIBLE = 0x00000010,
};


class _FK_OUT_CLASS LWindow : public fk::LObject
{
	_FK_RTTI_;

private:

protected:
	DWORD				_dwMaskView;					// 只使用 0 位字节，不使用FF位字节，0xFF000000
	WNDPROC				_OldWndProc;
	fk::LWindow*		_pWinParent;
	void*				_pPopupMenu;					// 以后换为
	fk::LObjectProps*	_pprops;						// 当前窗口所使用的属性.
	LPFNLINK_WINDOW_OBJECT	_lpfnLinkWindowObject;

public:
	_FK_DBGCLASSINFO_
	HWND				_hWnd;

	LWindow(void);
	LWindow(fk::LWindow* pWindowParent);
	virtual ~LWindow();

	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);

	/*
	 *	每一次添加属性之前和删除一个属性之后,就调用一次 CheckNewObjectProps 函数.
	 */
	b_call QueryNewObjectProps(void);
	b_call QueryRemoveObjectProps(void);

	fk::LPropItem* GetContextMenuPropItem(void);
	b_call SetContextMenuValue(LPCSTR lpszValue);
	b_call GetContextMenuValue(fk::LVar* pbContextMenuValue);

	virtual b_call IsRadState(void);
	virtual b_call LinkControlDataXml(fk::LPLINK_CONTROL_DATA* plcd);
	virtual b_call IsWindow();

	bool SubclassWindow(HWND hWnd);
	bool UnsubclassWindow(void);
	virtual b_call WindowCreate(WINDOWCREATEINFO* pwci);
	bool CreateWindowEx(DWORD dwExStyle,
    LPCTSTR lpClassName,
    LPCTSTR lpWindowName,
    DWORD dwStyle,
    int x,
    int y,
    int nWidth,
    int nHeight,
    HWND hWndParent,
    HMENU hMenu,
    HINSTANCE hInstance,
    LPVOID lpParam);

	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

	b_call CreateFkWindow(DWORD dwExStyle, LPCWSTR lpWindowName, DWORD dwStyle,
							int x,	int y,	int nWidth,	int nHeight, HWND hWndParent, int id, PVOID lpParam);

////////////////////////////////////////////////////////////////////////////////////////////////

	HWND CreateWindowEx(LPCTSTR lpstrWndClass, HWND hWndParent, LPRECT rect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL) throw();

	HWND Create(LPCTSTR lpstrWndClass, HWND hWndParent, LPRECT rect = NULL, LPCTSTR szWindowName = NULL,
		DWORD dwStyle = 0, DWORD dwExStyle = 0,
		HMENU MenuOrID = 0U, LPVOID lpCreateParam = NULL) throw();

	virtual void Attach(HWND hWndNew) throw();
	virtual void AttachModifyFont(HWND hWndNew) throw();
	virtual b_call InitFKWindow(void);

	static LPCWSTR GetWndClassName() throw();
	HWND Detach() throw();
	virtual bool DestroyWindow();

	// Attributes

	operator HWND() const throw()
	{ 
		return _hWnd; 
	}

	DWORD GetStyle() const throw();
	DWORD GetExStyle() const throw();

	LONG GetWindowLong(int nIndex) const throw();
	LONG_PTR GetWindowLongPtr(int nIndex);
	LONG SetWindowLong(int nIndex, LONG dwNewLong);
	LONG_PTR SetWindowLongPtr2(int nIndex, LONG_PTR dwNewLong);
	WORD GetWindowWord(int nIndex) const throw();
	WORD SetWindowWord(int nIndex, WORD wNewWord) throw();
	// Message Functions

	LRESULT SendMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0) throw();
	BOOL PostMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0) throw();
	BOOL SendNotifyMessage(UINT message, WPARAM wParam = 0, LPARAM lParam = 0) throw();
	// support for C style macros
	static LRESULT SendMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) throw();
	// Window Text Functions

	bool SetWindowText(LPCTSTR lpszString) throw();
	bool SetWindowText(fk::LVar& ptext) throw();

	int GetWindowText(__out_ecount_part_z(nMaxCount, return + 1) LPTSTR lpszStringBuf, __in int nMaxCount) const throw();
#ifdef _FK_USER_WTL_
	int GetWindowText(CSimpleString& strText) const;
#endif
	int GetWindowTextLength() const throw();

	void SetFont(HFONT hFont, BOOL bRedraw) throw();
	HFONT GetFont() const throw();
	// Menu Functions (non-child windows only)

	HMENU GetMenu() const throw();
	BOOL SetMenu(HMENU hMenu) throw();
	BOOL DrawMenuBar() throw();
	HMENU GetSystemMenu(BOOL bRevert) const throw();
	BOOL HiliteMenuItem(HMENU hMenu, UINT uItemHilite, UINT uHilite) throw();
	// Window Size and Position Functions

	BOOL IsIconic() const throw();
	BOOL IsZoomed() const throw();
	BOOL MoveWindow(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE) throw();
	BOOL MoveWindow(LPCRECT lpRect, BOOL bRepaint = TRUE) throw();
	BOOL SetWindowPos(HWND hWndInsertAfter, int x, int y, int cx, int cy, UINT nFlags) throw();
	BOOL SetWindowPos(HWND hWndInsertAfter, LPCRECT lpRect, UINT nFlags) throw();
	UINT ArrangeIconicWindows() throw();
	BOOL BringWindowToTop() throw();
	BOOL GetWindowRect(LPRECT lpRect) const throw();
	BOOL GetClientRect(LPRECT lpRect) const throw();
	BOOL GetWindowPlacement(WINDOWPLACEMENT FAR* lpwndpl) const throw();
	BOOL SetWindowPlacement(const WINDOWPLACEMENT FAR* lpwndpl) throw();
	// Coordinate Mapping Functions

	BOOL ClientToScreen(LPPOINT lpPoint) const throw();
	BOOL ClientToScreen(LPRECT lpRect) const throw();
	BOOL ScreenToClient(LPPOINT lpPoint) const throw();

	BOOL ScreenToClient(LPRECT lpRect) const throw();
	int MapWindowPoints(HWND hWndTo, LPPOINT lpPoint, UINT nCount) const throw();
	int MapWindowPoints(HWND hWndTo, LPRECT lpRect) const throw();
	// Update and Painting Functions

	HDC BeginPaint(LPPAINTSTRUCT lpPaint) throw();
	void EndPaint(LPPAINTSTRUCT lpPaint) throw();
	HDC GetDC() throw();
	HDC GetWindowDC() throw();
	int ReleaseDC(HDC hDC) throw();
	void Print(HDC hDC, DWORD dwFlags) const throw();
	void PrintClient(HDC hDC, DWORD dwFlags) const throw();
	BOOL UpdateWindow() throw();
	void SetRedraw(BOOL bRedraw) throw();
	BOOL GetUpdateRect(LPRECT lpRect, BOOL bErase = FALSE) throw();
	int GetUpdateRgn(HRGN hRgn, BOOL bErase = FALSE) throw();
	BOOL Invalidate(BOOL bErase = TRUE) throw();
	BOOL InvalidateRect(LPCRECT lpRect, BOOL bErase = TRUE) throw();
	BOOL ValidateRect(LPCRECT lpRect) throw();
	void InvalidateRgn(HRGN hRgn, BOOL bErase = TRUE) throw();
	BOOL ValidateRgn(HRGN hRgn) throw();
	BOOL ShowWindow(int nCmdShow) throw();
	BOOL IsWindowVisible() const throw();
	BOOL ShowOwnedPopups(BOOL bShow = TRUE) throw();
	HDC GetDCEx(HRGN hRgnClip, DWORD flags) throw();
	BOOL LockWindowUpdate(BOOL bLock = TRUE) throw();
	BOOL RedrawWindow(LPCRECT lpRectUpdate = NULL, HRGN hRgnUpdate = NULL, UINT flags = RDW_INVALIDATE | RDW_UPDATENOW | RDW_ERASE) throw();
	// Timer Functions

	UINT_PTR SetTimer(UINT_PTR nIDEvent, UINT nElapse, void (CALLBACK* lpfnTimer)(HWND, UINT, UINT_PTR, DWORD) = NULL) throw();
	BOOL KillTimer(UINT_PTR nIDEvent) throw();
	// Window State Functions

	BOOL IsWindowEnabled() const throw();
	BOOL EnableWindow(BOOL bEnable = TRUE) throw();

	HWND SetActiveWindow() throw();
	HWND SetCapture() throw();
	HWND SetFocus() throw();
	// Dialog-Box Item Functions

	BOOL CheckDlgButton(int nIDButton, UINT nCheck) throw();

	BOOL CheckRadioButton(int nIDFirstButton, int nIDLastButton, int nIDCheckButton) throw();
	int DlgDirList(__inout_z LPTSTR lpPathSpec, __in int nIDListBox, __in int nIDStaticPath, __in UINT nFileType) throw();
	int DlgDirListComboBox(__inout_z LPTSTR lpPathSpec, __in int nIDComboBox, __in int nIDStaticPath, __in UINT nFileType) throw();
	BOOL DlgDirSelect(__out_ecount_z(nCount) LPTSTR lpString, __in int nCount, __in int nIDListBox) throw();

	BOOL DlgDirSelectComboBox(__out_ecount_z(nCount) LPTSTR lpString, __in int nCount, __in int nIDComboBox) throw();
	UINT GetDlgItemInt(int nID, BOOL* lpTrans = NULL, BOOL bSigned = TRUE) const throw();
	UINT GetDlgItemText(__in int nID, __out_ecount_part_z(nMaxCount, return + 1) LPTSTR lpStr, __in int nMaxCount) const throw();
#ifdef _FK_USER_WTL_
	UINT GetDlgItemText(int nID, CSimpleString& strText) const;
#endif
#ifdef _OLEAUTO_H_
	bool GetDlgItemText(int nID, BSTR& bstrText) const throw();
#endif // _OLEAUTO_H_
	HWND GetNextDlgGroupItem(HWND hWndCtl, BOOL bPrevious) const throw();
	HWND GetNextDlgTabItem(HWND hWndCtl, BOOL bPrevious) const throw();
	UINT IsDlgButtonChecked(int nIDButton) const throw();
	LRESULT SendDlgItemMessage(int nID, UINT message, WPARAM wParam = 0, LPARAM lParam = 0) throw();
	BOOL SetDlgItemInt(int nID, UINT nValue, BOOL bSigned = TRUE) throw();
	BOOL SetDlgItemText(int nID, LPCTSTR lpszString) throw();
#ifndef _ATL_NO_HOSTING
	HRESULT GetDlgControl(int nID, REFIID iid, void** ppCtrl) throw();
	HRESULT GetDlgHost(int nID, REFIID iid, void** ppHost) throw();
#endif //!_ATL_NO_HOSTING

	// Scrolling Functions

	int GetScrollPos(int nBar) const throw();
	BOOL GetScrollRange(int nBar, LPINT lpMinPos, LPINT lpMaxPos) const throw();
	BOOL ScrollWindow(int xAmount, int yAmount, LPCRECT lpRect = NULL, LPCRECT lpClipRect = NULL) throw();
	int ScrollWindowEx(int dx, int dy, LPCRECT lpRectScroll, LPCRECT lpRectClip, HRGN hRgnUpdate, LPRECT lpRectUpdate, UINT uFlags) throw();

	int ScrollWindowEx(int dx, int dy, UINT uFlags, LPCRECT lpRectScroll = NULL, LPCRECT lpRectClip = NULL, HRGN hRgnUpdate = NULL, LPRECT lpRectUpdate = NULL) throw();
	int SetScrollPos(int nBar, int nPos, BOOL bRedraw) throw();
	BOOL SetScrollRange(int nBar, int nMinPos, int nMaxPos, BOOL bRedraw) throw();
	BOOL ShowScrollBar(UINT nBar, BOOL bShow = TRUE) throw();
	BOOL EnableScrollBar(UINT uSBFlags, UINT uArrowFlags = ESB_ENABLE_BOTH) throw();
	// Window Access Functions

	HWND ChildWindowFromPoint(POINT point) const throw();
	HWND ChildWindowFromPointEx(POINT point, UINT uFlags) const throw();
	HWND GetTopWindow() const throw();
	HWND GetWindow(UINT nCmd) const throw();
	HWND GetLastActivePopup() const throw();
	BOOL IsChild(HWND hWnd) const throw();
	HWND GetParent() const throw();
	HWND SetParent(HWND hWndNewParent) throw();
	// Window Tree Access

	int GetDlgCtrlID() const throw();
	int SetDlgCtrlID(int nID) throw();
	//HWND GetDlgItem(int nID) const throw();
	HWND GetDlgItem(int nID) const throw();
	// Alert Functions

	BOOL FlashWindow(BOOL bInvert) throw();
	int MessageBox(LPCTSTR lpszText, LPCTSTR lpszCaption = _T(""), UINT nType = MB_OK) throw();

	BOOL ChangeClipboardChain(HWND hWndNewNext) throw();
	HWND SetClipboardViewer() throw();
	BOOL OpenClipboard() throw();
	// Caret Functions

	BOOL CreateCaret(HBITMAP hBitmap) throw();
	BOOL CreateSolidCaret(int nWidth, int nHeight) throw();
	BOOL CreateGrayCaret(int nWidth, int nHeight) throw();
	BOOL HideCaret() throw();
	BOOL ShowCaret() throw();
#ifdef _INC_SHELLAPI
	// Drag-Drop Functions
	void DragAcceptFiles(BOOL bAccept = TRUE) throw();
#endif

	// Icon Functions

	HICON SetIcon(HICON hIcon, BOOL bBigIcon = TRUE) throw();
	HICON GetIcon(BOOL bBigIcon = TRUE) const throw();
	// Help Functions

	BOOL WinHelp(LPCTSTR lpszHelp, UINT nCmd = HELP_CONTEXT, DWORD dwData = 0) throw();
	BOOL SetWindowContextHelpId(DWORD dwContextHelpId) throw();
	DWORD GetWindowContextHelpId() const throw();
	// Hot Key Functions

	int SetHotKey(WORD wVirtualKeyCode, WORD wModifiers) throw();
	DWORD GetHotKey() const throw();
	// Misc. Operations

	//N new
	BOOL GetScrollInfo(int nBar, LPSCROLLINFO lpScrollInfo) throw();
	int SetScrollInfo(int nBar, LPSCROLLINFO lpScrollInfo, BOOL bRedraw) throw();
	BOOL IsDialogMessage(LPMSG lpMsg) throw();
	void NextDlgCtrl() const throw();
	void PrevDlgCtrl() const throw();
	void GotoDlgCtrl(HWND hWndCtrl) const throw();
	BOOL ResizeClient(int nWidth, int nHeight, BOOL bRedraw) throw();

	int GetWindowRgn(HRGN hRgn) throw();
	int SetWindowRgn(HRGN hRgn, BOOL bRedraw = FALSE) throw();
	HDWP DeferWindowPos(HDWP hWinPosInfo, HWND hWndInsertAfter, int x, int y, int cx, int cy, UINT uFlags) throw();
	DWORD GetWindowThreadID() throw();
	DWORD GetWindowProcessID() throw();
	//BOOL IsWindow() const throw();
	BOOL IsWindowUnicode() const throw();
	BOOL IsParentDialog() throw();
	BOOL ShowWindowAsync(int nCmdShow) throw();
	HWND GetDescendantWindow(int nID) const throw();
	void SendMessageToDescendants(UINT message, WPARAM wParam = 0, LPARAM lParam = 0, BOOL bDeep = TRUE) throw();
	BOOL CenterWindow(HWND hWndCenter = NULL) throw();
	BOOL CenterWindow(int iOfficeX, int iOfficeY, HWND hWndCenter = NULL) throw();
	BOOL ModifyStyle(DWORD dwRemove, DWORD dwAdd, UINT nFlags = 0) throw();
	BOOL ModifyStyleEx(DWORD dwRemove, DWORD dwAdd, UINT nFlags = 0) throw();
#ifdef _OLEAUTO_H_
	BOOL GetWindowText(BSTR* pbstrText) throw();
	BOOL GetWindowText(BSTR& bstrText) throw();
#endif // _OLEAUTO_H_

	BOOL GetWindowText(fk::LVar* pBaseData);

	BOOL GetWindowText(fk::LStringw* psText);

	HWND GetTopLevelParent() const throw();
	HWND GetTopLevelWindow() const throw();

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
										fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);

	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	virtual b_call WriteCtrlDataXml(fk::HXMLNODE hXmlParentNode);


};

class _FK_OUT_CLASS LWindowHandle : public LWindow
{
private:
protected:
public:
	LWindowHandle();
	virtual ~LWindowHandle(void);

	virtual bool DestroyWindow();
};

b_fncall ModifyWindowStyle(HWND hWnd, DWORD dwRemove, DWORD dwAdd);

enum umCloseWindow
{
	//CWL_ID=0x0001,
	CWL_LWINDOW=0x0002,
	//CWL_HWND=0x0003,
};

class _FK_OUT_CLASS LWindows : public fk::LObject
{
	_FK_RTTI_

protected:

public:
	LWindows();
	virtual ~LWindows();

	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual LWindow* GetLWindow(int iWndID) = 0;
	virtual b_call CreateWindowDefault(fk::LPCREATE_WINDOW_OBJECT lpcwd, fk::LWindow** ppWindow, LPARAM lParam) = 0;
	virtual b_call CloseWindow(DWORD dwData, umCloseWindow closeWindow) = 0;
	virtual v_call CloseFullWindow() = 0;

	virtual b_call DetachWindow(DWORD dwData, umCloseWindow closeWindow) = 0;
	virtual v_call DetachFullWindow() = 0;

	virtual b_call RemoveLWindow(LWindow* pWindow) = 0;
	virtual b_call AddWindow(LPCWSTR lpszVarName, fk::LWindow* pWindow, LPARAM lParam) = 0;
	virtual i_call GetCount(void) = 0;
	virtual i_call GetClassNameLen(void) = 0;

	virtual fk::LWindow* _call FindWindow(HWND hWndCtrl) = 0;
	virtual fk::LWindow* _call FindWindowVar(LPCWSTR lpszVarName) = 0;
	virtual fk::LWindow* _call FindWindowIsPos(int xPos, int yPos) = 0;
	virtual b_call RenWindowVar(LPCWSTR lpszOldVar, LPCWSTR lpszNewVar) = 0;

	virtual fk::LWindow* FindRegWindowItem(fk::LWindow* pwindow) = 0;
	virtual fk::LWindow* FindRegWindowItem(LPCWSTR lpszNewVar) = 0;

	virtual data_t* GetWindowDatas(void) = 0;

	virtual b_call CreateControlXml(fk::LModule* pmodule, LWindowMuch* pMuchParent, fk::LVxmlFile* pVxmlFile, LPCSTR lpszXmlPath) = 0;
	virtual b_call LinkControlDataXml(fk::LModule* pmodule, fk::LWindowMuch* pMuchParent, fk::LVxmlFile* pVxmlFile, LPCSTR lpszXmlPath) = 0;

	static LWindows* _call NewWindows(fk::LWindowMuch* pWindowMuchParent);

public:
	enum enumNotify
	{
		ON_AddControl		= 0x00000001,		// 添加一个控件。
		ON_RemoveControl	= 0x00000002,		// 移出一个控件。
		ON_RenControlName	= 0x00000004,		// 重命名控件名称。
	};

	typedef struct tagAddControlEvent
	{
		fk::NOTIFYEVENT ne;
		fk::LWindow*	pAddControl;
	}AddControlEvent,*LPAddControlEvent;

	typedef struct tagRemoveControlEvent
	{
		fk::NOTIFYEVENT ne;
		fk::LWindow*	pRemoveControl;
	}RemoveControlEvent,*LPRemoveControlEvent;

	typedef struct tagRenControlEvent
	{
		fk::NOTIFYEVENT ne;
		fk::LWindow*	pcontrol;
		LPCWSTR lpszOldVar;
		LPCWSTR lpszNewVar;
	}RenControlEvent,*LPRenControlEvent;
};

//
// 注册一个创建窗口类函数,  如何没有新的窗口类，可以不调用这里。
//
// b_fncall CreateObject(LWindow* pWindowParent, LPCSTR lpszClassName, fk::HXMLNODE hXmlNode, fk::LWindow** ppWindow, DWORD dwMake);
//
enum enumLinkDataExchange
{
	ldeInstallData		= 0x1,	// 安装控件数据。
	ldeDataToVariable	= 0x2,	// 控件数据到变量中。
	ldeClearVariable	= 0x3,	// 清空变量数据。
	ldeClearCtrl		= 0x4,	// 清空控件数据。
	ldeBuildFinalData	= 0x5,	// 生成最终数据。

	// 这里支持用户自定义类型。
};

typedef struct tagLINKOBJECTNAME
{
	fk::LObject**	pObject;
	wchar_t*		sVariable;
}LINKOBJECTNAME,*LPLINKOBJECTNAME;

/*
四种使用方式：

一.自动载入。
  call LWindoMuch(....)
  内部会自动调用 LoadVxmlCtrl(void);
二。手动载入文件。
	call SetVxmlFile();
	call LoadVxmlFile();
三。直接载入文件。
	call LoadVxmlFileCtrl(...);
四。载入外部配置文件。
	call LoadXmlConfig(...);
*/

//
// virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
// virtual v_call OnCommandProc(fk::LWindow* pWindow, int iNotify) = 0;			// 窗口命令走的是这个。
// virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr) = 0;
//
class _FK_OUT_CLASS LWindowMuch : public fk::LWindow
{
	_FK_RTTI_;

private:
	v_call DoOnVMBeginCreateControl(void);
	v_call DoOnVMEndCreateControl(void);
	v_call DoOnLoadVxmlCtrl(void);

	v_call SendLinkObjectEndMessage(void);

protected:
	LPWSTR				_lpszVxmlFile;
	int					_iBeginId;
	DWORD				_dwWindowMuchMask;

	fk::LObjects*		_pobjects;			// 当前窗口的对象列表。			// 这两个下一个版本会和为一个。
	fk::LWindows*		_pwindows;			// 当前窗口的窗口类列表。
	fk::LRadNotifyMgr*  _pRadNotifyMgr;
	fk::LModule*		_pHeadModule;		// 窗口所在dll 文件的 Module.
	void*				_pLinkObjectEnd;	//

	//
	// 这个函数写在了。 XulControl.cpp 文件中。
	//
	fk::enumReturn _fncall PropsInstallWindowValue(fk::LVxmlFile* pvxmlFile, fk::LModule* pmodule, fk::LWindowMuch* pWindowMuch);

	b_call CheckVxml(void);
	v_call ClearData(void);
	v_call CallCommandMessage(WPARAM wParam, LPARAM lParam);

	v_call CreateWindowQueryVisible(void);

	b_call LinkWindowObject(fk::LINKOBJECTNAME* plon, int iCount);
	b_call LinkNotifys(void);

public:
	enum enumWindowMuchState
	{
		WMSF_LoadControlXml	= 0x00000001,	// fk::LWindows 是在创建 fk::LDialog 创建控件状态。
		WMSF_Working		= 0x00000002,	// 正常状态。
	};

public:
	LWindowMuch(void);
	LWindowMuch(fk::LWindow* pwindow);
	LWindowMuch(fk::LModule* pHeadModule, fk::LWindow* pwindow, LPCWSTR lpszFileVxml);
	virtual ~LWindowMuch(void);

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void **ppv);

	// 最后一个基类实现。
	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

	b_call AddLinkObjectEndWMessage(fk::LWindow* pwin, UINT uimsg, WPARAM wparam, LPARAM lparam);
	b_call AddLinkObjectEndNMHDRMessage(fk::LWindow* pwin, UINT uimsg, WPARAM wparam, UINT uiChildMsg);
	//b_call IsLinkObjectState(void);

	//
	// 获得fk::LWindowMuch 状态， 返回 DWORD 类型的 enumWindowMuchState 状态。 
	// if (GetWindowMuchState() & fk::LWindowMuch::WMSF_LoadControlXml )
	//
	v_call ModifyWindowMuchState(DWORD dwRemoveState, DWORD dwAddState);
	dw_call GetWindowMuchState(void);	

	// class virtual: fk::LWindow
	virtual b_call IsRadState(void);
	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);
	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	virtual b_call InitFKWindow(void);				// 这个函数不要在重载。

	b_call LoadVxmlCtrl(void);
	b_call LoadVxmlFileCtrl(LPCWSTR lpszVxmlFile);
	b_call SetVxmlFile(LPCWSTR lpszVxmlFile);

	b_call LoadXmlConfig(fk::LVxmlFile* pVxmlFile, bool bReadDlg=true);
	b_call GetObjectVarName(LPCWSTR lpszClassName, fk::LStringw* pVarName);
	i_call NewCtrlID(void);
	LPWSTR GetVxmlFile(void);

	v_call ShowContextMenu(int x=-1, int y=-1);		// CONTEXTMENU

	fk::LWindows* _call GetWindows(void);
	fk::LObjects* _call GetObjects(void);
	fk::LModule* _call GetWindowModule(void);

	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, BYTE* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, short* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, int* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, UINT* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, long* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, DWORD* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, LONGLONG* pValue);
	v_call DDX_Text(DWORD umDataExchangeType, fk::LWindow* pwindow, ULONGLONG* pValue);
	v_call DDX_TextW(DWORD umDataExchangeType, fk::LWindow* pwindow, fk::LStringw* pValue);
	v_call DDX_TextA(DWORD umDataExchangeType, fk::LWindow* pwindow, fk::LStringa* pValue);
	v_call DDX_SetFormat(fk::LWindow* pwindow, HINSTANCE hRes, LPCWSTR lpszFormat, ...);

public:

};
extern IID IID_IWindowMuch;			// {4E1563A7-6008-4BBB-A1C7-7F246CF3FE13}


//
// 子类实现这三个函数。
// virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
// virtual v_call OnCommandProc(fk::LWindow* pWindow, int iNotify) = 0;			// 窗口命令走的是这个。
// virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr) = 0;
//
class _FK_OUT_CLASS LDialog : public fk::LWindowMuch
{
private:
protected:
	BOOL		_bModal;
	LPWSTR		_lpszResID;
	DWORD		_nHelpResID;
	UINT		_iAutoClosRet;

	HINSTANCE _call GetHinstanceRes();

public:
	LDialog(void);
	LDialog(fk::LModule* pHeadModule, fk::LWindow* pwindow, LPCWSTR lpszFileVxml);
	virtual ~LDialog();

	// 这两个资源是根据 pModule.
#if (_FK_OLD_VER<=0x0001)
	LDialog(fk::LModule* pHeadModule, UINT nResId);
	LDialog(fk::LModule* pHeadModule, UINT nResId, LPCWSTR lpszFileVxml);		//这个不要用，以后删除。
#endif

	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);

	virtual b_call SetPropValue(UINT uiPropID, UINT uiPropIDChild, LPARAM lParamValue);
	virtual b_call GetPropDisplayValue(UINT uiPropID, UINT uiPropIDChild, const fk::LVar* pBaseDataValue,
		fk::enumDataType umDisplayValue, fk::LStringw* spDisplayValueOut);

	virtual b_call GetPropValue(UINT uiPropID, UINT uiPropIDChild, fk::LVar* pBaseData);

	v_call SetAutoClose(UINT uiID, UINT uiTime);			// 支持 IDOK IDCANCEL等。 uiTime多少时间后，自动关闭，稍后支持。
	v_call CancelAutoClose(void);
	b_call GetAutoClose(void);

	b_call Create(HWND hWndParent, LPARAM lParam = NULL);
	b_call Create(HWND hWndParent, LPRECT lpRect, LPARAM lParam = NULL);
	i_call DoModal(HWND hWndParent, LPARAM lParam = NULL);

	b_call EndDialog(__in INT_PTR nResult);

public:
	// enumWindowMessage 是定义窗口基本 FK 消息组，包括一下控件 LWindow、LWindowMuch、LDialog三个类。
	enum enumViewMessage
	{
		OnBeforeCreateControl	= 0x00000001,			// 开始创建控件。
		OnAfterCreateControl	= 0x00000002,			// 结束创建控件。
	};
	typedef struct tagBeginCreateControl
	{
		fk::NOTIFYEVENT ne;
		fk::LWindow*  pWindow;
	}BeforeCreateControlEvent, LPBeforeCreateControlEvent;
	typedef struct tagEndCreateControl
	{
		fk::NOTIFYEVENT ne;
		fk::LWindow*  pWindow;
	}AfterCreateControlEvent, LPAfterCreateControlEvent;
};
//------------------------------------------------------------------------------------
// virtual lcall SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
#define FK_BEGIN_SUBWNDPROC(_CLASS_NAME_) \
LRESULT vir_call _CLASS_NAME_::SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)\
{\
	switch (uMsg)	{

#define FK_END_SUBWNDPROC(_BASE_CLASS_NAME_)\
	};\
	\
	return _BASE_CLASS_NAME_::SubWndProc(uMsg, wParam, lParam);\
}

//------------------------------------------------------------------------------------
// 	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
#define  FK_BEGIN_DIALOG_PROC(_CLASS_NAME_) \
	INT_PTR _call _CLASS_NAME_::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam) \
{\
	switch (uMsg)\
{

#define  FK_END_DIALOG_PROC(_BASE_CLASS_NAME_) \
};\
	\
	return _BASE_CLASS_NAME_::DialogProc(uMsg, wParam, lParam);\
}

//------------------------------------------------------------------------------------
#define FK_MESSAGE_HANDLER(_MSG_, _FUNC_CODE_, _RETURN_CODE_) \
	case _MSG_:\
{\
	_FUNC_CODE_;\
}_RETURN_CODE_;

//------------------------------------------------------------------------------------
// virtual v_call OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
#define FK_BEGIN_COMMAND_PROC(_CLASS_NAME_)\
vv_call _CLASS_NAME_::OnCommandProc(fk::LWindow* pWindow, int iNotify) \
{

#define FK_COMMAND_EVENT(_WINDOW_OBJECT_, _NOTIFY_, _FUNC_CODE_) \
	if(pWindow==_WINDOW_OBJECT_ && iNotify==_NOTIFY_)\
	{ \
		_FUNC_CODE_;\
		return ;\
	}

#define FK_END_COMMAND_PROC()\
};


//------------------------------------------------------------------------------------
// virtual v_call OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr);
#define FK_BEGIN_NOTIFY_PROC(_CLASS_NAME_)\
vv_call _CLASS_NAME_::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr) \
{

//FK_NOTIFY_EVENT(_pslvObject, NM_CLICK, OnObjectNMClick((LPNMITEMACTIVATE)lpnmhdr));
#define FK_NOTIFY_EVENT(_WINDOW_OBJECT_, _NOTIFY_, _FUNC_CODE_) \
if ( ( (pWindow==_WINDOW_OBJECT_) || (_WINDOW_OBJECT_->_hWnd==(HWND)pWindow) )&& lpnmhdr->code==_NOTIFY_)\
{ \
	_FUNC_CODE_;\
	return ;\
}

// FK_NOTIFY_OBJECT_EVENT(_pExample, _pExample->WmNotifyRefract(lpnmhdr));
#define FK_NOTIFY_OBJECT_EVENT(_WINDOW_OBJECT_, _FUNCTION_CODE_) \
		if ( (pWindow==_WINDOW_OBJECT_) || (_WINDOW_OBJECT_->_hWnd==(HWND)pWindow) )\
		{ \
			_FUNCTION_CODE_; \
			return;\
		}

#define FK_END_NOTIFY_PROC()\
};


//------------------------------------------------------------------------------------
// 这几个宏是windows 标准消息处理方式。
#define  FK_BEGIN_WM_COMMAND(uMsg, _WPARAM_, _LPARAM_)\
{\
	if (uMsg==WM_COMMAND)	\
	{\
		int iCmdID		= LOWORD(wParam);\
		int iNotifyID	= HIWORD(wParam);

#define FK_WM_COMMAND_ID(_CMD_ID_, func) \
	if(iCmdID==_CMD_ID_)\
{ \
	func;\
	return 0;\
}

#define FK_WM_COMMAND_ID_NOTIFY(_CMD_ID_, _NOTIFY_, func) \
	if(iCmdID==_CMD_ID_ && iNotifyID==_NOTIFY_)\
{ \
	func;\
	return 0;\
}

#define FK_END_WM_COMMAND()\
	} \
}


//------------------------------------------------------------------------------------
#define FK_COMMAND_HANDLE(_cmdid, _func, _return) \
	if(_cmdid == iCmdID)\
{\
	_func;\
	_return;\
}

#define FK_COMMAND_RANGE_FULL(idFirst, idLast, func, _return) \
	if((iCmdID >= idFirst) && (iCmdID <= idLast)){\
	func;\
	_return;\
	}

#define FK_COMMAND_TO_WND(hWndCtrl)\
	ATLASSERT(::IsWindow(hWndCtrl));\
	if(::SendMessage(hWndCtrl, WM_COMMAND, MAKEWPARAM(iCmdID, 0), 0))\
{\
	return S_OK;\
}

//------------------------------------------------------------------------------------
//#define FK_PROC_DEFINE  
//#define FK_NOTIFY_PROC_TYPE  vv_call _CLASS_NAME_::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
//#define FK_SUB_WND_PROC			SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
//#define FK_ON_COMMAND_PROC		OnCommandProc(fk::LWindow* pWindow, int iNotify)

//#define FK_BEGIN_EVENT_PROCESS(_RETURN, _CLASS_NAME, _PROCESS)    _RETURN _CLASS_NAME::_PROCESS{
//#define FK_END_EVENT_PROC(_BASE_CLAS)	 _BASE_CLAS; }

// 以下宏已经放弃，新版本不在使用。
#ifdef _FK_OLD_VER

#define FK_COMMAND_ID(_cmdid, func) \
	if(_cmdid == iCmdID)\
{\
	func(iCmdID, pCmdUI);\
	return S_OK;\
}

#define FK_COMMAND_CMDUI(_cmdid, func) \
	if(_cmdid == iCmdID){ \
	func(pCmdUI);\
	return S_OK;\
	}

#define FK_COMMAND_CODE(_IF_CODE_, _CALL_CODE_) \
	if(_IF_CODE_)\
	{ \
	_CALL_CODE_;\
	return S_OK;\
	}

#endif


_FK_END

#endif /* __FkWin_h__ */
