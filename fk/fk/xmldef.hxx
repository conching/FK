#ifndef __xmldef_h__
#define __xmldef_h__


/* 定义通用MSXML节点设置名称 */


_FK_BEGIN

static const wchar_t* S_EXT_VXML			= L".vxml";
static const wchar_t* S_MAINFRAME_FKFRAME	= L"FkFrame";


static const char* XMLF_CTRLID			= "CtrlID";
static const char* XMLF_TABLEOBJ		= "TableObj";
static const char* XMLF_FIELDNAME		= "FieldName";
static const char* XMLF_OBJECT_MAKE		= "ObjectMake";
static const char* XMLF_ITEM			= "item";
static const char* XMLF_ITEMS			= "items";
static const char* XMLF_ITEMS_SPRIT		= "/items";

static const wchar_t* XMLF_ITEMS_W			= L"items";
static const wchar_t* XMLF_ITEMS_SPRIT_W	= L"/items";

static const char* XMLF_NAME			= "name";
static const char* XMLF_MAINFRAMENAME	= "MainFrameName";
static const char* XMLF_CAPTION			= "caption";
static const char* XMLF_TEXT			= "text";
static const char* XMLF_DT				= "dt";
static const char* XMLF_DISPLAYDT		= "DisplayDT";
static const char* XMLF_WIDTH			= "width";
static const char* XMLF_LEFT_WIDTH		= "LeftWidth";
static const char* XMLF_RIGHT_WIDTH		= "RightWidth";

static const char* XMLF_HEIGHT			= "height";
static const char* XMLF_VISIBLE			= "visible";
static const char* XMLF_LEN				= "len";
static const char* XMLF_VALUE			= "value";
static const char* XMLF_INDEX			= "index";
static const char* XMLF_ID				= "id";
static const char* XMLF_ITEMSOBJ		= "ItemsObj";
static const char* XMLF_MAKE			= "make";
static const char* XMLF_PANEL_MAKE		= "PanelMake";
static const char* XMLF_FONT			= "font";
static const char* XMLF_SIZE			= "size";
static const char* XMLF_Quote			= "quote";
static const char* XMLF_CLASS_POPUP_MENU= "ClassPopupMenu";
static const char* XMLF_ITEM_CLASS		= "ItemClass";
static const char* XMLF_DLT_CLASSNAME	= "DLTClassName";

static const char* XMLF_USER_MAKE		= "UserMake";
static const char* XMLF_GUID			= "guid";
static const char* XMLF_GLINK			= "glink";
static const char* XMLF_KEY				= "key";
static const char* XMLF_LEFT			= "left";
static const char* XMLF_TOP				= "top";
static const char* XMLF_RIGHT			= "right";
static const char* XMLF_BOTTOM			= "bottom";
static const char* XMLF_PATH			= "path";
static const char* XMLF_SHORT_PATH		= "ShortPath";

static const char* XMLF_PARAM			= "param";
static const char* XMLF_LOAD			= "load";
static const char* XMLF_PROP_NAME		= "PropName";
static const char* XMLF_WHILE_COUNT		= "WhileCount";
static const char* XMLF_ROOT_PATH		= "RootPath";
static const char* XMLF_CHILD_NODE		= "ChildNode";
static const char* XMLF_GO_PATH			= "GoPath";
static const char* XMLF_INFO			= "info";
static const char* XMLF_FRL				= "frl";
static const char* XMLF_URL				= "url";
static const char* XMLF_BASES			= "bases";
static const char* XMLF_ARRAY_INITCOUNT	= "InitCount";
static const char* XMLF_ARRAY_NEWCOUNT	= "NewCount";
static const char* XMLF_ARRAY			= "array";
static const char* XMLF_ARRAY_SPRIT		= "/array";

static const char* XMLF_SDI				= "sdi";
static const char* XMLF_SDI_SPRIT		= "/sdi";
static const char* XMLF_SDI_SPLITTER	= "SDISplitter";
static const char* XMLF_SDI_SPLITTER_SPRIT	= "/SDISplitter";


static const char* XMLF_CMDS_PATH		= "/config/cmds";
static const char* XMLF_CMD_COUNT		= "CmdCount";
static const char* XMLF_LINE			= "line";
static const char* XMLF_STYLEEX			= "StyleEx";
static const char* XMLF_STYLE			= "style";
static const char* XMLF_RECT			= "rect";
static const char* XMLF_BMP				= "bmp";
static const char* XMLF_VER				= "ver";
static const char* XMLF_BITMAP			= "bitmap";
static const char* XMLF_BUTWIDTH		= "ButWidth";
static const char* XMLF_BUTHEIGHT		= "ButHeight";
static const char* XMLF_POPUP			= "popup";
static const char* XMLF_CLSID_CHILD		= "clsid";
static const char* XMLF_IID_KEY			= "iidkey";
static const char* XMLF_POS				= "pos";
static const char* XMLF_PARENT_IID		= "ParentIID";
static const char* XMLF_ROLL			= "roll";
static const char* XMLF_TITLE			= "title";
static const char* XMLF_MENU			= "menu";
static const char* XMLF_ICON			= "icon";
static const char* XMLF_FRAMETYPE		= "FrameType";
static const char* XMLF_CELL_TYPE		= "CellType";
static const char* XMLF_SPLITTERTYPE	= "SplitterType";
static const char* XMLF_SPLITTERWND_RESIZEPOS	= "ResizePos";
static const char* XMLF_SHOW_TITLE			= "ShowTitle";
static const char* XMLF_CMDGUID				= "CmdGuid";
static const char* XMLF_BEGIN_REPAIR_HINT	= "BeginRepairHint";
static const char* XMLF_REPAIR_HINT			= "RepairHint";
static const char* XMLF_XML_PATH			= "XmlPath";
static const char* XMLF_RES					= "res";
static const wchar_t* XMLF_RES_W			= L"res";

static const char* XMLF_OBJ					= "obj";
static const char* XMLF_DLL					= "dll";
static const char* XMLF_RUNL				= "runl";
static const char* XMLF_RCLSID				= "rclsid";
static const char* XMLF_CLS_CONTEXT			= "ClsContext";
static const char* XMLF_RIID				= "riid";

static const char* XMLF_CLASS				= "class";
static const char* XMLF_OPEN				= "open";
static const char* XMLF_FILE				= "file";
static const char* XMLF_TARGETFILE			= "TargetFile";
static const char* XMLF_EXT					= "ext";
static const char* XMLF_FILEMODE			= "FileMode";
static const char* XMLF_TYPE				= "type";
static const char* XMLF_DATACONNECTION	= "DataConnection";
static const char* XMLF_CONNECTION_TYPE	= "ConnectionType";
static const char* XMLF_CONFIG			= "config";
static const char* XMLF_1_0				= "1.0";
static const char* XMLF_PROP			= "prop";
static const char* XMLF_PROPID			= "PropID";
static const char* XMLF_PROPID_CHILD	= "PropIDChild";
static const char* XMLF_LINK_OBJECT		= "LinkObject";

//static const char* XMLF_LINK			= "link";				// 这个节点无法写html 文件 <textarea>节点中。所以不在使用了。
static const char* XMLF_GROUP			= "group";

static const char* XMLF_MODULE_ID			= "ModuleID";
static const char* XMLF_MODULE_KEY			= "ModuleKey";
static const char* XMLF_CONFIG_FILE_GUID	= "ConfigFileGuid";

static const char* XMLF_GENERICPANESTYLE	= "GenericPaneStyle";
static const char* XMLF_COMMANDBARSTYPE		= "CommandBarsType";

static const char* XMLF_PLUGINS_ITEMS_PATH	= "//config/Plugins/items";
static const char* XMLF_MDI_ITEMS_PATH		= "//config/MainFrame/mdi/items";
static const char* XMLF_NOTIFY_MACROS_PATH = "//config/macros";


static const char* XMLF_FKVER				= "fkver";
static const char* XMLF_SVER				= "sver";

static const char* XMLF_FUNCTION			= "function";

static const char* XMLF_COLUMNMASK			= "ColumnMask";
static const char* XMLF_FMT					= "fmt";
static const char* XMLF_CX					= "cx";
static const char* XMLF_PSZ_TEXT			= "pszText";
static const char* XMLF_CCH_TEXT_MAX		= "cchTextMax";
static const char* XMLF_IIMAGE				= "iImage";
static const char* XMLF_IORDER				= "iOrder";
static const char* XMLF_STATIC_CONST_GUID	= "StaticConstGuid";
static const char* XMLF_ORDER				= "order";

static const char* XMLF_DebugMake			=	"DebugMake";


//
// dna xml 文件路径
//
static const char* XMLF_DNA						= "dna";
static const char* XMLF_DNA_PATH				= "//config/dna";
static const char* XMLF_DNAMODULE_PATH_ITEMS	= "//config/DnaModule/items";

static const char* XMLF_DNA_LINKS_PATH			= "//config/dna/links";
static const char* XMLF_DNA_LINKS_ITEMS_PATH	= "//config/dna/links/items";
static const char* XMLF_DNA_CLIENT_LINKS_PATH	= "//config/dna/client/links";

static const char* XMLF_MODULECONFIG_DNAPOSITION_FIELD_PATH         = "//config/ModuleConfigColumn/DnaPosition";
static const char* XMLF_MODULECONFIG_DNAPOSITION_ITEM_FIELD_PATH    = "//config/ModuleConfigColumn/DnaPositionItem";


#define		XMLF_FRAME_CFG_PLATFORM			"/config/frame/mframe/ShowPlatform"


static const char* S_CLASS_STATIC = "fk::LWindow";
static const char* S_CLASS_BUTTON = "fk::LButton";
static const char* S_CLASS_LISTBOX = "fk::LListBox";
static const char* S_CLASS_COMBOBOX = "fk::LComboBox";
static const char* S_CLASS_EDIT = "fk::LEdit";
static const char* S_CLASS_HEADERCTRL = "fk::LHeaderCtrl";
static const char* S_CLASS_SCROLLBAR = "fk::LScrollBar";
static const char* S_CLASS_SYSLISTVIEW32 = "fk::LSysListView32";
static const char* S_CLASS_TREEVIEWCTRL = "fk::LSysTreeView32";
static const char* S_CLASS_TOOLBARCTRL = "fk::LToolBarCtrl";
static const char* S_CLASS_TABCTRL = "fk::LSysTabControl32";
static const char* S_CLASS_TOOLTIPCTRL = "fk::LToolTipCtrl";
static const char* S_CLASS_TRACKBARCTRL = "fk::LTrackBar32";
static const char* S_CLASS_UPDOWNCTRL = "fk::LUpDownCtrl";
static const char* S_CLASS_PROGRESSBARCTRL = "fk::LProgressBarCtrl";
static const char* S_CLASS_HOTKEYCTRL = "fk::LHotKeyCtrl";
static const char* S_CLASS_ANIMATECTRL = "fk::LAnimateCtrl";
static const char* S_CLASS_RICHEDITCTRL = "fk::LRichEditCtrl";
static const char* S_CLASS_DRAGLISTBOX = "fk::LDragListBox";
static const char* S_CLASS_REBARCTRL = "fk::LRebarCtrl";
static const char* S_CLASS_COMBOBOXEX = "fk::LComboBoxEx";
static const char* S_CLASS_DATETIMEPICKERCTRL = "fk::LSysDateTimePick32";
static const char* S_CLASS_MONTHCALENDARCTRL = "fk::LMonthCalendarCtrl";
static const char* S_CLASS_FLATSCROLLBAR = "fk::LFlatScrollBar";
static const char* S_CLASS_IPADDRESSCTRL = "fk::LIPAddressCtrl";
static const char* S_CLASS_PAGERCTRL = "fk::LPagerCtrl";
static const char* S_CLASS_LINKCTRL = "fk::LLinkCtrl";
static const char* S_CLASS_DIALOG = "fk::LDialog";


static const wchar_t* S_CLASS_OBJECT_W = L"fk::LObject";
static const wchar_t* S_CLASS_STATIC_W = L"fk::LStatic";
static const wchar_t* S_CLASS_BUTTON_W = L"fk::LButton";
static const wchar_t* S_CLASS_CHECKBOX_W = L"fk::LCheckBox";
static const wchar_t* S_CLASS_RADIO_BUTTON_W = L"fk::LRadioButton";
static const wchar_t* S_CLASS_GROUP_BOX_W = L"fk::LGroupBox";
static const wchar_t* S_CLASS_LISTBOX_W = L"fk::LListBox";
static const wchar_t* S_CLASS_COMBOBOX_W = L"fk::LComboBox";
static const wchar_t* S_CLASS_EDIT_W = L"fk::LEdit";
static const wchar_t* S_CLASS_HEADERCTRL_W = L"fk::LHeaderCtrl";
static const wchar_t* S_CLASS_SCROLLBAR_W = L"fk::LScrollBar";
static const wchar_t* S_CLASS_SYSLISTVIEW32_W = L"fk::LSysListView32";
static const wchar_t* S_CLASS_TREEVIEWCTRL_W = L"fk::LSysTreeView32";
static const wchar_t* S_CLASS_TOOLBARCTRL_W = L"fk::LToolBarCtrl";
static const wchar_t* S_CLASS_TABCTRL_W = L"fk::LSysTabControl32";
static const wchar_t* S_CLASS_TOOLTIPCTRL_W = L"fk::LToolTipCtrl";
static const wchar_t* S_CLASS_TRACKBARCTRL_W = L"fk::LTrackBar32";
static const wchar_t* S_CLASS_UPDOWNCTRL_W = L"fk::LUpDownCtrl";
static const wchar_t* S_CLASS_PROGRESSBARCTRL_W = L"fk::LProgressBarCtrl";
static const wchar_t* S_CLASS_HOTKEYCTRL_W = L"fk::LHotKeyCtrl";
static const wchar_t* S_CLASS_ANIMATECTRL_W = L"fk::LAnimateCtrl";
static const wchar_t* S_CLASS_RICHEDITCTRL_W = L"fk::LRichEditCtrl";
static const wchar_t* S_CLASS_DRAGLISTBOX_W = L"fk::LDragListBox";
static const wchar_t* S_CLASS_REBARCTRL_W = L"fk::LRebarCtrl";
static const wchar_t* S_CLASS_COMBOBOXEX_W = L"fk::LComboBoxEx";
static const wchar_t* S_CLASS_DATETIMEPICKERCTRL_W = L"fk::LSysDateTimePick32";
static const wchar_t* S_CLASS_MONTHCALENDARCTRL_W = L"fk::LMonthCalendarCtrl";
static const wchar_t* S_CLASS_FLATSCROLLBAR_W = L"fk::LFlatScrollBar";
static const wchar_t* S_CLASS_IPADDRESSCTRL_W = L"fk::LIPAddressCtrl";
static const wchar_t* S_CLASS_PAGERCTRL_W = L"fk::LPagerCtrl";
static const wchar_t* S_CLASS_LINKCTRL_W = L"fk::LLinkCtrl";
static const wchar_t* S_CLASS_DIALOG_W = L"fk::LDialog";
static const wchar_t* S_CLASS_WINDOW1234567890 = L"fk::LWindow1234567890";
static const wchar_t* S_CLASS_OBJECT1234567890 = L"fk::LObject1234567890";
static const wchar_t* S_CLASS_1234567890 = L"fk::L1234567890";

static const wchar_t* S_CLASS_OBJECT_REG_ITEM_W = L"fk::LObjectRegItem";
static const wchar_t* S_CLASS_OBJECT_REG_W = L"fk::LObjectReg";
static const wchar_t* S_CLASS_OBJECT_REG_MGR_W = L"fk::LObjectRegMgr";

static const char* S_CLASS_ENUMPLUS				= "fk::LEnumPlus";
static const wchar_t* S_CLASS_ENUMPLUS_W		= L"fk::LEnumPlus";
static const char* S_CLASS_MACRO				= "fk::LMacro";

static const char* S_CLASS_DATA_CONNECTIONA		= "fk::LDataConnection";
static const char* S_CLASS_DATA_CONNECTIONA_XML	= "fk::LDataConnectionXml";
static const char* S_CLASS_TABLEA				= "fk::LTable";
static const char* S_CLASS_XML_TABLEA			= "fk::LXmlTable";

static const char* S_CLASS_DBLISTVIEWA		= "fk::LDBListView";
static const char* S_CLASS_DBCHECKBUTTON	= "fk::LDBCheckButton";
static const char* S_CLASS_DBLISTBOX		= "fk::LDBListBox";
static const char* S_CLASS_DBCOMBOBOX		= "fk::LDBComboBox";
static const char* S_CLASS_DBEDIT			= "fk::LDBEdit";
static const char* S_CLASS_DATETIMEPICKER	= "fk::LDBDateTimePicker";



static const wchar_t* S_CLASS_DATA_CONNECTIONA_W	= L"fk::LDataConnection";
static const wchar_t* S_CLASS_TABLEA_W				= L"fk::LTable";



static const char* S_CLASS_XMLNODEITEMSMGR	= "fk::LXmlNodeItemsMgr";
static const char* S_CLASS_XMLNOTIFY		= "fk::LXmlNotify";
static const char* S_CLASS_XMLEDIT			= "fk::LXmlEdit";
static const char* S_CLASS_XMLCHECKBUTTON	= "fk::LXmlCheckButton";
static const char* S_CLASS_XMLRADIOBUTTON	= "fk::LXmlRadioButton";
static const char* S_CLASS_XMLCOMBOBOX		= "fk::LXmlComboBox";


static const char* XMLF_DLG_PATH			= "//config/dlg";
static const char* XMLF_OBJECTS_PATH		= "//config/objects";
static const char* XMLF_CONTROL_PATH		= "//config/control";
static const char* XMLF_CONFIG_PATH			= "//config";
static const char* XMLF_CONFIG_HEAD_PATH	= "//config/head";

static const char* XMLF_CONTROL_ITEMS_PATH	= "//config/control/items";
static const char* XMLF_OBJECTS_ITEMS_PATH	= "//config/objects/items";
static const wchar_t* XMLF_MAINFRAME_ITEM_PATHW	= L"//config/MainFrame";

static const char* XMLF_FLASHWINDOW_SHOWTIMER	= "//config/FlashWindow";
//static const char* XMLF_FLASHWINDOW_SHOW		= "//config/FlashWindow";
static const char* XMLF_SHOWTIMER				= "ShowTimer";
static const char* XMLF_SHOW					= "show";

static const char* XMLF_CONFIG_FONTS_PATH		= "//config/fonts";

static const wchar_t* S_CLASS_XML_TABLEA_W		= L"fk::LXmlTable";


static const char* XMLF_DEFAULT				= "default";

static const char* XMLF_MODIFYCOMOBJECTPATHSLEEPTIME	= "//config/Sys/ModifyComObjectPathSleepTime";
static const char* XMLF_SLEEP							= "sleep";


static const char* XMLF_WIZARD_ClassNamePrefixPath = "//config/dlt/wizard/NewClassName/ClassNamePrefix";
static const char* XMLF_WIZARD_ClassNamePrefixItemsPath = "//config/dlt/wizard/NewClassName/ClassNamePrefix/items";
/*
以下定义的数据，有部分数据是某些程序专用的，逐渐整合到程序中。
*/

#define		SCTKEY_SCT_SYSTEM				L"Sys"
#define		SCTKEY_MAIN_FRAME				L"MainFrame"
#define		SCTKEY_RECT						L"Rect"
#define		SCKEY_MAXIMIZE					L"Maximize"
#define		SCTKEY_MAIN_FRAME_PATH			L"//Sys/MainFrame"
#define		SCTKEY_MAIN_FRAME_RECT_PATH		L"//Sys/MainFrame/%s/Rect"
#define		SCTKEY_MAXIMIZE_PATH			L"//Sys/MainFrame/%s/Maximize"
#define		SXML_KEY_HELP_FILE_PATH			"//Sys/HelpFile"
#define		SCTKEY_SPLITTERWIDTH_PATH				L"//Sys/SplitterWidth"
#define		SCTKEY_INSTALL_UNHANDLED_EXCEPTION		"//Sys/InstallUnhandledExceptionFilter"


#define		SCTKEY_WINDOW_TEXT_FONT			L"//Sys/ui/window_text/font"
#define		SCTKEY_WINDOW_TEXT_FONT_SIZE	L"//Sys/ui/window_text/size"


#define		SCTKEY_APP_ONE_RUN_PATH			L"//Sys/OneRun"
#define		SCTKEY_SCT_LANGUAGE_PATH		L"//Sys/SctLanguage"
#define		SCTKEY_RegsvrComFrameworkFile	L"//Sys/RegsvrComFrameworkFile"
#define		SCTKEY_RegsvrFileManagerFile	L"//Sys/RegsvrFileManagerFile"

#define		SCTKEY_FLASHWINDOW_SHOWTIMER	L"//FlashWindow/ShowTimer"
#define		SCTKEY_FLASHWINDOW_SHOW			L"//FlashWindow/Show"


#define		SCTKEY_FILE_FILTRATE_COUNTSTOP	L"//FileFiltrate/CountStop"
#define		SCTKEY_FILE_FILTRATE_AUTOFILTRATE	L"//FileFiltrate/AutoFiltrate"

#define		SCTKEY_MAXIMIZE					L"Maximize"
#define		SCTKEY_SCT_LANGUAGE				L"SctLanguage"
#define		SCTKEY_REG_REGISTER				L"Register"
#define		SCTKEY_REG_USER					L"User"
#define		SCTKEY_REG_MA					L"MA"
#define		SCTKEY_REG_SN					L"SN"
#define		SCTKEY_REG_USER_PATH			L"//Sys/Register/User"
#define		SCTKEY_REG_MA_PATH				L"//Sys/Register/MA"
#define		SCTKEY_REG_SN_PATH				L"//Sys/Register/SN"



#define		SCTKEY_SOFTWARE_LANGUAGE_PATH	L"//Sys/SoftwareLanguage"
#define		SCTKEY_SOFTWARE_LANGUAGE		L"SoftwareLanguage"


#define		SCTKEY_SHELL					L"SctShellKey"
#define		SCTKEY_LIST_CTRL				L"ShellListCtrl"
#define		SCTKEY_FILTRATE_STRING			L"FiltrateString"
#define		SCTKEY_CLICK					L"Click"
#define		SCTKEY_DBLCLICK					L"DblClick"
#define		SCTKEY_ONERUN					L"OneRun"
#define		SCTKEY_LINE_HOT					L"LineHot"
#define		SCTKEY_LINE_COLD				L"LineCold"


#define		SCTKEY_SHELL_LISTCTRL_ATH				L"//SctShellKey/ShellListCtrl"
#define		SCTKEY_SHELL_LISTCTRL_DBCLICK_PATH		L"//SctShellKey/ShellListCtrl/DblClick"
#define		SCTKEY_SHELL_LISTCTRL_LINE_HOT_PATH		L"//SctShellKey/ShellListCtrl/LineHot"
#define		SCTKEY_SHELL_LISTCTRL_LINE_COLD_PATH	L"//SctShellKey/ShellListCtrl/LineCold"
#define		SCTKEY_SHELL_FILTRATE_STRING_PATH		L"//SctShellKey/FiltrateString"

#define 	SCTKEY_DOWN					L"Down"
#define 	SCTKEY_DATE					L"Date"
#define 	SCTKEY_PATHTO				L"PathTo"
#define		SCTKEY_VER					L"Ver"
#define		SCTKEY_FILE_NAME			L"FileName"
#define		SCTKEY_ZIP_FILE_NAME		L"ZipFileName"
#define		SCTKEY_UPGRADE				L"Upgrade"
#define		SCTKEY_NEW_VER				L"NewVer"
#define 	SCTKEY_URLPATH				L"UrlPath"
#define 	SCTKEY_UPDATEPATH			L"UpdatePath"
#define		SCTKEY_TARGETFULLNAME		L"PathTo"

#define 	SCTKEY_HOT_BEGIN_COLOR_PATH		L"//CommandItem/HotBeginColor"
#define 	SCTKEY_HOT_CENTER_COLOR_PATH	L"//CommandItem/HotCenterColor"
#define 	SCTKEY_HOT_END_COLOR_PATH		L"//CommandItem/HotEndColor"
#define 	SCTKEY_SEL_COLOR_PATH			L"//CommandItem/SelColor"
#define 	SCTKEY_FRAME_COLOR_PATH			L"//CommandItem/FrameColor"
#define 	SCTKEY_DOWN_BEGIN_COLOR_PATH	L"//CommandItem/DownBeginColor"
#define 	SCTKEY_DOWN_CENTER_COLOR_PATH	L"//CommandItem/DownCenterColor"
#define 	SCTKEY_DOWN_END_COLOR_PATH		L"//CommandItem/DownEndColor"

//
// XML日志状态.
#define		SCTKEY_SHOW_LOG_WINDOW_PATH		L"//Sys/SystemLog/ShowLogWindow"
#define		SCTKEY_SHOW_LOG_INFO_PATH		L"//Sys/SystemLog/Info"
#define		SCTKEY_SHOW_LOG_WARNING_PATH	L"//Sys/SystemLog/Warning"
#define		SCTKEY_SHOW_LOG_ERROR_PATH		L"//Sys/SystemLog/Error"
#define		SCTKEY_SHOW_LOG_FATAL_PATH		L"//Sys/SystemLog/Fatal"



/*
** 定义 libxml 风格节点路径。
*/
static const char* XMLF_TREE_PROPERTY_SHEET="//config/TreePropertySheet";
static const wchar_t* XMLF_TREE_PROPERTY_SHEET_W=L"//config/TreePropertySheet";
#define		XMLKEY_TREE_PROPERTY_PAGE		"/PropertyPage_"
#define		XMLKEY_TREE_PROPERTY_GUID		"guid"
static const char* XMLF_EXPANDALLROOT_PATH="/config/TreePropertySheet/ExpandAllRoot";




#define		LXML_SLC_FILTRATE_STRING_PATH	"/config/SctShellKey/FiltrateString"
static const wchar_t* LXML_MAIN_FRAME_RECT_PATH = L"/config/MainFrame/%s/Rect";
static const char* LXML_MAXIMIZE = "Maximize";

static const char*	XMLF_SPLITTER_WIDTH	=	"/config/Sys/Splitter";



static const char* XMLF_MAINFRAMETITLE_PATH = "/config/Sys/MainFrame/MainFrameTitle";



#define FK_XML_OBJECTNAME_LNG		50						// 所有 XML 文件和C/C++代码中的对象名称长度为 50 字。
#define	FK_XML_CLASSNAME_LNG		50						// 所有 XML 文件和C/C++代码中的类名称长度为 50 字。

_FK_END


#endif
