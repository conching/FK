#ifndef __XmlFile_h__
#define __XmlFile_h__

#include "fk\def.hxx"

_FK_BEGIN
	// {15B57B4F-1753-417c-8A6E-7D67E65262B3}
	static const GUID IID_XmlFile = { 0x15b57b4f, 0x1753, 0x417c, { 0x8a, 0x6e, 0x7d, 0x67, 0xe6, 0x52, 0x62, 0xb3 } };

/*
节点操作，采用LXmlFile 直接操作节点和节点拖管模型两种。

*/

class _FK_OUT_CLASS LXmlFile : public fk::LObject
{
private:
public:
	LXmlFile(fk::LObject* pParentObject);
	virtual ~LXmlFile(void);

	virtual b_call OpenFile(fk::LModule* pmodule, LPCWSTR lpszXmlFile) = 0;
	virtual b_call OpenFile(fk::LModule* pmodule, xmlDocPtr pxmlDocPtr) = 0;

	virtual b_call OpenCfgFile(fk::LModule* pmodule, LPCWSTR lpszXmlFileName) = 0;
	virtual b_call OpenResFile(fk::LModule* pmodule, LPCWSTR lpszXmlFileName) = 0;

	virtual b_call IsOpen(void) = 0;
	virtual b_call save(void) = 0;
	virtual b_call SaveAs(fk::LVar*  pbaseData) = 0;

	virtual b_call Close(void) = 0;
	virtual b_call ArlteOpen(void) = 0;
	virtual fk::rwstring* _call GetFileName(void) = 0;
	virtual fk::HXMLCONFIG GetDocPtr(void) = 0;

	virtual fk::LModule* _call GetModule() = 0;

	virtual b_call NewChildNode(xmlNodePtr pParentNode,
						xmlNsPtr ns, const char* pszName, const char* pszContent, fk::HXMLNODE* ppxmlNode) = 0;

	virtual b_call OpenNode(fk::LModule* pmodule, LPCSTR lpszXmlPathPart, fk::HXMLNODE* ppxmlNode, enumXMLGSN xmlgsnIn) = 0;
	virtual b_call OpenNode(LPCSTR lpszNodePath, fk::HXMLNODE* ppxmlNode, enumXMLGSN xmlgsnIn) = 0;
	virtual b_call GetChildNode(fk::HXMLNODE hxmlParentNode, fk::HXMLNODE* ppxmlNode) = 0;
	virtual b_call NextNode(fk::HXMLNODE hxmlNode, fk::HXMLNODE* ppxmlNode) = 0;
	virtual b_call PretNode(fk::HXMLNODE hxmlNode, fk::HXMLNODE* ppxmlNode) = 0;

	virtual b_call GetChildNodeFromName(fk::HXMLNODE hxmlNodeParent, LPCSTR lpszNodeName, fk::HXMLNODE* ppxmlNode) = 0;
	virtual b_call GetNodeFromName(fk::HXMLNODE hxmlNode, LPCSTR lpszNodeName, fk::HXMLNODE* ppxmlNode) = 0;

	virtual b_call CloseNode(fk::HXMLNODE hxmlNode) = 0;
	virtual b_call RemoveNode(fk::HXMLNODE hxmlNode) = 0;

	virtual b_call SetPropDataA(fk::HXMLNODE hxmlNode, LPCSTR szPropName, fk::enumDataType dt, data_t* pdata) = 0;
	virtual b_call SetPropDataW(fk::HXMLNODE hxmlNode, LPCWSTR szPropName, fk::enumDataType dt, data_t* pdata) = 0;
	virtual b_call GetPropDataA(fk::HXMLNODE hxmlNode, LPCSTR szPropName, fk::enumDataType dt, void* pdata) = 0;
	virtual b_call GetPropDataW(fk::HXMLNODE hxmlNode, LPCWSTR szPropName, fk::enumDataType dt, void* pdata) = 0;

	virtual b_call RemoveProp(LPCSTR lpszPropName) = 0;
	virtual b_call RemoveProp(xmlAttrPtr pxmlAttrPtr) = 0;
	virtual b_call FindProp(fk::HXMLNODE hxmlNode, LPCSTR lpszPropName, ::xmlAttrPtr* ppprop) = 0;

	virtual b_call GetNodeFullPathW(fk::HXMLNODE fxmlNode, fk::LStringw* psXmlPath) = 0;
	virtual b_call GetNodeFullPathA(fk::HXMLNODE fxmlNode, fk::LStringa* pcXmlPath) = 0;
	virtual v_call ErrorXmlPathNodeName(fk::HXMLNODE hxmlNode, LPCSTR lpszNodeName) = 0;

	static b_fncall NewXmlFile(fk::LObject* pParentObject, LPVOID* ppv, REFIID riid=GUID_NULL);

public:
	enum enumXmlFileNotify
	{
		ON_OPEN_NODE	= 0x00000001,		// 打开一个后发送的事件。
		ON_CLOSE_NODE	= 0x00000002,		// 关闭一个节点后发送的事件。

		ON_AFTER_CLOSE	= 0x00000004,	// 关闭文件之后发送
		ON_BEFORE_CLOSE = 0x00000004,	// 关闭文件之前发送

		ON_AFTER_OPEN	= 0x00000008,		// 打开文件后发送的事件。

		ON_XPN_PROP_EDIT	= 0x00000010,	// 某个属性的值改变了。

		ON_XPN_NODE_EDIT	= 0x00000020,	// 节点值改变。
	};
	typedef struct tagOpenNode
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile*	pxmlFile;
		fk::HXMLNODE	hxmlNode;
	}OPENNODE,*LPOpenNode;

	typedef struct tagCloseNode
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile*	pxmlFile;
		fk::HXMLNODE	hxmlNode;
	}CLOSENODE,*LPCloseNode;

	typedef struct tagAfterClose
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
	}AfterClose,*LPAfterClose;

	typedef struct tagBeforeClose
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
	}BeforeClose,*LPBeforeClose;

	typedef struct tagAfterOpen
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
	}AfterOpen,*LPAfterOpen;

	typedef struct tagBeforeOpen
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
	}BeforeOpen,*LPBeforeOpen;

	typedef struct tagPropEdit			// 属性编辑发送的事件。
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
		fk::HXMLNODE hxmlNode;
		fk::LStringw sNewValue;
	}PropEdit,*LPPropEdit;

	typedef struct tagNodeEdit
	{
		fk::NOTIFYEVENT ne;
		fk::LXmlFile* pxmlFile;
		fk::HXMLNODE hxmlNode;
		fk::LStringw sNewValue;
	}NodeEdit,*LPNodeEdit;
};


/*
动态析构fk::LXmlFile 使用的节点
使用方式：
fk::XmlFileAuto xfa;

if (xfa.BindXmlFile(_pxmlFile))
{
...
}
*/
class _FK_OUT_CLASS XmlFileAuto : public fk::LObject
{
private:
	fk::LXmlFile*	_pxmlFile;
	DWORD			_dwmake;
	fk::FARRAY		_farray;

	//fk::LCriticalSection   _cs;						// 临时使用 _cs.

	v_call AddNode(fk::HXMLNODE hxmlNode);
	v_call RemoveNode(fk::HXMLNODE hxmlNode);

public:
	XmlFileAuto(fk::LXmlFile* pxmlFile=NULL);
	virtual ~XmlFileAuto(void);

	b_call BindXmlFile(fk::LXmlFile* pxmlFile);
	b_call UnbindXmlFile(void);

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
};

_FK_END

#endif
