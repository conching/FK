

		《FK 应用程序框架 1.98》预览版本介绍

			作者：许宗森


《FK 应用程序框架》基本上已经可以使用。此版本已经具备部分向导，RAD 功能。

此版本需要的动态链接库比较多，以后会缩少动态链接库数量。
《FK 应用程序框架》需要的动态链接库列有，其它动态链接库不是：
zlib1.dll
libxml2.dll
iconv.dll
TraceLog.dll
atobj.dll
SafTools.dll
TypeLib.dll
safTheme.dll
ArtFramework.dll


向导功能：
------------
向导使用FK自己家的生成方式，没有使用 VS 生成方式。

1.建立 exe 项目向导。(支持)
2.建立 dll 项目向导。(支持)
3.建立 dlg 类向导。(支持)
4.建立 FK 类向导。(支持)
5.建立 FK 线程类向导。(支持)
6.WindowPanel 功能线程类向导。(支持)


RAD 环境支持事件:
------
1.RAD状态控件变量，非时时更新到 .cpp 文件中。
2.通过 NotifyView 视图，可以添加控件通报函数，可以时时更新到 .cpp 文件中。
3.控件通报信息，可以时时的同步显示到 NoetifyView 视图中。


RAD 功能：
------------
RAD 主要功能已经完成，控件的创建，拖动，属性编辑。
1.已经支持大部分控件.部分控件稍后渐渐添加，非常容易，添加也很快。
mask="0x00001003"是已经支持的控件，  mask="0x00001001" 是未支持的控件。

<item mask="0x00001003" fk::LButton
<item mask="0x00001003" fk::LCheckBox
<item mask="0x00001003" fk::LRadioButton
<item mask="0x00001003" fk::LEdit
<item mask="0x00001003" fk::LComboBox
<item mask="0x00001003" fk::LListBox
<item mask="0x00001003" fk::LGroupBox
<item mask="0x00001003" fk::LStatic
<item mask="0x00001001" fk::LStatic
<item mask="0x00001001" fk::LHorizontal
<item mask="0x00001001" fk::LVertical
<item mask="0x00001001" fk::LSlider
<item mask="0x00001001" fk::LSpin
<item mask="0x00001003" fk::LProgressBarCtrl
<item mask="0x00001001" fk::LHotKey
<item mask="0x00001003" fk::LSysListView32
<item mask="0x00001003" fk::LTreeViewCtrl
<item mask="0x00001003" fk::LTabCtrl
<item mask="0x00001001" fk::LAnimation
<item mask="0x00001001" fk::LRichEdit
<item mask="0x00001003" fk::LTrackBarCtrl
<item mask="0x00001003" fk::LDateTimePickerCtrl
<item mask="0x00001003" fk::LMonthCalendarCtrl
<item mask="0x00001001" fk::LIPAddress
<item mask="0x00001001" fk::LExtendedComboBox

RAD 问题：
1. RAD 建立 Group 控件，选择时有些问题，无法接受鼠标键。然后使用其它方法进行选定，
	只有弹出一个对话框后，Group 控件才能选定，而且不能拖动，只能拉大拉小. 这是个很讨厌的问题。
2. 选择多个控件后，同时改变窗口坐标属性，会出现坐标错乱，一个控件不会。



《项目向导》的问题。
===================

向导生成的项目有两个问题，程序修改有个地方会出现错误，所以需要手动修改。代码如下:

b_call LWizardInfo::SetPchSettings(void)
{
	fk::LAutoPtr ap;
	VCProject::VCFile* pfile = NULL;
	VCProject::IVCCollection* pvfiles = NULL;
	VCProject::IVCCollection* pvColFileConfig = NULL;
	VCProject::IVCCollection* pvColTools = NULL;
	VCProject::VCFileConfiguration*  pvcFileConfig = NULL;
	VCProject::VCCLCompilerTool* pClTool = NULL;
	HRESULT hr;

	ap.AddUnknown6((IUnknown**)&pfile, (IUnknown**)&pvfiles, (IUnknown**)&pvColFileConfig,
							(IUnknown**)&pvColTools, (IUnknown**)&pvcFileConfig, (IUnknown**)&pClTool);

	hr = _pvcProject->get_Files((IDispatch**)&pvfiles);

	hr = pvfiles->Item(CComVariant(L"stdafx.cpp"), (IDispatch**)&pfile);
	hr = pfile->get_FileConfigurations((IDispatch**)&pvColFileConfig);

	hr = pvColFileConfig->Item(CComVariant(L"Debug|Win32"), (IDispatch**)&pvcFileConfig);
	hr = pvcFileConfig->get_Tool((IDispatch**)&pvColTools);

	//
	// 这个地方异常,不知道如何设置单个文件的 put_UsePrecompiledHeader 属性。
	//
	hr = pvColTools->Item(CComVariant(L"VCCLCompilerTool"), (IDispatch**)&pClTool);     
	ASSERT_COM(hr);
	if (pClTool != NULL)
		pClTool->put_UsePrecompiledHeader(VCProject::pchCreateUsingSpecific);

	return true;
}



开发过程位置:
	在 《FK应用程序框架》文件夹中会有一个 fk_local.xml 文件，这个文件是用来映射 *.vxml、 配置文件、主题文件位置用的。
	例如： 
	d:\bin\[!PROJECT_NAME].exe   映射为源代码文件夹： .\code\[!PROJECT_NAME]\res 文件夹. 使用绝对路径。

	*******************************************************
	****************需要手动添加映射。*********************
	****************需要手动修改 vPath 路径。**************
	****************需要手动修改 FKPath 路径。*************
	****************需要手动修改 PathTemplate 路径。*******
	*******************************************************
