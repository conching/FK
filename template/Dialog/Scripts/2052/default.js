// JScript source code

function OnFinish(selProj, selObj)
{
	try
	{
		var strProjectPath = wizard.FindSymbol("PROJECT_PATH");
		var strProjectName = wizard.FindSymbol("PROJECT_NAME");
		var strProjectType = wizard.FindSymbol("ART_PROJECT_TYPE");

		var params = new ActiveXObject("Scripting.Dictionary");
        params.Add(0, "{87296BBA-EF70-4C16-9ED1-94A4F060D413}");
        params.Add(1, strProjectPath);
        params.Add(2, strProjectName);
        params.Add(3, strProjectType);
        params.Add(4, wizard);


        var strWizFullName = wizard.FindSymbol("ABSOLUTE_PATH");
        strWizFullName += "\\WizardFKDialog.vsz";
        var dteRet = dte.LaunchWizard(strWizFullName, params.Items());
        if (dteRet)
        {
        }
    }
    catch(e)
    {
    }
}

