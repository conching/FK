#include "stdafx.h"
#include "[!DIALOG_SHORT_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]


class  [!DIALOG_NAME_FULL_IMPL]: public [!NAMESPACE_FULL][!DIALOG_NAME_FULL]
{
	_FK_RTTI_;

private:
	FK_BEGIN_CODE(VARIABLE, [!DIALOG_VXML_NAME]_VXML)
	fk::LButton*	_pOKBut;
	fk::LButton*	_pCancelBut;
	FK_END_CODE()

	FK_BEGIN_CODE(EVENTDEFAULT, [!DIALOG_VXML_NAME]_VXML)
	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnOKBtnClick(void);
	void OnCancelBtnClick(void);
	FK_END_CODE()

private:

public:
	static wchar_t*	[!DIALOG_VXML_NAME]_VXML;
	[!DIALOG_NAME_FULL_IMPL](fk::LWindow* pParentWin);
	virtual ~[!DIALOG_NAME_FULL_IMPL]();

	// base class
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个�?
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	// user
};
wchar_t* [!DIALOG_NAME_FULL_IMPL]::[!DIALOG_VXML_NAME]_VXML	= L"[!DIALOG_NAME].vxml";

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!DIALOG_NAME_FULL_IMPL], [!NAMESPACE_FULL][!DIALOG_NAME_FULL], fk::LDialog, [!DIALOG_NAME_FULL_IMPL], [!DIALOG_NAME_FULL]);

//---------------------------------------------------------------------------------
[!DIALOG_NAME_FULL]::[!DIALOG_NAME_FULL](fk::LWindow* pParentWin):
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, [!DIALOG_NAME_FULL_IMPL]::[!DIALOG_VXML_NAME]_VXML)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!DIALOG_NAME_FULL]);
}

[!DIALOG_NAME_FULL]::~[!DIALOG_NAME_FULL]()
{

}

b_fncall [!DIALOG_NAME_FULL]::New[!DIALOG_NAME](fk::LWindow* pParentWin, fk::LWindow** ppwin)
{
	[!DIALOG_NAME_FULL_IMPL]*  pobject = NULL;

	pobject = new [!DIALOG_NAME_FULL_IMPL](pParentWin);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppwin = (fk::LWindow*)pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//INT_PTR _call [!DIALOG_NAME_FULL_IMPL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_DIALOG_PROC([!DIALOG_NAME_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC([!DIALOG_NAME_FULL])		 //***** 是基类�?

// vv_call [!DIALOG_NAME_FULL_IMPL]OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个�?
FK_BEGIN_COMMAND_PROC([!DIALOG_NAME_FULL_IMPL])
FK_COMMAND_EVENT(_pOKBut, BN_CLICKED, OnOKBtnClick());
FK_COMMAND_EVENT(_pCancelBut, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call [!DIALOG_NAME_FULL_IMPL]::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC([!DIALOG_NAME_FULL_IMPL])
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// LinObjectVariable 函数代码�?RAD 设计器维护自动生成，不需要手动修改。在RAD对话框设计器中，提供非时时代码同步功能�?
vb_call [!DIALOG_NAME_FULL_IMPL]::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBut, L"_pCancelBut"},
		{(fk::LObject**)&_pOKBut, L"_pOKBut"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/sizeof(lon[0])) )
	{
		// 这里显示错误信息，返�?true, 是为了让窗口能正确的显示�?
		// 返回 false，窗口不显示�?
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE [!DIALOG_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
[!DIALOG_NAME_FULL_IMPL]::[!DIALOG_NAME_FULL_IMPL]()
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!DIALOG_NAME_FULL_IMPL]);
}

[!DIALOG_NAME_FULL_IMPL]::~[!DIALOG_NAME_FULL_IMPL]()
{
}

vb_call [!DIALOG_NAME_FULL_IMPL]::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call [!DIALOG_NAME_FULL_IMPL]::OnInitDialog(void)
{
	LinkDataExchange(fk::ldeInstallData);
}

void [!DIALOG_NAME_FULL_IMPL]::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


void [!DIALOG_NAME_FULL_IMPL]::OnOKBtnClick(void)
{
	this->EndDialog(IDOK);
}

void [!DIALOG_NAME_FULL_IMPL]::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}

FK_NOTIFY_POS

[!END_NAMESPACE_MACRO]
