#include "stdafx.h"
#include "[!PROPERTY_PAGE_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]

//---------------------------------------------------------------------------------
class [!PROPERTY_PAGE_FULL_IMPL] : public [!PROPERTY_PAGE_CLASS],
									public fk::IPropertyPageImpl<[!PROPERTY_PAGE_FULL_IMPL]>,
									public fk::LDialog
{
private:
	FK_BEGIN_CODE(VARIABLE, [!VXML_NAME_MACRO]_VXML)
	FK_END_CODE()

	FK_BEGIN_CODE(EVENTDEFAULT, [!VXML_NAME_MACRO]_VXML)
	v_call OnInitDialog(void);
	FK_END_CODE()
private:

public:
	static wchar_t*	[!VXML_NAME_MACRO]_VXML;
	[!PROPERTY_PAGE_FULL_IMPL]();
	virtual ~[!PROPERTY_PAGE_FULL_IMPL]();

	FK_COM_ARQ();

	//
	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

	// IPropertyPage
	virtual STDMETHODIMP GetPageInfo(PROPPAGEINFO *pPageInfo);
	virtual STDMETHODIMP Apply(void);
};
wchar_t* [!PROPERTY_PAGE_FULL_IMPL]::[!VXML_NAME_MACRO]_VXML = L"[!VXML_NAME].vxml";
//---------------------------------------------------------------------------------
// class [!PROPERTY_PAGE_CLASS]
[!PROPERTY_PAGE_CLASS]::[!PROPERTY_PAGE_CLASS]()
{
}

[!PROPERTY_PAGE_CLASS]::~[!PROPERTY_PAGE_CLASS]()
{
}

b_fncall [!PROPERTY_PAGE_CLASS]::New[!PROPERTY_PAGE_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid)
{
	fk_pv2(powner==NULL, ppobj==NULL, return false;);
	[!NAMESPACE_FULL][!PROPERTY_PAGE_FULL_IMPL]*  pobject= NULL;

	pobject = new [!NAMESPACE_FULL][!PROPERTY_PAGE_FULL_IMPL](powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
FK_COM_A_CODE([!PROPERTY_PAGE_FULL_IMPL])

//INT_PTR _call [!DIALOG_NAME_FULL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_SUBWNDPROC([!PROPERTY_PAGE_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_MESSAGE_HANDLER(WM_STYLECHANGING, fk::IPropertyPageImpl<[!PROPERTY_PAGE_FULL_IMPL]>::OnStyleChange(wParam, lParam), return 0;)
FK_END_SUBWNDPROC(fk::LDialog)		 //***** 是基类。

// vv_call [!DIALOG_NAME_FULL]OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC([!PROPERTY_PAGE_FULL_IMPL])
//FK_COMMAND_EVENT(_pOKBtn, BN_CLICKED, OnOKBtnClick());
//FK_COMMAND_EVENT(_pCancelBtn, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call [!DIALOG_NAME_FULL]::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC([!PROPERTY_PAGE_FULL_IMPL])
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
vb_call [!PROPERTY_PAGE_FULL_IMPL]::LinkObjectVariable(void)
{
	/*
	fk::LINKOBJECTNAME lon[]={
	{(fk::LObject**)&_pCancelBtn, L"_pCancelBtn"},
		{(fk::LObject**)&_pOKBtn, L"_pOKBtn"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}
*/
	return true;
}

ULONG STDMETHODCALLTYPE [!PROPERTY_PAGE_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
// class [!PROPERTY_PAGE_CLASS]
[!PROPERTY_PAGE_FULL_IMPL]::[!PROPERTY_PAGE_FULL_IMPL]():
fk::LDialog(g_pmodule, [!PROPERTY_PAGE_FULL_IMPL]::[!VXML_NAME_MACRO]_VXML)
{
}

[!PROPERTY_PAGE_FULL_IMPL]::~[!PROPERTY_PAGE_FULL_IMPL]()
{
}

STDMETHODIMP [!PROPERTY_PAGE_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	if (ppv!=NULL)
	{
		FK_QUERY_IID(riid, IID_IPropertyPage, IPropertyPage, ppv, this);
	}
	else
	{
		ATLASSERT(0);
		return E_NOINTERFACE;
	}

	return S_FALSE;
}

vb_call [!PROPERTY_PAGE_FULL_IMPL]::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call [!PROPERTY_PAGE_FULL_IMPL]::OnInitDialog()
{
	LinkDataExchange(fk::ldeInstallData);
}

STDMETHODIMP [!PROPERTY_PAGE_FULL_IMPL]::GetPageInfo(PROPPAGEINFO *pPageInfo)
{
	fk::PROPPAGEINFOEX *pEx = NULL;

	int nCbSize = sizeof(fk::PROPPAGEINFOEX);
	if (pPageInfo->cb == nCbSize)
	{
		pEx			= static_cast<fk::PROPPAGEINFOEX*>(pPageInfo);
		pEx->hIcon	= LoadIcon(g_pmodule->hRes, MAKEINTRESOURCE(200));		// 这个200改为你的图标句柄。
		pEx->cb		= nCbSize;
	}

	pPageInfo->cb			= sizeof(PROPPAGEINFO);
	pPageInfo->pszTitle		= L"DemoPropertyPage";
	pPageInfo->size			= m_size;
	pPageInfo->pszHelpFile	= L"DemoPropertyPage";
	pPageInfo->pszDocString = L"DemoPropertyPage";
	pPageInfo->dwHelpContext= 200;

	return S_OK;
}

STDMETHODIMP [!PROPERTY_PAGE_FULL_IMPL]::Apply(void)
{
	return S_OK;
}


[!END_NAMESPACE_MACRO]
