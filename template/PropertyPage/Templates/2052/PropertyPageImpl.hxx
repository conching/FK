#ifndef _[!PROPERTY_PAGE_NAME_UPPER]_h_
#define _[!PROPERTY_PAGE_NAME_UPPER]_h_


[!BEGIN_NAMESPACE_MACRO]

class  [!PROPERTY_PAGE_CLASS] : public IUnknown
{
public:
	[!PROPERTY_PAGE_CLASS]();
	virtual ~[!PROPERTY_PAGE_CLASS]();

	static b_fncall New[!PROPERTY_PAGE_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid);
};


/*
手动添加这些的三行代码段，稍后提供代码同步功能，或者其它方式。

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	添加这些到 DllGetClassObject 函数。
	_FK_CLASS_FACTORY(rclsid, CLSID_[!PROPERTY_PAGE_NAME], ppv, [!NAMESPACE_FULL][!PROPERTY_PAGE_CLASS]::New[!PROPERTY_PAGE_NAME]_IID);
}

STDAPI DllRegisterServer(void)
{
	UpdateRegistryEx(IDR_[!PROPERTY_PAGE_NAME_UPPER], TRUE);			添加这些到 DllRegisterServer 函数。
}

STDAPI DllUnregisterServer(void)
{
	UpdateRegistryEx(IDR_[!PROPERTY_PAGE_NAME_UPPER], FALSE);			添加这些到 DllUnregisterServer 函数。
}
*/

[!END_NAMESPACE_MACRO]


#endif
