#include "stdafx.h"
#include "[!WINDOW_PANEL_NAME][!HXX]"



[!BEGIN_NAMESPACE_MACRO]

//---------------------------------------------------------------------------------
class [!WINDOW_PANEL_FULL_IMPL] : public [!WINDOW_PANEL_CLASS],
									public fk::LWindowMuch
{
private:
	FK_BEGIN_CODE(VARIABLE, [!VXML_NAME_MACRO]_VXML)
		FK_END_CODE()

		FK_BEGIN_CODE(EVENTDEFAULT, [!VXML_NAME_MACRO]_VXML)
		v_call OnCreate(void);
	v_call OnSize(WPARAM wParam, LPARAM lParam);
	FK_END_CODE()

public:
	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	static wchar_t*	[!VXML_NAME_MACRO]_VXML;
	[!WINDOW_PANEL_FULL_IMPL]();
	virtual ~[!WINDOW_PANEL_FULL_IMPL]();

	HRESULT FinalConstruct();
	void FinalRelease();

	FK_COM_ARQ();

	virtual STDMETHODIMP GetWnd(LONG_PTR* hWnd);
	virtual STDMETHODIMP CreateWnd(fk::IGenericPanel* pParentPanel,
									int iLeft, int iTop, int iWidth, int iHeight, LONG_PTR hWndParent);
	virtual STDMETHODIMP OnMdiActiveWindowPanel(fk::IMdiContainer* pMdiContainer,
		fk::IGenericPanel* pParentPanel, fk::ITabPageItem* pTabPageItem, UINT nType,
		VARIANT_BOOL bActive, VARIANT_BOOL* bOutActive, long* lpvObj);
	virtual STDMETHODIMP  OnGenericPanelActiveWindowPanel(fk::IGenericPanel* pParentPanel,
		fk::IGenericPanelItem* gpi, UINT nType, VARIANT_BOOL* bActive, long* lpvObj);
	virtual STDMETHODIMP QueryCloseWindowPanel(fk::IGenericPanel* pParentPanel, fk::enumCloseAction* pCloseAction, long* lpvObj);
	virtual STDMETHODIMP CloseWindowPanel(fk::IGenericPanel* pParentPanel, long* lpvObj);
};
wchar_t* [!WINDOW_PANEL_FULL_IMPL]::[!VXML_NAME_MACRO]_VXML = L"[!VXML_NAME].vxml";


//---------------------------------------------------------------------------------
// class [!WINDOW_PANEL_CLASS]
[!WINDOW_PANEL_CLASS]::[!WINDOW_PANEL_CLASS]()
{
}

[!WINDOW_PANEL_CLASS]::~[!WINDOW_PANEL_CLASS]()
{
}

b_fncall [!WINDOW_PANEL_CLASS]::New[!WINDOW_PANEL_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid)
{
	[!WINDOW_PANEL_CLASS]* pobj;

	pobj = new [!WINDOW_PANEL_FULL_IMPL]();
	if (pobj!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = (fk::IWindowPanel*)pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}


//---------------------------------------------------------------------------------
// class [!WINDOW_PANEL_FULL_IMPL]
[!WINDOW_PANEL_FULL_IMPL]::[!WINDOW_PANEL_FULL_IMPL]():
	fk::LWindowMuch(g_pmodule, [!WINDOW_PANEL_FULL_IMPL]::[!VXML_NAME_MACRO]_VXML)
{
}


[!WINDOW_PANEL_FULL_IMPL]::~[!WINDOW_PANEL_FULL_IMPL]()
{
}


FK_COM_A_CODE([!WINDOW_PANEL_FULL_IMPL])


HRESULT [!WINDOW_PANEL_FULL_IMPL]::FinalConstruct()
{
	return S_OK;
}


void [!WINDOW_PANEL_FULL_IMPL]::FinalRelease()
{
}

STDMETHODIMP [!WINDOW_PANEL_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	if (ppv!=NULL)
	{
		FK_QUERY_IID(riid, fk::IID_IWindowPanel, fk::IWindowPanel, ppv, this);
	}
	else
	{
		ATLASSERT(0);
		return E_NOINTERFACE;
	}

	return S_FALSE;
}

STDMETHODIMP [!WINDOW_PANEL_FULL_IMPL]::GetWnd(LONG_PTR* hWnd)
{
	*hWnd = (LONG)_hWnd;

	return S_OK;
}


STDMETHODIMP  [!WINDOW_PANEL_FULL_IMPL]::CreateWnd(fk::IGenericPanel* pParentPanel,
										  int iLeft,
										  int iTop,
										  int iWidth,
										  int iHeight,
										  LONG_PTR hWndParent
										  )
{
	RECT	rc;

	rc.left		= iLeft;
	rc.top		= iTop;
	rc.right	= iWidth;
	rc.bottom	= iHeight;

	//Create((HWND)hWndParent);
	CreateFkWindow(0, L"", WS_VISIBLE|WS_CHILDWINDOW|WS_CLIPCHILDREN,
										iLeft, iTop, iWidth, iHeight, (HWND)hWndParent, 0, NULL);
	if (IsWindow())
	{
		ShowWindow(SW_SHOW);
	}
	else
	{
		fk_TraceFormatMessage(FTEXTNIL, g_pmodule->hRes, STRNIL, ::GetLastError());
		fk_ErrorBox();
	}

	return S_OK;
}


STDMETHODIMP  [!WINDOW_PANEL_FULL_IMPL]::OnMdiActiveWindowPanel(fk::IMdiContainer* pMdiContainer,
													   fk::IGenericPanel* pParentPanel,
													   fk::ITabPageItem* pTabPageItem,
													   UINT nType,
													   VARIANT_BOOL bActive,
													   VARIANT_BOOL* bOutActive,
													   long* lpvObj
													   )
{
	fk::LStringw	sTitle;

	//sTitle.LoadStr(g_pmodule->hRes, IDS_TITLETOOLBOXVIEW);
	sTitle = L"[!WINDOW_PANEL_NAME]";

	pTabPageItem->put_Caption(sTitle._pwstr);

	return S_OK;
}

STDMETHODIMP [!WINDOW_PANEL_FULL_IMPL]::OnGenericPanelActiveWindowPanel(fk::IGenericPanel* pParentPanel,
															   fk::IGenericPanelItem* pPanelItem,
															   UINT nType,
															   VARIANT_BOOL* bActive,
															   long* lpvObj
															   )
{

	return S_OK;
}


STDMETHODIMP [!WINDOW_PANEL_FULL_IMPL]::QueryCloseWindowPanel(fk::IGenericPanel* pParentPanel,
													 fk::enumCloseAction* pCloseAction,
													 long* lpvObj
													 )
{

	return S_OK;
}


/*
** 删除自身的信息。
*/
STDMETHODIMP [!WINDOW_PANEL_FULL_IMPL]::CloseWindowPanel(fk::IGenericPanel* pParentPanel,
												long* lpvObj
												)
{

	return S_OK;
}

//---------------------------------------------------------------------------------
//INT_PTR _call [!WINDOW_PANEL_FULL_IMPL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_SUBWNDPROC([!WINDOW_PANEL_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_CREATE, OnCreate(), return 0);
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_END_SUBWNDPROC(fk::LWindowMuch)		 //***** 是基类。

// vv_call [!DIALOG_NAME_FULL]OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC([!WINDOW_PANEL_FULL_IMPL])
//FK_COMMAND_EVENT(_pOKBtn, BN_CLICKED, OnOKBtnClick());
//FK_COMMAND_EVENT(_pCancelBtn, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call [!WINDOW_PANEL_FULL_IMPL]::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC([!WINDOW_PANEL_FULL_IMPL])
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
vb_call [!WINDOW_PANEL_FULL_IMPL]::LinkObjectVariable(void)
{
	/*
	fk::LINKOBJECTNAME lon[]={
	{(fk::LObject**)&_pCancelBtn, L"_pCancelBtn"},
		{(fk::LObject**)&_pOKBtn, L"_pOKBtn"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}
*/
	return true;
}

ULONG STDMETHODCALLTYPE [!WINDOW_PANEL_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call [!WINDOW_PANEL_FULL_IMPL]::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call [!WINDOW_PANEL_FULL_IMPL]::OnCreate()
{
	LinkDataExchange(fk::ldeInstallData);
}

void [!WINDOW_PANEL_FULL_IMPL]::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


[!END_NAMESPACE_MACRO]
