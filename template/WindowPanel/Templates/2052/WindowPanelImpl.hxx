#ifndef _[!WINDOW_PANEL_NAME]_h_
#define _[!WINDOW_PANEL_NAME]_h_


[!BEGIN_NAMESPACE_MACRO]

class  [!WINDOW_PANEL_CLASS] : public fk::IWindowPanel
{
public:
	[!WINDOW_PANEL_CLASS]();
	virtual ~[!WINDOW_PANEL_CLASS]();

	static b_fncall New[!WINDOW_PANEL_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid);
};


/*
{[!CLSID_REGISTRY_FORMAT]}

手动添加这些的三行代码段，稍后提供代码同步功能，或者其它方式。

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	添加这些到 DllGetClassObject 函数。
	_FK_CLASS_FACTORY(rclsid, CLSID_[!WINDOW_PANEL_NAME], ppv, [!NAMESPACE_FULL][!WINDOW_PANEL_CLASS]::New[!WINDOW_PANEL_NAME]_IID);
}

STDAPI DllRegisterServer(void)
{
	UpdateRegistryEx(IDR_[!IDR_REGSVR_ID_NAME], TRUE);			//添加这些到 DllRegisterServer 函数。
}

STDAPI DllUnregisterServer(void)
{
	UpdateRegistryEx(IDR_[!IDR_REGSVR_ID_NAME], FALSE);			//添加这些到 DllUnregisterServer 函数。
}
*/

[!END_NAMESPACE_MACRO]


#endif
