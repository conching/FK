// JScript source code

function OnFinish(selProj, selObj)
{
	try
	{
		var strProjectPath = wizard.FindSymbol("PROJECT_PATH");
		var strProjectName = wizard.FindSymbol("PROJECT_NAME");
		var strProjectType = wizard.FindSymbol("ART_PROJECT_TYPE");

		var params = new ActiveXObject("Scripting.Dictionary");
        params.Add(0, "{87296BBA-EF70-4C16-9ED1-94A4F060D413}");
        params.Add(1, strProjectPath);
        params.Add(2, strProjectName);
        params.Add(3, strProjectType);
        params.Add(4, wizard);

    var strLCID = wizard.GetHostLocale();
        //SetLocaleForResources(strLCID);
	var strLang = wizard.GetLangFromLCID(strLCID);
	wizard.AddSymbol("LANGUAGE", strLang);
	wizard.AddSymbol("LANGUAGE_NAME", strLang);

	wizard.AddSymbol("LCID", strLCID);
	wizard.AddSymbol("HEX_LCID", LocaleTo4HexDigit(strLCID));
	var strCodePage = wizard.GetCodePageFromLCID(strLCID);
	wizard.AddSymbol("CODE_PAGE", strCodePage);
	wizard.AddSymbol("HEX_CODE_PAGE", LocaleTo4HexDigit(strCodePage));

	var strSystemLCID = wizard.GetSystemLCID();
	wizard.AddSymbol("SYSTEM_LCID", strSystemLCID);
	var strSystemCodePage = wizard.GetCodePageFromLCID(strSystemLCID);
	wizard.AddSymbol("SYSTEM_CODE_PAGE", strSystemCodePage);
	wizard.AddSymbol("SYSTEM_HEX_CODE_PAGE", LocaleTo4HexDigit(strSystemCodePage));


	wizard.AddSymbol("PRIMARY_LANG_ID", wizard.GetPrimaryLangIdFromLCID(strSystemLCID));
	wizard.AddSymbol("SUB_LANG_ID", wizard.GetSubLangIdFromLCID(strSystemLCID));
	wizard.AddSymbol("LANG_SUFFIX", wizard.GetLangAbbrevFromLCID(strSystemLCID));

        var strWizFullName = wizard.FindSymbol("ABSOLUTE_PATH");
        strWizFullName += "\\DefaultWizard.vsz";
        var dteRet = dte.LaunchWizard(strWizFullName, params.Items());
        if (dteRet)
        {
        }
    }
    catch(e)
    {
    }
}

/******************************************************************************
 Description: Given the LC string (example LCID "1033" for english US)
		it transforms it into 4 digit hex representation with leading 0
		if necessary (example "0409" for english US). Format used in RC files.
		Note that prefix "0x" is not added
******************************************************************************/
function LocaleTo4HexDigit(strLocale)
{
	var nlc = new Number(strLocale);
	var strLoc = nlc.toString(16);
	if(strLoc.length==3)
		return "0" + strLoc;
	return strLoc;
}
