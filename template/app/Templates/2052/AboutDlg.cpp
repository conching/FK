#include "stdafx.h"
#include "[!DIALOG_SHORT_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]


class  [!DIALOG_NAME_FULL_IMPL]: public [!NAMESPACE_FULL][!DIALOG_NAME_FULL]
{
	_FK_RTTI_;

private:
	FK_BEGIN_CODE(VARIABLE, [!DIALOG_NAME]_VXML)
	fk::LButton*	_pOKBtn;
	fk::LButton*	_pCancelBtn;
	FK_END_CODE()

	FK_BEGIN_CODE(EVENTDEFAULT, [!DIALOG_NAME]_VXML)
	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnOKBtnClick(void);
	void OnCancelBtnClick(void);
	FK_END_CODE()

private:

public:
	static wchar_t*	[!DIALOG_NAME]_VXML;
	[!DIALOG_NAME_FULL_IMPL]();
	virtual ~[!DIALOG_NAME_FULL_IMPL]();

	// base class
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	// user
};
wchar_t* [!DIALOG_NAME_FULL_IMPL]::[!DIALOG_NAME]_VXML = L"[!DIALOG_NAME].vxml";

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!DIALOG_NAME_FULL_IMPL], [!NAMESPACE_FULL][!DIALOG_NAME_FULL], fk::LDialog, [!DIALOG_NAME_FULL_IMPL], [!DIALOG_NAME_FULL]);
//---------------------------------------------------------------------------------
//INT_PTR _call [!DIALOG_NAME_FULL_IMPL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_DIALOG_PROC([!DIALOG_NAME_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC([!DIALOG_NAME_FULL])		 //***** 是基类。

// vv_call [!DIALOG_NAME_FULL_IMPL]OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC([!DIALOG_NAME_FULL_IMPL])
FK_COMMAND_EVENT(_pOKBtn, BN_CLICKED, OnOKBtnClick());
FK_COMMAND_EVENT(_pCancelBtn, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call [!DIALOG_NAME_FULL_IMPL]::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC([!DIALOG_NAME_FULL_IMPL])
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
//
vb_call [!DIALOG_NAME_FULL_IMPL]::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBtn, L"_pCancelBtn"},
		{(fk::LObject**)&_pOKBtn, L"_pOKBtn"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE [!DIALOG_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
[!DIALOG_NAME_FULL]::[!DIALOG_NAME_FULL]():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, [!DIALOG_NAME_FULL_IMPL]::[!DIALOG_NAME]_VXML)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!DIALOG_NAME_FULL]);
}

[!DIALOG_NAME_FULL]::~[!DIALOG_NAME_FULL]()
{

}

[!DIALOG_NAME_FULL]* [!DIALOG_NAME_FULL]::New[!DIALOG_SHORT_NAME](void)
{
	[!DIALOG_NAME_FULL_IMPL]*  pNew[!DIALOG_SHORT_NAME] = NULL;

	pNew[!DIALOG_SHORT_NAME] = new [!DIALOG_NAME_FULL_IMPL]();
	if (pNew[!DIALOG_SHORT_NAME]!=NULL)
	{
		if (pNew[!DIALOG_SHORT_NAME]->create())
		{
			pNew[!DIALOG_SHORT_NAME]->AddRef();
			return pNew[!DIALOG_SHORT_NAME];
		}
		else
		{
			delete pNew[!DIALOG_SHORT_NAME];
			pNew[!DIALOG_SHORT_NAME] = NULL;
		}
	}

	return NULL;
}


//---------------------------------------------------------------------------------
[!DIALOG_NAME_FULL_IMPL]::[!DIALOG_NAME_FULL_IMPL]()
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!DIALOG_NAME_FULL_IMPL]);
}

[!DIALOG_NAME_FULL_IMPL]::~[!DIALOG_NAME_FULL_IMPL]()
{
}

vb_call [!DIALOG_NAME_FULL_IMPL]::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call [!DIALOG_NAME_FULL_IMPL]::OnInitDialog(void)
{
	LinkDataExchange(fk::ldeInstallData);

}

void [!DIALOG_NAME_FULL_IMPL]::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


void [!DIALOG_NAME_FULL_IMPL]::OnOKBtnClick(void)
{
	this->EndDialog(IDOK);
}

void [!DIALOG_NAME_FULL_IMPL]::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}


[!END_NAMESPACE_MACRO]
