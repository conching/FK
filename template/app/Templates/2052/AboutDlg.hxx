#ifndef _[!DIALOG_SHORT_NAME]_h_
#define _[!DIALOG_SHORT_NAME]_h_


[!BEGIN_NAMESPACE_MACRO]

class [!DIALOG_NAME_FULL]: public fk::LDialog
{
	_FK_RTTI_;

private:

public:
	[!DIALOG_NAME_FULL]();
	virtual ~[!DIALOG_NAME_FULL]();

	static [!DIALOG_NAME_FULL]* New[!DIALOG_NAME](void);
};


[!END_NAMESPACE_MACRO]


#endif
