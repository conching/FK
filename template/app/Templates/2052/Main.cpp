#include "stdafx.h"
#include "resource.h"
#include "[!PROJECT_NAME]_i.h"
#include "[!PROJECT_MGR_NAME][!HXX]"


class C[!PROJECT_NAME] : public CAtlExeModuleT< C[!PROJECT_NAME] >
{
public :
	DECLARE_LIBID(LIBID_[!PROJECT_NAME]Lib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_[!UPPER_PROJECT_NAME], "{[!APPID_REGISTRY_FORMAT]}")
};

C[!PROJECT_NAME] _AtlModule;

extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	HRESULT			hr = 0;
	[!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]*		pmgr = NULL;
	int				iRet = 0;

	if (_AtlModule.ParseCommandLine(lpCmdLine, &hr))
	{
		pmgr = [!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]::New[!PROJECT_MGR_NAME]();
		iRet = pmgr->WinMain(false);
		pmgr->Release();

		fk::UninitializeModule(&g_pmodule);
		return iRet;
	}

	return 1;
}

