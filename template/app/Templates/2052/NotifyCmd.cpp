#include "stdafx.h"
#include "fk\xmldef.hxx"
#include "[!PROJECT_NOTIFY_CMD_NAME][!HXX]"
#include "resource.h"

[!BEGIN_NAMESPACE_MACRO]


#define ID_FILE_OPEN			10000


class [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL] : public [!PROJECT_NOTIFY_CMD_CLASS_FULL]
{
private:
	IID*				_pSelfIID;
	fk::INotifyCommand*		_pNotifyCmdParent;

	void OnFileOpen(fk::ICommandItem* pCmdUI);
	void OnFileClose(void);
	void OnShowOptionsDlg(fk::ICommandItem* pCmdUI);

protected:
public:
	[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]();
	virtual ~[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]();

	FK_COM_ARQ();

	virtual HRESULT STDMETHODCALLTYPE NotifyCommand(UINT codeNotify, int nCmdID, fk::ICommandItem *pCmdUI);
	virtual HRESULT STDMETHODCALLTYPE QueryState(UINT nCmdID, fk::ICommandItem *pCmdUI);

	virtual STDMETHODIMP NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb);
	virtual STDMETHODIMP GetCommandTips(UINT iCmdID, BSTR *bstrTips);
	virtual STDMETHODIMP ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu);

	virtual STDMETHODIMP GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo);
	virtual STDMETHODIMP SetParam(ULONG lParam);
	virtual STDMETHODIMP QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand);
	virtual STDMETHODIMP QueryNotifyCommandParam();

	virtual STDMETHODIMP SetNotifyCommandParent(fk::INotifyCommand* pNotifyCmd);
	virtual STDMETHODIMP GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd);
};

void [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::OnShowOptionsDlg(fk::ICommandItem* pCmdUI)
{
	fk_TraceText(FTEXTNIL, _T("��ʾ���öԻ���\n") );
	fk::ITreePropertySheet* psheet;
	xmlDoc* pxmldoc;
	HRESULT	hr;
	HRESULT nDlgRet;
	fk::LAutoPtr ap;

	try
	{
		if (fk::GetConfig(fk::HCONFIG_APPLICATION, &pxmldoc))
		{
			hr = CoCreateInstancePtr(fk::CLSID_TreePropertySheet, NULL, CLSCTX_ALL, fk::IID_ITreePropertySheet, (void**)&psheet);
			if(hr==S_OK)
			{
				hr=psheet->SetConfigPos((ULONG*)pxmldoc, ap.SysAllocString(fk::XMLF_TREE_PROPERTY_SHEET_W));
				if(hr==S_OK)
				{
					nDlgRet = psheet->DoModal(GetActiveWindow());
					psheet->Release();
					if (nDlgRet==FALSE)
					{
						fk_ErrorBox();
					}
				}
				else if(hr==S_FALSE)
				{
					psheet->Release();
				}
			}
			else
			{
				ATLASSERT(hr == S_OK);
				fk_TraceFormatMessage(FTEXTNIL, HRESNIL, L"\n", hr);
				fk_ErrorBox();
			}
		}
		else
		{
			fk_ErrorBox();
		}
	}
	catch(fk::CComException *pException)
	{
		fk_TraceError(FTEXTNIL, g_pmodule->hRes, pException->GetErrorInfo());

		pException->ShowErrorBox();
		delete pException;
	}
}


[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]()
{

}


[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::~[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]()
{

}

FK_COM_AR_DELETE_CODE([!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]);

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	FK_QUERY_IID(riid, fk::IID_INotifyCommand, fk::INotifyCommand, ppv, this);
	FK_QUERY_IID(riid, IID_IUnknown, IUnknown, ppv, this);

	return E_NOTIMPL;
}

void [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::OnFileOpen(fk::ICommandItem* pCmdUI)
{
}

HRESULT STDMETHODCALLTYPE [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::NotifyCommand(UINT codeNotify, int iCmdID, fk::ICommandItem *pCmdUI)
{
	FK_COMMAND_HANDLE(ID_FILE_OPEN, OnFileOpen(pCmdUI));
	FK_COMMAND_HANDLE(ID_TOOLS_OPTIONS, OnShowOptionsDlg(pCmdUI));

	return S_OK;
}

HRESULT STDMETHODCALLTYPE [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::QueryState(UINT iCmdID, fk::ICommandItem *pCmdUI)
{

	return S_OK;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb)
{
	pCmdUI->put_NextCmd(VARIANT_TRUE);
	return S_OK;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::GetCommandTips(UINT iCmdID, BSTR *bstrTips)
{
	ATL::CComBSTR bstrRet;

	bstrRet.LoadString(g_pmodule->hRes, iCmdID);
	if(bstrRet.Length()>0)
	{
		*bstrTips = bstrRet.Detach();
		return S_OK;
	}

	return S_FALSE;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI)
{
	pCmdUI->put_NextCmd(VARIANT_FALSE);

	return S_OK;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI)
{
	return S_OK;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}


STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo)
{
	ATLASSERT(0);

	return E_NOTIMPL;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::SetParam(ULONG lParam)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::QueryNotifyCommandParam()
{
	HRESULT hr;

	if (_pNotifyCmdParent!=NULL)
	{
		hr = _pNotifyCmdParent->QueryChildNotifyCommandParam(_pSelfIID, this);
		if (hr==S_OK)
			return S_OK;

		return S_FALSE;
	}

	return S_FALSE;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::SetNotifyCommandParent(fk::INotifyCommand* pNotifyCommand)
{
	if (pNotifyCommand!=NULL)
	{
		if (pNotifyCommand!=NULL)
		{
			_pNotifyCmdParent = pNotifyCommand;
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}

STDMETHODIMP [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]::GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd)
{
	if (ppNotifyCmd!=NULL)
	{
		if (_pNotifyCmdParent!=NULL)
		{
			*ppNotifyCmd = _pNotifyCmdParent;
			_pNotifyCmdParent->AddRef();
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}



[!PROJECT_NOTIFY_CMD_CLASS_FULL]::[!PROJECT_NOTIFY_CMD_CLASS_FULL]()
{
}

[!PROJECT_NOTIFY_CMD_CLASS_FULL]::~[!PROJECT_NOTIFY_CMD_CLASS_FULL]()
{
}

b_fncall [!PROJECT_NOTIFY_CMD_CLASS_FULL]::New[!PROJECT_NOTIFY_CMD_NAME]_IID(LPVOID* ppv, REFIID riid  _FK_OBJECT_POS_CXX2_)
{
	[!PROJECT_NOTIFY_CMD_CLASS_FULL]* pNotifyCmd = NULL;

	pNotifyCmd = new [!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]();
	pNotifyCmd->AddRef();

	*ppv = (fk::INotifyCommand*)([!PROJECT_NOTIFY_CMD_CLASS_FULL]*)pNotifyCmd;

	return true;
}


[!PROJECT_NOTIFY_CMD_CLASS_FULL]* _fncall [!PROJECT_NOTIFY_CMD_CLASS_FULL]::New[!PROJECT_NOTIFY_CMD_NAME](void)
{
	[!NAMESPACE_FULL][!PROJECT_NOTIFY_CMD_CLASS_FULL]* pNotifyCmd = NULL;

	pNotifyCmd = new [!NAMESPACE_FULL][!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]();
	pNotifyCmd->AddRef();

	return pNotifyCmd;
}



[!END_NAMESPACE_MACRO]
