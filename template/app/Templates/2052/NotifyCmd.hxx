#ifndef __[!PROJECT_NOTIFY_CMD_NAME]_h__
#define __[!PROJECT_NOTIFY_CMD_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]

class [!PROJECT_NOTIFY_CMD_CLASS_FULL] : public fk::INotifyCommand
{
private:
protected:
public:
	_FK_DBGCLASSINFO_

	[!PROJECT_NOTIFY_CMD_CLASS_FULL](void);
	virtual ~[!PROJECT_NOTIFY_CMD_CLASS_FULL](void);

	static b_fncall New[!PROJECT_NOTIFY_CMD_NAME]_IID(LPVOID* ppobj, REFIID riid=GUID_NULL  _FK_OBJECT_POS_H2_);
	static [!PROJECT_NOTIFY_CMD_CLASS_FULL]* _fncall New[!PROJECT_NOTIFY_CMD_NAME](void);
};

[!END_NAMESPACE_MACRO]



#endif
