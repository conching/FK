#include "StdAfx.h"
#include "[!PROJECT_MGR_NAME][!HXX]"
#include "resource.h"
#include "fk\xmldef.hxx"
#include "[!PROJECT_NOTIFY_CMD_NAME][!HXX]"



[!BEGIN_NAMESPACE_MACRO]


[!PROJECT_MGR_CLASS_FULL]* g_p[!PROJECT_MGR_NAME] = NULL;

static wchar_t*	RES_[!PROJECT_NAME]_VXML			= L"[!PROJECT_NAME].vxml";

class [!PROJECT_MGR_CLASS_FULL_IMPL] : public [!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]
{
	_FK_RTTI_;

private:

private:
	fk::LAutoPtr		_autoPtr;
	[!NAMESPACE_FULL][!PROJECT_NOTIFY_CMD_CLASS_FULL]*	_pNotifyCommand;

	b_call InitFk(void);
	b_call CreateMainFrame_(void);
	b_call RegMainFrameTargetProc_(fk::enumInstallEvent installEvent);

	v_call OnMFActiveItem(fk::PNOTIFYEVENT pEvent);
	v_call OnMFQueryClose(fk::PNOTIFYEVENT pEvent);
	v_call OnMFMainClose(fk::PNOTIFYEVENT pEvent);

	static b_fncall [!PROJECT_NAME]LanguageProc(fk::umLoadLanguage loadLanguage);

public:
	[!PROJECT_MGR_CLASS_FULL_IMPL](void);
	virtual ~[!PROJECT_MGR_CLASS_FULL_IMPL](void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);

	virtual i_call WinMain(bool bAutomation);
};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL_IMPL], [!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL], fk::LComponent, [!PROJECT_MGR_CLASS_FULL_IMPL], [!PROJECT_MGR_CLASS_FULL]);
[!PROJECT_MGR_CLASS_FULL]::[!PROJECT_MGR_CLASS_FULL](void):
fk::LComponent(g_pmodule)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]);
	_pMainFrame = NULL;
}

[!PROJECT_MGR_CLASS_FULL]::~[!PROJECT_MGR_CLASS_FULL](void)
{

}


[!PROJECT_MGR_CLASS_FULL]* _fncall [!PROJECT_MGR_CLASS_FULL]::New[!PROJECT_MGR_NAME](void)
{
	if (g_p[!PROJECT_MGR_NAME]==NULL)
	{
		g_p[!PROJECT_MGR_NAME] = new [!PROJECT_MGR_CLASS_FULL_IMPL]();

		if (g_p[!PROJECT_MGR_NAME]!=NULL)
		{
			if (g_p[!PROJECT_MGR_NAME]->create())
			{
				g_p[!PROJECT_MGR_NAME]->AddRef();

				return g_p[!PROJECT_MGR_NAME];
			}
			else
			{
				delete g_p[!PROJECT_MGR_NAME];
				g_p[!PROJECT_MGR_NAME] = NULL;
			}
		}
	}

	return g_p[!PROJECT_MGR_NAME];
}

FK_BEGIN_TARGETPROC([!PROJECT_MGR_CLASS_FULL_IMPL])
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_ACTIVEITEM, OnMFActiveItem(pEvent));
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_QUERYCLOSE, OnMFQueryClose(pEvent));
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_MAINFRAMECLOSE, OnMFMainClose(pEvent));
FK_END_TARGETPROC();

ULONG STDMETHODCALLTYPE [!PROJECT_MGR_CLASS_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

[!PROJECT_MGR_CLASS_FULL_IMPL]::[!PROJECT_MGR_CLASS_FULL_IMPL](void)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL_IMPL]);
	_pComFramework	= NULL;
	_pNotifyCommand = NULL;

	_autoPtr.AddUnknown2((IUnknown**)&_pNotifyCommand, (IUnknown**)&_pComFramework);

	fk::LanguageRegsvrLoad( (fk::LOADLANGUAGEPROC)[!PROJECT_MGR_CLASS_FULL_IMPL]::[!PROJECT_NAME]LanguageProc );
}

[!PROJECT_MGR_CLASS_FULL_IMPL]::~[!PROJECT_MGR_CLASS_FULL_IMPL](void)
{
	_autoPtr.clear();

	fk::LanguageUnregsvrLoad( (fk::LOADLANGUAGEPROC)[!PROJECT_MGR_CLASS_FULL_IMPL]::[!PROJECT_NAME]LanguageProc );

	fk::UninitApplicationInfo();
}

b_fncall [!PROJECT_MGR_CLASS_FULL_IMPL]::[!PROJECT_NAME]LanguageProc(fk::umLoadLanguage loadLanguage)
{
	switch(loadLanguage)
	{
	case fk::umLoadLanguageInstall:
		{
			// 这段代码使用 .dll 文件资源。
			//
			//if (fk::LanguageLoadProjectInline(g_pmodule,  _AtlBaseModule.GetModuleInstance() ) )
			//	return true;

			// 这段代码使用自身 .exe 文件资源。
			if (fk::LanguageLoadSelf(g_pmodule, _AtlBaseModule.GetModuleInstance()) )
				return true;

			return false;
		}
	case fk::umLoadLanguageUninstall:
		{

		}
	}

	return false;
}

b_call [!PROJECT_MGR_CLASS_FULL_IMPL]::RegMainFrameTargetProc_(fk::enumInstallEvent installEvent)
{
	fk::NOTIFYITEM		npi;
	fk::LNotify*		pNotify;
	HRESULT				hr;

	npi.dwSize			= sizeof(fk::NOTIFYITEM);
	npi.dwMask			= fk::NIF_CLASSPROC;
	npi.dwActiveEvent	= fk::ON_MF_ACTIVEITEM | fk::ON_MF_INSERTITEM | fk::ON_MF_MAINFRAMECLOSE;
	npi.pTargetProc		= (fk::LObject*)(this);
	npi.lParam			= (LPARAM)0;
	npi.dwRegObject		= (void*)_pMainFrame;
	npi.dwObjectID		= (DWORD)(void*)_pMainFrame;

	hr = _pMainFrame->QueryInterface(fk::IID_INotify, (LPVOID*)&pNotify);
	if (hr==S_OK)
	{
		if (installEvent==fk::umInstallEventInstall)
		{
			if (pNotify->NotifyAdd(&npi))
			{
				return true;
			}
			else
			{
				fk_ErrorBox();
			}
		}
		else if (installEvent==fk::umInstallEventUninstall)
		{
			if (pNotify->NotifyRemove(&npi))
			{
				return true;
			}
			else
			{
				fk_ErrorBox();
			}
		}
	}

	return false;
}

//
// 这里的初始化比较复杂，以后整合消减。
//
b_call [!PROJECT_MGR_CLASS_FULL_IMPL]::InitFk(void)
{
	HRESULT					hr;
	fk::LStringw			sSoftName;
	fk::INITAPPLICATIONW	initApp;

	if (fk::InitializeModule(&g_pmodule, _AtlBaseModule.m_hInst))
	{
		//
		// 如果有需要就添加。
		//
		// g_pmodule->fnGetDllDnaXmlFile = GetDllDnaXmlFile;
		//

		memset((void*)&initApp, 0, sizeof(fk::INITAPPLICATIONW));
		initApp.dwSize		= sizeof(fk::INITAPPLICATIONW);
		initApp.hMainModule = _AtlBaseModule.m_hInst;
		initApp.dwMask		= IAF_USER_CONFIG | IAF_APP_CONCIFG;
		initApp.pMainModule	= g_pmodule;
		initApp.dwMask		= IAF_USER_CONFIG | IAF_APP_CONCIFG;
		initApp.pMainModule= g_pmodule;

		if (fk::InitApplicationInfo(&initApp))
		{
			initApp.dwMask		= IAF_SOFTWARE_NAME;
			sSoftName.LoadStr(g_pmodule->hRes, IDS_PROJNAME);
			initApp.wsSoftwareName = (wchar_t*)sSoftName._pwstr;

			hr = CoCreateInstancePtr(fk::CLSID_ComFramework, NULL, CLSCTX_ALL, fk::IID_IComFramework, (LPVOID*)&_pComFramework);
			if (hr==S_OK)
			{
				hr = _pComFramework->InitComFramework();
				if (hr==S_OK)
				{

					return true;
				}
			}
		}
	}

	fk_ErrorBox();
	return false;
}


//
// 创建框架并安装命令接受器，显示主窗口。
//
b_call [!PROJECT_MGR_CLASS_FULL_IMPL]::CreateMainFrame_()
{
	HRESULT				hr;
	fk::ICommandBars*	pCommandBars = NULL;
	fk::LAutoPtr		ap;

	ap.AddUnknown((IUnknown**)&pCommandBars);

	hr = _pComFramework->CreateMainFrameObject((fk::IMainFrame**)&_pMainFrame);
	if (hr==S_OK)
	{
		RegMainFrameTargetProc_(fk::umInstallEventInstall);

		hr = _pMainFrame->CreateMainFrameXml((long*)g_pmodule, RES_[!PROJECT_NAME]_VXML);
		if (hr==S_OK)
		{
			_pNotifyCommand = [!NAMESPACE_FULL][!PROJECT_NOTIFY_CMD_CLASS_FULL]::New[!PROJECT_NOTIFY_CMD_NAME]();

			_pMainFrame->get_CommandBars((IUnknown**)&pCommandBars);
			_pMainFrame->InstallNotifyCommand(fk::umInstallEventInstall, _pNotifyCommand);

			pCommandBars->put_NotifyCommand((fk::INotifyCommand*)_pNotifyCommand);

			//
			// 这个函数同等于 ::ShowWindow 函数，传入 -1 是说明是使用 fk 读保存的坐标值。
			//
			_pMainFrame->ShowWindow(-1);
			return true;
		}
		else
		{
			fk_ErrorBox();
			return false;
		}

		return true;
	}
	else
	{
		fk_ErrorBox();
	}

	fk_ErrorBox();
	return false;
}

v_call [!PROJECT_MGR_CLASS_FULL_IMPL]::OnMFActiveItem(fk::PNOTIFYEVENT pEvent)
{
}

v_call [!PROJECT_MGR_CLASS_FULL_IMPL]::OnMFQueryClose(fk::PNOTIFYEVENT pEvent)
{
}

v_call [!PROJECT_MGR_CLASS_FULL_IMPL]::OnMFMainClose(fk::PNOTIFYEVENT pEvent)
{
}

i_call [!PROJECT_MGR_CLASS_FULL_IMPL]::WinMain(bool bAutomation)
{
	if (InitFk())
	{
		if (CreateMainFrame_())
		{
			_pComFramework->Run();
			return 1;
		}
	}

	fk_ErrorBox();
	return 0;
}




[!END_NAMESPACE_MACRO]
