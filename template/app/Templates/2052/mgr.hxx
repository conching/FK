#ifndef __[!PROJECT_MGR_NAME]_h__
#define __[!PROJECT_MGR_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]


class [!PROJECT_MGR_CLASS_FULL] : public fk::LComponent
{
	_FK_RTTI_;

private:
public:
	[!PROJECT_MGR_CLASS_FULL](void);
	virtual ~[!PROJECT_MGR_CLASS_FULL](void);

	fk::IComFramework*	_pComFramework;
	fk::IMainFrame*  _pMainFrame;

	virtual i_call WinMain(bool bAutomation) = 0;

	static [!PROJECT_MGR_CLASS_FULL]* _fncall New[!PROJECT_MGR_NAME](void);
};

extern [!PROJECT_MGR_CLASS_FULL]* g_[!PROJECT_MGR_NAME];

[!END_NAMESPACE_MACRO]


#endif