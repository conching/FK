//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by [!PROJECT_NAME].rc
//

#define IDS_PROJNAME                    100
#define IDR_[!UPPER_PROJECT_NAME]		101
#define IDR_MAINFRAME					102
#define ID_TOOLS_OPTIONS				32768
#define ID_HELP_HELP					32769

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32770
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
