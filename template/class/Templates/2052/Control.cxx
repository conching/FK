#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]

class [!CLASS_NAME_FULL_IMPL] : public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
	_FK_RTTI_;

private:
public:
	virtual b_call Assign(LObject* pobject);

	[!BUILD_CLASS_PROP_FUNCATION]
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

public:
	[!CLASS_NAME_FULL_IMPL](fk::LWindow* pParentWin);
	virtual ~[!CLASS_NAME_FULL_IMPL](void);
};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL], [!NAMESPACE_FULL][!CLASS_NAME_FULL], [!BASE_CLASS], [!CLASS_NAME_FULL_IMPL], [!CLASS_NAME_FULL]);
//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LWindow* pParentWin):
[!BASE_CLASS](pParentWin)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME](fk::LWindow* pParentWin, fk::LWindow** ppwin)
{
	[!CLASS_NAME_FULL]*  pobject= NULL;

	pobject = new [!CLASS_NAME_FULL_IMPL](powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}
//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](fk::LWindow* pParentWin):
[!NAMESPACE_FULL][!CLASS_NAME_FULL](pParentWin)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL]);
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL](void)
{
}

//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC([!CLASS_NAME_FULL_IMPL])
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

vb_call [!CLASS_NAME_FULL_IMPL]::Assign(LObject* pobject)
{
	return [!CLASS_NAME_FULL]::Assign(pobject);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]

[!END_NAMESPACE_MACRO]
