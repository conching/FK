#ifndef __[!CLASS_NAME]_h__
#define __[!CLASS_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]

// [!GUID_REGISTRY_FORMAT]
static const GUID IID_[!CLASS_NAME] = [!GUID_HEX_STRUCT_FORMAT];

class [!CLASS_NAME_FULL] : public [!BASE_CLASS]
{
	_FK_RTTI_;

private:
public:
	[!CLASS_NAME_FULL](fk::LWindow* pParentWin);
	virtual ~[!CLASS_NAME_FULL](void);

	static b_fncall New[!CLASS_NAME](fk::LWindow* pParentWin, fk::LWindow** ppwin);
};

[!END_NAMESPACE_MACRO]


#endif
