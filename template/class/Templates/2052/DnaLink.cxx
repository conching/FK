#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]

class [!CLASS_NAME_FULL_IMPL] : public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
	_FK_RTTI_;

private:
public:
	[!CLASS_NAME_FULL_IMPL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL_IMPL](void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	virtual b_call Assign(LObject* pobject);
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
	[!BUILD_CLASS_PROP_FUNCATION]

	virtual b_call LoadDnaPosition(void);

	vb_call LinkDnaPosition(void);
public:

};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL], [!NAMESPACE_FULL][!CLASS_NAME_FULL], fk::LDnaLink, [!CLASS_NAME_FULL_IMPL], [!CLASS_NAME_FULL]);
//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LObject* powner):
fk::LDnaLink(powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid)
{
	fk_pv2(powner==NULL, ppobj==NULL, return false;);
	[!NAMESPACE_FULL][!CLASS_NAME_FULL]*  pobject= NULL;

	pobject = new [!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL](powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC([!CLASS_NAME_FULL_IMPL])
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](fk::LObject* powner):
[!NAMESPACE_FULL][!CLASS_NAME_FULL](powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL]);
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL](void)
{
}

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	FK_QUERY_IID(riid, fk::IID_DnaLink, fk::LDnaLink, ppv, this);
	FK_QUERY_IID(riid, [!NAMESPACE_FULL]IID_[!CLASS_NAME], [!NAMESPACE_FULL][!CLASS_NAME_FULL], ppv, this);

	return [!NAMESPACE_FULL][!CLASS_NAME_FULL]::QueryInterface(riid, ppv);
}

vb_call [!CLASS_NAME_FULL_IMPL]::Assign(LObject* pobject)
{
	return [!CLASS_NAME_FULL]::Assign(pobject);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]


/*
查找 fk::DnaPosition 对象，并关联到当前对象。之后调用 LoadDnaPosition 方法，也可以不调用 LoadDnaPosition，或者移出。
*/
vb_call [!CLASS_NAME_FULL_IMPL]::LinkDnaPosition(void)
{
	fk::LGuid	guid;

	fk_autop3(fk::LModuleConfigManager*, pmcm,
				fk::LModuleConfig*, pmc,
				fk::LDnaPositionFullManager*, ppfm
				);

	if (fk::GetModuleConfigManager(&pmcm))
	{
		if (pmcm->GetDnaPositionFullManager(&ppfm))
		{
			if (guid.SetGuid((GUID*)&fk::IID_[!CLASS_NAME]))			// IID_xxxxxxxxxxxxx 修改为你的链接点ID。
			{
				if (ppfm->FindDnaPosition(&guid, &_pdnaPosition))		//
				{
					if (LoadDnaPosition())
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

b_call [!CLASS_NAME_FULL_IMPL]::LoadDnaPosition(void)
{
	data_t*			pdata;
	fk::LDnaItem*	pdnaItem;

	pdata = _pdnaPosition->GetDatas();
	for (; *pdata!=FK_DATA_END; pdata++)
	{
		pdnaItem = (fk::LDnaItem*)*pdata;
	}

	return true;
}

[!END_NAMESPACE_MACRO]
