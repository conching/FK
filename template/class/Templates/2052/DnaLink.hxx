#ifndef __[!CLASS_NAME]_h__
#define __[!CLASS_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]

// [!GUID_REGISTRY_FORMAT]
static const GUID IID_[!CLASS_NAME] = [!GUID_HEX_STRUCT_FORMAT];

class [!CLASS_NAME_FULL] : public fk::LDnaLink		//[!BASE_CLASS]
{
	_FK_RTTI_;

private:
protected:
	fk::LDnaPosition*	_pdnaPosition;

public:
	[!CLASS_NAME_FULL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL](void);
	/*
    	查找 fk::DnaPosition 对象，并关联到当前对象。
    */
    static b_fncall New[!CLASS_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid);
};

[!END_NAMESPACE_MACRO]


#endif
