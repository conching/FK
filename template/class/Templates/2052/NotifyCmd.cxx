#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"



[!BEGIN_NAMESPACE_MACRO]


#define ID_FILE_OPEN			10000


class [!CLASS_NAME_FULL_IMPL] : public [!CLASS_NAME_FULL]
{
private:
	IID*				_pSelfIID;
	fk::INotifyCommand*		_pNotifyCmdParent;

	void OnFileOpen(fk::ICommandItem* pCmdUI);
	void OnFileClose(void);

protected:
public:
	[!CLASS_NAME_FULL_IMPL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL_IMPL]();

	FK_COM_ARQ();

	virtual HRESULT STDMETHODCALLTYPE NotifyCommand(UINT codeNotify, int nCmdID, fk::ICommandItem *pCmdUI);
	virtual HRESULT STDMETHODCALLTYPE QueryState(UINT nCmdID, fk::ICommandItem *pCmdUI);

	virtual STDMETHODIMP NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb);
	virtual STDMETHODIMP GetCommandTips(UINT iCmdID, BSTR *bstrTips);
	virtual STDMETHODIMP ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu);

	virtual STDMETHODIMP GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo);
	virtual STDMETHODIMP SetParam(ULONG lParam);
	virtual STDMETHODIMP QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand);
	virtual STDMETHODIMP QueryNotifyCommandParam();

	virtual STDMETHODIMP SetNotifyCommandParent(fk::INotifyCommand* pNotifyCmd);
	virtual STDMETHODIMP GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd);
};

//////////////////////////////////////////////////////////////////////////

[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LObject* powner)
{
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL]()
{
}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME_FULL](fk::LObject* powner, LPVOID* ppobj, REFIID riid)
{
	[!CLASS_NAME_FULL_IMPL]* pNotifyCmd = NULL;

	pNotifyCmd = new [!CLASS_NAME_FULL_IMPL]();
	pNotifyCmd->AddRef();

	*ppobj = (fk::INotifyCommand*)([!CLASS_NAME_FULL]*)pNotifyCmd;

	return true;
}

//////////////////////////////////////////////////////////////////////////

[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](fk::LObject* powner):
[!CLASS_NAME_FULL](powner)
{

}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL]()
{

}

FK_COM_AR_DELETE_CODE([!CLASS_NAME_FULL_IMPL]);

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	FK_QUERY_IID(riid, [!NAMESPACE_FULL]IID_[!CLASS_NAME], [!NAMESPACE_FULL][!CLASS_NAME], ppv, this);
	FK_QUERY_IID(riid, fk::IID_INotifyCommand, fk::INotifyCommand, ppv, this);
	FK_QUERY_IID(riid, IID_IUnknown, IUnknown, ppv, this);

	return E_NOTIMPL;
}

void [!CLASS_NAME_FULL_IMPL]::OnFileOpen(fk::ICommandItem* pCmdUI)
{
}

HRESULT STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::NotifyCommand(UINT codeNotify, int iCmdID, fk::ICommandItem *pCmdUI)
{
	FK_COMMAND_HANDLE(ID_FILE_OPEN, OnFileOpen(pCmdUI));

	return S_OK;
}

HRESULT STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::QueryState(UINT iCmdID, fk::ICommandItem *pCmdUI)
{

	return S_OK;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb)
{
	pCmdUI->put_NextCmd(VARIANT_TRUE);
	return S_OK;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::GetCommandTips(UINT iCmdID, BSTR *bstrTips)
{
	ATL::CComBSTR bstrRet;

	bstrRet.LoadString(g_pmodule->hRes, iCmdID);
	if(bstrRet.Length()>0)
	{
		*bstrTips = bstrRet.Detach();
		return S_OK;
	}

	return S_FALSE;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI)
{
	pCmdUI->put_NextCmd(VARIANT_FALSE);

	return S_OK;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI)
{
	return S_OK;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}


STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo)
{
	ATLASSERT(0);

	return E_NOTIMPL;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::SetParam(ULONG lParam)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::QueryNotifyCommandParam()
{
	HRESULT hr;

	if (_pNotifyCmdParent!=NULL)
	{
		hr = _pNotifyCmdParent->QueryChildNotifyCommandParam(_pSelfIID, this);
		if (hr==S_OK)
			return S_OK;

		return S_FALSE;
	}

	return S_FALSE;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::SetNotifyCommandParent(fk::INotifyCommand* pNotifyCommand)
{
	if (pNotifyCommand!=NULL)
	{
		if (pNotifyCommand!=NULL)
		{
			_pNotifyCmdParent = pNotifyCommand;
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd)
{
	if (ppNotifyCmd!=NULL)
	{
		if (_pNotifyCmdParent!=NULL)
		{
			*ppNotifyCmd = _pNotifyCmdParent;
			_pNotifyCmdParent->AddRef();
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}



[!END_NAMESPACE_MACRO]
