#ifndef __[!CLASS_NAME]_h__
#define __[!CLASS_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]

// [!GUID_REGISTRY_FORMAT]
static const GUID IID_[!CLASS_NAME] = [!GUID_HEX_STRUCT_FORMAT];

class [!CLASS_NAME_FULL] : public fk::INotifyCommand
{
private:
protected:
public:
	_FK_DBGCLASSINFO_

	[!CLASS_NAME_FULL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL](void);

	static b_fncall New[!CLASS_NAME_FULL](fk::LObject* powner, LPVOID* ppobj, REFIID riid);
};

[!END_NAMESPACE_MACRO]

#endif
