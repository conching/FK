#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]

class [!CLASS_NAME_FULL_IMPL] : public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
private:
public:
	[!CLASS_NAME_FULL_IMPL](void);
	virtual ~[!CLASS_NAME_FULL_IMPL](void);

	virtual b_call Assign(LObject* pobject);
	virtual ULONG STDMETHODCALLTYPE Release();

	[!BUILD_CLASS_PROP_FUNCATION]

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
};

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](void)
{
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}

[!CLASS_NAME_FULL]* _call [!CLASS_NAME_FULL]::New[!CLASS_NAME](void)
{
	[!CLASS_NAME_FULL]*  pobject= NULL;

	pobject = new [!CLASS_NAME_FULL_IMPL]();
	if (pobject!=NULL)
	{
		pobject->AddRef();
		return pobject;
	}

	return NULL;
}



//---------------------------------------------------------------------------------
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC([!CLASS_NAME_FULL_IMPL])
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](void)
{
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL](void)
{
}

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call [!CLASS_NAME_FULL_IMPL]::Assign(LObject* pobject)
{
	return [!CLASS_NAME_FULL]::Assign(pobject);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]

[!END_NAMESPACE_MACRO]
