#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]

class [!CLASS_NAME_FULL_IMPL] : public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
	_FK_RTTI_;

private:
public:
	[!CLASS_NAME_FULL_IMPL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL_IMPL](void);

	virtual b_call Assign(LObject* pobject);
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	[!BUILD_CLASS_PROP_FUNCATION]
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

public:

};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL], [!NAMESPACE_FULL][!CLASS_NAME_FULL], [!BASE_CLASS], [!CLASS_NAME_FULL_IMPL], [!CLASS_NAME_FULL]);

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LObject* powner):
fk::LObject(powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME](fk::LObject* powner, LPVOID* ppobj, IID riid)
{
	fk_pv2(powner==NULL, ppobj==NULL, return false;);
	[!NAMESPACE_FULL][!CLASS_NAME_FULL]*  pobject= NULL;

	pobject = new [!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL](powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC([!CLASS_NAME_FULL_IMPL])
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](fk::LObject* powner):
[!NAMESPACE_FULL][!CLASS_NAME_FULL](powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL]);
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL](void)
{
}

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

STDMETHODIMP [!CLASS_NAME_FULL_IMPL]::QueryInterface(REFIID riid, void **ppv)
{
	FK_QUERY_IID(riid, [!NAMESPACE_FULL]IID_[!CLASS_NAME], [!NAMESPACE_FULL][!CLASS_NAME_FULL], ppv, this);

	return [!NAMESPACE_FULL][!CLASS_NAME_FULL]::QueryInterface(riid, ppv);
}

vb_call [!CLASS_NAME_FULL_IMPL]::Assign(LObject* pobject)
{
	return [!CLASS_NAME_FULL]::Assign(pobject);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]

[!END_NAMESPACE_MACRO]
