#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"



[!BEGIN_NAMESPACE_MACRO]

[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](void)
{
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}


ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call [!CLASS_NAME_FULL]::Execute()
{
}


[!END_NAMESPACE_MACRO]
