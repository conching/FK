#include "stdafx.h"
#include "[!CLASS_NAME][!HXX]"



[!BEGIN_NAMESPACE_MACRO]

[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LObject* powner):
fk::[!CLASS_NAME_FULL](powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL](void)
{
}

_FK_RTTI_CODE_([!NAMESPACE_FULL][!CLASS_NAME_FULL], fk::LThread, [!CLASS_NAME_FULL]);

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call [!CLASS_NAME_FULL]::execute()
{
}


[!END_NAMESPACE_MACRO]
