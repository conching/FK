#ifndef __[!CLASS_NAME]_h__
#define __[!CLASS_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]


// [!GUID_REGISTRY_FORMAT]
static const GUID IID_[!CLASS_NAME] = [!GUID_HEX_STRUCT_FORMAT];


class [!CLASS_NAME_FULL] : public fk::LThread
{
	_FK_RTTI_;

private:
public:
	[!CLASS_NAME_FULL](fk::LObject* powner);
	virtual ~[!CLASS_NAME_FULL](void);

	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call Execute();
};

[!END_NAMESPACE_MACRO]


#endif
