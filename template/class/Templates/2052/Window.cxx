#include "stdafx.h"
#include "[!CLASS_SHORT_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]


class  [!CLASS_NAME_FULL_IMPL]: public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
	_FK_RTTI_;

private:
	FK_BEGIN_CODE(EVENTDEFAULT)
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnCreate(LPARAM lParam);
	FK_END_CODE()

private:

public:
	[!CLASS_NAME_FULL_IMPL](fk::LWindow* pParentWindow);
	virtual ~[!CLASS_NAME_FULL_IMPL]();

	[!BUILD_CLASS_PROP_FUNCATION]
	// base class
	virtual l_call SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual ULONG STDMETHODCALLTYPE Release();

public:
	// user
};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL], [!NAMESPACE_FULL][!CLASS_NAME_FULL], [!BASE_CLASS], [!CLASS_NAME_FULL_IMPL], [!CLASS_NAME_FULL]);

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL](fk::LWindow* pParentWindow)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL]()
{

}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME](fk::LWindow* pParentWin, fk::LWindow** ppwin)
{
	[!CLASS_NAME_FULL_IMPL]*  pobject = NULL;

	pobject = new [!CLASS_NAME_FULL_IMPL]();
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//INT_PTR _call [!CLASS_NAME_FULL_IMPL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_SUBWNDPROC([!CLASS_NAME_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_CREATE, OnCreate(lParam), return 0);
FK_END_SUBWNDPROC([!CLASS_NAME_FULL])		 //***** �ǻ��ࡣ

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL](fk::LWindow* pParentWindow):
[!NAMESPACE_FULL][!CLASS_NAME_FULL](pParentWindow)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL]);
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL]()
{
}

v_call [!CLASS_NAME_FULL_IMPL]::OnCreate(LPARAM lParam)
{
	LPCREATESTRUCT lpCreateStruct = (LPCREATESTRUCT)lParam;

}

void [!CLASS_NAME_FULL_IMPL]::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]

[!END_NAMESPACE_MACRO]
