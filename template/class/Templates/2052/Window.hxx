#ifndef _[!CLASS_SHORT_NAME]_h_
#define _[!CLASS_SHORT_NAME]_h_


[!BEGIN_NAMESPACE_MACRO]

class [!CLASS_NAME_FULL]: public [!BASE_CLASS]
{
	_FK_RTTI_;

private:

public:
	[!CLASS_NAME_FULL](fk::LWindow* pParentWindow);
	virtual ~[!CLASS_NAME_FULL]();

	static b_fncall New[!CLASS_NAME](fk::LWindow* pParentWin, fk::LWindow** ppwin);
};


[!END_NAMESPACE_MACRO]


#endif
