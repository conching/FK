#include "stdafx.h"
#include "[!CLASS_SHORT_NAME][!HXX]"


[!BEGIN_NAMESPACE_MACRO]


class  [!CLASS_NAME_FULL_IMPL]: public [!NAMESPACE_FULL][!CLASS_NAME_FULL]
{
	_FK_RTTI_;

private:
	FK_BEGIN_CODE(VARIABLE)
	FK_END_CODE()

	FK_BEGIN_CODE(EVENTDEFAULT)
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnCreate(LPARAM lParam);
	FK_END_CODE()

private:

public:
	static wchar_t*	[!VXML_NAME_MACRO]_VXML;
	[!CLASS_NAME_FULL_IMPL]();
	virtual ~[!CLASS_NAME_FULL_IMPL]();

	[!BUILD_CLASS_PROP_FUNCATION]
	// base class
	virtual l_call SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	// user
};
static wchar_t*	[!VXML_NAME_MACRO]_VXML	= L"[!VXML_NAME].vxml";

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL], [!NAMESPACE_FULL][!CLASS_NAME_FULL], fk::LWindowMuch, [!CLASS_NAME_FULL_IMPL], [!CLASS_NAME_FULL]);

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL]::[!CLASS_NAME_FULL]():
LWindowMuch(g_pmodule, [!CLASS_NAME_FULL_IMPL]::[!VXML_NAME_MACRO]_VXML)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL]);
}

[!CLASS_NAME_FULL]::~[!CLASS_NAME_FULL]()
{

}

b_fncall [!CLASS_NAME_FULL]::New[!CLASS_NAME_FULL](fk::LWindow* pParentWin, fk::LWindow** ppwin)
{
	[!CLASS_NAME_FULL_IMPL]* pobject = NULL;

	pobject = new [!CLASS_NAME_FULL_IMPL]();
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//INT_PTR _call [!CLASS_NAME_FULL_IMPL]::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_SUBWNDPROC([!CLASS_NAME_FULL_IMPL])
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_CREATE, OnCreate(lParam), return 0);
FK_END_SUBWNDPROC([!CLASS_NAME_FULL])		 //***** 是基类。

// vv_call [!CLASS_NAME_FULL_IMPL]OnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC([!CLASS_NAME_FULL_IMPL])
//FK_COMMAND_EVENT(_pExample, BN_CLICKED, OnOKBtnClick());
FK_END_COMMAND_PROC()

// vv_call [!CLASS_NAME_FULL_IMPL]::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC([!CLASS_NAME_FULL_IMPL])
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
//
vb_call [!CLASS_NAME_FULL_IMPL]::LinkObjectVariable(void)
{
	//fk::LINKOBJECTNAME lon[]={
	//};

	//if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	//{
	//	// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
	//	// 返回 false，窗口不显示。
	//	//
	//	fk_ErrorBox();
	//	return true;
	//}

	return true;
}

ULONG STDMETHODCALLTYPE [!CLASS_NAME_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
[!CLASS_NAME_FULL_IMPL]::[!CLASS_NAME_FULL_IMPL]()
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!CLASS_NAME_FULL_IMPL]);
}

[!CLASS_NAME_FULL_IMPL]::~[!CLASS_NAME_FULL_IMPL]()
{
}

vb_call [!CLASS_NAME_FULL_IMPL]::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call [!CLASS_NAME_FULL_IMPL]::OnCreate(void)
{
	LinkDataExchange(fk::ldeInstallData);
}

void [!CLASS_NAME_FULL_IMPL]::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}

[!BUILD_CLASS_PROP_FUNCATION_CODE]

[!END_NAMESPACE_MACRO]
