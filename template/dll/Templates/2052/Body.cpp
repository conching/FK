#include "stdafx.h"
#include "[!BODY_NAME][!HXX]"


class ATL_NO_VTABLE [!BODY_CLASS_IMPL] :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<[!BODY_CLASS_IMPL], &CLSID_[!BODY_NAME]>,
	public IDispatchImpl<fk::IExtensibility, &__uuidof(fk::IExtensibility)>,
	public IDispatchImpl<[!BODY_CLASS_FULL], &IID_I[!BODY_NAME], &LIBID_[!PROJECT_NAME]Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
private:

public:
	[!BODY_CLASS_IMPL]();
	virtual ~[!BODY_CLASS_IMPL]();

	DECLARE_REGISTRY_RESOURCEID(IDR_[!IDR_UPPER_BODY_NAME])


	//BEGIN_COM_MAP([!BODY_CLASS_IMPL])
	//	COM_INTERFACE_ENTRY(fk::IExtensibility)
	//	COM_INTERFACE_ENTRY(IDispatch)
	//END_COM_MAP()

	virtual STDMETHODIMP _InternalQueryInterface(REFIID iid, void ** ppvObject);

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();
	void FinalRelease();

public:

	virtual STDMETHODIMP OnAddInsUpdate();
	virtual STDMETHODIMP OnBeginShutdown();
	virtual STDMETHODIMP OnConnection(fk::IComFramework* pComFramework, fk::enumConnectMode ConnectMode2, fk::IAddIn * pAddIn, LONG* pUserDispatch);
	virtual STDMETHODIMP OnStartupComplete();
	virtual STDMETHODIMP OnDisconnection(fk::enumDisconnectMode disconnectMode, fk::IMainFrame* pMainFrame, fk::IAddIn *pAddIn);
};

OBJECT_ENTRY_AUTO(__uuidof([!BODY_NAME]), [!BODY_CLASS_IMPL])

[!BODY_CLASS_FULL]::[!BODY_CLASS_FULL]()
{
}

[!BODY_CLASS_FULL]::~[!BODY_CLASS_FULL]()
{
}


//
[!BODY_CLASS_IMPL]::[!BODY_CLASS_IMPL]()
{
}

[!BODY_CLASS_IMPL]::~[!BODY_CLASS_IMPL]()
{
}

STDMETHODIMP [!BODY_CLASS_IMPL]::_InternalQueryInterface(REFIID iid, void ** ppvObject)
{
	FK_QUERY_IID_ATL(iid, IID_[!BODY_INTERFACE], [!BODY_INTERFACE], ppvObject, this);
	FK_QUERY_IID_ATL(iid, fk::IID_IExtensibility, [!BODY_INTERFACE], ppvObject, this);

	return S_FALSE;
}

HRESULT [!BODY_CLASS_IMPL]::FinalConstruct()
{
	return S_OK;
}

void [!BODY_CLASS_IMPL]::FinalRelease()
{
}



STDMETHODIMP [!BODY_CLASS_IMPL]::OnAddInsUpdate()
{
	return S_OK;
}

STDMETHODIMP [!BODY_CLASS_IMPL]::OnBeginShutdown()
{
	return S_OK;
}

STDMETHODIMP [!BODY_CLASS_IMPL]::OnConnection(fk::IComFramework* pComFramework, fk::enumConnectMode ConnectMode2, fk::IAddIn * pAddIn, LONG* pUserDispatch)
{
	try
	{
		switch (ConnectMode2)
		{
		case fk::umConnectModeCommandLine:	//1
			{
			}break;
		case fk::umConnectModeStartup:		//2
			{
			}break;
		case fk::umConnectModeAfterStartup:	//3
			{
			}break;
		case fk::umConnectModeUISetup:		//4
			{
			}break;
		default:
			{
				return E_NOTIMPL;
			}
		}
	}
	catch(...)
	{

	}

	return S_OK;
}

STDMETHODIMP [!BODY_CLASS_IMPL]::OnStartupComplete()
{
	return S_OK;
}

STDMETHODIMP [!BODY_CLASS_IMPL]::OnDisconnection(fk::enumDisconnectMode disconnectMode, fk::IMainFrame* pMainFrame, fk::IAddIn *pAddIn)
{
	switch (disconnectMode)
	{
	case fk::umDisconnectModeUninstallUI:
		{
		}break;
	case fk::umDisconnectModeUserClosed:
		{
		}break;
	case fk::umDisconnectModeCloseAddIn:
		{
		}break;
	}

	return S_OK;
}
