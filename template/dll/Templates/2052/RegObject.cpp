#include "stdafx.h"
#include "[!PROJECT_NAME]RegObject[!HXX]"


[!BEGIN_NAMESPACE_MACRO]


b_fncall Create[!PROJECT_NAME]Class(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject);
b_fncall Link[!PROJECT_NAME]Class(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow);


b_fncall Reg[!PROJECT_NAME]Class(void)
{
	fk::REGOBJECTITEM rci;

	rci.dwSize				= sizeof(fk::REGOBJECTITEM);
	rci.dwMask				= 0;
	rci.lpfnCreateObject	= (fk::LPFNCREATEOBJECT)[!NAMESPACE_FULL]Create[!PROJECT_NAME]Class;
	rci.lpfnLinkObject		= (fk::LPFNLINK_OBJECT)[!NAMESPACE_FULL]Link[!PROJECT_NAME]Class;
	rci.lpszClass			= NULL;

	if (fk::ObjectReg(&rci))
	{
	}
	else
	{
		fk_ErrorBox();
		return false;
	}

	return true;
}

b_fncall Unreg[!PROJECT_NAME]Class(void)
{
	fk::ObjectUnreg((fk::LPFNCREATEOBJECT)[!NAMESPACE_FULL]Create[!PROJECT_NAME]Class);

	return true;
}


b_fncall Create[!PROJECT_NAME]Class(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject)
{
	return true;
}

b_fncall Link[!PROJECT_NAME]Class(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow)
{
	return true;
}

[!END_NAMESPACE_MACRO]
