#ifndef __[!PROJECT_NAME]_Reg_Object_h__
#define __[!PROJECT_NAME]_Reg_Object_h__


[!BEGIN_NAMESPACE_MACRO]

b_fncall Reg[!PROJECT_NAME]Class(void);

b_fncall Unreg[!PROJECT_NAME]Class(void);

[!END_NAMESPACE_MACRO]


#endif
