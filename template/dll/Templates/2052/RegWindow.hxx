#ifndef __[!PROJECT_NAME]_Reg_Window_h__
#define __[!PROJECT_NAME]_Reg_Window_h__


[!BEGIN_NAMESPACE_MACRO]

b_fncall RegCreate[!PROJECT_NAME]Window();
b_fncall UnregCreate[!PROJECT_NAME]Window();

[!END_NAMESPACE_MACRO]


#endif
