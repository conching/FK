#include "StdAfx.h"
#include "[!PROJECT_MGR_NAME][!HXX]"
#include "resource.h"
#include "fk\xmldef.hxx"




[!BEGIN_NAMESPACE_MACRO]


[!PROJECT_MGR_CLASS_FULL]* g_p[!PROJECT_MGR_NAME] = NULL;


class [!PROJECT_MGR_CLASS_FULL_IMPL] : public [!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]
{
	_FK_RTTI_;

private:
	fk::LAutoPtr		_autoPtr;

	b_call RegMainFrameTargetProc_(fk::enumInstallEvent installEvent);
public:
	[!PROJECT_MGR_CLASS_FULL_IMPL](fk::LObject* powner);
	virtual ~[!PROJECT_MGR_CLASS_FULL_IMPL](void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);
};

_FK_RTTI_CODE2_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL_IMPL], [!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL], fk::LComponent, [!PROJECT_MGR_CLASS_FULL_IMPL], [!PROJECT_MGR_CLASS_FULL]);

///////////////////////////////////////////////////////////////////////////////////////////////////
[!PROJECT_MGR_CLASS_FULL]::[!PROJECT_MGR_CLASS_FULL](fk::LObject* powner):
fk::LComponent(powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL]);
}

[!PROJECT_MGR_CLASS_FULL]::~[!PROJECT_MGR_CLASS_FULL](void)
{

}

b_fncall [!PROJECT_MGR_CLASS_FULL]::New[!PROJECT_MGR_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid)
{
	[!PROJECT_MGR_CLASS_FULL_IMPL]*  pobject= NULL;

	pobject = new [!PROJECT_MGR_CLASS_FULL_IMPL](powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
[!PROJECT_MGR_CLASS_FULL_IMPL]::[!PROJECT_MGR_CLASS_FULL_IMPL](fk::LObject* powner):
[!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL](powner)
{
	_FK_RTTI_INIT_([!NAMESPACE_FULL][!PROJECT_MGR_CLASS_FULL_IMPL]);
}

[!PROJECT_MGR_CLASS_FULL_IMPL]::~[!PROJECT_MGR_CLASS_FULL_IMPL](void)
{
	_autoPtr.clear();

	fk::UninitApplicationInfo();
}

//	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC([!PROJECT_MGR_CLASS_FULL_IMPL])
//FK_TARGETPROC_EVENT(_pObject, fk::ON_MSG, OnMFActiveItem(pEvent));
FK_END_TARGETPROC();

ULONG STDMETHODCALLTYPE [!PROJECT_MGR_CLASS_FULL_IMPL]::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

[!END_NAMESPACE_MACRO]
