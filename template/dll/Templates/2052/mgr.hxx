#ifndef __[!PROJECT_MGR_NAME]_h__
#define __[!PROJECT_MGR_NAME]_h__


[!BEGIN_NAMESPACE_MACRO]


class [!PROJECT_MGR_CLASS_FULL] : public fk::LComponent
{
	_FK_RTTI_;

private:
public:
	[!PROJECT_MGR_CLASS_FULL](fk::LObject* powner);
	virtual ~[!PROJECT_MGR_CLASS_FULL](void);

	static b_fncall New[!PROJECT_MGR_NAME](fk::LObject* powner, LPVOID* ppobj, REFIID riid);
};

extern [!PROJECT_MGR_CLASS_FULL]* g_[!PROJECT_MGR_NAME];

[!END_NAMESPACE_MACRO]


#endif