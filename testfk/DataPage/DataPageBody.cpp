#include "stdafx.h"
#include "DataPageBody.hxx"


class ATL_NO_VTABLE LDataPageBodyImpl :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<LDataPageBodyImpl, &CLSID_DataPageBody>,
	public IDispatchImpl<fk::IExtensibility, &__uuidof(fk::IExtensibility)>,
	public IDispatchImpl<LDataPageBody, &IID_IDataPageBody, &LIBID_DataPageLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
private:

public:
	LDataPageBodyImpl();
	virtual ~LDataPageBodyImpl();

	DECLARE_REGISTRY_RESOURCEID(IDR_DATAPAGE_BODY)


	//BEGIN_COM_MAP(LDataPageBodyImpl)
	//	COM_INTERFACE_ENTRY(fk::IExtensibility)
	//	COM_INTERFACE_ENTRY(IDispatch)
	//END_COM_MAP()

	virtual STDMETHODIMP _InternalQueryInterface(REFIID iid, void ** ppvObject);

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();
	void FinalRelease();

public:

	virtual STDMETHODIMP OnAddInsUpdate();
	virtual STDMETHODIMP OnBeginShutdown();
	virtual STDMETHODIMP OnConnection(fk::IComFramework* pComFramework, fk::enumConnectMode ConnectMode2, fk::IAddIn * pAddIn, LONG* pUserDispatch);
	virtual STDMETHODIMP OnStartupComplete();
	virtual STDMETHODIMP OnDisconnection(fk::enumDisconnectMode disconnectMode, fk::IMainFrame* pMainFrame, fk::IAddIn *pAddIn);
};

OBJECT_ENTRY_AUTO(__uuidof(DataPageBody), LDataPageBodyImpl)

LDataPageBody::LDataPageBody()
{
}

LDataPageBody::~LDataPageBody()
{
}


//
LDataPageBodyImpl::LDataPageBodyImpl()
{
}

LDataPageBodyImpl::~LDataPageBodyImpl()
{
}

STDMETHODIMP LDataPageBodyImpl::_InternalQueryInterface(REFIID iid, void ** ppvObject)
{
	FK_QUERY_IID_ATL(iid, IID_IDataPageBody, IDataPageBody, ppvObject, this);
	FK_QUERY_IID_ATL(iid, fk::IID_IExtensibility, IDataPageBody, ppvObject, this);

	return S_FALSE;
}

HRESULT LDataPageBodyImpl::FinalConstruct()
{
	return S_OK;
}

void LDataPageBodyImpl::FinalRelease()
{
}



STDMETHODIMP LDataPageBodyImpl::OnAddInsUpdate()
{
	return S_OK;
}

STDMETHODIMP LDataPageBodyImpl::OnBeginShutdown()
{
	return S_OK;
}

STDMETHODIMP LDataPageBodyImpl::OnConnection(fk::IComFramework* pComFramework, fk::enumConnectMode ConnectMode2, fk::IAddIn * pAddIn, LONG* pUserDispatch)
{
	try
	{
		switch (ConnectMode2)
		{
		case fk::umConnectModeCommandLine:	//1
			{
			}break;
		case fk::umConnectModeStartup:		//2
			{
			}break;
		case fk::umConnectModeAfterStartup:	//3
			{
			}break;
		case fk::umConnectModeUISetup:		//4
			{
			}break;
		default:
			{
				return E_NOTIMPL;
			}
		}
	}
	catch(...)
	{

	}

	return S_OK;
}

STDMETHODIMP LDataPageBodyImpl::OnStartupComplete()
{
	return S_OK;
}

STDMETHODIMP LDataPageBodyImpl::OnDisconnection(fk::enumDisconnectMode disconnectMode, fk::IMainFrame* pMainFrame, fk::IAddIn *pAddIn)
{
	switch (disconnectMode)
	{
	case fk::umDisconnectModeUninstallUI:
		{
		}break;
	case fk::umDisconnectModeUserClosed:
		{
		}break;
	case fk::umDisconnectModeCloseAddIn:
		{
		}break;
	}

	return S_OK;
}
