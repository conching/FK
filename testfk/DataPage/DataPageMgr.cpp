#include "StdAfx.h"
#include "DataPageMgr.hxx"
#include "resource.h"
#include "fk\xmldef.hxx"




_FK_BEGIN


LDataPageMgr* g_pDataPageMgr = NULL;


class LDataPageMgrImpl : public fk::LDataPageMgr
{
private:
	fk::LAutoPtr		_autoPtr;

	b_call RegMainFrameTargetProc_(fk::enumInstallEvent installEvent);
public:
	LDataPageMgrImpl(void);
	virtual ~LDataPageMgrImpl(void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);
};

LDataPageMgrImpl::LDataPageMgrImpl(void)
{
}

LDataPageMgrImpl::~LDataPageMgrImpl(void)
{
	_autoPtr.clear();

	fk::UninitApplicationInfo();
}

//	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC(LDataPageMgrImpl)
//FK_TARGETPROC_EVENT(_pObject, fk::ON_MSG, OnMFActiveItem(pEvent));
FK_END_TARGETPROC();

ULONG STDMETHODCALLTYPE LDataPageMgrImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}




LDataPageMgr::LDataPageMgr(void):
	fk::LComponent(g_pmodule)
{
}

LDataPageMgr::~LDataPageMgr(void)
{

}


LDataPageMgr* _fncall LDataPageMgr::NewDataPageMgr(void)
{
	if (g_pDataPageMgr==NULL)
	{
		g_pDataPageMgr = new LDataPageMgrImpl();
		g_pDataPageMgr->AddRef();

		return g_pDataPageMgr;
	}
	else
	{
		g_pDataPageMgr->AddRef();
	}

	return g_pDataPageMgr;
}


_FK_END
