#ifndef __DataPageMgr_h__
#define __DataPageMgr_h__


_FK_BEGIN


class LDataPageMgr : public fk::LComponent
{
private:
public:
	LDataPageMgr(void);
	virtual ~LDataPageMgr(void);

	static LDataPageMgr* _fncall NewDataPageMgr(void);
};

extern LDataPageMgr* g_DataPageMgr;

_FK_END


#endif