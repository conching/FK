#include "stdafx.h"
#include "DataPageRegObject.hxx"


_FK_BEGIN


b_fncall CreateDataPageClass(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject);
b_fncall LinkDataPageClass(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow);


b_fncall RegDataPageClass(void)
{
	fk::REGOBJECTITEM rci;

	rci.dwSize				= sizeof(fk::REGOBJECTITEM);
	rci.dwMask				= 0;
	rci.lpfnCreateObject	= (fk::LPFNCREATEOBJECT)fk::CreateDataPageClass;
	rci.lpfnLinkObject		= (fk::LPFNLINK_OBJECT)fk::LinkDataPageClass;
	rci.lpszClass			= NULL;

	if (fk::ObjectReg(&rci))
	{
	}
	else
	{
		fk_ErrorBox();
		return false;
	}

	return true;
}

b_fncall UnregDataPageClass(void)
{
	fk::ObjectUnreg((fk::LPFNCREATEOBJECT)fk::CreateDataPageClass);

	return true;
}


b_fncall CreateDataPageClass(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject)
{
	return true;
}

b_fncall LinkDataPageClass(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow)
{
	return true;
}

_FK_END
