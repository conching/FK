#include "stdafx.h"
#include "DataPageRegWindow.hxx"


_FK_BEGIN

fk::LStdControlProps*  g_pscp = NULL;

//
// 创建标准控件入口。
b_fncall CreateDataPageWindow(fk::LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow** ppWindow);
b_fncall LinkDataPageWindowDataXml(fk::LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow* pwindow);


b_fncall RegCreateDataPageWindow()
{
	fk::REGWINDOWCLASSITEM		rwi;

	rwi.dwSize					= sizeof(fk::REGWINDOWCLASSITEM);
	rwi.dwMask					= 0;
	rwi.lpfnCreateWindowObject	= (fk::LPFNCREATE_WINDOW_OBJECT)fk::CreateDataPageWindow;
	rwi.lpfnLinkWindowObject	= (fk::LPFNLINK_WINDOW_OBJECT)fk::LinkDataPageWindowDataXml;
	rwi.lpszClass				= NULL;

	if (fk::RegWindowObject(&rwi))
	{
		g_pscp = fk::LStdControlProps::NewStdControlProps(g_pmodule);

		return true;
	}

	return false;
}

b_fncall UnregCreateDataPageWindow()
{
	fk::LStdControlProps*  pscp;

	if (fk::UnregWindowObject((fk::LPFNCREATE_WINDOW_OBJECT)fk::CreateDataPageWindow))
	{
		pscp = g_pscp;
		pscp->Release();

		g_pscp = NULL;

		return true;
	}

	return false;
}



//
// 创建标准控件入口。
// 这个比较功能，以后可以更改为更快的比较方法。
//
//	if (stricmp(lpcwo->lpszClassName, "fk::LSplitterWnd") == 0)
//	{
//		umReturn = fk::xulSplitterWnd(lpcwo, ppWindow);
//	}
//
b_fncall CreateDataPageWindow(fk::LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow** ppWindow)
{
/*
	如果需要创建控件，使用这段代码， 关闭函数最后的 return true;

	UINT			uiCWOFType;
	LPRECT			lprcWnd;
	fk::enumReturn	umReturn;

	fk_pv3(lpcwo == NULL, lpcwo->pWindowMuchParent == NULL, lpcwo->lpszClassName == NULL, return false;);

	if (lpcwo->lpszClassName != NULL)
	{
		uiCWOFType	= FK_DWORD_4(lpcwo->dwMask);
		lprcWnd		= &lpcwo->rcCtrl;

		//if (stricmp(lpcwo->lpszClassName, "fk::LSplitterWnd") == 0)
		//{
		//	umReturn = fk::xulSplitterWnd(lpcwo, ppWindow);
		//}
		//else if (stricmp(lpcwo->lpszClassName, "fk::LCheckBox") == 0)
		//{
		//	umReturn = xulCreateCheckBox(lpcwo, ppWindow);
		//}
		else
		{
			return false;
		}
	}

	if (umReturn == fk::umReturnSuccess || umReturn == fk::umReturnWarning)
		return true;

	return false;
*/

	return true;
}

b_fncall LinkDataPageWindowDataXml(fk::LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow* pwindow)
{
	return true;
}


_FK_END



/*
这是一个创建控件的代码实例，拷贝，修改。

1. 修改函数名称。
2. 修改 fknew() 中的代码。
3. 修改pSplitterWnd->Craete 代码。

4. 其它修改可以跟类的使用情况。

*/

/*
enumReturn _fncall xulSplitterWnd(LPCREATE_WINDOW_OBJECT lpcwo, fk::LWindow** ppWindow)
{
	fk_pv2(lpcwo == NULL, ppWindow == NULL, return fk::umReturnError;);

	fk::LSplitterWnd*	pSplitterWnd;
	LPRECT				lprcWnd;
	wchar_t*			lpszCaption;
	fk::enumReturn	umReturn = umReturnError;
	fk::LCtrlPropsError	ctrlPropsError;
	UINT				uiCWOFType;

	lprcWnd		= &lpcwo->rcCtrl;
	uiCWOFType	= FK_DWORD_4(lpcwo->dwMask);

	if (uiCWOFType == fk::CWOF_DATA_XML)
	{
		g_pscp->clear();
		umReturn = g_pscp->ReadData(lpcwo->pVxmlFile, lpcwo->hXmlNode, &ctrlPropsError);		// 读控件的标准属性值。
		if (umReturn == fk::umReturnSuccess || umReturn == fk::umReturnWarning)
		{
			fknew(pSplitterWnd = new fk::LSplitterWnd(lpcwo->pWindowMuchParent), pSplitterWnd);
			if (lpcwo->dwMask & fk::CWOF_DATA_VCRES_ID)
			{
				pSplitterWnd->Attach(::GetDlgItem(lpcwo->pWindowMuchParent->_hWnd, g_pscp->uiCtrlID));
			}
			else
			{
				// 创建窗口控件的窗口。
				lprcWnd = &g_pscp->rcObject;
				if (pSplitterWnd->CreateSplitterWnd(lpcwo->pWindowMuchParent, lpcwo->pWindowMuchParent->_hWnd, lprcWnd->left, lprcWnd->top, lprcWnd->right, lprcWnd->bottom))
				{

				}

				pSplitterWnd->SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
			}

			if (ppWindow != NULL)
			{
				*ppWindow = pSplitterWnd;
			}

			return fk::umReturnSuccess;
		}
		else
		{
			ctrlPropsError.ShowErrorBox(::GetActiveWindow(), MB_OK | MB_ICONERROR);
		}

		return umReturnError;
	}
	else if (uiCWOFType == fk::CWOF_DATA_DEFAULT)
	{
		fknew(pSplitterWnd = new fk::LSplitterWnd(lpcwo->pWindowMuchParent), pSplitterWnd);

		lpszCaption = fk::FormVarNameGetCaption(lpcwo->lpszVarName);
		if (pSplitterWnd->CreateSplitterWnd(lpcwo->pWindowMuchParent, lpcwo->pWindowMuchParent->_hWnd, lprcWnd->left, lprcWnd->top, lprcWnd->right, lprcWnd->bottom) )
		{
			if (ppWindow != NULL)
			{
				*ppWindow = (fk::LWindow*)pSplitterWnd;
			}

			return fk::umReturnSuccess;
		}
		else
		{
			fkdelete(pSplitterWnd);
			return fk::umReturnError;
		}
	}

	return fk::umReturnError;
}
*/