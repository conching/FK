

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:59 2022
 */
/* Compiler settings for .\DataPage.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IDataPageBody,0x4EEADF64,0x96B2,0x44BF,0x86,0xC4,0x69,0xE7,0x6F,0x2C,0xE2,0xF9);


MIDL_DEFINE_GUID(IID, IID_ITableBrowse,0xFBB9B2ED,0x86FA,0x42D9,0x91,0xCE,0x10,0x55,0xE4,0x58,0x6F,0x2D);


MIDL_DEFINE_GUID(IID, LIBID_DataPageLib,0xFED5EC96,0xEFF4,0x4BDA,0x8D,0xB3,0x19,0xFF,0xE0,0x7F,0xC5,0x9F);


MIDL_DEFINE_GUID(CLSID, CLSID_DataPageBody,0xACF86BAD,0x4A3D,0x4AF5,0xB2,0x95,0x80,0xF8,0x80,0x94,0xEB,0x5E);


MIDL_DEFINE_GUID(CLSID, CLSID_TableBrowse,0xA5BBABF2,0xCE59,0x4D7D,0x84,0xFB,0x25,0x25,0x85,0x8B,0xF0,0xDE);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



