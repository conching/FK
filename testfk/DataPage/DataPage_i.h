

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0366 */
/* at Fri Sep 02 16:15:59 2022
 */
/* Compiler settings for .\DataPage.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __DataPage_i_h__
#define __DataPage_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IDataPageBody_FWD_DEFINED__
#define __IDataPageBody_FWD_DEFINED__
typedef interface IDataPageBody IDataPageBody;
#endif 	/* __IDataPageBody_FWD_DEFINED__ */


#ifndef __ITableBrowse_FWD_DEFINED__
#define __ITableBrowse_FWD_DEFINED__
typedef interface ITableBrowse ITableBrowse;
#endif 	/* __ITableBrowse_FWD_DEFINED__ */


#ifndef __DataPageBody_FWD_DEFINED__
#define __DataPageBody_FWD_DEFINED__

#ifdef __cplusplus
typedef class DataPageBody DataPageBody;
#else
typedef struct DataPageBody DataPageBody;
#endif /* __cplusplus */

#endif 	/* __DataPageBody_FWD_DEFINED__ */


#ifndef __TableBrowse_FWD_DEFINED__
#define __TableBrowse_FWD_DEFINED__

#ifdef __cplusplus
typedef class TableBrowse TableBrowse;
#else
typedef struct TableBrowse TableBrowse;
#endif /* __cplusplus */

#endif 	/* __TableBrowse_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IDataPageBody_INTERFACE_DEFINED__
#define __IDataPageBody_INTERFACE_DEFINED__

/* interface IDataPageBody */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IDataPageBody;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4EEADF64-96B2-44BF-86C4-69E76F2CE2F9")
    IDataPageBody : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IDataPageBodyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IDataPageBody * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IDataPageBody * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IDataPageBody * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IDataPageBody * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IDataPageBody * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IDataPageBody * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IDataPageBody * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IDataPageBodyVtbl;

    interface IDataPageBody
    {
        CONST_VTBL struct IDataPageBodyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IDataPageBody_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IDataPageBody_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IDataPageBody_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IDataPageBody_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IDataPageBody_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IDataPageBody_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IDataPageBody_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IDataPageBody_INTERFACE_DEFINED__ */


#ifndef __ITableBrowse_INTERFACE_DEFINED__
#define __ITableBrowse_INTERFACE_DEFINED__

/* interface ITableBrowse */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITableBrowse;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FBB9B2ED-86FA-42D9-91CE-1055E4586F2D")
    ITableBrowse : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct ITableBrowseVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITableBrowse * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITableBrowse * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITableBrowse * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITableBrowse * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITableBrowse * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITableBrowse * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITableBrowse * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } ITableBrowseVtbl;

    interface ITableBrowse
    {
        CONST_VTBL struct ITableBrowseVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITableBrowse_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define ITableBrowse_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define ITableBrowse_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define ITableBrowse_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define ITableBrowse_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define ITableBrowse_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define ITableBrowse_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ITableBrowse_INTERFACE_DEFINED__ */



#ifndef __DataPageLib_LIBRARY_DEFINED__
#define __DataPageLib_LIBRARY_DEFINED__

/* library DataPageLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_DataPageLib;

EXTERN_C const CLSID CLSID_DataPageBody;

#ifdef __cplusplus

class DECLSPEC_UUID("ACF86BAD-4A3D-4AF5-B295-80F88094EB5E")
DataPageBody;
#endif

EXTERN_C const CLSID CLSID_TableBrowse;

#ifdef __cplusplus

class DECLSPEC_UUID("A5BBABF2-CE59-4D7D-84FB-2525858BF0DE")
TableBrowse;
#endif
#endif /* __DataPageLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


