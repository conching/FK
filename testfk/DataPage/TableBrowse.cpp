#include "stdafx.h"
#include "TableBrowse.hxx"



_FK_BEGIN

//---------------------------------------------------------------------------------
class LTableBrowseImpl : public LTableBrowse,
									public fk::LWindowMuch
{
private:
	FK_BEGIN_CODE(VARIABLE, TABLEBROWSE_VXML)
	fk::LSysListView32* _pSysListView321;
	FK_END_CODE(VARIABLE)

		FK_BEGIN_CODE(EVENTDEFAULT, TABLEBROWSE_VXML)
		v_call OnCreate(void);
	v_call OnSize(WPARAM wParam, LPARAM lParam);
	FK_END_CODE()

public:
	virtual LRESULT SubWndProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	static wchar_t*	TABLEBROWSE_VXML;
	LTableBrowseImpl();
	virtual ~LTableBrowseImpl();

	HRESULT FinalConstruct();
	void FinalRelease();

	FK_COM_ARQ();

	virtual STDMETHODIMP GetWnd(LONG_PTR* hWnd);
	virtual STDMETHODIMP CreateWnd(fk::IGenericPanel* pParentPanel,
									int iLeft, int iTop, int iWidth, int iHeight, LONG_PTR hWndParent);
	virtual STDMETHODIMP CreateWndXml( 
		IGenericPanel *pParentPanel,
		int iLeft,
		int iTop,
		int iWidth,
		int iHeight,
		LONG_PTR hWndParent,
		IUnknown *pXmlFile,
		LONG *hXmlNode);

	virtual STDMETHODIMP OnMdiActiveWindowPanel(fk::IMdiContainer* pMdiContainer,
		fk::IGenericPanel* pParentPanel, fk::ITabPageItem* pTabPageItem, UINT nType,
		VARIANT_BOOL bActive, VARIANT_BOOL* bOutActive, long* lpvObj);
	virtual STDMETHODIMP  OnGenericPanelActiveWindowPanel(fk::IGenericPanel* pParentPanel,
		fk::IGenericPanelItem* gpi, UINT nType, VARIANT_BOOL* bActive, long* lpvObj);
	virtual STDMETHODIMP QueryCloseWindowPanel(fk::IGenericPanel* pParentPanel, fk::enumCloseAction* pCloseAction, long* lpvObj);
	virtual STDMETHODIMP CloseWindowPanel(fk::IGenericPanel* pParentPanel, long* lpvObj);
};
wchar_t* LTableBrowseImpl::TABLEBROWSE_VXML = L"TableBrowse.vxml";


//---------------------------------------------------------------------------------
// class LTableBrowse
LTableBrowse::LTableBrowse()
{
}

LTableBrowse::~LTableBrowse()
{
}

bool LTableBrowse::NewTableBrowse(fk::LObject* powner, LPVOID* ppv, REFIID riid)
{
	LTableBrowse* pobj;

	pobj = new LTableBrowseImpl();
	if (pobj!=NULL)
	{
		pobj->AddRef();
		*ppv = (fk::IWindowPanel*)pobj;
		return true;
	}

	return false;
}


//---------------------------------------------------------------------------------
// class LTableBrowseImpl
LTableBrowseImpl::LTableBrowseImpl():
	fk::LWindowMuch(g_pmodule, (fk::LWindow*)NULL, LTableBrowseImpl::TABLEBROWSE_VXML)
{
}


LTableBrowseImpl::~LTableBrowseImpl()
{
}


FK_COM_A_CODE(LTableBrowseImpl)


HRESULT LTableBrowseImpl::FinalConstruct()
{
	return S_OK;
}


void LTableBrowseImpl::FinalRelease()
{
}

STDMETHODIMP LTableBrowseImpl::QueryInterface(REFIID riid, void **ppv)
{
	if (ppv!=NULL)
	{
		FK_QUERY_IID(riid, fk::IID_IWindowPanel, fk::IWindowPanel, ppv, this);
	}
	else
	{
		ATLASSERT(0);
		return E_NOINTERFACE;
	}

	return S_FALSE;
}

STDMETHODIMP LTableBrowseImpl::GetWnd(LONG_PTR* hWnd)
{
	*hWnd = (LONG)_hWnd;

	return S_OK;
}


STDMETHODIMP  LTableBrowseImpl::CreateWnd(fk::IGenericPanel* pParentPanel,
										  int iLeft,
										  int iTop,
										  int iWidth,
										  int iHeight,
										  LONG_PTR hWndParent
										  )
{
	RECT	rc;

	rc.left		= iLeft;
	rc.top		= iTop;
	rc.right	= iWidth;
	rc.bottom	= iHeight;

	//Create((HWND)hWndParent);
	CreateFkWindow(0, L"", WS_VISIBLE|WS_CHILDWINDOW|WS_CLIPCHILDREN,
										iLeft, iTop, iWidth, iHeight, (HWND)hWndParent, 0, NULL);
	if (IsWindow())
	{
		ShowWindow(SW_SHOW);
	}
	else
	{
		fk_TraceFormatMessage(FTEXTNIL, g_pmodule->hRes, STRNIL, ::GetLastError());
		fk_ErrorBox();
	}

	return S_OK;
}

STDMETHODIMP LTableBrowseImpl::CreateWndXml(IGenericPanel *pParentPanel,
											int iLeft,
											int iTop,
											int iWidth,
											int iHeight,
											LONG_PTR hWndParent,
											IUnknown *pXmlFile,
											LONG *hXmlNode)
{
	return S_FALSE;
}

STDMETHODIMP  LTableBrowseImpl::OnMdiActiveWindowPanel(fk::IMdiContainer* pMdiContainer,
													   fk::IGenericPanel* pParentPanel,
													   fk::ITabPageItem* pTabPageItem,
													   UINT nType,
													   VARIANT_BOOL bActive,
													   VARIANT_BOOL* bOutActive,
													   long* lpvObj
													   )
{
	fk::LStringw	sTitle;

	//sTitle.LoadStr(g_pmodule->hRes, IDS_TITLETOOLBOXVIEW);
	sTitle = L"TableBrowse";

	pTabPageItem->put_Caption(sTitle._pwstr);

	return S_OK;
}

STDMETHODIMP LTableBrowseImpl::OnGenericPanelActiveWindowPanel(fk::IGenericPanel* pParentPanel,
															   fk::IGenericPanelItem* pPanelItem,
															   UINT nType,
															   VARIANT_BOOL* bActive,
															   long* lpvObj
															   )
{

	return S_OK;
}


STDMETHODIMP LTableBrowseImpl::QueryCloseWindowPanel(fk::IGenericPanel* pParentPanel,
													 fk::enumCloseAction* pCloseAction,
													 long* lpvObj
													 )
{

	return S_OK;
}


/*
** 删除自身的信息。
*/
STDMETHODIMP LTableBrowseImpl::CloseWindowPanel(fk::IGenericPanel* pParentPanel,
												long* lpvObj
												)
{

	return S_OK;
}

//---------------------------------------------------------------------------------
//INT_PTR _call LTableBrowseImpl::DialogProc(UINT u2Msg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_SUBWNDPROC(LTableBrowseImpl)
FK_MESSAGE_HANDLER(WM_CREATE, OnCreate(), return 0);
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_END_SUBWNDPROC(fk::LWindowMuch)		 //***** 是基类。

// vv_call LTableBrowseDlgOnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC(LTableBrowseImpl)
//FK_COMMAND_EVENT(_pOKBtn, BN_CLICKED, OnOKBtnClick());
//FK_COMMAND_EVENT(_pCancelBtn, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call LTableBrowseImpl::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC(LTableBrowseImpl)
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
vb_call LTableBrowseImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
	{(fk::LObject**)&_pSysListView321, L"_pSysListView321"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE LTableBrowseImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call LTableBrowseImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call LTableBrowseImpl::OnCreate()
{
	LinkDataExchange(fk::ldeInstallData);
}

void LTableBrowseImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


_FK_END
