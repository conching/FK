#ifndef _TableBrowse_h_
#define _TableBrowse_h_


_FK_BEGIN

class  LTableBrowse : public fk::IWindowPanel
{
public:
	LTableBrowse();
	virtual ~LTableBrowse();

	static b_fncall NewTableBrowse(fk::LObject* powner, LPVOID* ppobj, REFIID riid=GUID_NULL);
};


/*
手动添加这些的三行代码段，稍后提供代码同步功能，或者其它方式。

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	添加这些到 DllGetClassObject 函数。
	_FK_CLASS_FACTORY(rclsid, CLSID_TableBrowse, ppv, fk::LTableBrowse::NewTableBrowse_IID);
}

STDAPI DllRegisterServer(void)
{
	UpdateRegistryEx(IDR_TableBrowse, TRUE);			添加这些到 DllRegisterServer 函数。
}

STDAPI DllUnregisterServer(void)
{
	UpdateRegistryEx(IDR_TableBrowse, FALSE);			添加这些到 DllUnregisterServer 函数。
}
*/
_FK_END

#endif
