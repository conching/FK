#ifndef __OutTestMainFrame_hxx__
#define __OutTestMainFrame_hxx__

_FK_BEGIN

/*
enum enumMainFrameType
{
umMainFrameTypeNull	= 0,
umMainFrameTypeSDI	= 1,
umMainFrameTypeMDI	= 2,
umMainFrameTypeSDISplitter	= 3,
umMainFrameTypeMDISplitter	= 4
}
enumMainFrameType;
*/

b_fncall CreateTestMainFrame(void);

_FK_END

#endif
