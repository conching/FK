#ifndef _SDIPanel_h_
#define _SDIPanel_h_


_FK_BEGIN

class  LSDIPanel : public fk::IWindowPanel
{
public:
	LSDIPanel();
	virtual ~LSDIPanel();

	static b_fncall NewSDIPanel(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid=GUID_NULL);
};


/*
4C5843BB-ABDE-4CD3-997B-149594C71445

手动添加这些的三行代码段，稍后提供代码同步功能，或者其它方式。

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	添加这些到 DllGetClassObject 函数。
	_FK_CLASS_FACTORY(rclsid, CLSID_SDIPanel, ppv, fk::LSDIPanel::NewSDIPanel_IID);
}

STDAPI DllRegisterServer(void)
{
	UpdateRegistryEx(IDR_I, TRUE);			//添加这些到 DllRegisterServer 函数。
}

STDAPI DllUnregisterServer(void)
{
	UpdateRegistryEx(IDR_I, FALSE);			//添加这些到 DllUnregisterServer 函数。
}
*/

_FK_END

#endif
