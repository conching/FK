#include "stdafx.h"
#include "resource.h"
#include "TestMainFrame_i.hxx"
#include "TestMainFrameMgr.hxx"
#include "TestMainFrameRegWindow.hxx"
#include "TestMainFrameRegObject.hxx"
#include "SDIPanel.hxx"


class CTestMainFrame : public CAtlDllModuleT< CTestMainFrame >
{
public :
	DECLARE_LIBID(LIBID_TestMainFrameLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_TESTMAINFRAME, "{34D9665D-518E-4C78-B666-0636280C4E7C}")
};

CTestMainFrame _AtlModule;



b_fncall TestMainFrameLoadLanguageProc(fk::umLoadLanguage loadLanguage);
v_fncall RegsvrTestMainFrameProcess(fk::LStringws* spFiles);
HRESULT _fncall UpdateRegistryEx(int iRes, BOOL bRegister) throw();

// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
	return _AtlModule.DllCanUnloadNow();
}

// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
	HRESULT	hr;

	hr = _AtlModule.DllGetClassObject(rclsid, riid, ppv);

	if (hr!=S_OK)
	{
		_FK_CLASS_FACTORY(rclsid, CLSID_SDIPanel, ppv, fk::LSDIPanel::NewSDIPanel);

		return S_OK;
	}

	return hr;
}

// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
	// registers object, typelib and all interfaces in typelib
	HRESULT hr = _AtlModule.DllRegisterServer();

	UpdateRegistryEx(IDR_SDIPANEL, TRUE);

	return hr;
}

// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
	HRESULT hr = _AtlModule.DllUnregisterServer();

	UpdateRegistryEx(IDR_SDIPANEL, FALSE);

	return hr;
}

HRESULT _fncall UpdateRegistryEx(int iRes, BOOL bRegister) throw()
{
	__if_exists(_GetMiscStatus) 
	{ 
		ATL::_ATL_REGMAP_ENTRY regMapEntries[2]; 
		memset(&regMapEntries[1], 0, sizeof(ATL::_ATL_REGMAP_ENTRY)); 
		regMapEntries[0].szKey = L"OLEMISC"; 
		TCHAR szOleMisc[32]; 
		ATL::Checked::itot_s(_GetMiscStatus(), szOleMisc, _countof(szOleMisc), 10); 
		USES_CONVERSION_EX; 
		regMapEntries[0].szData = T2OLE_EX(szOleMisc, _ATL_SAFE_ALLOCA_DEF_THRESHOLD); 
		if (regMapEntries[0].szData == NULL) 
			return E_OUTOFMEMORY; 
		__if_exists(_Module) 
		{ 
			return _Module.UpdateRegistryFromResource(iRes, bRegister, regMapEntries); 
		} 
		__if_not_exists(_Module) 
		{ 
			return ATL::_pAtlModule->UpdateRegistryFromResource(iRes, bRegister, regMapEntries); 
		} 
	} 
	__if_not_exists(_GetMiscStatus) 
	{ 
		__if_exists(_Module) 
		{ 
			return _Module.UpdateRegistryFromResource(iRes, bRegister); 
		} 
		__if_not_exists(_Module) 
		{ 
			return ATL::_pAtlModule->UpdateRegistryFromResource(iRes, bRegister); 
		} 
	} 
}

v_fncall RegsvrTestMainFrameProcess(fk::LStringws* spFiles)
{
	spFiles->NewTextw()->Assign(L"TestMainFrame.dll");
}

b_fncall TestMainFrameLoadLanguageProc(fk::umLoadLanguage loadLanguage)
{
	switch(loadLanguage)
	{
	case fk::umLoadLanguageInstall:
		{
			// 这段代码使用 .dll 文件资源。
			//
			//if (fk::LanguageLoadProjectInline(g_pmodule,  _AtlBaseModule.GetModuleInstance() ) )
			//	return true;

			// 这段代码使用自身 .exe 文件资源。
			if (fk::LanguageLoadSelf(g_pmodule, _AtlBaseModule.GetModuleInstance()) )
				return true;

			return false;
		}
	case fk::umLoadLanguageUninstall:
		{

		}
	}

	return false;
}

v_fncall GetDllDnaXmlFile(fk::LStringws* spDnaXmlFiles)
{
	//spDnaXmlFiles->NewTextw()->Assign(L"dna.xml");
}



#ifdef _MANAGED
#pragma managed(push, off)
#endif

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	if (DLL_PROCESS_ATTACH==dwReason)
	{
		if (!fk::IsRegsvr32Exe(hInstance))
		{
			fk::InitializeModule(&g_pmodule, hInstance);
			g_pmodule->fnGetDllDnaXmlFile = GetDllDnaXmlFile;

			fk::RegCreateTestMainFrameWindow();
			fk::RegTestMainFrameClass();
			fk::LanguageRegsvrLoad( (fk::LOADLANGUAGEPROC)TestMainFrameLoadLanguageProc);
			fk::RepairAddProcess(RegsvrTestMainFrameProcess);
		}
	}
	else if(DLL_PROCESS_DETACH == dwReason)
	{
		if (!fk::IsRegsvr32Exe(hInstance))
		{
			fk::LanguageUnregsvrLoad( (fk::LOADLANGUAGEPROC)TestMainFrameLoadLanguageProc);
			fk::UnregCreateTestMainFrameWindow();
			fk::UnregTestMainFrameClass();
			fk::UninitializeModule(&g_pmodule);
		}
	}

	return _AtlModule.DllMain(dwReason, lpReserved); 
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

