#include "StdAfx.h"
#include "TestMainFrameMgr.hxx"
#include "resource.h"
#include "fk\xmldef.hxx"
#include "SDIPanel.hxx"


_FK_BEGIN

static wchar_t* S_TEST_XML_SDI_MAINFRAME_NAME = L"TestXmlSdiMainFrame";
static wchar_t* S_TEST_SDI_MAINFRAME_NAME = L"TestSdiMainFrame";


LTestMainFrameMgr* g_pTestMainFrameMgr = NULL;

class LTestMainFrameMgrImpl : public fk::LTestMainFrameMgr
{
	_FK_RTTI_;

private:
	fk::LAutoPtr		_autoPtr;
	fk::IComFramework*	_pcomFramework;

	fk::IMainFrame*	_pxmlSdiMainFrame;
	fk::IMainFrame*	_psdiMainFrame;

	fk::IMainFrame*	_pxmlSdiMainFrameSplitter;
	fk::IMainFrame*	_pSdiMainFrameSplitter;

	b_call RegMainFrameTargetProc_(fk::enumInstallEvent installEvent);

	b_call CreateXmlSdiMainFrame(void);				// 使用 Xml 方式创建的 sdi MainFrame
	b_call CreateSdiMainFrame(void);				// 使用代码方式创建的 sdi MainFrame

	b_call CreateXmlSdiMainFrameSplitter(void);		// 使用 xml 方式创建 sdi MainFrameSplitter。
	b_call CreateSdiMainFrameSplitter(void);		// 使用代码方式创建 sdi MainFrameSplitter。

public:
	LTestMainFrameMgrImpl(fk::LObject* pParentObject);
	virtual ~LTestMainFrameMgrImpl(void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);

	virtual b_call CreateTestMainFrame(void);
};


///////////////////////////////////////////////////////////////////////////////////////////////////
LTestMainFrameMgr::LTestMainFrameMgr(fk::LObject* pParentObject):
fk::LComponent(pParentObject)
{
	_FK_RTTI_INIT_(fk::LTestMainFrameMgr);
}

LTestMainFrameMgr::~LTestMainFrameMgr(void)
{

}

b_fncall NewTestMainFrameMgr(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid=GUID_NULL)
{
	LTestMainFrameMgrImpl*  pobject= NULL;

	if (g_pTestMainFrameMgr==NULL)
	{
		pobject = new LTestMainFrameMgrImpl(pParentObject);
		if (pobject!=NULL)
		{
			g_pTestMainFrameMgr = pobject;

			pobject->AddRef();
			*ppobj = pobject;
			return true;
		}

		return false;
	}

	*ppobj = g_pTestMainFrameMgr;
	return false;
}

b_fncall LTestMainFrameMgr::GetTestMainFrameManager(fk::LTestMainFrameMgr** ppTestMainFrameMgr)
{
	if (g_pTestMainFrameMgr==NULL)
	{
		if (fk::NewTestMainFrameMgr(g_pmodule, (LPVOID*)ppTestMainFrameMgr))
		{
			g_pTestMainFrameMgr->AddRef();
			return true;
		}

		return false;
	}

	if (g_pTestMainFrameMgr!=NULL)
	{
		*ppTestMainFrameMgr = g_pTestMainFrameMgr;
		g_pTestMainFrameMgr->AddRef();
		return true;
	}

	return false;
}

_FK_RTTI_CODE2_(fk::LTestMainFrameMgrImpl, fk::LTestMainFrameMgr, fk::LComponent, LTestMainFrameMgrImpl, LTestMainFrameMgr);

///////////////////////////////////////////////////////////////////////////////////////////////////
LTestMainFrameMgrImpl::LTestMainFrameMgrImpl(fk::LObject* pParentObject):
fk::LTestMainFrameMgr(pParentObject)
{
	_FK_RTTI_INIT_(fk::LTestMainFrameMgrImpl);

	fk_AutoPtr5(_autoPtr, _pxmlSdiMainFrame, _psdiMainFrame, _pxmlSdiMainFrameSplitter, _pxmlSdiMainFrame, _pcomFramework);

	if (!fk::GetComFramework((void**)&_pcomFramework))
		fk_ErrorBox();
}

LTestMainFrameMgrImpl::~LTestMainFrameMgrImpl(void)
{
	_autoPtr.clear();

	fk::UninitApplicationInfo();
}

//	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC(LTestMainFrameMgrImpl)
//FK_TARGETPROC_EVENT(_pObject, fk::ON_MSG, OnMFActiveItem(pEvent));
FK_END_TARGETPROC();

ULONG STDMETHODCALLTYPE LTestMainFrameMgrImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

// 使用 Xml 方式创建的 sdi MainFrame
b_call LTestMainFrameMgrImpl::CreateXmlSdiMainFrame(void)
{
	HRESULT hr;
	fk::LGuid guid;

	if (_pcomFramework->CreateMainFrameObject(&_pxmlSdiMainFrame)==S_OK)
	{
		hr = _pxmlSdiMainFrame->CreateMainFrameXml((long*)g_pmodule, L"SdiMainFrame.vxml");
		if (hr==S_OK)
		{
			_pxmlSdiMainFrame->ShowWindow(SW_SHOW);
			return true;
		}
	}

	fk_ErrorBox();
	return false;
}

//
// 使用代码方式创建的 sdi MainFrame
//
b_call LTestMainFrameMgrImpl::CreateSdiMainFrame(void)
{
	HRESULT hr;
	MAINFRAMEINFO mfi;
	fk::LAutoPtr ap;
	fk::LSDIPanel* psdiPanel;

	fk_AutoPtr(ap, psdiPanel);

	mfi.dwSize	= sizeof(MAINFRAMEINFO);
	mfi.bsName	= ap.SysAllocString(S_TEST_SDI_MAINFRAME_NAME);
	mfi.bsTitle	= ap.SysAllocString(S_TEST_SDI_MAINFRAME_NAME);
	mfi.hIcon	= (long*)::LoadIcon(g_pmodule->hInst, MAKEINTRESOURCE(0));
	mfi.hMenu	= NULL;
	//mfi.iidKey	= NULL;
	memset(&mfi.iidKey, 0, sizeof(GUID));
	mfi.mftMainFrameType = fk::umMainFrameTypeSDI;

	hr = _pcomFramework->CreateMainFrameObject(&_psdiMainFrame);
	if (hr==S_OK)
	{
		hr = _psdiMainFrame->CreateMainFrame(&mfi);
		if (hr==S_OK)
		{
			fk::LSDIPanel::NewSDIPanel(this, (LPVOID*)&psdiPanel);
			if (psdiPanel!=NULL)
			{
				_psdiMainFrame->SetSDIWindowPanel(L"umMainFrameTypeSDI", L"umMainFrameTypeSDI", psdiPanel, NULL, NULL);
				_psdiMainFrame->ShowWindow(SW_SHOW);
				return true;
			}
		}
	}

	fk_ErrorBox();
	return false;
}

// 使用 xml 方式创建 sdi MainFrameSplitter。
b_call LTestMainFrameMgrImpl::CreateXmlSdiMainFrameSplitter(void)
{
	HRESULT hr;
	fk::LGuid guid;

	if (_pcomFramework->CreateMainFrameObject(&_pxmlSdiMainFrameSplitter)==S_OK)
	{
		hr = _pxmlSdiMainFrameSplitter->CreateMainFrameXml((long*)g_pmodule, L"SdiMainFrameSplitter.vxml");
		if (hr==S_OK)
		{
			_pxmlSdiMainFrameSplitter->ShowWindow(SW_SHOW);
			return true;
		}
	}

	fk_ErrorBox();
	return false;
}

// 使用代码方式创建 sdi MainFrameSplitter。
b_call LTestMainFrameMgrImpl::CreateSdiMainFrameSplitter(void)
{
	HRESULT hr;
	MAINFRAMEINFO mfi;
	fk::LAutoPtr ap;
	fk::LSDIPanel* psdiPanel;

	fk_AutoPtr(ap, psdiPanel);

	FK_TEST_RUN(FTEXTNIL, L"开始测试，单窗口分格框架。\n");

	mfi.dwSize	= sizeof(MAINFRAMEINFO);
	mfi.bsName	= ap.SysAllocString(S_TEST_SDI_MAINFRAME_NAME);
	mfi.bsTitle	= ap.SysAllocString(S_TEST_SDI_MAINFRAME_NAME);
	mfi.hIcon	= (long*)::LoadIcon(g_pmodule->hInst, MAKEINTRESOURCE(0));
	mfi.hMenu	= NULL;
	//mfi.iidKey	= NULL;
	memset(&mfi.iidKey, 0, sizeof(GUID));
	mfi.mftMainFrameType = fk::umMainFrameTypeSDISplitter;

	hr = _psdiMainFrame->CreateMainFrame(&mfi);
	if (hr==S_OK)
	{
		fk::LSDIPanel::NewSDIPanel(this, (LPVOID*)&psdiPanel);
		if (psdiPanel!=NULL)
		{
			_psdiMainFrame->SetSDIWindowPanel(NULL, NULL, psdiPanel, NULL, NULL);
			FK_TEST_RUN_SUCCESS();
			return true;
		}
	}

	return false;
}

vb_call LTestMainFrameMgrImpl::CreateTestMainFrame(void)
{
	bool bIsError = false;
	FK_TEST_RUN(FTEXTNIL, L"开始测试应该框架。\n");

	if (!CreateXmlSdiMainFrame())
	{
		fk_ErrorBox();
		bIsError = true;
	}

	if (!CreateSdiMainFrame())
	{
		fk_ErrorBox();
		bIsError = true;
	}

	if (!CreateXmlSdiMainFrameSplitter())
	{
		fk_ErrorBox();
		bIsError = true;
	}

//	if (bIsError)
//	{
//		FK_TEST_RUN_SUCCESS();
//		return true;
//	}
//
	return false;
}

_FK_END
