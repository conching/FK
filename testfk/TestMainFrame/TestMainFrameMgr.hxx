#ifndef __TestMainFrameMgr_h__
#define __TestMainFrameMgr_h__


_FK_BEGIN

class _FK_OUT_CLASS LTestMainFrameMgr : public fk::LComponent
{
	_FK_RTTI_;

private:
public:
	LTestMainFrameMgr(fk::LObject* pParentObject);
	virtual ~LTestMainFrameMgr(void);

	virtual b_call CreateTestMainFrame(void) = 0;

	//static b_fncall NewTestMainFrameMgr(fk::LObject* pParentObject, LPVOID* ppobj, REFIID riid);
	static b_fncall GetTestMainFrameManager(fk::LTestMainFrameMgr** ppTestMainFrameMgr);
};

extern LTestMainFrameMgr* g_TestMainFrameMgr;

_FK_END

#endif
