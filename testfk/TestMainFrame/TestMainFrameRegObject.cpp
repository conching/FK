#include "stdafx.h"
#include "TestMainFrameRegObject.hxx"


_FK_BEGIN


b_fncall CreateTestMainFrameClass(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject);
b_fncall LinkTestMainFrameClass(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow);


b_fncall RegTestMainFrameClass(void)
{
	fk::REGOBJECTITEM rci;

	rci.dwSize				= sizeof(fk::REGOBJECTITEM);
	rci.dwMask				= 0;
	rci.lpfnCreateObject	= (fk::LPFNCREATEOBJECT)fk::CreateTestMainFrameClass;
	rci.lpfnLinkObject		= (fk::LPFNLINK_OBJECT)fk::LinkTestMainFrameClass;
	rci.lpszClass			= NULL;

	if (fk::ObjectReg(&rci))
	{
	}
	else
	{
		fk_ErrorBox();
		return false;
	}

	return true;
}

b_fncall UnregTestMainFrameClass(void)
{
	fk::ObjectUnreg((fk::LPFNCREATEOBJECT)fk::CreateTestMainFrameClass);

	return true;
}


b_fncall CreateTestMainFrameClass(fk::LPCREATEOBJECTINFO lpcoi, fk::LObject** ppNewObject)
{
	return true;
}

b_fncall LinkTestMainFrameClass(fk::LPCREATEOBJECTINFO lpcwo, fk::LObject* pwindow)
{
	return true;
}

_FK_END
