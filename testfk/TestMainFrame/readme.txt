
编译项目的问题
-------------

手动修改方法

修改 TestMainFrame_i.c 文件属性。
1. 选择 TestMainFrame_i.c，单击鼠标右建显示菜单，选择属性，弹出 TestMainFrame_i.c属性 对话框。
2. 选择全部 Configuration,  选择全部 Platform.
3. 找到 C/C++ \ Precompiled Headers 属性页。
4. 修改 Create/User Precompiled Headers 属性为：Not Using Precompiled Headers.



修改 stdafx.cpp 文件属性。
1. 选择 stdafx.cpp ，单击鼠标右建显示菜单，选择属性，弹出 stdafx.cpp 属性 对话框。
2. 选择全部 Configuration,  选择全部 Platform.
3. 找到 C/C++ \ Precompiled Headers 属性页。
4. 修改 Create/User Precompiled Headers 属性为：Create Precompiled Header (/Yc).



此版本需要的动态链接库比较多，以后会缩少动态链接库数量。
《FK 应用程序框架》需要的动态链接库列表，其它动态链接库不是：
zlib1.dll
libxml2.dll
iconv.dll
TraceLog.dll
atobj.dll
SafTools.dll
TypeLib.dll
safTheme.dll
ArtFramework.dll



1.输出 exe 先放入.\fk\bin\xxx 文件夹中。			**** 因为需要调用 《FK应用程序框架》 .dll 文件。****


程序执行配置说明：
-----------------

一、发布版本位置：
	1.输出 exe 需要和《FK应用程序框架》.dll 放在一起。(这个问题正在解决中。)
	2.每个 .exe 文件都会有两个 TestMainFrame.pxml 和 TestMainFrame.uxml 文件, 放在 cfg 文件夹。
	3.*.vxml 存放在 \res\TestMainFrame 文件夹中。 以后会打包为一个文件，或者和.exe .dl文件l编译在一起。



二、开发过程位置:
	在 《FK应用程序框架》文件夹中会有一个 fk_local.xml 文件，这个文件是用来映射 *.vxml、 配置文件、主题文件位置。
	例如： 
	d:\bin\TestMainFrame.exe   映射为源代码文件夹： E:\code\TestMainFrame\res 文件夹. 使用绝对路径。



*******************************************************
* FK自动添加映射，根据项目路径情况，需要手动修改,打开fk_local.xml 文件。
*******************************************************
