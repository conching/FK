#include "stdafx.h"
#include "AboutDlg.hxx"


_TFK_BEGIN


class  LAboutDlgImpl: public tfk::LAboutDlg
{
private:
	FK_BEGIN_CODE(VARIABLE, AboutDlg_VXML)
	fk::LButton*	_pOKBtn;
	fk::LButton*	_pCancelBtn;
	FK_END_CODE()

	FK_BEGIN_CODE(EVENTDEFAULT, AboutDlg_VXML)
	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnOKBtnClick(void);
	void OnCancelBtnClick(void);
	FK_END_CODE()

private:

public:
	static wchar_t*	AboutDlg_VXML;
	LAboutDlgImpl();
	virtual ~LAboutDlgImpl();

	// base class
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	// user
};
wchar_t* LAboutDlgImpl::AboutDlg_VXML = L"AboutDlg.vxml";

//---------------------------------------------------------------------------------
//INT_PTR _call LAboutDlgImpl::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_DIALOG_PROC(LAboutDlgImpl)
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC(LAboutDlg)		 //***** 是基类。

// vv_call LAboutDlgImplOnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个。
FK_BEGIN_COMMAND_PROC(LAboutDlgImpl)
FK_COMMAND_EVENT(_pOKBtn, BN_CLICKED, OnOKBtnClick());
FK_COMMAND_EVENT(_pCancelBtn, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

// vv_call LAboutDlgImpl::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC(LAboutDlgImpl)
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// 在RAD对话框设计器中，拷贝代码到这里。 稍后提供代码同步功能。
//
vb_call LAboutDlgImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBtn, L"_pCancelBtn"},
		{(fk::LObject**)&_pOKBtn, L"_pOKBtn"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE LAboutDlgImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
LAboutDlg::LAboutDlg():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, LAboutDlgImpl::AboutDlg_VXML)
{

}

LAboutDlg::~LAboutDlg()
{

}

LAboutDlg* LAboutDlg::NewAboutDlg(void)
{
	LAboutDlgImpl*  pNewAboutDlg = NULL;

	pNewAboutDlg = new LAboutDlgImpl();
	if (pNewAboutDlg!=NULL)
	{
		pNewAboutDlg->AddRef();
		return pNewAboutDlg;	
	}

	return NULL;
}


//---------------------------------------------------------------------------------
LAboutDlgImpl::LAboutDlgImpl()
{
}

LAboutDlgImpl::~LAboutDlgImpl()
{
}

vb_call LAboutDlgImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call LAboutDlgImpl::OnInitDialog(void)
{
	LinkDataExchange(fk::ldeInstallData);

}

void LAboutDlgImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


void LAboutDlgImpl::OnOKBtnClick(void)
{
	this->EndDialog(IDOK);
}

void LAboutDlgImpl::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}


_TFK_END
