#ifndef _AboutDlg_h_
#define _AboutDlg_h_


_TFK_BEGIN

class LAboutDlg: public fk::LDialog
{
private:

public:
	LAboutDlg();
	virtual ~LAboutDlg();

	static LAboutDlg* NewAboutDlg(void);
};


_TFK_END


#endif
