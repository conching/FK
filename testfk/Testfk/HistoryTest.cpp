#include "stdafx.h"
#include "HistoryTest.hxx"
#include "fk\history.hxx"

_FK_BEGIN

class LHistoryTestImpl : public fk::LHistoryTest
{
	_FK_RTTI_;

private:
	fk::History*	_phistory;
	//fk::LAutoPtr	_ap;

public:
	LHistoryTestImpl(fk::LObject* pParentObject);
	virtual ~LHistoryTestImpl(void);

	virtual b_call Assign(LObject* pobject);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

public:

	virtual v_call Test(void);
};

_FK_RTTI_CODE2_(fk::LHistoryTestImpl, fk::LHistoryTest, fk::LObject, LHistoryTestImpl, LHistoryTest);

//---------------------------------------------------------------------------------
//_FK_RTTI_CODE_(fk::LHistoryTest, fk::LObject, LHistoryTest);

LHistoryTest::LHistoryTest(fk::LObject* pParentObject):
fk::LObject(pParentObject)
{
	_FK_RTTI_INIT_(fk::LHistoryTest);
}

LHistoryTest::~LHistoryTest(void)
{
}

LHistoryTest* _call LHistoryTest::NewHistoryTest(fk::LObject* pParentObject)
{
	LHistoryTest*  pobject= NULL;

	pobject = new LHistoryTestImpl(pParentObject);
	if (pobject!=NULL)
	{
		pobject->AddRef();
		return pobject;
	}

	return NULL;
}



//---------------------------------------------------------------------------------
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC(LHistoryTestImpl)
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

LHistoryTestImpl::LHistoryTestImpl(fk::LObject* pParentObject):
fk::LHistoryTest(pParentObject)
{
	_FK_RTTI_INIT_(fk::LHistoryTestImpl);
}

LHistoryTestImpl::~LHistoryTestImpl(void)
{
}

ULONG STDMETHODCALLTYPE LHistoryTestImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call LHistoryTestImpl::Assign(LObject* pobject)
{
	return LHistoryTest::Assign(pobject);
}

vv_call LHistoryTestImpl::Test(void)
{

}


_FK_END
