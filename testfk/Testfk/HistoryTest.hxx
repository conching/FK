#ifndef __HistoryTest_h__
#define __HistoryTest_h__


_FK_BEGIN

class LHistoryTest : public fk::LObject
{
	_FK_RTTI_;

private:
public:
	LHistoryTest(fk::LObject* pParentObject);
	virtual ~LHistoryTest(void);

	virtual v_call Test(void) = 0;

	static LHistoryTest* _call NewHistoryTest(fk::LObject* pParentObject);
};

_FK_END


#endif
