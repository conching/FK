#include "stdafx.h"
#include "NamespaceAddDlg.hxx"


_FK_BEGIN

class  LNamespaceAddDlgImpl: public LNamespaceAddDlg
{
private:
	fk::LEdit*	_pBeginNamespaceEdit;
	fk::LEdit*	_pEndNamespaceMacroEdit;
	fk::LEdit*	_pNamespaceEdit;
	fk::LButton*	_pCancelBut;
	fk::LButton*	_pOKBut;

	fk::enumNamespaceType _umNamespaceType;

	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);

	void OnOKBtnClick(void);
	void OnCancelBtnClick(void);

public:
	static wchar_t*	NAMESPACEADDDLG_VXML;
	LNamespaceAddDlgImpl(enumNamespaceType umnt);
	virtual ~LNamespaceAddDlgImpl();

	//
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

	virtual b_call CheckData(void);
};
//---------------------------------------------------------------------------------
LNamespaceAddDlg::LNamespaceAddDlg():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, LNamespaceAddDlgImpl::NAMESPACEADDDLG_VXML)
{

}

LNamespaceAddDlg::~LNamespaceAddDlg()
{

}

LNamespaceAddDlg* LNamespaceAddDlg::NewNamespaceAddDlg(enumNamespaceType umnt)
{
	LNamespaceAddDlgImpl*  pNewNamespaceAddDlg = NULL;

	pNewNamespaceAddDlg = new LNamespaceAddDlgImpl(umnt);
	if (pNewNamespaceAddDlg!=NULL)
	{
		pNewNamespaceAddDlg->AddRef();
		return pNewNamespaceAddDlg;	
	}

	return NULL;
}

//---------------------------------------------------------------------------------
wchar_t* LNamespaceAddDlgImpl::NAMESPACEADDDLG_VXML	= L"NAMESPACEADDDLG.vxml";

FK_BEGIN_DIALOG_PROC(LNamespaceAddDlgImpl)
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC(LNamespaceAddDlg)		 //***** 是基类。

FK_BEGIN_COMMAND_PROC(LNamespaceAddDlgImpl)
FK_COMMAND_EVENT(_pOKBut, BN_CLICKED, OnOKBtnClick());
FK_COMMAND_EVENT(_pCancelBut, BN_CLICKED, OnCancelBtnClick());
FK_END_COMMAND_PROC()

FK_BEGIN_NOTIFY_PROC(LNamespaceAddDlgImpl)
FK_END_NOTIFY_PROC()

vb_call LNamespaceAddDlgImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBut, L"_pCancelBut"},
		{(fk::LObject**)&_pOKBut, L"_pOKBut"},
		{(fk::LObject**)&_pBeginNamespaceEdit, L"_pBeginNamespaceEdit"},
		{(fk::LObject**)&_pEndNamespaceMacroEdit, L"_pEndNamespaceMacroEdit"},
		{(fk::LObject**)&_pNamespaceEdit, L"_pNamespaceEdit"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE LNamespaceAddDlgImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}


//---------------------------------------------------------------------------------
LNamespaceAddDlgImpl::LNamespaceAddDlgImpl(enumNamespaceType umnt)
{
	_umNamespaceType = umnt;
}

LNamespaceAddDlgImpl::~LNamespaceAddDlgImpl()
{
}


vb_call LNamespaceAddDlgImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
			DDX_TextW(fk::ldeInstallData, _pBeginNamespaceEdit, &_sBeginNamespaceMacro);
			DDX_TextW(fk::ldeInstallData, _pEndNamespaceMacroEdit, &_sEndNamespaceMacro);																		  
			DDX_TextW(fk::ldeInstallData, _pNamespaceEdit, &_sNamespace);

		}return true;
	case  fk::ldeDataToVariable:
		{
			DDX_TextW(fk::ldeDataToVariable, _pBeginNamespaceEdit, &_sBeginNamespaceMacro);
			DDX_TextW(fk::ldeDataToVariable, _pEndNamespaceMacroEdit, &_sEndNamespaceMacro);
			DDX_TextW(fk::ldeDataToVariable, _pNamespaceEdit, &_sNamespace);

		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{

		}return true;
	default:
		{

		}
	}

	return false;
}

v_call LNamespaceAddDlgImpl::OnInitDialog(void)
{
	if (_umNamespaceType==umNamespaceAdd)
	{
		SetWindowText(L"Add Namespace");
	}
	else if (_umNamespaceType==umNamespaceEdit)
	{
		SetWindowText(L"Edit Namespace");
	}

	LinkDataExchange(fk::ldeInstallData);
}

void LNamespaceAddDlgImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
}

void LNamespaceAddDlgImpl::OnOKBtnClick(void)
{
	if (LinkDataExchange(fk::ldeDataToVariable))
	{
		if (CheckData())
		{
			this->EndDialog(IDOK);
		}
	}
}

vb_call LNamespaceAddDlgImpl::CheckData()
{
	if (_sBeginNamespaceMacro.IsExist() && _sEndNamespaceMacro.IsExist() && _sNamespace.IsExist())
	{

		return true;
	}

	return false;
}

void LNamespaceAddDlgImpl::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}

_FK_END
