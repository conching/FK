#ifndef _NamespaceAddDlg_h_
#define _NamespaceAddDlg_h_



_FK_BEGIN

enum enumNamespaceType
{
	umNamespaceAdd = 0x1,
	umNamespaceEdit = 0x2,
};

class LNamespaceAddDlg: public fk::LDialog
{
private:

public:
	LNamespaceAddDlg();
	virtual ~LNamespaceAddDlg();

	fk::LStringw _sBeginNamespaceMacro;
	fk::LStringw _sEndNamespaceMacro;
	fk::LStringw _sNamespace;

	virtual b_call CheckData() = 0;

	static LNamespaceAddDlg* NewNamespaceAddDlg(enumNamespaceType umnt);
};


_FK_END


#endif
