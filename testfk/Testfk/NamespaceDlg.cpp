#include "stdafx.h"
#include "NamespaceDlg.hxx"
#include "fk\db.h"
#include "fk\dbCtrls.h"
#include "NamespaceAddDlg.hxx"



_FK_BEGIN


class  LNamespaceDlgImpl: public fk::LNamespaceDlg
{
private:
		FK_BEGIN_CODE(VARIABLE, NewProject_VXML)

			fk::LButton*	_pOKBut;
		fk::LButton*	_pCancelBut;
		fk::LButton*	_pSysListView;
		fk::LButton*	_pEditBtn;
		fk::LButton*	_pDeleteBtn;
		fk::LButton*	_pAddBtn;
		FK_END_CODE(VARIABLE)
private:
	fk::LAutoPtr _ap;
	fk::LDataConnection*	_pNamespaceDBcon;
	fk::LTable*				_pNamespaceTable;
	fk::LSysListView32*		_pSysListView321;
	fk::LDBListView*			_pDBSysListView321;

	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnNotify(WPARAM wParam, LPARAM lParam);
	void OnDBSysListView321_ItemChanged(LPNMHDR lpnmhdr);
	void OnDBSysListViewDBLick(LPNMHDR lpnmhdr);	

	void OnAddBtnClick(void);
	void OnEditBtnClick(void);
	void OnCancelBtnClick(void);
	void OnOkBtnClick(void);
	void OnDeleteBtnClick(void);

public:
	static wchar_t*	NAMESPACEDLG_VXML;
	LNamespaceDlgImpl();
	virtual ~LNamespaceDlgImpl();

	//
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);
};
wchar_t* LNamespaceDlgImpl::NAMESPACEDLG_VXML = L"NAMESPACEDLG.vxml";

//---------------------------------------------------------------------------------
LNamespaceDlg::LNamespaceDlg():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, LNamespaceDlgImpl::NAMESPACEDLG_VXML)
{

}

LNamespaceDlg::~LNamespaceDlg()
{

}

LNamespaceDlg* LNamespaceDlg::NewNamespaceDlg(void)
{
	LNamespaceDlg*  pNewNamespaceDlg = NULL;

	pNewNamespaceDlg = new LNamespaceDlgImpl();
	if (pNewNamespaceDlg!=NULL)
	{
		pNewNamespaceDlg->AddRef();
		return pNewNamespaceDlg;	
	}

	return NULL;
}

//---------------------------------------------------------------------------------
FK_BEGIN_DIALOG_PROC(LNamespaceDlgImpl)
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_MESSAGE_HANDLER(WM_NOTIFY, OnNotify(wParam, lParam), return 0);
FK_END_DIALOG_PROC(LNamespaceDlg)		 //***** 是基类。

FK_BEGIN_COMMAND_PROC(LNamespaceDlgImpl)
FK_COMMAND_EVENT(_pAddBtn, BN_CLICKED, OnAddBtnClick());
FK_COMMAND_EVENT(_pEditBtn, BN_CLICKED, OnEditBtnClick());
FK_COMMAND_EVENT(_pCancelBut, BN_CLICKED, OnCancelBtnClick());
FK_COMMAND_EVENT(_pOKBut, BN_CLICKED, OnOkBtnClick());
FK_COMMAND_EVENT(_pDeleteBtn, BN_CLICKED, OnDeleteBtnClick());
FK_END_COMMAND_PROC()

// vv_call _CLASS_NAME_::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC(LNamespaceDlgImpl)
FK_NOTIFY_EVENT(_pDBSysListView321, NM_DBLCLK, OnDBSysListViewDBLick(lpnmhdr));
FK_NOTIFY_EVENT(_pDBSysListView321, LVN_ITEMCHANGED, OnDBSysListView321_ItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

vb_call LNamespaceDlgImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBut, L"_pCancelBut"},
		{(fk::LObject**)&_pOKBut, L"_pOKBut"},
		{(fk::LObject**)&_pSysListView321, L"_pSysListView321"},
		{(fk::LObject**)&_pEditBtn, L"_pEditBtn"},
		{(fk::LObject**)&_pDeleteBtn, L"_pDeleteBtn"},
		{(fk::LObject**)&_pAddBtn, L"_pAddBtn"}
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE LNamespaceDlgImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}


//---------------------------------------------------------------------------------
LNamespaceDlgImpl::LNamespaceDlgImpl()
{
	_pNamespaceDBcon = NULL;
	_pNamespaceTable = NULL;

	_ap.AddUnknown2((IUnknown**)&_pNamespaceDBcon, (IUnknown**)&_pNamespaceTable);
	_pDBSysListView321= NULL;
}

LNamespaceDlgImpl::~LNamespaceDlgImpl()
{
}


vb_call LNamespaceDlgImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

v_call LNamespaceDlgImpl::OnInitDialog(void)
{
	fk::LStringw  sNamespaceFull(fk::umStringBuffer, MAX_PATH);

	_pNamespaceDBcon = fk::LDataConnectionXml::NewDataConnectionXml(this);
	((fk::LDataConnectionXml*)_pNamespaceDBcon)->SetDataConnectionType(fk::dctXml);

	_pobjects->AddObject(L"_pNamespaceDBcon", _pNamespaceDBcon);
	if (fk::GetModulePathW(g_pmodule, fk::umPathConfig, sNamespaceFull._pwstr))
	{
		sNamespaceFull.Append(L"\\namespace.xml");
		if (!::PathFileExists(sNamespaceFull))
		{
			fk_TraceFormatMessage(FTEXTNIL, g_pmodule->hRes, L"%s\n", ::GetLastError(), sNamespaceFull._pwstr);
			fk_ErrorBox();
		}
	}

	_pNamespaceDBcon->SetConnectionString(sNamespaceFull);
	_pNamespaceDBcon->OpenConnection();

	_pNamespaceTable = fk::LTable::NewXmlTable(_pNamespaceDBcon, g_pmodule, L"//config/namespace");
	_pobjects->AddObject(L"_pNamespaceTable", _pNamespaceTable);
	if (_pNamespaceTable->OpenTable())
	{
		_pDBSysListView321			= fk::LDBListView::NewDBListView(this);
		_pDBSysListView321->Attach(_pSysListView321->_hWnd);
		_pDBSysListView321->SetTable(_pNamespaceTable);
	}
}

void LNamespaceDlgImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
}

void LNamespaceDlgImpl::OnNotify(WPARAM wParam, LPARAM lParam)
{
	LPNMHDR lpnmhdr = (LPNMHDR)lParam;
	LRESULT lret;

	if (_pDBSysListView321!=NULL)
	{
		if (lpnmhdr->hwndFrom == _pDBSysListView321->_hWnd)
		{
			lret = _pDBSysListView321->wmNotify((LPNMHDR)lParam);
			switch(lpnmhdr->code)
			{
			case LVN_ITEMCHANGED:
				{
					OnDBSysListView321_ItemChanged(lpnmhdr);
				}break;
			case NM_DBLCLK:
				{
				}break;
			}
		}
	}
}

void LNamespaceDlgImpl::OnDBSysListView321_ItemChanged(LPNMHDR lpnmhdr)
{
	fk::LTableRow*  ptr;
	fk::LVar*  pbnm;
	fk::LVar*  penm;
	fk::LVar*  pn;

	if (_pDBSysListView321!=NULL)
	{
		ptr = _pDBSysListView321->GetSelectTableRow();
		if (ptr!=NULL)
		{
			ptr->GetData(L"BeginNamespaceMacro", (fk::LVar**)&pbnm);
			ptr->GetData(L"EndNamespaceMacro", (fk::LVar**)&penm);
			ptr->GetData(L"Namespace", (fk::LVar**)&pn);

			_sBeginNamespaceMacro = pbnm->_pwstr;
			_sEndNamespaceMacro = penm->_pwstr;
			_sNamespace = pn->_pwstr;
		}
	}
}

void LNamespaceDlgImpl::OnDBSysListViewDBLick(LPNMHDR lpnmhdr)
{
	fk::LTableRow* pTableRow;

	pTableRow = _pDBSysListView321->GetSelectTableRow();
	if (pTableRow!=NULL)
	{
		this->EndDialog(IDOK);	
	}
}

void LNamespaceDlgImpl::OnAddBtnClick(void)
{
	fk::LAutoPtr ap;
	fk::LNamespaceAddDlg* pnad;
	fk::LTableRow* pNewTableRow;

	fknew2(pnad = fk::LNamespaceAddDlg::NewNamespaceAddDlg(fk::umNamespaceAdd), pnad, ap);
	if (pnad->DoModal(_hWnd)==IDOK)
	{
		if (pnad->CheckData())
		{
			if (_pNamespaceTable->CreateTableRowDiscon(&pNewTableRow))
			{
				UINT uiCount = _pNamespaceTable->GetRowCount();
				DWORD dwMask = 0;

				pNewTableRow->SetData(L"id", fk::dtUInt, (data_t*)&uiCount);
				pNewTableRow->SetData(L"Mask", fk::dtUInt, (data_t*)&dwMask);
				pNewTableRow->SetData(L"BeginNamespaceMacro", fk::dtWChar, (data_t*)pnad->_sBeginNamespaceMacro._pwstr);
				pNewTableRow->SetData(L"EndNamespaceMacro", fk::dtWChar, (data_t*)pnad->_sEndNamespaceMacro._pwstr);
				pNewTableRow->SetData(L"Namespace", fk::dtWChar, (data_t*)pnad->_sNamespace._pwstr);
				_pNamespaceTable->AppendRow(pNewTableRow);

				_pNamespaceTable->SaveTable();
				_pNamespaceDBcon->Save();
			}
		}
	}
}

void LNamespaceDlgImpl::OnEditBtnClick(void)
{
	fk::LAutoPtr ap;
	fk::LNamespaceAddDlg* pned;
	fk::LTableRow* pTableRow;

	fknew2(pned = fk::LNamespaceAddDlg::NewNamespaceAddDlg(fk::umNamespaceEdit), pned, ap);

	pned->_sBeginNamespaceMacro = _sBeginNamespaceMacro;
	pned->_sEndNamespaceMacro = _sEndNamespaceMacro;
	pned->_sNamespace = _sNamespace;

	if (pned->DoModal(_hWnd)==IDOK)
	{
		if (pned->CheckData())
		{
			pTableRow = _pDBSysListView321->GetSelectTableRow();
			if (pTableRow!=NULL)
			{
				pTableRow->SetData(L"BeginNamespaceMacro", fk::dtWChar, (data_t*)pned->_sBeginNamespaceMacro._pwstr);
				pTableRow->SetData(L"EndNamespaceMacro", fk::dtWChar, (data_t*)pned->_sEndNamespaceMacro._pwstr);
				pTableRow->SetData(L"Namespace", fk::dtWChar, (data_t*)pned->_sNamespace._pwstr);
			}
		}
	}
}

void LNamespaceDlgImpl::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}

void LNamespaceDlgImpl::OnOkBtnClick(void)
{
	this->EndDialog(IDOK);
}

void LNamespaceDlgImpl::OnDeleteBtnClick(void)
{
	_pDBSysListView321->DeleteSelectItem(_hWnd);
}


_FK_END

