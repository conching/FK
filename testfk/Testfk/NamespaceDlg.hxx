#ifndef _NamespaceDlg_h_
#define _NamespaceDlg_h_


_FK_BEGIN

class LNamespaceDlg: public fk::LDialog
{
private:

public:
	LNamespaceDlg();
	virtual ~LNamespaceDlg();

	fk::LStringw _sBeginNamespaceMacro;
	fk::LStringw _sEndNamespaceMacro;
	fk::LStringw _sNamespace;

	static LNamespaceDlg* NewNamespaceDlg(void);
};


_FK_END


#endif
