#include "stdafx.h"
#include "NewProject.hxx"
#include "NamespaceDlg.hxx"



#define  _TEST  1


_FK_BEGIN

static wchar_t*	NewProject_VXML			= L"NewProject.vxml";

enum enumNewProjectType
{
	umnptSingleDocument = 0x1,
	umnptSingleDocumentSplication = 0x2,
	umnptMultipleDocument = 0x3,
	umnptMultipleDocumentSplication = 0x04,
	umnptDialog = 0x5,
	umnptNoUserInterface=0x6,
};

class  LNewProjectImpl: public fk::LNewProject
{
private:
	FK_BEGIN_CODE(VARIABLE, NewProject_VXML)
	fk::LButton*	_pCancelBut;
	fk::LButton*	_pOKBut;
	fk::LButton*	_pBrowseProjectPathBtn;
	fk::LButton*	_pBrowseNamespaceBtn;

	fk::LRadioButton*	_pSingleDocumentRB;
	fk::LRadioButton*	_pSingleDocumentWindowRB;
	fk::LRadioButton*	_pMultipleDocumentRB;
	fk::LRadioButton*	_pMultipleDSWRb;
	fk::LRadioButton*	_pDialogRB;
	fk::LRadioButton*	_pNotUserInterfRB;

	fk::LEdit*	_pProjectNameEdit;
	fk::LEdit*	_pProjectManaerHxxEdit;
	fk::LEdit*	_pManagerCppEdit;
	fk::LEdit*	_pNotifyCommandCppEdit;
	fk::LEdit*	_pNotifyCommandHxxEdit;
	fk::LEdit*	_pWinmainCppEdit;
	fk::LEdit*	_pProjectPathEdit;

	fk::LEdit*	_pBeginNamespaceMacroEdit;
	fk::LEdit*	_pEndNamespaceMacroEdit;
	fk::LEdit*	_pNamespaceEdit;
	FK_BEGIN_CODE(VARIABLE)

private:
	fk::LStringw	_sProjectName;
	fk::LStringw	_sProjectMgrHxx;
	fk::LStringw	_sProjectMgrCpp;
	fk::LStringw	_sNotifyCmdCpp;
	fk::LStringw	_sNotifyCmdHxx;
	fk::LStringw	_sWinmainCpp;


	fk::LStringw	_sProjectPath;

	fk::LStringa	_cBeginNamespaceMacro;
	fk::LStringa	_cEndNamespaceMacro;
	fk::LStringa	_cNamespace;

	enumNewProjectType _umNewProjectType;

	fk::LAutoPtr	_autoPtr;
	float _fVCVer;

	fk::LStringw	_sHFileExt;
	fk::LStringw	_sCppFileExt;

	v_call BuldMacroParams(fk::LMacroParamsa* pmp);
	v_call OnInitDialog(void);

	v_call OnProjectNameEditChane(void);
	v_call OnBrowseProjectFolder(void);
	v_call OnOKBtnClick(void);
	v_call OnCancelBtnClick(void);

	v_call OnSingleDocumentRBClick(void);
	v_call OnSingleDocumentWindowRBClick(void);
	v_call OnMultipleDocumentRBClick(void);
	v_call OnMultipleDSWRbClick(void);
	v_call OnDialogRBClick(void);
	v_call OnNotUserInterfRBClick(void);
	v_call OnBrowseNamespaceBtn(void);

	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnClose(WPARAM wParam, LPARAM lParam);
	void OnKeyDown(WPARAM wParam, LPARAM lParam);

public:
	LNewProjectImpl();
	virtual ~LNewProjectImpl();

	ULONG STDMETHODCALLTYPE Release(void);

	// fk::LObject
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个。
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

	virtual b_call BuildFiles(HWND hWnd);

	virtual b_call InitNewProject(LPCWSTR lpszProjectPath, LPCWSTR lpszProjectName, LPCWSTR lpszLangID, float fVCVer);
};

LNewProjectImpl::LNewProjectImpl()
{
	_umNewProjectType= umnptSingleDocument;
	_fVCVer = 0.0;

	_sProjectMgrHxxFull.MallocBuffer(MAX_PATH);
	_sProjectMgrCppFull.MallocBuffer(MAX_PATH);

	_sNotifyCmdCppFull.MallocBuffer(MAX_PATH);
	_sNotifyCmdHxxFull.MallocBuffer(MAX_PATH);

	_sWinMainCppFull.MallocBuffer(MAX_PATH);

	_sHFileExt	= L".hxx";
	_sCppFileExt= L".cpp";
}

LNewProjectImpl::~LNewProjectImpl()
{
	_autoPtr.clear();
}

// INT_PTR _call _CLASS_NAME_::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_DIALOG_PROC(LNewProjectImpl)
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_CLOSE, OnClose(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC(LNewProject)		 //***** 是基类。

// virtual v_call OnCommandProc(fk::LWindow* pWindow, int iNotify);
FK_BEGIN_COMMAND_PROC(LNewProjectImpl)
FK_COMMAND_EVENT(_pProjectNameEdit, EN_CHANGE, OnProjectNameEditChane());
FK_COMMAND_EVENT(_pOKBut, BN_CLICKED, OnOKBtnClick());
FK_COMMAND_EVENT(_pCancelBut, BN_CLICKED, OnCancelBtnClick());
FK_COMMAND_EVENT(_pSingleDocumentRB, BN_CLICKED, OnSingleDocumentRBClick());
FK_COMMAND_EVENT(_pSingleDocumentWindowRB, BN_CLICKED, OnSingleDocumentWindowRBClick());
FK_COMMAND_EVENT(_pMultipleDocumentRB, BN_CLICKED, OnMultipleDocumentRBClick());
FK_COMMAND_EVENT(_pMultipleDSWRb, BN_CLICKED, OnMultipleDSWRbClick());
FK_COMMAND_EVENT(_pDialogRB, BN_CLICKED, OnDialogRBClick());
FK_COMMAND_EVENT(_pNotUserInterfRB, BN_CLICKED, OnNotUserInterfRBClick());
FK_COMMAND_EVENT(_pBrowseProjectPathBtn, BN_CLICKED, OnBrowseProjectFolder());
FK_COMMAND_EVENT(_pBrowseNamespaceBtn, BN_CLICKED, OnBrowseNamespaceBtn());

FK_END_COMMAND_PROC()

// vv_call _CLASS_NAME_::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC(LNewProjectImpl)
FK_END_NOTIFY_PROC()

ULONG STDMETHODCALLTYPE LNewProjectImpl::Release(void)
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

vb_call LNewProjectImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
		{(fk::LObject**)&_pCancelBut, L"_pCancelBut"},
		{(fk::LObject**)&_pOKBut, L"_pOKBut"},
		{(fk::LObject**)&_pSingleDocumentRB, L"_pSingleDocumentRB"},
		{(fk::LObject**)&_pSingleDocumentWindowRB, L"_pSingleDocumentWindowRB"},
		{(fk::LObject**)&_pMultipleDocumentRB, L"_pMultipleDocumentRB"},
		{(fk::LObject**)&_pMultipleDSWRb, L"_pMultipleDSWRb"},
		{(fk::LObject**)&_pDialogRB, L"_pDialogRB"},
		{(fk::LObject**)&_pNotUserInterfRB, L"_pNotUserInterfRB"},
		{(fk::LObject**)&_pProjectNameEdit, L"_pProjectNameEdit"},
		{(fk::LObject**)&_pProjectManaerHxxEdit, L"_pProjectManaerHxxEdit"},
		{(fk::LObject**)&_pManagerCppEdit, L"_pManagerCppEdit"},
		{(fk::LObject**)&_pNotifyCommandCppEdit, L"_pNotifyCommandCppEdit"},
		{(fk::LObject**)&_pNotifyCommandHxxEdit, L"_pNotifyCommandHxxEdit"},
		{(fk::LObject**)&_pWinmainCppEdit, L"_pWinmainCppEdit"},
		{(fk::LObject**)&_pProjectPathEdit, L"_pProjectPathEdit"},
		{(fk::LObject**)&_pBrowseProjectPathBtn, L"_pBrowseProjectPathBtn"},
		{(fk::LObject**)&_pBeginNamespaceMacroEdit, L"_pBeginNamespaceMacroEdit"},
		{(fk::LObject**)&_pEndNamespaceMacroEdit, L"_pEndNamespaceMacroEdit"},	
		{(fk::LObject**)&_pNamespaceEdit, L"_pNamespaceEdit"},
		{(fk::LObject**)&_pBrowseNamespaceBtn, L"_pBrowseNamespaceBtn"}	
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		//
		// 这里显示错误信息，返回 true, 是为了让窗口能正确的显示。
		// 返回 false，窗口不显示。
		//
		fk_ErrorBox();
		return true;
	}

	return true;
}

v_call LNewProjectImpl::OnInitDialog()
{
	_pOKBut->EnableWindow(FALSE);

	switch (_umNewProjectType)
	{
	case umnptSingleDocument:
		{
			_pSingleDocumentRB->SetCheck(BST_CHECKED);
		}break;
	case umnptSingleDocumentSplication:
		{
			_pSingleDocumentWindowRB->SetCheck(BST_CHECKED);
		}break;
	case umnptMultipleDocument:
		{
			_pMultipleDocumentRB->SetCheck(BST_CHECKED);
		}break;
	case umnptMultipleDocumentSplication:
		{
			_pMultipleDSWRb->SetCheck(BST_CHECKED);
		}break;
	case umnptDialog:
		{
			_pDialogRB->SetCheck(BST_CHECKED);
		}break;
	case umnptNoUserInterface:
		{
			_pNotUserInterfRB->SetCheck(BST_CHECKED);
		}break;
	}

	_pProjectPathEdit->EnableWindow(FALSE);

	this->CenterWindow(GetDesktopWindow());
}


vb_call LNewProjectImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
			//
			// 这里先取出 _sProjectName 控件数据，在安装到其它控件上。
			//
			DDX_TextW(fk::ldeDataToVariable, _pProjectNameEdit, &_sProjectName);

			if (_sProjectName.IsExist())
			{
				DDX_TextW(fk::ldeInstallData, _pProjectManaerHxxEdit, &_sProjectName);
				_pProjectManaerHxxEdit->AppendText(L"Mgr");
				_pProjectManaerHxxEdit->AppendText(_sHFileExt);
				DDX_TextW(fk::ldeInstallData, _pManagerCppEdit, &_sProjectName);
				_pManagerCppEdit->AppendText(L"Mgr");
				_pManagerCppEdit->AppendText(_sCppFileExt);

				DDX_TextW(fk::ldeInstallData, _pNotifyCommandCppEdit, &_sProjectName);
				_pNotifyCommandCppEdit->AppendText(L"NotifyCmd");
				_pNotifyCommandCppEdit->AppendText(_sCppFileExt);
				DDX_TextW(fk::ldeInstallData, _pNotifyCommandHxxEdit, &_sProjectName);
				_pNotifyCommandHxxEdit->AppendText(L"NotifyCmd");
				_pNotifyCommandHxxEdit->AppendText(_sHFileExt);

				DDX_TextW(fk::ldeInstallData, _pWinmainCppEdit, &_sProjectName);
				_pWinmainCppEdit->AppendText(_sCppFileExt);

				_pOKBut->EnableWindow(TRUE);
			}

			DDX_TextA(ldeInstallData, _pBeginNamespaceMacroEdit, &_cBeginNamespaceMacro);
			DDX_TextA(ldeInstallData, _pEndNamespaceMacroEdit, &_cEndNamespaceMacro);
			DDX_TextA(ldeInstallData, _pNamespaceEdit, &_cNamespace);

		}return true;
	case  fk::ldeDataToVariable:
		{
			DDX_TextW(fk::ldeDataToVariable, _pProjectPathEdit, &_sProjectPath);
			DDX_TextW(fk::ldeDataToVariable, _pProjectNameEdit, &_sProjectName);

			DDX_TextW(fk::ldeDataToVariable, _pManagerCppEdit, &_sProjectMgrCpp);
			DDX_TextW(fk::ldeDataToVariable, _pProjectManaerHxxEdit, &_sProjectMgrHxx);

			DDX_TextW(fk::ldeDataToVariable, _pNotifyCommandCppEdit, &_sNotifyCmdCpp);
			DDX_TextW(fk::ldeDataToVariable, _pNotifyCommandHxxEdit, &_sNotifyCmdHxx);

			DDX_TextA(fk::ldeDataToVariable, _pBeginNamespaceMacroEdit, &_cBeginNamespaceMacro);
			DDX_TextA(fk::ldeDataToVariable, _pEndNamespaceMacroEdit, &_cEndNamespaceMacro);
			DDX_TextA(fk::ldeDataToVariable, _pNamespaceEdit, &_cNamespace);

			DDX_TextW(fk::ldeDataToVariable, _pWinmainCppEdit, &_sWinmainCpp);
		}return true;
	case  fk::ldeClearCtrl:
		{
			_pProjectNameEdit->SetWindowText(L"");
			_pProjectManaerHxxEdit->SetWindowText(L"");
			_pManagerCppEdit->SetWindowText(L"");
			_pNotifyCommandCppEdit->SetWindowText(L"");
			_pNotifyCommandHxxEdit->SetWindowText(L"");
			_pWinmainCppEdit->SetWindowText(L"");
		}return true;
	case fk::ldeClearVariable:
		{
			_sProjectName.clear();
			_sProjectMgrHxx.clear();
			_sProjectMgrCpp.clear();
			_sNotifyCmdCpp.clear();
			_sNotifyCmdHxx.clear();
			_sWinmainCpp.clear();

			_sProjectMgrHxxFull.clear();
			_sProjectMgrCppFull.clear();
			_sNotifyCmdCppFull.clear();
			_sNotifyCmdHxxFull.clear();
			_sWinMainCppFull.clear();
			_sProjectPath.clear();
		}return true;
	case fk::ldeBuildFinalData:
		{
			if (LinkDataExchange(fk::ldeDataToVariable))
			{
				_sProjectMgrCppFull.Format(L"%s\\%s", _sProjectPath._pwstr, _sProjectMgrCpp._pwstr);
				_sProjectMgrHxxFull.Format(L"%s\\%s", _sProjectPath._pwstr, _sProjectMgrHxx._pwstr);

				_sNotifyCmdCppFull.Format(L"%s\\%s", _sProjectPath._pwstr, _sNotifyCmdCpp._pwstr);
				_sNotifyCmdHxxFull.Format(L"%s\\%s", _sProjectPath._pwstr, _sNotifyCmdHxx._pwstr);

				_sWinMainCppFull.Format(L"%s\\%s", _sProjectPath._pwstr, _sWinmainCpp._pwstr);
			}

		}return true;
	default:
		{
		}
	}

	return false;
}


v_call LNewProjectImpl::BuldMacroParams(fk::LMacroParamsa* pmp)
{
	fk::LStringa cProjectName;
	fk::LStringa cText(fk::umStringBuffer, MAX_PATH);

	cProjectName.Assignw(_sProjectName);

	cText.Assignw(_sProjectName._pwstr);
	pmp->AddMacroItem("[!PROJECT_NAME]", cText._pstr);

	cText.clear();
	cText.Format("%sMgr", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_MGR_NAME]", cText._pstr);

	cText.clear();
	cText.Format("L%sMgr", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_MGR_CLASS_FULL]", cText._pstr);
	cText.clear();
	cText.Format("L%sMgrImpl", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_MGR_CLASS_FULL_IMPL]", cText._pstr);

	switch (_umNewProjectType)
	{
	case umnptSingleDocument:
	case umnptSingleDocumentSplication:
	case umnptMultipleDocument:
	case umnptMultipleDocumentSplication:
		{
		}break;
	case umnptDialog:
		{
			cText.clear();
			cText.Format("%sDlg", cProjectName._pstr);
			pmp->AddMacroItem("[!PROJECT_DLG_NAME]", cText._pstr);
			cText.clear();
			cText.Format("L%sDlg", cProjectName._pstr);
			pmp->AddMacroItem("[!PROJECT_DLG_FULL]", cText._pstr);
			cText.clear();
			cText.Format("L%sDlgImpl", cProjectName._pstr);
			pmp->AddMacroItem("[!PROJECT_DLG_FULL_IMPL]", cText._pstr);
		}break;
	case umnptNoUserInterface:
		{
		}break;
	}

	pmp->AddMacroItem("[!ABOUT_DLG_NAME]", "AboutDlg");
	pmp->AddMacroItem("[!ABOUT_DLG_CLASS_FULL]", "LAboutDlg");

	cText.clear();
	cText.Format("%sNotifyCmd", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_NOTIFY_CMD_NAME]", cText._pstr);
	cText.clear();
	cText.Format("L%sNotifyCmd", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_NOTIFY_CMD_CLASS_FULL]", cText._pstr);
	cText.clear();
	cText.Format("L%sNotifyCmdImpl", cProjectName._pstr);
	pmp->AddMacroItem("[!PROJECT_NOTIFY_CMD_CLASS_FULL_IMPL]", cText._pstr);

	cText.clear();
	cText.Format("%sAutoPtr", cProjectName._pstr);
	pmp->AddMacroItem("[!AUTOPTR_CLASS_NAME]", cText._pstr);
	cText.clear();
	cText.Format("L%ssAutoPtr", cProjectName._pstr);
	pmp->AddMacroItem("[!AUTOPTR_CLASS_NAME_FULL]", cText._pstr);

	cText.Assignw(_sHFileExt);
	pmp->AddMacroItem("[!HXX]", cText._pstr);

	if (_cBeginNamespaceMacro.IsExist())
	{
		pmp->AddMacroItem("[!BEGIN_NAMESPACE_MACRO]", _cBeginNamespaceMacro._pstr);
	}
	else
	{
		pmp->AddMacroItem("[!BEGIN_NAMESPACE_MACRO]", "");
	}

	if (_cEndNamespaceMacro.IsExist())
	{
		pmp->AddMacroItem("[!END_NAMESPACE_MACRO]", _cEndNamespaceMacro._pstr);
	}
	else
	{
		pmp->AddMacroItem("[!END_NAMESPACE_MACRO]", "");
	}

	if (_cNamespace.IsExist())
	{
		cText.Format("%s", _cNamespace._pstr);
		pmp->AddMacroItem("[!NAMESPACE]", cText._pstr);

		cText.Format("%s::", _cNamespace._pstr);
		pmp->AddMacroItem("[!NAMESPACE_FULL]", cText._pstr);
	}
	else
	{
		pmp->AddMacroItem("[!NAMESPACE]", "");
		pmp->AddMacroItem("[!NAMESPACE_FULL]", "");
	}
}

v_call LNewProjectImpl::OnProjectNameEditChane(void)
{
	if (_pProjectNameEdit->GetWindowTextLength())
	{
		this->LinkDataExchange(fk::ldeInstallData);
	}
	else
	{
		this->LinkDataExchange(fk::ldeClearVariable);
	}
}

v_call LNewProjectImpl::OnBrowseProjectFolder(void)
{
	fk::FMBROWSEINFO bi;

	bi.dwSize		= sizeof(fk::FMBROWSEINFO);
	bi.dwMask		= 0;
	bi.hRes			= g_pmodule->hRes;
	bi.hWndCtrl		= _pProjectPathEdit->_hWnd;
	bi.umControlType= fk::umBrowseInfoEdit;
	bi.lpidRet		= NULL;
	bi.spPathRet	= NULL;
	bi.bi.hwndOwner = _hWnd;
	bi.bi.iImage	= 0;
	bi.bi.lParam	= 0;
	bi.bi.lpfn		= 0;
	bi.bi.lpfn		= 0;
	bi.bi.lpszTitle = 0;
	bi.bi.pidlRoot	= 0;
	bi.bi.pszDisplayName = 0;
	bi.bi.ulFlags	= BIF_NEWDIALOGSTYLE;

	if (fk::BrowseForFolder(&bi))
	{

	}
}

v_call LNewProjectImpl::OnOKBtnClick(void)
{
	this->EndDialog(IDOK);
}

v_call LNewProjectImpl::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}

v_call LNewProjectImpl::OnSingleDocumentRBClick(void)
{
	_umNewProjectType = fk::umnptSingleDocument;
}

v_call LNewProjectImpl::OnSingleDocumentWindowRBClick(void)
{
	_umNewProjectType = fk::umnptSingleDocumentSplication;
}

v_call LNewProjectImpl::OnMultipleDocumentRBClick(void)
{
	_umNewProjectType = fk::umnptMultipleDocument;
}

v_call LNewProjectImpl::OnMultipleDSWRbClick(void)
{
	_umNewProjectType = fk::umnptMultipleDocumentSplication;
}

v_call LNewProjectImpl::OnDialogRBClick(void)
{
	_umNewProjectType = fk::umnptDialog;
}

v_call LNewProjectImpl::OnNotUserInterfRBClick(void)
{
	_umNewProjectType = fk::umnptNoUserInterface;
}


v_call LNewProjectImpl::OnBrowseNamespaceBtn(void)
{
	fk::LAutoPtr ap;
	fk::LNamespaceDlg* pnd;

	fknew2(pnd = fk::LNamespaceDlg::NewNamespaceDlg(), pnd, ap);

	if (pnd->DoModal(_hWnd)==IDOK)
	{
		_cBeginNamespaceMacro = pnd->_sBeginNamespaceMacro;
		_cEndNamespaceMacro = pnd->_sEndNamespaceMacro;
		_cNamespace = pnd->_sNamespace;

		this->LinkDataExchange(fk::ldeInstallData);
	}
}

void LNewProjectImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
}

void LNewProjectImpl::OnClose(WPARAM wParam, LPARAM lParam)
{
	this->EndDialog(IDCANCEL);
}

void LNewProjectImpl::OnKeyDown(WPARAM wParam, LPARAM lParam)
{
}

vb_call LNewProjectImpl::BuildFiles(HWND hWnd)
{

	return true;
}

vb_call LNewProjectImpl::InitNewProject(LPCWSTR lpszProjectPath, LPCWSTR lpszProjectName, LPCWSTR lpszLangID, float fVCVer)
{
	fk_ParamAssert3(lpszProjectPath==NULL, lpszProjectName==NULL, lpszLangID==NULL, return false; );

	_sProjectPath	= lpszProjectPath;
	_sProjectName	= lpszProjectName;


	_fVCVer			= fVCVer;

	return true;
}



LNewProject::LNewProject():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, NewProject_VXML)
{

}

LNewProject::~LNewProject()
{

}

LNewProject* _fncall LNewProject::NewNewProject()
{
	LNewProject*  pNewNewProject = NULL;

	pNewNewProject = new LNewProjectImpl();
	if (pNewNewProject!=NULL)
	{
		pNewNewProject->AddRef();
		return pNewNewProject;	
	}

	return NULL;
}


_FK_END
