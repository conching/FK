#ifndef _NewProject_h_
#define _NewProject_h_


_FK_BEGIN


/*
输出：
ProjectMgr.cpp 文件名称。
ProjectMgr.hxx
ProjectNotifyCommand.cpp
ProjectNotifyCommand.hxx
Project.cpp 文件名称。

AboutDlg.cpp
AboutDlg.hxx
AboutDlg.vxml 文件名称。
*/

class LNewProject: public fk::LDialog
{
private:

public:
	LNewProject();
	virtual ~LNewProject();

	// out
	fk::LStringw	_sAboutDlgHxxFull;
	fk::LStringw	_sAboutDlgCppFull;
	fk::LStringw	_sAboutDlgVxml;

	fk::LStringw	_sProjectAutoPtrHxxFull;
	fk::LStringw	_sProjectAutoPtrCppFull;

	fk::LStringw	_sProjectMgrHxxFull;
	fk::LStringw	_sProjectMgrCppFull;

	fk::LStringw	_sResourceHxxFull;

	fk::LStringw	_sProjectHiFull;
	fk::LStringw	_sProjectCiFull;

	fk::LStringw	_sProjectIdlFull;
	fk::LStringw	_sProjectRcFull;
	fk::LStringw	_sProjectRgsFull;
	fk::LStringw	_sProjectVxmlFull;

	fk::LStringw	_sStdafxCppFull;
	fk::LStringw	_sStdafxHxxFull;

	fk::LStringw	_sNotifyCmdCppFull;
	fk::LStringw	_sNotifyCmdHxxFull;

	fk::LStringw	_sWinMainCppFull;
	fk::LStringw	_sReadmeTxtFull;

	fk::LStringw	_sProjectFiltersFull;

	virtual b_call InitNewProject(LPCWSTR lpszProjectPath, LPCWSTR lpszProjectName, LPCWSTR lpszLangID, float fVCVer) = 0;

	static LNewProject* _fncall NewNewProject();
};


_FK_END


#endif
