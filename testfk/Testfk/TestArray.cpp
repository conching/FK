#include "stdafx.h"
#include "fk\array.hxx"
#include "fk\map.h"
#include "fk\test.h"



_TFK_BEGIN
#define MAX_LOADSTRING 100


bool _fncall _ArrayDeleteDataItem(fk::FARRAY hArray, data_t* ppArray, uint iCount);


typedef struct tagTESTDATAITEM
{
	DWORD dwSize;
	DWORD dwMask;
	DWORD dwValue;
	fk::LStringw  sText;
}TESTDATAITEM,*LPTESTDATAITEM;


bool _fncall _ArrayDeleteDataItem(fk::FARRAY hArray, data_t* ppArray, uint iCount)
{
	int iIndex;
	data_t* pdata;
	LPTESTDATAITEM lptdi;

	pdata = ppArray;
	for (iIndex=0; iIndex<iCount; iIndex++)
	{
		lptdi = (LPTESTDATAITEM)*pdata;
		delete lptdi;

		*pdata++;
	}

	return true;
}

void ArrayPrintFull(fk::FARRAY fArray)
{
	data_t* pdata;
	LPTESTDATAITEM lpi = NULL;

	pdata = fk::ArrayGetDatas(fArray);

	for (int iIndex=0; *pdata!=FK_DATA_END; iIndex++, *pdata++)
	{
		lpi = (LPTESTDATAITEM)*pdata;
		fk_TraceTest(FTEXTNIL, L"Data index = %d, value=%d\n", iIndex, lpi->dwValue);
	}
}


void ArrayPrintFull2(fk::FARRAY fArray)
{
	data_t* pdata;
	LPTESTDATAITEM lpi = NULL;

	pdata = fk::ArrayGetDatas(fArray);

	for (; *pdata!=FK_DATA_END; pdata++)
	{
		fk_TraceTest(FTEXTNIL, L"Data index = %d, value=%d\n", 0, 0);
	}
}


v_fncall _TestArray()
{
	fk::FARRAY			fArray;
	fk::ARRAYCLASS		ac;
	data_t* parray = NULL;
	data_t	idst = 0;
	LPTESTDATAITEM ppArray[10];
	LPTESTDATAITEM lpi = NULL;
	int iCount = 0;
	int iIndex = 0;
	int iFindIndex = 0;

	ac.dwSize		= sizeof(fk::ARRAYCLASS);
	ac.uiInitCount	= 4;
	ac.uiNewCount	= 10;

	//ac.fnArrayCreateData = (fk::FNARRAYCREATEDATA)_ArrayCreateDataInt;
	ac.fnArrayDeleteData = (fk::FNARRAYDELETEDATA)_ArrayDeleteDataItem;

	fk::TraceTestNode(FTEXTNIL, 3, L"test fk::FARRAY\n");

	fArray = fk::ArrayCreateEx(4, 10, NULL, _ArrayDeleteDataItem);
	FK_TEST_IF(fArray!=NULL);

	iCount = 10;
	iIndex = 0;
	while (iIndex<iCount)
	{
		lpi = new TESTDATAITEM;
		lpi->dwSize = sizeof(TESTDATAITEM);
		lpi->dwMask = 0;
		lpi->dwValue = iIndex;
		ppArray[iIndex] = lpi;
		iIndex++;
	}

	ArrayPrintFull2(fArray);

	FK_TEST_IF(fk::ArrayAdd(fArray, 10, (data_t*)ppArray));
	FK_TEST_IF(fk::ArrayGetCount(fArray)>10);
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 3);
	FK_TEST_IF(lpi->dwValue==3);

	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 0);
	FK_TEST_IF(lpi->dwValue==0);

	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 9);
	FK_TEST_IF(lpi->dwValue==9);


	lpi = new TESTDATAITEM;
	lpi->dwSize = sizeof(TESTDATAITEM);
	lpi->dwMask = 0;
	lpi->dwValue = iIndex+1;
	fk::ArrayAdd(fArray, 1, (data_t*)lpi);


	FK_TEST_IF(fk::ArrayGetCount(fArray)>11);
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 10);
	FK_TEST_IF(lpi->dwValue==11);


	lpi = new TESTDATAITEM;
	lpi->dwSize = sizeof(TESTDATAITEM);
	lpi->dwMask = 0;
	lpi->dwValue = 1111;
	fk::ArrayInsert(fArray, 5, 1, (data_t*)lpi);
	FK_TEST_IF(fk::ArrayGetCount(fArray)>12);

	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 5);
	FK_TEST_IF(lpi->dwValue==5);

	iFindIndex = fk::ArrayFind(fArray, 0, (data_t*)lpi);
	FK_TEST_IF(iFindIndex==5);

	FK_TEST_IF(fk::ArrayRemoveData(fArray, 0, (data_t*)lpi));
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 5);
	FK_TEST_IF(lpi->dwValue==6);

	FK_TEST_IF(fk::ArrayRemoveIndex(fArray, 5));
	FK_TEST_IF(lpi->dwValue==7);

	FK_TEST_IF(fk::ArrayRemoveRange(fArray, 6, 3));
	FK_TEST_IF(fk::ArrayGetCount(fArray)>10);

	ArrayPrintFull(fArray);
}

_TFK_END
