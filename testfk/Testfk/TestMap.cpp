#include "stdafx.h"
#include "fk\array.hxx"
#include "fk\map.h"
#include "fk\test.h"
#define MAX_LOADSTRING 100


bool _fncall _ArrayDeleteDataItem(fk::FARRAY hArray, data_t* ppArray, uint iCount);


bool _fncall _ArrayDeleteDataItem(fk::FARRAY hArray, data_t* ppArray, uint iCount)
{
	int iIndex;

	for (iIndex=0; iIndex<iCount; iIndex++)
	{

	}

	return true;
}

void ArrayPrintFull(fk::LArray* pa)
{
	int			iCount;
	data_t*		pArray;
	int*		piValue;
	int			iValue;
	int			iIndex;

	pArray = (data_t*)pa->GetDatas();
	iCount = pa->GetCount();
	iIndex = 0;
	printf("Print full item:\n");
	while (iIndex<iCount)
	{
		piValue = (int*)*pArray++;
		iValue = *piValue;
		wprintf(L"%d  ", iValue);
		iIndex++;
	}
}


typedef struct tagTESTDATAITEM
{
	DWORD dwSize;
	DWORD dwMask;
	DWORD dwValue;
	fk::LStringw  sText;
}TESTDATAITEM,*LPTESTDATAITEM;


void _TestArray()
{
	fk::FARRAY			fArray;
	fk::ARRAYCLASS		ac;
	data_t* parray = NULL;
	data_t	idst = 0;
	LPTESTDATAITEM ppArray[10];
	LPTESTDATAITEM lpi = NULL;
	int iCount = 0;
	int iIndex = 0;
	int iFindIndex = 0;

	ac.dwSize		= sizeof(fk::ARRAYCLASS);
	ac.uiInitCount	= 4;
	ac.uiNewCount	= 10;

	//ac.fnArrayCreateData = (fk::FNARRAYCREATEDATA)_ArrayCreateDataInt;
	ac.fnArrayDeleteData = (fk::FNARRAYDELETEDATA)_ArrayDeleteDataItem;

	fArray = fk::ArrayCreateEx(4, 10, NULL, _ArrayDeleteDataItem);
	FK_TEST_IF(fArray!=NULL);

	iCount = 10;
	iIndex = 0;
	while (iIndex<iCount)
	{
		lpi = new TESTDATAITEM;
		lpi->dwSize = sizeof(TESTDATAITEM);
		lpi->dwMask = 0;
		lpi->dwValue = iIndex;
		ppArray[iIndex] = lpi;
		iIndex++;
	}

	FK_TEST_IF(fk::ArrayAdd(fArray, 10, (data_t*)ppArray));
	FK_TEST_IF(fk::ArrayGetCount(fArray)>10);
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 3);
	FK_TEST_IF(lpi->dwValue==3);


	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 0);
	FK_TEST_IF(lpi->dwValue==0);

	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 9);
	FK_TEST_IF(lpi->dwValue==9);


	lpi = new TESTDATAITEM;
	lpi->dwSize = sizeof(TESTDATAITEM);
	lpi->dwMask = 0;
	lpi->dwValue = iIndex+1;
	fk::ArrayAdd(fArray, 1, (data_t*)&lpi);


	FK_TEST_IF(fk::ArrayGetCount(fArray)>11);
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 11);
	FK_TEST_IF(lpi->dwValue==11);


	lpi = new TESTDATAITEM;
	lpi->dwSize = sizeof(TESTDATAITEM);
	lpi->dwMask = 0;
	lpi->dwValue = 1111;
	fk::ArrayInsert(fArray, 5, 1, (data_t*)lpi);
	FK_TEST_IF(fk::ArrayGetCount(fArray)>12);


	//
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 5);
	FK_TEST_IF(lpi->dwValue==5);

	iFindIndex = fk::ArrayFind(fArray, 0, (data_t*)lpi);
	FK_TEST_IF(iFindIndex==5);

	FK_TEST_IF(fk::ArrayRemoveData(fArray, 0, (data_t*)lpi));
	lpi = (LPTESTDATAITEM)fk::ArrayGetItem(fArray, 5);
	FK_TEST_IF(lpi->dwValue==6);

	FK_TEST_IF(fk::ArrayRemoveIndex(fArray, 5));
	FK_TEST_IF(lpi->dwValue==7);

	FK_TEST_IF(fk::ArrayRemoveRange(fArray, 6, 3));
	FK_TEST_IF(fk::ArrayGetCount(fArray)>10);
}


void TestMapIntWChar()
{
	fk::FMAP			hMap;
	fk::LPMAPINTWCHAR	lpMapIntWChar;
	wchar_t*			lpszText = L"MapWCharPtr";
	int					iLen;
	wchar_t				szPath[200];
	int					iAddCount;
	int					iIndex=0;
	int					iMapCount=0;
	data_t*				pDataArray;

	iLen = wcslen(lpszText);
	hMap = fk::MapCreate(5, 5, fk::umMapDataTypIntWstr, NULL);

	szPath[0]	= L'\0';
	iAddCount	= 10000;
	iIndex		= 0;
	for (; iIndex<iAddCount; iIndex++)
	{
		swprintf(szPath, L"%s%d", lpszText, iIndex);
		lpMapIntWChar = fk::MapMallocWCharPtr(iIndex, szPath, iLen);
		fk::MapAdd(hMap, (void*)lpMapIntWChar);	
	}
	fk::MapEraseIndex(hMap, 9997);

	iIndex		= 0;
	pDataArray	= fk::MapGetData(hMap);
	iMapCount	= fk::MapGetCount(hMap);
	for (; iIndex<iMapCount; iIndex++)
	{
		lpMapIntWChar = (fk::LPMAPINTWCHAR)(*(pDataArray++));//
		//lpMapIntWChar = (fk::LPMAPINTWCHAR)pDataArray[iIndex];
		wprintf(L"key=%d,%s\n", lpMapIntWChar->iKey, lpMapIntWChar->pText);
	}

	 fk::MapFindIntPtr(hMap, iAddCount-2);

	fk::MapDeleteItem(hMap);
}


void TestMap()
{
	TestMapIntWChar();
}


//int _tmain(int argc, _TCHAR* argv[])
//{
//	TestMap();
//
//	return 0;
//}
