#include "stdafx.h"
#include "TestRuntimeClass.hxx"


_TFK_BEGIN

class LTestRuntimeClassImpl : public tfk::LTestRuntimeClass
{
	_FK_RTTI_;

private:
public:
	LTestRuntimeClassImpl(fk::LObject* pParentObject);
	virtual ~LTestRuntimeClassImpl(void);

	virtual b_call Assign(LObject* pobject);
	virtual ULONG STDMETHODCALLTYPE Release();
	virtual STDMETHODIMP QueryInterface(REFIID riid, void **ppv);

	
	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);

	virtual int execute(void);
	virtual void undo(void);

public:

};

_FK_RTTI_CODE2_(tfk::LTestRuntimeClassImpl, tfk::LTestRuntimeClass, fk::ICommand, LTestRuntimeClassImpl, LTestRuntimeClass);

//---------------------------------------------------------------------------------
LTestRuntimeClass::LTestRuntimeClass(fk::LObject* pParentObject):
fk::ICommand(pParentObject)
{
	_FK_RTTI_INIT_(tfk::LTestRuntimeClass);
}

LTestRuntimeClass::~LTestRuntimeClass(void)
{
}

b_fncall LTestRuntimeClass::NewTestRuntimeClass(fk::LObject* powner, LPVOID* ppobj, IID riid)
{
	fk_pv2(powner==NULL, ppobj==NULL, return false;);
	tfk::LTestRuntimeClass*  pobject= NULL;

	pobject = new tfk::LTestRuntimeClassImpl(powner);
	if (pobject!=NULL)
	{
		if (pobject->create())
		{
			pobject->AddRef();
			*ppobj = pobject;

			return true;
		}
		else
		{
			delete pobject;
			pobject = NULL;
		}
	}

	return false;
}

//---------------------------------------------------------------------------------
//	virtual b_call TargetProc(void* pRegObj, UINT uMsg, fk::PNOTIFYEVENT pEvent);
FK_BEGIN_TARGETPROC(LTestRuntimeClassImpl)
//FK_TARGETPROC_EVENT(_pObjectName, fk::ON_MESSAGE_ID, OnMessageID(pEvent));
FK_END_TARGETPROC();

LTestRuntimeClassImpl::LTestRuntimeClassImpl(fk::LObject* pParentObject):
tfk::LTestRuntimeClass(pParentObject)
{
	_FK_RTTI_INIT_(tfk::LTestRuntimeClassImpl);
}

LTestRuntimeClassImpl::~LTestRuntimeClassImpl(void)
{
}

ULONG STDMETHODCALLTYPE LTestRuntimeClassImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

STDMETHODIMP LTestRuntimeClassImpl::QueryInterface(REFIID riid, void **ppv)
{
	//FK_QUERY_IID(riid, tfk::IID_TestRuntimeClass, tfk::LTestRuntimeClass, ppv, this);

	return tfk::LTestRuntimeClass::QueryInterface(riid, ppv);
}

vb_call LTestRuntimeClassImpl::Assign(LObject* pobject)
{
	return LTestRuntimeClass::Assign(pobject);
}



int LTestRuntimeClassImpl::execute(void)
{
	return 0;
}

void LTestRuntimeClassImpl::undo(void)
{

}

_TFK_END
