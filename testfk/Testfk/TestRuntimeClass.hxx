#ifndef __TestRuntimeClass_h__
#define __TestRuntimeClass_h__


_TFK_BEGIN

// [!GUID_REGISTRY_FORMAT]
//static const GUID IID_TestRuntimeClass = [!GUID_HEX_STRUCT_FORMAT];

class LTestRuntimeClass : public fk::ICommand
{
	_FK_RTTI_;

private:
public:
	LTestRuntimeClass(fk::LObject* pParentObject);
	virtual ~LTestRuntimeClass(void);

    static b_fncall NewTestRuntimeClass(fk::LObject* powner, LPVOID* ppobj, IID riid);
};

_TFK_END


#endif
