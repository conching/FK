#include "stdafx.h"
#include "resource.h"
#include "Testfk_i.h"
#include "TestfkMgr.hxx"


class CTestfk : public CAtlExeModuleT< CTestfk >
{
public :
	DECLARE_LIBID(LIBID_TestfkLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_TESTFK, "{1035A06E-6D5C-4A0C-BA99-19732AC54172}")
};

CTestfk _AtlModule;

extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	HRESULT			hr = 0;
	tfk::LTestfkMgr*		pmgr = NULL;
	int				iRet = 0;
	::CoInitialize(NULL);
	if (_AtlModule.ParseCommandLine(lpCmdLine, &hr))
	{
		pmgr = tfk::LTestfkMgr::NewTestfkMgr();
		iRet = pmgr->WinMain(false);
		pmgr->Release();

		fk::UninitializeModule(&g_pmodule);
		return iRet;
	}

	return 1;
}

