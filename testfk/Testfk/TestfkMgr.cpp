#include "StdAfx.h"
#include "TestfkMgr.hxx"
#include "resource.h"
#include "fk\xmldef.hxx"
#include "TestfkNotifyCmd.hxx"


_TFK_BEGIN


LTestfkMgr* g_pTestfkMgr = NULL;

static wchar_t*	RES_Testfk_VXML			= L"Testfk.vxml";

fk::LMacroParamsw*		g_pMacroParamsw = NULL;
fk::LMacroParamsa*		g_pMacroParamsa = NULL;

b_fncall GetMacroParamsw(fk::LMacroParamsw** ppMacroParamsw)
{
	fk_pv1(ppMacroParamsw==NULL, return false;);

	if (g_pMacroParamsw==NULL)
	{
		g_pMacroParamsw = fk::LMacroParamsw::NewMacroParamsw(g_pmodule);
		g_pMacroParamsw->AddRef();
	}

	*ppMacroParamsw = g_pMacroParamsw;
	return true;
}


class LTestfkMgrImpl : public tfk::LTestfkMgr
{
private:

private:
	fk::LAutoPtr		_autoPtr;
	tfk::LTestfkNotifyCmd*	_pNotifyCommand;


	v_call OnMFActiveItem(fk::PNOTIFYEVENT pEvent);
	v_call OnMFQueryClose(fk::PNOTIFYEVENT pEvent);
	v_call OnMFMainClose(fk::PNOTIFYEVENT pEvent);

	b_call InitFk(void);
	b_call CreateMainFrame_(void);
	b_call InitMacroParamsw(void);

	b_call RegMainFrameTargetProc_(fk::enumInstallEvent installEvent);

	b_call ModuleConfigDnaRegsvrTest(void);			// 添加临时数据。

	static b_fncall TestfkLanguageProc(fk::umLoadLanguage loadLanguage);

public:
	LTestfkMgrImpl(void);
	virtual ~LTestfkMgrImpl(void);

	virtual ULONG STDMETHODCALLTYPE Release();
	virtual b_call TargetProc(void* pobj, UINT uiMsg, fk::PNOTIFYEVENT pEvent);

	virtual i_call WinMain(bool bAutomation);
};

FK_BEGIN_TARGETPROC(LTestfkMgrImpl)
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_ACTIVEITEM, OnMFActiveItem(pEvent));
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_QUERYCLOSE, OnMFQueryClose(pEvent));
FK_TARGETPROC_EVENT(_pMainFrame, fk::ON_MF_MAINFRAMECLOSE, OnMFMainClose(pEvent));
FK_END_TARGETPROC();

ULONG STDMETHODCALLTYPE LTestfkMgrImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

LTestfkMgrImpl::LTestfkMgrImpl(void)
{
	_pComFramework	= NULL;
	_pNotifyCommand = NULL;
	_pModuleConfig = NULL;

	_autoPtr.AddUnknown2((IUnknown**)&_pNotifyCommand, (IUnknown**)&_pComFramework);

	fk::LanguageRegsvrLoad( (fk::LOADLANGUAGEPROC)LTestfkMgrImpl::TestfkLanguageProc );
}

LTestfkMgrImpl::~LTestfkMgrImpl(void)
{
	_autoPtr.clear();

	fk::LanguageUnregsvrLoad( (fk::LOADLANGUAGEPROC)LTestfkMgrImpl::TestfkLanguageProc );

	if (_pModuleConfig!=NULL)
	{
		_pModuleConfig->Release();
	}

	fk::UninitApplicationInfo();
}

b_fncall LTestfkMgrImpl::TestfkLanguageProc(fk::umLoadLanguage loadLanguage)
{
	switch(loadLanguage)
	{
	case fk::umLoadLanguageInstall:
		{
			// 这段代码使用 .dll 文件资源。
			//
			//if (fk::LanguageLoadProjectInline(g_pmodule,  _AtlBaseModule.GetModuleInstance() ) )
			//	return true;

			// 这段代码使用自身 .exe 文件资源。
			if (fk::LanguageLoadSelf(g_pmodule, _AtlBaseModule.GetModuleInstance()) )
				return true;

			return false;
		}
	case fk::umLoadLanguageUninstall:
		{

		}
	}

	return false;
}

b_call LTestfkMgrImpl::RegMainFrameTargetProc_(fk::enumInstallEvent installEvent)
{
	fk::NOTIFYITEM		npi;
	fk::LNotify*		pNotify;
	HRESULT				hr;

	npi.dwSize			= sizeof(fk::NOTIFYITEM);
	npi.dwMask			= fk::NIF_CLASSPROC;
	npi.dwActiveEvent	= fk::ON_MF_ACTIVEITEM | fk::ON_MF_INSERTITEM | fk::ON_MF_MAINFRAMECLOSE;
	npi.pTargetProc		= (fk::LObject*)(this);
	npi.lParam			= (LPARAM)0;
	npi.dwRegObject		= (void*)_pMainFrame;
	npi.dwObjectID		= (DWORD)(void*)_pMainFrame;

	hr = _pMainFrame->QueryInterface(fk::IID_INotify, (LPVOID*)&pNotify);
	if (hr==S_OK)
	{
		if (installEvent==fk::umInstallEventInstall)
		{
			if (pNotify->NotifyAdd(&npi))
			{
				return true;
			}
			else
			{
				fk_ErrorBox();
			}
		}
		else if (installEvent==fk::umInstallEventUninstall)
		{
			if (pNotify->NotifyRemove(&npi))
			{
				return true;
			}
			else
			{
				fk_ErrorBox();
			}
		}
	}

	return false;
}

v_fncall GetDllDnaXmlFile(fk::LStringws* spDnaXmlFiles)	
{
	//spDnaXmlFiles->NewTextw()->Assign(L"TraceLog.xml");
}

//
// 这里的初始化比较复杂，以后整合消减。
//
b_call LTestfkMgrImpl::InitFk(void)
{
	HRESULT					hr;
	fk::LStringw			sSoftName;
	fk::INITAPPLICATIONW	initApp;

	if (fk::InitializeModule(&g_pmodule, _AtlBaseModule.m_hInst))
	{
		g_pmodule->fnGetDllDnaXmlFile = GetDllDnaXmlFile;

		memset((void*)&initApp, 0, sizeof(fk::INITAPPLICATIONW));
		initApp.dwSize		= sizeof(fk::INITAPPLICATIONW);
		initApp.hMainModule = _AtlBaseModule.m_hInst;
		initApp.dwMask		= IAF_USER_CONFIG | IAF_APP_CONCIFG;
		initApp.pMainModule	= g_pmodule;
		initApp.dwMask		= IAF_USER_CONFIG | IAF_APP_CONCIFG;
		initApp.pMainModule	= g_pmodule;

		if (fk::InitApplicationInfo(&initApp))
		{
			if (InitMacroParamsw())
			{
				this->ModuleConfigDnaRegsvrTest();
				initApp.dwMask		= IAF_SOFTWARE_NAME;
				sSoftName.LoadStr(g_pmodule->hRes, IDS_PROJNAME);
				initApp.wsSoftwareName = (wchar_t*)sSoftName._pwstr;

				hr = CoCreateInstancePtr(fk::CLSID_ComFramework, NULL, CLSCTX_ALL, fk::IID_IComFramework, (LPVOID*)&_pComFramework);
				if (hr==S_OK)
				{
					hr = _pComFramework->InitComFramework();
					if (hr==S_OK)
					{
						return true;
					}
				}
			}
		}
	}

	fk_ErrorBox();
	return false;
}


//
// 创建框架并安装命令接受器，显示主窗口。
//
b_call LTestfkMgrImpl::CreateMainFrame_()
{
	HRESULT				hr;
	fk::ICommandBars*	pCommandBars = NULL;
	fk::LAutoPtr		ap;

	ap.AddUnknown((IUnknown**)&pCommandBars);

	hr = _pComFramework->CreateMainFrameObject((fk::IMainFrame**)&_pMainFrame);
	if (hr==S_OK)
	{
		RegMainFrameTargetProc_(fk::umInstallEventInstall);

		hr = _pMainFrame->CreateMainFrameXml((long*)g_pmodule, RES_Testfk_VXML);
		if (hr==S_OK)
		{
			_pNotifyCommand = tfk::LTestfkNotifyCmd::NewTestfkNotifyCmd();

			_pMainFrame->get_CommandBars((IUnknown**)&pCommandBars);
			_pMainFrame->InstallNotifyCommand(fk::umInstallEventInstall, (IUnknown*)_pNotifyCommand);
			pCommandBars->put_NotifyCommand((fk::INotifyCommand*)_pNotifyCommand);

			//
			// 这个函数同等于 ::ShowWindow 函数，传入 -1 是说明是使用 fk 读保存的坐标值。
			//
			_pMainFrame->ShowWindow(-1);
			return true;
		}
		else
		{
			fk_ErrorBox();
			return false;
		}

		return true;
	}
	else
	{
		fk_ErrorBox();
	}

	fk_ErrorBox();
	return false;
}

#define S_MACRO_RUN_PATH			"$(TestFKPath)"
#define S_MACRO_EXT					"$(TestFKExt)"
#define S_MACRO_FILE_NAME			"$(TestFKFileName)"
#define S_MACRO_FKROOT_199			"$(FKRoot1.99)"

b_call LTestfkMgrImpl::InitMacroParamsw(void)
{
	fk::LStringw		sPath(MAX_PATH);
	fk::LStringw		sExt;
	fk::LStringw		sFileName;

	fk_autop(fk::LMacroParamsw*, pmp);
	
	if (GetMacroParamsw(&pmp))
	{
		if (::GetModuleFileName(NULL, sPath, MAX_PATH))
		{
			sExt = ::PathFindExtension(sPath);
			sFileName = ::PathFindFileName(sPath);
			::PathRemoveFileSpec(sPath);

			pmp->AddMacroItem(_CRT_WIDE(S_MACRO_RUN_PATH), sPath, fk::umMacroParamSuccess);
			pmp->AddMacroItem(_CRT_WIDE(S_MACRO_EXT), sExt, fk::umMacroParamSuccess);
			pmp->AddMacroItem(_CRT_WIDE(S_MACRO_FILE_NAME), sFileName, fk::umMacroParamSuccess);

			pmp->AddMacroItem(_CRT_WIDE(S_MACRO_FKROOT_199), L"l:\\1.99\\fk", fk::umMacroParamSuccess); 

			return true;
		}
	}

	return false;
}

//
// 测试代码方式，注册Dna模块文件到主程序配置文件中。
//
b_call LTestfkMgrImpl::ModuleConfigDnaRegsvrTest(void)			// 添加临时数据。
{
	fk::LMacroParamsw*			pmp = NULL;
	fk::LStringw				sModulePath(MAX_PATH);
	fk::LModuleConfig*			pModuleConfig;

	fk_autop(fk::LModuleConfigManager*, pmcm);

	if (fk::GetModuleConfigManager(&pmcm))
	{
		if (GetMacroParamsw(&pmp))
		{
			sModulePath = _CRT_WIDE(S_MACRO_FKROOT_199);
			sModulePath += "\\prj\\joint.fk\\cfg\\TraceLogBase.xml";

			pmp->MakeMacro(&sModulePath);
			if (fk::PathFileExistsBox(GetActiveWindow(), sModulePath))
			{
				pmcm->AddModuleConfig(0, sModulePath, &pModuleConfig);

				pModuleConfig->Release();
				return true;
			}
		}
	}

	return false;
}

v_call LTestfkMgrImpl::OnMFActiveItem(fk::PNOTIFYEVENT pEvent)
{
}

v_call LTestfkMgrImpl::OnMFQueryClose(fk::PNOTIFYEVENT pEvent)
{
}

v_call LTestfkMgrImpl::OnMFMainClose(fk::PNOTIFYEVENT pEvent)
{
}

i_call LTestfkMgrImpl::WinMain(bool bAutomation)
{
	if (InitFk())
	{
		if (CreateMainFrame_())
		{
			_pComFramework->Run();
			return 1;
		}
	}

	fk_ErrorBox();
	return 0;
}



////////////////////////////////////////////////////////////////////////////////////////////////


LTestfkMgr::LTestfkMgr(void):
fk::LComponent(g_pmodule)
{
	_pMainFrame = NULL;
}

LTestfkMgr::~LTestfkMgr(void)
{

}


LTestfkMgr* _fncall LTestfkMgr::NewTestfkMgr(void)
{
	if (g_pTestfkMgr==NULL)
	{
		g_pTestfkMgr = new LTestfkMgrImpl();
		g_pTestfkMgr->AddRef();
	}

	return g_pTestfkMgr;
}


_TFK_END
