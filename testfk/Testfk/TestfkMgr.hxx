#ifndef __TestfkMgr_h__
#define __TestfkMgr_h__


_TFK_BEGIN


class LTestfkMgr : public fk::LComponent
{
private:
public:
	LTestfkMgr(void);
	virtual ~LTestfkMgr(void);

	fk::IComFramework*	_pComFramework;
	fk::IMainFrame*		_pMainFrame;
	fk::LModuleConfig*	_pModuleConfig;

	virtual i_call WinMain(bool bAutomation) = 0;

	static LTestfkMgr* _fncall NewTestfkMgr(void);
};

extern LTestfkMgr* g_TestfkMgr;

_TFK_END


#endif