#include "stdafx.h"
#include "fk\xmldef.hxx"
#include "TestfkNotifyCmd.hxx"
#include "fk\test.h"
#include "ThreadListTestDlg.hxx"
#include "NamespaceAddDlg.hxx"
#include "NamespaceDlg.hxx"
#include "NewProject.hxx"
#include "fkdna/fkdna.hxx"
#include "HistoryTest.hxx"
//#include "..\prj\fkdna\ModuleConfigManagerDlg.hxx"

#include "..\TestMainFrame\TestMainFrameMgr.hxx"
#include "TestRuntimeClass.hxx"

_TFK_BEGIN

#define ID_FILE_OPEN			10000

v_fncall _TestArray();		// TestArray.cpp
//v_fncall TestBrowseObjectsDialog(HWND hwndParent);		// TestBrowseObjectsDialog.cpp file

class LTestfkNotifyCmdImpl : public LTestfkNotifyCmd
{
private:
	IID*				_pSelfIID;
	fk::INotifyCommand*		_pNotifyCmdParent;

	void OnFileOpen(fk::ICommandItem* pCmdUI);
	void OnFileClose(void);
	void OnShowOptionsDlg(fk::ICommandItem* pCmdUI);

	void Test_fk_pv1(fk::LObject* pparam1, fk::LObject* pparam2, fk::LObject* pparam3, fk::LObject* pparam4,
		fk::LObject* pparam5, fk::LObject* pparam6, fk::LObject* pparam7, fk::LObject* pparam8, fk::LObject* pparam9);

	void OnTestFK_ParamVerify(fk::ICommandItem* pCmdUI);
	void OnTestLStringw(fk::ICommandItem* pCmdUI);
	void OnTestLEnumStringw(fk::ICommandItem* pCmdUI);
	void OnTestAutoPtr(fk::ICommandItem* pCmdUI);
	void OnTestArray(fk::ICommandItem* pCmdUI);
	void OnThreadListOrThread(fk::ICommandItem* pCmdUI);
	v_call OnToolsToolsOptions(fk::ICommandItem* pCmdUI);
	v_call OnTestCommandLine(fk::ICommandItem* pCmdUI);
	v_call OnDialogShowNewProject(fk::ICommandItem* pCmdUI);

	v_call OnTestRuntimeClass(void);

	//v_call OnTestBrowseObjectsDialog(fk::ICommandItem* pCmdUI);
	v_call OnShowModuleConfigManagerDialog(fk::ICommandItem* pCmdUI);
	v_call OnShowModuleConfigMainFrame(fk::ICommandItem* pcmdUI);
	v_call OnTestMainFrameTestSDIMainFrame(void);

	v_call OnErrorThrow(fk::ICommandItem* pcmdUI);

	v_call OnTestBaseData(void);

protected:
public:
	LTestfkNotifyCmdImpl();
	virtual ~LTestfkNotifyCmdImpl();

	FK_COM_ARQ();

	virtual HRESULT STDMETHODCALLTYPE NotifyCommand(UINT codeNotify, int nCmdID, fk::ICommandItem *pCmdUI);
	virtual HRESULT STDMETHODCALLTYPE QueryState(UINT nCmdID, fk::ICommandItem *pCmdUI);

	virtual STDMETHODIMP NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb);
	virtual STDMETHODIMP GetCommandTips(UINT iCmdID, BSTR *bstrTips);
	virtual STDMETHODIMP ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI);
	virtual STDMETHODIMP BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu);

	virtual STDMETHODIMP GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo);
	virtual STDMETHODIMP SetParam(ULONG lParam);
	virtual STDMETHODIMP QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand);
	virtual STDMETHODIMP QueryNotifyCommandParam();

	virtual STDMETHODIMP SetNotifyCommandParent(fk::INotifyCommand* pNotifyCmd);
	virtual STDMETHODIMP GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd);
};
//v_call LTestfkNotifyCmdImpl::OnTestBrowseObjectsDialog(fk::ICommandItem* pCmdUI)
//{
//	TestBrowseObjectsDialog(::GetActiveWindow());
//}

v_call LTestfkNotifyCmdImpl::OnShowModuleConfigManagerDialog(fk::ICommandItem* pCmdUI)
{
	fk::ModuleConfigManagerDlg* pdlg;

	pdlg = fk::ModuleConfigManagerDlg::NewModuleConfigManagerDlg();

	pdlg->DoModal(::GetActiveWindow());
}

v_call LTestfkNotifyCmdImpl::OnShowModuleConfigMainFrame(fk::ICommandItem* pcmdUI)
{
	fk::ShowModuleConfigMainFrame();
}

v_call LTestfkNotifyCmdImpl::OnTestMainFrameTestSDIMainFrame(void)
{
	//fk_autop(fk::LTestMainFrameMgr*, ptmf);

	//if (fk::LTestMainFrameMgr::GetTestMainFrameManager(&ptmf))
	//{
	//	ptmf->CreateTestMainFrame();
	//}
}

v_call LTestfkNotifyCmdImpl::OnErrorThrow(fk::ICommandItem* pcmdUI)
{
	fk::LError	error;

	try
	{
		error.AddItemComma(L"aaaaa");
		error.AddItemComma(L"bbbbb");
		error.AddItemComma(L"ccccc");

		if (error.IsError())
		{
			throw 0;
		}
	}
	catch(...)
	{
		error.TraceError();

		fk_ErrorBox();
	}
}

void LTestfkNotifyCmdImpl::OnShowOptionsDlg(fk::ICommandItem* pCmdUI)
{
	fk_TraceText(FTEXTNIL, _T("显示设置对话框。\n") );
	fk::ITreePropertySheet* psheet;
	xmlDoc* pxmldoc;
	HRESULT	hr;
	HRESULT nDlgRet;
	fk::LAutoPtr ap;

	try
	{
		if (fk::GetConfig(fk::HCONFIG_APPLICATION, &pxmldoc))
		{
			hr = CoCreateInstancePtr(fk::CLSID_TreePropertySheet, NULL, CLSCTX_ALL, fk::IID_ITreePropertySheet, (void**)&psheet);
			if(hr==S_OK)
			{
				hr=psheet->SetConfigPos((ULONG*)pxmldoc, ap.SysAllocString(fk::XMLF_TREE_PROPERTY_SHEET_W));
				if(hr==S_OK)
				{
					nDlgRet = psheet->DoModal(GetActiveWindow());
					psheet->Release();
					if (nDlgRet==FALSE)
					{
						fk_ErrorBox();
					}
				}
				else if(hr==S_FALSE)
				{
					psheet->Release();
				}
			}
			else
			{
				ATLASSERT(hr == S_OK);
				fk_TraceFormatMessage(FTEXTNIL, HRESNIL, L"\n", hr);
				fk_ErrorBox();
			}
		}
		else
		{
			fk_ErrorBox();
		}
	}
	catch(fk::CComException *pException)
	{
		fk_TraceError(FTEXTNIL, g_pmodule->hRes, pException->GetErrorInfo());

		pException->ShowErrorBox();
		delete pException;
	}
}


LTestfkNotifyCmdImpl::LTestfkNotifyCmdImpl()
{

}


LTestfkNotifyCmdImpl::~LTestfkNotifyCmdImpl()
{

}

FK_COM_AR_DELETE_CODE(LTestfkNotifyCmdImpl);

STDMETHODIMP LTestfkNotifyCmdImpl::QueryInterface(REFIID riid, void **ppv)
{
	FK_QUERY_IID(riid, fk::IID_INotifyCommand, fk::INotifyCommand, ppv, this);
	FK_QUERY_IID(riid, IID_IUnknown, IUnknown, ppv, this);

	return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE LTestfkNotifyCmdImpl::NotifyCommand(UINT codeNotify, int iCmdID, fk::ICommandItem *pCmdUI)
{
	FK_COMMAND_HANDLE(ID_FILE_OPEN, OnFileOpen(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TESTFK_PARAMVERIFY1, OnTestFK_ParamVerify(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_LSTRINGW, OnTestLStringw(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_LENUMSTRINGW, OnTestLEnumStringw(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_LAUTOPTR, OnTestAutoPtr(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_THREADLISTORTHREAD, OnThreadListOrThread(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_COMMANDLINE, OnTestCommandLine(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TEST_TESTARRAY, OnTestArray(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TOOLS_TOOLSOPTIONS, OnToolsToolsOptions(pCmdUI), return 0);
	FK_COMMAND_HANDLE(ID_DIALOG_SHOWNEWPROJECT, OnDialogShowNewProject(pCmdUI), return 0;);	
	FK_COMMAND_HANDLE(ID_TEST_SHOWMODULECONFIGMANAGERDIALOG, this->OnShowModuleConfigManagerDialog(pCmdUI), return 0);
	FK_COMMAND_HANDLE(ID_TEST_RUNTIMECLASS, OnTestRuntimeClass(), return 0; );
	FK_COMMAND_HANDLE(ID_ERROR_ERROR, OnErrorThrow(pCmdUI), return 0;);
	FK_COMMAND_HANDLE(ID_TOOLS_SHOWMODULECONFIGMAINFRAME, OnShowModuleConfigMainFrame(pCmdUI), return 0;);
	//FK_COMMAND_HANDLE(ID_TEST_TESTBROWSEOBJECTSDIALOG, OnTestBrowseObjectsDialog(pCmdUI), return 0; );

	FK_COMMAND_HANDLE(ID_TESTMAINFRAME_TESTSDIMAINFRAME, OnTestMainFrameTestSDIMainFrame(), return 0;);

	return S_OK;
}

HRESULT STDMETHODCALLTYPE LTestfkNotifyCmdImpl::QueryState(UINT iCmdID, fk::ICommandItem *pCmdUI)
{

	return S_OK;
}

STDMETHODIMP LTestfkNotifyCmdImpl::NotifyCommandPoint(UINT iCmdID, POINT *pt, fk::ICommandItem *pCmdUI, IDispatch *ppmb)
{
	pCmdUI->put_NextCmd(VARIANT_TRUE);
	return S_OK;
}

STDMETHODIMP LTestfkNotifyCmdImpl::GetCommandTips(UINT iCmdID, BSTR *bstrTips)
{
	ATL::CComBSTR bstrRet;

	bstrRet.LoadString(g_pmodule->hRes, iCmdID);
	if(bstrRet.Length()>0)
	{
		*bstrTips = bstrRet.Detach();
		return S_OK;
	}

	return S_FALSE;
}

STDMETHODIMP LTestfkNotifyCmdImpl::ExpansionCommand(UINT iCmdID, fk::ICommandItem *pCmdUI)
{
	pCmdUI->put_NextCmd(VARIANT_FALSE);

	return S_OK;
}

STDMETHODIMP LTestfkNotifyCmdImpl::wmNotify(UINT_PTR wParam, LONG_PTR lParam, fk::ICommandItem *pCmdUI)
{
	return S_OK;
}

STDMETHODIMP LTestfkNotifyCmdImpl::BuildMenuNc(fk::IPopupMenuBar* pPopupMenuBar, LPCSTR pszPath, UINT uiPos, fk::enumInsertMenu umInsertMenu)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}


STDMETHODIMP LTestfkNotifyCmdImpl::GetInfo(fk::COMMANDNOTIFYINFO** ppNotifyInfo)
{
	ATLASSERT(0);

	return E_NOTIMPL;
}

STDMETHODIMP LTestfkNotifyCmdImpl::SetParam(ULONG lParam)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP LTestfkNotifyCmdImpl::QueryChildNotifyCommandParam(IID* piid, fk::INotifyCommand* pChildNotifyCommand)
{
	ATLASSERT(0);
	return E_NOTIMPL;
}

STDMETHODIMP LTestfkNotifyCmdImpl::QueryNotifyCommandParam()
{
	HRESULT hr;

	if (_pNotifyCmdParent!=NULL)
	{
		hr = _pNotifyCmdParent->QueryChildNotifyCommandParam(_pSelfIID, this);
		if (hr==S_OK)
			return S_OK;

		return S_FALSE;
	}

	return S_FALSE;
}

STDMETHODIMP LTestfkNotifyCmdImpl::SetNotifyCommandParent(fk::INotifyCommand* pNotifyCommand)
{
	if (pNotifyCommand!=NULL)
	{
		if (pNotifyCommand!=NULL)
		{
			_pNotifyCmdParent = pNotifyCommand;
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}

STDMETHODIMP LTestfkNotifyCmdImpl::GetNotifyCommandParent(fk::INotifyCommand** ppNotifyCmd)
{
	if (ppNotifyCmd!=NULL)
	{
		if (_pNotifyCmdParent!=NULL)
		{
			*ppNotifyCmd = _pNotifyCmdParent;
			_pNotifyCmdParent->AddRef();
			return S_OK;
		}

		return E_INVALIDARG;
	}

	return E_POINTER;
}



LTestfkNotifyCmd::LTestfkNotifyCmd()
{
}

LTestfkNotifyCmd::~LTestfkNotifyCmd()
{
}

b_fncall LTestfkNotifyCmd::NewTestfkNotifyCmd_IID(LPVOID* ppv, REFIID riid  _FK_OBJECT_POS_CXX2_)
{
	LTestfkNotifyCmd* pNotifyCmd = NULL;

	pNotifyCmd = new LTestfkNotifyCmdImpl();
	pNotifyCmd->AddRef();

	*ppv = (fk::INotifyCommand*)(LTestfkNotifyCmd*)pNotifyCmd;

	return true;
}


LTestfkNotifyCmd* _fncall LTestfkNotifyCmd::NewTestfkNotifyCmd(void)
{
	tfk::LTestfkNotifyCmd* pNotifyCmd = NULL;

	pNotifyCmd = new tfk::LTestfkNotifyCmdImpl();
	pNotifyCmd->AddRef();

	return pNotifyCmd;
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
// 检测函数传入的参数。在函数的其它地方也可以调用 fk_pv 宏，这里写这么多是为了测试。
//
void LTestfkNotifyCmdImpl::Test_fk_pv1(fk::LObject* pobj1, fk::LObject* pobj2,
									   fk::LObject* pobj3, fk::LObject* pobj4,
									   fk::LObject* pobj5, fk::LObject* pobj6,
									   fk::LObject* pobj7, fk::LObject* pobj8,
									   fk::LObject* pobj9)
{
	fk_pv1(pobj1==NULL, ; );
	fk_pv2(pobj1==NULL, pobj2==NULL,  ; );
	fk_pv3(pobj1==NULL, pobj2==NULL, pobj3==NULL,  ; );
	fk_pv4(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL,  ; );
	fk_pv5(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL, pobj5==NULL,  ; );
	fk_pv6(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL, pobj5==NULL, pobj6==NULL,  ; );
	fk_pv7(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL, pobj5==NULL, pobj6==NULL, pobj7==NULL,  ; );
	fk_pv8(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL, pobj5==NULL, pobj6==NULL, pobj7==NULL, pobj8==NULL,  ; );
	fk_pv9(pobj1==NULL, pobj2==NULL, pobj3==NULL, pobj4==NULL, pobj5==NULL, pobj6==NULL, pobj7==NULL, pobj8==NULL, pobj9==NULL, return ; );
}


void LTestfkNotifyCmdImpl::OnFileOpen(fk::ICommandItem* pCmdUI)
{
}

void LTestfkNotifyCmdImpl::OnTestFK_ParamVerify(fk::ICommandItem* pCmdUI)
{
	fk::LObject*		pparam1 = NULL;
	fk::LObject*		pparam2 = NULL;
	fk::LObject*		pparam3 = NULL;
	fk::LObject*		pparam4 = NULL;
	fk::LObject*		pparam5 = NULL;
	fk::LObject*		pparam6 = NULL;
	fk::LObject*		pparam7 = NULL;
	fk::LObject*		pparam8 = NULL;
	fk::LObject*		pparam9 = NULL;

	Test_fk_pv1(pparam1, pparam2, pparam3, pparam4, pparam5, pparam6, pparam7, pparam8, pparam9);
}

void LTestfkNotifyCmdImpl::OnTestLStringw(fk::ICommandItem* pCmdUI)
{
	// 
	// 自动管理大小。假如第一次分配100字节，以后100-10=90，在90-100之间不在重新分配，
	// 那个10字符可以使用 SetDeviationTextw 函数设置10-50字节之间。
	//
	fk::LStringw    sAuto;				// 等同于 MallocAuto 方法，可以忽略，不调用。
	wchar_t*		szTextValue		= L"100";
	int				iCountValue		= 100;
	BYTE			byCountValue	= 100;
	float			fCountValue		= 10.7654321;
	wchar_t*		szFloatValue	= L"10.765432";			// float 会取消最后一个小数 1.

	//
	// 在 MAX_PATH 区域内操作，如果大于MAX_PATH，会自动重新分配。
	//
	fk::LStringw	sText(fk::umStringBuffer, MAX_PATH);	//等同于 .MallocBuffer 方法。

	sText = szTextValue;
	FK_TEST_IF(sText.StrICmp(szTextValue)!=0);
	FK_TEST_IF(sText.GetLong()!=iCountValue);

	FK_TEST_IF(sText.SetLong(iCountValue, 10));
	FK_TEST_IF(sText.GetLong()!=iCountValue);
	FK_TEST_IF(sText.StrICmp(szTextValue)!=0);

	FK_TEST_IF(sText.SetInt(iCountValue, 10));
	FK_TEST_IF(sText.GetInt()!=iCountValue);
	FK_TEST_IF(sText.StrICmp(szTextValue)!=0);

	FK_TEST_IF(sText.SetByte(byCountValue, 10));
	FK_TEST_IF(sText.GetByte()!=byCountValue);
	FK_TEST_IF(sText.StrICmp(szTextValue)!=0);

	FK_TEST_IF(sText.SetFloat(fCountValue));
	FK_TEST_IF(sText.GetFloat()!=fCountValue);
	FK_TEST_IF(sText.StrICmp(szFloatValue)!=0);

	fCountValue		= 10.7654321;					// 重新定义值，因为那个最后一个小数 1 已经没有了。
	FK_TEST_IF(sText.SetDouble(fCountValue));
	FK_TEST_IF(sText.GetDouble()!=fCountValue);
	FK_TEST_IF(sText.StrICmp(szFloatValue)!=0);

	FK_TEST_IF(sText.LoadStr(g_pmodule->hRes, IDS_LOAD_STR)>0);
	FK_TEST_IF(sText.StrICmp(L"LoadStr")!=0);

	FK_TEST_IF(sText.LoadFormat(g_pmodule->hRes, IDS_LOAD_FORMAT, L"Format")>0);
	FK_TEST_IF(sText.StrICmp(L"LoadFormat")!=0);

	FK_TEST_IF(sText.Format(L"%s", L"Format")>0);
	FK_TEST_IF(sText.StrICmp(L"Format")!=0);

	FK_TEST_IF(sText.FormatL(L"%s", MAX_PATH, L"Format")>0);
	FK_TEST_IF(sText.StrICmp(L"Format")!=0);

	sText = L"Append";
	sText.AppendFormat(NULL, L"%s", L"Format");
	FK_TEST_IF(sText.StrICmp(L"AppendFormat")!=0);

	char szOut[MAX_PATH];
	szOut[0]= L'\0';
	sText	= "ToMbs";
	FK_TEST_IF(sText.ToMbs(szOut, MAX_PATH));
	FK_TEST_IF(stricmp(szOut, "ToMbs")!=0);

	sText = L"Append";
	sText.Appendw(L"w");
	FK_TEST_IF(sText.StrICmp(L"Appendw")!=0);

	sText = L"Append";
	sText.Appenda("a");
	FK_TEST_IF(sText.StrICmp(L"Appenda")!=0);

	sText = L"App";
	sText.Append(L"end");
	FK_TEST_IF(sText.StrICmp(L"Append")!=0);

	sText = L"Del1234ete";
	FK_TEST_IF(sText.Delete(3, 4));
	FK_TEST_IF(sText.StrICmp(L"Delete")!=0);

	sText = L"1234Delete";
	FK_TEST_IF(sText.Delete(0, 4));
	FK_TEST_IF(sText.StrICmp(L"Delete")!=0);

	sText = L"  TrimRL  ";
	sText.TrimRL();
	FK_TEST_IF(sText.StrICmp(L"TrimRL")!=0);

	sText = L"  TrimR  ";
	sText.TrimR();
	FK_TEST_IF(sText.StrICmp(L"  TrimR")!=0);

	sText = L"  TrimL  ";
	sText.TrimL();
	FK_TEST_IF(sText.StrICmp(L"TrimL  ")!=0);

	FK_TEST_IF(sText.Insert(10, L"Insert"));
	FK_TEST_IF(sText.StrICmp(L"Insert")!=0);

	sText = L"Inert";
	FK_TEST_IF(sText.Insert(2, L"s"));
	FK_TEST_IF(sText.StrICmp(L"Insert")!=0);

	sText = L"Inser";
	FK_TEST_IF(sText.Insert(5, L"t"));
	FK_TEST_IF(sText.StrICmp(L"Insert")!=0);

	sText = L"FindLastOf";
	int iCount = strlen("FindL");
	FK_TEST_IF(sText.FindLastOf(L"Last", -1)!=5)

		FK_TEST_IF(sText.FindI(L"Last", -1)!=5)
		FK_TEST_IF(sText.Find(L"last", -1)!=-1)

		fk::LStringw sSubText;
	sText = L"Text Sub";
	FK_TEST_IF(sText.Substr(5, 3, &sSubText));
	FK_TEST_IF(sSubText.StrICmp(L"Sub")!=0);

	sText = L"ABCD-1234-IJKL-MNOPQ";
	FK_TEST_IF(sText.Replace(L"1234", L"EFGH"));
	FK_TEST_IF(sSubText.StrICmp(L"ABCD-EFGH-IJKL-MNOPQ")!=0);

	sText = L"//config/view";
	sText.IfEndAddRL(L"/", L"items", L"/items");
	FK_TEST_IF(sSubText.StrICmp(L"//config/view/items")!=0);

	sText = L"ABCDEFGHIJILMNABCDqwertABCD1234";
	sText.remove(L"ABCD", -1, -1);
	FK_TEST_IF(sText.StrICmp(L"EFGHIJILMNqwert1234") != 0);
}


void LTestfkNotifyCmdImpl::OnTestLEnumStringw(fk::ICommandItem* pCmdUI)
{
	fk::LStringws   sStringws;
	fk::LEnumData*	pEnumData = NULL;
	fk::LStringw*	pItemText = NULL;
	fk::LStringw	sFullString;

	//FK_TEST_RUN(NULL, L"LTestfkNotifyCmdImpl::OnTestLEnumStringw");

	FK_TEST_IF(sStringws.NewTextw()->Assign(L"void "));
	FK_TEST_IF(sStringws.NewTextw()->Assign(L"void "));
	FK_TEST_IF(sStringws.NewTextw()->Assign(L"LTestfkNotifyCmdImpl::"));
	FK_TEST_IF(sStringws.NewTextw()->Assign(L"OnTestLEnumStringw"));
	FK_TEST_IF(sStringws.NewTextw()->Assign(L"_________"));
	FK_TEST_IF(sStringws.NewTextw()->Assign(L"(fk::ICommandItem* pCmdUI)"));
	FK_TEST_IF(sStringws.FindItemI(L"OnTestLEnumStringw")==2);

	FK_TEST_IF(sStringws.GetEnumData(&pEnumData));

	if (pEnumData!=NULL)
	{
		FK_TEST_IF(sStringws.FindItemI(L"OnTestLEnumStringw")>=0);

		FK_TEST_IF(sStringws.DeleteItem(3));

		pEnumData->Reset();
		while (pEnumData->Next(1, (data_t**)&pItemText, NULL))
		{
			fk::TraceDebug(FTEXTNIL, L"%s\n", fkpstr(pItemText));
		}

		FK_TEST_IF(sStringws.FullCompose(&sFullString, NULL));
		fk::TraceDebug(FTEXTNIL, L"%s\n", fkpstr(pItemText));
	}
}

v_fncall AutoPtrDestructiorInt(data_t* data)
{
	int* pIntArray;

	pIntArray = (int*)data;

	delete []pIntArray;
}

void LTestfkNotifyCmdImpl::OnTestAutoPtr(fk::ICommandItem* pCmdUI)
{
	fk::LAutoPtr		ap;
	HRESULT				hr;
	fk::IProcessMacro*  ppm, *ppm2, *ppm3, *ppm4, *ppm5, *ppm6, *ppm7, *ppm8 = NULL;
	fk::LNotify*		pNotifyObject = NULL;
	wchar_t*			pszBufferText = NULL;

	void Add(data_t** pptr, int iType);

	ppm = NULL;
	ap.AddUnknown((IUnknown**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	ap.clear();

	ppm = NULL; ppm2 = NULL;
	ap.AddUnknown2((IUnknown**)&ppm, (IUnknown**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;
	ap.AddUnknown3((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;  ppm4 = NULL;
	ap.AddUnknown4((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3, (IUnknown**)&ppm4);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm4);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;  ppm4 = NULL;  ppm5 = NULL;
	ap.AddUnknown5((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3, (IUnknown**)&ppm4, (IUnknown**)&ppm5);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm4);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm5);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;  ppm4 = NULL;  ppm5 = NULL;  ppm6 = NULL;
	ap.AddUnknown6((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3, (IUnknown**)&ppm4, (IUnknown**)&ppm5, (IUnknown**)&ppm6);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm4);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm5);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm6);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;  ppm4 = NULL;  ppm5 = NULL;  ppm6 = NULL;  ppm7 = NULL;
	ap.AddUnknown7((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3, (IUnknown**)&ppm4, (IUnknown**)&ppm5, (IUnknown**)&ppm6, (IUnknown**)&ppm7);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm4);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm5);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm6);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm7);
	ap.clear();

	ppm = NULL;  ppm2 = NULL;  ppm3 = NULL;  ppm4 = NULL;  ppm5 = NULL;  ppm6 = NULL;  ppm7 = NULL;  ppm8 = NULL;
	ap.AddUnknown8((IUnknown**)&ppm, (IUnknown**)&ppm2, (IUnknown**)&ppm3, (IUnknown**)&ppm4, (IUnknown**)&ppm5, (IUnknown**)&ppm6, (IUnknown**)&ppm7, (IUnknown**)&ppm8);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm2);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm3);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm4);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm5);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm6);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm7);
	hr = ::CoCreateInstancePtr(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm8);
	ap.clear();


	pNotifyObject = fk::LNotify::NewNotify(g_pmodule);
	ap.AddObject((fk::LObject**)&pNotifyObject);

	ppm = NULL;
	hr  = ap.CreateInstance(fk::CLSID_ProcessMacro, NULL, CLSCTX_ALL, fk::IID_IProcessMacro, (void**)&ppm);
	hr	= ap.QueryInterface(ppm, fk::IID_IProcessMacro, (void**)&ppm2);

	pszBufferText		= (wchar_t*)ap.Malloc(MAX_PATH);
	pszBufferText[0]	= L'\0';
	wcscpy(pszBufferText, L"1234567890ASDFGHJKL");

	int* pIntArray = new int[100];
	pIntArray[0] = 0;
	pIntArray[1] = 1;
	pIntArray[2] = 2;
	pIntArray[3] = 3;
	pIntArray[4] = 4;
	ap.AddDestructor((data_t**)&pIntArray, (fk::FNDESTRUCTOR)AutoPtrDestructiorInt);


	VARIANT	vtValue;
	vtValue.vt = VT_BSTR;
	vtValue.bstrVal = ::SysAllocString(L"1234567890");
	ap.AddVariantClear(&vtValue);


	char*  pszValue = NULL;
	wchar_t* pwszValue = NULL;

	//char*  pszValue = (char*)::malloc(MAX_PATH);
	//pszValue[0] = '\0';
	//strcpy(pszValue, "1234567890");
	//ap.AddChar(&pszValue);
	//pwszValue		= (wchar_t*)::malloc(MAX_PATH);
	//pwszValue[0]	= L'\0';
	//wcscpy(pwszValue, L"1234567890");
	//ap.AddWchar(&pwszValue);

	ap.clear();

	pszValue		= (char*)fk::fkMalloc(MAX_PATH);
	pszValue[0]		= '\0';
	strcpy(pszValue, "1234567890");
	ap.AddFKChar(&pszValue);

	pwszValue		= (wchar_t*)fk::fkMalloc(MAX_PATH);
	pwszValue[0]	= L'\0';
	wcscpy(pwszValue, L"1234567890");
	ap.AddFKWchar(&pwszValue);

	//void AddBase(LBase** pBase);

	fk::LStringw*  pNewStringw = new fk::LStringw();
	ap.AddBaseData((fk::LVar**)&pNewStringw);

	//void AddItemIDList(LPITEMIDLIST lpItemIDList);
	ap.SysAllocString(L"1234567890");

	BSTR bstrValue = ::SysAllocString(L"1234567890-123");
	ap.AddSysAllocString(&bstrValue);

	GUID* pguid = ap.MallocGuid();

	ap.clear();
}
void LTestfkNotifyCmdImpl::OnTestArray(fk::ICommandItem* pCmdUI)
{
	_TestArray();
}

void LTestfkNotifyCmdImpl::OnThreadListOrThread(fk::ICommandItem* pCmdUI)
{
	fk::LThreadListTestDlg*  dlg;

	dlg = fk::LThreadListTestDlg::NewThreadListTestDlg();
	dlg->DoModal(GetActiveWindow());

	dlg->Release();
}

//
// 	"U:\1.97\Build\prj\ErrorProject\ErrorProject.vcproj" /clean /platform:"Debug|Win32" /time /S:3 /logfile:"f:\1.97\LogFile.txt"

//
v_call LTestfkNotifyCmdImpl::OnTestCommandLine(fk::ICommandItem* pCmdUI)
{
	fk::FCOMMANDLINE		fCommandLine= NULL;
	fk::LPCOMMANDLINEITEM	pcli		= NULL;
	int						iCount	= 0;
	data_t*					pdata	= NULL;
	fk::LStringw			sCommandLine;

	sCommandLine = L"\"e:\\1.97\\test\\test CommandLine\\CmdLine.txt\" /S:4 /p e:\\1.97\\test\\testCommandLine\\CmdLine.txt \
					/rebuild \"Debug|win32\"";

	fCommandLine = fk::ClCreateCommandLine();
	FK_TEST_IF(fCommandLine==NULL);

	FK_TEST_IF(!fk::ClAddParam(fCommandLine, fk::CLIF_EXIST_VALUE, L"/S", L"ssssssssssssss"));
	FK_TEST_IF(!fk::ClAddParam(fCommandLine, fk::CLIF_EXIST_VALUE, L"/rebuild", L"rebuild"));
	FK_TEST_IF(!fk::ClAddParam(fCommandLine, 0, L"/p", L"ppp"));

	FK_TEST_IF(!fk::ClSetCommandLine(fCommandLine, fkstr(sCommandLine)) );

	iCount = fk::ClGetCount(fCommandLine);
	FK_TEST_IF(iCount<=0);

	pdata = fk::ClGetDatas(fCommandLine);
	FK_TEST_IF(pdata==NULL);

	FK_TEST_IF(fk::ClGetPathCount(fCommandLine)!=2);
	FK_TEST_IF(fk::ClGetParamCount(fCommandLine)!=3);

	pcli = fk::ClFindParam(fCommandLine, L"/S");
	FK_TEST_IF(pcli==NULL);
	FK_TEST_IF(pcli->iParamIndex<0);
	FK_TEST_IF(pcli->iPathIndex!=-1);
	FK_TEST_IF(pcli->sParam.IsEmpty());
	FK_TEST_IF(pcli->sValue.IsEmpty());
	FK_TEST_IF(pcli->sParam.StrICmp(L"/s")!=0);
	FK_TEST_IF(pcli->sValue.StrICmp(L"4")!=0);
	FK_TEST_IF(pcli->sShowValue.IsEmpty());

	pcli = fk::ClFindParam(fCommandLine, L"/rebuild");
	FK_TEST_IF(pcli==NULL);
	FK_TEST_IF(pcli->iParamIndex<0);
	FK_TEST_IF(pcli->iPathIndex!=-1);
	FK_TEST_IF(pcli->sParam.IsEmpty());
	FK_TEST_IF(pcli->sValue.IsEmpty());
	FK_TEST_IF(pcli->sShowValue.IsEmpty());
	FK_TEST_IF(pcli->sParam.StrICmp(L"/REBUILD")!=0);
	FK_TEST_IF(pcli->sValue.StrICmp(L"\"Debug|Win32\"")!=0);
	FK_TEST_IF(pcli->sShowValue.StrICmp(L"Debug|Win32")!=0);

	pcli = fk::ClFindPathFromIndex(fCommandLine, 0);
	FK_TEST_IF(pcli==NULL);
	FK_TEST_IF(pcli->iParamIndex!=-1);
	FK_TEST_IF(pcli->iPathIndex<0);
	FK_TEST_IF(!pcli->sParam.IsEmpty());
	FK_TEST_IF(pcli->sValue.IsEmpty());
	FK_TEST_IF(pcli->sShowValue.IsEmpty());
	FK_TEST_IF(pcli->sValue.StrICmp(L"\"e:\\1.97\\test\\test CommandLine\\CmdLine.txt\"")!=0);
	FK_TEST_IF(pcli->sShowValue.StrICmp(L"e:\\1.97\\test\\test CommandLine\\CmdLine.txt")!=0);

	pcli = fk::ClFindPathFromIndex(fCommandLine, 1);
	FK_TEST_IF(pcli==NULL);
	FK_TEST_IF(pcli->iParamIndex!=-1);
	FK_TEST_IF(pcli->iPathIndex<0);
	FK_TEST_IF(!pcli->sParam.IsEmpty());
	FK_TEST_IF(pcli->sValue.IsEmpty());
	FK_TEST_IF(pcli->sShowValue.IsEmpty());
	FK_TEST_IF(pcli->sValue.StrICmp(L"e:\\1.97\\test\\testCommandLine\\CmdLine.txt")!=0);
	FK_TEST_IF(pcli->sShowValue.StrICmp(L"e:\\1.97\\test\\testCommandLine\\CmdLine.txt")!=0);

	for (; *pdata!=FK_DATA_END; pdata++)
	{
		pcli = (fk::LPCOMMANDLINEITEM)*pdata;

		fk::TraceTestText(FTEXTNIL, L"PathIndex:%d,ParamIndex:%d,sParam:%s sValue:%s\r\n",
			pcli->iPathIndex, pcli->iParamIndex, pcli->sParam._pwstr, pcli->sValue._pwstr);
	}

	FK_TEST_IF(!fk::ClDestroyCommandLine(fCommandLine));
}


v_call LTestfkNotifyCmdImpl::OnDialogShowNewProject(fk::ICommandItem* pCmdUI)
{
	fk::LAutoPtr		ap;
	fk::LNewProject*	pnp;

	fknew2(pnp = fk::LNewProject::NewNewProject(), pnp, ap);
	pnp->DoModal(::GetActiveWindow());
}

v_call LTestfkNotifyCmdImpl::OnTestRuntimeClass(void)
{
	FK_RUN_NULL(FTEXTNIL, L"测试 LRuntimeClass 功能。");
	//fk_autop(fk::LHistoryTest*, pht);
	//fk::LRuntimeClass* prcThis;
	//fk::LObject* pobject;
	//data_t* pdata;

	//pht = fk::LHistoryTest::NewHistoryTest(g_pmodule);
	//pht = fk::LHistoryTest::NewHistoryTest(g_pmodule);
	//pht = fk::LHistoryTest::NewHistoryTest(g_pmodule);

	//prcThis = fk::LHistoryTest::GetEndClass();

	//pdata = g_pmodule->GetObjects()->GetDatas();
	//for (; fkDataIsExist(pdata); pdata++) {
	//	pobject = (fk::LObject*)*pdata;
	//	if (pobject->IsKindOf(prcThis))
	//	{
	//		fk::TraceExecute(FTEXTNIL, L"%s 是 fk::LHistoryTest。\n", pobject->GetClassName()._pwstr);
	//	}
	//	else
	//	{
	//		fk::TraceExecute(FTEXTNIL, L"%s 不是 fk::LHistoryTest。\n", pobject->GetClassName()._pwstr);
	//	}
	//}

	fk::LRuntimeClass* pRuntimeClass;

	// 没有创建 tfk::LTestRuntimeClass 对象，获得它的运行库。
	pRuntimeClass = tfk::LTestRuntimeClass::GetThisRuntimeClass();
}

v_call LTestfkNotifyCmdImpl::OnToolsToolsOptions(fk::ICommandItem* pCmdUI)
{
	fk_TraceText(FTEXTNIL, _T("显示设置对话框。\n") );
	fk::LAutoPtr			ap;
	fk::ITreePropertySheet* psheet;
	xmlDoc* pxmldoc;
	HRESULT	hr;
	HRESULT nDlgRet;

	try
	{
		ap.AddUnknown((IUnknown**)&psheet);
		if (fk::GetConfig(fk::HCONFIG_APPLICATION, &pxmldoc))
		{
			hr = CoCreateInstancePtr(fk::CLSID_TreePropertySheet, NULL, CLSCTX_ALL, fk::IID_ITreePropertySheet, (void**)&psheet);
			if (hr==S_OK)
			{
				hr = psheet->SetConfigPos((ULONG*)pxmldoc, CComBSTR(fk::XMLF_TREE_PROPERTY_SHEET));
				if (hr==S_OK)
				{
					nDlgRet = psheet->DoModal(GetActiveWindow());
					if (nDlgRet)
					{
						return ;
					}
				}
			}

			ATLASSERT(hr == S_OK);
			fk_TraceFormatMessage(FTEXTNIL, HRESNIL, L"\n", hr);
		}
	}
	catch(fk::CComException *pException)
	{
		fk_TraceError(FTEXTNIL, g_pmodule->hRes, pException->GetErrorInfo());

		pException->ShowErrorBox();
		delete pException;
	}

	fk_ErrorBox();
}
_TFK_END
