#ifndef __TestfkNotifyCmd_h__
#define __TestfkNotifyCmd_h__


_TFK_BEGIN

class LTestfkNotifyCmd : public fk::INotifyCommand
{
private:
protected:
public:
	_FK_DBGCLASSINFO_

	LTestfkNotifyCmd(void);
	virtual ~LTestfkNotifyCmd(void);

	static b_fncall NewTestfkNotifyCmd_IID(LPVOID* ppobj, REFIID riid=GUID_NULL  _FK_OBJECT_POS_H2_);
	static LTestfkNotifyCmd* _fncall NewTestfkNotifyCmd(void);
};

_TFK_END



#endif
