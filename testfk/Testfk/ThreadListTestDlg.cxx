#include "stdafx.h"
#include "ThreadListTestDlg.hxx"


_FK_BEGIN


class LThreadTest : public fk::LThread
{
private:
	UINT			_uiCount;
	fk::LWindow*	_pWndEdit;

public:
	LThreadTest(fk::LObject* pParentObject, fk::LWindow* pWndEdit, UINT uiCount, DWORD dwExitCode);
	virtual ~LThreadTest(void);

	virtual b_call execute(void);
};

LThreadTest::LThreadTest(fk::LObject* pParentObject, fk::LWindow* pWndEdit, UINT uiCount, DWORD dwExitCode):
fk::LThread(pParentObject, dwExitCode)
{
	_FK_DBGCLASSINFO_INIT_(L"fk::LThreadTest");

	_pWndEdit	= pWndEdit;
	_uiCount	= uiCount;
}

LThreadTest::~LThreadTest(void)
{
}

vb_call LThreadTest::execute(void)
{
	fk::LStringw   sText(fk::umStringBuffer, 100);

	for (UINT uiIndex=0; uiIndex<_uiCount; uiIndex++)
	{
		if (_dwThreadMask & fk::TS_ENDTHREAD)
		{
			break;
		}
		else
		{
			sText.Format(L"%d", uiIndex);
			_pWndEdit->SetWindowText(sText._pwstr);
			_pWndEdit->UpdateWindow();
		}

		::Sleep(300);
	}

	return true;
}





class  LThreadListTestDlgImpl: public fk::LThreadListTestDlg
{
private:
	FK_BEGIN_CODE(VARIABLE, THREADLISTTESTDLG_VXML)
	fk::LButton* _pCancelBut;
	fk::LEdit*	 _pEdit1;
	fk::LEdit*	 _pEdit2;
	fk::LEdit*	 _pEdit3;
	fk::LEdit*	 _pEdit4;
	fk::LEdit*	 _pEdit5;
	fk::LButton* _pCreateThreadListBtn;
	fk::LButton* _pStartThreadBtn;
	fk::LButton* _pStopBtn;
	fk::LButton* _pClearBtn;
	fk::LButton* _pThreadListInfoBtn;
	fk::LButton* _pAutoRunBtn;
	fk::LEdit*	 _pInfoEdit;
	FK_END_CODE(VARIABLE)

	FK_BEGIN_CODE(EVENTDEFAULT, THREADLISTTESTDLG_VXML)
	v_call OnInitDialog(void);
	void OnSize(WPARAM wParam, LPARAM lParam);
	void OnOKBtnClick(void);
	void OnCancelBtnClick(void);
	v_call OnCreateThreadListBtnClicked(void);
	v_call OnStartThreadBtnClicked(void);
	v_call OnStopBtnClicked(void);
	v_call OnClearBtnClicked(void);
	v_call OnThreadListInfoBtnClicked(void);
	v_call OnAutoRunBtnClicked(void);
	FK_END_CODE()

private:
	fk::LThreadList*	_pThreadList;
	b_call ThreadListInstallEvent(fk::enumInstallEvent ie);

public:
	static wchar_t*	THREADLISTTESTDLG_VXML;
	LThreadListTestDlgImpl();
	virtual ~LThreadListTestDlgImpl();

	// base class
	virtual INT_PTR _call DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual v_call OnCommandProc(fk::LWindow* pwindow, int iNotify);			// 窗口命令走的是这个�?
	virtual v_call OnNotifyProc(fk::LWindow* pwindow, LPNMHDR lpnmhdr);
	virtual ULONG STDMETHODCALLTYPE Release();

	virtual b_call LinkObjectVariable(void);
	virtual b_call LinkDataExchange(DWORD umDataExchangeType);

public:
	// user
};
wchar_t* LThreadListTestDlgImpl::THREADLISTTESTDLG_VXML	= L"ThreadListTestDlg.vxml";

//---------------------------------------------------------------------------------
LThreadListTestDlg::LThreadListTestDlg():
fk::LDialog(g_pmodule, (fk::LWindow*)NULL, LThreadListTestDlgImpl::THREADLISTTESTDLG_VXML)
{

}

LThreadListTestDlg::~LThreadListTestDlg()
{

}

LThreadListTestDlg* LThreadListTestDlg::NewThreadListTestDlg(void)
{
	LThreadListTestDlgImpl*  pNewThreadListTestDlg = NULL;

	pNewThreadListTestDlg = new LThreadListTestDlgImpl();
	if (pNewThreadListTestDlg!=NULL)
	{
		pNewThreadListTestDlg->AddRef();
		return pNewThreadListTestDlg;
	}

	return NULL;
}

//---------------------------------------------------------------------------------
//INT_PTR _call LThreadListTestDlgImpl::DialogProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
FK_BEGIN_DIALOG_PROC(LThreadListTestDlgImpl)
FK_MESSAGE_HANDLER(WM_SIZE, OnSize(wParam, lParam), return 0);
FK_MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog(), return 0);
FK_END_DIALOG_PROC(LThreadListTestDlg)		 //***** 是基类�?

// vv_call LThreadListTestDlgImplOnCommandProc(fk::LWindow* pWindow, int iNotify);			// 窗口命令走的是这个�?
FK_BEGIN_COMMAND_PROC(LThreadListTestDlgImpl)
FK_COMMAND_EVENT(_pCancelBut, BN_CLICKED, OnCancelBtnClick());
FK_COMMAND_EVENT(_pCreateThreadListBtn, BN_CLICKED, OnCreateThreadListBtnClicked());
FK_COMMAND_EVENT(_pStartThreadBtn, BN_CLICKED, OnStartThreadBtnClicked());
FK_COMMAND_EVENT(_pStopBtn, BN_CLICKED, OnStopBtnClicked());
FK_COMMAND_EVENT(_pClearBtn, BN_CLICKED, OnClearBtnClicked());
FK_COMMAND_EVENT(_pThreadListInfoBtn, BN_CLICKED, OnThreadListInfoBtnClicked());
FK_COMMAND_EVENT(_pAutoRunBtn, BN_CLICKED, OnAutoRunBtnClicked());
FK_END_COMMAND_PROC()

// vv_call LThreadListTestDlgImpl::OnNotifyProc(fk::LWindow* pWindow, LPNMHDR lpnmhdr)
FK_BEGIN_NOTIFY_PROC(LThreadListTestDlgImpl)
//FK_NOTIFY_EVENT(_pExample, NM_DBLCLK, OnExampleDBLick(lpnmhdr));
//FK_NOTIFY_EVENT(_pExample, LVN_ITEMCHANGED, OnExampleItemChanged(lpnmhdr));
FK_END_NOTIFY_PROC()

// LinObjectVariable 函数代码�?RAD 设计器维护自动生成，不需要手动修改。在RAD对话框设计器中，提供非时时代码同步功能�?
vb_call LThreadListTestDlgImpl::LinkObjectVariable(void)
{
	fk::LINKOBJECTNAME lon[]={
	{(fk::LObject**)&_pCancelBut, L"_pCancelBut"},
	{(fk::LObject**)&_pEdit1, L"_pEdit1"},
	{(fk::LObject**)&_pEdit2, L"_pEdit2"},
	{(fk::LObject**)&_pEdit3, L"_pEdit3"},
	{(fk::LObject**)&_pEdit4, L"_pEdit4"},
	{(fk::LObject**)&_pEdit5, L"_pEdit5"},
	{(fk::LObject**)&_pCreateThreadListBtn, L"_pCreateThreadListBtn"},
	{(fk::LObject**)&_pStartThreadBtn, L"_pStartThreadBtn"},
	{(fk::LObject**)&_pStopBtn, L"_pStopBtn"},
	{(fk::LObject**)&_pClearBtn, L"_pClearBtn"},
	{(fk::LObject**)&_pThreadListInfoBtn, L"_pThreadListInfoBtn"},
	{(fk::LObject**)&_pAutoRunBtn, L"_pAutoRunBtn"},
	{(fk::LObject**)&_pInfoEdit, L"_pInfoEdit"},
	};

	if (!this->LinkWindowObject(lon, sizeof(lon)/ sizeof(lon[0])) )
	{
		// ������ʾ������Ϣ������ true, ��Ϊ���ô�������ȷ����ʾ��
		// ���� false�����ڲ���ʾ��
		//
		fk_ErrorBox();
		return true;
	}

	return true;
}

ULONG STDMETHODCALLTYPE LThreadListTestDlgImpl::Release()
{
	long dwRef = 0;

	if (ObjectRelease(&dwRef))
	{
		delete this;
	}

	return dwRef;
}

//---------------------------------------------------------------------------------
LThreadListTestDlgImpl::LThreadListTestDlgImpl()
{
		_pThreadList = fk::LThreadList::NewThreadList(this);
}

LThreadListTestDlgImpl::~LThreadListTestDlgImpl()
{
}

vb_call LThreadListTestDlgImpl::LinkDataExchange(DWORD umDataExchangeType)
{
	switch (umDataExchangeType)
	{
	case fk::ldeInstallData:
		{
		}return true;
	case  fk::ldeDataToVariable:
		{
		}return true;
	case  fk::ldeClearCtrl:
		{
		}return true;
	case fk::ldeClearVariable:
		{
		}return true;
	case fk::ldeBuildFinalData:
		{
		}return true;
	default:
		{
		}
	}

	return false;
}

// �¼�ע��

b_call LThreadListTestDlgImpl::ThreadListInstallEvent(fk::enumInstallEvent ie)
{
	fk::NOTIFYITEM		npi;
	fk::LNotify*		pnotify;

	npi.dwSize			= sizeof(fk::NOTIFYITEM);
	npi.dwMask			= fk::NIF_CLASSPROC;
	npi.dwActiveEvent	=	fk::ON_TLE_ADD | fk::ON_TLE_REMOVE | fk::ON_TLE_BEGIN_ENDTHREADLIST |
		fk::ON_TLE_END_ENDTHREADLIST | fk::ON_TLE_BEGIN_CANCELENDTHREADLIST | 
		fk::ON_TLE_END_CANCELENDTHREADLIST | fk::ON_TLE_BEGINCLEAR| fk::ON_TLE_ENDCLEAR | 
		fk::ON_TLE_BEGINENDTHREADONE | fk::ON_TLE_ENDENDTHREADONE;

	npi.pTargetProc		= (fk::LObject*)(this);
	npi.lParam			= NULL;
	npi.dwRegObject		= (void*)_pThreadList;
	npi.dwObjectID		= (DWORD)this;

	//ON_TLE_CREATETHREAD|
	//ON_TLE_BEGIN_ENDTHREAD|
	//ON_TLE_END_ENDTHREAD|

	FK_DEBUG_TARGETPROC_REGCLASS(npi, L"LThreadListTestDlg");

	pnotify = _pThreadList->GetNotify();

	if (ie==fk::umInstallEventInstall)
	{
		if (pnotify->NotifyAdd(&npi))
		{
			return true;
		}
		else
		{
			fk_ErrorBox();
		}
	}
	else if (ie==fk::umInstallEventUninstall)
	{
		if (pnotify->NotifyRemove(&npi))
		{
			return true;
		}
		else
		{
			fk_ErrorBox();
		}
	}

	return false;
}

b_fncall ThreadListGetEventInfo(UINT umsg, fk::LStringw* psInfo)
{
	switch (umsg)
	{
	case fk::ON_TLE_ADD:
		{
			psInfo->Assign(L"fk::ON_TLE_ADD");
		}break;
	case fk::ON_TLE_REMOVE:
		{
			psInfo->Assign(L"fk::ON_TLE_REMOVE");
		}break;
	case fk::ON_TLE_BEGIN_ENDTHREADLIST:
		{
			psInfo->Assign(L"fk::ON_TLE_BEGIN_ENDTHREADLIST");
		}break;
	case fk::ON_TLE_END_ENDTHREADLIST:
		{
			psInfo->Assign(L"fk::ON_TLE_END_ENDTHREADLIST");
		}break;
	case fk::ON_TLE_BEGIN_CANCELENDTHREADLIST:
		{
			psInfo->Assign(L"fk::ON_TLE_BEGIN_CANCELENDTHREADLIST");
		}break;
	case fk::ON_TLE_END_CANCELENDTHREADLIST:
		{
			psInfo->Assign(L"fk::ON_TLE_END_CANCELENDTHREADLIST");
		}break;
	case fk::ON_TLE_BEGINCLEAR:
		{
			psInfo->Assign(L"fk::ON_TLE_BEGINCLEAR");
		}break;
	case fk::ON_TLE_ENDCLEAR:
		{
			psInfo->Assign(L"fk::ON_TLE_ENDCLEAR");
		}break;
	case fk::ON_TLE_BEGINENDTHREADONE:
		{
			psInfo->Assign(L"fk::ON_TLE_BEGINENDTHREADONE");
		}break;
	case fk::ON_TLE_ENDENDTHREADONE:
		{
			psInfo->Assign(L"fk::ON_TLE_ENDENDTHREADONE");
		}break;
	default:
		{
			psInfo->Assign(L"����� fk::LThreadList �¼� ID��");
			FKASSERT(0);
			return false;
		}
	}

	return true;
}

v_call LThreadListTestDlgImpl::OnInitDialog(void)
{
	LinkDataExchange(fk::ldeInstallData);
}

void LThreadListTestDlgImpl::OnSize(WPARAM wParam, LPARAM lParam)
{
	int x;
	int y;

	x = LOWORD(lParam);
	y = LOWORD(lParam);
}


void LThreadListTestDlgImpl::OnOKBtnClick(void)
{
	this->EndDialog(IDOK);
}

void LThreadListTestDlgImpl::OnCancelBtnClick(void)
{
	this->EndDialog(IDCANCEL);
}


v_call LThreadListTestDlgImpl::OnCreateThreadListBtnClicked(void)
{
	fk::LThread*	pNewThread;
	fk::LEdit*		pEdit;
	fk::LEdit*		pEdits[] = {_pEdit1, _pEdit2, _pEdit3, _pEdit4, _pEdit5};
	UINT			uiCount[] = {9999, 9999999, 9999999, 99999999, 99999};

	if (_pThreadList->GetThreadCount() <= 0)
	{
		for (UINT uiIndex=0; uiIndex<sizeof(pEdits)/sizeof(pEdits[0]); uiIndex++)
		{
			pEdit = (fk::LEdit*)pEdits[uiIndex];
			fk_pv1(pEdit==NULL, ;);
			if (pEdit!=NULL)
			{
				fknew( pNewThread = new LThreadTest(this, pEdit, uiCount[uiIndex], (DWORD)pEdits[uiIndex]), pNewThread);
				_pThreadList->AddThread(pNewThread, (UINT)pEdits[uiIndex]);
				pEdit->SetWindowLong(GWL_USERDATA, (LONG)pEdit);
			}
		}
	}
}


v_call LThreadListTestDlgImpl::OnStartThreadBtnClicked(void)
{
	_pThreadList->RunThread();
}


v_call LThreadListTestDlgImpl::OnStopBtnClicked(void)
{
	_pThreadList->EndThreadListAndWait();
}


v_call LThreadListTestDlgImpl::OnClearBtnClicked(void)
{
	_pThreadList->clear();
}


v_call LThreadListTestDlgImpl::OnThreadListInfoBtnClicked(void)
{
	fk::LStringw sText;

	if (_pInfoEdit!=NULL)
	{
		sText.Format(L"ThreadList Count:%d\r\n", _pThreadList->GetThreadCount());
		_pInfoEdit->AppendText(sText);

		if (_pThreadList->IsStopThread())
		{
			sText.Format(L"ThreadList.IsStopThread() is true. \r\n");
			_pInfoEdit->AppendText(sText);
		}
		else
		{
			sText.Format(L"ThreadList.IsStopThread() is false. \r\n");
			_pInfoEdit->AppendText(sText);
		}
	}
}


v_call LThreadListTestDlgImpl::OnAutoRunBtnClicked(void)
{
}

FK_NOTIFY_POS

_FK_END
