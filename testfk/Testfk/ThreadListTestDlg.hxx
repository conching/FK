#ifndef _ThreadListTestDlg_h_
#define _ThreadListTestDlg_h_


_FK_BEGIN

class LThreadListTestDlg: public fk::LDialog
{
private:

public:
	LThreadListTestDlg();
	virtual ~LThreadListTestDlg();

	static LThreadListTestDlg* NewThreadListTestDlg(void);
};


_FK_END


#endif
