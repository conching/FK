//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Testfk.rc
//
#define IDS_PROJNAME                    100
#define IDR_TESTFK                      101
#define IDS_LOAD_FORMAT_L               101
#define IDS_LOAD_FORMAT                 102
#define IDR_MAINFRAME                   102
#define IDS_FORMAT                      103
#define IDS_FORMAT_L                    104
#define IDS_APPEND_FORMAT               105
#define IDS_LOAD_STR                    106
#define ID_TOOLS_OPTIONS                32768
#define ID_HELP_HELP                    32769
#define ID_DEMO_NULL                    32770
#define ID_TESTFK_PARAMVERIFY1          32771
#define ID_TEST_LOBJECTS                32772
#define ID_TEST_LSTRINGW                32773
#define ID_TEST_LENUMSTRINGW            32774
#define ID_TEST_LAUTOPTR                32775
#define ID_TEST_THREADLISTORTHREAD      32776
#define ID_TEST_COMMANDLINE             32777
#define ID_TEST_TESTARRAY               32778
#define ID_TEST_TESTARRAY32779          32779
#define ID_DIALOG_SHOWNEWPROJECT        32780
#define ID_TOOLS_TOOLSOPTIONS           32781
#define ID_TEST_TESTBROWSEOBJECTSDIALOG 32782
#define ID_TEST_SHOWMODULECONFIGMANAGERDIALOG 32783
#define ID_ERROR_ERROR                  32784
#define ID_TOOLS_SHOWMODULECONFIGMAINFRAME 32785
#define ID_TEST_RUNTIMECLASS            32786
#define ID_Menu                         32787
#define ID_TESTMAINFRAME_TESTSDIMAINFRAME 32788
#define ID_TESTMAINFRAME_TESTSDIMAINFRAMESPLITTER 32789

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
